CREATE OR REPLACE VIEW v_assets AS

SELECT
	asset_category.asset_category_id AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	assets_details.asset_serial_no AS `referenceCode`,
	assets_details.asset_serial_no AS `transactionCode`,
	assets_details.asset_id AS `patient_id`,
  	'' AS `recepientId`,
	account_type.account_type_id AS `accountParentId`,
	account_type.account_type_name AS `accountsclassfication`,
	`account`.`account_id` AS `accountId`,
	account.account_name AS `accountName`,
	'' AS `transactionName`,
	CONCAT(assets_details.asset_name) AS `transactionDescription`,
	(asset_amortization.endBalance) AS `dr_amount`,
	0 AS `cr_amount`,
	asset_amortization.amortizationDate AS `transactionDate`,
	asset_amortization.amortizationDate AS `createdAt`,
	1 AS `status`,
	2 AS `branch_id`,
	'FIXED ASSET' AS `transactionCategory`,
	'Amortization' AS `transactionClassification`,
	'account_payments' AS `transactionTable`,
	'' AS `referenceTable`
FROM
	assets_details,asset_category,account,account_type,asset_amortization
	WHERE assets_details.asset_id = asset_amortization.asset_id 
	AND assets_details.asset_category_id = asset_category.asset_category_id 
	AND account.account_id = asset_category.account_id
	AND account.account_type_id = account_type.account_type_id;