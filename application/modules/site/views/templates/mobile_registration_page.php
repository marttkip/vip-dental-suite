<?php 
	
	$contacts = $this->site_model->get_contacts();
	
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$email2 = $contacts['email'];
		$logo = $contacts['logo'];
		$company_name = $contacts['company_name'];
		$phone = $contacts['phone'];
		
		if(!empty($facebook))
		{
			$facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank" title="Facebook">Facebook</a></li>';
		}
		
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
	}
	// var_dump($logo);die();

	
?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url()."assets/themes/wizard/";?>assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="<?php echo base_url()."assets/themes/wizard/";?>assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title> <?php echo $company_name?> | Registration Form</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<!--     Fonts and icons     -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">

	<!-- CSS Files -->
    <link href="<?php echo base_url()."assets/themes/wizard/";?>assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url()."assets/themes/wizard/";?>assets/css/gsdk-bootstrap-wizard.css" rel="stylesheet" />

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link href="<?php echo base_url()."assets/themes/wizard/";?>assets/css/demo.css" rel="stylesheet" />
</head>

<body>
<div class="image-container set-full-height" style="background-image: url('<?php echo base_url()."assets/themes/wizard/";?>assets/img/wizard2.jpg')">
    <!--   Creative Tim Branding   -->
    <a href="#">
         <div class="logo-container">
            <!-- <div class="logo"> -->
                
            <!-- </div> -->
            <div class="brand">
                <?php echo $company_name?>
            </div>
        </div>
    </a>

	<!--  Made With Get Shit Done Kit  -->
		<a href="#" class="made-with-mk">
			<div class="brand">VIP DENTAL</div>
			<div class="made-with"> <strong></strong></div>
		</a>

		 <input type="hidden" id="base_url" value="<?php echo site_url();?>">
        <input type="hidden" id="config_url" value="<?php echo site_url();?>">
        <input type="hidden" id="config_url" value="<?php echo site_url();?>">
    <!--   Big container   -->
    <div class="container">

    	<?php echo $content?>
        
    </div> <!--  big container -->

    <div class="footer">
        <div class="container">
             Made with <i class="fa fa-heart heart"></i> by <a href="#">VIP Dental Suite</a>.</a>
        </div>
    </div>

</div>

</body>

	<!--   Core JS Files   -->
	<script src="<?php echo base_url()."assets/themes/wizard/";?>assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url()."assets/themes/wizard/";?>assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url()."assets/themes/wizard/";?>assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
	<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>  
	<!--  Plugin for the Wizard -->
	<script src="<?php echo base_url()."assets/themes/wizard/";?>assets/js/gsdk-bootstrap-wizard.js"></script>

	<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script src="<?php echo base_url()."assets/themes/wizard/";?>assets/js/jquery.validate.min.js"></script>

	<script type="text/javascript">
		
$(document).ready(function() {

	$('.datepicker').datepicker({
	    format: 'yyyy-mm-dd'
	});

});
$(document).on("submit","form#registration-form",function(e)
{

	e.preventDefault();
	
	var form_data = new FormData(this);

	var config_url = $('#config_url').val();	

	 var url = config_url+"site/register_patient_details";
       $.ajax({
       type:'POST',
       url: url,
       data:form_data,
       dataType: 'text',
       processData: false,
       contentType: false,
       success:function(data){
          var data = jQuery.parseJSON(data);
        
          	if(data.message == "success")
			{
				window.location.href = config_url+'thank-you';
				
			}
			else
			{

				alert(data.result);
			}
       
       },
       error: function(xhr, status, error) {
       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       
       }
       });
	 
	
   
	
});

function update_patient_history(history_id)
{
		var session_id = $('#session_id').val();
 		var checkedValue = $('#checkbox'+history_id).val();
  		 // alert(checkedValue);
  		 var XMLHttpRequestObject = false;
		 
		if (window.XMLHttpRequest) {

		 XMLHttpRequestObject = new XMLHttpRequest();
		} 
		 
		else if (window.ActiveXObject) {
		 XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		} 
		var config_url = $('#config_url').val();
		var url = config_url+"site/update_patient_history/"+history_id+"/"+session_id+"/"+checkedValue;
		// alert(url);
		if(XMLHttpRequestObject) {
		     
		 XMLHttpRequestObject.open("GET", url);
		     
		 XMLHttpRequestObject.onreadystatechange = function(){
		   
		   if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		   
		   }
		 }
		 
		 XMLHttpRequestObject.send(null);
		}
	
}

</script>

</html>
