<?php 

$date_timesession = date('YmdHis');
?>
<div class="row">
        <div class="col-sm-10 col-sm-offset-1">

            <!--      Wizard container        -->
            <div class="wizard-container">

                <div class="card wizard-card" data-color="orange" id="wizardProfile">
               
                    <?php   echo form_open("reception/register_other_patient_new", array("id" => "registration-form"));?>
                <!--        You can switch ' data-color="orange" '  with one of the next bright colors: "blue", "green", "orange", "red"          -->

                    	<div class="wizard-header">
                        	<h3>
                        	   <b>VIP DENTAL SUITE</b>  <br>
                        	   <small>This information will let us know more about you. Looking forward to serving you.</small>
                        	</h3>
                    	</div>

						<div class="wizard-navigation">
							<ul>
	                            <li><a href="#about" data-toggle="tab">About</a></li>
	                            <li><a href="#medical" data-toggle="tab">Medical Information</a></li>
	                            <li><a href="#account" data-toggle="tab">Complete</a></li>
	                        </ul>

						</div>
						<input type="hidden" name="session_id" id="session_id"  class="form-control" value="<?php echo $date_timesession?>" placeholder="Andrew..." autocomplete="off">
                        <div class="tab-content">
                            <div class="tab-pane" id="about">
                              <div class="row">
                                  <h4 class="info-text"> Let's start with the basic information </h4>
                                 
                                  <div class="col-sm-6">
                                      <div class="form-group">
                                        <label>Name <small>(required)</small></label>
                                        <input name="patient_surname" type="text" class="form-control" placeholder="Andrew Johns" autocomplete="off">
                                      </div>
                                      <div class="form-group">
                                      	<label>Gender <small>(required)</small></label>
	                                      <select class="form-control" name="gender_id" autocomplete="off">
				                            <?php
				                                if($genders->num_rows() > 0)
				                                {
				                                    $gender = $genders->result();
				                                    
				                                    foreach($gender as $res)
				                                    {
				                                        $gender_id = $res->gender_id;
				                                        $gender_name = $res->gender_name;
				                                        
				                                        if($gender_id == set_value("gender_id"))
				                                        {
				                                            echo '<option value="'.$gender_id.'" selected>'.$gender_name.'</option>';
				                                        }
				                                        
				                                        else
				                                        {
				                                            echo '<option value="'.$gender_id.'">'.$gender_name.'</option>';
				                                        }
				                                    }
				                                }
				                            ?>
				                        </select>
				                      </div>
                                      <div class="form-group">
                                        <label>Phone <small>(required)</small></label>
                                        <input name="patient_phone1" type="text" class="form-control" placeholder="+247..." autocomplete="off">
                                      </div>
                                     
                                       <div class="form-group">
                                          <label>Email <small>(required)</small></label>
                                          <input name="patient_email" type="email" class="form-control" placeholder="andrew@creative-tim.com" autocomplete="off">
                                      </div>
                                      <div class="form-group">
                                        <label>Birth Date <small>(required)</small></label>
                                        <input name="patient_dob" type="text" class="form-control datepicker" placeholder="" autocomplete="off">
                                      </div>
									  <div class="form-group">
                                        <label>Occupation </label>
                                        <input name="patient_occupation" type="text" class="form-control" placeholder="" autocomplete="off">
                                      </div>
                                      
                                  </div>

                                  <div class="col-sm-6">
                                  	  	
												
									
                                       <div class="form-group">
                                        <label>Other Phone </label>
                                        <input name="patient_phone2" type="text" class="form-control" placeholder="+247..." autocomplete="off">
                                      </div>
									  
                                      <div class="form-group">
                                        <label>Next Of Kin Name</label>
                                        <input name="patient_kin_sname" type="text" class="form-control" placeholder="Kin Surname..." autocomplete="off">
                                      </div>
                                      <div class="form-group">
                                        <label>Kin Contact</label>
                                        <input name="next_of_kin_contact" type="text" class="form-control" placeholder="Smith..." autocomplete="off">
                                      </div>
                                      <div class="form-group">
										<label>Relationship to Kin</label>
										<select class="form-control" name="relationship_id">
											<?php
												if($relationships->num_rows() > 0)
												{
													$relationship = $relationships->result();
													
													foreach($relationship as $res)
													{
														$relationship_id = $res->relationship_id;
														$relationship_name = $res->relationship_name;
														
														if($relationship_id == set_value("relationship_id"))
														{
															echo '<option value="'.$relationship_id.'" selected>'.$relationship_name.'</option>';
														}
														
														else
														{
															echo '<option value="'.$relationship_id.'">'.$relationship_name.'</option>';
														}
													}
												}
											?>
										</select>
                                      </div>
									  <div class="form-group">
                                        <label>Residence </label>
                                        <input name="patient_town" type="text" class="form-control" placeholder="" autocomplete="off">
                                      </div>


									  <div class="form-group">
										<label>Mode of Payment</label>
										<select class="form-control" name="visit_type_id">
											<?php
												if($visit_types->num_rows() > 0)
												{
													$visit_type = $visit_types->result();
													
													foreach($visit_type as $res)
													{
														$visit_type_id = $res->visit_type_id;
														$visit_type_name = $res->visit_type_name;
														
														if($visit_type_id == set_value("visit_type_id"))
														{
															echo '<option value="'.$visit_type_id.'" selected>'.$visit_type_name.'</option>';
														}
														
														else
														{
															echo '<option value="'.$visit_type_id.'">'.$visit_type_name.'</option>';
														}
													}
												}
											?>
										</select>
                                      </div>

									  <div class="form-group">
										<label>How did you know about us?</label>
										<select class="form-control" name="about_us_view"> 
											<?php
												if($about_us_views->num_rows() > 0)
												{
													$about_us_view = $about_us_views->result();
													
													foreach($about_us_view as $res)
													{
														$place_id = $res->place_id;
														$place_name = $res->place_name;
														
														if($place_id == set_value("place_id"))
														{
															echo '<option value="'.$place_id.'" selected>'.$place_name.'</option>';
														}
														
														else
														{
															echo '<option value="'.$place_id.'">'.$place_name.'</option>';
														}
													}
												}
											?>
										</select>
                                      </div>
                                  </div>
                                
                              </div>
                            </div>
                            
                            <div class="tab-pane" id="medical">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4 class="info-text"> Please share with us you medical history ? </h4>
                                    </div>
                                   	<?php

									if(!isset($visit_id))
									{
										$visit_id = 0;
									}
									if(!isset($patient_id))
									{
										$patient_id = 0;
									}
									$patient_history_form = $this->dental_model->get_patient_history_forms(1,0,7);

									$history =  "<table class='table table-condensed table-striped table-hover'> ";
									$count_disease = 0;

									if($patient_history_form->num_rows() > 0)
									{
										$patient_history_rs = $patient_history_form->result();
										
										foreach($patient_history_rs as $dis)
										{	
											$fd_id = $dis->patient_history_id;
											$disease = $dis->patient_history_name;
											
											

												$patient_history_status = 0;
											

											if($patient_history_status == 1)
											{
												$checked = "checked='checked'";
												$checked_value = 0;
											}
											else
											{
												$checked = '';
												$checked_value = 1;
											}
															
											$history =  $history."<tr>
																		<td style='width:90%' >".$disease." </td>
																		<td style='width:10%'> 
																			<input type='checkbox' id='checkbox".$fd_id."' onclick='update_patient_history(".$fd_id.")' ".$checked." value='".$checked_value."'>
																		</td>  
																</tr>";
											
											$count_disease++;
										}
									}
									$history .= "</table>
												";


									// history two 

									$patient_history_form2 = $this->dental_model->get_patient_history_forms(1,7,7);

									$history2 =  "<table class='table table-condensed table-striped table-hover'> ";
									$count_disease = 0;

									if($patient_history_form2->num_rows() > 0)
									{
										$patient_history_rs = $patient_history_form2->result();
										
										foreach($patient_history_rs as $dis)
										{	
											$fd_id = $dis->patient_history_id;
											$disease = $dis->patient_history_name;
											
											

												$patient_history_status = 0;
											

											if($patient_history_status == 1)
											{
												$checked = "checked='checked'";
												$checked_value = 0;
											}
											else
											{
												$checked = '';
												$checked_value = 1;
											}
															
											$history2 =  $history2."<tr>
																		<td style='width:90%' >".$disease." </td>
																		<td style='width:10%'> 
																			<input type='checkbox' id='checkbox".$fd_id."' onclick='update_patient_history(".$fd_id.")' ".$checked." value='".$checked_value."'>
																		</td>  
																</tr>";
											
											$count_disease++;
										}
									}
									$history2 .= "</table>
												";


									$patient_history_form3 = $this->dental_model->get_patient_history_forms(1,14,7);

									$history3 =  "<table class='table table-condensed table-striped table-hover'> ";
									$count_disease = 0;

									if($patient_history_form3->num_rows() > 0)
									{
										$patient_history_rs = $patient_history_form3->result();
										
										foreach($patient_history_rs as $dis)
										{	
											$fd_id = $dis->patient_history_id;
											$disease = $dis->patient_history_name;
											
											

											
												$patient_history_status = 0;
											

											if($patient_history_status == 1)
											{
												$checked = "checked='checked'";
												$checked_value = 0;
											}
											else
											{
												$checked = '';
												$checked_value = 1;
											}
															
											$history3 =  $history3."<tr>
																		<td style='width:90%' >".$disease." </td>
																		<td style='width:10%'> 
																			<input type='checkbox' id='checkbox".$fd_id."' onclick='update_patient_history(".$fd_id.")' ".$checked." value='".$checked_value."'>
																		</td>  
																</tr>";
											
											$count_disease++;
										}
									}
									$history3 .= "</table>
												";

									?>
									<!-- <div class="row"> -->
										<div class="col-md-12">
											<section class="panel panel-featured panel-featured-info">
												<header class="panel-heading">
													<h2 class="panel-title">Dental History (Please check the box for YES)</h2>
												</header>
												<div class="panel-body">
													<div class="col-md-12">
														<div class="col-md-4">
															<?php echo $history;?>
														</div>
														<div class="col-md-4">
															<?php echo $history2;?>
														</div>
														<div class="col-md-4">
															<?php echo $history3;?>
														</div>
													</div>
												</div>
											</section>
											<?php
											$patient_health_form = $this->dental_model->get_patient_history_forms(2,0,9);

											$health =  "<table class='table table-condensed table-striped table-hover'> ";
											$count_disease = 0;

											if($patient_health_form->num_rows() > 0)
											{
												$patient_health_rs = $patient_health_form->result();
												
												foreach($patient_health_rs as $dis)
												{	
													$fd_id = $dis->patient_history_id;
													$disease = $dis->patient_history_name;
													
													

													
														$patient_history_status = 0;
													

													if($patient_history_status == 1)
													{
														$checked = "checked='checked'";
														$checked_value = 0;
													}
													else
													{
														$checked = '';
														$checked_value = 1;
													}
																	
													$health =  $health."<tr>
																				<td style='width:90%' >".$disease." </td>
																				<td style='width:10%'> 
																					<input type='checkbox' id='checkbox".$fd_id."' onclick='update_patient_history(".$fd_id.")' ".$checked." value='".$checked_value."'>
																				</td>  
																		</tr>";
													
													$count_disease++;
												}
											}
											$health .= "</table>
														";


											// history two 

											$patient_health_form2 = $this->dental_model->get_patient_history_forms(2,9,9);

											$health2 =  "<table class='table table-condensed table-striped table-hover'> ";
											$count_disease = 0;

											if($patient_health_form2->num_rows() > 0)
											{
												$patient_health_rs = $patient_health_form2->result();
												
												foreach($patient_health_rs as $dis)
												{	
													$fd_id = $dis->patient_history_id;
													$disease = $dis->patient_history_name;
													
													

													
													$patient_history_status = 0;
													

													if($patient_history_status == 1)
													{
														$checked = "checked='checked'";
														$checked_value = 0;
													}
													else
													{
														$checked = '';
														$checked_value = 1;
													}
																	
													$health2 =  $health2."<tr>
																				<td style='width:90%' >".$disease." </td>
																				<td style='width:10%'> 
																					<input type='checkbox' id='checkbox".$fd_id."' onclick='update_patient_history(".$fd_id.")' ".$checked." value='".$checked_value."'>
																				</td>  
																		</tr>";
													
													$count_disease++;
												}
											}
											$health2 .= "</table>
														";


											$patient_health_form3 = $this->dental_model->get_patient_history_forms(2,18,9);

											$health3 =  "<table class='table table-condensed table-striped table-hover'> ";
											$count_disease = 0;

											if($patient_health_form3->num_rows() > 0)
											{
												$patient_health_rs = $patient_health_form3->result();
												
												foreach($patient_health_rs as $dis)
												{	
													$fd_id = $dis->patient_history_id;
													$disease = $dis->patient_history_name;
													
													

												
													$patient_history_status = 0;
													

													if($patient_history_status == 1)
													{
														$checked = "checked='checked'";
														$checked_value = 0;
													}
													else
													{
														$checked = '';
														$checked_value = 1;
													}
																	
													$health3 =  $health3."<tr>
																				<td style='width:90%' >".$disease." </td>
																				<td style='width:10%'> 
																					<input type='checkbox' id='checkbox".$fd_id."' onclick='update_patient_history(".$fd_id.")' ".$checked." value='".$checked_value."'>
																				</td>  
																		</tr>";
													
													$count_disease++;
												}
											}
											$health3 .= "</table>
														";

										


											?>

											<section class="panel panel-featured panel-featured-info">
												<header class="panel-heading">
													<h2 class="panel-title">Health History</h2>
												</header>
												<header class="panel-heading">
													<h5 class="panel-title">Have you had any of the following symptoms in the last 7 days? (Please check the box for YES)</h5>
												</header>
												<div class="panel-body">
													<div class="col-md-12">
														<div class="col-md-4">
															<?php echo $health;?>
														</div>
														<div class="col-md-4">
															<?php echo $health2;?>
														</div>
														<div class="col-md-4">
															<?php echo $health3;?>
														</div>
													</div>
													<div class="col-md-12">
														<div id="history_section"></div>
													</div>
													<div class="col-md-12">
														<?php echo $result;?>
													</div>

												</div>
											</section>
											<?php
											$patient_health_form = $this->dental_model->get_patient_history_forms(3,0,9);

											$health_first =  "<table class='table table-condensed table-striped table-hover'> ";
											$count_disease = 0;

											if($patient_health_form->num_rows() > 0)
											{
												$patient_health_rs = $patient_health_form->result();
												
												foreach($patient_health_rs as $dis)
												{	
													$fd_id = $dis->patient_history_id;
													$disease = $dis->patient_history_name;
													
													

													
														$patient_history_status = 0;
													

													if($patient_history_status == 1)
													{
														$checked = "checked='checked'";
														$checked_value = 0;
													}
													else
													{
														$checked = '';
														$checked_value = 1;
													}
																	
													$health_first =  $health_first."<tr>
																				<td style='width:90%' >".$disease." </td>
																				<td style='width:10%'> 
																					<input type='checkbox' id='checkbox".$fd_id."' onclick='update_patient_history(".$fd_id.")' ".$checked." value='".$checked_value."'>
																				</td>  
																		</tr>";
													
													$count_disease++;
												}
											}
											$health_first .= "</table>
														";


											$patient_health_form = $this->dental_model->get_patient_history_forms(3,9,9);

											$health_second =  "<table class='table table-condensed table-striped table-hover'> ";
											$count_disease = 0;

											if($patient_health_form->num_rows() > 0)
											{
												$patient_health_rs = $patient_health_form->result();
												
												foreach($patient_health_rs as $dis)
												{	
													$fd_id = $dis->patient_history_id;
													$disease = $dis->patient_history_name;
													
													

													
														$patient_history_status = 0;
													

													if($patient_history_status == 1)
													{
														$checked = "checked='checked'";
														$checked_value = 0;
													}
													else
													{
														$checked = '';
														$checked_value = 1;
													}
																	
													$health_second =  $health_second."<tr>
																				<td style='width:90%' >".$disease." </td>
																				<td style='width:10%'> 
																					<input type='checkbox' id='checkbox".$fd_id."' onclick='update_patient_history(".$fd_id.")' ".$checked." value='".$checked_value."'>
																				</td>  
																		</tr>";
													
													$count_disease++;
												}
											}
											$health_second .= "</table>
														";


											$patient_health_form = $this->dental_model->get_patient_history_forms(3,9,9);

											$health_third =  "<table class='table table-condensed table-striped table-hover'> ";
											$count_disease = 0;

											if($patient_health_form->num_rows() > 0)
											{
												$patient_health_rs = $patient_health_form->result();
												
												foreach($patient_health_rs as $dis)
												{	
													$fd_id = $dis->patient_history_id;
													$disease = $dis->patient_history_name;
													
													

													
														$patient_history_status = 0;
													

													if($patient_history_status == 1)
													{
														$checked = "checked='checked'";
														$checked_value = 0;
													}
													else
													{
														$checked = '';
														$checked_value = 1;
													}
																	
													$health_third =  $health_third."<tr>
																				<td style='width:90%' >".$disease." </td>
																				<td style='width:10%'> 
																					<input type='checkbox' id='checkbox".$fd_id."' onclick='update_patient_history(".$fd_id.")' ".$checked." value='".$checked_value."'>
																				</td>  
																		</tr>";
													
													$count_disease++;
												}
											}
											$health_third .= "</table>
														";
											?>

											<section class="panel panel-featured panel-featured-info">
												<header class="panel-heading">
													<h5 class="panel-title">Have you ever had any of the following diseases and for how long?(Please check the box for YES)</h5>
												</header>
												<div class="panel-body">
													<div class="col-md-12">
														<div class="col-md-4">
															<?php echo $health_first;?>
														</div>
														<div class="col-md-4">
															<?php echo $health_second;?>
														</div>
														<div class="col-md-4">
															<?php echo $health_third;?>
														</div>
													</div>
													<div class="col-md-12">
														<div id="history_section"></div>
													</div>
													<div class="col-md-12">
														<?php echo $result;?>
													</div>

												</div>
											</section>
										</div>
										
									</div>

                                <!-- </div> -->
                            </div>

                            <div class="tab-pane" id="account">
                                <h4 class="info-text"> Thank you for sharing with us your information.</h4>
                                <div class="row">

                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="col-sm-12">
                                        	<input type="checkbox" name="jobb" value="Design">
                                            I have accepted to share my informaiton and be treated at Health Rite
                                        </div>
                                        
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="wizard-footer height-wizard">
                            <div class="pull-right">
                                <input type='button' class='btn btn-next btn-fill btn-warning btn-wd btn-sm' name='next' value='Next' />
                                <input type='submit' class='btn btn-finish btn-fill btn-warning btn-wd btn-sm'  name='finish' value='Finish' />

                            </div>

                            <div class="pull-left">
                                <input type='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Previous' />
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </form>
                </div>
            </div> <!-- wizard container -->
        </div>
        </div><!-- end row -->