<div class="row" style="height: 60vh;margin-top: 100px;">
        <div class="col-sm-10 col-sm-offset-1" style="opacity: 1;background: rgba(76, 175, 80, 0.3);">
        	<div class="center-align" >
                <div class="col-md-4">

                    <div class="icon">
                    	<!-- <i class="fa-solid fa-check"></i> -->
                        <i class="fa fa-check" style="font-size:30vh;color: antiquewhite;"></i>
                    </div>
                   
                </div>

                <div class="col-md-8">

                	<div style="margin: 10vh auto; font-size:40px; font-weight: bold;color: antiquewhite;"> Thank you for successfully completing our registration form </div> 
                </div>
            </div>
        </div>
</div>