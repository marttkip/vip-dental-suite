<?php 

$date_timesession = date('YmdHis');
?>
<div class="row">
        <div class="col-sm-10 col-sm-offset-1">

            <!--      Wizard container        -->
            <div class="wizard-container">

                <div class="card wizard-card" data-color="orange" id="wizardProfile">
               
                    <?php   echo form_open("reception/register_other_patient_new", array("id" => "registration-form"));?>
                <!--        You can switch ' data-color="orange" '  with one of the next bright colors: "blue", "green", "orange", "red"          -->

                    	<div class="wizard-header">
                        	<h3>
                        	   <b>VIP DENTAL SUITE</b>  <br>
                        	   <small>This information will let us know more about you. Looking forward to serving you.</small>
                        	</h3>
                    	</div>

						<div class="wizard-navigation">
							<ul>
	                            <li><a href="#about" data-toggle="tab">About</a></li>
	                            <li><a href="#account" data-toggle="tab">Complete</a></li>
	                        </ul>

						</div>
						<input type="hidden" name="session_id" id="session_id"  class="form-control" value="<?php echo $date_timesession?>" placeholder="Andrew..." autocomplete="off">
                        <div class="tab-content">
                            <div class="tab-pane" id="about">
                              <div class="row">
                                  <h4 class="info-text"> Let's start with the basic information </h4>
                                 
                                  <div class="col-sm-12">
                                      <div class="form-group">
                                        <label>Name <small>(required)</small></label>
                                        <input name="patient_surname" type="text" class="form-control" placeholder="Andrew Johns" autocomplete="off">
                                      </div>
                                      <div class="form-group">
                                      	<label>Gender <small>(required)</small></label>
	                                      <select class="form-control" name="gender_id" autocomplete="off" required>
				                            <?php
				                                if($genders->num_rows() > 0)
				                                {
				                                    $gender = $genders->result();
				                                    
				                                    foreach($gender as $res)
				                                    {
				                                        $gender_id = $res->gender_id;
				                                        $gender_name = $res->gender_name;
				                                        
				                                        if($gender_id == set_value("gender_id"))
				                                        {
				                                            echo '<option value="'.$gender_id.'" selected>'.$gender_name.'</option>';
				                                        }
				                                        
				                                        else
				                                        {
				                                            echo '<option value="'.$gender_id.'">'.$gender_name.'</option>';
				                                        }
				                                    }
				                                }
				                            ?>
				                        </select>
				                      </div>
                                      <div class="form-group">
                                        <label>Phone <small>(required)</small></label>
                                        <input name="patient_phone1" type="text" class="form-control" placeholder="+247..." autocomplete="off">
                                      </div>
                                     
                                       <div class="form-group">
                                          <label>Email <small>(required)</small></label>
                                          <input name="patient_email" type="email" class="form-control" placeholder="andrew@creative-tim.com" autocomplete="off">
                                      </div>
                                      <div class="form-group">
                                        <label>Age <small>(required)</small></label>
                                        <!-- <input name="patient_dob" type="date" class="form-control datepicker"  max="3000-12-31" min="1000-01-01" placeholder="" autocomplete="off"> -->
                                        <input name="age" type="text" class="form-control"  placeholder="25" autocomplete="off" required>
                                      </div>
									  <div class="form-group" style="display:none">
                                        <label>Occupation </label>
                                        <input name="patient_occupation" type="text" class="form-control" placeholder="" autocomplete="off">
                                      </div>
                                      
                                  </div>

                                  <div class="col-sm-6" style="display: none;">
                                  	  	
												
									
                                       <div class="form-group">
                                        <label>Other Phone </label>
                                        <input name="patient_phone2" type="text" class="form-control" placeholder="+247..." autocomplete="off">
                                      </div>
									  
                                      <div class="form-group">
                                        <label>Next Of Kin Name</label>
                                        <input name="patient_kin_sname" type="text" class="form-control" placeholder="Kin Surname..." autocomplete="off">
                                      </div>
                                      <div class="form-group">
                                        <label>Kin Contact</label>
                                        <input name="next_of_kin_contact" type="text" class="form-control" placeholder="Smith..." autocomplete="off">
                                      </div>
                                      <div class="form-group">
										<label>Relationship to Kin</label>
										<select class="form-control" name="relationship_id">
											<?php
												if($relationships->num_rows() > 0)
												{
													$relationship = $relationships->result();
													
													foreach($relationship as $res)
													{
														$relationship_id = $res->relationship_id;
														$relationship_name = $res->relationship_name;
														
														if($relationship_id == set_value("relationship_id"))
														{
															echo '<option value="'.$relationship_id.'" selected>'.$relationship_name.'</option>';
														}
														
														else
														{
															echo '<option value="'.$relationship_id.'">'.$relationship_name.'</option>';
														}
													}
												}
											?>
										</select>
                                      </div>
									  <div class="form-group">
                                        <label>Residence </label>
                                        <input name="patient_town" type="text" class="form-control" placeholder="" autocomplete="off">
                                      </div>


									  <div class="form-group">
										<label>Mode of Payment</label>
										<select class="form-control" name="visit_type_id">
											<?php
												if($visit_types->num_rows() > 0)
												{
													$visit_type = $visit_types->result();
													
													foreach($visit_type as $res)
													{
														$visit_type_id = $res->visit_type_id;
														$visit_type_name = $res->visit_type_name;
														
														if($visit_type_id == set_value("visit_type_id"))
														{
															echo '<option value="'.$visit_type_id.'" selected>'.$visit_type_name.'</option>';
														}
														
														else
														{
															echo '<option value="'.$visit_type_id.'">'.$visit_type_name.'</option>';
														}
													}
												}
											?>
										</select>
                                      </div>

									  <div class="form-group">
										<label>How did you know about us?</label>
										<select class="form-control" name="about_us_view"> 
											<?php
												if($about_us_views->num_rows() > 0)
												{
													$about_us_view = $about_us_views->result();
													
													foreach($about_us_view as $res)
													{
														$place_id = $res->place_id;
														$place_name = $res->place_name;
														
														if($place_id == set_value("place_id"))
														{
															echo '<option value="'.$place_id.'" selected>'.$place_name.'</option>';
														}
														
														else
														{
															echo '<option value="'.$place_id.'">'.$place_name.'</option>';
														}
													}
												}
											?>
										</select>
                                      </div>
                                  </div>
                                
                              </div>
                            </div>
                       
                            <div class="tab-pane" id="account">
                                <h4 class="info-text"> Thank you for sharing with us your information.</h4>
                                <div class="row">

                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="col-sm-12">
                                        	<input type="checkbox" name="jobb" value="Design">
                                            I have accepted to share my informaiton and be treated at Vip Dental
                                        </div>
                                        
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="wizard-footer height-wizard">
                            <div class="pull-right">
                                <input type='button' class='btn btn-next btn-fill btn-warning btn-wd btn-sm' name='next' value='Next' />
                                <input type='submit' class='btn btn-finish btn-fill btn-warning btn-wd btn-sm'  name='finish' value='Finish' />

                            </div>

                            <div class="pull-left">
                                <input type='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Previous' />
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </form>
                </div>
            </div> <!-- wizard container -->
        </div>
        </div><!-- end row -->