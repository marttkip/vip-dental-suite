<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class Site extends MX_Controller 
{
	//paths
	var $posts_path;
	var $brand_models_path;
	var $brands_path;
	var $categories_path;
	var $products_path;
	var $gallery_path;
	
	//locations
	var $posts_location;
	var $brand_models_location;
	var $brands_location;
	var $categories_location;
	var $products_location;
	var $gallery_location;
	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('site_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('reception/reception_model');
		$this->load->model('dental/dental_model');
		//image paths
		$this->posts_path = realpath(APPPATH . '../assets/images/posts');
		$this->brand_models_path = realpath(APPPATH . '../assets/brand_model/images');
		$this->brands_path = realpath(APPPATH . '../assets/brand/images');
		$this->categories_path = realpath(APPPATH . '../assets/categories/images');
		$this->products_path = realpath(APPPATH . '../assets/products/images');
		$this->gallery_path = realpath(APPPATH . '../assets/products/gallery');
		
		//image locations
		$this->posts_location = base_url().'assets/images/posts/';
		$this->brand_models_location = base_url().'assets/brand_model/images/';
		$this->brands_location = base_url().'assets/brand/images/';
		$this->categories_location = base_url().'assets/categories/images/';
		$this->products_location = base_url().'assets/products/images/';
		$this->gallery_location = base_url().'assets/products/gallery/';
		
		// Allow from any origin
	    if (isset($_SERVER['HTTP_ORIGIN'])) {
	        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	        header('Access-Control-Allow-Credentials: true');
	        header('Access-Control-Max-Age: 86400');    // cache for 1 day
	    }
	
	    // Access-Control headers are received during OPTIONS requests
	    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	
	        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
	            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	
	        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
	            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	
	        exit(0);
	    }
	}
    
	/*
	*
	*	Default action is to go to the home page
	*
	*/
	public function index() 
	{
		redirect('home');
	}
    
	/*
	*
	*	Home Page
	*
	*/
	public function cover() 
	{
		$this->load->view('cover');
	}
    
	/*
	*
	*	Home Page
	*
	*/
	public function home_page() 
	{
		//get page data
		$where = 'post.blog_category_id = blog_category.blog_category_id AND post.post_status = 1';
		$table = 'post, blog_category';
		$v_data['posts'] = $this->blog_model->get_all_posts($table, $where, 10, 0);
		$where = 'product.product_id = product_review.product_id AND product_review.product_review_status = 1 AND product.category_id = category.category_id AND product.brand_id = brand.brand_id AND product.brand_model_id = brand_model.brand_model_id';
		$table = 'product_review, product, category, brand, brand_model';
		$v_data['product_reviews'] = $this->products_model->get_all_product_review($table, $where, 3, 0);
		$v_data['latest'] = $this->products_model->get_latest_products();
		$v_data['active_brands'] = $this->brands_model->all_active_brands();
		$v_data['total_products'] = $this->products_model->total_products();
		$v_data['total_categories'] = $this->categories_model->total_categories();
		
		//image paths
		$v_data['products_path'] = $this->products_path;
		$v_data['posts_path'] = $this->posts_path;
		$v_data['brands_path'] = $this->brands_path;
		$v_data['categories_path'] = $this->categories_path;
		
		//image locations
		$v_data['products_location'] = $this->products_location;
		$v_data['posts_location'] = $this->posts_location;
		$v_data['brands_location'] = $this->brands_location;
		$v_data['categories_location'] = $this->categories_location;
		
		//slider page data
		//Brands & models
		$results = $v_data['active_brands'];
		
		$count = 0;
		$models = '';
		$brands = '';
		
		if($results->num_rows() > 0)
		{
			foreach($results->result() as $res)
			{
				$count++;
				
				if($count == 1)
				{
					$this->db->where("brand_model_status = 1 AND brand_id = ".$res->brand_id);
					$this->db->select("brand_model_name, brand_model_id");
					$this->db->order_by("brand_model_name");
					
					$result2 = $this->db->get("brand_model");
					
					if($result2->num_rows() > 0)
					{
						foreach($result2->result() as $res2)
						{
							$models .= "<option value='".$res2->brand_model_id."'>".$res2->brand_model_name."</option>";
						}
					}
				}
				$brands .= "<option value='".$res->brand_id."'>".$res->brand_name."</option>";
			}
		}
		$v_data['brands'] = $brands;
		$v_data['models'] = $models;
		
		//Year from & to
		$year_from = 1980;
		$v_data['year_from'] = "";
		$v_data['year_to'] = "";
		for($r = $year_from; $r <= date("Y"); $r++)
		{
			$v_data['year_from'] .= "<option>".$r."</option>";
		}
		for($r = date("Y"); $r >= $year_from; $r--)
		{
			$v_data['year_to'] .= "<option>".$r."</option>";
		}
		
		//Categpries & sub categories
		$results = $this->categories_model->all_parent_categories();
		$categories = "";
		$children = "";
		$count = 0;
		
		if($results->num_rows() > 0)
		{
			foreach($results->result() as $res)
			{
				$count++;
				
				if($count == 1)
				{
					$this->db->where("category_parent = ".$res->category_id);
					$this->db->order_by("category_name");
					
					$result2 = $this->db->get("category");
					
					if($result2->num_rows() > 0)
					{
						foreach($result2->result() as $res2)
						{
							$children .= "<option value='".$res2->category_id."'>".$res2->category_name."</option>";
						}
					}
				}
				$categories .= "<option value='".$res->category_id."'>".$res->category_name."</option>";
			}
		}
		$v_data['categories'] = $categories;
		$v_data['children'] = $children;
		
		//Location
		$this->db->order_by('location_name');
		$results = $this->db->get('location');
		$locations = "";
		
		if($results->num_rows() > 0)
		{
			foreach($results->result() as $res)
			{
				$locations .= "<option value='".$res->location_id."'>".$res->location_name."</option>";
			}
		}
		$v_data['locations'] = $locations;
		
		//contacts
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$data['title'] = $this->site_model->display_page_title();
		$data['class'] = 'home floated-search';
		$data['contacts'] = $contacts;
		$data['content'] = $this->load->view("home", $v_data, TRUE);
		
		$this->load->view("site/templates/general_page", $data);
	}
    
	/*
	*
	*	Products Page
	*
	*/
	public function products($search = '__', $category = '__', $brand = '__', $brand_model = '__', $order_by = 'product_date', $new_products = 0, $new_categories = 0, $new_brands = 0, $price_range = '__') 
	{
		$table = "product, category, brand_model, brand, location, customer";
		$where = "product.customer_id = customer.customer_id AND location.location_id = product.location_id AND product.category_id = category.category_id AND product.brand_model_id = brand_model.brand_model_id AND brand_model.brand_id = brand.brand_id AND product.product_status = 1";
		$order = "product_date";
		
		//filter by category
		$v_data['filter_category_id'] = '';
		if(($category != '__') && (!empty($category)))
		{
			$category_web = $this->site_model->decode_web_name($category);
			$category_id = $this->categories_model->get_category_id($category_web);
			$parent_category = $this->categories_model->check_parent($category_id);
			$v_data['filter_category_id'] = $parent_category;
			$segment = 5;
			$base_url = base_url().'spareparts/category/'.$category;
			
			$where .= ' AND (category.category_id = '.$category_id.' OR category.category_parent = '.$category_id.' OR category.category_parent IN (SELECT category.category_id FROM category WHERE category.category_parent = '.$category_id.')  OR category.category_parent IN (SELECT category.category_id FROM category WHERE category.category_parent IN (SELECT category.category_id FROM category WHERE category.category_parent = '.$category_id.')))';
		}
		
		//filter brand
		$v_data['filter_brand_id'] = '';
		$v_data['category_filter'] = $category;
		if(($brand != '__') && (!empty($brand)))
		{
			$brand_web = $this->site_model->decode_web_name($brand);
			$brand_id = $this->brands_model->get_brand_id($brand_web);
			$v_data['filter_brand_id'] = $brand_id;
			$segment = 6;
			$base_url = base_url().'spareparts/brand/'.$brand;
			
			$where .= ' AND (brand.brand_id = '.$brand_id.')';
		}
		
		//filter brand_model
		if(($brand_model != '__') && (!empty($brand_model)))
		{
			$brand_model_web = $this->site_model->decode_web_name($brand_model);
			$brand_model_id = $this->brand_models_model->get_brand_model_id($brand_model_web);
			$segment = 6;
			$base_url = base_url().'spareparts/model/'.$brand_model;
			
			$where .= ' AND (brand_model.brand_model_id = '.$brand_model_id.')';
		}
		
		$limit = NULL;
		
		//ordering products
		switch ($order_by)
		{
			case 'product_date':
				$order_method = 'DESC';
			break;
			
			case 'price':
				$order_by = 'product_selling_price';
				$order_method = 'ASC';
			break;
			
			case 'price_desc':
				$order_by = 'product_selling_price';
				$order_method = 'DESC';
			break;
		}
		
		//case of search
		if($search != '__')
		{
			$where .= " AND (product.product_name LIKE '%".$search."%' OR category.category_name LIKE '%".$search."%' OR brand.brand_name LIKE '%".$search."%')";
		}
		
		//case of latest products
		if($new_products == 1)
		{
			$limit = 30;
		}
		
		//case of latest category
		if($new_categories == 1)
		{
			$query = $this->categories_model->latest_category();
			
			if($query->num_rows() > 0)
			{
				$category = $query->row();
				$latest_category_id = $category->category_id;
				
				$where .= ' AND category.category_id = '.$latest_category_id;
			}
		}
		
		//case of latest brand
		if($new_brands == 1)
		{
			$query = $this->brands_model->latest_brand();
			
			if($query->num_rows() > 0)
			{
				$brand = $query->row();
				$latest_brand_id = $brand->brand_id;
				
				$where .= ' AND brand.brand_id = '.$latest_brand_id;
			}
		}
		
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'spareparts';
		$config['total_rows'] = $this->users_model->count_items($table, $where, $limit);
		$config['uri_segment'] = 5;
		$config['per_page'] = 21;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination no-margin-top">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = '»';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = '«';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		
		if($limit == NULL)
		{
        	$v_data["links"] = $this->pagination->create_links();
			$v_data["first"] = $page + 1;
			$v_data["total"] = $config['total_rows'];
			
			if($v_data["total"] < $config["per_page"])
			{
				$v_data["last"] = $page + $v_data["total"];
			}
			
			else
			{
				$v_data["last"] = $page + $config["per_page"];
			}
		}
		
		else
		{
			$v_data["first"] = $page + 1;
			$v_data["total"] = $config['total_rows'];
			$v_data["last"] = $config['total_rows'];
		}
		$v_data["title"] = $this->site_model->display_page_title();
		
		//Retrieve product images
		$v_data['product_images'] = $this->products_model->get_product_images();
		
		$table2 = "product, category, brand_model, brand, location, customer";
		$where2 = "product.customer_id = customer.customer_id AND location.location_id = product.location_id AND product.category_id = category.category_id AND product.brand_model_id = brand_model.brand_model_id AND brand_model.brand_id = brand.brand_id AND product.product_status = 1";
		
		//search page data
		//Brands & models
		$results = $this->brands_model->all_active_brands();
		
		$count = 0;
		$models = '';
		$brands = '';
		
		if($results->num_rows() > 0)
		{
			foreach($results->result() as $res)
			{
				$count++;
				$brand_web_name = $this->site_model->create_web_name($res->brand_name);
				
				if($count == 1)
				{
					$this->db->where("brand_model_status = 1 AND brand_id = ".$res->brand_id);
					$this->db->select("brand_model_name, brand_model_id");
					$this->db->order_by("brand_model_name");
					
					$result2 = $this->db->get("brand_model");
					
					if($result2->num_rows() > 0)
					{
						foreach($result2->result() as $res2)
						{
							$total_models = $this->users_model->count_items($table2, $where2.' AND product.brand_model_id = '.$res2->brand_model_id);
							$model_web_name = $this->site_model->create_web_name($res2->brand_model_name);
							$models .= '<li class="list-group-item"><span class="badge">'.$total_models.'</span><a href="'.site_url().'spareparts/model/'.$category.'/'.$brand_web_name.'/'.$model_web_name.'">'.$res2->brand_model_name.'</a></li>';
						}
					}
				}
				
				$total_brands = $this->users_model->count_items($table, $where.' AND product.brand_id = '.$res->brand_id);
				$brands .= '<li class="list-group-item"><span class="badge">'.$total_brands.'</span><a href="'.site_url().'spareparts/brand/'.$category.'/'.$brand_web_name.'">'.$res->brand_name.'</a></li>';
			}
		}
		$v_data['brands'] = $brands;
		$v_data['models'] = $models;
		
		//Year from & to
		$year_from = 1980;
		$v_data['year_from'] = "";
		$v_data['year_to'] = "";
		for($r = $year_from; $r <= date("Y"); $r++)
		{
			$v_data['year_from'] .= "<option>".$r."</option>";
		}
		for($r = date("Y"); $r >= $year_from; $r--)
		{
			$v_data['year_to'] .= "<option>".$r."</option>";
		}
		
		//Categpries & sub categories
		$results = $this->categories_model->all_parent_categories();
		$categories = "";
		$children = "";
		$count = 0;
		
		if($results->num_rows() > 0)
		{
			foreach($results->result() as $res)
			{
				$count++;
				
				if($count == 1)
				{
					$children = $this->categories_model->limit_sub_categories($res->category_id);
				}
				
				$category_web_name = $this->site_model->create_web_name($res->category_name);
				$total_categories = $this->users_model->count_items($table2, $where2.' AND (category.category_id = '.$res->category_id.' OR category.category_parent = '.$res->category_id.' OR category.category_parent IN (SELECT category.category_id FROM category WHERE category.category_parent = '.$res->category_id.')  OR category.category_parent IN (SELECT category.category_id FROM category WHERE category.category_parent IN (SELECT category.category_id FROM category WHERE category.category_parent = '.$res->category_id.')))');
				$categories .= '<li class="list-group-item"><span class="badge">'.$total_categories.'</span><a href="'.site_url().'spareparts/category/'.$category_web_name.'" onClick="limit_sub_categories('.$res->category_id.')" title="View all parts under'.$res->category_name.'">'.$res->category_name.'</a></li>';
			}
		}
		$v_data['categories'] = $categories;
		$v_data['children'] = $children;
		
		//Location
		$this->db->order_by('location_name');
		$results = $this->db->get('location');
		$locations = "";
		
		if($results->num_rows() > 0)
		{
			foreach($results->result() as $res)
			{
				
				$total_locations = $this->users_model->count_items($table, $where.' AND product.location_id = '.$res->location_id);
				$locations .= '<li class="list-group-item"><span class="badge">'.$total_categories.'</span><a href="'.$res->location_id.'">'.$res->location_name.'</a></li>';
			}
		}
		$v_data['locations'] = $locations;
		
		//products path
		$v_data['products_path'] = $this->products_path;
		$v_data['products_location'] = $this->products_location;
		
		$v_data['products'] = $this->products_model->get_all_products($table, $where, $config["per_page"], $page, $limit, $order_by, $order_method);
		
		$data['content'] = $this->load->view('products/products', $v_data, true);
		
		$data['title'] = $this->site_model->display_page_title();
		$this->load->view('templates/general_page', $data);
	}
    
	/*
	*
	*	Search for a product
	*
	*/
	public function search()
	{
		$search = $this->input->post('search_item');
		
		if(!empty($search))
		{
			redirect('products/search/'.$search);
		}
		
		else
		{
			redirect('products/all-products');
		}
	}
    
	/*
	*
	*	Products Page
	*
	*/
	public function view_product($product_id)
	{
		$data['logo'] = base_url().'assets/logo.png';
		$this->products_model->update_clicks($product_id);
		//Required general page data
		$v_data['crumbs'] = $this->site_model->get_crumbs();
		
		//get page data
		$v_data['all_features'] = $this->features_model->all_features();
		$v_data['similar_products'] = $this->products_model->get_similar_products($product_id);
		$v_data['product_details'] = $this->products_model->get_product($product_id);
		$v_data['product_images'] = $this->products_model->get_gallery_images($product_id);
		$v_data['product_features'] = $this->products_model->get_features($product_id);
		$data['content'] = $this->load->view('products/view_product', $v_data, true);
		
		$data['title'] = $this->site_model->display_page_title();
		$this->load->view('templates/general_page', $data);
	}
    
	/*
	*
	*	About
	*
	*/
	public function about()
	{
		$data['content'] = $this->load->view('about', '', true);
		
		$data['title'] = $this->site_model->display_page_title();
		$this->load->view('templates/general_page', $data);
	}
    
	/*
	*
	*	Contact
	*
	*/
	public function contact()
	{
		$data['content'] = $this->load->view('contact', '', true);
		
		$data['title'] = $this->site_model->display_page_title();
		$this->load->view('templates/general_page', $data);
	}
	
	public function get_sub_categories($category_id)
	{
		$return['children'] = $this->categories_model->limit_sub_categories($category_id);
		
		echo json_encode($return);
	}
	
	public function get_brand_models($brand_id, $category)
	{
		$table2 = "product, category, brand_model, brand, location, customer";
		$where2 = "product.customer_id = customer.customer_id AND location.location_id = product.location_id AND product.category_id = category.category_id AND product.brand_model_id = brand_model.brand_model_id AND brand_model.brand_id = brand.brand_id AND product.product_status = 1";
		
		//Models
		$results = $this->brands_model->get_brand($brand_id);
		
		$models = '';
		
		if($results->num_rows() > 0)
		{
			foreach($results->result() as $res)
			{
				$brand_web_name = $this->site_model->create_web_name($res->brand_name);
				
				$this->db->where("brand_model_status = 1 AND brand_id = ".$res->brand_id);
				$this->db->select("brand_model_name, brand_model_id");
				$this->db->order_by("brand_model_name");
				
				$result2 = $this->db->get("brand_model");
				
				if($result2->num_rows() > 0)
				{
					foreach($result2->result() as $res2)
					{
						$total_models = $this->users_model->count_items($table2, $where2.' AND product.brand_model_id = '.$res2->brand_model_id);
						$model_web_name = $this->site_model->create_web_name($res2->brand_model_name);
						$models .= '<li class="list-group-item"><span class="badge">'.$total_models.'</span><a href="'.site_url().'spareparts/model/'.$category.'/'.$brand_web_name.'/'.$model_web_name.'">'.$res2->brand_model_name.'</a></li>';
					}
				}
			}
		}
		
		$v_data['models'] = $models;
		
		echo json_encode($v_data);
	}
	
	public function speech()
	{
		$this->load->view('speech');
	}
	
	public function search_box()
	{
		$data['title'] = 'General Queue';
		$v_data['title'] = 'General Queue';
		
		$data['content'] = $this->load->view('search', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function change_branch($branch_id)
	{
		$this->db->where('branch_id',$branch_id);
		$query = $this->db->get('branch');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$branch_id = $value->branch_id;
				$branch_code = $value->branch_code;
			}
			$this->session->set_userdata('branch_id',$branch_id);
			$this->session->set_userdata('branch_code',$branch_code);
		}
		$response['message'] = 'success';
		echo json_encode($response);
	}

	public function register()
	{
		// var_dump('jhbcwfjh'); die();
		$v_data['relationships'] = $this->reception_model->get_relationship();
		$v_data['visit_types'] = $this->reception_model->get_visit_types(); //gets all visit types from reception/models/reception_model.php, stores the vsit types inside "visit_type_name" and sends data retrieved to site/site.php
		$v_data['religions'] = $this->reception_model->get_religion();
		$v_data['about_us_views'] = $this->reception_model->get_about_us_view();
		$v_data['civil_statuses'] = $this->reception_model->get_civil_status();
		$v_data['titles'] = $this->reception_model->get_title();
		$v_data['genders'] = $this->reception_model->get_gender();
		$v_data['insurance'] = $this->reception_model->get_insurance();
		$v_data['dependant_staff'] = $dependant_staff;
		$v_data['branches'] = $this->reception_model->get_branches();
		// $data['content'] = $this->load->view('add_patient', $v_data, true);

		$data['title'] = 'General Queue';
		$v_data['title'] = 'General Queue';
		
		$data['content'] = $this->load->view('registration', $v_data, true);

		
		
		$this->load->view('site/templates/registration_page', $data);

	}

	public function register_mobile()
	{
		// var_dump('jhbcwfjh'); die();
		$v_data['relationships'] = $this->reception_model->get_relationship();
		$v_data['visit_types'] = $this->reception_model->get_visit_types(); //gets all visit types from reception/models/reception_model.php, stores the vsit types inside "visit_type_name" and sends data retrieved to site/site.php
		$v_data['religions'] = $this->reception_model->get_religion();
		$v_data['about_us_views'] = $this->reception_model->get_about_us_view();
		$v_data['civil_statuses'] = $this->reception_model->get_civil_status();
		$v_data['titles'] = $this->reception_model->get_title();
		$v_data['genders'] = $this->reception_model->get_gender();
		$v_data['insurance'] = $this->reception_model->get_insurance();
		$v_data['dependant_staff'] = $dependant_staff;
		$v_data['branches'] = $this->reception_model->get_branches();
		// $data['content'] = $this->load->view('add_patient', $v_data, true);

		$data['title'] = 'General Queue';
		$v_data['title'] = 'General Queue';
		
		$data['content'] = $this->load->view('mobile_registration', $v_data, true);

		
		
		$this->load->view('site/templates/mobile_registration_page', $data);

	}

	public function register_new_patient()
	{

	
		//form validation rules
		// $this->form_validation->set_rules('title_id', 'Title', 'is_numeric|xss_clean');
		$this->form_validation->set_rules('patient_surname', 'Name', 'required|xss_clean');
		$this->form_validation->set_rules('patient_dob', 'Date of Birth', 'trim|xss_clean');
		$this->form_validation->set_rules('gender_id', 'Gender', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_number', 'Other Phone', 'xss_clean');
		
		
		//if form conatins invalid data
		if ($this->form_validation->run() == FALSE)
		{
			$response['message'] = 'fail';
			$response['result'] ='Ensure that you have entered all fields correctly';
		}
		
		else
		{
			// var_dump($_POST); die();
			$patient_id = $this->site_model->save_other_patient();
			$response['message'] = 'success';
			$response['result'] ='You have updated';
		}


		echo json_encode($response);
	}
	public function register_patient_details()
	{
		// $url = 'http://localhost/vip-dental-suite/site/register_new_account';
		$url = 'https://simplex-financials.com/vip-dental-suite/site/register_new_account';
		// $url = 'https://vip.maat-ea.com/site/register_new_account';
	    //Encode the array into JSON.
	    $patient_details['patient_surname'] = ucwords(strtolower($this->input->post('patient_surname')));
	    $patient_details['patient_phone1'] = $this->input->post('patient_phone1');
	    $patient_details['patient_email'] = $this->input->post('patient_email');
	    
	    $patient_details['patient_dob'] = $this->input->post('patient_dob');
	     $patient_details['age'] = $this->input->post('age');
	    $patient_details['gender_id'] = $this->input->post('gender_id');
	    $patient_details['patient_occupation'] = $this->input->post('patient_occupation');
	    $patient_details['about_us'] = $this->input->post('about_us');
	    $patient_details['about_us_view'] = $this->input->post('about_us_view');
	    $patient_details['patient_town'] = $this->input->post('patient_town');
	    $patient_details['next_of_kin_contact'] = $this->input->post('next_of_kin_contact');
	    $patient_details['insurance_company_id'] = $this->input->post('insurance_company_id');
	    $patient_details['patient_phone2'] = $this->input->post('patient_phone2');
	    $patient_details['branch_id'] = 2;
	    $patient_details['about_us_view'] = $this->input->post('about_us_view');
	    $patient_details['next_of_kin_contact'] = $this->input->post('next_of_kin_contact');
	    $patient_details['patient_kin_sname'] = $this->input->post('patient_kin_sname');

	    //The JSON data.
	    // var_dump($url);die();
	    $data_string = json_encode($patient_details);

	    try{                                                                                                         

	        $ch = curl_init($url);
	        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	            'Content-Type: application/json',
	            'Content-Length: ' . strlen($data_string))
	        );
	        $result = curl_exec($ch);
	        $response['message'] = 'success';
			$response['result'] ='You have updated';

	        curl_close($ch);
	    }
	    catch(Exception $e)
	    {
	        $response = "something went wrong";
	        echo json_encode($response.' '.$e);
	    }


		echo json_encode($response);
	}

	public function register_new_account()
	{
		$json = file_get_contents('php://input');
		$decoded = json_decode($json,true);
		// var_dump($decoded['patient_surname']);die();
		$patient_details['patient_surname'] = $decoded['patient_surname'];
	    $patient_details['patient_phone1'] = $decoded['patient_phone1'];
	    $patient_details['patient_email'] = $decoded['patient_email'];
	    $age = $decoded['age'];

	    if(!empty($age) AND $age > 0){
	    	$year = date('Y') - $age;
	    	$patient_date_of_birth = $year.'-01-01';
	    }
	    else
	    {
	    	$patient_date_of_birth = NULL;
	    }
	    $patient_details['patient_date_of_birth'] = $patient_date_of_birth;
	    $patient_details['gender_id'] = $decoded['gender_id'];
	    $patient_details['patient_occupation'] = $decoded['patient_occupation'];
	    $patient_details['about_us'] = $decoded['about_us'];
	    $patient_details['about_us_view'] = $decoded['about_us_view'];
	    $patient_details['patient_town'] = $decoded['patient_town'];
	    $patient_details['insurance_company_id'] = $decoded['insurance_company_id'];
	    $patient_details['patient_phone2'] = $decoded['patient_phone2'];
	    $patient_details['branch_id'] = 2;
	    $patient_details['patient_kin_phonenumber1'] = $decoded['next_of_kin_contact'];
	    $patient_details['patient_kin_sname'] = $decoded['patient_kin_sname'];
	    $patient_details['category_id'] = 3;
	    $patient_details['about_us'] = 54;
	    $patient_details['patient_age'] = $age;
	    $patient_details['patient_date'] = date('Y-m-d H:i:s');
	     // var_dump($patient_details);die();

	    if($this->db->insert('patients_list', $patient_details))
		{
			$patient_id = $this->db->insert_id();

			// update all patients details with the information
			// $visit_array['visit_id'] = 0;
			// $visit_array['patient_id'] = $patient_id; 
			// $this->db->where('session_id = '.$session_id.'');
			// $this->db->update('patient_history_result',$visit_array);


			return $patient_id;
		}
		else{
			return FALSE;
		}
		




	}
	function update_patient_history($patient_history_id,$session_id,$checked)
	{
		$this->db->where('patient_history_id = '.$patient_history_id.' AND session_id = '.$session_id.'');
		$query = $this->db->get('patient_history_result');

		if($query->num_rows() > 0)
		{
			// update
			$visit_array['patient_history_id'] = $patient_history_id;
			$visit_array['session_id'] = $session_id; 
			$visit_array['visit_id'] = 0;
			$visit_array['patient_id'] = 0; 
			$visit_array['patient_history_status'] = $checked;

			$this->db->where('patient_history_id = '.$patient_history_id.' AND session_id = '.$session_id.'');
			$this->db->update('patient_history_result',$visit_array);
		}
		else
		{
			$visit_array['patient_history_id'] = $patient_history_id;
			$visit_array['session_id'] = $session_id; 
			$visit_array['visit_id'] = 0; 
			$visit_array['patient_id'] = 0; 
			$visit_array['patient_history_status'] = $checked;

			$this->db->insert('patient_history_result',$visit_array);
		}

		$response['status'] = 'success';
		$response['message'] ='You have updated';
		echo json_encode($response);
	}
	public function thank_you()
	{

		$data['title'] = 'General Queue';
		$v_data['title'] = 'General Queue';
		
		$data['content'] = $this->load->view('thank_you', $v_data, true);
		
		$this->load->view('site/templates/registration_page', $data);

	}

}
?>