<?php
//personnel data



$result = '';
if($query->num_rows() > 0)
{
	$count = 0;
		
	$result .= 
	'
	<br/>
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Education</th>
				<th>Proficiency</th>
				<th>Institution</th>
				<th>Year</th>
				<th>Status</th>
				<th colspan="5">Actions</th>
			</tr>
		</thead>
		  <tbody>
		  
	';
	
	foreach ($query->result() as $row)
	{
		$personnel_education_id = $row->personnel_education_id;
		$education_id = $row->education_id;
		$education_name = $row->education_name;

		$proficiency = $row->proficiency;
		$institution = $row->institution;
		$year = $row->year;

		$personnel_education_status = $row->personnel_education_status;
		$created = date('jS M Y H:i a',strtotime($row->created));
		$last_modified = date('jS M Y H:i a',strtotime($row->last_modified));
		
		//create deactivated status display
		if($personnel_education_status == 0)
		{
			$status = '<span class="label label-danger">Deactivated</span>';
			$button = '<a class="btn btn-sm btn-info" href="'.site_url().'hr/activate-education-contact/'.$personnel_education_id.'/'.$personnel_id.'" onclick="return confirm(\'Do you want to activate '.$institution.'?\');" title="Activate '.$institution.'"><i class="fa fa-thumbs-up"></i></a>';
		}
		//create activated status display
		else if($personnel_education_status == 1)
		{
			$status = '<span class="label label-success">Active</span>';
			$button = '<a class="btn btn-sm btn-default" href="'.site_url().'hr/deactivate-education-contact/'.$personnel_education_id.'/'.$personnel_id.'" onclick="return confirm(\'Do you want to deactivate '.$institution.'?\');" title="Deactivate '.$institution.'"><i class="fa fa-thumbs-down"></i></a>';
		}
		
		$count++;
		$result .= 
		'
			<tr>
				<td>'.$count.'</td>
				<td>'.$education_name.'</td>
				<td>'.$proficiency.'</td>
				<td>'.$institution.'</td>
				<td>'.$year.'</td>
				<td>'.$status.'</td>
				<td><a  class="btn btn-sm btn-danger" onclick="delete_education_item('.$personnel_education_id.','.$personnel_id.');" title="Delete '.$institution.'"><i class="fa fa-trash"></i></a></td>
			</tr> 
		';
	}
	
	$result .= 
	'
				  </tbody>
				</table>
	';
}

else
{
	$result = "<p>No contacts have been added</p>";
}
	

	echo $result;
?>