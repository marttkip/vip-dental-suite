<?php
//personnel data



$result = '';
if($query->num_rows() > 0)
{
	$count = 0;
		
	$result .= 
	'
	<br/>
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Employer</a></th>
				<th>Employer Address</a></th>
				<th>Job Title</a></th>
				<th>Start - End Date</a></th>
				<th>Reason for leaving</a></th>
				<th>Status</a></th>
				<th colspan="5">Actions</th>
			</tr>
		</thead>
		  <tbody>
		  
	';
	
	foreach ($query->result() as $row)
	{
		$personnel_employment_id = $row->personnel_employment_id;
		$job_title_id = $row->job_title_id;
		$employer_name = $row->employer_name;
		$employer_address = $row->employer_address;
		$start_date = $row->start_date;
		$end_date = $row->end_date;
		$job_title_name = $row->job_title_name;
		$reason = $row->reason;
		$personnel_employment_status = $row->personnel_employment_status;
		$created = date('jS M Y H:i a',strtotime($row->created));
		$last_modified = date('jS M Y H:i a',strtotime($row->last_modified));
		
		//create deactivated status display
		if($personnel_employment_status == 0)
		{
			$status = '<span class="label label-danger">Deactivated</span>';
			$button = '<a class="btn btn-sm btn-info" href="'.site_url().'hr/activate-emergency-contact/'.$personnel_employment_id.'/'.$personnel_id.'" onclick="return confirm(\'Do you want to activate '.$employer_name.'?\');" title="Activate '.$employer_name.'"><i class="fa fa-thumbs-up"></i></a>';
		}
		//create activated status display
		else if($personnel_employment_status == 1)
		{
			$status = '<span class="label label-success">Active</span>';
			$button = '<a class="btn btn-sm btn-default" href="'.site_url().'hr/deactivate-emergency-contact/'.$personnel_employment_id.'/'.$personnel_id.'" onclick="return confirm(\'Do you want to deactivate '.$employer_name.'?\');" title="Deactivate '.$employer_name.'"><i class="fa fa-thumbs-down"></i></a>';
		}
		
		$count++;
		$result .= 
		'
			<tr>
				<td>'.$count.'</td>
				<td>'.$employer_name.'</td>
				<td>'.$employer_address.'</td>
				<td>'.$job_title_name.'</td>
				<td>'.$start_date.' - '.$end_date.'</td>
				<td>'.$reason.'</td>
				<td>'.$status.'</td>
				<td>'.$button.'</td>
				<td><a  class="btn btn-sm btn-danger" onclick="delete_employment_item('.$personnel_employment_id.','.$personnel_id.');" title="Delete '.$employer_name.'"><i class="fa fa-trash"></i></a></td>
			</tr> 
		';
	}
	
	$result .= 
	'
				  </tbody>
				</table>
	';
}

else
{
	$result = "<p>No contacts have been added</p>";
}
	

	echo $result;
?>