<?php
//personnel data
$row = $personnel->row();

// var_dump($row) or die();
$personnel_onames = $row->personnel_onames;
$personnel_fname = $row->personnel_fname;
$personnel_dob = $row->personnel_dob;
$personnel_email = $row->personnel_email;
$personnel_phone = $row->personnel_phone;
$personnel_address = $row->personnel_address;
$civil_status_id = $row->civilstatus_id;
$personnel_locality = $row->personnel_locality;
$title_id = $row->title_id;
$gender_id = $row->gender_id;
$personnel_username = $row->personnel_username;
$personnel_kin_fname = $row->personnel_kin_fname;
$personnel_kin_onames = $row->personnel_kin_onames;
$personnel_kin_contact = $row->personnel_kin_contact;
$personnel_kin_address = $row->personnel_kin_address;
$kin_relationship_id = $row->kin_relationship_id;
$job_title_idd = $row->job_title_id;
$staff_id = $row->personnel_staff_id;
$bank_branch_id = $row->bank_branch_id;
$bank_id = $row->bank_id;



//echo $gender_id;
//repopulate data if validation errors occur
$validation_error = validation_errors();
				
if(!empty($validation_error))
{
	$personnel_onames =set_value('personnel_onames');
	$personnel_fname =set_value('personnel_fname');
	$personnel_dob =set_value('personnel_dob');
	$personnel_email =set_value('personnel_email');
	$personnel_phone =set_value('personnel_phone');
	$personnel_address =set_value('personnel_address');
	$civil_status_id =set_value('civil_status_id');
	$personnel_locality =set_value('personnel_locality');
	$title_id =set_value('title_id');
	$gender_id =set_value('gender_id');
	$personnel_username =set_value('personnel_username');
	$personnel_kin_fname =set_value('personnel_kin_fname');
	$personnel_kin_onames =set_value('personnel_kin_onames');
	$personnel_kin_contact =set_value('personnel_kin_contact');
	$personnel_kin_address =set_value('personnel_kin_address');
	$kin_relationship_id =set_value('kin_relationship_id');
	$job_title_id =set_value('job_title_id');
	$staff_id =set_value('staff_id');
	$bank_id2 = set_value('bank_id');
	$bank_branch_id2 = set_value('bank_branch_id');
	
}
?>
        
          <section class="panel">

                <header class="panel-heading">
                	<div class="row">
	                	<div class="col-md-6">
		                    <h2 class="panel-title"><?php echo $title;?> <?php echo $personnel_fname.' '.$personnel_onames;?> Details</h2>
		                    <i class="fa fa-phone"/></i>
		                    <span id="mobile_phone"><?php echo $personnel_phone;?></span>
		                    <i class="fa fa-envelope"/></i>
		                    <span id="work_email"><?php echo $personnel_email;?></span>
		                </div>
		                <div class="col-md-6">
		                		<a href="<?php echo site_url();?>hr/personnel" class="btn btn-sm btn-info pull-right">Back to personnel</a>
		                </div>
	                </div>
                </header>
                <div class="panel-body">
                    
                    <div class="row">
                    	<div class="col-md-12">
                        	<?php
                            	$success = $this->session->userdata('success_message');
                            	$error = $this->session->userdata('error_message');
								
								if(!empty($success))
								{
									echo '
										<div class="alert alert-success">'.$success.'</div>
									';
									
									$this->session->unset_userdata('success_message');
								}
								
								if(!empty($error))
								{
									echo '
										<div class="alert alert-danger">'.$error.'</div>
									';
									
									$this->session->unset_userdata('error_message');
								}
								
							?>
                        	<div class="tabs">
								<ul class="nav nav-tabs nav-justified">
									<li class="active">
										<a class="text-center" data-toggle="tab" href="#general"><i class="fa fa-user"></i> PERSONAL INFORMATION</a>
									</li>
									<li>
										<a class="text-center" data-toggle="tab" href="#education">EDUCATION BACKGROUND</a>
									</li>
									<li>
										<a class="text-center" data-toggle="tab" href="#employment">EMPLOYMENT HISTORY </a>
									</li>
								
									<li>
										<a class="text-center" data-toggle="tab" href="#uploads">MEDICAL INFORMATION</a>
									</li>
								
									
									<li>
										<a class="text-center" data-toggle="tab" href="#emergency">EMERGENCY CONTACTS</a>
									</li>
									<li>
										<a class="text-center" data-toggle="tab" href="#dependants">DEPENDANTS</a>
									</li>
									<li>
										<a class="text-center" data-toggle="tab" href="#account"><i class="fa fa-lock"></i> ACCOUNT DETAILS</a>
									</li>
									<li>
										<a class="text-center" data-toggle="tab" href="#job">JOB / ROLE</a>
									</li>
									<li>
										<a class="text-center" data-toggle="tab" href="#leave"><i class="fa fa-calendar-check-o"></i> LEAVE</a>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="general">
										<?php echo $this->load->view('edit/about', '', TRUE);?>
									</div>
									<div class="tab-pane" id="education">
										<?php echo $this->load->view('edit/education', '', TRUE);?>
									</div>
									<div class="tab-pane" id="employment">
										<?php echo $this->load->view('edit/employment', '', TRUE);?>
									</div>
									<div class="tab-pane" id="uploads">
										<?php echo $this->load->view('edit/uploads', '', TRUE);?>
									</div>
									<div class="tab-pane" id="account">
										<?php echo $this->load->view('edit/account', '', TRUE);?>
									</div>
									<div class="tab-pane" id="emergency">
										<?php echo $this->load->view('edit/emergency', '', TRUE);?>
									</div>
									<div class="tab-pane" id="dependants">
										<?php echo $this->load->view('edit/dependants', '', TRUE);?>
									</div>
									<div class="tab-pane" id="job">
										<?php echo $this->load->view('edit/jobs', '', TRUE);?>
									</div>
									<div class="tab-pane" id="leave">
										<?php echo $this->load->view('edit/leave', '', TRUE);?>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </section>
        

<script type="text/javascript">
        	


$(function() {
	// var patient_id = <?php echo $patient_id?>;
	var personnel_id = <?php echo $personnel_id?>;

	// alert(personnel_id);

	get_education_list(personnel_id);
	get_employment_list(personnel_id);


	
});

$(document).on("submit","form#add-education-contact",function(e)
{
	e.preventDefault();
	
	var form_data = new FormData(this);

	// alert(form_data);
	var personnel_id = $('#personnel_id').val();
	var config_url = $('#config_url').val();	

	 var url = config_url+"hr/personnel/add_education_contact/"+personnel_id;
	
       $.ajax({
       type:'POST',
       url: url,
       data:form_data,
       dataType: 'text',
       processData: false,
       contentType: false,
       success:function(data){
          var data = jQuery.parseJSON(data);        
			if(data.message == "success")
			{
			
				get_education_list(personnel_id);

		
	 
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}
       
       },
       error: function(xhr, status, error) {
       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       
       }
       });
	 
	
   
	
});

function get_education_list(personnel_id)
{
	var config_url = $('#config_url').val();	

	var url = config_url+"hr/personnel/get_education_list/"+personnel_id;

	$.ajax({
	type:'POST',
	url: url,
	data:{appointment_id: 1},
	dataType: 'text',
	success:function(data){
	  var data = jQuery.parseJSON(data);    
	  var page_item = data;

	  // alert(page_item);
	  $('#education-history').html(page_item);

	},
	error: function(xhr, status, error) {
	alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	}
	});
}



$(document).on("submit","form#add-employment-contact",function(e)
{
	e.preventDefault();
	
	var form_data = new FormData(this);

	// alert(form_data);
	var personnel_id = $('#personnel_id').val();
	var config_url = $('#config_url').val();	

	 var url = config_url+"hr/personnel/add_employment_contact/"+personnel_id;
	
       $.ajax({
       type:'POST',
       url: url,
       data:form_data,
       dataType: 'text',
       processData: false,
       contentType: false,
       success:function(data){
          var data = jQuery.parseJSON(data);        
			if(data.message == "success")
			{
			
				get_employment_list(personnel_id);

		
	 
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}
       
       },
       error: function(xhr, status, error) {
       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       
       }
       });
	 
	
   
	
});

function get_employment_list(personnel_id)
{
	var config_url = $('#config_url').val();	

	var url = config_url+"hr/personnel/get_employment_list/"+personnel_id;

	$.ajax({
	type:'POST',
	url: url,
	data:{appointment_id: 1},
	dataType: 'text',
	success:function(data){
	  var data = jQuery.parseJSON(data);    
	  var page_item = data;

	  // alert(page_item);
	  $('#employment-history').html(page_item);

	},
	error: function(xhr, status, error) {
	alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	}
	});
}



function delete_employment_item(personnel_employment_id,personnel_id)
{


	var res =  confirm('Are you sure you want to delete this entry ?');


	if (res)
	{
		var config_url = $('#config_url').val();	

		var url = config_url+"hr/personnel/delete_employment_item/"+personnel_employment_id+"/"+personnel_id;

		$.ajax({
		type:'POST',
		url: url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
		  	var data = jQuery.parseJSON(data);        
			if(data.message == "success")
			{
			
				get_employment_list(personnel_id);

		
	 
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
	}
	
}



function delete_education_item(personnel_education_id,personnel_id)
{
	var res =  confirm('Are you sure you want to delete this entry ?');


	if (res)
	{
		var config_url = $('#config_url').val();	

		var url = config_url+"delete-education-item/"+personnel_education_id+"/"+personnel_id;

		$.ajax({
		type:'POST',
		url: url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
		  	var data = jQuery.parseJSON(data);        
			if(data.message == "success")
			{
			
				get_education_list(personnel_id);

		
	 
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
	}
}

</script>