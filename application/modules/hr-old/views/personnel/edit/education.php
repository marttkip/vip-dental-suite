<?php
//personnel data

//repopulate data if validation errors occur
$validation_error = validation_errors();
				
if(!empty($validation_error))
{
	$education_id = set_value('education_id');
	$proficiency = set_value('proficiency');
	$institution = set_value('institution');
	$year = set_value('year');

}

else
{
	$education_id = '';
	$proficiency = '';
	$institution = '';
	$year = '';
}
?>

    <?php
    if(isset($error)){
        echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
    }
    if(!empty($validation_errors))
    {
        echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
    }
	
    ?>
            
    <?php echo form_open('hr/add-education-contact/'.$personnel_id, array("class" => "form-horizontal", "role" => "form" , "id"=>"add-education-contact"));?>
	<div class="row" style="margin-top:20px;">
		<div class="col-md-3">
			
			<input type="hidden" name="personnel_id" id="personnel_id" value="<?php echo $personnel_id?>">
	        
	        <div class="form-group">
	            <label class="col-lg-5 control-label">Education Level: </label>
	            
	            <div class="col-lg-7">
	                <select class="form-control" name="education_id">
	                	<?php
	                    	if($education->num_rows() > 0)
							{
								$title = $education->result();
								
								foreach($title as $res)
								{
									$db_education_id = $res->education_id;
									$education_name = $res->education_name;
									
									if($db_education_id == $education_id)
									{
										echo '<option value="'.$db_education_id.'" selected>'.$education_name.'</option>';
									}
									
									else
									{
										echo '<option value="'.$db_education_id.'">'.$education_name.'</option>';
									}
								}
							}
						?>
	                </select>
	            </div>
	        </div>

	        
	        
	         
	    </div>
		<div class="col-md-3">
			<div class="form-group">
	            <label class="col-lg-5 control-label">Proficiency/Qualification: </label>
	            
	            <div class="col-lg-7">
	            	<input type="text" class="form-control" name="proficiency" placeholder="Level / Qualification" value="<?php echo $proficiency;?>">
	            </div>
	        </div>
	    </div>
	    <div class="col-md-3">
	        <div class="form-group">
	            <label class="col-lg-5 control-label">Institution: </label>
	            
	            <div class="col-lg-7">
	            	<input type="text" class="form-control" name="institution" placeholder="Institution" value="<?php echo $institution;?>">
	            </div>
	        </div>
	    </div>
	    <div class="col-md-3">
	        <div class="form-group">
	            <label class="col-lg-5 control-label">Year Qualified: </label>
	            
	            <div class="col-lg-7">
	            	<input type="text" class="form-control" name="year" placeholder="Year Qualified" value="<?php echo $year;?>">
	            </div>
	        </div>
	    </div>

	</div>
	<div class="row" style="margin-top:10px;">
		<div class="col-md-12">
	        <div class="form-actions center-align">
	        	
	            <button class="btn btn-primary" type="submit" onclick="return confirm('Are you sure you want to added education history ? ')">
	                Add Education History
	            </button>
	        </div>
	    </div>
	</div>
    <?php echo form_close();?>
  	<div id="education-history"></div>
