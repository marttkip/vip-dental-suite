<?php


//repopulate data if validation errors occur
$validation_error = validation_errors();
				
if(!empty($validation_error))
{
	$employer_name = set_value('employer_name');
	$employer_address = set_value('employer_address');
	$job_title_id = set_value('job_title_id');
	$start_date = set_value('start_date');

	$end_date = set_value('end_date');
	$reason = set_value('reason');
}

else
{
	$employer_name = '';
	$employer_address = '';
	$job_title_id = '';
	$start_date = '';
	$title_id = '';
	$end_date = '';
	$reason = '';
}
?>

    <?php
    if(isset($error)){
        echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
    }
    if(!empty($validation_errors))
    {
        echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
    }
	
    ?>
            
    <?php echo form_open('hr/add-employment-contact/'.$personnel_id, array("class" => "form-horizontal", "role" => "form","id"=>"add-employment-contact"));?>
	<div class="row">
		<input type="hidden" name="personnel_id" id="personnel_id" value="<?php echo $personnel_id?>">
		<div class="col-md-6">
			<div class="form-group">
	            <label class="col-lg-5 control-label">Employer Name: </label>
	            
	            <div class="col-lg-7">
	            	<input type="text" class="form-control" name="employer_name" placeholder="Employer Name" value="<?php echo $employer_name;?>">
	            </div>
	        </div>
	        <div class="form-group">
	            <label class="col-lg-5 control-label">Employer Address: </label>
	            
	            <div class="col-lg-7">
	            	<input type="text" class="form-control" name="employer_address" placeholder="Employer Address" value="<?php echo $employer_address;?>">
	            </div>
	        </div>


	        
	        <div class="form-group">
	            <label class="col-lg-5 control-label">Job Title: </label>
	            
	            <div class="col-lg-7">
	                <select class="form-control" name="job_title_id">
	                	<option value="">--Select position--</option>
	                	<?php
	                    	if($job_titles_query->num_rows() > 0)
							{
								$job_titles = $job_titles_query->result();
								
								foreach($job_titles as $res)
								{
									$db_job_title_id = $res->job_title_id;
									$job_title_name = $res->job_title_name;
									
									if($db_job_title_id == $job_title_id)
									{
										echo '<option value="'.$db_job_title_id.'" selected>'.$job_title_name.'</option>';
									}
									
									else
									{
										echo '<option value="'.$db_job_title_id.'">'.$job_title_name.'</option>';
									}
								}
							}
						?>
	                </select>
	            </div>
	        </div>
	        
	        
	         
	    </div>
		<div class="col-md-6">
	         <div class="form-group">
	            <label class="col-lg-5 control-label">Start Date: </label>
	            
	            <div class="col-lg-7">
	            	<div class="input-group">
	                    <span class="input-group-addon">
	                        <i class="fa fa-calendar"></i>
	                    </span>
	                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="start_date" placeholder="Start Date" value="<?php echo $start_date;?>">
	                </div>
	            </div>
	        </div>
	         <div class="form-group">
	            <label class="col-lg-5 control-label">End Date: </label>
	            
	            <div class="col-lg-7">
	            	<div class="input-group">
	                    <span class="input-group-addon">
	                        <i class="fa fa-calendar"></i>
	                    </span>
	                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="end_date" placeholder="End Date" value="<?php echo $end_date;?>">
	                </div>
	            </div>
	        </div>
	         <div class="form-group">
	            <label class="col-lg-5 control-label">Reason For Leaving: </label>
	            
	            <div class="col-lg-7">
	            	
	                <textarea class="form-control" name="reason" id="reason" rows="5"></textarea>
	            </div>
	        </div>
	        
		</div>
	</div>
	<div class="row" style="margin-top:10px;">
		<div class="col-md-12">
	        <div class="form-actions center-align">
	        	
	            <button class="btn btn-primary" type="submit" onclick="return confirm('Are you sure you want to added employment history ? ')">
	                Add Employment History
	            </button>
	        </div>
	    </div>
	</div>
    <?php echo form_close();?>
  	<div id="employment-history"></div>
   