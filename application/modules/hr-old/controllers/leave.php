<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/hr/controllers/hr.php";
error_reporting(0);
class Leave extends hr 
{
	function __construct()
	{
		parent:: __construct();
	}
	
	function calender()
	{
		// var_dump(d)
		$v_data['title'] = $data['title'] = 'Leave Schedule';
		
		$v_data['personnel'] = $this->personnel_model->retrieve_personnel();
		$v_data['leave_types'] = $this->personnel_model->get_leave_types();

	
		
		$data['content'] = $this->load->view('leave/calender', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function leave_schedule()
	{
		$leave_result = $this->leave_model->get_assigned_leave();
		
		//initialize required variables
		$totals = '';
		$highest_bar = 0;
		$r = 0;
		$data = array();
		
		if($leave_result->num_rows() > 0)
		{
			$result = $leave_result->result();
			
			foreach($result as $res)
			{
				$personnel_id = $res->personnel_id;
				$personnel_fname = $res->personnel_fname;
				$personnel_onames = $res->personnel_onames;
				$leave_type_name = $res->leave_type_name;
				$leave_duration_status = $res->leave_duration_status;
				$start_date = $res->start_date;
				$end_date = $res->end_date;

				

				$start_date = date('D M d Y',strtotime($start_date)); 
				$end_date = date('D M d Y',strtotime($end_date)); 
				$time_start = $start_date.' 8:00 AM GMT+0300'; 
				$time_end = $end_date.' 11:59 PM GMT+0300';
				//$color = $this->reception_model->random_color();
				// var_dump($time_end);die();
				if($leave_duration_status == 1)
				{
					$color = '#0088CC';
				}
				
				else
				{
					$color = '#b71c1c';
				}
				$leave_days = $this->site_model->calculate_leave_days($start_date, $end_date);
				
				$data['title'][$r] = $personnel_fname.' '.$personnel_onames.' - '.$leave_type_name.'. '.$leave_days.' days';
				$data['start'][$r] = $time_start;
				$data['end'][$r] = $time_end;
				$data['backgroundColor'][$r] = $color;
				$data['borderColor'][$r] = $color;
				$data['allDay'][$r] = TRUE;
				$data['url'][$r] = site_url().'hr/personnel-leave-detail/'.$personnel_id;
				$r++;
			}
		}
		
		$data['total_events'] = $r;
		echo json_encode($data);
	}
	
	public function view_leave($date)
    {
        $v_data['leave'] = $this->leave_model->get_day_leave($date);
        $v_data['date'] = $date;
		
		$v_data['title'] = $data['title'] = 'Leave starting '.date('jS M Y',strtotime($date));
		
		$v_data['personnel'] = $this->personnel_model->retrieve_personnel();
		$v_data['leave_types'] = $this->personnel_model->get_leave_types();
		
		$data['content'] = $this->load->view('leave/list', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
    }
	
	function add_leave($date, $personnel_id)
	{
		$this->form_validation->set_rules('personnel_id', 'Personnel', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('leave_type_id', 'Leave Type', 'trim|numeric|required|xss_clean');

		if ($this->form_validation->run())
		{
			if($this->personnel_model->add_personnel_leave($this->input->post('personnel_id')))
			{
				$this->session->set_userdata("success_message", "Personnel leave  added successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add personnel leave. Please try again");
			}
			
			redirect('hr/personnel-leave-detail/'.$personnel_id);
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$this->view_leave($date);
		}
	}

	/*
	*
	*	Delete an existing personnel
	*	@param int $personnel_id
	*
	*/
	public function activate_leave($personnel_leave_id, $personnel_id)
	{
		if($this->leave_model->activate_leave_duration($personnel_leave_id))
		{
			$this->session->set_userdata('success_message', 'Leave has been claimed');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Leave could not claimed');
		}
		redirect('hr/personnel-leave-detail/'.$personnel_id);
	}

	/*
	*
	*	Delete an existing personnel
	*	@param int $personnel_id
	*
	*/
	public function deactivate_leave($leave_duration_id, $personnel_id)
	{
		if($this->leave_model->deactivate_leave_duration($leave_duration_id))
		{
			$this->session->set_userdata('success_message', 'Leave has been unclaimed');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Leave could not unclaimed');
		}
		redirect('hr/personnel-leave-detail/'.$personnel_id);
	}

	/*
	*
	*	Delete an existing personnel
	*	@param int $personnel_id
	*
	*/
	public function delete_leave($leave_duration_id, $personnel_id)
	{
		if($this->leave_model->delete_leave_duration($leave_duration_id))
		{
			$this->session->set_userdata('success_message', 'Leave has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Leave could not deleted');
		}
		redirect('hr/personnel-leave-detail/'.$personnel_id);
	}
	
	function add_calender_leave()
	{
		$this->form_validation->set_rules('personnel_id', 'Personnel', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('leave_type_id', 'Leave Type', 'trim|numeric|required|xss_clean');

		if ($this->form_validation->run())
		{
			if($this->personnel_model->add_personnel_leave($this->input->post('personnel_id')))
			{
				$this->session->set_userdata("success_message", "Personnel leave  added successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add personnel leave. Please try again");
			}
			
			redirect('hr/leave');
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$this->calender();
		}
	}
	
	public function leave_application($leave_type_id)
	{
			// var_dump($leave_type_id);die();
		$this->form_validation->set_rules('personnel_id', 'Personnel', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('leave_type_request_id', 'Leave Type', 'trim|required|xss_clean');
		
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');


		
		if($end_date < $start_date)
		{
			$this->session->set_userdata("error_message","The leave end date cannot be earlier than the start date.");
		}
		
		else if ($this->form_validation->run())
		{
			$leave_type_request_id = $this->input->post('leave_type_request_id');

			$exploded = explode('#',$leave_type_request_id);

			$leave_type_id = $exploded[0];
			$leave_type_count = $exploded[1];

			$leave_types = $this->leave_model->get_leave_types_by_id($leave_type_id);
			$leave_balance = 0;
			if($leave_types->num_rows() > 0)
		    {
		        foreach($leave_types->result() as $res)
		        {
		            $leave_type_id = $res->leave_type_id;
		            $leave_type_name = strtoupper($res->leave_type_name);
		            $leave_balance = $res->leave_days;
		        }
		    }
		            
		    $this->load->model('admin/email_model');
			$start_date = date('jS M Y',strtotime($start_date));
	        $end_date = date('jS M Y',strtotime($end_date));
	        $days_taken = $this->site_model->calculate_leave_days($start_date, $end_date, $leave_type_count);
	        $leave_balance -= $days_taken;


	  //       $personnel_fname = $this->session->userdata('first_name');
	  //       $message['subject'] =  $personnel_fname.' '.$leave_type_name.' Leave Request';
			// $v_data2['leave_type_name'] = $leave_type_name;
			// $v_data2['leave_balance'] = $leave_balance;
			// $v_data2['days_taken'] = $days_taken;
			// $v_data2['start_date'] = $start_date;
			// $v_data2['end_date'] = $end_date;

			// $text =  $this->load->view('leave/leave_request_email', $v_data2,true);
		
			// $message['text'] =$text;
			// $contacts = $this->site_model->get_contacts();
			// $sender_email = $contacts['email'];
			// $shopping = "";
			// $from = $sender_email; 
			
			// $button = '';
			// $sender['email']= $sender_email; 
			// $sender['name'] = $contacts['company_name'];
			// $receiver['name'] = 'Admin';
			// // $payslip = $title;



			// $sender_email = $sender_email;
			// $tenant_email = 'marttkip@gmail.com';//$this->config->item('directors_email');;
			// // var_dump($receiver); die();
			// $email_array = explode('/', $tenant_email);
			// $total_rows_email = count($email_array);

			// for($x = 0; $x < $total_rows_email; $x++)
			// {
			// 	$receiver['email'] = $email_tenant = $email_array[$x];

			// 	$this->email_model->send_sendgrid_mail($receiver, $sender, $message);	
				

			// }
	  //       	echo $text; die(); 
        



            if($leave_balance > 0)
            {

            	if($this->leave_model->add_personnel_leave_request($this->input->post('personnel_id')))
				{
					$this->session->set_userdata("success_message", "Leave application successfully pending approval.");

					// send the notifications to the admins

					
	                
				
					redirect('my-profile');
				}
				
				else
				{
					$this->session->set_userdata("error_message","Could not apply for leave. Please try again");
				}

            }

			
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		$v_data['title'] = $data['title'] = 'Leave Application';
		$v_data['leave_types'] = $this->personnel_model->get_leave_types();
		$v_data['personnel_id'] = $personnel_id = $this->session->userdata('personnel_id');
		$v_data['selected_leave_type_id'] = $leave_type_id;
		$v_data['leave'] = $this->personnel_model->get_personnel_leave($personnel_id);

		
		$data['content'] = $this->load->view('leave/leave_application', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function personnel_leaves($personnel_id)
	{

		
		$v_data['personnel'] = $this->personnel_model->get_personnel($personnel_id);

		$v_data['leave'] = $this->personnel_model->get_personnel_leave($personnel_id);
		$v_data['leave_types'] = $this->personnel_model->get_leave_types();
		$row = $v_data['personnel']->row();
		$names  = $row->personnel_fname;
		$data['title'] = $names.' Leaves';
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('leave/personnel_leave', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function leave_summaries()
	{


		$v_data['leave_types'] = $this->personnel_model->get_leave_types();
		
		$data['title'] = $names.' Leaves';
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('leave/leave_summaries', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function leave_calculator()
	{
		$where = 'personnel.personnel_status = 1 AND personnel_type_id = 1';
		$personnel = $this->personnel_model->retrieve_personnel($where);
		$leave_types = $this->personnel_model->get_leave_types(null,1);


		if($personnel->num_rows() > 0)
		{
			foreach($personnel->result() AS $key => $value)
			{
				foreach ($personnel->result() as $row)
				{
					$personnel_fname = $row->personnel_fname;
					$personnel_onames = $row->personnel_onames;
					$personnel_id = $row->personnel_id;
					$engagement_date = $row->engagement_date;
					$current_year = date('Y');

					$month = date('m',strtotime($engagement_date));
					$year = date('Y',strtotime($engagement_date));


					if($current_year > $year)
					{
						// do the full year
						if($leave_types->num_rows() > 0)
						{
							foreach($leave_types->result() as $res)
							{
								$leave_type_id = $res->leave_type_id;
								$leave_type_name = $res->leave_type_name;
								$total_leave = $res->leave_days;




								// insert into leave allocations 

								if($leave_type_id == 2 AND $gender_id == 2)
								{

									
									$array_allocation['personnel_id'] = $personnel_id;
									$array_allocation['leave_type_id'] = $leave_type_id;
									$array_allocation['leave_allocation_status'] = 0;
									$array_allocation['created'] = date('Y-m-d');
									$array_allocation['created_by'] = 0;
									$array_allocation['allocated_days'] = $total_leave;
									$array_allocation['year'] = $current_year;
									$array_allocation['remarks'] = $leave_type_name.' Leave Allocation';

									
									$this->db->where('leave_allocation_status = 0 AND leave_type_id = '.$leave_type_id.' AND personnel_id ='.$personnel_id);
									$query = $this->db->get('leave_allocations');

									if($query->num_rows() == 0)
									{
										// add allocations

										$this->db->insert('leave_allocations',$array_allocation);
									}

								}

								else if($leave_type_id == 1 AND $gender_id == 1)
								{


									$array_allocation['personnel_id'] = $personnel_id;
									$array_allocation['leave_type_id'] = $leave_type_id;
									$array_allocation['leave_allocation_status'] = 0;
									$array_allocation['created'] = date('Y-m-d');
									$array_allocation['created_by'] = 0;
									$array_allocation['allocated_days'] = $total_leave;
									$array_allocation['year'] = $current_year;
									$array_allocation['remarks'] = $leave_type_name.' Leave Allocation';

									
									$this->db->where('leave_allocation_status = 0 AND leave_type_id = '.$leave_type_id.' AND personnel_id ='.$personnel_id);
									$query = $this->db->get('leave_allocations');

									if($query->num_rows() == 0)
									{
										// add allocations

										$this->db->insert('leave_allocations',$array_allocation);
									}

								}
								else
								{



									$array_allocation['personnel_id'] = $personnel_id;
									$array_allocation['leave_type_id'] = $leave_type_id;
									$array_allocation['leave_allocation_status'] = 0;
									$array_allocation['created'] = date('Y-m-d');
									$array_allocation['created_by'] = 0;
									$array_allocation['allocated_days'] = $total_leave;
									$array_allocation['year'] = $current_year;
									$array_allocation['remarks'] = $leave_type_name.' Leave Allocation';

									
									$this->db->where('leave_allocation_status = 0 AND leave_type_id = '.$leave_type_id.' AND personnel_id ='.$personnel_id);
									$query = $this->db->get('leave_allocations');

									if($query->num_rows() == 0)
									{
										// add allocations

										$this->db->insert('leave_allocations',$array_allocation);
									}

								}
								
							}
						}
					}
					else if($current_year == $year)
					{

					

						// use engagement date

						$month = (int)$month;

						// for use to calculate the annual leave
						$remaining_months = 12-$month;
						$remaining_months += 1;
					

						// var_dump($remaining_months);die();
						if($leave_types->num_rows() > 0)
						{
							foreach($leave_types->result() as $res)
							{
								$leave_type_id = $res->leave_type_id;
								$leave_type_name = $res->leave_type_name;
								$total_leave = $res->leave_days;


								if($leave_type_id == 4 AND $remaining_months != 0)
								{
									$total_leave = round($remaining_months*1.75);
								}
								else if($leave_type_id == 4 AND $remaining_months == 0)
								{
									$total_leave = $total_leave;
								}

								if($leave_type_id == 2 AND $gender_id == 2)
								{

									
									$array_allocation['personnel_id'] = $personnel_id;
									$array_allocation['leave_type_id'] = $leave_type_id;
									$array_allocation['leave_allocation_status'] = 0;
									$array_allocation['created'] = date('Y-m-d');
									$array_allocation['created_by'] = 0;
									$array_allocation['allocated_days'] = $total_leave;
									$array_allocation['year'] = $current_year;
									$array_allocation['remarks'] = $leave_type_name.' Leave Allocation';

									
									$this->db->where('leave_allocation_status = 0 AND leave_type_id = '.$leave_type_id.' AND personnel_id ='.$personnel_id);
									$query = $this->db->get('leave_allocations');

									if($query->num_rows() == 0)
									{
										// add allocations

										$this->db->insert('leave_allocations',$array_allocation);
									}

								}

								else if($leave_type_id == 1 AND $gender_id == 1)
								{


									$array_allocation['personnel_id'] = $personnel_id;
									$array_allocation['leave_type_id'] = $leave_type_id;
									$array_allocation['leave_allocation_status'] = 0;
									$array_allocation['created'] = date('Y-m-d');
									$array_allocation['created_by'] = 0;
									$array_allocation['allocated_days'] = $total_leave;
									$array_allocation['year'] = $current_year;
									$array_allocation['remarks'] = $leave_type_name.' Leave Allocation';

									
									$this->db->where('leave_allocation_status = 0 AND leave_type_id = '.$leave_type_id.' AND personnel_id ='.$personnel_id);
									$query = $this->db->get('leave_allocations');

									if($query->num_rows() == 0)
									{
										// add allocations

										$this->db->insert('leave_allocations',$array_allocation);
									}

								}
								else
								{



									$array_allocation['personnel_id'] = $personnel_id;
									$array_allocation['leave_type_id'] = $leave_type_id;
									$array_allocation['leave_allocation_status'] = 0;
									$array_allocation['created'] = date('Y-m-d');
									$array_allocation['created_by'] = 0;
									$array_allocation['allocated_days'] = $total_leave;
									$array_allocation['year'] = $current_year;
									$array_allocation['remarks'] = $leave_type_name.' Leave Allocation';

									
									$this->db->where('leave_allocation_status = 0 AND leave_type_id = '.$leave_type_id.' AND personnel_id ='.$personnel_id);
									$query = $this->db->get('leave_allocations');

									if($query->num_rows() == 0)
									{
										// add allocations

										$this->db->insert('leave_allocations',$array_allocation);
									}

								}
								// insert into leave allocations 

								
							}
						}

					}

				}
			}
		}
	}

}
?>