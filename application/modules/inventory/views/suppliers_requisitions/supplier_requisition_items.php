<?php

$result = '';

$result ='<table class="table table-hover table-condensed table-bordered ">
              <thead>
                <tr>
                  <th>Product Name</th>
                  <th>Instructions</th>
                  <th>Current Stock</th>
                  <th>Units To Order</th>
                  <th>Total Units</th>
                  <th colspan="2">Actions</th>
                </tr>
              </thead>
              <tbody>
            ';
if($query != null)
{
	if($query->num_rows() > 0)
	{
		foreach ($query->result() as $key => $value) {
			# code...
			$product_id = $value->product_id;
			$order_item_id = $value->order_item_id;
			$order_item_instruction = $value->order_item_instruction;
			$product_name = $value->product_name;
			$units = $value->order_item_quantity;
			$current_stock = $value->current_stock;


			if(empty($units))
			{
				$units = 0;
			}


			 $result .= 
                '
                    <tr >
                        <td>'.$product_name.'</td>
                    
                       <td><input type="text" name="order_item_instruction'.$order_item_id.'" id="order_item_instruction'.$order_item_id.'" class="form-control" value="'.$order_item_instruction.'" ></td>
                       <td><input type="text" name="current_stock'.$order_item_id.'" id="current_stock'.$order_item_id.'" class="form-control" value="'.$current_stock.'" ></td>
                        <td><input type="text" name="units'.$order_item_id.'" id="units'.$order_item_id.'" class="form-control" value="'.$units.'" ></td>
                        <td>'.$units.'</td>
                        <td><a class="btn btn-xs btn-success" onclick="update_item_checked('.$order_item_id.','.$requisition_id.','.$store_id.')"><i class="fa fa-pencil"></i></a></td>
                        <td><a class="btn btn-xs btn-danger" onclick="remove_items('.$order_item_id.','.$requisition_id.')"><i class="fa fa-trash"></i></a></td>
                    </tr> 
                ';
			
		}
	}
}
 $result .= 
        '
          </tbody>
        </table>
        ';
echo $result;
if($requisition_id == 0)
{
	?>
	<div class="row">
		<div class="center-align">

			<a class="btn btn-xs btn-success" onclick="confirm_requisition(<?php echo $requisition_id?>)"> Confirm Requisition Items </a>
		</div>
	</div>
	<?php
}

?>