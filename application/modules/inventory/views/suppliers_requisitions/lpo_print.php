<?php

// COMPANY DETAILS
$data['contacts'] = $this->site_model->get_contacts();

$supplier_order_details = $this->requisition_model->get_supplier_order_details($supplier_order_id,$creditor_id);
// var_dump($supplier_order_id);die();
if($supplier_order_details->num_rows() > 0)
{
	foreach ($supplier_order_details->result() as $key_supplier) {
		# code...

		$order_number = $key_supplier->order_number;
		$lpo_number = $key_supplier->lpo_number;
		$order_sent_by = $key_supplier->order_sent_by;
		$lpo_generated_by = $key_supplier->lpo_generated_by;
		$order_approval_status = $key_supplier->order_approval_status;
		$approved_by = $key_supplier->approvee;
		$creditor_name = $key_supplier->creditor_name;
		$vendor_code = $key_supplier->vendor_code;
		$requisition_number = $key_supplier->requisition_number;
		$authorised_by = $key_supplier->authorised_by;
		$creditor_contact_person_name = $key_supplier->creditor_contact_person_name;
		$creditor_phone = $key_supplier->creditor_phone;
		$creditor_email = $key_supplier->creditor_email;
		$creditor_contact_person_email = $key_supplier->creditor_contact_person_email;
		$creditor_contact_person_name = $key_supplier->creditor_contact_person_name;
		// $supplier_order_id = $key_supplier->supplier_order_id;

		// $total_qoute_amount = $key_supplier->total_qoute_amount;
		// $qoutation_attachment = $key_supplier->qoutation_attachment;
		// $request_form_attachment = $key_supplier->request_form_attachment;
		// $lpo_form_attachment = $key_supplier->lpo_form_attachment;
	    if($order_approval_status == 3)
		{
			$add = 'P';
		}
		else if($order_approval_status == 4)
		{
			$add = 'PROVISIONAL LOCAL PURCASE ORDER';
		}
		else if($order_approval_status == 5)
		{
			$add = 'LOCAL PURCASE ORDER';
		}


	  if($order_approval_status == 3)
		{
			$title = '';
		}
		else if($order_approval_status == 4)
		{
			$title = 'THIS IS A PROVISIONAL LOCAL PURCASE ORDER NOT A FINAL LPO';
		}
		else if($order_approval_status == 5)
		{
		$title = 'Merchants are warned not to supply goods against any order than this form fully completed';
		}




		// $order_id = $key_supplier->order_id;
	}
}
$order_id = $supplier_order_id;

$prepared_by = '';
if($order_sent_by)
{
	$prepared_by = $this->requisition_model->get_personnel_name($order_sent_by);
}
// $approved_by = '';
// if($lpo_generated_by)
// {
// 	// $approved_by = $this->requisition_model->get_personnel_name($lpo_generated_by);
// }

$approvee = '';
if($approved_by)
{
	$approvee = $this->requisition_model->get_personnel_name($approved_by);
}

$authorise = '';
if($authorised_by)
{
	$authorise = $this->requisition_model->get_personnel_name($authorised_by);
}

$text = 'APPROVED';	
$color ='green';
$image = 'original.jpeg';

// order details
// $order_details = $this->requisition_model->get_order_items($order_id);
// end of order details

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | LPO</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 10px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:0px;}
			img.logo{height:130px; margin:0 auto;}
			.col-print-1 {width:8%;  float:left;}
			.col-print-2 {width:16%; float:left;}
			.col-print-3 {width:25%; float:left;}
			.col-print-4 {width:33%; float:left;}
			.col-print-5 {width:42%; float:left;}
			.col-print-6 {width:50%; float:left;}
			.col-print-7 {width:58%; float:left;}
			.col-print-8 {width:66%; float:left;}
			.col-print-9 {width:75%; float:left;}
			.col-print-10{width:83%; float:left;}
			.col-print-11{width:92%; float:left;}
			.col-print-12{width:100%; float:left;}


			@media print and (color) {
			   * {
			      -webkit-print-color-adjust: exact;
			      print-color-adjust: exact;

			   }
			    
			}

			.watermark-title{
			 
				position: fixed;
				top: 0;
				bottom: 0;
				left: 0;
				right: 0;
				z-index: -1;
				color: red;
				font-size: 105px;
				font-weight: 500px;
				display: grid;
				justify-content: center;
				align-content: center;
				opacity: 0.3;
				transform: rotate(45deg);
				z-index: -1;
			}
			
			

			@media print and (color) {
			   * {
			      -webkit-print-color-adjust: exact;
			      print-color-adjust: exact;


			   }
			    th, td {
                border: 1px solid #000 !important;
                padding-left: 2px;
                background-color: transparent !important;
                line-height: 2;
                /*padding: 0.5em 1em !important;*/
            	}
			    .watermark-title{
			 
				 position: fixed;
				top: 0;
				bottom: 0;
				left: 0;
				right: 0;
				z-index: -1;
				color: red;
				font-size: 105px;
				font-weight: bold;
				display: grid;
				justify-content: center;
				align-content: center;
				opacity: 0.3;
				transform: rotate(45deg);
				z-index: -1;
				}
			}

		</style>
    </head>
    <body class="receipt_spacing">
    	<!-- <div class="watermark-title" style="color: <?php echo $color?> !important;font-weight: bold;"><?php echo $text?></div> -->
    	<div class="row receipt_bottom_border">
        	<div class="col-print-4 pull-left">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" />
            </div>
            <div class="col-print-8 pull-left" style="margin-top: 20px !important;">
            	<strong style="font-size: 14px;" >
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['city'];?><br/>
                </strong>
            </div>
        </div>
    
        
      <div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align">
            	<strong><?php echo $add;?></strong>
            </div>
        </div>
      
        
        <!-- Patient Details -->
    	<div class="row receipt_bottom_border" style="margin-bottom: 10px;">
        	
	        <div class="col-md-12">
		        <table class="table table-hover table-bordered table-striped col-md-12">
		        		<tr>
		        			<td style="width:60%">
		        				<strong>Supplier's Code :</strong> <span style="font-size:15px;font-weight: bold;"><?php echo strtoupper(strtolower($vendor_code));?> </span><br>
		                		<strong>Supplier's Name :</strong> <span style="font-size:15px;font-weight: bold;"><?php echo strtoupper(strtolower($creditor_name));?> </span>
		                		<br>
		                		<br>
		                		<strong>Tel :</strong>  <?php echo $creditor_phone;?><br>
		        			</td>
		        			<td style="width:40%">
		        				<strong>L.P.O No: </strong>
		                    	 <?php echo $lpo_number;?><br>
		                    	 <strong>Req No.: </strong>
		                    	 <?php echo $requisition_number;?><br>
		                    	 <strong>Date :</strong> <?php echo date('jS F Y',strtotime(date("Y-m-d"))); ?><br>
		        			</td>
		        		</tr>
		        		<tr>
		        			<td style="width:60%">
		        				<strong>Contact Person :</strong> <?php echo $creditor_contact_person_name?> <br>
		                		<strong>Email :</strong> <?php echo $creditor_contact_person_email?> <br>
		                		<strong>Your Ref :</strong> __________________________
		                		<br>
		                		<br>
		        			</td>
		        			<td style="width:40%">
		        				<span style="text-decoration:underline;font-weight: bold;">Billing Address</span><br/>
		        				<span style="font-size:15px;font-weight: bold;"><?php echo strtoupper(strtolower($contacts['company_name']));?></span><br/>
			                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <br/>
			                    Tel : <?php echo $contacts['phone'];?><br/>
			                    <?php echo $contacts['city'];?><br/>
		        			</td>
		        		</tr>
		        </table>
		    </div>
            
        </div>
        
    	<div class="row">
        	<div class="col-md-12">
            				<table class="table table-hover table-bordered table-striped col-md-12">
                                <thead>
	                                <tr>
	                                  <th>CODE.</th>
	                                  <th>ITEM</th>
	                                  <th>QTY</th>
	                                  <th>DISC</th>
	                                  <th>VAT</th>
	                                  <th>PRICE</th>
	                                  <th>TOTAL</th>
	                                </tr>
                                </thead>
                                <tbody>
                                	<?php
                                	$item_no = 0;
                                	$items_rs = $this->orders_model->get_order_items_supplier($order_id,$creditor_id);

                                	$total_amount = 0;
                                	$grand_amount = 0;
                                	if($items_rs->num_rows() > 0)
                                	{
                                		foreach ($items_rs->result() as $key => $row) {
                                			# code...
                                			$product_id = $row->product_id;
											$product_name = $row->product_name;
											$product_status = $row->product_status;
											$reorder_level = $row->reorder_level;
											$store_id = $row->store_id;	
											$product_unitprice = $row->buying_unit_price;
								            $product_deleted = $row->product_deleted;
								            $quantity_received = $row->order_item_quantity;
								            $pack_size = $row->pack_size;
								            $discount = $row->discount;
								            $vat = $row->vat;
								            $unit_price = $row->unit_price;
											$less_vat = $row->less_vat;
											$vat = $row->vat;
											$discount = $row->discount;
								            $selling_unit_price = $row->selling_unit_price;
											

											$units_received = $quantity_received;
											
											$total_amount = $product_unitprice * $quantity_received;
											//status
											if($product_status == 1)
											{
												$status = 'Active';
											}
											else
											{
												$status = 'Disabled';
											}
											if($discount > 0)
											{
												$total_amount = $quantity_received *$product_unitprice;

												$amount_discounted = ($total_amount * (100-$discount))/100;
												$discount = '<span class="pull-left">'.number_format($discount,1).'% </span> <span class="pull-right">  '.number_format($amount_discounted,2).'</span>';
											}
											else{
												$discount = '<span class="pull-left">'.number_format(0,1).'% </span> <span class="pull-right">  '.number_format(0,2).'</span>';
											}
											
											$button = '';
											

											
											$markup = round(($product_unitprice * 1.33), 0);
											$markdown = $markup;//round(($markup * 0.9), 0);


											$grand_amount += $less_vat;				

											$item_no++;
											?>
											<tr>
		                                        <td><?php echo $item_no;?></td>
		                                        <td><?php echo $product_name;?></td>
		                                        <td><?php echo $quantity_received;?></td>
		                                        <td><?php echo $discount;?></td>
		                                        <td><?php echo number_format($vat,2);?></td>
		                                        <td><?php echo number_format($less_vat,2);?></td>
		                                        <td><?php echo number_format($less_vat,2);?></td>
											</tr>
											<?php
                                		}
                                	}
                                	?>
									 
                                      <tr style="height: 35px;">
                                      	<td colspan="6"><strong>Total Units :</strong> <?php echo $item_no?></td>
                                        <td><strong> <?php echo number_format($grand_amount,2);?></strong></td>
                                      </tr>

                                      <tr style="height: 25px;">
                                      	<td colspan="7"><strong>Terms and conditions apply :</strong></td>
                                     
                                      </tr>
                                      <?php

                                  ?>
                                    
                                </tbody>
                              </table>
            </div>
        </div>
        
    	<div class="row" style="font-weight:bold; font-size:12px;">
    		<div class="col-md-12">
	        	<div class="col-print-4 pull-left">
		            <div class="col-md-12" style="margin-bottom:30px">
		            	Prepared by: <?php echo $prepared_by?>
		            </div>
		            <br>
		            <div class="col-md-12">
		              Signature by: ...............................
		            </div>
		          </div>
	        	<div class="col-print-4 ">
	        		<div class="col-md-12" style="margin-bottom:30px">
		            	Approved By: <?php echo $approvee;?>
		            </div>
		            <br>
		            <div class="col-md-12">
		              Signature by: ................................
		            </div>
	            	
	            </div>

	            <div class="col-print-4">
	        		<div class="col-md-12" style="margin-bottom:30px">
		            	Authorised By: <?php echo $authorise;?>
		            </div>
		            <br>
		            <div class="col-md-12">
		              Signature by: ................................
		            </div>
	            	
	            </div>
	        </div>
        </div>
        <?php
        // get office who officiated the transaction

        $authorising_officer =  $this->orders_model->get_lpo_authorising_personnel($order_id);
        ?>
        <div class="row" style="font-style:bold; font-size:11px; margin-top:10px">
        	<div class="col-md-8 pull-left">
	            <div class="col-md-6 pull-left"> 
	            	<strong>Authorised By :</strong> 
                    <?php echo $authorising_officer;?>
	            	<?php echo $contacts['company_name'];?>

	            </div>
	           
	        </div>
        </div>
        <br>
         <div class="row" >
        	<div class="col-md-12">
        		            	<strong><?php echo $title;?></strong>

        	</div>
        </div>
    </body>
    
</html>