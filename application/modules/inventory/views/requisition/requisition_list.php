
<section class="panel panel-featured panel-featured-info">
    <header class="panel-heading">
         <h2 class="panel-title pull-left"><?php echo $title;?></h2>
         <div class="widget-icons pull-right">
            	<a href="<?php echo base_url();?>procurement/add-requisition" class="btn btn-success btn-sm">Add Requisition</a>
          </div>

         
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
			<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');
				$search_result ='';
				$search_result2  ='';
				if(!empty($error))
				{
					$search_result2 = '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}
				
				if(!empty($success))
				{
					$search_result2 ='<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}
						
				$search = $this->session->userdata('orders_search');
				
				if(!empty($search))
				{
					$search_result = '<a href="'.site_url().'inventory/orders/close_search_orders" class="btn btn-danger">Close Search</a>';
				}


				$result = '<div class="padd">';	
				$result .= ''.$search_result2.'';
				$result .= '
						';
				
				//if users exist display them
				if ($query->num_rows() > 0)
				{
					$count = $page;
					
					$result .= 
					'
					<div class="row">
						<div class="col-md-12">
							<table class="example table-autosort:0 table-stripeclass:alternate table table-hover table-bordered " id="TABLE_2">
							  <thead>
								<tr>
								  <th >#</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Date Created</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Requisition Number</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort"></th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Created By</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Status</th>
								  <th colspan="2">Actions</th>
								</tr>
							  </thead>
							  <tbody>
							';
					
								//get all administrators
								$personnel_query = $this->personnel_model->get_all_personnel();
								
								foreach ($query->result() as $row)
								{
									$requisition_id = $row->requisition_id;
									$requisition_number = $row->requisition_number;
									$requisition_date = $row->created;
									$status = $row->requisition_status;

									// var_dump($order_approval_status); die();

									// $order_details = $this->orders_model->get_order_items($order_id);
									$total_price = 0;
									$total_items = 0;
									//creators & editors
									
									if($personnel_query->num_rows() > 0)
									{
										$personnel_result = $personnel_query->result();
										
										foreach($personnel_result as $adm)
										{
											$personnel_id2 = $adm->personnel_id;
											
											if($created_by == $personnel_id2 ||  $modified_by == $personnel_id2 )
											{
												$created_by = $adm->personnel_fname;
												break;
											}
											
											else
											{
												$created_by = '-';
											}
										}
									}
									
									else
									{
										$created_by = '-';
									}

									
									$button = '';

									


									
									// just to mark for the next two stages
								

									$count++;
									$result .= 
									'
										<tr>
											<td>'.$count.'</td>
											<td>'.date('jS M Y H:i a',strtotime($requisition_date)).'</td>
											<td>'.$requisition_number.'</td>
											<td></td>
											<td>'.$created_by.'</td>
											<td>'.$status.'</td>
											<td><a href="'.site_url().'inventory/add-order-item/'.$requisition_id.'" class="btn btn-success  btn-sm fa fa-folder"> Order Items</a></td>
											
											
										</tr> 
									';
									// }
								}
					
						$result .= 
						'
								  </tbody>
								</table>
							</div>
						</div>
						';
				}
				
				else
				{
					$result .= "There are no orders";
				}
				$result .= '</div>';
				echo $result;
			?>

    </div>
</section>