<?php

	$date_tomorrow = date('Y-m-d');
		$visit_date = date('jS M Y',strtotime($date_tomorrow));

	    $date_tomorrow = date('Y-m-d');
		//$date_tomorrow = date("Y-m-d", strtotime("-6 day", strtotime($date_tomorrow)));


		$branch = $this->config->item('branch_name');
		$message['subject'] =  $branch.' '.$visit_date.' report';

		$where = $where1 = $where6 = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 AND visit.visit_date = "'.$date_tomorrow.'"';
		$payments_where = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 ';
		$table = 'visit, patients';


		






		 // var_dump($total_other_collection+$total_mpesa_collection+$total_cash_collection); die();
		
		//count outpatient visits
		$where2 = $where1.' AND visit.inpatient = 0';

		(int)$outpatients = $this->reception_model->count_items($table, $where2);
		// var_dump($outpatients); die();
		//count inpatient visits
		$where2 = $where6.' AND visit.inpatient = 1';

		(int)$inpatients = $this->reception_model->count_items($table, $where2);


		$table1 = 'petty_cash,account';
		$where1 = 'petty_cash.account_id = account.account_id AND (account.account_name = "Cash Box" OR account.account_name = "Cash Collection") AND petty_cash.petty_cash_delete = 0';		
		
		$where1 .=' AND petty_cash.petty_cash_date = "'.$date_tomorrow.'"';
		$total_transfers = $this->reports_model->get_total_transfers($where1,$table1);



		$table1 = 'account_payments';
		$where1 = 'account_payments.account_to_id = (SELECT account_id FROM account WHERE account_name = "Equity Bank")AND account_payments.account_payment_deleted = 0 AND account_payments.account_from_id = (SELECT account_id FROM account WHERE account_name = "Cash Account")';			
		$where1 .=' AND account_payments.payment_date = "'.$date_tomorrow.'"';
		$select = 'SUM(account_payments.amount_paid) AS total_paid';
		$total_bank_transfers = $this->reports_model->get_total_account_transfers($where1,$table1,$select);


		$table5 = 'account_payments';
		$where5 = 'account_payments.account_to_id = (SELECT account_id FROM account WHERE account_name = "Petty Cash") AND account_payments.account_payment_deleted = 0 AND account_payments.account_from_id = (SELECT account_id FROM account WHERE account_name = "Cash Account")';			
		$where5.=' AND account_payments.payment_date = "'.$date_tomorrow.'"';
		$select5 = 'SUM(account_payments.amount_paid) AS total_paid';
		$total_petty_cash_transfers = $this->reports_model->get_total_account_transfers($where5,$table5,$select5);


		$table2 = 'visit';
		$where2 = 'visit.visit_delete = 0 AND rejected_amount > 0';			
		$where2 .=' AND visit.visit_date = "'.$date_tomorrow.'"';
		$select2 = 'SUM(visit.rejected_amount) AS total_paid';
		$total_rejected_amount = $this->reports_model->get_total_account_transfers($where2,$table2,$select2);

		$table3 = 'payments';
		$where3 = 'cancel = 1';			
		$where3 .=' AND cancelled_date = "'.$date_tomorrow.'"';
		$select3 = 'SUM(amount_paid) AS total_paid';
		$total_cancelled_amount = $this->reports_model->get_total_account_transfers($where3,$table3,$select3);


		$table4 = 'finance_purchase';
		$where4 = 'finance_purchase.finance_purchase_status = 1';			
		$where4 .=' AND finance_purchase.transaction_date = "'.$date_tomorrow.'"';
		$select4 = 'SUM(finance_purchase.finance_purchase_amount) AS total_paid';
		$total_petty_cash_usage = $this->reports_model->get_total_account_transfers($where4,$table4,$select4);

	



		$total_patients = $outpatients + $inpatients;

		$visit_types_rs = $this->reception_model->get_visit_types();
		$visit_result = '';
		$total_balance = 0;
		$total_invoices = 0;
		$total_payments = 0;
		$total_patients = 0;
		// $total_cash_invoices = 0;
		// $total_insurance_invoices = 0;

		// $total_cash_payments = 0;
		// $total_insurance_payments = 0;

		// $total_cash_balance = 0;
		// $total_insurance_balance = 0;

		

		$where = '(v_transactions_by_date.transactionCategory = "Revenue") AND visit_invoice.visit_invoice_id = v_transactions_by_date.transaction_id AND v_transactions_by_date.transaction_date = "'.$date_tomorrow.'"';
		
		$table = 'v_transactions_by_date,visit_invoice';

		$query = $this->reports_model->get_all_patients_invoices_reports($table, $where);

		$visit_result .=
						'
							<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
							  <thead>
								<tr>
								  <th>#</th>
								  <th>PATIENT TYPE</th>
								  <th>PATIENT NUMBER</th>
								  <th>PATIENT NAMES</th>
								  <th>DOCTOR</th>
								  <th>AMOUNT INVOICED</th>
								  <th>WAIVED AMOUNT</th>
								  <th>AMOUNT COLLECTED</th>
								  <th>BALANCES</th>
								</tr>
							  </thead>
							  <tbody>
					';
		if ($query->num_rows() > 0)
		{
			$count = 0;
			
		
			// $personnel_query = $this->accounting_model->get_all_personnel();
			$total_waiver = 0;
	
			$total_invoice = 0;
			$total_cash_invoices =0;
			$total_balance = 0;
			$total_rejected_amount = 0;
			$total_cash_balance = 0;
			$total_insurance_payments =0;
			$total_insurance_invoice =0;
			$total_insurance_credits =0;
			$total_insurance_balance =0;
			$total_payable_by_patient = 0;
			$total_payable_by_insurance = 0;
			$total_debit_notes = 0;
			$total_credit_notes= 0;
			$total_credits = 0;
			foreach ($query->result() as $row)
			{
				$total_invoiced = 0;
				$visit_date = date('jS M Y',strtotime($row->transaction_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}
				
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type;
				$patient_number = $row->patient_number;
				$visit_type = $row->payment_type;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				$rejected_amount = $row->amount_rejected;
				$visit_invoice_number = $row->visit_invoice_number;
				$visit_invoice_id = $row->visit_invoice_id;
				$parent_visit = $row->parent_visit;
				$branch_code = $row->branch_code;
				$preauth_status = $row->preauth_status;

				if(empty($rejected_amount))
				{
					$rejected_amount = 0;
				}
				// $coming_from = $this->reception_model->coming_from($visit_id);
				// $sent_to = $this->reception_model->going_to($visit_id);
				$visit_type_name = $row->payment_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;

				$doctor = $row->personnel_fname;
				$count++;
				


				if($visit_type == 1)
				{

					$invoice_total = $this->accounts_model->get_visit_invoice_total($visit_invoice_id);
					$payments_value = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
					$credit_note =  $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

					$invoice_total -= $credit_note;
					$balance  = $this->accounts_model->balance($payments_value,$invoice_total);

					$total_cash_invoices += $invoice_total;
					$total_cash_payments += $payments_value;
					$total_cash_balance += $balance;
					$total_waiver += $credit_note;

					$total_payable_by_patient += $invoice_total;
					$total_credits += $credit_note;
					$total_payments += $payments_value;
					$total_balance += $balance;



					$visit_result .= 
									'
											<tr>
												<td >'.$count.'</td>
												<td>'.$visit_type_name.'</td>
												<td >'.$patient_number.'</td>
												<td >'.strtoupper(strtolower($patient_surname).' '.strtolower($patient_othernames)).'</td>
												<td>'.strtoupper($doctor).'</td>
												<td>'.number_format($invoice_total,2).'</td>
												<td>'.(number_format($credit_note,2)).'</td>
												<td>'.(number_format($payments_value,2)).'</td>
												<td>'.(number_format($balance,2)).'</td>
											</tr> 
									';



				}
				else
				{


					$insurance_invoice = $this->accounts_model->get_visit_invoice_total($visit_invoice_id);
					$insurance_payments = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
					$insurance_credit =  $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

					$insurance_invoice -= $insurance_credit;
					$insurance_balance  = $this->accounts_model->balance($insurance_payments,$insurance_invoice);

					$total_insurance_invoices += $insurance_invoice;
					$total_insurance_payments += $insurance_payments;
					$total_insurance_balance += $insurance_balance;
					$total_insurance_credits += $insurance_credit;


					$total_payable_by_patient += $insurance_invoice;
					$total_credits += $insurance_credit;
					// $total_payments += $insurance_payments;
					$total_balance += $insurance_balance;


					$visit_result .= 
									'
											<tr>
												<td >'.$count.'</td>
												<td>'.$visit_type_name.'</td>
												<td >'.$patient_number.'</td>
												<td >'.strtoupper(strtolower($patient_surname).' '.strtolower($patient_othernames)).'</td>
												<td>'.strtoupper($doctor).'</td>
												<td>'.number_format($insurance_invoice,2).'</td>
												<td>'.(number_format($insurance_credit,2)).'</td>
												<td>'.(number_format($insurance_payments,2)).'</td>
												<td>'.(number_format($insurance_balance,2)).'</td>
											</tr> 
									';

					
				}

					
				
				
			}
			// echo $total_cash_invoices.', ';

			$visit_result .= 
					'
						<tr>
							<td colspan=5> TOTALS</td>
							<td style="border-top:2px solid #000;"><strong>'.number_format($total_payable_by_patient,2).'</strong></td>
							<td style="border-top:2px solid #000;"><strong>'.number_format($total_credits,2).'</strong></td>
							<td style="border-top:2px solid #000;"><strong>'.number_format($total_payments,2).'</strong></td>
							<td style="border-top:2px solid #000;"><strong>'.number_format($total_balance,2).'</strong></td>
						</tr> 
				';
			$visit_result .=
	'
				  </tbody>
				</table>
	';
		
		}
		
		else
		{
			$visit_result .= "There are no visits";
		}


		$services_result = $this->reports_model->get_all_service_types();
		$service_result = '';
		$total_service_invoice = 0;
		$total_service_payment = 0;
		$total_service_balance = 0;
		if($services_result->num_rows() > 0)
		{
			$result = $services_result->result();
			$grand_total = 0;			
			foreach($result as $res)
			{
				$service_id = $res->service_id;
				$service_name = $res->service_name;
				$count++;
				
				//get service total
				$service_invoice = $this->reports_model->get_service_invoice_total($service_id,$date_tomorrow,1);
				$service_payment = 0;//$this->reports_model->get_service_payments_total($service_id,$date_tomorrow,1);
				$service_balance =0;// abs($service_payment - $service_invoice);

				$total_service_invoice = $total_service_invoice + $service_invoice;
				$total_service_payment = $total_service_payment + $service_payment;
				$total_service_balance = $total_service_balance + $service_balance;
				
				$grand_total += $service_invoice;

				

				$service_result .='<tr>
							  		<td style="text-align:left;"> '.strtoupper($service_name).'  </td>
							  		<td style="text-align:center;"> '.number_format($service_invoice,2).'</td>
							  	</tr>';
				

			}

			$undefined_payment = $this->reports_model->get_payments_total(0,$date_tomorrow);
			
		
			$service_result .='<tr>
							  		<td style="text-align:left;"> TOTAL </td>
							  		<td style="text-align:center;border-top:2px solid #000;">Ksh. '.number_format($total_service_invoice,2).'</td
							  	</tr>';
		}


		// insurance departments

		$services_result = $this->reports_model->get_all_service_types();
		$insurance_service_result = '';
		$total_service_invoice = 0;
		$total_service_payment = 0;
		$total_service_balance = 0;
		if($services_result->num_rows() > 0)
		{
			$result = $services_result->result();
			$grand_total = 0;			
			foreach($result as $res)
			{
				$service_id = $res->service_id;
				$service_name = $res->service_name;
				$count++;
				
				//get service total
				$service_invoice = $this->reports_model->get_service_invoice_total($service_id,$date_tomorrow,0);
				$service_payment = $this->reports_model->get_service_payments_total($service_id,$date_tomorrow,0);
				$service_balance = abs($service_payment - $service_invoice);

				$total_service_invoice = $total_service_invoice + $service_invoice;
				$total_service_payment = $total_service_payment + $service_payment;
				$total_service_balance = $total_service_balance + $service_balance;
				
				$grand_total += $service_invoice;

				

				$insurance_service_result .='<tr>
							  		<td style="text-align:left;"> '.strtoupper($service_name).'  </td>
							  		<td style="text-align:center;"> '.number_format($service_invoice,2).'</td>
							  		<td style="text-align:center;"> '.number_format($total_insurance_payments,2).'</td>
							  		<td style="text-align:center;"> '.number_format($service_balance,2).'</td>
							  	</tr>';
				

			}

			$undefined_payment = $this->reports_model->get_payments_total(0,$date_tomorrow);
			$insurance_service_result .='<tr>
							  		<td style="text-align:left;" colspan="1"> WAIVER </td>
							  		<td style="text-align:center;"> ('.number_format($total_waiver,2).')</td>
							  		<td style="text-align:center;"></td>
							  		<td style="text-align:center;">('.number_format($total_waiver,2).')</td>
							  	</tr>';
			$insurance_service_result .='<tr>
							  		<td style="text-align:left;" colspan="2"> PAYMENTS</td>
							  		<td style="text-align:center;"> '.number_format($total_insurance_payments,2).'</td>
							  		<td style="text-align:center;"> ('.number_format($total_service_payment,2).')</td>
							  	</tr>';

			$insurance_service_result .='<tr>
							  		<td style="text-align:left;"> TOTAL </td>
							  		<td style="text-align:center;border-top:2px solid #000;">Ksh. '.number_format($total_service_invoice-$total_waiver,2).'</td>
							  		<td style="text-align:center;border-top:2px solid #000;">Ksh. '.number_format($total_insurance_payment,2).'</td>
							  		<td style="text-align:center;border-top:2px solid #000;">Ksh. '.number_format($total_service_balance - $total_insurance_payments -$total_waiver,2).'</td>
							  	</tr>';
		}





		


$where = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 AND visit_type.visit_type_id = visit.visit_type AND payments.visit_id = visit.visit_id AND payments.payment_created = "'.$date_tomorrow.'" AND payments.cancel = 0 AND payments.payment_method_id = payment_method.payment_method_id AND payments.payment_method_id = 2 ';
$table = 'visit,patients,payments,payment_method, visit_type';
$select = 'visit.visit_date,patients.patient_surname,patients.patient_othernames, patients.patient_phone1,payment_method.payment_method_id,payment_method.payment_method,payments.amount_paid,visit.visit_type ';
//$select4 = 'SUM(payments.amount) AS total_paid';

$order_by = 'payments.payment_method_id';

//cash payments todays visit
// $where2 = $payments_where.' AND payments.payment_method_id = 2 AND payments.payment_type = 1 AND payments.cancel = 0 AND visit.visit_date = "'.$date_tomorrow.'" AND payments.payment_created = "'.$date_tomorrow.'"';
$payments_rs = $this->reports_model->get_content($table,$where,$select,NULL,NULL,$order_by);

// var_dump($payments_rs->num_rows());die();
$payments_result = '';
if($payments_rs->num_rows() > 0)
{
	$paying_id = 0;
	$x = 0;
	$total_amount = 0;
	foreach ($payments_rs->result() as $key => $value) {
		# code...

		$patient_surname = $value->patient_surname;
		$patient_othernames = $value->patient_othernames;
		$payment_method = $value->payment_method;
		$payment_method_id = $value->payment_method_id;
		$amount_paid = $value->amount_paid;
		$patient_phone1 = $value->patient_phone1;
		$visit_id = $value->visit_id;
		$total += $amount_paid;  

		$x++;

	 //    if($paying_id != $payment_method_id AND $x > 0)
		// {
			
		// 	$payments_result .= '<tr>
		// 							<td></td>
		// 							<td><strong>TOTAL</strong></td>
		// 							<td><strong>'.number_format($total_cash_collection,2).'</strong></td>
		// 							<td></td>
		// 						<tr>';
		// 	$payments_result .= '<tr>
		// 							<td colspan="4"> &nbsp; </td>
									
		// 						<tr>';
		// 	$total_amount = 0;
		// }

		//$paying_id = $payment_method_id;
		$payments_result .= '<tr>
								<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_phone1.'</td>
								<td>'.strtoupper($payment_method).' (KES)</td>
								<td>'.number_format($amount_paid,2).'</td>
								<td>posted</td>
							<tr>
						';
		//$total_amount += $amount_paid;



	}

		$payments_result .= '<tr>
								<td></strong>TOTAL</strong></td>
								<td></td>
								<td><strong>'.number_format($total_cash_collection,2).'</strong></td>
								
							<tr>';
}


$where = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 AND payments.visit_id = visit.visit_id AND payments.payment_created = "'.$date_tomorrow.'" AND payments.cancel = 0 AND payments.payment_method_id = payment_method.payment_method_id AND payments.payment_method_id = 5 ';
$table = 'visit,patients,payments,payment_method';
$select = 'visit.visit_date,patients.patient_surname,patients.patient_othernames, patients.patient_phone1,payment_method.payment_method_id,payment_method.payment_method,payments.amount_paid,visit.visit_id ';

//$order_by = 'payments.payment_method_id= 5';

//cash payments todays visit
// $where2 = $payments_where.' AND payments.payment_method_id = 2 AND payments.payment_type = 1 AND payments.cancel = 0 AND visit.visit_date = "'.$date_tomorrow.'" AND payments.payment_created = "'.$date_tomorrow.'"';
$payments_rs = $this->reports_model->get_content($table,$where,$select,NULL,NULL,$order_by);
$doctor_results = $this->reports_model->get_all_doctors();
// var_dump($payments_rs->num_rows());die();
$payments_result1 = '';
if($payments_rs->num_rows() > 0)
{
	$paying_id = 0;
	$x = 0;
	$total_amount = 0;
	foreach ($payments_rs->result() as $key => $value) {
		# code...

		$patient_surname = $value->patient_surname;
		$patient_othernames = $value->patient_othernames;
		$payment_method = $value->payment_method;
		$payment_method_id = $value->payment_method_id;
		$amount_paid = $value->amount_paid;
		$patient_phone1 = $value->patient_phone1;
		$visit_id = $value->visit_id;
		$total += $total_amount;  

		$x++;

		$payments_result1 .= '<tr>
								<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_phone1.'</td>
								<td>'.strtoupper($payment_method).' (KES)</td>
								<td>'.number_format($amount_paid,2).'</td>
								<td>posted</td>
							<tr>';
	



	}

	$payments_result1 .= '<tr>
								<td></strong>TOTAL</strong></td>
								<td></td>
								<td><strong>'.number_format($total_mpesa_collection,2).'</strong></td>
								
							<tr>';
}

$where = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 AND payments.visit_id = visit.visit_id AND payments.payment_created = "'.$date_tomorrow.'" AND payments.cancel = 0 AND payments.payment_method_id = payment_method.payment_method_id AND payments.payment_method_id = 7 ';
$table = 'visit,patients,payments,payment_method';
$select = 'visit.visit_date,patients.patient_surname,patients.patient_othernames, patients.patient_phone1,payment_method.payment_method_id,payment_method.payment_method,payments.amount_paid,visit.visit_id ';


$payments_rs = $this->reports_model->get_content($table,$where,$select,NULL,NULL,$order_by);


$payments_result2 = '';
if($payments_rs->num_rows() > 0)
{
	$paying_id = 0;
	$x = 0;
	$total_amount = 0;
	foreach ($payments_rs->result() as $key => $value) {
		# code...

		$patient_surname = $value->patient_surname;
		$patient_othernames = $value->patient_othernames;
		$payment_method = $value->payment_method;
		$payment_method_id = $value->payment_method_id;
		$amount_paid = $value->amount_paid;
		$patient_phone1 = $value->patient_phone1;
		$visit_id = $value->visit_id;
		$total += $total_amount;  

		$x++;

		$payments_result2 .= '<tr>
								<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_phone1.'</td>
								<td>'.strtoupper($payment_method).' (KES)</td>
								<td>'.number_format($amount_paid,2).'</td>
								<td>posted</td>
							<tr>';
		$total_amount += $amount_paid;
	


	}
}


$where = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 AND payments.visit_id = visit.visit_id AND payments.payment_created = "'.$date_tomorrow.'" AND payments.cancel = 0 AND payments.payment_method_id = payment_method.payment_method_id AND payments.payment_method_id = 9 ';
$table = 'visit,patients,payments,payment_method';
$select = 'visit.visit_date,patients.patient_surname,patients.patient_othernames, patients.patient_phone1,payment_method.payment_method_id,payment_method.payment_method,payments.amount_paid,visit.visit_id ';


$payments_rs = $this->reports_model->get_content($table,$where,$select,NULL,NULL,$order_by);


$payments_result3 = '';
if($payments_rs->num_rows() > 0)
{
	$paying_id = 0;
	$x = 0;
	$total_amount = 0;
	foreach ($payments_rs->result() as $key => $value) {
		# code...

		$patient_surname = $value->patient_surname;
		$patient_othernames = $value->patient_othernames;
		$payment_method = $value->payment_method;
		$payment_method_id = $value->payment_method_id;
		$amount_paid = $value->amount_paid;
		$patient_phone1 = $value->patient_phone1;
		$visit_id = $value->visit_id;
		$total += $total_amount;  

		$x++;

		$payments_result3 .= '<tr>
								<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_phone1.'</td>
								<td>'.strtoupper($payment_method).' (KES)</td>
								<td>'.number_format($amount_paid,2).'</td>
								<td>posted</td>
							<tr>';
		$total_amount += $amount_paid;
		// if($paying_id != $payment_method_id AND $x > 0)
		// {
			
		// 	$payments_result3 .= '<tr>
		// 							<td></td>
		// 							<td><strong>TOTAL</strong></td>
		// 							<td><strong>'.number_format($total,2).'</strong></td>
		// 							<td></td>
		// 						<tr>';
		// 	$payments_result3 .= '<tr>
		// 							<td colspan="4"> &nbsp; </td>
									
		// 						<tr>';
		// 	$total_amount = 0;
		// }

		//$paying_id = $payment_method_id;



	}

		$payments_result3 .= '<tr>
								<td></strong>TOTAL</strong></td>
								<td></td>
								<td><strong>'.number_format($total_swap_collection,2).'</strong></td>
								
							<tr>';
}






$debtors_where = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 AND visit.visit_type = 1 AND visit.visit_date = "'.$date_tomorrow.'" ';
$debtors_table = 'visit,patients';
$debtors_select = 'visit.visit_date,patients.patient_surname,patients.patient_othernames, patients.patient_phone1,visit.visit_id ';

$debtors_order_by = 'visit.visit_id';

//cash payments todays visit
// $where2 = $payments_where.' AND payments.payment_method_id = 2 AND payments.payment_type = 1 AND payments.cancel = 0 AND visit.visit_date = "'.$date_tomorrow.'" AND payments.payment_created = "'.$date_tomorrow.'"';
$debtors_rs = $this->reports_model->get_content($debtors_table,$debtors_where,$debtors_select,NULL,NULL,$debtors_order_by);

// var_dump($debtors_rs->num_rows());die();
$debtors_result1 = '';
$total_invoice1 = 0;
	$total_payment = 0;
$patients = 0;
if($debtors_rs->num_rows() > 0)
{
	$paying_id = 0;
	


	foreach ($debtors_rs->result() as $key => $value_debtor) {
		# code...

		$patient_surname = $value_debtor->patient_surname;
		$patient_othernames = $value_debtor->patient_othernames;
		$payment_method = $value_debtor->payment_method;
		$payment_method_id = $value_debtor->payment_method_id;
		$amount_paid = $value_debtor->amount_paid;
		$patient_phone1 = $value_debtor->patient_phone1;
		$visit_id = $value_debtor->visit_id;

		$patients++;

		// this is to check for any credit note or debit notes
		$payments_value = $this->reports_model->visit_payments($visit_id,$date_tomorrow);

		$invoice_total = $this->reports_model->visit_invoices($visit_id,$date_tomorrow);


		if($payments_value > $invoice_total)
		{
			$balance1 = $payments_value - $invoice_total;
		}
		else
		{
			$balance = 0;
		}
		$total_payment += $payments_value;
		$total_invoice1  += $balance1;
		$debtors_result1 .= '<tr>
								<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_phone1.'</td>
								<td>Sales Journal (KES)</td>
								<td>'.number_format($balance1,2).'</td>
								
							<tr>';
		



	}

	$debtors_result1 .= '<tr>
								<td></strong>TOTAL</strong></td>
								<td></td>
								<td><strong>'.number_format($total_invoice1,2).'</strong></td>
								
							<tr>';
}

$debtors_where = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 AND visit.visit_date = "'.$date_tomorrow.'" ';
$debtors_table = 'visit,patients';
$debtors_select = 'visit.visit_date,patients.patient_surname,patients.patient_othernames, patients.patient_phone1,visit.visit_id ';

$debtors_order_by = 'visit.visit_id';

//cash payments todays visit
// $where2 = $payments_where.' AND payments.payment_method_id = 2 AND payments.payment_type = 1 AND payments.cancel = 0 AND visit.visit_date = "'.$date_tomorrow.'" AND payments.payment_created = "'.$date_tomorrow.'"';
$debtors_rs = $this->reports_model->get_content($debtors_table,$debtors_where,$debtors_select,NULL,NULL,$debtors_order_by);

// var_dump($debtors_rs->num_rows());die();
$debtors_result2 = '';
$total_invoice2 = 0;
	$total_payment = 0;
$patients1 = 0;
if($debtors_rs->num_rows() > 0)
{
	$paying_id = 0;
	


	foreach ($debtors_rs->result() as $key => $value_debtor) {
		# code...

		$patient_surname = $value_debtor->patient_surname;
		$patient_othernames = $value_debtor->patient_othernames;
		$payment_method = $value_debtor->payment_method;
		$payment_method_id = $value_debtor->payment_method_id;
		$amount_paid = $value_debtor->amount_paid;
		$patient_phone1 = $value_debtor->patient_phone1;
		$visit_id = $value_debtor->visit_id;

		$patients1++;

		// this is to check for any credit note or debit notes
		$payments_value = $this->reports_model->visit_payments($visit_id,$date_tomorrow);

		$invoice_total = $this->reports_model->visit_invoices($visit_id,$date_tomorrow);


		if($payments_value > $invoice_total)
		{
			$balance2 = $payments_value - $invoice_total;
		}
		else
		{
			$balance = 0;
		}
		$total_payment += $payments_value;
		$total_invoice2  += $balance2;
		$debtors_result2 .= '<tr>
								<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_phone1.'</td>
								<td>Sales Journal (KES)</td>
								<td>'.number_format($balance2,2).'</td>
								
							<tr>';
		



	}

	$debtors_result2 .= '<tr>
								<td></strong>TOTAL</strong></td>
								<td></td>
								<td><strong>'.number_format($total_invoice2,2).'</strong></td>
								
							<tr>';
}

$debtors_where = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 AND visit.visit_type > 1 AND visit.visit_date = "'.$date_tomorrow.'" ';
$debtors_table = 'visit,patients';
$debtors_select = 'visit.visit_date,patients.patient_surname,patients.patient_othernames, patients.patient_phone1,visit.visit_id ';

$debtors_order_by = 'visit.visit_id';

//cash payments todays visit
// $where2 = $payments_where.' AND payments.payment_method_id = 2 AND payments.payment_type = 1 AND payments.cancel = 0 AND visit.visit_date = "'.$date_tomorrow.'" AND payments.payment_created = "'.$date_tomorrow.'"';
$debtors_rs = $this->reports_model->get_content($debtors_table,$debtors_where,$debtors_select,NULL,NULL,$debtors_order_by);
//$payments_value1 = $this->reports_model->visit_payments1($visit_id,$date_tomorrow);

//$invoice_total1 = $this->reports_model->visit_invoices1($visit_id,$date_tomorrow);

//$balance1 = $payment_value1 - $invoice_total;

// var_dump($debtors_rs->num_rows());die();
$debtors_result3 = '';
$total_invoice3 = 0;
	$total_payment = 0;
$patients = 0;
if($debtors_rs->num_rows() > 0)
{
	$paying_id = 0;
	


	foreach ($debtors_rs->result() as $key => $value_debtor) {
		# code...

		$patient_surname = $value_debtor->patient_surname;
		$patient_othernames = $value_debtor->patient_othernames;
		$payment_method = $value_debtor->payment_method;
		$payment_method_id = $value_debtor->payment_method_id;
		$amount_paid = $value_debtor->amount_paid;
		$patient_phone1 = $value_debtor->patient_phone1;
		$visit_id = $value_debtor->visit_id;

		$patients++;

		// this is to check for any credit note or debit notes
		$payments_value = $this->reports_model->visit_payments($visit_id,$date_tomorrow);

		$invoice_total = $this->reports_model->visit_invoices($visit_id,$date_tomorrow);


		if($payments_value > $invoice_total)
		{
			$balance3 = $payments_value - $invoice_total;
		}
		else
		{
			$balance = 0;
		}
		$total_payment += $payments_value;
		$total_invoice  += $balance;
		$debtors_result3 .= '<tr>
								<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_phone1.'</td>
								<td>Sales Journal (KES)</td>
								<td>'.number_format($balance,2).'</td>
								
							<tr>';
		



	}

	$debtors_result3 .= '<tr>
								<td></strong>TOTAL</strong></td>
								<td></td>
								<td><strong>'.number_format($total_invoice3,2).'</strong></td>
								
							<tr>';
}



$expense_where = '((v_account_ledger_by_date.transactionClassification = "Purchase Payment" AND v_account_ledger_by_date.accountName = "Petty Cash")
									OR (v_account_ledger_by_date.transactionCategory = "Transfer" AND  v_account_ledger_by_date.accountName = "Petty Cash")) AND  v_account_ledger_by_date.transactionDate = "'.$date_tomorrow.'" ';
$expense_table = 'v_account_ledger_by_date';
$expense_select = '*';

$expense_order_by = 'v_account_ledger_by_date.transactionDate';

//cash payments todays visit
// $where2 = $payments_where.' AND payments.payment_method_id = 2 AND payments.payment_type = 1 AND payments.cancel = 0 AND visit.visit_date = "'.$date_tomorrow.'" AND payments.payment_created = "'.$date_tomorrow.'"';
$expense_rs = $this->reports_model->get_content($expense_table,$expense_where,$expense_select,NULL,NULL,$expense_order_by);

// var_dump($expense_rs->num_rows());die();
$expense_result = '';
$total_expense = 0;
	// $total_payment = 0;
if($expense_rs->num_rows() > 0)
{
	$paying_id = 0;
	$x = 0;


	foreach ($expense_rs->result() as $key => $value) {
		# code...


		 $transactionClassification = $value->transactionClassification;

      $document_number = '';
      $transaction_number = '';
      $finance_purchase_description = '';
      $finance_purchase_amount = 0 ;
      if($transactionClassification == 'Purchase Payment')
      {
        $referenceId = $value->payingFor;

        // get purchase details
        $detail = $this->reports_model->get_purchases_details($referenceId);
        $row = $detail->row();
        $document_number = $row->document_number;
        $transaction_number = $row->transaction_number;
        $finance_purchase_description = $row->finance_purchase_description;

      }

       $referenceId = $value->payingFor;
      $document_number =$transaction_number = $value->referenceCode;
      $transactionName = $value->transactionName;
    
      $cr_amount = $value->cr_amount;
      $dr_amount = $value->dr_amount;


      $transaction_date = $value->transactionDate;
      $transaction_date = date('jS M Y',strtotime($transaction_date));
      $creditor_name = $value->creditor_name;
      $creditor_id = 0;//$value->creditor_id;
      $account_name = '';//$value->account_name;
      $finance_purchase_id = '';//$value->finance_purchase_id;
      $finance_purchase_payment_id = $value->transactionId;

		

		$x++;
		$total_expense += $cr_amount;
		$expense_result .= '<tr>
								<td>'.$transaction_number.'</td>
								<td>'.strtoupper($transactionName).'</td>
								<td>PurchaseD Item (KES)</td>
								<td>'.number_format($cr_amount,2).'</td>
								
							<tr>';
		



	}

	$expense_result .= '<tr>
								<td></strong>TOTAL</strong></td>
								<td></td>
								<td></td>
								<td><strong>'.number_format($total_expense,2).'</strong></td>
								
							<tr>';
}


$total_cash_breakdown = 0;
$payment_methods = $this->hospital_reports_model->get_payment_methods();
$payments_result ='';
if($payment_methods->num_rows() > 0)
{
    foreach($payment_methods->result() as $res)
    {
        $method_name = $res->payment_method;
        $payment_method_id = $res->payment_method_id;
        $total = $this->hospital_reports_model->get_amount_collected($payment_method_id);
 
        
         
		$payments_result .='
		<tr>
			<th style="text-align:left">'.strtoupper($method_name).'</th>
			<td>KES. '.number_format($total, 2).'</td>
		</tr>
		';
		$total_cash_breakdown += $total;
    }
    
	$payments_result .= 
					'
					<tr>
						<th style="text-align:left">TOTAL</th>
						<td style="border-top:2px solid #000;">KES.'.number_format($total_cash_breakdown, 2).'</td>
					</tr>
					';
}

 $this->session->set_userdata('branch_id',2);
  $this->session->set_userdata('branch_id',2);
$period_collection =  $this->hospital_reports_model->get_debt_payment_totals(0); 
$debt_collection = $this->hospital_reports_model->get_debt_payment_totals(1); 




echo '<p>Good evening ,<br>
		Herein is a report of todays transactions. This is sent at '.date('H:i:s A').'
		</p>


		<h4 style="text-decoration:underline"><strong>VISIT SUMMARY FOR THE DAY</strong></h4>
		'.$visit_result.'

		<h4 style="text-decoration:underline"><strong>CASH VS INSURANCE SUMMARY WORK DONE FOR TODAY </strong></h4>
		<table  class="table table-hover table-bordered ">
				<thead>
					<tr>
						<th style="padding:5px;">TYPE</th>
						<th style="padding:5px;">INVOICE AMOUNT (KES) </th>
						<th style="padding:5px;">PAYMENTS (KES) </th>
						<th style="padding:5px;">BALANCE (KES) </th>
					
					</tr>
				</thead>
				</tbody>
		  	<tr>
		  		<td>TOTAL CASH INCOME  </td>
		  		<td style="text-align:center;"> '.number_format($total_cash_invoices,2).'</td>
		  		<td style="text-align:center;"> '.number_format($total_cash_payments,2).'</td>
		  		<td style="text-align:center;"> '.number_format($total_cash_balance - $total_waiver,2).'</td>
		  
		  	</tr>
		  	<tr>
		  		<td>TOTAL INSURANCE INCOME</td>
		  		<td style="text-align:center;"> '.number_format($total_insurance_invoices,2).'</td>
		  		<td style="text-align:center;"> '.number_format($total_insurance_payments,2).'</td>
		  		<td style="text-align:center;"> '.number_format($total_insurance_balance,2).'</td>
		  	</tr>

		  	<tr>
		  		<td>TOTAL</td>
		  		<td style="text-align:center;border-top:2px solid #000;"> '.number_format($total_cash_invoices + $total_insurance_invoices,2).'</td>
		  		<td style="text-align:center;border-top:2px solid #000;"> '.number_format($total_cash_payments+$total_insurance_payments,2).'</td>
		  		<td style="text-align:center;border-top:2px solid #000;"> '.number_format(($total_cash_balance+$total_insurance_balance) - $total_waiver,2).'</td>
		  	
		  	</tr>
		  	
		  	</tbody>

		</table>
		

		<h4 style="text-decoration:underline"><strong>PAYMENTS BREAKDOWN </strong></h4>
		
		<table  class="table table-hover table-bordered ">
				<thead>
					<tr>
						<th width="50%"> TYPE</th>
						<th width="50%">AMOUNT</th>
					</tr>
				</thead>
				</tbody>
		  
		  			<tr>
						<td width="50%">DEBT REPAYMENT</td>
						<th width="50%">'.number_format($debt_collection,2).'</th>
					</tr>
					<tr>
						<td width="50%">TODAY\'S PAYMENTS</td>
						<th width="50%">'.number_format($period_collection,2).'</th>
					</tr>
					<tr>
						<th width="50%">TOTAL PAYMENTS</th>
						<th width="50%">'.number_format($period_collection+$debt_collection,2).'</th>
					</tr>
		  		
		
		  		</tbody>

		</table>

	<h4 style="text-decoration:underline"><strong>TOTAL BREAKDOWN FOR THE DAY</strong></h4>
		
		<table  class="table table-hover table-bordered ">
				<thead>
					<tr>
						<th width="50%"> ACCOUNT</th>
						<th width="50%">AMOUNT</th>
					</tr>
				</thead>
				</tbody>
		  
		  			'.$payments_result.'
		  		
		
		  		</tbody>

		</table>
		

		<h4 style="text-decoration:underline"><strong>SERVICES SUMMARY PER DEPARTMENT</strong></h4>
		<table  class="table table-hover table-bordered ">
			<thead>
				<tr>
					<th style="padding:5px;">DEPARTMENT NAME</th>
					<th style="padding:5px;">AMOUNT INVOICED</th>
				</tr>
			</thead>
			</tbody> 
			  	'.$service_result.'
		  	</tbody>
		</table>

		';
?>

<!DOCTYPE html>
<html lang="en">
  
    <body class="receipt_spacing">		

		<h4 style="text-decoration:underline"><strong>EXPENDITURE BREAKDOWN FOR THE DAY</strong></h4>
		<div class="col-print-12">
			<table  class="table table-hover table-bordered ">
			
				<thead>
					<tr>
						<th >Number</th>
						<th >Item Description</th>
						<th >Item</th>
						<th >TOTAL</th>
					</tr>
				</thead>
				</tbody> 
				  	<?php echo $expense_result;?>
			  	</tbody>
			</table>
			
		</div>

		
	
		
	</body>
</html>


