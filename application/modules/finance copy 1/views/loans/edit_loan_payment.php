<?php

	$loan_id = $this->session->userdata('payment_loan_id_searched');
	$all_leases = $this->loans_model->get_loan($loan_id);
  	foreach ($all_leases->result() as $leases_row)
  	{
  		$loan_id = $leases_row->loan_id;
  		$loan_name = $leases_row->loan_name;
      $opening_balance = $leases_row->opening_balance;
  	}
  $expense_accounts = $this->purchases_model->get_child_accounts("Expense Accounts");

  $loan_invoices = $this->loans_model->get_loan_invoice_number($loan_id);

  $loan_payment_details = $this->loans_model->get_loan_payment_details($loan_payment_id);

  if($loan_payment_details->num_rows() > 0)
  {
  	foreach ($loan_payment_details->result() as $key => $value) {
  		# code...

  		$total_amount = $value->total_amount;
  		$transaction_date = $value->transaction_date;
  		$reference_number = $value->reference_number;
  		$document_number = $value->document_number;
  		$account_from_id = $value->account_from_id;

  	}
  }

?>
<div class="row">
  <section class="panel">
      <header class="panel-heading">
          <h3 class="panel-title">Edit Payment </h3>
          <div class="widget-tools">
                <a href="<?php echo site_url();?>loan-statement/<?php echo $loan_id?>" class="btn btn-sm btn-warning pull-right" style="margin-top:-25px;"><i class="fa fa-arrow-left"></i> Back to loan statement</a>
            </div>
      </header>
      <div class="panel-body">
        <?php echo form_open("finance/loans/add_payment_item/".$loan_id.'/'.$loan_payment_id, array("class" => "form-horizontal"));?>


              <input type="hidden" name="type_of_account" value="1">
              <input type="hidden" name="loan_id" id="lease_id" value="<?php echo $loan_id?>">
              <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">

              <div class="col-md-12">
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="col-md-3 control-label">Invoice Number: </label>
                    <div class="col-md-8">
                      <select class="form-control  " name="invoice_id" id="invoice_id" required>
                        <!-- <option value="0">--- select an invoice - ---</option> -->
                        <?php

                        if($loan_invoices->num_rows() > 0)
                        {
                          foreach ($loan_invoices->result() as $key => $value) {
                            // code...
                            $loan_invoice_id = $value->loan_invoice_id;
                            $invoice_number = $value->invoice_number;
                            $loan_invoice_type = $value->loan_invoice_type;
                            $balance = $value->balance;



                            $invoice_type = 0;
                            

                            // var_dump($loan_invoice_id);die();
                            if($balance > 0)
                            {
                               echo '<option value="'.$loan_invoice_id.'.'.$invoice_number.'.'.$invoice_type.'"> #'.$invoice_number.' kes.'.number_format($balance,2).'</option>';
                            }
                           
                          }
                        }
                        ?>
                        <!-- <option value="0.0.3">--- on account - ---</option> -->

                      </select>
                      </div>
                  </div>
                </div>
                <div class="col-md-3">

                  <div class="form-group">
                      <label class="col-md-3 control-label">Account: </label>
                      <div class="col-md-8">
                          <select id="account_to_id" name="account_to_id" class="form-control" required>
                                    
                            <?php
                              $changed = '<option value="0">---- SELECT AN ACCOUNT-----</option>';
                              $accounts = $this->purchases_model->get_all_accounts();
                             if($accounts->num_rows() > 0)
                             {
                                 foreach($accounts->result() as $row):
                                     // $company_name = $row->company_name;
                                     $account_name = $row->account_name;
                                     $account_id = $row->account_id;
                                     $parent_account = $row->parent_account;

                                     if($parent_account != $current_parent)
                                     {
                                        $account_from_name = $this->transfer_model->get_account_name($parent_account);
                                      $changed .= '<optgroup label="'.$account_from_name.'">';
                                     }
                                    
                                    $changed .= "<option value=".$account_id." > ".$account_name."</option>";
                                     
                                     
                                     $current_parent = $parent_account;
                                     if($parent_account != $current_parent)
                                     {
                                      $changed .= '</optgroup>';
                                     }

                                   
                                  
                                 endforeach;
                             }
                             echo $changed;
                             ?>
                              
              
                        </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="col-md-3 control-label">Amount: </label>
                    <div class="col-md-8">
                      <input type="text" class="form-control" name="amount_paid" id="amount_paid" placeholder=""  autocomplete="off" required>
                    </div>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <button class="btn btn-info" type="submit">Add Payment Item </button>
                  </div>
                </div>

              </div>



          <?php echo form_close();?>




    <?php echo form_open("finance/loans/confirm_payment/".$loan_id.'/'.$loan_payment_id, array("class" => "form-horizontal"));?>

      <?php
      $invoice_where = 'loan_payment_item.loan_id = '.$loan_id.' AND loan_payment_item.account_to_id = account.account_id AND loan_payment_item.loan_payment_id ='.$loan_payment_id;
      $invoice_table = 'loan_payment_item,account';
      $invoice_order = 'loan_payment_item_id';

      $invoice_query = $this->loans_model->get_loans_list($invoice_table, $invoice_where, $invoice_order);

      $result_payment ='<hr><table class="table table-bordered table-striped table-condensed">
                          <thead>
                            <tr>
                              <th >#</th>
                              <th >REFERENCE</th>
                              <th >ACCOUNT</th>
                              <th >AMOUNT ALLOCATED</th>
                              <th colspan="1" >Action</th>
                            </tr>
                          </thead>
                            <tbody>';
      $total_amount = 0;
      $total_vat_amount = 0;
      if($invoice_query->num_rows() > 0)
      {
        $x = 0;

        foreach ($invoice_query->result() as $key => $value) {
          // code...
          $loan_payment_item_id = $value->loan_payment_item_id;
          $invoice_type = $value->invoice_type;
          $account_name = $value->account_name;

         
            $type = "loan Bill";
            // loan invoice
            $loan_invoice_id = $value->loan_invoice_id;
            $invoice_where = 'loan_invoice.loan_id = '.$loan_id.' AND loan_invoice_id = '.$loan_invoice_id;
            $invoice_table = 'loan_invoice';
            $invoice_order = 'loan_invoice_id';

            $invoice_items = $this->loans_model->get_loans_list($invoice_table, $invoice_where, $invoice_order);
            $invoice_things = $invoice_items->row();

            $invoice_number = $invoice_things->invoice_number;
          
          $amount = $value->amount_paid;
          $total_amount += $amount;
          $checkbox_data = array(
                    'name'        => 'loan_payments_items[]',
                    'id'          => 'checkbox'.$loan_payment_item_id,
                    'class'          => 'css-checkbox  lrg ',
                    'checked'=>'checked',
                    'value'       => $loan_payment_item_id
                  );

          $x++;
          $result_payment .= '<tr>
                                  <td>'.form_checkbox($checkbox_data).'<label for="checkbox'.$loan_payment_item_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
                                  <td>'.$invoice_number.'</td>
                                  <td>'.$account_name.'</td>
                                  <td>'.number_format($amount,2).'</td>
                                  <td><a href="'.site_url().'delete-loan-payment-item/'.$loan_payment_item_id.'/'.$loan_id.'/'.$loan_payment_id.'" onclick="return confirm("Do you want to remove this entry ? ")" type="submit" class="btn btn-sm btn-danger" ><i class="fa fa-trash"></i></a></td>
                              </tr>';
        }

        // display button

        $display = TRUE;
      }
      else {
        $display = TRUE;
      }

      $result_payment .='</tbody>
                      </table>';
      ?>

      <?php echo $result_payment;?>

      <br>
      <?php
      if($display)
      {
        ?>
        <div class="row">
          <div class="col-md-12">
              <div class="col-md-6">
              </div>
              <div class="col-md-6">
                  <!-- <h2 class="pull-right"> KES. <?php echo number_format($total_amount,2);?></h2> -->

                  <input type="hidden" name="type_of_account" value="1">
                  <input type="hidden" name="loan_id" id="loan_id" value="<?php echo $loan_id;?>">
                  <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
                  <div class="form-group">
                      <label class="col-md-4 control-label">Payment Account: </label>
                      <div class="col-md-8">
                        <select class="form-control select2" name="account_from_id" id="account_from_id"  required>
                          <option value="">---- select a payment account --- </option>
                          <?php
                            $accounts = $this->purchases_model->get_child_accounts("Bank");
                            if($accounts->num_rows() > 0)
                            {
                              foreach ($accounts->result() as $key => $value) {
                                // code...
                                $account_id = $value->account_id;
                                $account_name = $value->account_name;

                                if($account_from_id == $account_id)
                                {
                                	echo '<option value="'.$account_id.'" selected> '.$account_name.'</option>';
                                }
                                else
                                {
                                	echo '<option value="'.$account_id.'"> '.$account_name.'</option>';
                                }
                              }
                            }
                          ?>
                        </select>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Reference Number: </label>
                    <div class="col-md-8">
                      <input type="text" class="form-control" name="reference_number" id="reference_number" placeholder=""  autocomplete="off" value="<?php echo $reference_number?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Total Amount: </label>

                    <div class="col-md-8">
                      <input type="number" class="form-control" name="amount_paid" placeholder=""  autocomplete="off" value="<?php echo $total_amount;?>" readonly>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Payment Date: </label>

                    <div class="col-md-8">
                       <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="payment_date" placeholder="Credit Note Date" id="datepicker" value="<?php echo $transaction_date?>" required>
                        </div>
                    </div>
                  </div>

                <div class="col-md-12">
                    <div class="text-center">
                      <button class="btn btn-info btn-sm " type="submit" onclick="return confirm('Are you sure you want to update payment details ? ')">Update Payment Details </button>
                    </div>
                </div>
              </div>

          </div>
        </div>
        <?php
      }
      ?>

      </div>
      <?php echo form_close();?>
    </section>
</div>