<?php

class providers_model extends CI_Model
{
  /*
  * Retrieve all provider
  * @param string $table
  *   @param string $where
  *
  */
  public function get_all_providers($table, $where, $per_page, $page, $order = 'provider_name', $order_method = 'ASC')
  {
    //retrieve all users
    $this->db->from($table);
    $this->db->select('*');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    $query = $this->db->get('', $per_page, $page);

    return $query;
  }

	/*
	*	Add a new provider
	*
	*/
  public function get_providers_list($table, $where, $order)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order,'asc');
		$query = $this->db->get('');

		return $query;
	}

  /*
	*	get a single provider's details
	*	@param int $provider_id
	*
	*/
	public function get_provider($provider_id)
	{
		//retrieve all users
		$this->db->from('provider');
		$this->db->select('*');
		$this->db->where('provider_id = '.$provider_id);
		$query = $this->db->get();

		return $query;
	}


  public function add_invoice_item($provider_id,$provider_invoice_id)
	{
		$amount = $this->input->post('unit_price');
		$account_to_id=$this->input->post('account_to_id');
    $item_description = $this->input->post('item_description');
		$quantity=$this->input->post('quantity');
    $vat_amount = $this->input->post('vat_amount');
    $total_amount = $this->input->post('total_amount');
		$tax_type_id=$this->input->post('tax_type_id');


		$service = array(
							'provider_invoice_id'=>0,
							'unit_price'=> $amount,
							'account_to_id' => $account_to_id,
							'provider_id' => $provider_id,
              'item_description'=>$item_description,
							'created_by' => $this->session->userdata('personnel_id'),
							'created' => date('Y-m-d'),
              'total_amount'=>$total_amount,
              'vat_amount'=>$vat_amount,
              'quantity'=>$quantity,
              'vat_type_id'=>$tax_type_id
						);
    if(!empty($provider_invoice_id))
    {
      $service['provider_invoice_id'] = $provider_invoice_id;
      $service['provider_invoice_item_status'] = 1;
    }
    else
    {
      $service['provider_invoice_item_status'] = 0;
    }


		$this->db->insert('provider_invoice_item',$service);
		return TRUE;

	}

  public function confirm_provider_invoice($provider_id,$provider_invoice_id = NULL)
	{
		$amount = $this->input->post('amount');
		$amount_charged = $this->input->post('amount_charged');
		$invoice_date = $this->input->post('invoice_date');
    $vat_charged = $this->input->post('vat_charged');
    $invoice_number = $this->input->post('invoice_number');

		$date_check = explode('-', $invoice_date);
		$month = $date_check[1];
		$year = $date_check[0];


		$document_number = $this->create_invoice_number();

		// var_dump($checked); die();

		$insertarray['transaction_date'] = $invoice_date;
		$insertarray['invoice_year'] = $year;
		$insertarray['invoice_month'] = $month;
		$insertarray['provider_id'] = $provider_id;
		$insertarray['document_number'] = $document_number;
    $insertarray['invoice_number'] = strtoupper($invoice_number);
		$insertarray['total_amount'] = $amount_charged;
		$insertarray['vat_charged'] = $vat_charged;
		$insertarray['created_by'] = $this->session->userdata('personnel_id');
		$insertarray['created'] = date('Y-m-d');
		$insertarray['amount'] = $amount;

    if(!empty($provider_invoice_id))
    {
        $this->db->where('provider_invoice_id',$provider_invoice_id);
        if($this->db->update('provider_invoice', $insertarray))
        {

          $total_visits = sizeof($_POST['provider_invoice_items']);
          //check if any checkboxes have been ticked
          if($total_visits > 0)
          {
            for($r = 0; $r < $total_visits; $r++)
            {
              $visit = $_POST['provider_invoice_items'];
              $provider_invoice_item_id = $visit[$r];
              //check if card is held
              $service = array(
                        'provider_invoice_id'=>$provider_invoice_id,
                        'created' =>$invoice_date,
                        'provider_invoice_item_status'=>1,
                        'year'=>$year,
                        'month'=>$month,
                      );
              $this->db->where('provider_invoice_item_id',$provider_invoice_item_id);
              $this->db->update('provider_invoice_item',$service);
            }
          }

          return TRUE;
        }
    }
    else
    {
      if($this->db->insert('provider_invoice', $insertarray))
      {

        $provider_invoice_id = $this->db->insert_id();
        $total_visits = sizeof($_POST['provider_invoice_items']);
        //check if any checkboxes have been ticked
        if($total_visits > 0)
        {
          for($r = 0; $r < $total_visits; $r++)
          {
            $visit = $_POST['provider_invoice_items'];
            $provider_invoice_item_id = $visit[$r];
            //check if card is held
            $service = array(
                      'provider_invoice_id'=>$provider_invoice_id,
                      'created' =>$invoice_date,
                      'provider_invoice_item_status'=>1,
                      'year'=>$year,
                      'month'=>$month,
                    );
            $this->db->where('provider_invoice_item_id',$provider_invoice_item_id);
            $this->db->update('provider_invoice_item',$service);
          }
        }

        return TRUE;
      }
    }
		

	}

  public function create_invoice_number()
	{
		//select product code
		$this->db->where('provider_invoice_id > 0');
		$this->db->from('provider_invoice');
		$this->db->select('MAX(document_number) AS number');
		$this->db->order_by('provider_invoice_id','DESC');
		// $this->db->limit(1);
		$query = $this->db->get();
		// var_dump($query); die();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			// var_dump($number);die();
			$number++;

		}
		else{
			$number = 1;
		}
		// var_dump($number);die();
		return $number;
	}

  public function get_provider_invoice($provider_id,$limit=null)
	{
		$this->db->where('provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id AND provider_invoice.provider_invoice_status = 1 AND provider_invoice.provider_id = '.$provider_id);
		if($limit)
		{
			$this->db->limit($limit);
		}
		$this->db->group_by('provider_invoice.provider_invoice_id');
		$this->db->order_by('provider_invoice.transaction_date','DESC');
		return $this->db->get('provider_invoice_item,provider_invoice');
	}



  public function get_provider_invoice_number($provider_id,$limit=null)
  {
    // $this->db->where('v_providers_invoice_balances.provider_id = '.$provider_id);
    // $this->db->select('*');
    // return $this->db->get('v_providers_invoice_balances');

    $select_statement = "
                        SELECT
                          data.invoice_id AS provider_invoice_id,
                          data.invoice_number AS invoice_number,
                          data.invoice_date AS invoice_date,
                          data.provider_invoice_type AS provider_invoice_type,
                          COALESCE (SUM(data.dr_amount),0) AS dr_amount,
                          COALESCE (SUM(data.cr_amount),0) AS cr_amount,
                          COALESCE (SUM(data.dr_amount),0) - COALESCE (SUM(data.cr_amount),0) AS balance
                        FROM 
                        (
                         

                            SELECT
                            `provider_invoice`.`provider_id` AS provider_id,
                            `provider_invoice`.`provider_invoice_id` AS invoice_id,
                            `provider_invoice`.`invoice_number` AS invoice_number,
                            `provider_invoice`.`transaction_date` AS invoice_date,
                            'provider Bills' AS provider_invoice_type,
                            COALESCE (SUM(`provider_invoice_item`.`total_amount`),0) AS dr_amount,
                            0 AS cr_amount
                            FROM (`provider_invoice`,provider_invoice_item,provider)
                            WHERE `provider_invoice_item`.`provider_invoice_id` = `provider_invoice`.`provider_invoice_id` AND provider_invoice.provider_invoice_status = 1 AND provider.provider_id = provider_invoice.provider_id AND provider_invoice.transaction_date >= provider.start_date
                            GROUP BY `provider_invoice`.`provider_invoice_id`

                            UNION ALL 

                            SELECT
                            `provider_invoice`.`provider_id` AS provider_id,
                            `provider_invoice`.`provider_invoice_id` AS invoice_id,
                            `provider_invoice`.`invoice_number` AS invoice_number,
                            `provider_invoice`.`transaction_date` AS invoice_date,
                            'provider Bills Credit Note' AS provider_invoice_type,
                            0 AS dr_amount,
                            COALESCE (SUM(`provider_credit_note_item`.`credit_note_amount`),0) AS cr_amount
                            FROM (`provider_invoice`,provider_credit_note,provider_credit_note_item)
                            WHERE `provider_credit_note_item`.`provider_credit_note_id` = `provider_credit_note`.`provider_credit_note_id`
                            AND `provider_invoice`.`provider_invoice_id` = `provider_credit_note_item`.`provider_invoice_id` AND provider_credit_note.provider_credit_note_status = 1
                            GROUP BY `provider_credit_note_item`.`provider_invoice_id`

                            UNION ALL


                            SELECT
                            `provider_invoice`.`provider_id` AS provider_id,
                            `provider_invoice`.`provider_invoice_id` AS invoice_id,
                            `provider_invoice`.`invoice_number` AS invoice_number,
                            `provider_invoice`.`transaction_date` AS invoice_date,
                            'Bill Payments' AS provider_invoice_type,
                            0 AS dr_amount,
                            COALESCE (SUM(`provider_payment_item`.`amount_paid`),0) AS cr_amount
                            FROM (provider_payment_item,provider_payment,provider_invoice)
                            WHERE `provider_payment_item`.`provider_invoice_id` = `provider_invoice`.`provider_invoice_id` 
                            AND `provider_payment_item`.`provider_payment_id` = `provider_payment`.`provider_payment_id` AND provider_payment_item.invoice_type = 0 AND provider_payment.provider_payment_status = 1
                            GROUP BY provider_invoice.provider_invoice_id

                            UNION ALL 

                             SELECT
                            `provider`.`provider_id` AS provider_id,
                            `provider`.`provider_id` AS invoice_id,
                            `provider`.`provider_id` AS invoice_number,
                            `provider`.`start_date` AS invoice_date,
                            'Opening Balance' AS provider_invoice_type,
                             COALESCE (SUM(opening_balance),0) AS dr_amount,
                            '0' AS cr_amount
                            FROM (provider)
                            WHERE provider.provider_id > 0
                            GROUP BY provider.provider_id

                            UNION ALL 

                            SELECT
                            `provider`.`provider_id` AS provider_id,
                            `provider`.`provider_id` as invoice_id,
                            `provider`.`provider_id` as invoice_number,
                            `provider`.`start_date` as invoice_date,
                            'Opening Balance Payment' AS provider_invoice_type,
                            0 AS dr_amount,
                            COALESCE (SUM(`provider_payment_item`.`amount_paid`),0) AS cr_amount
                            FROM (provider_payment_item,provider_payment,provider)
                            WHERE `provider_payment_item`.`provider_id` = `provider`.`provider_id` 
                            AND `provider_payment_item`.`provider_payment_id` = `provider_payment`.`provider_payment_id` 
                            AND provider_payment_item.invoice_type = 2 AND provider_payment.provider_payment_status = 1
                            GROUP BY provider.provider_id

                          ) AS data,provider WHERE data.provider_id = ".$provider_id." AND data.provider_id = provider.provider_id AND data.invoice_date >= provider.start_date   GROUP BY data.invoice_number ORDER BY data.invoice_date ASC ";
                          $query = $this->db->query($select_statement);
                  return $query;


  }

  public function add_credit_note_item($provider_id,$provider_credit_note_id)
  {

    $amount = $this->input->post('amount');
		$account_to_id=$this->input->post('account_to_id');
    $description = $this->input->post('description');
		$tax_type_id=$this->input->post('tax_type_id');

    if($tax_type_id == 0)
    {
      $amount = $amount;
      $vat = 0;
    }
    else if($tax_type_id == 1)
    {

      $vat = $amount *0.16;
      $amount = $amount*1.16;
    }
    else if($tax_type_id == 2){

      $vat = $amount*0.05;
      $amount = $amount *1.05;
    }

    // var_dump($amount);die();


		$service = array(
							'provider_id' => $provider_id,
              'account_to_id' => $account_to_id,
              'description'=>$description,
							'created_by' => $this->session->userdata('personnel_id'),
							'created' => date('Y-m-d'),
              'credit_note_amount'=>$amount,
              'credit_note_charged_vat'=>$vat,
              'vat_type_id'=>$tax_type_id
						);

    if(!empty($provider_credit_note_id))
    {
      $service['provider_credit_note_id'] = $provider_credit_note_id;
      $service['provider_credit_note_item_status'] = 1;
    }
    else
    {
       $service['provider_credit_note_item_status'] = 0;
    }

		$this->db->insert('provider_credit_note_item',$service);
		return TRUE;

  }


  public function confirm_provider_credit_note($provider_id,$provider_credit_note_id)
  {
    $amount = $this->input->post('amount');
    $amount_charged = $this->input->post('amount_charged');
    $invoice_date = $this->input->post('credit_note_date');
    $provider_invoice_id = $this->input->post('invoice_id');
    $vat_charged = $this->input->post('vat_charged');
    $invoice_number = $this->input->post('credit_note_number');

    $date_check = explode('-', $invoice_date);
    $month = $date_check[1];
    $year = $date_check[0];


    $document_number = $this->create_credit_note_number();

    // var_dump($checked); die();

    $insertarray['transaction_date'] = $invoice_date;
    $insertarray['invoice_year'] = $year;
    $insertarray['invoice_month'] = $month;
    $insertarray['provider_id'] = $provider_id;
    $insertarray['provider_invoice_id'] = $provider_invoice_id;
    $insertarray['document_number'] = $document_number;
    $insertarray['invoice_number'] = strtoupper($invoice_number);
    $insertarray['total_amount'] = $amount_charged;
    $insertarray['vat_charged'] = $vat_charged;
 
    $insertarray['amount'] = $amount;
    $insertarray['account_from_id'] = 83;


     $total_visits = sizeof($_POST['provider_notes_items']);

     // var_dump($total_visits);die();

     if(!empty($provider_credit_note_id))
     {
        $this->db->where('provider_credit_note_id',$provider_credit_note_id);
        if($this->db->update('provider_credit_note', $insertarray))
        {


          $total_visits = sizeof($_POST['provider_notes_items']);
          //check if any checkboxes have been ticked
          if($total_visits > 0)
          {
            for($r = 0; $r < $total_visits; $r++)
            {
              $visit = $_POST['provider_notes_items'];
              $provider_credit_note_item_id = $visit[$r];
              //check if card is held
              $service = array(
                        'provider_credit_note_id'=>$provider_credit_note_id,
                        'created' =>$invoice_date,
                        'provider_credit_note_item_status'=>1,
                        'provider_invoice_id'=>$provider_invoice_id,
                        'year'=>$year,
                        'month'=>$month,
                      );
              $this->db->where('provider_credit_note_item_id',$provider_credit_note_item_id);
              $this->db->update('provider_credit_note_item',$service);
            }
          }
          return TRUE;
        }
     }
     else
     {

        $insertarray['created_by'] = $this->session->userdata('personnel_id');
        $insertarray['created'] = date('Y-m-d');
        if($this->db->insert('provider_credit_note', $insertarray))
        {
          $provider_credit_note_id = $this->db->insert_id();


          $total_visits = sizeof($_POST['provider_notes_items']);
          //check if any checkboxes have been ticked
          if($total_visits > 0)
          {
            for($r = 0; $r < $total_visits; $r++)
            {
              $visit = $_POST['provider_notes_items'];
              $provider_credit_note_item_id = $visit[$r];
              //check if card is held
              $service = array(
                        'provider_credit_note_id'=>$provider_credit_note_id,
                        'created' =>$invoice_date,
                        'provider_credit_note_item_status'=>1,
                        'provider_invoice_id'=>$provider_invoice_id,
                        'year'=>$year,
                        'month'=>$month,
                      );
              $this->db->where('provider_credit_note_item_id',$provider_credit_note_item_id);
              $this->db->update('provider_credit_note_item',$service);
            }
          }
          return TRUE;
        }
     }

  }

  public function create_credit_note_number()
	{
		//select product code
		$this->db->where('provider_invoice_id > 0');
		$this->db->from('provider_invoice');
		$this->db->select('MAX(document_number) AS number');
		$this->db->order_by('provider_invoice_id','DESC');
		// $this->db->limit(1);
		$query = $this->db->get();
		// var_dump($query); die();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			// var_dump($number);die();
			$number++;

		}
		else{
			$number = 1;
		}
		// var_dump($number);die();
		return $number;
	}


  public function get_provider_credit_notes($provider_id,$limit=null)
	{
		$this->db->where('provider_credit_note.provider_credit_note_id = provider_credit_note_item.provider_credit_note_id AND provider_credit_note.provider_credit_note_status = 1 AND provider_credit_note.provider_id = '.$provider_id);
		if($limit)
		{
			$this->db->limit($limit);
		}
		$this->db->group_by('provider_credit_note.provider_credit_note_id');
		$this->db->order_by('provider_credit_note.transaction_date','DESC');
		return $this->db->get('provider_credit_note_item,provider_credit_note');
	}


  public function get_provider_payments($provider_id,$limit=null)
  {
    $this->db->where('provider_payment.provider_payment_id = provider_payment_item.provider_payment_id AND provider_payment.provider_payment_status = 1 AND provider_payment.provider_id = '.$provider_id);
    if($limit)
    {
      $this->db->limit($limit);
    }
    $this->db->group_by('provider_payment.provider_payment_id');
    $this->db->join('account','account.account_id = provider_payment.account_from_id','left');
    $this->db->order_by('provider_payment.transaction_date','DESC');
    return $this->db->get('provider_payment_item,provider_payment');
  }

  public function add_payment_item($provider_id,$provider_payment_id)
  {

    $amount = $this->input->post('amount_paid');
    $provider_invoice_id = $this->input->post('invoice_id');



    // if(empty($provider_invoice_id))
    // {
    //   $invoice_type = 2;
    // }
    // else
    // {
      $exploded = explode('.', $provider_invoice_id);
      $invoice_id = $exploded[0];
      $invoice_number = $exploded[1];
      $invoice_type = $exploded[2];

    // }

    $service = array(
              'provider_invoice_id'=>$invoice_id,
              'invoice_number'=>$invoice_number,
              'invoice_type'=>$invoice_type,
              'provider_payment_item_status' => 0,
              'provider_payment_id' => 0,
              'provider_id' => $provider_id,
              'created_by' => $this->session->userdata('personnel_id'),
              'created' => date('Y-m-d'),
              'amount_paid'=>$amount,
            );
            // var_dump($service);die();

    if(!empty($provider_payment_id))
    {
      $service['provider_payment_id'] = $provider_payment_id;
      $service['provider_payment_item_status'] = 1;
    }

    $this->db->insert('provider_payment_item',$service);
    return TRUE;

  }

  public function confirm_provider_payment($provider_id,$provider_payment_id)
  {
    $amount_paid = $this->input->post('amount_paid');
    $payment_date = $this->input->post('payment_date');
    $reference_number = $this->input->post('reference_number');
    $account_from_id = $this->input->post('account_from_id');

    $date_check = explode('-', $payment_date);
    $month = $date_check[1];
    $year = $date_check[0];


    // var_dump($year);die();

    if(!empty($provider_payment_id))
    {
     

      // $document_number = $this->create_credit_payment_number();

      $insertarray['transaction_date'] = $payment_date;
      $insertarray['payment_year'] = $year;
      $insertarray['payment_month'] = $month;
      $insertarray['provider_id'] = $provider_id;
      $insertarray['reference_number'] = strtoupper($reference_number);
      $insertarray['total_amount'] = $amount_paid;
      $insertarray['account_from_id'] = $account_from_id;
      $insertarray['created_by'] = $this->session->userdata('personnel_id');
       $this->db->where('provider_payment_id',$provider_payment_id);

      if($this->db->update('provider_payment', $insertarray))
      {


        $total_visits = sizeof($_POST['provider_payments_items']);

        //check if any checkboxes have been ticked
        if($total_visits > 0)
        {
          for($r = 0; $r < $total_visits; $r++)
          {
            $visit = $_POST['provider_payments_items'];
            $provider_payment_item_id = $visit[$r];
            //check if card is held
            $service = array(
                      'provider_payment_id'=>$provider_payment_id,
                      'created' =>$payment_date,
                      'provider_payment_item_status'=>1,
                      'year'=>$year,
                      'month'=>$month,
                    );
            $this->db->where('provider_payment_item_id',$provider_payment_item_id);
            $this->db->update('provider_payment_item',$service);
          }
        }



          return TRUE;
      }

    }

    else
    {
      $document_number = $this->create_credit_payment_number();

      $insertarray['transaction_date'] = $payment_date;
      $insertarray['payment_year'] = $year;
      $insertarray['payment_month'] = $month;
      $insertarray['provider_id'] = $provider_id;
      $insertarray['document_number'] = $document_number;
      $insertarray['reference_number'] = strtoupper($reference_number);
      $insertarray['total_amount'] = $amount_paid;
      $insertarray['account_from_id'] = $account_from_id;
      $insertarray['created_by'] = $this->session->userdata('personnel_id');
      $insertarray['created'] = date('Y-m-d');

      if($this->db->insert('provider_payment', $insertarray))
      {
        $provider_payment_id = $this->db->insert_id();


        $total_visits = sizeof($_POST['provider_payments_items']);

        //check if any checkboxes have been ticked
        if($total_visits > 0)
        {
          for($r = 0; $r < $total_visits; $r++)
          {
            $visit = $_POST['provider_payments_items'];
            $provider_payment_item_id = $visit[$r];
            //check if card is held
            $service = array(
                      'provider_payment_id'=>$provider_payment_id,
                      'created' =>$payment_date,
                      'provider_payment_item_status'=>1,
                      'year'=>$year,
                      'month'=>$month,
                    );
            $this->db->where('provider_payment_item_id',$provider_payment_item_id);
            $this->db->update('provider_payment_item',$service);
          }
        }



        return TRUE;
      }
    }

    
  }

  public function create_credit_payment_number()
  {
    //select product code
    $this->db->where('provider_payment_id > 0');
    $this->db->from('provider_payment');
    $this->db->select('MAX(document_number) AS number');
    $this->db->order_by('provider_payment_id','DESC');
    // $this->db->limit(1);
    $query = $this->db->get();
    // var_dump($query); die();
    if($query->num_rows() > 0)
    {
      $result = $query->result();
      $number =  $result[0]->number;
      // var_dump($number);die();
      $number++;

    }
    else{
      $number = 1;
    }
    // var_dump($number);die();
    return $number;
  }




  /*
  * Add a new provider
  *
  */
  public function add_provider()
  {
    $provider_type_id = $this->input->post('provider_type_id');

    if(isset($provider_type_id))
    {
      $provider_type_id = 1;
    }
    else
    {
      $provider_type_id = 0;
    }
    $data = array(
      'provider_name'=>$this->input->post('provider_name'),
      'provider_email'=>$this->input->post('provider_email'),
      'provider_phone'=>$this->input->post('provider_phone'),
      'provider_location'=>$this->input->post('provider_location'),
      'provider_building'=>$this->input->post('provider_building'),
      'provider_floor'=>$this->input->post('provider_floor'),
      'provider_address'=>$this->input->post('provider_address'),
      'provider_post_code'=>$this->input->post('provider_post_code'),
      'provider_city'=>$this->input->post('provider_city'),
      'opening_balance'=>$this->input->post('opening_balance'),
      'start_date'=>$this->input->post('provider_account_date'),
      'provider_contact_person_name'=>$this->input->post('provider_contact_person_name'),
      'provider_contact_person_onames'=>$this->input->post('provider_contact_person_onames'),
      'provider_contact_person_phone1'=>$this->input->post('provider_contact_person_phone1'),
      'provider_contact_person_phone2'=>$this->input->post('provider_contact_person_phone2'),
      'provider_contact_person_email'=>$this->input->post('provider_contact_person_email'),
      'provider_description'=>$this->input->post('provider_description'),
      'branch_code'=>$this->session->userdata('branch_code'),
      'created_by'=>$this->session->userdata('provider_id'),
      'debit_id'=>$this->input->post('debit_id'),
      'modified_by'=>$this->session->userdata('provider_id'),
      'provider_type_id'=>$provider_type_id,
      'created'=>date('Y-m-d H:i:s')
    );

    if($this->db->insert('provider', $data))
    {
      return $this->db->insert_id();
    }
    else{
      return FALSE;
    }
  }

  /*
  * Update an existing provider
  * @param string $image_name
  * @param int $provider_id
  *
  */
  public function edit_provider($provider_id)
  {
    $data = array(
      'provider_name'=>$this->input->post('provider_name'),
      'provider_email'=>$this->input->post('provider_email'),
      'provider_phone'=>$this->input->post('provider_phone'),
      'provider_location'=>$this->input->post('provider_location'),
      'provider_building'=>$this->input->post('provider_building'),
      'provider_floor'=>$this->input->post('provider_floor'),
      'provider_address'=>$this->input->post('provider_address'),
      'provider_post_code'=>$this->input->post('provider_post_code'),
      'provider_city'=>$this->input->post('provider_city'),
      'opening_balance'=>$this->input->post('opening_balance'),
      'start_date'=>$this->input->post('provider_account_date'),
      'provider_contact_person_name'=>$this->input->post('provider_contact_person_name'),
      'provider_contact_person_onames'=>$this->input->post('provider_contact_person_onames'),
      'provider_contact_person_phone1'=>$this->input->post('provider_contact_person_phone1'),
      'provider_contact_person_phone2'=>$this->input->post('provider_contact_person_phone2'),
      'provider_contact_person_email'=>$this->input->post('provider_contact_person_email'),
      'provider_description'=>$this->input->post('provider_description'),
      'debit_id'=>$this->input->post('debit_id'),
      'modified_by'=>$this->session->userdata('provider_id'),
    );

    $this->db->where('provider_id', $provider_id);
    if($this->db->update('provider', $data))
    {
      return TRUE;
    }
    else{
      return FALSE;
    }
  }


  /*
  * get a single provider's details
  * @param int $provider_id
  *
  */
  public function get_provider_account($provider_id)
  {
    //retrieve all users
    $this->db->from('v_general_ledger');
    $this->db->select('SUM(dr_amount) AS total_invoice_amount');
    $this->db->where('transactionClassification = "providers Invoices" AND recepientId = '.$provider_id);
    $query = $this->db->get();
    $invoices = $query->row();

    $total_invoice_amount = $invoices->total_invoice_amount;


    $this->db->from('v_general_ledger');
    $this->db->select('SUM(cr_amount) AS total_paid_amount');
    $this->db->where('transactionClassification = "providers Invoices Payments" AND recepientId = '.$provider_id);
    $query = $this->db->get();
    $payments = $query->row();

    $total_paid_amount = $payments->total_paid_amount;


    $response['total_invoice'] = $total_invoice_amount;
    $response['total_paid_amount'] = $total_paid_amount;
    $response['total_credit_note'] = 0;

    return $response;
  }



   /*
  * Retrieve all provider
  * @param string $table
  *   @param string $where
  *
  */
  public function get_all_providers_details($table, $where, $per_page, $page, $order = 'provider_name', $order_method = 'ASC')
  {
    //retrieve all users
    $this->db->from($table);
    $this->db->select('*');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    // $this->db->group_by('provider_invoice.provider_invoice_id');
    $query = $this->db->get('', $per_page, $page);

    return $query;
  }


  public function get_provider_invoice_details($provider_invoice_id)
  {

      $this->db->from('provider_invoice');
      $this->db->select('*');
      $this->db->where('provider_invoice_id = '.$provider_invoice_id);
      $query = $this->db->get();
      return $query;
  }

  public function get_provider_payment_details($provider_payment_id)
  {

      $this->db->from('provider_payment');
      $this->db->select('*');
      $this->db->where('provider_payment_id = '.$provider_payment_id);
      $query = $this->db->get();
      return $query;
  }

  public function check_on_account($provider_payment_id)
  {

     $this->db->from('provider_payment_item');
      $this->db->select('*');
      $this->db->where('invoice_type = 3 AND provider_payment_id = '.$provider_payment_id);
      $query = $this->db->get();
      if($query->num_rows() > 0)
      {
          return TRUE;
      }
      else
      {
        return FALSE;
      }


  }

  public function get_provider_credit_note_details($provider_credit_note_id)
  {

      $this->db->from('provider_credit_note');
      $this->db->select('*');
      $this->db->where('provider_credit_note_id = '.$provider_credit_note_id);
      $query = $this->db->get();
      return $query;
  }
  
  public function get_content($table, $where,$select,$group_by=NULL,$limit=NULL)
  {
    $this->db->from($table);
    $this->db->select($select);
    $this->db->where($where);
    if($group_by != NULL)
    {
      $this->db->group_by($group_by);
    }
    $query = $this->db->get('');
    
    return $query;
  }

   public function export_providers()
  {
    $this->load->library('excel');
    

    $income_rs = $this->company_financial_model->get_providers_aging_report();
    
    
    $title = 'Providers List';

    $income_result = '';
    $total_income = 0;
    $total_income = 0;
    $total_thirty = 0;
    $total_sixty = 0;
    $total_ninety = 0;
    $total_over_ninety = 0;
    $total_coming_due = 0;
    $grand_total = 0;
    $total_unallocated = 0;
    $grand_dr =0;
    $grand_cr = 0;
    
    if($income_rs->num_rows() > 0)
    {
      $count = 0;
      /*
        -----------------------------------------------------------------------------------------
        Document Header
        -----------------------------------------------------------------------------------------
      */


      $row_count = 0;
      $report[$row_count][0] = '#';
      $report[$row_count][1] = 'Doctor';
      $report[$row_count][2] = 'Coming Due';
      $report[$row_count][3] = '1 - 30 Days';
      $report[$row_count][4] = '31 - 60 Days';
      $report[$row_count][5] = '61 - 90 Days';
      $report[$row_count][6] = 'Over 90 Days';
      $report[$row_count][7] = 'Unallocated Funds';
      $report[$row_count][8] = 'Total Debits';
      $report[$row_count][9] = 'Total Credits';
      $report[$row_count][10] = 'Balance';
      //get & display all services
      
      //display all patient data in the leftmost columns
      foreach($income_rs->result() as $value)
      {
        $row_count++;
        $total_invoiced = 0;
        # code...
        // $total_amount = $value->total_amount;
        $payables = $value->payables;
        $thirty_days = $value->thirty_days;
        $sixty_days = $value->sixty_days;
        $ninety_days = $value->ninety_days;
        $over_ninety_days = $value->over_ninety_days;
        $coming_due = $value->coming_due;
        $provider_id = $value->recepientId;
        $Total = $value->Total;
        $total_dr = $value->total_dr;
        $total_cr = $value->total_cr;

        $unallocated = $this->company_financial_model->get_unallocated_funds($provider_id);
        $total_thirty += $thirty_days;
        $total_sixty += $sixty_days;
        $total_ninety += $ninety_days;
        $total_over_ninety += $over_ninety_days;
        $total_coming_due += $coming_due;
        $grand_total += $Total;
        $total_unallocated += $unallocated;
        $grand_dr += $total_dr;
        $grand_cr += $total_cr;

        $balance = $total_dr - $total_cr;
        $Total = $balance;
        $grand_total += $Total;


        $count++;
        
        //display the patient data
        $report[$row_count][0] = $count;
        $report[$row_count][1] = $payables;
        $report[$row_count][2] = number_format($coming_due,2);
        $report[$row_count][3] = number_format($thirty_days,2);
        $report[$row_count][4] = number_format($sixty_days,2);
        $report[$row_count][5] = number_format($ninety_days,2);
        $report[$row_count][6] = number_format($over_ninety_days,2);
        $report[$row_count][7] = number_format($unallocated,2);
        $report[$row_count][8] = number_format($total_dr,2);
        $report[$row_count][9] = number_format($total_cr,2);
        $report[$row_count][10] = number_format($balance,2);
          
        
        
      }
    }
    
    //create the excel document
    $this->excel->addArray ( $report );
    $this->excel->generateXML ($title);
  }

}
?>
