<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once "./application/modules/admin/controllers/admin.php";


class loans extends admin
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('loans_model');
		$this->load->model('purchases_model');
    $this->load->model('transfer_model');
    $this->load->model('financials/company_financial_model');
	}



  public function loans_invoices()
  {
    // $v_data['property_list'] = $property_list;


   
    $loan_id = $this->session->userdata('invoice_loan_id_searched');


    $where = 'loan_invoice.loan_invoice_status = 1 AND loan_invoice.loan_id = '.$loan_id;

    $search_purchases = $this->session->userdata('search_purchases');
    if($search_purchases)
    {
      $where .= $search_purchases;
    }
    $table = 'loan_invoice';


    $segment = 3;
    $this->load->library('pagination');
    $config['base_url'] = site_url().'accounting/loan-invoices';
    $config['total_rows'] = $this->purchases_model->count_items($table, $where);
    $config['uri_segment'] = $segment;
    $config['per_page'] = 20;
    $config['num_links'] = 5;

    $config['full_tag_open'] = '<ul class="pagination pull-right">';
    $config['full_tag_close'] = '</ul>';

    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';

    $config['next_tag_open'] = '<li>';
    $config['next_link'] = 'Next';
    $config['next_tag_close'] = '</span>';

    $config['prev_tag_open'] = '<li>';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $this->pagination->initialize($config);

    $page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
    $query = $this->loans_model->get_all_loans_details($table, $where, $config["per_page"], $page, $order='loan_invoice.transaction_date', $order_method='DESC');
    $v_data['loan_invoices'] = $query;
    $v_data['page'] = $page;
    $data['title'] = 'loan Invoices';
    $v_data['title'] = $data['title'];
    $data['content'] = $this->load->view('loans/loans_statement', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
  }


  public function search_loans_invoice()
  {
    // var_dump($_POST);die();
    $loan_id = $loan_id_searched = $this->input->post('loan_id');

    $search_title = '';

    if(!empty($loan_id))
    {

      $loan_id = ' AND loan.loan_id = '.$loan_id.' ';
    }
    $search = $loan_id;

    $this->session->set_userdata('invoice_loan_id_searched', $loan_id_searched);
    $this->session->set_userdata('search_loans_invoice', $search);
    redirect('accounting/loan-invoices');
  }

  public function search_loans_bill($loan_id)
  {

    $loan_id = $loan_id_searched = $loan_id;

    $search_title = '';

    if(!empty($loan_id))
    {

      $loan_id = ' AND loan.loan_id = '.$loan_id.' ';
    }
    $search = $loan_id;

    $this->session->set_userdata('invoice_loan_id_searched', $loan_id_searched);
    $this->session->set_userdata('search_loans_invoice', $search);
    redirect('accounting/loan-invoices');
  }

  public function close_searched_invoices_loan()
  {
    $this->session->unset_userdata('invoice_loan_id_searched');
    $this->session->unset_userdata('search_loans_invoice');
    redirect('accounting/loans');
  }
  public function calculate_value()
  {
    $quantity = $this->input->post('quantity');
    $tax_type_id = $this->input->post('tax_type_id');
    $unit_price = $this->input->post('unit_price');


    if(empty($quantity))
    {
      $quantity = 1;
    }
    if(empty($unit_price))
    {
      $unit_price = 0;
    }
    if(empty($tax_type_id))
    {
      $tax_type_id = 0;
    }

    if($tax_type_id == 0)
    {
      $total_amount = $unit_price *$quantity;
      $vat = 0;
    }
    if($tax_type_id == 1)
    {
      $total_amount = ($unit_price * $quantity)*1.16;
      $vat = ($unit_price * $quantity)*0.16;
    }
    if($tax_type_id == 2)
    {
      $total_amount = ($unit_price * $quantity)*1.05;
      $vat = ($unit_price * $quantity)*0.05;
    }

    if($tax_type_id == 3)
    {
      $total_amount = ($unit_price * $quantity);
      $vat = ($unit_price * $quantity)*0.16;
    }

    $response['message'] = 'success';
    $response['amount'] = $total_amount;
    $response['vat'] = $vat;

    echo json_encode($response);


  }

  public function add_invoice_item($loan_id,$loan_invoice_id = NULL)
  {

    $this->form_validation->set_rules('quantity', 'Invoice Item', 'trim|required|xss_clean');
    $this->form_validation->set_rules('unit_price', 'Unit Price', 'trim|required|xss_clean');
    $this->form_validation->set_rules('account_to_id', 'Expense Account', 'trim|required|xss_clean');
    $this->form_validation->set_rules('item_description', 'Item', 'trim|required|xss_clean');
    $this->form_validation->set_rules('tax_type_id', 'VAT Type', 'trim|xss_clean');
    $this->form_validation->set_rules('vat_amount', 'VAT Amount', 'trim|xss_clean');
    $this->form_validation->set_rules('total_amount', 'Total Amount', 'trim|xss_clean');

    //if form conatins invalid data
    if ($this->form_validation->run())
    {
			// var_dump($_POST);die();
      $this->loans_model->add_invoice_item($loan_id,$loan_invoice_id);
      $this->session->set_userdata("success_message", 'Invoice Item successfully added');
      $response['status'] = 'success';
      $response['message'] = 'Payment successfully added';
    }
    else
    {
      $this->session->set_userdata("error_message", validation_errors());
      $response['status'] = 'fail';
      $response['message'] = validation_errors();

    }
    $redirect_url = $this->input->post('redirect_url');
    redirect($redirect_url);

  }

  public function confirm_invoice_note($loan_id,$loan_invoice_id = NULL)
  {
    $this->form_validation->set_rules('vat_charged', 'tax', 'trim|xss_clean');
		$this->form_validation->set_rules('amount_charged', 'Amount Charged', 'trim|xss_clean');
    $this->form_validation->set_rules('invoice_date', 'Invoice Date ', 'trim|required|xss_clean');
    $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
    $this->form_validation->set_rules('invoice_number', 'Invoice Number', 'trim|xss_clean');
		if ($this->form_validation->run())
		{
			// var_dump($_POST);die();
				$this->loans_model->confirm_loan_invoice($loan_id,$loan_invoice_id);

				$this->session->set_userdata("success_message", 'loan invoice successfully added');
				$response['status'] = 'success';
				$response['message'] = 'Payment successfully added';
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();

		}


    if(!empty($loan_invoice_id))
    {
        redirect('accounting/loan-invoices');
    }
    else
    {
      $redirect_url = $this->input->post('redirect_url');
      redirect($redirect_url);
    }

        
  }


  // credit notes

  public function loans_credit_note()
  {
    // $v_data['property_list'] = $property_list;


     $loan_id = $this->session->userdata('credit_note_loan_id_searched');


    $where = 'loan_credit_note.loan_credit_note_status AND loan_credit_note.loan_id = '.$loan_id;

    $search_purchases = $this->session->userdata('search_purchases');
    if($search_purchases)
    {
      $where .= $search_purchases;
    }
    $table = 'loan_credit_note';


    $segment = 3;
    $this->load->library('pagination');
    $config['base_url'] = site_url().'accounting/loan-credit-notes';
    $config['total_rows'] = $this->purchases_model->count_items($table, $where);
    $config['uri_segment'] = $segment;
    $config['per_page'] = 20;
    $config['num_links'] = 5;

    $config['full_tag_open'] = '<ul class="pagination pull-right">';
    $config['full_tag_close'] = '</ul>';

    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';

    $config['next_tag_open'] = '<li>';
    $config['next_link'] = 'Next';
    $config['next_tag_close'] = '</span>';

    $config['prev_tag_open'] = '<li>';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $this->pagination->initialize($config);

    $page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
    $query = $this->loans_model->get_all_loans_details($table, $where, $config["per_page"], $page, $order='loan_credit_note.transaction_date', $order_method='DESC');
    $v_data['loan_credit_notes'] = $query;
    $v_data['page'] = $page;

    $data['title'] = 'loan Credit Notes';
    $v_data['title'] = $data['title'];
    $data['content'] = $this->load->view('loans/loans_credit_notes', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
  }


  public function search_loans_credit_notes($loan_id=null)
  {
    if(!empty($loan_id))
    {
      $loan_id = $loan_id_searched = $loan_id;
    }
    else
    {
      $loan_id = $loan_id_searched = $this->input->post('loan_id');
    }



    $search_title = '';

    if(!empty($loan_id))
    {

      $loan_id = ' AND loan.loan_id = '.$loan_id.' ';
    }
    $search = $loan_id;
      // var_dump($loan_id_searched);die();
    $this->session->set_userdata('credit_note_loan_id_searched', $loan_id_searched);
    $this->session->set_userdata('search_loans_credit_notes', $search);
    redirect('accounting/loan-credit-notes');
  }

  public function close_searched_credit_notes_loan()
  {
    $this->session->unset_userdata('credit_note_loan_id_searched');
    $this->session->unset_userdata('search_loans_credit_notes');
    redirect('accounting/loans');
  }

  public function add_credit_note_item($loan_id,$loan_credit_note_id=NULL)
  {

    $this->form_validation->set_rules('amount', 'Unit Price', 'trim|required|xss_clean');
    $this->form_validation->set_rules('account_to_id', 'Invoice', 'trim|required|xss_clean');
    $this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean');
    $this->form_validation->set_rules('tax_type_id', 'VAT Type', 'trim|xss_clean');

    //if form conatins invalid data
    if ($this->form_validation->run())
    {
      $this->loans_model->add_credit_note_item($loan_id,$loan_credit_note_id);
      $this->session->set_userdata("success_message", 'Invoice Item successfully added');
      $response['status'] = 'success';
      $response['message'] = 'Payment successfully added';
    }
    else
    {
      $this->session->set_userdata("error_message", validation_errors());
      $response['status'] = 'fail';
      $response['message'] = validation_errors();

    }
    $redirect_url = $this->input->post('redirect_url');
    redirect($redirect_url);

  }

  public function confirm_credit_note($loan_id,$loan_credit_note_id=NULL)
  {
    $this->form_validation->set_rules('vat_charged', 'tax', 'trim|xss_clean');
		$this->form_validation->set_rules('amount_charged', 'Amount Charged', 'trim|xss_clean');
    $this->form_validation->set_rules('credit_note_date', 'Invoice Date ', 'trim|required|xss_clean');
    $this->form_validation->set_rules('invoice_id', 'Invoice ', 'trim|required|xss_clean');
    $this->form_validation->set_rules('amount', 'Amount', 'trim|xss_clean');
    $this->form_validation->set_rules('credit_note_number', 'Invoice Number', 'trim|xss_clean');

		if ($this->form_validation->run())
		{
        // var_dump($_POST);die();
				$this->loans_model->confirm_loan_credit_note($loan_id,$loan_credit_note_id);

				$this->session->set_userdata("success_message", 'loan invoice successfully added');
				$response['status'] = 'success';
				$response['message'] = 'Payment successfully added';
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();

		}

    if(!empty($loan_credit_note_id))
    {
     
      redirect('accounting/loan-credit-notes');
    }
    else
    {
      $redirect_url = $this->input->post('redirect_url');
      redirect($redirect_url);
    }
  }

  // loans payments_import


  public function loans_payments()
  {
    // $v_data['property_list'] = $property_list;
      $loan_id = $this->session->userdata('payment_loan_id_searched');


    $where = 'loan_payment.loan_payment_status = 1 AND account.account_id = loan_payment.account_from_id AND loan_payment.loan_id = '.$loan_id;

    $search_purchases = $this->session->userdata('search_purchases');
    if($search_purchases)
    {
      $where .= $search_purchases;
    }
    $table = 'loan_payment,account';

 // $this->db->join('account','account.account_id = loan_payment.account_from_id','left');

    $segment = 3;
    $this->load->library('pagination');
    $config['base_url'] = site_url().'accounting/loan-payments';
    $config['total_rows'] = $this->purchases_model->count_items($table, $where);
    $config['uri_segment'] = $segment;
    $config['per_page'] = 20;
    $config['num_links'] = 5;

    $config['full_tag_open'] = '<ul class="pagination pull-right">';
    $config['full_tag_close'] = '</ul>';

    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';

    $config['next_tag_open'] = '<li>';
    $config['next_link'] = 'Next';
    $config['next_tag_close'] = '</span>';

    $config['prev_tag_open'] = '<li>';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $this->pagination->initialize($config);

    $page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
    $query = $this->loans_model->get_all_loans_details($table, $where, $config["per_page"], $page, $order='loan_payment.transaction_date', $order_method='DESC');
    $v_data['loan_payments'] = $query;
    $v_data['page'] = $page;
    $data['title'] = 'loan Payments';

    $v_data['title'] = $data['title'];
    $data['content'] = $this->load->view('loans/loans_payments', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
  }


  public function search_loans_payments($loan_id=null)
  {
    if(!empty($loan_id))
    {
       $loan_id = $loan_id_searched = $loan_id;
    }
    else
    {
       $loan_id = $loan_id_searched = $this->input->post('loan_id');
    }



    $search_title = '';

    if(!empty($loan_id))
    {

      $loan_id = ' AND loan.loan_id = '.$loan_id.' ';
    }
    $search = $loan_id;
      // var_dump($loan_id_searched);die();
    $this->session->set_userdata('payment_loan_id_searched', $loan_id_searched);
    $this->session->set_userdata('search_loans_payments', $search);
    redirect('accounting/loan-payments');
  }

  public function close_searched_payments_loan()
  {
    $this->session->unset_userdata('payment_loan_id_searched');
    $this->session->unset_userdata('search_loans_payments');
    redirect('accounting/loans');
  }
  public function add_payment_item($loan_id,$loan_payment_id = NULL)
  {

    $this->form_validation->set_rules('amount_paid', 'Unit Price', 'trim|required|xss_clean');
    $this->form_validation->set_rules('account_to_id', 'Invoice', 'trim|required|xss_clean');

    //if form conatins invalid data
    if ($this->form_validation->run())
    {

      $this->loans_model->add_payment_item($loan_id,$loan_payment_id);
      $this->session->set_userdata("success_message", 'Payment successfully added');
      $response['status'] = 'success';
      $response['message'] = 'Payment successfully added';
    }
    else
    {
      $this->session->set_userdata("error_message", validation_errors());
      $response['status'] = 'fail';
      $response['message'] = validation_errors();

    }
    $redirect_url = $this->input->post('redirect_url');
    redirect($redirect_url);

  }

  public function confirm_payment($loan_id,$loan_payment_id=NULL)
  {
    $this->form_validation->set_rules('reference_number', 'Reference Number', 'trim|required|xss_clean');
    $this->form_validation->set_rules('payment_date', 'Invoice Date ', 'trim|required|xss_clean');
    $this->form_validation->set_rules('amount_paid', 'Amount', 'trim|required|xss_clean');
    $this->form_validation->set_rules('account_from_id', 'Payment Amount', 'trim|required|xss_clean');

    if ($this->form_validation->run())
    {
        // var_dump($_POST);die();


        $status = $this->loans_model->confirm_loan_payment($loan_id,$loan_payment_id);

        if($status == TRUE)
        {

          if(!empty($loan_payment_id))
          {
            $this->session->set_userdata("success_message", 'Payment has been successfully updated');
            $response['status'] = 'success';
            $response['message'] = 'Payment successfully added';
            $redirect_url = $this->input->post('redirect_url');
            redirect('loan-statement/'.$loan_id);

          }
          else
          {
            $this->session->set_userdata("success_message", 'loan invoice successfully added');
            $response['status'] = 'success';
            $response['message'] = 'Payment successfully added';
            $redirect_url = $this->input->post('redirect_url');
            redirect($redirect_url);
          }

        }
        else
        {
          $this->session->set_userdata("success_message", 'Sorry could not make the update. Please try again');
          $response['status'] = 'success';
          $response['message'] = 'Payment successfully added';
          $redirect_url = $this->input->post('redirect_url');
          redirect($redirect_url);
        }
        

    }
    else
    {
      $this->session->set_userdata("error_message", validation_errors());
      $response['status'] = 'fail';
      $response['message'] = validation_errors();

    }


    $redirect_url = $this->input->post('redirect_url');
    redirect($redirect_url);

    


  }


  /*
  *
  * Add a new loan
  *
  */
  public function add_loan()
  {
    //form validation rules
    $this->form_validation->set_rules('loan_name', 'Name', 'required|xss_clean');
    $this->form_validation->set_rules('loan_description', 'Description', 'xss_clean');
    $this->form_validation->set_rules('account_to_id', 'Account To','xss_clean');
    $this->form_validation->set_rules('account_from_id', 'Account From','xss_clean');
    $this->form_validation->set_rules('loan_account_date', 'Loan Date','xss_clean');

    // var_dump($_POST); die();
    //if form conatins invalid data
    if ($this->form_validation->run())
    {
      $loan_id = $this->loans_model->add_loan();
      if($loan_id > 0)
      {
        $this->session->set_userdata("success_message", "loan added successfully");
        $redirect_url = $this->input->post('redirect_url');
        if(!empty($redirect_url))
        {
          redirect($redirect_url);
        }
        else
        {
          redirect('accounting/loans');
        }
      }

      else
      {
        $this->session->set_userdata("error_message","Could not add loan. Please try again");

        $redirect_url = $this->input->post('redirect_url');
        if(!empty($redirect_url))
        {
          redirect($redirect_url);
        }
        else
        {
          redirect('accounting/loans');
        }
      }
    }
    $data['title'] = 'Add loan';
    $v_data['title'] = $data['title'];
    $data['content'] = $this->load->view('loans/add_loan', $v_data, true);

    $this->load->view('admin/templates/general_page', $data);
  }

  /*
  *
  * Add a new loan
  *
  */
  public function edit_loan($loan_id)
  {
    //form validation rules
    $this->form_validation->set_rules('loan_name', 'Name', 'required|xss_clean');
    $this->form_validation->set_rules('loan_description', 'Description', 'xss_clean');
    $this->form_validation->set_rules('account_to_id', 'Account To','xss_clean');
    $this->form_validation->set_rules('account_from_id', 'Account From','xss_clean');
    $this->form_validation->set_rules('loan_account_date', 'Loan Date','xss_clean');

    //if form conatins invalid data
    if ($this->form_validation->run())
    {
      $loan_id = $this->loans_model->edit_loan($loan_id);
      if($loan_id > 0)
      {
        $this->session->set_userdata("success_message", "loan updated successfully");
        redirect('accounting/loans');
      }

      else
      {
        $this->session->set_userdata("error_message","Could not add loan. Please try again");
        redirect('finance/edit-loan/'.$loan_id);
      }
    }
    $data['title'] = 'Add loan';
    $v_data['title'] = $data['title'];
    $v_data['loan'] = $this->loans_model->get_loan($loan_id);
    $data['content'] = $this->load->view('loans/edit_loan', $v_data, true);

    $this->load->view('admin/templates/general_page', $data);
  }

  public function transfer_loan_bills()
  {
    $this->db->from('account_invoices');
    $this->db->select('*');
    $this->db->where('account_to_type = 2 AND account_invoice_deleted = 0 AND account_from_id > 0 and sync_status = 0');

    $query = $this->db->get();
    // var_dump($query);die();
    if($query->num_rows() > 0)
    {
      foreach ($query->result() as $key => $value) {
        # code...
        $invoice_amount = $value->invoice_amount;
        $account_to_id = $value->account_to_id;
        $account_invoice_status = $value->account_invoice_status;
        $account_invoice_description = $value->account_invoice_description;
        $account_from_id = $value->account_from_id;
        $created = $value->created;
        $created_by = $value->created_by;
        $invoice_number = $value->invoice_number;
        $department_id = $value->department_id;
        $invoice_date = $value->invoice_date;
        $account_invoice_id = $value->account_invoice_id;

        $exploded = explode('-', $invoice_date);

        $year = $exploded[0];
        $month = $exploded[1];

        $document_number = $this->loans_model->create_invoice_number();

        $invoice['amount'] = $invoice_amount;
        $invoice['loan_invoice_status'] = 1;
        $invoice['vat_charged'] = 0;
        $invoice['total_amount'] = $invoice_amount;
        $invoice['created'] = $created;
        $invoice['created_by'] = $created_by;
        $invoice['transaction_date'] = $invoice_date;
        $invoice['invoice_number'] = $invoice_number;
        $invoice['document_number'] = $document_number;
        $invoice['loan_id'] = $account_from_id;
        $invoice['invoice_year'] = $year;
        $invoice['invoice_month'] = $month;


        $this->db->insert('loan_invoice',$invoice);
        $loan_invoice_id = $this->db->insert_id();


        $invoice_item['loan_invoice_id'] = $loan_invoice_id;
        $invoice_item['item_description'] = $account_invoice_description;
        $invoice_item['unit_price'] = $invoice_amount;
        $invoice_item['total_amount'] = $invoice_amount;
        $invoice_item['created'] = $created;
        $invoice_item['created_by'] = $created_by;
        $invoice_item['quantity'] = 1;
        $invoice_item['year'] = $year;
        $invoice_item['month'] = $month;
        $invoice_item['loan_id'] = $account_from_id;
        $invoice_item['account_to_id'] = $account_from_id;
        $this->db->insert('loan_invoice_item',$invoice_item);

        $update_item['sync_status'] = 1;
        $this->db->where('account_invoice_id',$account_invoice_id);
        $this->db->update('account_invoices',$update_item);


      }
    }
  }



  public function transfer_loan_payments()
  {
    $this->db->from('account_payments');
    $this->db->select('*');
    $this->db->where('account_to_type = 2 AND account_payment_deleted = 0 AND account_to_id > 0 AND sync_status = 0');

    $query = $this->db->get();
    // var_dump($query);die();
    if($query->num_rows() > 0)
    {
      foreach ($query->result() as $key => $value) {
        # code...
        $amount_paid = $value->amount_paid;
        $account_to_id = $value->account_to_id;
        $account_payment_status = $value->account_payment_status;
        $account_payment_description = $value->account_payment_description;
        $account_from_id = $value->account_from_id;
        $created = $value->created;
        $created_by = $value->created_by;
        $receipt_number = $value->receipt_number;
        $payment_to = $value->payment_to;
        $payment_date = $value->payment_date;
        $account_payment_id = $value->account_payment_id;


        $exploded = explode('-', $payment_date);

        $year = $exploded[0];
        $month = $exploded[1];

        $document_number = $this->loans_model->create_credit_payment_number();

        $invoice['total_amount'] = $amount_paid;
        $invoice['loan_payment_status'] = 1;
        $invoice['created'] = $created;
        $invoice['created_by'] = $created_by;
        $invoice['transaction_date'] = $payment_date;
        $invoice['reference_number'] = $receipt_number;
        $invoice['document_number'] = $document_number;
        $invoice['loan_id'] = $account_to_id;
        $invoice['account_from_id'] = $account_from_id;
        $invoice['payment_year'] = $year;
        $invoice['payment_month'] = $month;


        $this->db->insert('loan_payment',$invoice);
        $loan_payment_id = $this->db->insert_id();

        $invoice_item['loan_payment_id'] = $loan_payment_id;
        $invoice_item['description'] = $account_payment_description;
        $invoice_item['amount_paid'] = $amount_paid;
        $invoice_item['created'] = $created;
        $invoice_item['created_by'] = $created_by;
        $invoice_item['year'] = $year;
        $invoice_item['month'] = $month;
        $invoice_item['loan_id'] = $account_to_id;
        $invoice_item['invoice_type'] = 2;
        $this->db->insert('loan_payment_item',$invoice_item);

          $update_item['sync_status'] = 1;
        $this->db->where('account_payment_id',$account_payment_id);
        $this->db->update('account_payments',$update_item);
      }
    }
  }
	public function merge_credit_notes()
	{

		$this->db->select('*');
		$this->db->where('is_store = 3 and supplier_id > 0');
		$query = $this->db->get('orders');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$supplier_invoice_number = $value->supplier_invoice_number;
				$order_id = $value->order_id;

				$this->db->select('*');
				$this->db->where('is_store = 0 and supplier_id > 0 and supplier_invoice_number = "'.$supplier_invoice_number.'"');
				$this->db->limit(1);
				$query2 = $this->db->get('orders');
				if($query2->num_rows() == 1)
				{
					// update the reference_id
					$rows = $query2->row();
					$reference_id = $rows->order_id;

					$update_array['reference_id'] = $reference_id;
					$this->db->where('is_store = 3 and order_id ='.$order_id);
					$this->db->update('orders',$update_array);
				}
			}
		}
	}




  public function loans_list()
  {
    $data['title'] = 'Vendor Expenses';
    $v_data['title'] = $data['title'];

		// var_dump($v_date); die();
    $data['content'] = $this->load->view('loans/loans_accounts', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
  }
	public function delete_loan_payment_item($loan_payment_item_id,$loan_id,$loan_payment_id = NULL)
	{
		$this->db->where('loan_payment_item_id',$loan_payment_item_id);
		$this->db->delete('loan_payment_item');

    if(!empty($loan_payment_id))
    {
        redirect('edit-loan-payment/'.$loan_payment_id);
    }
		else
    {
      redirect('accounting/loan-payments');
    }
	}


	public function delete_loan_invoice_item($loan_invoice_item_id,$loan_id,$loan_invoice_id = NUll)
	{
		$this->db->where('loan_invoice_item_id',$loan_invoice_item_id);
		$this->db->delete('loan_invoice_item');

    if(!empty($loan_invoice_id))
    {
        redirect('loan-invoice/edit-loan-invoice/'.$loan_invoice_id);
    }
    else
    {
          redirect('accounting/loan-invoices');
    }
		
	}


  public function allocate_loan_payment($loan_payment_id,$loan_payment_item_id,$loan_id)
  {


    $data['title'] = 'Allocating loan Payment';
    $v_data['title'] = $data['title'];
    $v_data['loan_id'] = $loan_id;
    $v_data['loan_payment_id'] = $loan_payment_id;
    $v_data['loan_payment_item_id'] = $loan_payment_item_id;


    // var_dump($v_date); die();
    $data['content'] = $this->load->view('loans/allocate_loan_payment', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
  }


  public function delete_payment_item($loan_payment_id,$loan_payment_item_id,$loan_payment_item_id_db,$loan_id)
  {

    // var_dump($loan_payment_item_id_db);die();
    $this->db->where('loan_payment_item_id',$loan_payment_item_id_db);
    $this->db->delete('loan_payment_item');

    redirect('allocate-payment/'.$loan_payment_id.'/'.$loan_payment_item_id.'/'.$loan_id);
  }
  

  public function search_loans()
  {
    $loan_name = $year_from = $this->input->post('loan_name');
    $redirect_url = $this->input->post('redirect_url');
    $loan_search = '';
     if(!empty($loan_name))
    {
      $loan_search .= 'loans LIKE \'%'.$loan_name.'%\'';

    }

    $search = $loan_search;

    

    // var_dump($date_to); die();
    $this->session->set_userdata('loan_search',$search);
    redirect($redirect_url);

  }
  public function close_loan_loan_search()
  {

    $this->session->unset_userdata('loan_search');


    redirect('accounting/loans');

  }

  public function get_invoice_details($loan_invoice_id)
  {
    $data['loan_invoice_id'] = $loan_invoice_id;
    $this->load->view('loans/view_loan_invoice', $data);  
  }


  public function get_suppliers_invoice_details($order_id)
  {
    $data['order_id'] = $order_id;
    $this->load->view('loans/view_supplier_invoice', $data);  
  }
  public function get_payment_details($loan_payment_id)
  {
    $data['loan_payment_id'] = $loan_payment_id;
    $this->load->view('loans/view_loan_payment', $data);  
  }

  public function delete_loan_invoice($loan_invoice_id)
  {
    $update_query['loan_invoice_status'] = 2;
    $update_query['last_modified_by'] = $this->session->userdata('personnel_id');
    $this->db->where('loan_invoice_id',$loan_invoice_id);
    $this->db->update('loan_invoice',$update_query);

    redirect('accounting/loan-invoices');
  }

  public function edit_loan_invoice($loan_invoice_id)
  {

      $data['title'] = 'Edit loan Invoice';
      $v_data['loan_invoice_id'] = $loan_invoice_id;
      $loan_id = $this->session->userdata('invoice_loan_id_searched');
      $v_data['title'] = $data['title'];
      $data['content'] = $this->load->view('loans/edit_loan_invoice', $v_data, true);
      $this->load->view('admin/templates/general_page', $data);

  }


  public function delete_loan_payment($loan_payment_id)
  {
    $update_query['loan_payment_status'] = 2;
    $update_query['last_modified_by'] = $this->session->userdata('personnel_id');
    $this->db->where('loan_payment_id',$loan_payment_id);
    $this->db->update('loan_payment',$update_query);

    redirect('accounting/loan-payments');
  }

  public function edit_loan_payment($loan_payment_id)
  {

      $data['title'] = 'Edit loan Invoice';
      $v_data['loan_payment_id'] = $loan_payment_id;
      $loan_id = $this->session->userdata('payment_loan_id_searched');
      $v_data['title'] = $data['title'];
      $data['content'] = $this->load->view('loans/edit_loan_payment', $v_data, true);
      $this->load->view('admin/templates/general_page', $data);

  }

   public function delete_credit_note_item($loan_credit_note_item_id,$loan_credit_note_id=NULL)
  {
   
    $this->db->where('loan_credit_note_item_id',$loan_credit_note_item_id);
    $this->db->delete('loan_credit_note_item',$update_query);

    if(!empty($loan_credit_note_id))
    {
      redirect('edit-loan-credit-note/'.$loan_credit_note_id);
    }
    else
    {
      redirect('accounting/loan-credit-notes');
    }
  }

  public function delete_loan_credit_note($loan_credit_note_id,$loan_id)
  {


    $update_query['loan_credit_note_status'] = 2;
    $update_query['last_modified_by'] = $this->session->userdata('personnel_id');
    $this->db->where('loan_credit_note_id',$loan_credit_note_id);
    $this->db->update('loan_credit_note',$update_query);

    redirect('accounting/loan-credit-notes');
  }
  public function edit_loan_credit_note($loan_credit_note_id)
  {
     $data['title'] = 'Edit loan Credit Note';
      $v_data['loan_credit_note_id'] = $loan_credit_note_id;
      $loan_id = $this->session->userdata('credit_note_loan_id_searched');
      $v_data['title'] = $data['title'];
      $data['content'] = $this->load->view('loans/edit_loan_credit_note', $v_data, true);
      $this->load->view('admin/templates/general_page', $data);
  }

  public function get_credit_note_details($loan_credit_note_id)
  {
    $data['loan_credit_note_id'] = $loan_credit_note_id;
    $this->load->view('loans/view_loan_credit_notes', $data); 
  }

  public function delete_loan($loan_id)
  {
      $loan_entry['loan_status'] = 1;
      $this->db->where('loan_id',$loan_id);
      $this->db->update('loan',$loan_entry);

      redirect('accounting/loans');
  }

   public function export_loans()
  {
    $this->loans_model->export_loans();
  }

}
?>
