<!-- dates  -->
<?php

$where = 'v_transactions_by_date.transactionCategory = "Revenue Payment"';
		
$table = 'v_transactions_by_date';
$visit_search = $this->session->userdata('cash_report_search');

// if(!empty($visit_search))
// {
// 	$where .= $visit_search;
// }
// else
// {
$where .=' AND v_transactions_by_date.transaction_date BETWEEN "'.$first_day.'" AND "'.$last_day.'" ';
// }

$branch_session = $branch_id;

if($branch_session > 0)
{
	$where .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
	

}
$where2 = $where;

$normal_payments = $this->reports_model->get_normal_payments($where2, $table, 'cash');
$payment_methods = $this->reports_model->get_payment_methods($where2, $table, 'cash');


$branch_session = $this->session->userdata('branch_id');

$branch_add = '';
$visit_branch_add = '';
if($branch_session > 0)
{
	$branch_add = ' AND branch_id = '.$branch_session;
	$visit_branch_add = ' AND visit.branch_id = '.$branch_session;
}

$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0  AND appointments.appointment_date BETWEEN "'.$first_day.'" AND "'.$last_day.'" '.$visit_branch_add;
$table = 'patients,visit,appointments';
$total_appointments = $this->reception_model->count_items($table, $where);



$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 0 AND (appointments.appointment_status = 4 ) AND appointments.appointment_date BETWEEN "'.$first_day.'" AND "'.$last_day.'"'.$visit_branch_add;
$table = 'patients,visit,appointments';
$honoured_appointments = $this->reception_model->count_items($table, $where);








$where = 'patients.patient_delete = 0 AND visit.appointment_id = 0 AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0  AND visit.visit_date BETWEEN "'.$first_day.'" AND "'.$last_day.'" '.$visit_branch_add;
$table = 'patients,visit';
$coming_appointments = $this->reception_model->count_items($table, $where);




$where = 'patients.patient_delete = 0 AND (visit.revisit = 1 OR visit.revisit = 0) AND close_card <> 2 AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0   AND visit.visit_date BETWEEN "'.$first_day.'" AND "'.$last_day.'" '.$visit_branch_add;
$table = 'patients,visit';
$new_visits = $this->reception_model->count_items($table, $where);


$where = 'patients.patient_delete = 0 AND visit.revisit = 2 AND close_card <> 2 AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0  AND visit.visit_date BETWEEN "'.$first_day.'" AND "'.$last_day.'" '.$visit_branch_add;
$table = 'patients,visit';
$revisit = $this->reception_model->count_items($table, $where);





// $patients = $this->reports_model->get_patients_visits(1);
?>
<div class="row">
    <!-- End Transaction Breakdown -->
    <div class="col-md-5">
       <?php echo $this->load->view('search/visit_report_search', '', TRUE);?>

       <?php
			$search = $this->session->userdata('new_visit_report_search');
			echo $title;
			if(!empty($search))
			{
				echo ' <a href="'.site_url().'reports/close_new_visit_report" class="btn btn-warning btn-xs ">Close Search</a>
				';
			}
			
		?>
    </div>
     
   
    <div class="col-md-7">
    	
    	<h4 class="center-align">How patient's came to know about us</h4>
    	<div class="panel-body" style="height:80vh;overflow-y:scroll;">
    		

        	<?php 


        	$month = date('m',strtotime($first_day));
        	$year = date('Y',strtotime($first_day));
        	$this->db->where('place.place_delete = 0');

			$query = $this->db->get('place');


			$patients_list = '';
			$patient_count = 0;
			$amount_total = 0;
			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					// code...

					$place_name = $value->place_name;
					$place_id = $value->place_id;

					$community_where ='patients.patient_delete = 0 AND  YEAR(patients.patient_date) = '.$year.' AND patients.about_us = '.$place_id.'  AND MONTH(patients.patient_date) = "'.$month.'" AND patients.branch_id ='.$branch_session;
					$community_table = 'patients';
					$patient_numbers = $this->reception_model->count_items($community_table, $community_where);

					(int)$month;

					// if($patient_numbers > 0)
					// {

						$where ='patients.patient_delete = 0 AND  YEAR(patients.patient_date) = '.$year.' AND patients.about_us = '.$place_id.'  AND MONTH(patients.patient_date) = "'.$month.'" AND visit_invoice.visit_id = visit.visit_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND YEAR(visit_invoice.created) = "'.$year.'" AND MONTH(visit_invoice.created) = "'.$month.'"  AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0 AND visit.patient_id = patients.patient_id AND visit.branch_id ='.$branch_session;
						$table = 'patients,visit_invoice,visit_charge,visit';
						$select = 'SUM(visit_charge.visit_charge_amount*visit_charge.visit_charge_units) AS number';
						$cpm_group = 'patients.about_us';
						$amount = $this->dashboard_model->count_items_group($table, $where, $select,$cpm_group);

						$patient_count += $patient_numbers;
						$amount_total += $amount;
						$patients_list .= '<tr>
												<th>'.$place_name.'</th>
												<td><a onclick="open_patients('.$place_id.','.$year.','.$month.')">'.$patient_numbers.'</a></td>
												<td><a onclick="open_patients('.$place_id.','.$year.','.$month.')">'.number_format($amount,2).'</a></td>

											</tr>';
					// }
					

				}
				$patients_list .= '<tr>
												<th>TOTALS</th>
												<td>'.$patient_count.'</td>
												<td>'.number_format($amount_total,2).'</td>

											</tr>';
			}
				
        	?>

        	<table class="table table-condensed table-bordered">
        		<thead>
        			<thead>
        				<th>Place</th>
        				<th>Count</th>
        				<th>Revenue</th>
        			</thead>
        		</thead>
        		<tbody>
        			<?php echo $patients_list;?>
        		</tbody>
        	</table>
    	</div>
    </div>
  
    
</div>



<script type="text/javascript">
	function open_patients(place_id,year,month)
	{
		open_sidebar();

		var config_url = $('#config_url').val();
        var data_url = config_url+"admin/get_all_patients/"+place_id+"/"+year+"/"+month;

    	// alert(data_url);
        
        $.ajax({
        type:'POST',
        url: data_url,
        data:{visit_id: 1},
        dataType: 'text',
		success:function(data)
		{
			$("#sidebar-div").html(data);		
		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);
        }

        });
	}
</script>
