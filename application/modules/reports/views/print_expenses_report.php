<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Payments</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>
        <?php
        ?>
      <div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align">
            	<strong><?php echo $title?></strong>
            </div>
        </div>
        
    	<div class="row">
        	<div class="col-md-12">
        		<?php 
        			// echo $result;
					// var_dump($query);die();=
        			$query = $this->reports_model->get_expenses_detail();
					$result ='';

					if ($query->num_rows() > 0)
					{
						$count = 0;
						
						
							$result .= 
							'
								<table class="table table-condensed table-hover table-bordered ">
								  <thead>
									<tr>
									  <th>#</th>
									  <th>Transaction Date</th>
									  <th>Account Name</th>
									  <th>Transaction Category</th>
									  <th>Description</th>
									  <th>Reference Code</th>
									  <th>Amount</th>
									</tr>
								  </thead>
								  <tbody>
							';
						
						
						
						foreach ($query->result() as $row)
						{

							$total_amount = $row->dr_amount;
							$accountName = $row->accountName;
							$referenceCode = $row->referenceCode;
							$account_id = $row->accountId;
						    $transactionDescription = $row->transactionDescription;
						    $transactionClassification = $row->transactionClassification;
						    $transactionDate = $row->transactionDate;
						    $transactionCode = $row->transactionCode;

							if(empty($referenceCode))
							{
								$referenceCode = $transactionCode;
							}

							if($transactionClassification == "Creditors Invoices Payments")
							{
								$total_amount = $row->cr_amount;
								$accountName = 'Creditor Payments';
							}

							$total_operational_amount += $total_amount;
							$count++;
							$result .='<tr>
											<td class="text-left">'.$count.'</td>
			                          		<td class="text-left">'.strtoupper($transactionDate).'</td>
			                          		<td class="text-left">'.strtoupper($accountName).'</td>
			                          		<td class="text-left">'.strtoupper($transactionClassification).'</td>
			            					<td class="text-left">'.strtoupper($transactionDescription).'</td>
			                          		<td class="text-left">'.strtoupper($referenceCode).'</td>
			            					<td class="text-right">'.number_format($total_amount,2).'</td>
			            				 </tr>';
						}
						$result .='<tr>
										<th class="text-left" colspan="6">Total</th>
		            					<th class="text-right">'.number_format($total_operational_amount,2).'</th>
		            				 </tr>';
						
						$result .= 
						'
									  </tbody>
									</table>
						';
					}

					else
					{
						$result .= "There are no expense recorded";
					}
					echo $result;
        		 ?>
			
            </div>
        </div>
        <?php

		$payments_result = $this->reports_model->get_expenses_summary();
		// var_dump($payments_result);die();
		$results ='';
		$total_visit_report ='';
		$amount_paid_totals = 0;
		if($payments_result->num_rows() > 0)
		{
		    $x = 0;
		    foreach ($payments_result->result() as $key => $value) {
		        # code...
		        $accountName = $value->accountName;
		        $total_amount = $value->total_amount;
		        $total_cr_amount = $value->total_cr_amount;
		        $accountsclassfication = $value->accountsclassfication;
		        if($accountsclassfication == "Bank")
		        {
		        	$total_amount = $total_cr_amount;
		        	$accountName = 'Creditor Payments';
		        }
		        $x++;
		        $color = $this->reception_model->random_color();                           
		        $total_visit_report .= '{
		                                    label: " '.$accountName.' ('.number_format($total_amount,2).')",
		                                    data: [
		                                        ['.$x.', '.$total_amount.']
		                                    ],
		                                    color: "'.$color.'",
		                                },';
		        $amount_paid_totals += $total_amount;

		        $results .= '
		                    <tr>
		                        <th>'.$accountName.'</th>
		                        <td>'.number_format($total_amount,2).'</td>
		                    </tr>';


		    }
		}

		// var_dump($total_visit_report);die();
		?>
        <div class="row">
            <div class="col-md-6">

                <h3>Per Expense Account </h3>
                <table class="table table-striped table-hover table-condensed">
                    <thead>
                        <tr>
                            <th>Transaction Category Name</th>
                            <th>Amount Used</th>
                        </tr>
                    </thead>
                    <tbody>
                       
                        <?php echo $results;?>
                        <tr>
                            <th>TOTALS</th>
                            <td><?php echo number_format($amount_paid_totals,2);?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
               
            </div>
        </div>
    </body>
    
</html>