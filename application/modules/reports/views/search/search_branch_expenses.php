       
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<!-- <h2 class="panel-title pull-right"> Search E:</h2> -->
            	<h2 class="panel-title">Search expenses reponse</h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
			<?php
            echo form_open("search-expense-report", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-4">
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label"> Date From: </label>
                        
                        <div class="col-lg-8">
                        	<div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder=" Date From">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label"> Date To: </label>
                        
                        <div class="col-lg-8">
                        	<div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder="Visit Date To">
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-4">
                	<div class="form-group">
	                    <div class="center-align">
	                        <button type="submit" class="btn btn-info">Search Report</button>
	                    </div>
	                </div>
                </div>
            </div>
            <br>
           
            
            
            <?php
            echo form_close();
            ?>
          </div>
		</section>