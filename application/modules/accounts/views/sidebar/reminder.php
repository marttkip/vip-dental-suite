<?php

$patient = $this->reception_model->patient_names2($patient_id);

$patient_number = $patient['patient_number'];
$patient_name  = $patient['patient_surname'];
if(!empty($visit_invoice_id))
{
	$visit_invoice_detail = $this->accounts_model->get_visit_invoice_details($visit_invoice_id);

	if($visit_invoice_detail->num_rows() > 0)
	{
		foreach ($visit_invoice_detail->result() as $key => $value) {
			# code...
			$visit_invoice_number = $value->visit_invoice_number;
			$preauth_date = $value->preauth_date;
			$created = $value->created;
			$preauth_amount = $value->preauth_amount;
			$scheme_name = $value->scheme_name;
			$member_number = $value->member_number;
			$bill_to = $value->bill_to;
			$insurance_limit = $value->insurance_limit;
			

		}
	}
}



$visit__rs1  = $this->accounts_model->get_incomplete_invoices($patient_id,NULL,$visit_date,$visit_invoice_id);

$result = "

	<table align='center' class='table table-striped table-bordered table-condensed'>
	<tr>
		<th></th>
		<th></th>
		<th>Invoice Item</th>
		<th>Description</th>
		<th>Units</th>
		<th>Unit Cost</th>
		<th>Total</th>

	</tr>
";
	$total= 0;  
	$number = 0;
	if($visit__rs1->num_rows() > 0)
	{						
		foreach ($visit__rs1->result() as $key1 => $value) :
			$v_procedure_id = $value->visit_charge_id;
			$procedure_id = $value->service_charge_id;
			$visit_charge_amount = $value->visit_charge_amount;
			$units = $value->visit_charge_units;
			$procedure_name = $value->service_charge_name;
			$service_id = $value->service_id;
			$personnel_creator = $value->personnel_creator;
			$visit_invoice_id = $value->visit_invoice_id;
			$visit_comment = $value->visit_charge_comment;
			// $visit_type_id = 1;
			$total= $total +($units * $visit_charge_amount);

			if($visit_invoice_id > 0)
			{
				$text_color = "success";
			}
			else
			{
				$text_color = 'default';
			}

			
			$checked="";
			$number++;
			$personnel_id = $this->session->userdata('personnel_id');

			if($personnel_creator == $personnel_id OR $personnel_id == 0)
			{
				$personnel_check = TRUE;
			}
			else
			{
				$personnel_check = FALSE;
			}
			
			
		
	
			
		$result .='
					<tr> 

						<td class="'.$text_color.'"> </td>
						<td class="'.$text_color.'" >'.$number.'</td>
						<td  class="'.$text_color.'" align="left">'.$procedure_name.'</td>
						<td  class="'.$text_color.'" align="left">'.$visit_comment.'</td>
						<td  class="'.$text_color.'" align="center">
							'.$units.'

							<input type="hidden" id="visit_invoice_id'.$v_procedure_id.'" class="form-control" value="'.$visit_invoice_id.'" size="3" />
						</td>
						<td  class="'.$text_color.'" align="center">'.$visit_charge_amount.'</td>
						<td  class="'.$text_color.'" align="center">'.number_format($units*$visit_charge_amount,2).'</td>
				
					</tr>	
			';
								
			endforeach;

	}
	else
	{
		
	}
	$result .="
		<tr bgcolor='#D9EDF7'>
		
		<td></td>
		<td colspan='4'></td>
		<th>Grand Invoice Total: </th>
		<th align='center'><div id='grand_total'>".number_format($total,2)."</div></th>
		
		</tr>
		 </table>
		";
	$result .= "</table>";
	$preauth_amount = $total;
	
?>
<div>
<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i><?php echo $title?></h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="padd">
        	
            <div class="col-print-12">                    
                <div class="col-md-8">

                	<div class="col-md-6">
                		<p><strong> Patient Name:</strong> <?php echo $patient_name;?></p>
                		<p><strong> File Number :</strong> <?php echo $patient_number;?></p>
                	</div>
                	<div class="col-md-6">
                		<p><strong> Invoice Number:</strong> <?php echo $visit_invoice_number?></p>
                		<p><strong> Invoice Date  :</strong> <?php echo date('jS M Y',strtotime($created))?></p>
                	</div>
                	<div class="col-md-12">
                		<?php echo $result;?>
                	</div>
                	

                </div>
                <div class="col-md-4">
                	<?php echo form_open("reception/save_inpatient_visit/".$patient_id, array("class" => "form-horizontal","id"=>"add-reminder"));?>
                	<div class="form-group">
                
						<label class="col-lg-12">Reminder Date: </label>
						
						<div class="col-lg-12">
			                <div class="input-group">
			                    <span class="input-group-addon">
			                        <i class="fa fa-calendar"></i>
			                    </span>
			                    <input data-format="yyyy-MM-dd" type="text" class="form-control datepicker" id="reminder_date" name="reminder_date" placeholder="Reminder Date" value="<?php echo date('Y-m-d');?>" required="required">
			                </div>
						</div>
				
                    </div>
                    <input type="hidden" name="visit_invoice_id" id="visit_invoice_id" value="<?php echo $visit_invoice_id?>">
                    <input type="hidden" name="patient_id" id="patient_id" value="<?php echo $patient_id?>">
                    <input type="hidden" name="visit_id" id="visit_idd" value="<?php echo $visit_id?>">
                	<div class="form-group">
                		<label class="col-lg-12">Reminder Note: </label>
                        <div class="col-lg-12">
                           <textarea class="form-control" rows="5" name="reminder_note" id="reminder_note" required="required"></textarea>
                        </div>
                    </div>
                	<div class="form-group">
                        <div class="center-align">
                            <button class="btn btn-info btn-sm" type="submit" onclick="return confirm('Are you sure you want to add this reminder ?')"> Add Reminder</button>
                        </div>
                    </div>
                    <?php echo form_close();?>
                </div>
              

            </div>
           

            
        </div>
    </div>
</section>
<div class="col-md-12">					        	
	<div class="center-align">
		<a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
	</div>  
</div>
</div>