<?php
$patient_items = '';
$visit_type_id = null;
$close_card = 3;
if($query->num_rows() > 0)
{
    foreach ($query->result() as $key => $res) 
    {
        # code...

        $visit_date = date('D M d Y',strtotime($res->visit_date)); 
        $insurance_number = $res->insurance_number;
        $insurance_description = $res->insurance_description;
        $insurance_limit = $res->insurance_limit;
        $visit_type = $res->visit_type;
        $patient_id = $res->patient_id;
        $visit_id = $res->visit_id;
        $visit_type_id = $res->visit_type;
         $close_card = $res->close_card;
        

    }

}
?>
<div>
<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i><?php echo $title?></h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="padd">
           
            <div class="row" id="invoice-div" style="padding:10px;">
                    <input type="text" name="search_procedures" id="search_procedures" class="form-control" onkeyup="search_procedures_profoma(<?php echo $patient_id;?>,<?php echo $visit_id;?>,<?php echo $visit_type_id;?>,<?php echo $visit_profoma_id;?>)" placeholder="Search procedures.... root canal,extraction">
                    
            </div>
            <input type="hidden" name="visit_type_id" id="visit_type_id" value="<?php echo $visit_type_id?>">
             <!-- <input type="hidden" name="visit_invoice" id="visit_invoice" value="<?php echo $visit_invoice_id?>"> -->
            <div class="col-md-12" style="padding:10px;" id="charges-div">
                    <ul  id="searched-procedures" style="list-style: none;">

                    </ul>
            </div>
              
            <div class="col-md-12" style="padding:10px;">
                    <div id="visit-charges"></div>
            </div>
        </div>
    </div>
</section>
<div class="col-md-12">                             
    <div class="center-align">
        <a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
    </div>  
</div>
</div>
<script type="text/javascript">
        
</script>
        