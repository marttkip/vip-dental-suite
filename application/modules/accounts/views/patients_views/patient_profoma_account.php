<?php
		$result = '';
		// $search = $this->session->userdata('debtors_search_query');
		// if(!empty($search))
		// {
		// 	echo '<a href="'.site_url().'accounting/reports/close_reports_search" class="btn btn-sm btn-warning">Close Search</a>';
		// }
		$query = $this->accounts_model->get_patient_profoma_statement($patient_id,1);
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = 0;
			
			$result .= 
				'
					<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Profoma Date</th>
						  <th>Profoma To</th>
						  <th>Profoma No</th>
						  <th>Profoma Amount</th>
						</tr>
					  </thead>
					  <tbody>
			';
			
			// $personnel_query = $this->accounting_model->get_all_personnel();
			$total_waiver = 0;
			$total_payments = 0;
			$total_invoice = 0;
			$total_balance = 0;
			$total_rejected_amount = 0;
			$total_cash_balance = 0;
			$total_insurance_payments =0;
			$total_insurance_invoice =0;
			$total_payable_by_patient = 0;
			$total_payable_by_insurance = 0;
			$total_debit_notes = 0;
			$total_credit_notes= 0;
			
			foreach ($query->result() as $row)
			{
				$total_invoiced = 0;
				$visit_date = date('jS M Y',strtotime($row->invoice_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}
				
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type;
				$patient_number = $row->patient_number;
				$visit_type = $row->visit_type;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				$rejected_amount = $row->amount_rejected;
				$visit_profoma_number = $row->visit_profoma_number;
				$visit_profoma_id = $row->visit_profoma_id;
				$parent_visit = $row->parent_visit;
				$branch_code = $row->branch_code;
				$visit_type_name = $row->visit_type_name;
				$reminder_status = $row->reminder_status;
				$bill_to = $row->bill_to;


				if(empty($rejected_amount))
				{
					$rejected_amount = 0;
				}
				// $coming_from = $this->reception_model->coming_from($visit_id);
				// $sent_to = $this->reception_model->going_to($visit_id);
				
				$total_amount_credited = 0;
				$transaction_id = $row->transaction_id;
				$reference_id = $row->reference_id;
				$transaction_date = $row->transaction_date;
				$count++;
				$invoice_total = $this->accounts_model->get_visit_profoma_invoice_total($visit_profoma_id);
				$payments_value = 0;// $this->accounts_model->get_visit_invoice_payments($visit_profoma_id);
				$credit_note = 0;//$this->accounts_model->get_visit_invoice_credit_notes($visit_profoma_id);
				$total_amount_credited = $credit_note + $payments_value;
			

				$balance  = 0;//$this->accounts_model->balance($total_amount_credited,$invoice_total);

				$total_payable_by_patient += $invoice_total;
				$total_payments += $payments_value;
				$total_balance += $invoice_total;
				$total_credit_notes += $credit_note;

				if($reminder_status == 1)
				{
					$add_reminder =	 '<i class="fa fa-clock-o"></i>';
				}
				else
				{
					$add_reminder = '';
				}

				
				$result .= 

					'
						<tr>
							<td>'.$count.'</td>
							<td>'.$visit_date.' </td>
							<td>'.strtoupper($visit_type_name).'</td>
							<td>'.$visit_profoma_number.'</td>
							<td>'.(number_format($invoice_total,2)).'</td>
							<td><a href="'.site_url().'print-profoma-invoice/'.$visit_profoma_id.'/'.$visit_id.'" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print Profoma Invoice</a></td>
							'.$check.'
						</tr> 
				';
				
			}

			$result .= 
					'
						<tr>
							<td colspan=4> Totals</td>
							<td><strong>'.number_format($total_balance,2).'</strong></td>
						</tr> 
				';
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no visits";
		}
		
		echo $result;
?>