<?php 
	
	if(!isset($contacts))
	{
		$contacts = $this->site_model->get_contacts();
	}
	$data['contacts'] = $contacts; 

?>
<!doctype html>
<html class="fixed sidebar-left-collapsed">
	<head>
        <?php echo $this->load->view('admin/includes/header', $contacts, TRUE); ?>
        	<link rel="stylesheet" href="<?php echo base_url()."assets/calendar/";?>yearview.css" />
        	
		  
		  <script src="<?php echo base_url()."assets/calendar/";?>fullcalendar.js"></script>
		  <style type="text/css">
		  	.fc .fc-widget-header {
				background: #800080;
				border-color: #800080;
				color: white;
				font-size: 13px;
				font-size: 1.3rem;
				font-weight: 500;
				padding: 0px 0;
				text-transform: uppercase;
			}
		  </style>
    </head>

	<body>
    	<input type="hidden" id="base_url" value="<?php echo site_url();?>">
    	<input type="hidden" id="config_url" value="<?php echo site_url();?>">
    	<!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->
    	<section class="body">
            <!-- Top Navigation -->
            <?php echo $this->load->view('admin/includes/top_navigation', $data, TRUE); ?>
            
                
            <div class="inner-wrapper">
            	<?php echo $this->load->view('admin/includes/top_level_navigation', '', TRUE); ?>
            	
                
                <section role="main" class="content-body">
                	
                		<?php echo $content;?>
                    
					
                
                </section>
            </div>

           <aside id="sidebar-right" class="sidebar-right" style="display: none;overflow: auto;">
				<div class="nano has-scrollbar">
					<div class="nano-content" tabindex="0" style="right: -17px;">
						<!-- <a href="#" class="mobile-close d-md-none">
							Collapse <i class="fa fa-chevron-right"></i>
						</a> -->
			
						<div class="sidebar-right-wrapper">
							<div id="current-sidebar-div"></div>
							<div id="existing-sidebar-div"></div>							
			
						</div>
					</div>
				<div class="nano-pane" style="opacity: 1; visibility: visible;"><div class="nano-slider" style="height: 189px; transform: translate(0px);"></div></div></div>
			</aside>

			 <div id="add_template_note" class="modal fade">
			    <div class="modal-dialog">
			        <div class="modal-content">
			            <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
			                <h4  class="modal-title"> Add message Template </h4>
			            </div>
			            <div  class="modal-body"> 
			            	<form id="add_message_template" method="post">
								<div class="row">
									<div class="col-md-12">

										 <div class="form-group">
											<label class="col-lg-12 control-label">Template Title: </label>
											<div class="col-lg-12">
												<input type="text" id="template_code" class="form-control" name="template_code">
											</div>
										 </div>
										 <div class="form-group">
											<label class="col-lg-12 control-label">Note: </label>
											<div class="col-lg-12">
												<textarea id="schedule_note" class="form-control cleditor" name="template_description"></textarea>
											</div>
										 </div>
										
										
									</div>
								</div>
									<input type="hidden" name="contact_category_id" value="0">
								<br/>
								<div class="row">
							        <div class="col-md-12">
							        	<div class=" center-align">
							        		<button type="submit" class="btn btn-sm btn-success " onclick="return confirm('Are you sure you want to add the template ? ')">Add Template</button>
							        	</div>
							               
							        </div>
							    </div>
							</form>
			            </div>
			            <div class="modal-footer">
			            	<div id="buttons-div"></div>
			                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			            </div>
			        </div>
			    </div>
			</div>
			
            
        </section>



        <script type="text/javascript">
        	$(document).on("submit","form#add-event-planner",function(e)
			{
				e.preventDefault();
				
				var form_data = new FormData(this);

				// alert(form_data);
				
				var config_url = $('#config_url').val();	

				 var url = config_url+"planner/add_event";
				 // var values = $('#visit_type_id').val();
				 
			       $.ajax({
			       type:'POST',
			       url: url,
			       data:form_data,
			       dataType: 'text',
			       processData: false,
			       contentType: false,
			       success:function(data)
			       {
			          	var data = jQuery.parseJSON(data);
			        
			          	if(data.message == "success")
						{
							document.getElementById("sidebar-right").style.display = "none"; 
							close_side_bar();

							$('#calendar').fullCalendar('destroyEvents');
							var formDate = window.localStorage.getItem('date_set_old');
			    			var url = config_url+'planner/get_uhdc_calendar/'+formDate;
							$('#calendar').fullCalendar( 'refetchEvents',url );



					
						}
						else
						{
							alert('Please ensure you have added included all the items');
						}
			       
			       },
			       error: function(xhr, status, error) {
			       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			       
			       }
			       });
				 
				
			   
				
			});

			
        </script>
        <script type="text/javascript">
            tinymce.init({
                selector: ".cleditor",
               	height: "150"
            });
        </script>
        <!-- Vendor -->
		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-appear/jquery.appear.js"></script>		
		<!-- <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>		 -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.pie.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.categories.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.resize.js"></script>
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-sparkline/jquery.sparkline.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/raphael/raphael.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/morris/morris.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/gauge/gauge.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/snap-svg/snap.svg.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/liquid-meter/liquid.meter.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/jquery.vmap.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/jquery.vmap.world.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>	
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>			
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/select2/select2.js"></script>


		<!-- chart -->

		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-appear/jquery.appear.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.pie.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.categories.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.resize.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-sparkline/jquery.sparkline.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/raphael/raphael.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/morris/morris.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/gauge/gauge.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/snap-svg/snap.svg.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/liquid-meter/liquid.meter.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/chartist/chartist.js"></script>


		<!-- end of chart -->
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/assets/javascripts/";?>theme.js"></script>
		<script src="<?php echo base_url()."assets/themes/jasny/js/jasny-bootstrap.js";?>"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/theme.init.js"></script>

		<!-- Example -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/dashboard/examples.dashboard.js"></script>
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/charts.js"></script>
        <!-- s -->

        <script src='<?php echo base_url()."assets/fullcalendar/";?>fullcalendar.min.js'></script>
		<script src='<?php echo base_url()."assets/fullcalendar/";?>scheduler.min.css'></script>
		<script src='<?php echo base_url()."assets/fullcalendar/";?>moment.min.js'></script>
		<script src='<?php echo base_url()."assets/fullcalendar/";?>scheduler.min.js'></script>
      
        <script src="<?php echo base_url()."assets/bluish/"?>js/excanvas.min.js"></script>
        <script src="<?php echo base_url()."assets/bluish/"?>js/jquery.flot.js"></script>
        <script src="<?php echo base_url()."assets/bluish/"?>js/jquery.flot.resize.js"></script>
        <script src="<?php echo base_url()."assets/bluish/"?>js/jquery.flot.axislabels.js"></script>
        <script src="<?php echo base_url()."assets/bluish/"?>js/jquery.flot.pie.js"></script>
        <script src="<?php echo base_url()."assets/bluish/"?>js/jquery.flot.stack.js"></script>
        <!-- <script src="<?php echo base_url()."assets/"?>js/main.js"></script> -->
	    <link rel="stylesheet" href="<?php echo base_url()."assets/themes/bluish";?>/style/jquery.cleditor.css"> 
		<script src="<?php echo base_url()."assets/themes/bluish";?>/js/jquery.cleditor.min.js"></script> <!-- CLEditor -->
		<script type="text/javascript" src="<?php echo base_url();?>assets/themes/tinymce/tinymce.min.js"></script>
		 <script src='<?php echo base_url()."assets/bluish/"?>src/jquery-customselect.js'></script>
	    <link href='<?php echo base_url()."assets/bluish/"?>src/jquery-customselect.css' rel='stylesheet' />
         <!-- <script src="<?php echo base_url()."assets/bluish/"?>src/owl.carousel.js"></script> -->
	    
		


		
	</body>
</html>
