<?php

$rs2 = $this->dental_model->get_visit_procedure_billed_list($visit_id,NULL,$patient_id);


echo "
<table align='center' class='table table-striped table-hover table-condensed'>
	<tr>
		<th>Date</th>
		<th>Th</th>
		<th>Surf</th>
		<th>Dx</th>
		<th>Status</th>
		<th>Description</th>
		<th>Provider</th>
	</tr>		
";                     
		$total= 0;  
		if($rs2->num_rows() >0){
			foreach ($rs2->result() as $key1):
				$v_procedure_id = $key1->dentine_id;
				$teeth = $key1->teeth_id;
				$surface = $key1->teeth_section;
				$visit_charge_notes = $key1->notes;
				$personnel_fname = $key1->personnel_fname;
				$personnel_onames = $key1->personnel_onames;
				$date = $key1->created;
				$plan_status = $key1->plan_status;

				
				if($plan_status == 1)
				{
					$plan_color = 'red';
					$status = 'TP';
				}else if($plan_status == 2)
				{
					$plan_color = 'black';
					$status = 'CP';
				}
				else{
					$plan_color = 'green';
					$status = 'EX';
				}

				$checked="";
				$personnel_check = TRUE;
				if($personnel_check)
				{
					$checked = "<td>
								
								<td>
									<a class='btn btn-xs btn-danger' href='#' onclick='delete_procedure(".$v_procedure_id.")'><i class='fa fa-trash'></i></a>
								</td>";
				}
				if($plan_status  < 3)
				{
					$charge_button = '<td>-</td>';
				}
				else
				{
					$charge_button = '<td>-</td>';
				}


				
				echo"

						<tr  style='color:".$plan_color." !important;'> 
							<td align='center' onclick='get_procedure_information(".$v_procedure_id.",".$visit_id.")'>".$date."</td>
							<td align='center' onclick='get_procedure_information(".$v_procedure_id.",".$visit_id.")'>".$teeth."</td>
							<td align='center' onclick='get_procedure_information(".$v_procedure_id.",".$visit_id.")'>".$surface."</td>
							<td align='center' onclick='get_procedure_information(".$v_procedure_id.",".$visit_id.")'>".$status."</td>
							<td align='center' onclick='get_procedure_information(".$v_procedure_id.",".$visit_id.")'>".$status."</td>
							<td align='center' onclick='get_procedure_information(".$v_procedure_id.",".$visit_id.")'>".$personnel_fname." ".$personnel_onames."</td>
							".$charge_button."
						</tr>	
				";

				if(!empty($visit_charge_notes))
				{
					echo"
							<tr style='color:".$plan_color." !important;'> 
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td colspan='1'>".$visit_charge_notes."</td>
								
							</tr>	
						";

				}

				$procedure_rs = $this->dental_model->get_visit_dentine_notes($visit_id,$v_procedure_id);

				if($procedure_rs->num_rows() > 0)
				{
					foreach ($procedure_rs->result() as $key => $value) {
						# code...
						$notes = $value->notes;
						$visit_date = $value->created;
						$personnel_fname = $value->personnel_fname;

						echo"
							<tr style='color:".$plan_color." !important;'> 
								<td></td>
								<td colspan='3'>".date('jS M Y',strtotime($visit_date))."</td>
								<td colspan='2'>".$notes."</td>
								<td colspan='1'>".$personnel_fname."</td>
							</tr>	
						";
					}
				}
				


				endforeach;

		}
// echo"
// <tr bgcolor='#D9EDF7'>
// <td></td>
// <td></td>
// <th>Grand Total: </th>
// <th colspan='3'><div id='grand_total'>".number_format($total)."</div></th>
// <td></td>
// <td></td>
// </tr>
//  </table>
// ";


// echo '<div class="row">
//       	<div class="col-md-12">
//   			<div class="form-group">
//    				<label class="col-lg-4 control-label">Deductions and/or additions:  </label>
//     		<div class="col-lg-8">
//       				<textarea id="deductions_and_other_info" rows="5" cols="50" class="form-control col-md-12" > '.$payment_info.' </textarea>
//       		</div>
//       	</div>
//     </div>
//   </div>';

//   echo '
//   <br>
// 	<div class="row">
//         <div class="form-group">
//             <div class="col-lg-12">
//                 <div class="center-align">
//                       <a hred="#" class="btn btn-large btn-info" onclick="save_other_deductions('.$visit_id.')">Save other payment information</a>
//                   </div>
//             </div>
//         </div>
//     </div>';
?>