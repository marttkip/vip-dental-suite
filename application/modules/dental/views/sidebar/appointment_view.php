<?php

$next_app_rs = $this->dental_model->get_next_appointment($visit_id,$patient_id);


$visit_date = '';
$appointment_note = '';
if($next_app_rs->num_rows() == 1)
{
	foreach ($next_app_rs->result() as $key => $value) {
		# code...
		$visit_date = $value->visit_date;
		$procedure_done = $value->procedure_done;
		$appointment_id = $value->appointment;
		$time_start = $value->time_start;
	}
	?>
	<!-- <h2>NEXT APPP</h2> -->

		<div class="center-align">

			<h5><?php echo date('jS M Y',strtotime($visit_date))?> AT <?php echo date('h:i A',strtotime($time_start))?> <a onclick="delete_event_details(<?php echo $appointment_id;?>,1,<?php echo $patient_id;?>,<?php echo $visit_id;?>)" class="btn btn-sm btn-danger"> <I class="fa fa-trash"></I></a></h5>
			
		</div>
		<STRONG>APPOINTMENT NOTE</STRONG>
		
		<p><?php echo $procedure_done?></p>


	<?php
}
else
{
	?>
	<div class="col-md-12">
		<div class="center-align">

			<p>No appointment set</p>
		</div>
		
	</div>
	<?php
}
?>
