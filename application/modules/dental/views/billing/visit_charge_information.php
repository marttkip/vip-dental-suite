<?php

$rs2 = $this->dental_model->get_visit_procedure_billed_list(null,$visit_charge_id);


                     
$total= 0;  
if($rs2->num_rows() >0){
	foreach ($rs2->result() as $key1):
		$v_procedure_id = $key1->dentine_id;
		$dentine_id = $key1->dentine_id;
		$teeth = $teeth_id = $key1->teeth_id;
		$surface = $key1->teeth_section;
		$visit_charge_notes = $key1->notes;
		$personnel_fname = $key1->personnel_fname;
		$personnel_onames = $key1->personnel_onames;
		$date = $key1->created;
		$plan_status = $key1->plan_status;
		$visit_date = $key1->created;

		
		if($plan_status == 1)
		{
			$plan_color = 'red';
			$status = 'TP';
		}else if($plan_status == 2)
		{
			$plan_color = 'black';
			$status = 'CP';
		}
		else{
			$plan_color = 'green';
			$status = 'EX';
		}
		
		$checked="";
		$personnel_check = TRUE;
		if($personnel_check)
		{
			$checked = "
						<td>
							<a class='btn btn-xs btn-danger' href='#' onclick='delete_procedure(".$v_procedure_id.")'><i class='fa fa-trash'></i></a>
						</td>";
		}
		
		
		endforeach;

}

$notes = '';
$rs2 = $this->dental_model->get_visit_procedure_notes(null,$visit_charge_id);
if($rs2->num_rows() >0){
	foreach ($rs2->result() as $key2):
		$notes = $key2->notes;
	endforeach;
}
?>

<div class="row" style="margin-top: 20px;">
	<section class="panel">
	    <!-- <header class="panel-heading">
	        <h5 class="pull-left"><i class="icon-reorder"></i>Procedure</h5>         
	    </header> -->
	    <div class="panel-body">
	        <div class="padd">
	        	<?php
	        	///echo form_open("finance/creditors/confirm_invoice_note/".$visit_id."/".$visit_charge_id, array("class" => "form-horizontal","id" => "submit-charges-old"));
	        	?>
	          	<div class="row">
		            <div class="col-md-6">

		            	<div class="form-group">
		                    <label class="col-md-4 control-label">Description: </label>
		                    
		                    <div class="col-md-8">
		                      
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label class="col-md-4 control-label">Teeth: </label>
		                    
		                    <div class="col-md-8">
		                    	<?php echo $teeth?>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label class="col-md-4 control-label">Surface: </label>
		                    
		                    <div class="col-md-8">
		                    	<?php echo $surface?>
		                    </div>
		                </div>
		            </div>
		            <input type="hidden" name="dentine_id" id="dentine_id<?php echo $v_procedure_id;?>" value="<?php echo $dentine_id;?>">
		            <input type="hidden" name="teeth_id" id="teeth_id<?php echo $v_procedure_id;?>" value="<?php echo $teeth_id;?>">
		            <input type="hidden" name="patient_id" id="patient_id<?php echo $v_procedure_id;?>" value="<?php echo $patient_id;?>">
		            <div class="col-md-6">	
						<h4>Plans</h4>
						<div class="form-group left-align">
			                <div class="radio">

			                	<?php
			                	if($plan_status == 1)
			                	{
			                		?>
			                		<label>
				                        <input id="optionsRadios2" type="radio" name="work_status" checked  id="work_status"  value="1">
				                         Treatment Plan
				                    </label>
				                    <label>
				                        <input id="optionsRadios2" type="radio" name="work_status"  id="work_status"  value="2">
				                         Completed
				                    </label>
				                    <label>
				                        <input id="optionsRadios2" type="radio" name="work_status"  id="work_status"  value="3">
				                         Exam
				                    </label>
				                    <br>
			                		<?php

			                	}
			                	else if($plan_status == 2)
			                	{
			                		?>

			                		<label>
				                        <input id="optionsRadios2" type="radio" name="work_status"   id="work_status"  value="1">
				                         Treatment Plan
				                    </label>
				                    <label>
				                        <input id="optionsRadios2" type="radio" name="work_status" checked  id="work_status"  value="2">
				                         Completed
				                    </label>
				                    <label>
				                        <input id="optionsRadios2" type="radio" name="work_status"   id="work_status"  value="3">
				                         Exam
				                    </label>
				                    <br>

			                		<?php
			                	}else if($plan_status == 3)
			                	{
			                		?>

			                		<label>
				                        <input id="optionsRadios2" type="radio" name="work_status"   id="work_status"  value="1">
				                         Treatment Plan
				                    </label>
				                    <label>
				                        <input id="optionsRadios2" type="radio" name="work_status"   id="work_status"  value="2">
				                         Completed
				                    </label>
				                    <label>
				                        <input id="optionsRadios2" type="radio" name="work_status" checked  id="work_status"  value="3">
				                         Exam
				                    </label>
				                    <br>

			                		<?php
			                	}
			                	?>
			                	
			                	
			                    
			                </div>
			            </div>
			            <div class="form-group">
			            	<h4>Previous procedure notes</h4>
			            	<div class="col-md-12">
			            		<table class="table table-condensed table-hover">
			            			<thead>
			            				<tr>
			            					<td>Date</td>
			            					<td>Notes</td>
			            					<td>Doctor</td>
			            				</tr>
			            			</thead>
			            			
			            		
				            		<?php
				            		if(!empty($visit_charge_notes))
									{
										echo"
												<tr> 
													<td>".date('jS M Y',strtotime($date))."</td>
													<td>".$visit_charge_notes."</td>
													<td >".$name."</td>
												</tr>	
											";

									}

									$procedure_rs = $this->dental_model->get_visit_procedure_notes(NULL,$v_procedure_id,NULL,$visit_date);

									if($procedure_rs->num_rows() > 0)
									{
										foreach ($procedure_rs->result() as $key => $value) {
											# code...
											$notes = $value->notes;
											$visit_date = $value->visit_date;
											$personnel_fname = $value->personnel_fname;

											echo"
												<tr> 
													<td>".date('jS M Y',strtotime($visit_date))."</td>
													<td>".$notes."</td>
													<td >".$personnel_fname."</td>
												</tr>	
											";
										}
									}
				            		?>
				            	</table>
			            	</div>
			            </div>             	
		                <div class="form-group">
		                   <div class="col-md-12">
		                      <textarea  name="notes<?php echo $v_procedure_id;?>" id="notes<?php echo $v_procedure_id;?>"  class="form-control cleditor" required="required"><?php echo $notes?></textarea>
		                    </div>
		                </div>
		            </div>
	          	</div>
	          	<input type="hidden" name="visit_id" id="visit_id" value="<?php echo $visit_id?>">
		        <input type="hidden" name="visit_charge_id" id="visit_charge_id" value="<?php echo $visit_charge_id?>">
	          	<div class="row" style="margin-top: 10px;">
		            <div class="col-md-6">		           		
		            	<button  class="btn btn-sm btn-success pull-right" onclick="update_visit_charges(<?php echo $visit_id;?>,<?php echo $visit_charge_id;?>)"> 
		            		submit update 
		            	</button>
		            </div>
		            <div class="col-md-6 ">	
		            	<?php
		            	if($plan_status == 1)
		            	{		            	
			            	?>	           		
				            	<button  class="btn btn-sm btn-danger pull-right" onclick="delete_procedure(<?php echo $visit_charge_id;?>,<?php echo $visit_id;?>,<?php echo $dentine_id?>)"> 
				            		 Delete
				            	</button>
			            	<?php
		                 }
		            	?>
		            </div>
		        </div>
		        <?php //echo form_close();?>
	        </div>
	    </div>
	</section>
</div>
<div class="row" style="margin-top: 5px;">
	<ul>
		<li style="margin-bottom: 5px;">
			<div class="row">
		        <div class="col-md-12 center-align">
			        <a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>			        
		        </div>
		    </div>
			
		</li>
	</ul>
</div>