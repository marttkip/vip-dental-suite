<?php

$visit_cr_note_number ='';
$preauth_date = date('Y-m-d');
$preauth_amount = '';
$insurance_limit = '';
$member_number = '';
$scheme_name = '';
$bill_to = 0;
$created = NULL;
$preauth_date = NULL;
if(!empty($visit_credit_note_id))
{
	$visit_invoice_detail = $this->accounts_model->get_visit_credit_note_details($visit_credit_note_id);

	if($visit_invoice_detail->num_rows() > 0)
	{
		foreach ($visit_invoice_detail->result() as $key => $value) {
			# code...
			$visit_cr_note_number = $value->visit_cr_note_number;
			$preauth_date = $value->preauth_date;
			$preauth_amount = $value->preauth_amount;
			$authorising_officer = $value->authorising_officer;
			$insurance_limit = $value->insurance_limit;
			$member_number = $value->member_number;
			$scheme_name = $value->scheme_name;
			$bill_to  = $value->bill_to;
			$dentist_id  = $value->dentist_id;
			$created  = $value->created;

		}
	}
}

$visit_type = 0;
if($visit_id > 0)
{
	$visit_rs = $this->accounts_model->get_visit_details($visit_id);
	$visit_type_id = 0;
	$close_card = 3;
	if($visit_rs->num_rows() > 0)
	{
		foreach ($visit_rs->result() as $key => $value) {
			# code...
			$close_card = $value->close_card;
			$visit_type_id = $value->visit_type;
			$visit_time_out = $value->visit_time_out;
			$parent_visit = $value->parent_visit;
			$close_card = $value->close_card;
			$visit_id = $value->visit_id;
			$patient_id = $value->patient_id;
			$visit_type = $value->visit_type;
			
			$visit_time_out = date('jS F Y',strtotime($visit_time_out));
		}
	}

}



if($bill_to > 0)
{
	$visit_type = $bill_to;
}

if($visit_type == 1)
{
	$display = 'none';
}
else if($visit_type_id > 1)
{
	$display = 'block';
}

if(empty($preauth_date))
{
	$preauth_date = date('Y-m-d');
}


if(empty($created))
{
	$created = date('Y-m-d');
}

// get all the procedures

$visit__rs1 = $this->accounts_model->get_visit_credit_note($visit_id,$visit_credit_note_id,$patient_id);
// var_dump($visit__rs1->result())	;die();
echo form_open("finance/creditors/confirm_invoice_note/".$visit_id."/".$patient_id, array("class" => "form-horizontal","id" => "confirm-credit-note"));
$result = "

	<table align='center' class='table table-striped table-bordered table-condensed'>
	<tr>
		<th></th>
		<th></th>
		<th>Invoice Item</th>
		<th>Tooth</th>
		<th>Units</th>
		<th>Unit Cost</th>
		<th>Total</th>
		<th></th>
		<th></th>

	</tr>
";
	$total= 0;  
	$number = 0;
	if($visit__rs1->num_rows() > 0)
	{						
		foreach ($visit__rs1->result() as $key1 => $value) :
			$v_procedure_id = $value->visit_credit_note_item_id;
			$procedure_id = $value->service_charge_id;
			$visit_cr_note_amount = $value->visit_cr_note_amount;
			$visit_cr_note_comments = $value->visit_cr_note_comment;
			$units = $value->visit_cr_note_units;
			$procedure_name = $value->service_charge_name;
			$service_id = $value->service_id;
			$visit_credit_note_id = $value->visit_credit_note_id;

			// $visit_type_id = 1;
			$total= $total +($units * $visit_cr_note_amount);

			if($visit_credit_note_id > 0)
			{
				$text_color = "success";
			}
			else
			{
				$text_color = 'default';
			}

			// if($visit_type_id == 1)
			// {
			// 	$visit = 'SELF';
			// }
			// else
			// {
			// 	$visit = 'INSURANCE';
			// }
		
			$checked="";
			$number++;
			$personnel_check = TRUE;
			if($personnel_check)
			{
				$checked = "<td>
							<a class='btn btn-sm btn-primary'  onclick='calculatecredittotal(".$visit_cr_note_amount.",".$v_procedure_id.", ".$procedure_id.",".$visit_id.",".$patient_id.",".$visit_credit_note_id.")'><i class='fa fa-pencil'></i></a>
							</td>
							<td>
								<a class='btn btn-sm btn-danger' href='#' onclick='delete_procedure(".$v_procedure_id.", ".$visit_id.")'><i class='fa fa-trash'></i></a>
							</td>";
			}

			// if($close_card == 3)
			// {

			// }
		 $checkbox_data = array(
			                    'name'        => 'visit_invoice_items[]',
			                    'id'          => 'checkbox'.$v_procedure_id,
			                    'class'          => 'css-checkbox  lrg ',
			                    'checked'=>'checked',
			                    'value'       => $v_procedure_id
			                  );
			
		$result .='
					<tr> 

						<td class="'.$text_color.'">'.form_checkbox($checkbox_data).'<label for="checkbox'.$v_procedure_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
						<td class="'.$text_color.'" >'.$number.'</td>
						<td  class="'.$text_color.'" align="left">'.$procedure_name.'</td>
						<td  class="'.$text_color.'" align="center">
							<textarea id="visit_cr_note_comments'.$v_procedure_id.'" class="form-control" >'.$visit_cr_note_comments.'</textarea>
							
						</td>
						<td  class="'.$text_color.'" align="center">
							<input type="text" id="units'.$v_procedure_id.'" class="form-control" value="'.$units.'"  size="3" />

							<input type="hidden" id="visit_credit_note_id'.$v_procedure_id.'" class="form-control" value="'.$visit_credit_note_id.'"  size="3" />
						</td>
						<td  class="'.$text_color.'" align="center"><input type="text" class="form-control" size="5" value="'.$visit_cr_note_amount.'" id="billed_amount'.$v_procedure_id.'" ></div></td>

						<td  class="'.$text_color.'" align="center">'.number_format($units*$visit_cr_note_amount,2).'</td>
						'.$checked.'
					</tr>	
			';
								
			endforeach;

	}
	else
	{
		
	}
	$result .="
		<tr bgcolor='#D9EDF7'>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<th>Grand Total: </th>
		<th colspan='3'><div id='grand_total'>".number_format($total,2)."</div></th>
		<td></td>
		<td></td>
		</tr>
		 </table>
		";
	$result .= "</table>";
	$preauth_amount = $total;
	echo $result;
?>

<div class="row" style="margin-top: 10px;">
	<div class="col-md-12">
		 <input type="hidden" class="form-control" name="charge_visit_id" id="charge_visit_id" placeholder="Middle Name" value="<?php echo $visit_id;?>">
		 <input type="hidden" class="form-control" name="charge_patient_id" id="charge_patient_id" placeholder="Patient Id" value="<?php echo $patient_id;?>">
		  <input type="hidden" class="form-control" name="amount" id="amount" placeholder="Middle Name" value="<?php echo $total;?>">
		  <div class="col-md-4">
		  </div>
		
		<div class="col-md-8">

			<div class="form-group">
                <label class="col-md-4 control-label">Invoice: </label>
                
                <div class="col-md-8">
                    <select name="visit_invoice_id" id="visit_invoice_id" class="form-control" required="required">
							
							<?php		
								$doctors = $this->reception_model->get_all_doctors();
								$types = '<option value="">----Select an Invoice----</option>';

                             $invoices_rs = $this->accounts_model->get_patient_invoices($patient_id);


                                if($invoices_rs->num_rows() > 0)
                                {
                                  foreach ($invoices_rs->result() as $key => $value) {
                                    // code...
                                    $visit_invoice_id = $value->visit_invoice_id;
                                    $visit_invoice_number = $value->visit_invoice_number;
                                    // $visit_invoice_type = $value->visit_invoice_type;
                                    $balance = $value->balance;

                                    $payments_value = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
									$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);
                                    $dr_amount = $value->dr_amount;
                                    $cr_amount = $credit_note + $payments_value;
                                    $invoice_date = $value->invoice_date;
                                    $balance = $dr_amount - $cr_amount;


                                      $invoice_type = 1;
                                    

                                    if($cr_amount > 0)
                                    {
                                      $color_checked = 'orange';
                                    }
                                    else if($cr_amount == 0)
                                    {
                                      $color_checked = 'red';
                                    }
                                    else
                                    {
                                      $color_checked = 'white';
                                    }

                                    // var_dump($visit_invoice_id);die();
                                   if($balance > 0)
                                   {
                                   		$types .= '<option value="'.$visit_invoice_id.'" style="background:'.$color_checked.';color:white;"> '.$invoice_date.' # '.$visit_invoice_number.' Bill .'.number_format($dr_amount,2).' Credits.('.number_format($cr_amount,2).') Bal.'.number_format($balance,2).'</option>';
                                   }

                                    
                                    
                                   
                                  }
                                }
                  
                          
							echo $types;
								
							?>
							
					</select>
                </div>
            </div>
			
            <div class="form-group">
                <label class="col-md-4 control-label">Total Amount: </label>
                
                <div class="col-md-8">
                    <input type="text" class="form-control" name="preauth_amount" placeholder="Total Amount" value="<?php echo number_format($preauth_amount,2)?>">
                </div>

            </div>

	    
            <div class="form-group">
                <label class="col-md-4 control-label">Credit note number: </label>
                
                <div class="col-md-8">
                    <input type="text" class="form-control" name="visit_cr_note_number" placeholder="Credit Note number" value="<?php echo $visit_cr_note_number?>" readonly>
                </div>
                
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Credit Note Date: </label>
                
                <div class="col-md-8">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control datepicker" name="credit_note_date" placeholder="Invoice Date" value="<?php echo $created?>">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="center-align" style="margin-top:10px;">
					<button type="submit" class="btn btn-sm btn-success" onclick="return confirm('Are you sure you want to confirm this credit note ? ')"> COMPLETE CREDIT NOTE</button>
				</div>
	                
            </div>
			
		</div>
		
	</div>

</div>
 <?php echo form_close();?>