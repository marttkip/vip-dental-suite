<?php

$result = '';

$result ='      <table class="table table-bordered table-striped table-condensed">
                <thead>
                    <tr>
                        <th></th>
                        <th>#</th>
                        <th>Name</th>
                        <th>Invoice Date</th>
                        <th>Invoice Number</th>
                        <th>Amount Invoiced</th>
                        <th>Balance</th>
                    </tr>
                </thead>
                  <tbody>
            ';
if($query)
{
    if($query->num_rows() > 0)
    {
        foreach ($query->result() as $key => $value) {
            # code...
            $visit_invoice_id = $value->visit_invoice_id;
            $visit_invoice_number = $value->visit_invoice_number;


             $result .= 
                '
                    <tr onclick="add_to_list('.$visit_invoice_id.');">
                              <td>'.form_checkbox($checkbox_data).'<label for="checkbox'.$visit_invoice_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
                        <td >'.$count.'</td>
                        <td >'.$patient_name.'</td>
                        <td>'.$invoice_date.'</td>
                        <td >'.$visit_invoice_number.'</td>
                        <td>'.number_format($invoice_bill,2).'<input type="hidden" class="form-control" colspan="3" name="invoiced_amount'.$visit_invoice_id.'" id="invoiced_amount'.$visit_invoice_id.'" value="'.$invoice_bill.'" />
                        <input type="hidden" class="form-control" colspan="3" name="patient_id'.$visit_invoice_id.'" id="patient_id'.$visit_invoice_id.'" value="'.$patient_id.'"/></td>
                        
                        <td>'.number_format($invoice_balance,2).'</td>
                    </tr> 
                ';
            
        }
    }
}
 $result .= 
        '
          </tbody>
        </table>
        ';
echo $result;

?>