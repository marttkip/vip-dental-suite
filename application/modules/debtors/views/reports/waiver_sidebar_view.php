<div class="col-md-12">
	<div class="row">
		<section class="panel">
			<div class="panel-body">
				
			<form  method="post" id="add-unallocated-payment">

					<div class="row">
						<div class="col-md-12">
							
							<div class="col-md-6">		
								
								
								<div class="form-group">
									<label class="col-md-12 ">AMOUNT TO WRITE OFF</label>
									<div class="col-md-12">
										<input id="amount" class="form-control" name="amount" id="amount" placeholder="" value="<?php echo $balance?>" required="required" readonly>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-12 ">ACCOUNT DEBITED *** (Expense Account i.e. WHT Tax, Commission)</label>
									<div class="col-md-12">
										 <select id="account_from_id" name="account_from_id" class="form-control" required>
                                    
		                                    <?php
		                                    	$changed = '<option value="">--- Account ---</option>';
		                                     if($accounts->num_rows() > 0)
		                                     {
		                                         foreach($accounts->result() as $row):
		                                             // $company_name = $row->company_name;
		                                             $account_name = $row->account_name;
		                                             $account_id = $row->account_id;
		                                             $parent_account = $row->parent_account;

		                                             if($parent_account != $current_parent)
		                                             {
		                                             	  $account_from_name = $this->transfer_model->get_account_name($parent_account);
		                                             	$changed .= '<optgroup label="'.$account_from_name.'">';
		                                             }

		                                           	 $changed .= "<option value=".$account_id."> ".$account_name."</option>";
		                                           	 $current_parent = $parent_account;
		                                           	 if($parent_account != $current_parent)
		                                             {
		                                             	$changed .= '</optgroup>';
		                                             }

		                                         	 
		                                         	
		                                         endforeach;
		                                     }
		                                     echo $changed;
		                                     ?>
		                                      
											    
		                                </select>
									
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12 ">AMOUNT CREDITED *** (Accounts Receivable)</label>
									<div class="col-md-12">

										<select id="account_to_id" name="account_to_id" class="form-control"  required>
                                    
		                                    <?php
		                                    	$changed = '<option value="">--- Account ---</option>';
		                                     if($accounts->num_rows() > 0)
		                                     {
		                                         foreach($accounts->result() as $row):
		                                             // $company_name = $row->company_name;
		                                             $account_name = $row->account_name;
		                                             $account_id = $row->account_id;
		                                             $parent_account = $row->parent_account;

		                                             if($parent_account != $current_parent)
		                                             {
		                                             	  $account_from_name = $this->transfer_model->get_account_name($parent_account);
		                                             	$changed .= '<optgroup label="'.$account_from_name.'">';
		                                             }

		                                           	 $changed .= "<option value=".$account_id."> ".$account_name."</option>";
		                                           	 $current_parent = $parent_account;
		                                           	 if($parent_account != $current_parent)
		                                             {
		                                             	$changed .= '</optgroup>';
		                                             }

		                                         	 
		                                         	
		                                         endforeach;
		                                     }
		                                     echo $changed;
		                                     ?>
		                                      
											    
		                                </select>
										
									</div>
								</div>

								
								
								
								
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-lg-12 ">JOURNAL DATE:</label>
									
									<div class="col-lg-12">
						                <div class="input-group">
						                    <span class="input-group-addon">
						                        <i class="fa fa-calendar"></i>
						                    </span>
						                    <input data-format="YYYY-MM-DD" type="text" data-plugin-datepicker class="form-control datepicker" name="cancellation_date" placeholder="Date"  id="cancellation_date"  value="<?php echo date('Y-m-d')?>" required>
						                </div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12 ">REASON OF CANCELLING</label>
									<div class="col-md-12">
										<textarea id="write_off_description" class="form-control cleditor" name="write_off_description" placeholder="Description..." required="required"></textarea>
									</div>
								</div>
							</div>

						</div>
					</div>
					
					<input type="hidden" name="visit_invoice_id" id="visit_invoice_id" value="<?php echo $visit_invoice_id;?>">
					<input type="hidden" name="visit_id" id="visit_id" value="<?php echo $visit_id;?>">
					<input type="hidden" name="patient_id" id="patient_id" value="<?php echo $patient_id;?>">
								
					
					<div class="row" style="margin-top: 10px">
				        <div class="col-md-12 center-align">
				        	<button type="submit" class="btn btn-sm btn-success " onclick="add_credit_note()">WRITE OF INVOICE</button>	
				        
				        </div>
				    </div>
				</form>
			</div>
		</section>
	</div>
	<div class="row" style="margin-top: 5px;">
		<ul>
			<li style="margin-bottom: 5px;">
				<div class="row">
			        <div class="col-md-12 center-align">
				        	<!-- <div id="old-patient-button" style="display:none">
				        				        		
				        		
				        	</div> -->
				        	<!-- <div> -->
				        		<a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
				        	<!-- </div> -->
				        		
			               
			        </div>
			    </div>
				
			</li>
		</ul>
	</div>
</div>

<script type="text/javascript">
	
	
</script>