
<div class="col-md-12" >
	<div class="col-md-2" >
		<div class="row" style="height: 60vh;overflow-y: scroll;">
			<?php
			$this->db->where('leave_type_id > 0');
			$query = $this->db->get('leave_type');
			$result_items = '';
			if($query->num_rows() > 0)
			{
				foreach($query->result() as $key => $value) {
					// code...
					$leave_type_name = $value->leave_type_name;
					$leave_type_id = $value->leave_type_id;
					$color_code = $value->color_code;
					

					$result_items .='<div class="col-md-12">
										<div class="col-md-1 ">
											<input type="checkbox" name="leave_type'.$leave_type_id.'" checked>
										</div>
										<label class="col-md-2 control-label">
											<div style="height:20px;background-color:'.$color_code.';width:100%"></div>
										</label>
							            <div class="col-md-8">
							                  '.$leave_type_name.'
							            </div>
							        </div>
							            ';
				}
			}


			?>
			<div class="col-md-10">
				<div class="col-md-12" style="margin-bottom:20px;">
					<button class="btn btn-info btn-sm col-md-12 center-align" onclick="add_new_event()"> <i class="fa fa-plus"></i> ADD EVENT </button>
				</div>
				
				<br>
				<h4>LEAVE TYPES</h4>
				<br>
				<?php echo $result_items;?>


				<?php
				$this->db->where('event_type_id > 0');
				$query = $this->db->get('event_type');
				$result_items_events = '';
				if($query->num_rows() > 0)
				{
					foreach($query->result() as $key => $value) {
						// code...
						$event_type_name = $value->event_type_name;
						$event_type_id = $value->event_type_id;
						$color_code = $value->color_code;
						

						$result_items_events .='<div class="col-md-12">
													<div class="col-md-1 ">
														<input type="checkbox" name="event_type'.$event_type_id.'" checked>
													</div>
													<label class="col-md-2 control-label">
														<div style="height:20px;background-color:'.$color_code.';width:100%"></div>
													</label>
										            <div class="col-md-8">
										                  '.$event_type_name.'
										            </div>
										        </div>';
					}
				}


				?>
				<br>
				<div class="col-md-12">
					<h4>OTHER CALENDARS</h4>
					<br>
					<?php echo $result_items_events;?>
				</div>
			</div>
		</div>
		
	</div>
	<div class="col-md-10" >
		<div style="height:90vh;overflow-y:scroll;">
			<div id="calendar"></div>
		</div>
		
	</div>
	
</div>


<script type="text/javascript">
	
	// Code goes here
	$(document).ready(function() {

	    // page is now ready, initialize the calendar...
	    get_valendar_view();
	  

	   
	});



	function AddEvent(){
	  $('#calendar').fullCalendar('renderEvent', ourEvent, true);
	}
	function get_valendar_view()
	{
		
		var config_url = $('#config_url').val();

		

	        	// alert(appointments);
	          
	          	 $('#calendar').fullCalendar({


			        // put your options and callbacks here
			       		schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
						defaultView: 'month',
						yearColumns: 3,
				        // defaultDate: '2014-11-20', 
				        timezone: 'Africa/Nairobi',
				        selectable: true, 
				        // weekNumbers: true,
				        customButtons: 
				        {
			      			
					        printButton: {
					        text: 'print',
					        click: function() {
						          // alert("sdhkasjhdak");
						          // $('#calendar_note').modal();
						          var start_date = window.localStorage.getItem('date_set_old');
						          // alert(start_date);
						          window.open(config_url+'planner/print-planner/'+start_date, '_blank');
						        }
						      },
					          nextButton: {
					                text:'next',
					                click: function () {
										$('#calendar').fullCalendar('next');
									   	var start_date = $('#calendar').fullCalendar('getView').start.format('YYYY-MM-DD');
          								var end_date = $('#calendar').fullCalendar('getView').end.format('YYYY-MM-DD');
	          							$('#calendar').fullCalendar('destroyEvents');
            							callback(events);


					                }
				            	},prevButton: {
					                text:'prev',
					                click: function () {
										$('#calendar').fullCalendar('prev');
										var start_date = $('#calendar').fullCalendar('getView').start.format('YYYY-MM-DD');
          								var end_date = $('#calendar').fullCalendar('getView').end.format('YYYY-MM-DD');
										

										$('#calendar').fullCalendar('destroyEvents');
            							callback(events);

					                }
				            	},

			      		},
			      		header: {
							left: 'title',
							// center: 'title',
							right: 'prevButton,nextButton,printButton'
							// right: 'year,month,basicWeek,basicDay'
						},
						events: function(start, end, timezone, callback) {

								

						        // alert(start.unix());
								window.localStorage.setItem('date_set_old',end.unix());

							
						        $.ajax({
						          url: config_url+'planner/get_uhdc_calendar',
						          // type:'POST',
						          dataType: 'json',
						          data: {
						            start: start.unix(),
						            end: end.unix()
						          },
						          success: function(doc) {
						            var events = [];
						            doc.forEach(function(eventObject) {
						                events.push({
										    id: eventObject.id,
						                    title: eventObject.title,
						                    start: eventObject.start,
						                    end: eventObject.end,
						                    description: eventObject.description,
						                    backgroundColor: eventObject.backgroundColor,
						                    borderColor: eventObject.borderColor
						                });
						            });
						            $('#calendar').fullCalendar('destroyEvents');
						            callback(events);
						        }
						    });
   						},
					    dayClick: function(date, jsEvent, view, resource,event) {
					    		// document.getElementById("loader").style.display = "block";
					    		var config_url = $('#config_url').val();
					    		var start_date =  date.format();
					    		var end_date =  date.format();
					    		
					    	  document.getElementById("sidebar-right").style.display = "block"; 
								document.getElementById("existing-sidebar-div").style.display = "none"; 

								var config_url = $('#config_url').val();
								var data_url = config_url+"planner/add_new_event";
						       $.ajax({
						       type:'POST',
						       url: data_url,
						       data: { start: start_date, end: end_date },
						       dataType: 'text',
						       // processData: false,
						       // contentType: false,
						       success:function(data){
						          // var data = jQuery.parseJSON(data);
						        
							          	document.getElementById("current-sidebar-div").style.display = "block"; 
										$("#current-sidebar-div").html(data);
										
										tinymce.init({
										                selector: ".cleditor",
										               	height: "200"
											            });
										$('.datepicker').datepicker();
										$('.timepicker').timepicker();
							       
							       },
							       error: function(xhr, status, error) {
							       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
							       // document.getElementById("loader").style.display = "none";
							       
							       }
						       });

						      

						  }

			    });

	      
	       
	      


		

	}

	function add_new_event()
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = $('#config_url').val();
		var data_url = config_url+"planner/add_new_event";
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){

			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);
			
			tinymce.init({
			                selector: ".cleditor",
			               	height: "200"
				            });
			$('.datepicker').datepicker();
			$('.timepicker').timepicker();


			// alert(data);
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});

	}


	function close_side_bar()
	{
		// $('html').removeClass('sidebar-right-opened');
		document.getElementById("sidebar-right").style.display = "none"; 
		document.getElementById("current-sidebar-div").style.display = "none"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 
		tinymce.remove();
	}

	function GetCalendarDateRange() {


        var calendar = $('#calendar').fullCalendar('getCalendar');
        var view = calendar.view;
            // alert(view.start._d);
        var start = view.start._d;
        var end = view.end._d;
        var dates = { start: start, end: end };

   
        return dates;
    }
	


</script>

