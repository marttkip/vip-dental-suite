<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Schedule </title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css">	
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/";?>fontawesome/css/font-awesome.css">
		<!-- <script src="<?php echo base_url()."assets/themes/bluish/"?>js/jquery.js"></script>  -->
		<!-- jQuery -->

		<!-- full calendae -->
		<!-- <link href='<?php echo base_url()."assets/fullcalendar/";?>fullcalendar.print.min.css' rel='stylesheet' media='print' />  -->
		<link href='<?php echo base_url()."assets/fullcalendar/";?>fullcalendar.min.css' rel='stylesheet'/>
		
		<script src='<?php echo base_url()."assets/fullcalendar/";?>lib/moment.min.js'></script>
		<script src='<?php echo base_url()."assets/fullcalendar/";?>lib/jquery.min.js'></script>

		
		<style type="text/css">
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom: 5px;}
            .row .col-md-12 table {
                /*border:solid #000 !important;*/
                /*border-width:1px 0 0 1px !important;*/
                font-size:10px;
            }
            .row .col-md-12 th, .row .col-md-12 td {
                /*border:solid #000 !important;*/
                /*border-width:0 1px 1px 0 !important;*/
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{max-height:70px;}
            /*.table {margin-bottom: 0;}*/
            .fc .fc-widget-header
			{
				font-size: 1.3rem !important;
				font-weight: 500 !important;
				padding: 0px 0 !important;
			}
			.fc-time-area .fc-event-container {
			  padding-bottom: 0 !important;
			}
			#datepicker {
		    display: inline-block;
		  }
		  .fc-toolbar.fc-header-toolbar
		  {
		  	margin-bottom: 0.1em !important;
		  }
		  .ui-widget-content span [text='all-day']
		  {
		  	display: none !important;
		  }
		  .table
		  {
		  	margin-bottom: 0px !important;
		  }
		  .bg-info
		  {
		  	margin-bottom:5px !important;
		  }
		  table #bottom-table th
		  {
		  	color: black !important;
		  }
		  table.borderless td, .borderless th .borderless tr {
		    border: none !important;
		    color: black !important;

			}


			.borderless td, .borderless th
			{
				line-height: 1 !important;
				padding: 1px !important;
				/*padding: 5px;*/
			}
			
			table.borderless td
			{
				width: 35% !important;
			}
			
			.head-info
			{
				text-align: center !important;
			}
			.bold
			{
			    font-weight: bold !important;
			}

			.fc table th
			{
				color: #000 !important;
			}

		
			.fc-bgevent
			{
				/*border: 1px solid grey !important;*/
			}
			.fc-business-container
			{
				/*border: 1px solid grey !important;*/
			}

			table {
					    border:solid grey !important;
					    border-width:1px 0 0 1px !important;
					}
					th, td {
					    border:solid lightgrey !important;
					    border-width:0 1px 1px 0 !important;
					    z-index: 1;
					    /*background-color: #fff;*/
					    /*border-bottom: none !important;*/
					}
			table.borderless {
		    	border: none !important;
			}
			.fc-toolbar h2
			{
				font-size: 18px;
			}
			.fc-right
			{
				display: none;
			}
			.fc-time-grid .fc-slats td {
			    height: 3.9em !important;
			}
			#calendar-all .fc-scroller {
			  overflow-x: hidden !important;
			  overflow-y: hidden !important;
			}

			.fc-event .fc-bg {
				z-index: 10;
				background-color: #fff;
				/*opacity: 0.35;*/
				position: absolute !important;
			}
			.fc-time-grid .fc-event-container
			{
				background-color: #000 !important;
			}
        </style>
        <link rel="stylesheet" href="<?php echo base_url()."assets/calendar/";?>yearview.css" />
        	
		  
		  <script src="<?php echo base_url()."assets/calendar/";?>fullcalendar.js"></script>
		  <style type="text/css">
		  	.fc .fc-widget-header {
				background: #800080;
				border-color: #800080;
				color: white;
				font-size: 13px;
				font-size: 1.3rem;
				font-weight: 500;
				padding: 0px 0;
				text-transform: uppercase;
			}
			.fc table th {
				color: #fff !important;
			}
		  </style>
    </head>
    <body class="receipt_spacing" onLoad="">
    	
    	<input type="hidden" id="config_url" value="<?php echo site_url();?>">
    	<div class="col-md-12 left-align ">
			<div class="row">
	        	<div class="col-xs-12 " style="padding-bottom: 5px;">
	            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
	            </div>
	        </div>
	        
        </div>
   
        <div class="row receipt_bottom_border" >
        	<div class="col-md-12">
            	<div  style="padding-left: 20px;padding-right: 20px;" id="wrapper">
					<div id="calendar"></div>
				</div>
            </div>
        	
        </div>

        <script type="text/javascript">
        	

        	// Code goes here
			$(document).ready(function() {

			    // page is now ready, initialize the calendar...

			    var config_url = $('#config_url').val();

				 var url = config_url+"planner/get_uhdc_calendar_old";
			       $.ajax({
			      	type:'POST',
			       	url: url,
			       	data:{planner:1},
					cache:false,
					contentType: false,
					processData: false,
					dataType: "json",
			       success:function(data){
			          // var data = jQuery.parseJSON(data);
			         var appointments = [];
					var total_events = parseInt(data.total_events, 10);

			          	for(i = 0; i < total_events; i++)
						{
						      
				            var data_array = [];	
							data_title = data.title[i];
							data_start = data.start[i];
							data_end = data.end[i];
							data_backgroundColor = data.backgroundColor[i];
							data_borderColor = data.borderColor[i];
							data_allDay = data.allDay[i];
							data_url = data.url[i];
							
							//add the items to an array
							data_array.title = data_title;
							data_array.start = data_start;
							data_array.end = data_end;
							data_array.backgroundColor = data_backgroundColor;
							data_array.borderColor = data_borderColor;
							data_array.allDay = data_allDay;
							data_array.url = data_url;
						
							//console.log(data_array);
							appointments.push(data_array);
						}
			        	

			        	// alert(appointments);
			          
			          	 $('#calendar').fullCalendar({
					        // put your options and callbacks here
					        header: {
									left: '',
									center: 'title',
									// right: 'year,month,basicWeek,basicDay'
								},
								  defaultView: 'year',
								  yearColumns: 3,
						        // defaultDate: '2014-11-20', 
						        timezone: 'Africa/Nairobi',
						        selectable: true, 
						        weekNumbers: true,
						       
					      		events:  appointments
					    });
			          	var start_date = $('#calendar').fullCalendar('getView').start.format('YYYY-MM-DD');
			          	var end_date = $('#calendar').fullCalendar('getView').end.format('YYYY-MM-DD');

			          	// alert(start_date);
			       
			       },
			       error: function(xhr, status, error) {
			       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			       
			       }
			       });
			});

			function AddEvent(){
			  $('#calendar').fullCalendar('renderEvent', ourEvent, true);
			}
        </script>
		<!-- Full Google Calendar - Calendar -->
		<!-- <script src='<?php echo base_url()."assets/fullcalendar/";?>fullcalendar.min.js'></script>
		<script src='<?php echo base_url()."assets/fullcalendar/";?>scheduler.min.css'></script>
		<script src='<?php echo base_url()."assets/fullcalendar/";?>moment.min.js'></script>
		<script src='<?php echo base_url()."assets/fullcalendar/";?>scheduler.min.js'></script> -->

    </body>
</html>


