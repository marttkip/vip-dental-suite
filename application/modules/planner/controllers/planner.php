<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once "./application/modules/admin/controllers/admin.php";
error_reporting(0);
class Planner extends admin 
{
	function __construct()
	{
		parent:: __construct();
		
		
		$this->load->model('planner/planner_model');
		$this->load->model('auth/auth_model');
		$this->load->model('reception/reception_model');
		$this->load->model('messaging/messaging_model');
		
		// if(!$this->auth_model->check_login())
		// {
		// 	redirect('login');
		// }
	}



	public function index() 
	{
		$branch_id = $this->session->userdata('branch_id');
		$branch_name = $this->session->userdata('branch_name');
		$data['title'] = 'Organization Planner';
		$v_data['title'] = $data['title'];
		// var_dump($v_data);die();
		$data['content'] = $this->load->view('planner/planner', $v_data, true);
		
		$this->load->view('admin/templates/calendar_page', $data);
	}
	public function planner_view()
	{
		$leave_result = $this->planner_model->get_all_schedule();
		
		//initialize required variables
		$totals = '';
		$highest_bar = 0;
		$r = 0;
		$data = array();
		
		if($leave_result->num_rows() > 0)
		{
			$result = $leave_result->result();
			
			foreach($result as $res)
			{
				$event_id = $res->event_id;
				$event_name = $res->event_name;
				$event_type = $res->event_type;
				$start_date = $res->start_date;
				$end_date = $res->end_date;


				

				$start_date = date('D M d Y',strtotime($start_date)); 
				$end_date = date('D M d Y',strtotime($end_date)); 
				$time_start = $start_date.' 8:00 AM GMT+0300'; 
				$time_end = $end_date.' 11:59 PM GMT+0300';
				//$color = $this->reception_model->random_color();
				// var_dump($time_end);die();
				if($event_type == 'Leave')
				{
					$color = '#0088CC';
				}
				
				else
				{
					$color = '#b71c1c';
				}
				$leave_days = $this->site_model->calculate_leave_days($start_date, $end_date);
				
				$data['title'][$r] = $event_name;
				$data['start'][$r] = $time_start;
				$data['end'][$r] = $time_end;
				$data['backgroundColor'][$r] = $color;
				$data['borderColor'][$r] = $color;
				$data['allDay'][$r] = TRUE;
				$data['url'][$r] = site_url().'hr/personnel-leave-detail/'.$event_id;
				$r++;
			}
		}
		
		$data['total_events'] = $r;
		echo json_encode($data);
	}

	public function get_uhdc_calendar_old()
	{


		// if(empty($todays_date))
		// {
		// 	parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);

		// 	$epoch2 = $_GET['end'];

		// 	$dt = new DateTime("@$epoch2");  // convert UNIX timestamp to PHP DateTime
		// 	$todays_date =  $dt->format('Y-m-d');
		// }
		// else
		// {
		// 	$dt = new DateTime("@$todays_date");  // convert UNIX timestamp to PHP DateTime
		// 	$todays_date =  $dt->format('Y-m-d');
		// }

		// var_dump($todays_date);die();
		$leave_result = $this->planner_model->get_all_schedule();
		
		//initialize required variables
		$totals = '';
		$highest_bar = 0;
		$r = 0;
		$data = array();
		
		if($leave_result->num_rows() > 0)
		{
			$result = $leave_result->result();
			
			foreach($result as $res)
			{
				$event_id = $res->event_id;
				$event_name = $res->event_name;
				$event_type = $res->event_type;
				$start_date = $res->start_date;
				$end_date = $res->end_date;
				$color_code = $res->color_code;


				

				$start_date = date('D M d Y',strtotime($start_date)); 
				$end_date = date('D M d Y',strtotime($end_date)); 
				$time_start = $start_date.' 8:00 AM GMT+0300'; 
				$time_end = $end_date.' 11:59 PM GMT+0300';
				//$color = $this->reception_model->random_color();
				// var_dump($time_end);die();
				// if($event_type == 'Leave')
				// {
				// 	$color = '#0088CC';
				// }
				
				// else
				// {
					$color = $color_code;
				// }
				$leave_days = $this->site_model->calculate_leave_days($start_date, $end_date);
				
				$data['title'][$r] = $event_name;
				$data['start'][$r] = $time_start;
				$data['end'][$r] = $time_end;
				$data['backgroundColor'][$r] = $color;
				$data['borderColor'][$r] = $color;
				$data['allDay'][$r] = TRUE;
				$data['url'][$r] = site_url().'hr/personnel-leave-detail/'.$event_id;
				$r++;
			}
		}
		
		$data['total_events'] = $r;
		echo json_encode($data);
	}


	public function get_uhdc_calendar()
	{

		$start = '2021-08-01';//$_POST['start'];
		$end = '2021-09-30';//$_POST['start'];

		parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);
		$epoch1 = $_GET['start'];
		$epoch2 = $_GET['end'];

		$dt = new DateTime("@$epoch1");  // convert UNIX timestamp to PHP DateTime
		$start =  $dt->format('Y-m-d');

		$dt = new DateTime("@$epoch2");  // convert UNIX timestamp to PHP DateTime
		$end =  $dt->format('Y-m-d');
	
		$leave_result = $this->planner_model->get_all_schedule($start,$end);
		
		//initialize required variables
		$totals = '';
		$highest_bar = 0;
		$r = 0;
		$data = array();
		
		if($leave_result->num_rows() > 0)
		{
			$result = $leave_result->result();
			
			foreach($result as $res)
			{
				$event_id = $res->event_id;
				$event_name = $res->event_name;
				$event_type = $res->event_type;
				$start_date = $res->start_date;
				$end_date = $res->end_date;
				$color_code = $res->color_code;


				

				$start_date = date('Y-m-d',strtotime($start_date)); 
				$end_date = date('Y-m-d',strtotime($end_date)); 
				$time_start = $start_date.'T08:00:00+0300'; 
				$time_end = $end_date.'T11:00:00+0300';
				//$color = $this->reception_model->random_color();
				// var_dump($time_end);die();
				// if($event_type == 'Leave')
				// {
				// 	$color = '#0088CC';
				// }
				
				// else
				// {
					$color = $color_code;
				// }
				$leave_days = $this->site_model->calculate_leave_days($start_date, $end_date);


				$data['results'][] = array(
											'id'=>$event_id,
										    'title' => $event_name,
										    'start' => $time_start,
										    'end' => $time_end,
										    'description' => $event_type,
										    'backgroundColor'=>$color,
										    'borderColor'=>$color,
										    'className'=>'fc-nonbusiness'
										    
										  );
				
				// $data['title'][$r] = $event_name;
				// $data['start'][$r] = $time_start;
				// $data['end'][$r] = $time_end;
				// $data['backgroundColor'][$r] = $color;
				// $data['borderColor'][$r] = $color;
				// $data['allDay'][$r] = TRUE;
				// $data['url'][$r] = site_url().'hr/personnel-leave-detail/'.$event_id;
				$r++;
			}
		}
		$data['success'] = true;
		// $data['total_events'] = $r;
		echo json_encode($data['results']);
	}
	public function add_new_event()
	{
		$v_data['visit_id'] = 1;

		$start_date = $this->input->post('start');
		$end_date = $this->input->post('end');


		if(!empty($start_date) AND !empty($end_date))
		{
			$v_data['start_date'] = $start_date;
			$v_data['end_date'] = $end_date;
		}
		else
		{
			$v_data['start_date'] = date('Y-m-d');
			$v_data['end_date'] = date('Y-m-d');
		}
		
		
		$page = $this->load->view('add_new_event',$v_data,true);
		echo $page;
	}
	public function add_event()
	{
		$this->form_validation->set_rules('event_name', 'Event Name', 'required');
		$this->form_validation->set_rules('start_date', 'Start Date', 'required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		$this->form_validation->set_rules('start_time', 'Start Time', 'required');
		$this->form_validation->set_rules('end_time', 'End Time', 'required');
		$this->form_validation->set_rules('event_description', 'Description', 'required');
		$this->form_validation->set_rules('event_type_id', 'Event Type', 'required');
		$event_name = $this->input->post('event_name');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		$start_time = $this->input->post('start_time');
		$end_time = $this->input->post('end_time');
		$event_description = $this->input->post('event_description');
		$event_type_id = $this->input->post('event_type_id');

					
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_userdata('error_message', validation_errors());
			// $data['message'] = validation_errors();
			$data['message'] = 'fail';
		}
		else
		{

			$startTime = date("H:i:00", strtotime($start_time));
			$appointment_array['start_date'] = $start_date.'T'.$startTime;

			$endTime = date("H:i:00", strtotime($end_time));
			$appointment_array['end_date'] = $end_date.'T'.$endTime;

			$appointment_array['event_type_id'] = $event_type_id;
			$appointment_array['event_description'] = $event_description;			
			$appointment_array['event_name'] = $event_name;
			$appointment_array['event_duration_status'] = 1;
			$appointment_array['created'] = date('Y-m-d');
			$appointment_array['created_by'] = $this->session->userdata('personnel_id');
			// var_dump($appointment_array);die();

			$this->db->insert('event_duration',$appointment_array);
		
			
			$data['message'] = 'success';
		}

		echo json_encode($data);

	}
	public function print_planner($date)
	{

		$branch_id = $this->session->userdata('branch_id');
		$branch_name = $this->session->userdata('branch_name');
		$data['title'] = 'Online Diary';
		$v_data['title'] = $data['title'];
		$v_data['todays_date'] = $todays_date;
		$v_data['contacts'] = $this->site_model->get_contacts();
		
		$html = $this->load->view('print_planner', $v_data);


	}

}
?>