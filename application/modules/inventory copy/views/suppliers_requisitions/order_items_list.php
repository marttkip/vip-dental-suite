 <?php
      $invoice_where = 'order_supplier.order_id = '.$order_id.' AND order_supplier.order_item_id = order_item.order_item_id AND order_item.product_id = product.product_id';
      $invoice_table = 'order_supplier,product,order_item';
      $invoice_order = 'order_supplier.order_supplier_id';

      $invoice_query = $this->requisition_model->get_items_list($invoice_table, $invoice_where, $invoice_order);

      $result_payment ='<table class="table table-bordered table-striped table-condensed">
                          <thead>
                            <tr>
                              <th >#</th>
                              <th >Product</th>
                              <th >Units</th>
                              <th >Price</th>
                              <th >Discount</th>
                              <th >VAT</th>
                              <th >Total Amount</th>
                            </tr>
                          </thead>
                            <tbody>';
      $grand_amount = 0;
      $total_vat_amount = 0;
      if($invoice_query->num_rows() > 0)
      {
        $x = 0;

        foreach ($invoice_query->result() as $key => $row) {
          // code...
        	$product_id = $row->product_id;
			$product_name = $row->product_name;
			$product_status = $row->product_status;
			// $category_name = $row->product_category_name;
			$reorder_level = $row->reorder_level;
			$store_id = $row->store_id;
			// $opening_quantity = $row->opening_quantity;			
			$product_unitprice = $row->buying_unit_price;
            $product_deleted = $row->product_deleted;
            // $creditor_name = $row->creditor_name;
            // $supplier_invoice_date = $row->supplier_invoice_date;
            // $supplier_invoice_number = $row->supplier_invoice_number;
            $quantity_received = $row->order_item_quantity;
            $pack_size = $row->pack_size;
            $discount = $row->discount;
            $vat = $row->vat;
            $unit_price = $row->unit_price;
			$less_vat = $row->less_vat;
			$vat = $row->vat;
			$discount = $row->discount;
            $selling_unit_price = $row->selling_unit_price;
			

			$units_received = $quantity_received;
			
			$total_amount = $product_unitprice * $quantity_received;
			//status
			if($product_status == 1)
			{
				$status = 'Active';
			}
			else
			{
				$status = 'Disabled';
			}
			if($discount > 0)
			{
				$total_amount = $quantity_received *$product_unitprice;

				$amount_discounted = ($total_amount * (100-$discount))/100;
				$discount = '<span class="pull-left">'.number_format($discount,1).'% </span> <span class="pull-right">  '.number_format($amount_discounted,2).'</span>';
			}
			
			$button = '';
			

			
			$markup = round(($product_unitprice * 1.33), 0);
			$markdown = $markup;//round(($markup * 0.9), 0);


			$grand_amount += $less_vat;				

        

          $x++;
       
  
          $result_payment .= '<tr>
                                  <td>'.$x.'</td>
                                  <td>'.$product_name.'</td>
                                   <td>'.$quantity_received.'</td>
                                   <td>'.number_format($product_unitprice,2).'</td>
                                    <td>'.$discount.'</td>
                                    <td>'.number_format($vat,2).'</td>
                                  <td>'.number_format($less_vat,2).'</td>
                              </tr>';
          // $result_payment .=form_close();
        }
        $result_payment .= '<tr>
                                  <td colspan="6">Total Invoice</td>
                                  <td ><strong>'.number_format($grand_amount,2).'</strong></td>
                              </tr>';

        // display button

        $display = TRUE;
      }
      else {
        $display = FALSE;
      }

      $result_payment .='</tbody>
                      </table>';
      ?>

      <?php echo $result_payment;?>
