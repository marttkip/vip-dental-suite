<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = 0;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>SUPPLIER </th>
						<th>REQUISITION NUMBER </th>
						<th>LPO NUMBER </th>
						<th>REQUISITION DATE</th>
                        <th>STATUS</th>
					</tr>
				</thead>
				 <tbody>
				  
			';
			
			
			
			foreach ($query->result() as $key)
			{
				    $creditor_name = $key->creditor_name;
				    $creditor_id = $key->creditor_id;
	                $requisition_number = $key->requisition_number;
	                $creditor_id = $key->creditor_id;
	                $requisition_id = $key->requisition_id;
	                $order_id = $key->order_id;
	                $order_approval_status = $key->order_approval_status;
	                $requisition_date = $key->requisition_date;
	                $lpo_number = $key->lpo_number;
	             

	                if($order_approval_status == 3)
	                {
	                	$billing_status = 'Waiting for LPO Generation';
	                	$color_code = '';
	                }
	                else if($order_approval_status == 4){
	                	$billing_status = 'Waiting for LPO Approval';
	                	$color_code = 'warning';
	                }
	                else if($order_approval_status == 5){
	                	$billing_status = 'Waiting For Delivery';
	                	$color_code = 'danger';
	                }
	                else if($order_approval_status == 7){
	                	$billing_status = 'Delivery';
	                	$color_code = '';
	                }

	                
				
				$count++;
				$result .= 
							'

								<tr onclick="view_invoice_details('.$requisition_id.','.$order_id.','.$creditor_id.')">
									<td class="'.$color_code.'">'.$count.' </td>
									<td class="'.$color_code.'">'.$creditor_name.'</td>
									<td class="'.$color_code.'">'.$requisition_number.'</td>
									<td class="'.$color_code.'">'.$lpo_number.'</td>
									<td class="'.$color_code.'">'.date('jS M Y',strtotime($requisition_date)).'</td>
									<td class="'.$color_code.'">'.$billing_status.'</td>
								</tr> 
							';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There Are No Invoices";
		}

		echo $result;
?>