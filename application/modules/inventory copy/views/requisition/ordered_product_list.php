<?php

$result = '';

$result ='<table class="table table-hover table-condensed table-bordered ">
              <thead>
                <tr>
                  <th>PRODUCT NAME</th>
                  <th>STORE FROM</th>
                  <th>REQUESTED UNITS</th>
                  <th>UNITS</th>
                  <th>AWARDED UNITS</th>
                   <th>TOTAL ITEMS</th>
                </tr>
              </thead>
              <tbody>
            ';
if($query != null)
{
	if($query->num_rows() > 0)
	{
		foreach ($query->result() as $key => $value) {
			# code...
			$product_id = $value->product_id;
			$product_deductions_id = $value->product_deductions_id;
			$product_name = $value->product_name;
			$store_name = $value->store_name;
			$quantity_given = $value->quantity_given;
			$units = $value->product_deductions_quantity;

			if(empty($units))
			{
				$units = 0;
			}


			 $result .= 
                '
                    <tr >
                        <td>'.$product_name.'</td>
                        <td>'.$store_name.'</td>
                        <td>'.$units.'</td>
                        <td><input type="text" name="units'.$product_deductions_id.'" id="units'.$product_deductions_id.'" class="form-control" value="'.$quantity_given.'" ></td>
                         <td>'.$quantity_given.'</td>
                        <td><a class="btn btn-xs btn-success" onclick="award_item('.$product_deductions_id.','.$requisition_id.','.$order_id.','.$product_id.')"><i class="fa fa-pencil"></i></a></td>
                    </tr> 
                ';
			
		}
	}
}
 $result .= 
        '
          </tbody>
        </table>
        ';
echo $result;


?>