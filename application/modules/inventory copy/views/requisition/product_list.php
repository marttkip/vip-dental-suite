<?php

$result = '';

$result ='<table class="table table-hover table-condensed table-bordered ">
              <thead>
                <tr>
                  <th>Product Name</th>
                  <th>Reorder Level</th>
                  <th>Current Price</th>
                  <th>Current Stock</th>
                  <th>Supplier</th>
                </tr>
              </thead>
              <tbody>
            ';
if($query != null)
{
	if($query->num_rows() > 0)
	{
		foreach ($query->result() as $key => $value) {
			# code...
			$product_id = $value->product_id;
			$product_name = $value->product_name;


			 $result .= 
                '
                    <tr onclick="add_to_list('.$product_id.','.$requisition_id.');">
                        <td>'.$product_name.'</td>
                        <td></td>
                        <td>'.number_format(0,2).'</td>
                      	<td>'.number_format(0,2).'</td>
                        <td></td>
                    </tr> 
                ';
			
		}
	}
}
 $result .= 
        '
          </tbody>
        </table>
        ';
echo $result;

?>