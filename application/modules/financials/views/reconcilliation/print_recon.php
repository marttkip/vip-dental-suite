<?php

$recon_id = $recon_id;


$recon_rs = $this->reconcilliation_model->get_recon_details($recon_id);

if($recon_rs->num_rows() > 0)  
{
	foreach ($recon_rs->result() as $key => $value) {
		# code...
		$account_id = $value->account_id;
		$account_name = $value->account_name;
		$recon_date = $value->recon_date;
		$start_date = $value->start_date;
		$opening_balance = $value->opening_balance;
		$interest_earned = $value->interest_earned;
		$service_charged = $value->service_charged;
		$money_in = $value->money_in;
		$money_out = $value->money_out;
		$money_in += $value->interest_earned;
		$money_out += $value->service_charged;
		$cleared_balance = $value->cleared_balance;
		$ending_balance = $value->ending_balance;


	}
}


$total_cleared_transactions = ($money_in) - ($money_out);
$cleared_balance_checked = $total_cleared_transactions + $opening_balance;


// get all uncleared  

$unclearead_checks_response = $this->reconcilliation_model->get_all_uncleared_checks($recon_date,$recon_id); 

$total_checks = $unclearead_checks_response['total_amount'];
$total_checks_items = $unclearead_checks_response['count'];


$unclearead_deposits_response = $this->reconcilliation_model->get_all_uncleared_deposits($recon_date,$recon_id); 

$total_deposits = $unclearead_deposits_response['total_amount'];
$total_deposits_items = $unclearead_deposits_response['count'];

$total_uncleared_transactions = $total_deposits + $total_checks;



$registered_balance = $cleared_balance + $total_uncleared_transactions;


// get all cleared 
$clearead_checks_response = $this->reconcilliation_model->get_all_uncleared_checks($recon_date,$recon_id,1); 

$total_cleared_checks = $clearead_checks_response['total_amount'];
$total_cleared_checks_items = $clearead_checks_response['count'];


$clearead_deposits_response = $this->reconcilliation_model->get_all_uncleared_deposits($recon_date,$recon_id,1); 

$total_cleared_deposits = $clearead_deposits_response['total_amount'];
$total_cleared_deposits_items = $clearead_deposits_response['count'];

$total_new_transactions = $total_cleared_deposits + $total_cleared_checks;
?>
<?php



	$checked =  ucfirst(strtolower($account_name)). ' For period ending '.date('jS M Y',strtotime($recon_date));

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Bank Reconcilliation</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}

			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>

      <div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align" style="padding: 5px;">
            	<strong>BANK RECONCILLIATION</strong><br>

            	<?php
            	
				 echo $checked;
            	?>

            </div>
        </div>

    	<div class="row">
        	<div style="margin: auto;max-width: 700px;">
				<div class="col-md-12" style="margin-top: 10px;">
					<table class="table table-hover table-bordered table-striped" id="testTable">
						
						<tbody>
							<tr>
								<th  width="80%">  <strong>BEGINING BALANCE</strong></th>
								<td  width="20%"> <?php echo number_format($opening_balance,2)?></td>
							</tr>
							<tr>
								<th colspan="2" style="padding-left:20px;"> Cleared Transactions </th>
							

							</tr>
							<tr>
								<th  style="padding-left:35px;"> Checks and payments  </th>
								<td  > <?php echo number_format(-$money_out,2)?></td>
							

							</tr>
							<tr>
								<th  style="padding-left:35px;"> Deposits and Credits  </th>
								<td  > <?php echo number_format($money_in,2)?></td>
							</tr>
							<tr>
								<th style="padding-left:20px;" > Total Cleared Transactions  </th>
								<td  style="border-top:2px solid !important;" > <?php echo number_format($total_cleared_transactions,2)?></td>
							</tr>
							<tr>
								<th > Cleared Balance  </th>
								<td  style="border-bottom:3px  double  !important;border-top:2px solid !important;"> <strong><?php echo number_format($cleared_balance_checked,2)?></strong></td>
							</tr>

							<!-- SDAK -->
							<tr>
								<th colspan="2" style="padding-left:20px;"> Uncleared Transactions </th>
							

							</tr>
							<tr>
								<th  style="padding-left:35px;"> Checks and payments - <?php echo $total_checks_items;?> items  </th>
								<td  > <?php echo number_format($total_checks,2)?></td>
							

							</tr>
							<tr>
								<th  style="padding-left:35px;"> Deposits and Credits  - <?php echo $total_deposits_items;?> items </th>
								<td  > <?php echo number_format($total_deposits,2);?></td>
							</tr>
							<tr>
								<th style="padding-left:20px;" > Total Uncleared Transactions  </th>
								<td  style="border-top:2px solid !important;" > <strong><?php echo number_format($total_uncleared_transactions,2)?></strong></td>
							</tr>
							<tr>
								<th > Register Balance as at <?php echo $recon_date;?> </th>
								<td style="border-bottom:3px  double  !important;border-top:2px solid !important;" > <strong><?php echo number_format($registered_balance,2)?></strong></td>
							</tr>

							<!-- new transactions -->

							<tr>
								<th colspan="2" style="padding-left:20px;"> New Transactions </th>
							

							</tr>
							<tr>
								<th  style="padding-left:35px;"> Checks and payments - <?php echo number_format($total_cleared_checks_items)?> items  </th>
								<td  > <?php echo number_format($total_cleared_checks,2)?></td>
							

							</tr>
							<tr>
								<th  style="padding-left:35px;"> Deposits and Credits  - <?php echo number_format($total_cleared_deposits_items)?> items  </th>
								<td  > <?php echo number_format($total_cleared_deposits,2)?></td>
							</tr>
							<tr>
								<th style="padding-left:20px;" > Total New Transactions  </th>
								<td style="border-top:2px solid !important;" > <strong><?php echo number_format($total_new_transactions,2)?></strong></td>
							</tr>
							<tr>
								<th > Ending Balance as at <?php echo $recon_date;?> </th>
								<td  style="border-bottom:3px  double  !important;border-top:2px solid !important;"> <strong><?php echo number_format($ending_balance,2)?></strong></td>
							</tr>

						</tbody>

						
					</table>
				
				
				</div>
            </div>
        </div>

    	<div class="row" style="font-style:italic; font-size:11px;">
        	<div class="col-sm-12">
                <div class="col-sm-10 pull-left">
                    <strong>Prepared by: </strong>
                </div>
                <div class="col-sm-2 pull-right">
                    <?php echo date('jS M Y H:i a'); ?>
                </div>
            </div>

        </div>
    </body>

</html>
