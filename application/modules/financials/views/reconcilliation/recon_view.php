<?php 

$recon_id = $recon_id;


$recon_rs = $this->reconcilliation_model->get_recon_details($recon_id);

if($recon_rs->num_rows() > 0)  
{
	foreach ($recon_rs->result() as $key => $value) {
		# code...
		$account_id = $value->account_id;
		$account_name = $value->account_name;
		$recon_date = $value->recon_date;
		$start_date = $value->start_date;
		$opening_balance = $value->opening_balance;
		$interest_earned = $value->interest_earned;
		$service_charged = $value->service_charged;


	}
}

// var_dump($recon_id);die();
?>
<div class="row">
	<input type="hidden" name="recon_id" id="recon_id" value="<?php echo $recon_id;?>">
    <!-- <div class="col-md-12"> -->

        <section class="panel">
            <header class="panel-heading">

                <h2 class="panel-title"><?php echo ucfirst(strtolower($account_name)). ' For period starting'.date('jS M Y',strtotime($recon_date));?></h2>
                
            </header>

            <div class="panel-body">
				<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');

				if(!empty($error))
				{
					echo '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}

				if(!empty($success))
				{
					echo '<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}

				?>
				<div class="col-md-12" style="height: 60vh">

					<div class="col-md-6" style="height: 55vh;overflow-y: scroll;">
						<h4>Cheques and Payments</h4>
						<table class="table table-hover table-stripped table-condensed table-bordered " >
						 	<thead>
								<tr>
								  <th><input type="checkbox" id="mark_all_our" name="mark_all_our" value="1" onclick="mark_all_transaction_out(<?php echo $recon_id;?>,<?php echo $account_id?>)"></th>
								  <th> DATE</th>
								  <th> CHQ #</th>
								  <th> PAYEE </th>
								  <th> AMOUNT </th>
								</tr>
							 </thead>
						  	<tbody id="money-out-div" >

								
							</tbody>
						</table>
							
						
					</div>
					<div class="col-md-6"  style="height: 55vh;overflow-y: scroll;">
						<h4>Deposits and Other Credits</h4>

						<table class="table table-hover table-stripped table-condensed table-bordered " >
						 	<thead>
								<tr>
								  <th><input type="checkbox" id="mark_all_in" name="mark_all_in" value="1" onclick="mark_all_transaction_in(<?php echo $recon_id;?>,<?php echo $account_id?>)"></th>
								  <th> DATE</th>
								  <th> CHQ #</th>
								  <th> PAYEE </th>
								  <th> AMOUNT </th>
								</tr>
							 </thead>
						  	<tbody id="money-in-div" >

								
							</tbody>
						</table>
						
						
							
					</div>
				</div>
				<hr>
				<input type="hidden" name="ending_balance" id="ending_balance">
				<input type="hidden" name="cleared_balance" id="cleared_balance">
				<input type="hidden" name="difference" id="difference">
				<input type="hidden" name="total_deposits" id="total_deposits">
				<input type="hidden" name="total_cheques" id="total_cheques">
				<div class="col-md-12" style="height: 11vh">
					<div class="col-md-6">
						<div class="col-md-6">
							Begining Balance
						</div>
						<div class="col-md-6 align-right">
							<?php echo number_format($opening_balance,2)?>
						</div>
						<div class="col-md-12">
							Transactions you have marked cleared
						</div>
						<div class="col-md-6">
							Deposits and other credits
						</div>
						<div class="col-md-6 align-right">
							<div id="total-money-in-div"></div>
						</div>
						<div class="col-md-6">
							Cheques and Other Payments
						</div>
						<div class="col-md-6 align-right">
							<div id="total-money-out-div"></div>
						</div>
					</div>
					<div class="col-md-6" style="border: 1px solid grey;">
						<div class="col-md-6">
							Service Charged
						</div>
						<div class="col-md-6 align-right">
							<div id="service-charged-div"></div>
						</div>
						
						<div class="col-md-6">
							Interest Earned
						</div>
						<div class="col-md-6 align-right">
							<div id="interest-earned-div"></div>
						</div>
						<div class="col-md-6">
							Ending Balance
						</div>
						<div class="col-md-6 align-right">
							<div id="total-ending-balance"></div>
						</div>
						<div class="col-md-6">
							Cleared Balance
						</div>
						<div class="col-md-6 align-right">
							<div id="total-cleared-balance"></div>
						</div>
						<div class="col-md-6">
							Difference
						</div>
						<div class="col-md-6 align-right">
							<div id="total-difference"></div>
						</div>
					</div>
				</div>
				<hr>
				<div class="col-md-12" style="height: 6vh">
					<div class="pull-right" style="margin-top: 20px;">
						
						<a  onclick="edit_recon_detail(<?php echo $recon_id;?>)" class="btn btn-sm btn-warning "> EDIT RECON DETAILS </a>
						<a  onclick="complete_reconcilliation(<?php echo $recon_id;?>)" class="btn btn-sm btn-success"> RECONCILE NOW </a>
					</div>
				</div>

				
				
          	</div>
		</section>
    <!-- </div> -->
</div>
<script type="text/javascript">
	
$(document).ready(function()
{
	var recon_id = $('#recon_id').val();
	// alert(recon_id);
	get_money_out(recon_id);
	get_money_in(recon_id);
	get_total_money(recon_id);

	
});

function get_money_out(recon_id)
{
	
	var config_url = $('#config_url').val();
    var data_url = config_url+"financials/reconcilliation/get_money_out/"+recon_id;
    
    $.ajax({
	    type:'POST',
	    url: data_url,
	    data:{account_payment_id: 1},
	    dataType: 'text',
	    success:function(data){
	    	// alert(data);
	        $("#money-out-div").html(data);

	    },
	    error: function(xhr, status, error) {
	    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	    alert(error);
	    }

    });
}

function get_money_in(recon_id)
{
	var config_url = $('#config_url').val();
    var data_url = config_url+"financials/reconcilliation/get_money_in/"+recon_id;
    //window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{account_payment_id: 1},
    dataType: 'text',
    success:function(data){

        $("#money-in-div").html(data);

    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
}


function get_total_money(recon_id)
{
	
	var config_url = $('#config_url').val();
    var data_url = config_url+"financials/reconcilliation/get_total_recons/"+recon_id;
    
    $.ajax({
    type:'POST',
    url: data_url,
    data:{account_payment_id: 1},
    dataType: 'text',
    success:function(data){
    	// alert(data);
    	 var data = jQuery.parseJSON(data);
        $("#total-money-out-div").html(data.total_money_out);
        $("#total-money-in-div").html(data.total_money_in);
        
        $("#total-ending-balance").html(data.total_ending_balance);
        $("#total-difference").html(data.total_difference);
        $("#total-cleared-balance").html(data.total_cleared_balance);
         $("#service-charged-div").html(data.service_charged);
          $("#interest-earned-div").html(data.interest_earned);
        document.getElementById("ending_balance").value = data.ending_balance;
        document.getElementById("cleared_balance").value = data.cleared_balance;
        document.getElementById("difference").value = data.difference;
        document.getElementById("total_deposits").value = data.money_in;
        document.getElementById("total_cheques").value = data.money_out;

        


    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
}
function mark_transaction_out(transaction_id,recon_id,type)
{
	var config_url = $('#config_url').val();
    var data_url = config_url+"financials/reconcilliation/update_transaction/"+transaction_id+"/"+recon_id+"/"+type;
    //window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{account_payment_id: 1},
    dataType: 'text',
    success:function(data){

        get_money_out(recon_id);
		get_money_in(recon_id);
		get_total_money(recon_id);

    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
}

function mark_transaction_in(transaction_id,recon_id,type)
{
	var config_url = $('#config_url').val();
    var data_url = config_url+"financials/reconcilliation/update_transaction_in/"+transaction_id+"/"+recon_id+"/"+type;
    //window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{account_payment_id: 1},
    dataType: 'text',
    success:function(data){

        get_money_out(recon_id);
		get_money_in(recon_id);
		get_total_money(recon_id);

    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
}

function complete_reconcilliation(recon_id)
{
	var res = confirm('Are you sure you want to complate this reconcilliation');

	if(res)
	{

		var config_url = $('#config_url').val();
	    var data_url = config_url+"financials/reconcilliation/complete_reconcilliation/"+recon_id;
	    //window.alert(data_url);

	    var ending_balance = $('#ending_balance').val();
	    var cleared_balance = $('#cleared_balance').val();
	    var difference = $('#difference').val();
	    var total_deposits = $('#total_deposits').val();
	    var total_cheques = $('#total_cheques').val();

	    var difference = $('#difference').val();
	    $.ajax({
	    type:'POST',
	    url: data_url,
	    data:{ending_balance: ending_balance,cleared_balance: cleared_balance, difference: difference,total_deposits: total_deposits, total_cheques: total_cheques},
	    dataType: 'text',
	    success:function(data){
	    	var data = jQuery.parseJSON(data);

	    	if(data.message == 'success')
	    	{

	    		window.location.href = config_url+'accounting/bank-reconcilliation';

	    	}
	    	else
	    	{
	    		alert('Something went wrong. Please try again');
	    	}
	      

	    },
	    error: function(xhr, status, error) {
	    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	    alert(error);
	    }

	    });
	}
}
function edit_recon_detail(recon_id)
{
	document.getElementById("sidebar-right").style.display = "block"; 

    var config_url = $('#config_url').val();
    var data_url = config_url+"financials/reconcilliation/edit_recon/"+recon_id;
    //window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{account_payment_id: 1},
    dataType: 'text',
    success:function(data){

        document.getElementById("current-sidebar-div").style.display = "block"; 
        $("#current-sidebar-div").html(data);

         $('.datepicker').datepicker({
				    format: 'yyyy-mm-dd'
				});
          get_account_reconcilliation(recon_id);

    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
}
function close_side_bar()
{
    // $('html').removeClass('sidebar-right-opened');
    document.getElementById("sidebar-right").style.display = "none"; 
    document.getElementById("current-sidebar-div").style.display = "none"; 
    document.getElementById("existing-sidebar-div").style.display = "none"; 
    tinymce.remove();
}

   function get_account_reconcilliation(recon_id)
    {
    	var config_url = $('#config_url').val();
        var data_url = config_url+"financials/reconcilliation/get_reconcilliation_detail/"+recon_id;
        // window.alert(data_url);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{account_payment_id: 1},
        dataType: 'text',
        success:function(data){

             var data = jQuery.parseJSON(data);

            if(data.message == "success")
            {
              
            	document.getElementById("ending_balance2").value = data.ending_balance;
            	document.getElementById("recon_date2").value = data.start_date;
            	document.getElementById("opening_balance2").value = data.account_opening_balance;
            	document.getElementById("interest_earned2").value = data.interest_earned;
            	document.getElementById("service_charged2").value = data.service_charged;
            	document.getElementById("interest_date2").value = data.interest_date;
            	document.getElementById("charged_date2").value = data.charged_date;
            	$("#opening-balance2").html(data.account_opening_balance);
            	$("#last-recon2").html(data.start_date);


            
            }
            else
            {
                alert('Please ensure you have added included all the items');
            }

        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
    }

     $(document).on("submit","form#update-recon-detail",function(e)
    {
       var res = confirm('Are you sure you want to update this reconcilliation ?');

       if(res)
       {


	        e.preventDefault();
	        // myApp.showIndicator();
	        
	        var form_data = new FormData(this);

	        // alert(form_data);

	        var config_url = $('#config_url').val(); 
	         var recon_id = $('#recon_id2').val();    

	         var url = config_url+"financials/reconcilliation/edit_reconcilliation/"+recon_id;
	        $.ajax({
	        type:'POST',
	        url: url,
	        data:form_data,
	        dataType: 'text',
	        processData: false,
	        contentType: false,
	        success:function(data)
	        {
	          var data = jQuery.parseJSON(data);

	            if(data.message == "success")
	            {
	              

	             	get_money_out(recon_id);
					get_money_in(recon_id);
					get_total_money(recon_id);
					close_side_bar();
	            
	            }
	            else
	            {
	                alert('Please ensure you have added included all the items');
	            }

	        },
	        error: function(xhr, status, error) {
	        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	        }
	        });
	    }
         
        
    });

     function mark_all_transaction_out(recon_id,account_id)
     {
     

     	var checkedValue = null; 
		var inputElements = document.getElementsByName('mark_all_our');
		for(var i=0; inputElements[i]; ++i){
		      if(inputElements[i].checked){
		           checkedValue = inputElements[i].value;
		           break;
		      }
		}
		     	

		if(checkedValue == null)
		{
			checkedValue = 0;
		}

		// alert(checkedValue);


     	var config_url = $('#config_url').val();
	    var data_url = config_url+"financials/reconcilliation/update_all_transaction/"+recon_id+"/"+checkedValue+"/"+account_id;
	    //window.alert(data_url);
	    $.ajax({
	    type:'POST',
	    url: data_url,
	    data:{account_payment_id: 1},
	    dataType: 'text',
	    success:function(data){

	        get_money_out(recon_id);
			get_money_in(recon_id);
			get_total_money(recon_id);

	    },
	    error: function(xhr, status, error) {
	    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	    alert(error);
	    }

	    });
     }

     function mark_all_transaction_in(recon_id,account_id)
     {
     

     	var checkedValue = null; 
		var inputElements = document.getElementsByName('mark_all_in');
		for(var i=0; inputElements[i]; ++i){
		      if(inputElements[i].checked){
		           checkedValue = inputElements[i].value;
		           break;
		      }
		}
		     	

		if(checkedValue == null)
		{
			checkedValue = 0;
		}

		// alert(checkedValue);


     	var config_url = $('#config_url').val();
	    var data_url = config_url+"financials/reconcilliation/update_all_transaction_in/"+recon_id+"/"+checkedValue+"/"+account_id;
	    //window.alert(data_url);
	    $.ajax({
	    type:'POST',
	    url: data_url,
	    data:{account_payment_id: 1},
	    dataType: 'text',
	    success:function(data){

	        get_money_out(recon_id);
			get_money_in(recon_id);
			get_total_money(recon_id);

	    },
	    error: function(xhr, status, error) {
	    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	    alert(error);
	    }

	    });
     }


</script>
