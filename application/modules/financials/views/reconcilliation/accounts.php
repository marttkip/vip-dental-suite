<!-- search -->
<?php echo $this->load->view('search/search_reconlist', '', TRUE);?>
<!-- end search -->

<div class="row">
    <!-- <div class="col-md-12"> -->

        <section class="panel">
            <header class="panel-heading">

                <h2 class="panel-title"><?php echo ucfirst(strtolower($title));?></h2>
                
                <a class="btn btn-sm btn-primary pull-right" onclick="add_new_recon()"  style="margin-top: -25px; margin-right: 5px;"><i class="fa fa-plus"></i> Add Bank Reconcilliation</a>
            </header>

            <div class="panel-body">
				<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');

				if(!empty($error))
				{
					echo '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}

				if(!empty($success))
				{
					echo '<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}

				$search = $this->session->userdata('vendor_expense_search');
				$search_title = $this->session->userdata('vendor_expense_title_search');

				if(!empty($search))
				{
					echo '
					<a href="'.site_url().'financials/company_financial/close_creditor_expense_ledger" class="btn btn-warning btn-sm ">Close Search</a>
					';
					echo $search_title;
				}
				$reconcilliation_rs = $this->reconcilliation_model->get_all_reconcilliations();
				?>
				<table class="table table-hover table-stripped table-condensed table-bordered ">
				 	<thead>
						<tr>
						  <th>Bank Account</th>
						  <th>Reconcilliation Date</th>
						  <th>Opening Balance</th>
						  <th>Cheques & Payments</th>
						  <th>Deposits & Credits</th>
                          <th>Ending Balance</th>
                          <th>Status</th>
                          <th colspan="2">Action</th>
						</tr>
					 </thead>
				  	<tbody>
				  		<?php
				  		$result = '';
				  		if($reconcilliation_rs->num_rows() > 0)
				  		{
				  			foreach ($reconcilliation_rs->result() as $key => $value) {
				  				# code...
				  				$recon_date = $value->recon_date;
				  				$recon_id = $value->recon_id;
				  				$money_in = $value->money_in;
				  				$recon_id = $value->recon_id;
				  				$account_name = $value->account_name;
				  				$money_out = $value->money_out;
				  				$opening_balance = $value->opening_balance;
				  				$ending_balance = $value->ending_balance;

								$recon_status = $value->recon_status;

								if($recon_status == 0)
								{
									$status = 'Open';
								}
								else
								{
									$status = 'Closed';
								}


								if($recon_status == 2)
								{
									$button = '<td><a href="'.site_url().'recon-report/'.$recon_id.'" class="btn btn-xs btn-warning">Recon Summary</a></td>';
								}
								else
								{
									$button = '';
								}


				  				$result .= '<tr>
				  								<td>'.$account_name.'</td>
				  								<td>'.date('jS M Y',strtotime($recon_date)).'</td>
				  								<td>'.number_format($opening_balance,2).'</td>
				  								<td>'.number_format($money_out,2).'</td>
				  								<td>'.number_format($money_in,2).'</td>
				  								<td>'.number_format($ending_balance,2).'</td>
				  								<td>'.$status.'</td>
				  								<td><a href="'.site_url().'view-recon-details/'.$recon_id.'" class="btn btn-xs btn-success">View Details</a></td>
				  								'.$button.'
				  								
				  							</tr>';
				  			}
				  		}
				  		echo $result;
				  		?>
					</tbody>
				</table>
          	</div>
		</section>
    <!-- </div> -->
</div>

<script type="text/javascript">
	function add_new_recon()
	{
		document.getElementById("sidebar-right").style.display = "block"; 
            // document.getElementById("existing-sidebar-div").style.display = "none"; 

        var config_url = $('#config_url').val();
        var data_url = config_url+"financials/reconcilliation/add_new_recon";
        //window.alert(data_url);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{account_payment_id: 1},
        dataType: 'text',
        success:function(data){

            document.getElementById("current-sidebar-div").style.display = "block"; 
            $("#current-sidebar-div").html(data);

             $('.datepicker').datepicker({
					    format: 'yyyy-mm-dd'
					});

        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
	}

	function close_side_bar()
    {
        // $('html').removeClass('sidebar-right-opened');
        document.getElementById("sidebar-right").style.display = "none"; 
        document.getElementById("current-sidebar-div").style.display = "none"; 
        document.getElementById("existing-sidebar-div").style.display = "none"; 
        tinymce.remove();
    }

    function get_account_reconcilliation(account_id)
    {
    	var config_url = $('#config_url').val();
        var data_url = config_url+"financials/reconcilliation/get_account_reconcilliation/"+account_id;
        //window.alert(data_url);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{account_payment_id: 1},
        dataType: 'text',
        success:function(data){

             var data = jQuery.parseJSON(data);

            if(data.message == "success")
            {
              
            	document.getElementById("ending_balance").value = data.ending_balance;
            	document.getElementById("recon_date").value = data.start_date;
            	document.getElementById("opening_balance").value = data.account_opening_balance;
            	document.getElementById("interest_earned").value = data.interest_earned;
            	document.getElementById("service_charged").value = data.service_charged;
            	document.getElementById("interest_date").value = data.interest_date;
            	document.getElementById("charged_date").value = data.charged_date;
            	$("#opening-balance").html(data.account_opening_balance);
            	$("#last-recon").html(data.start_date);


            
            }
            else
            {
                alert('Please ensure you have added included all the items');
            }

        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
    }


      

    $(document).on("submit","form#edit-direct-payment",function(e)
    {
       var res = confirm('Are you sure you want to add this reconcilliation ?');

       if(res)
       {


	        e.preventDefault();
	        // myApp.showIndicator();
	        
	        var form_data = new FormData(this);

	        // alert(form_data);

	        var config_url = $('#config_url').val();    

	         var url = config_url+"financials/reconcilliation/add_reconcilliation";
	        $.ajax({
	        type:'POST',
	        url: url,
	        data:form_data,
	        dataType: 'text',
	        processData: false,
	        contentType: false,
	        success:function(data)
	        {
	          var data = jQuery.parseJSON(data);

	            if(data.message == "success")
	            {
	              

	              window.location.href = config_url+'accounting/bank-reconcilliation';
	            
	            }
	            else
	            {
	                alert('Please ensure you have added included all the items');
	            }

	        },
	        error: function(xhr, status, error) {
	        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	        }
	        });
	    }
         
        
    });

  

    $("#recon_date").datepicker({

            onSelect: function(dateText) {

                console.log("Selected date: " + dateText + "; input's current value: " + this.value);

		        $(this).change();

		    }

		})

        .on("change", function() {

            console.log("Got change event from field");

        });

</script>