<?php echo $this->load->view('search/search_charting', '', TRUE);?>
 <div class="row">
	<div class="col-md-12">
    	<div class="pull-left">
        	<?php
			$search = $this->session->userdata('service_search');
			
			if(!empty($search))
			{
				echo '<a href="'.site_url().'hospital_administration/services/close_service_search" class="btn btn-sm btn-warning"><i class="fa fa-times"></i> Close Search</a>';
			}
			?>
        </div>
        
		<div class="pull-right">
		 <a href="<?php echo site_url()?>administration/add-procedure" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Add procedure </a>

		</div>
	</div>
</div>
<div class="row">
    <div class="col-md-12">

      
 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
      </header>             

          <!-- Widget content -->
                <div class="panel-body">
          
<?php
		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
		
		if(!empty($success))
		{
			echo '
				<div class="alert alert-success">'.$success.'</div>
			';
			$this->session->unset_userdata('success_message');
		}
		
		if(!empty($error))
		{
			echo '
				<div class="alert alert-danger">'.$error.'</div>
			';
			$this->session->unset_userdata('error_message');
		}
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
				'
					<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th><a href="#">Image</a></th>
						  <th><a href="'.site_url().'administration/charting/dental_procedure.dental_procedure_name/'.$order_method.'/'.$page.'">Procedure name</a></th>
						  <th>Status</th>
						  <th colspan="4">Actions</th>
						</tr>
					  </thead>
					  <tbody>
				';
			
			//get all administrators
			$administrators = $this->personnel_model->retrieve_personnel();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				
				$dental_procedure_id = $row->dental_procedure_id;
				// $department_name = $row->department_name;
				$dental_procedure_name = $row->dental_procedure_name;
				$dental_procedure_status = $row->dental_procedure_status;
				$image = $row->image_conotation;
			
				
				//create deactivated status display
				if($dental_procedure_status == 0)
				{
					$status = '<span class="label label-important">Deactivated</span>';
					$button = '<a class="btn btn-info btn-sm" href="'.site_url().'administration/activate-service/'.$dental_procedure_id.'" onclick="return confirm(\'Do you want to activate '.$dental_procedure_name.'?\');" title="Activate '.$dental_procedure_name.'"><i class="fa fa-thumbs-up"></i> Activate</a>';
				}
				//create activated status display
				else if($dental_procedure_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default btn-sm" href="'.site_url().'administration/deactivate-service/'.$dental_procedure_id.'" onclick="return confirm(\'Do you want to deactivate '.$dental_procedure_name.'?\');" title="Deactivate '.$dental_procedure_name.'"><i class="fa fa-thumbs-down"></i> Deactivate</a>';
				}
				
				
				
				$count++;
				$result .= 
					'
						<tr>
							<td>'.$count.'</td>
							<td><img src="'.base_url()."assets/dentine/".$image.'" style=" width: 50px !important;"></td>
							<td>'.$dental_procedure_name.'</td>
							<td><a href="'.site_url().'administration/charting-notations/'.$dental_procedure_id.'" class="btn btn-sm btn-warning"><i class="fa fa-money"></i> Teeth Uploads </a></td>
							<td><a href="'.site_url().'administration/edit-procedure/'.$dental_procedure_id.'" class="btn btn-sm btn-info"> <i class="fa fa-pencil"></i> Edit</a></td>
							<td><a href="'.site_url().'administration/delete-procedure/'.$dental_procedure_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete this service?\')"><i class="fa fa-trash"></i> Delete</a></td>
						</tr> 
					';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no services";
		}
		?>
            <?php echo $result; ?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
        </div>
        </div>
        