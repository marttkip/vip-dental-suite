<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/accounts/controllers/accounts.php";
error_reporting(0);
class Reports extends accounts
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('reception/reception_model');
		$this->load->model('hospital_reports/hospital_reports_model');
		$this->load->model('accounts/accounts_model');
		
		// $this->load->model('nurse/nurse_model');
		// $this->load->model('pharmacy/pharmacy_model');
		// $this->load->model('admin/dashboard_model');
	}

	public function daily_records()
	{

		$module = NULL;
		
		$v_data['branch_name'] = $branch_name;



		$where = 'visit_type.visit_type_id = visit_invoice.bill_to AND visit_invoice.visit_invoice_delete = 0 AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id AND patients.patient_id = visit_invoice.patient_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit_invoice.dentist_id = personnel.personnel_id';
		$table = 'patients, visit_type,visit_invoice,visit_charge,service_charge,personnel';
		$visit_search = $this->session->userdata('drb_search');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			
			
		}
		else
		{
			$where .= ' AND visit_invoice.created = "'.date('Y-m-d').'" ';
		
			$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
			$visit_invoices = ' AND visit_invoice.created = \''.date('Y-m-d').'\'';
			$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

			// $this->session->set_userdata('drb_search', $visit_invoices);
			// $this->session->set_userdata('drb_payments_search', $visit_payments);
		


			$where .= '';

		}
		
		
		$query = $this->hospital_reports_model->get_all_daily_sales($table, $where,1);
		// var_dump($query);die();
		$v_data['query'] = $query;
		$v_data['page'] = 0;
		$v_data['total_patients'] = 1;		
	
		// $page_title = $this->session->userdata('page_title');
		// if(empty($page_title))
		// {
		// 	$page_title = 'Daily Records Report for '.date('Y-m-d');
		// }
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		$v_data['total_visits'] = 20;//$config['total_rows'];
		
		$v_data['module'] = $module;

		// var_dump($v_data);die();
		
		$data['content'] = $this->load->view('reports/daily_sales', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}

	public function print_drb()
	{

		$module = NULL;
		
		$v_data['branch_name'] = $branch_name;



		$where = 'visit_type.visit_type_id = visit_invoice.bill_to AND visit_invoice.visit_invoice_delete = 0 AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id AND patients.patient_id = visit_invoice.patient_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit_invoice.dentist_id = personnel.personnel_id';
		$table = 'patients, visit_type,visit_invoice,visit_charge,service_charge,personnel';
		$visit_search = $this->session->userdata('drb_search');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			
			
		}
		else
		{
			$where .= ' AND visit_invoice.created = "'.date('Y-m-d').'" ';
		
			$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
			$visit_invoices = ' AND visit_invoice.created = \''.date('Y-m-d').'\'';
			$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

			// $this->session->set_userdata('drb_search', $visit_invoices);
			// $this->session->set_userdata('drb_payments_search', $visit_payments);
		


			$where .= '';

		}
		
		
		$query = $this->hospital_reports_model->get_all_daily_sales($table, $where,1);
		// var_dump($query);die();
		$v_data['query'] = $query;
		$v_data['page'] = 0;
		$v_data['total_patients'] = 1;		
	
		// $page_title = $this->session->userdata('page_title');
		// if(empty($page_title))
		// {
		// 	$page_title = 'Daily Records Report for '.date('Y-m-d');
		// }
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		$v_data['total_visits'] = 20;//$config['total_rows'];
		
	
		$data['title'] = 'Daily Sales Records';
		$v_data['title'] = $data['title'];
		$v_data['todays_date'] = $todays_date;
		$v_data['contacts'] = $this->site_model->get_contacts();
		
		$html = $this->load->view('reports/print_drb', $v_data);

	}

	public function export_drb()
	{
		$this->accounting_model->export_drb();
	}

	public function search_drb()
	{
		
	
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$search_title = '';
		
		
		$prev_search = '';
		$prev_table = '';
		
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$visit_invoices = ' AND visit_invoice.created BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$visit_payments = ' AND payments.payment_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$visit_report = ' AND v_transactions_by_date.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$search_title .= 'Report  from '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit = ' AND visit.visit_date = \''.$visit_date_from.'\'';
			$visit_payments = ' AND payments.payment_date = \''.$visit_date_from.'\'';
			$visit_invoices = ' AND visit_invoice.created = \''.$visit_date_from.'\'';
			$visit_report = ' AND v_transactions_by_date.transaction_date = \''.$visit_date_from.'\'';
			$search_title .= 'Report  of '.date('jS M Y', strtotime($visit_date_from)).' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$visit_payments = ' AND payments.payment_date = \''.$visit_date_to.'\'';
			$visit_invoices = ' AND visit_invoice.created = \''.$visit_date_to.'\'';
			$visit_report = ' AND v_transactions_by_date.transaction_date = \''.$visit_date_to.'\'';
			$search_title .= 'Report  of '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else
		{
			$visit = '';
			$visit_payments = '';
			$visit_invoices = '';
			$visit_report ='';
		}

		$search = $visit_invoices;

		// var_dump($visit_invoices);die();
		$this->session->set_userdata('drb_search', $search);
		$this->session->set_userdata('drb_report_search', $visit_report);
		$this->session->set_userdata('drb_visit_search', $visit);
		$this->session->set_userdata('drb_payments_search', $visit_payments);
		$this->session->set_userdata('drb_search_title', $search_title);
		
		redirect('hospital-reports/daily-sales-records');
	}

	public function close_drb_search()
	{
		$this->session->unset_userdata('drb_search');
		$this->session->unset_userdata('drb_report_search');
		$this->session->unset_userdata('drb_visit_search');
		$this->session->unset_userdata('drb_payments_search');
		$this->session->unset_userdata('drb_search_title');

		redirect('hospital-reports/daily-sales-records');

	}
	

}
?>
