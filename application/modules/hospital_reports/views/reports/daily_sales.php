<!-- search -->
<?php echo $this->load->view('search_drb', '', TRUE);?>
<!-- end search -->
<?php
	$where = 'patients.patient_id = v_transactions_by_date.patient_id ';
	$table = 'patients, v_transactions_by_date';
	$visit_search = $this->session->userdata('drb_report_search');
	// var_dump($visit_search);die();
	if(!empty($visit_search))
	{
		$where .= $visit_search;
	
		
		
	}
	else
	{
		$where .= ' AND v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
	
		$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
		$visit_invoices = ' AND v_transactions_by_date.transaction_date= \''.date('Y-m-d').'\'';
		$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

		// $this->session->set_userdata('drb_search', $visit_invoices);
		// $this->session->set_userdata('drb_payments_search', $visit_payments);
	


		$where .= '';

	}
	
	
	$query = $this->hospital_reports_model->get_drb_sales($table, $where,1);
	$v_data['total_patients'] = $query->num_rows();

?>
<?php echo $this->load->view('transaction_statistics', $v_data, TRUE);?>
 
<div class="row">
    <div class="col-md-12">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title">DRB</h2>
            	  <div class="widget-icons pull-right" style="margin-top: -24px !important;">
            	  	<a href="<?php echo site_url().'print-drb'?>" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print DRB</a>
            	  
            	</div>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
          <h5 class="center-align"><?php echo $this->session->userdata('search_title');?></h5>
<?php
		$result = '';
		$search = $this->session->userdata('drb_search');

		// var_dump($search);die();
		if(!empty($search))
		{
			echo '<a href="'.site_url().'hospital_reports/reports/close_drb_search" class="btn btn-sm btn-warning">Close Search</a>';
		}
		// var_dump($query);die();
		//if users exist display them
		//if users exist display them
		$total_invoiced = 0;
		$total_balance = 0;

	

		$result .= '
						<table class="table table-hover table-bordered table-striped table-responsive col-md-12">

							<thead>
								<th>#</th>
								<th>FILE NUMBER</th>
								<th>DOCTOR</th>
								<th>PATIENT NAME</th>
								<th>INVOICE NUMBER</th>
								<th>AMOUNT CHARGED</th>
								<th>AMOUNT PAID</th>
								<th>BALANCE</th>
								<th></th>
						  </thead>
						  <tbody>

						';
		$count = 0;
		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{



				


				$visit_date = date('d.m.Y',strtotime($row->transaction_date));
				
				$patient_id = $row->patient_id;
				$dependant_id = $row->dependant_id;
				$transaction_category = $row->transaction_category;
				$patient_surname = $row->patient_surname;
				$patient_othernames = $row->patient_othernames;
				$patient_first_name = $row->patient_first_name;
				$patient_number = $row->patient_number;


				$count++;

				// get all invoices for the patient

				$where = 'patients.patient_id = v_transactions_by_date.patient_id AND v_transactions_by_date.transactionCategory = "Revenue" AND v_transactions_by_date.patient_id ='.$patient_id;
				$table = 'patients,v_transactions_by_date';
				$visit_search = $this->session->userdata('drb_report_search');
				// var_dump($visit_search);die();
				if(!empty($visit_search))
				{
					$where .= $visit_search;
				
					
					
				}
				else
				{
					$where .= ' AND v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
				
					$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
					$visit_invoices = ' AND v_transactions_by_date.transaction_date= \''.date('Y-m-d').'\'';
					$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

					
				


					$where .= '';

				}
				
				
				$query_two = $this->hospital_reports_model->get_drb_sales_items($table, $where,0);


				if($query_two->num_rows() > 0)
				{
					foreach ($query_two->result() as $key => $value2) {
						# code...

						$dr_amount = $value2->dr_amount;
						$transaction_id = $value2->transaction_id;
						$reference_code = $value2->reference_code;
						$transaction_date = $value2->transaction_date;
						$personnel_name = $value2->personnel_name;
						$payment_type_name = $value2->payment_type_name;

						

						$initials = $this->hospital_reports_model->initials($personnel_name);


						$payments_query = $this->accounts_model->get_visit_invoice_payments($transaction_id,1);
						$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($transaction_id);

						$add_payments = '';
						$payments_value = 0;
						if($payments_query->num_rows() > 0)
						{
							foreach ($payments_query->result() as $key => $value3) {
								# code...
								$payment_method = $value3->payment_method;
								$confirm_number = $value3->confirm_number;
								$payment_date = $value3->payment_date;
								$total_amount = $value3->total_amount;
								

								if($transaction_date == $payment_date)
								{
									$payments_value += $total_amount;
									$add_payments .= 
												'
													<tr>
														<td colspan="2"></td>
														<td colspan="3"><strong>METHOD : </strong>'.$payment_method.' <strong>REF : </strong>'.$confirm_number.' <strong>AMOUNT:</strong>'.number_format($payments_value,2).'  </td>
														
														<td colspan="4"></td>
													</tr> 
												';

								}

								
							}
						}
						$total_payments += $payments_value;


						$dr_amount -= $credit_note;

						$total_invoiced += $dr_amount;

						$balance  = $this->accounts_model->balance($payments_value,$dr_amount);

						$total_balance += $balance;
						$result .= 
									'
										<tr>
											<td>'.$count.'</td>
											<td>'.$patient_number.'</td>
											<td>'.$initials.' </td>
											<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_first_name.'</td>
											<td>'.$reference_code.'</td>
											<td>'.number_format($dr_amount,2).'</td>
											<td>'.number_format($payments_value,2).'</td>
											<td>'.number_format($balance,2).'</td>
											<td>'.strtoupper($payment_type_name).'</td>
										</tr> 
									';

						// get all payments details 

						$result .= $add_payments;
					}

				}




				$where = 'patients.patient_id = v_transactions_by_date.patient_id AND v_transactions_by_date.transactionCategory = "Revenue Payment" AND v_transactions_by_date.transaction_date > v_transactions_by_date.invoice_date  AND v_transactions_by_date.patient_id ='.$patient_id;
				$table = 'patients,v_transactions_by_date';
				$visit_search = $this->session->userdata('drb_report_search');
				// var_dump($visit_search);die();
				if(!empty($visit_search))
				{
					$where .= $visit_search;
				
					
					
				}
				else
				{
					$where .= ' AND v_transactions_by_date.transaction_date > v_transactions_by_date.invoice_date AND v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
				
					$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
					$visit_invoices = ' AND v_transactions_by_date.transaction_date= \''.date('Y-m-d').'\'';
					$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

					
				


					$where .= '';

				}
				
				
				$query_two = $this->hospital_reports_model->get_drb_sales_items($table, $where,0);


				if($query_two->num_rows() > 0)
				{
					foreach ($query_two->result() as $key => $value2) {
						# code...

						$cr_amount = $value2->cr_amount;
						$transaction_id = $value2->transaction_id;
						$reference_code = $value2->reference_code;
						$personnel_name = $value2->personnel_name;
						$payment_type_name = $value2->payment_type_name;

						$initials = $this->hospital_reports_model->initials($personnel_name);

						$payments_query = $this->accounts_model->get_payment_details($transaction_id);
						$add_payments = '';
						$payments_value = 0;

						// var_dump($payments_query->result());die();
						if($payments_query->num_rows() > 0)
						{
							foreach ($payments_query->result() as $key => $value4) {
								# code...
								$payment_method = $value4->payment_method;
								$confirm_number = $value4->confirm_number;
								$total_amount = $value4->payment_item_amount;
								$payments_value += $total_amount;
								

								$add_payments .= 
												'
													<tr>
														<td colspan="2"></td>
														<td colspan="3"><strong>METHOD : </strong>'.$payment_method.' <strong>REF : </strong>'.$confirm_number.' <strong>AMOUNT:</strong>'.number_format($payments_value,2).'  </td>
														
														<td colspan="4"></td>
													</tr> 
												';
							}
						}
						$total_payments += $payments_value;

						$total_balance -= $payments_value;

						$result .= 
									'
										<tr>
											<td>'.$count.'</td>
											<td>'.$patient_number.'</td>
											<td>'.$initials.' </td>
											<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_first_name.'</td>
											<td>-</td>
											<td>-</td>
											<td>'.number_format($payments_value,2).'</td>
											<td>('.number_format($payments_value,2).')</td>
											<td>'.strtoupper($payment_type_name).'</td>
										</tr> 
									';
						$result .= $add_payments;
					}
					
				}


			
				

				
			
			
				
		
			
			}
		}
		
		$result .= 
				'
				
						<tr>
						  <th colspan="4"></th>
						  <th>Total</th>
						  <th>'.number_format($total_invoiced,2).'</th>
						  <th>'.number_format($total_payments,2).'</th>
						  <th>'.number_format($total_balance,2).'</th>
					
						</tr>
			';
		$result .= '
			</tbody>
			</table>';
		
		echo $result;
		
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
    </div>
  </div>