<?php  
$patient_data = $this->reception_model->get_patient_data($patient_id);
$insurance_number = '';
$insurance_company_id = 0;
$scheme_name = '';
if($patient_data->num_rows() > 0)
{
	foreach ($patient_data->result() as $key => $value) {
		# code...
		$insurance_company_id = $value->insurance_company_id;
		$scheme_name = $value->scheme_name;
		$insurance_number = $value->insurance_number;
		$patient_number = $value->patient_number;
	}
}


$branch_session = $this->session->userdata('branch_id');

// var_dump($branch_session);die();
?>
<style type="text/css">
	
</style>
<input type="hidden" name="insurance_company_id" id="insurance_company_id" value="<?php echo $insurance_company_id?>">
<input type="hidden" name="patient_number" id="patient_number" value="<?php echo $patient_number?>">
<?php echo form_open("reception/save_inpatient_visit/".$patient_id, array("class" => "form-horizontal"));?>
<div class="row">
	<div class="col-md-4">
    
		<!-- <h4 class="center-align" style="margin-bottom:10px;">Visit details</h4> -->
		
        <div class="form-group">
			<label class="col-lg-4 control-label">Visit type: </label>
			
			<div class="col-lg-8">
				<select name="visit_type_id" id="visit_type_id" class="form-control">
					<option value="0">----Select a visit type----</option>
					<?php
											
						if($visit_types->num_rows() > 0){

							foreach($visit_types->result() as $row):
								$visit_type_name = $row->visit_type_name;
								$visit_type_id = $row->visit_type_id;

								if($visit_type_id == $insurance_company_id)
								{
									echo "<option value='".$visit_type_id."' selected='selected'>".$visit_type_name."</option>";
								}
								
								else
								{
									echo "<option value='".$visit_type_id."'>".$visit_type_name."</option>";
								}
							endforeach;
						}
					?>
				</select>
			</div>
		 </div>
		 <div id="insured_company" style="display: none;">
            <!-- <a onclick="get_smart_details()" class="btn btn-info btn-xs"> Get Smart Data</a> -->
			<div class="form-group" style="margin-bottom: 15px;">
				<label class="col-lg-4 control-label">Insurance Number: </label>
				<div class="col-lg-8">
					<input type="text" name="insurance_number" id="insurance_number" class="form-control" value="<?php echo $insurance_number;?>">
				</div>
			</div>

            
			<div class="form-group" style="margin-bottom: 15px;">
				<label class="col-lg-4 control-label">Insurance Scheme: </label>
				<div class="col-lg-8">
					<input type="text" name="insurance_description" id="insurance_description" class="form-control" value="<?php echo $scheme_name?>">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 15px;">
				<label class="col-lg-4 control-label">Insurance Limit: </label>
				<div class="col-lg-8">
					<input type="text" name="insurance_limit" id="limit" class="form-control" value="" >
				</div>
			</div>

			<div class="form-group" style="margin-bottom: 15px;">
				<label class="col-lg-4 control-label">Principal Member: </label>
				<div class="col-lg-8">
					<input type="text" name="principal_member" class="form-control" value="">
				</div>
			</div>

			<input type="hidden" name="nr" id="nr" class="form-control" value="">
			<input type="hidden" name="medicalaid_code" id="medicalaid_code" class="form-control" value="">
			<input type="hidden" name="medicalaid_plan" id="medicalaid_plan" class="form-control" value="">
			<input type="hidden" name="global_id" id="global_id" class="form-control" value="">
		</div>
	</div>
	<div class="col-md-4">	

		<div class="form-group">
			<label class="col-lg-4 control-label">Doctor: </label>		
			<div class="col-lg-8">
				 <select name="personnel_id" class="form-control custom-select" id="doctor_idd" >
					<option value="">----Select a Doctor----</option>
					<?php
						$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
						$branch_session = $this->session->userdata('branch_id');



						if(count($doctor) > 0){
							foreach($doctor as $row):
								$fname = $row->personnel_fname;
								$onames = $row->personnel_onames;
								$personnel_id = $row->personnel_id;
								$authorize_invoice_changes = $row->authorize_invoice_changes;
								$branch_id = $row->branch_id;
								
								if($branch_session == $branch_id OR $authorize_invoice_changes == 1)
								{
									echo "<option value='".$personnel_id."'> ".$fname." ".$onames."</option>";
								}
								
								else
								{
									// if($authorize_invoice_changes == 1)
									// {
									// 	echo "<option value='".$personnel_id."'> ".$fname." ".$onames."</option>";
									// }
									
								}
							endforeach;
						}
					?>
				</select>
			</div>
		</div>
       <div class="form-group">
			<label class="col-lg-4 control-label">Account Notes: </label>
			<div class="col-lg-8">
				<textarea name="account_notes" placeholder="Account Notes: Write any information you would like to let the doctor know" class="form-control" rows="5"></textarea>
			</div>
		</div>
	
		<input type="hidden" name="department_id" id="department_id" value="4">
		
		<input type="hidden" name="patient_type_id" value="<?php echo $patient_type_id;?>">
	</div>
	<!--end left -->
	<!-- start right -->
	<div class="col-md-4">

		
		<div class="form-group">
            <label class="col-lg-4 control-label">Branch: </label>
            
            <div class="col-lg-8">
                <select class="form-control" name="branch_id" required="required">
                	
                    <?php
                    	$session_branch_id = $this->session->userdata('branch_id');
						if($branches->num_rows() > 0){
							foreach($branches->result() as $row):
								$branch_name = $row->branch_name;
								$branch_code = $row->branch_code;
								$branch_id = $row->branch_id;

								if($branch_id == $session_branch_id)
								{
									echo "<option value='".$branch_id."#".$branch_code."' selected>".$branch_name."</option>";
								}
								else
								{
									if($session_branch_id == 0)
									{
										echo "<option value='".$branch_id."#".$branch_code."'>".$branch_name."</option>";
									}
									
								}
							endforeach;
						}
					?>
                </select>
            </div>

        </div>

         <h4 class="center-align">VITAL SIGNS</h4>
		<br>
	 	<div class="form-group">
			<label class="col-lg-12">Temparature: </label>
			<div class="col-lg-12">
				<input type="text" class="form-control" name="temparature" value="">
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-12">SpO2: </label>
			<div class="col-lg-12">
				<input type="text" class="form-control" name="spo2" value="">
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-12">Pulse:</label>
			
			<div class="col-lg-12">
                <input type="text" class="form-control" name="pulse" value="">
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-12">P I %:</label>
			
			<div class="col-lg-12">
                <input type="text" class="form-control" name="pi" value="">
			</div>
		</div>


		<div class="form-group" style="display: none;">
			<label class="col-lg-4 control-label">Normal Visit OR Billing? </label>
            <div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="dental_visit" value="0" checked="checked" >
                        Normal
                    </label>
                </div>
            </div>
            
            <div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="dental_visit" value="1">
                        Billing Only
                    </label>
                </div>
            </div>
		</div>



		<!-- <h4 class="center-align" style="margin-bottom:10px;">Appointment details</h4> -->
        
		<div class="form-group" style="display:none;">
			<label class="col-lg-4 control-label">Visit date: </label>
			
			<div class="col-lg-8">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date" placeholder="Visit Date" value="<?php echo date('Y-m-d');?>">
                </div>
			</div>
		</div>
  		
        <div class="form-group" style="display:none;">
			<label class="col-lg-4 control-label">Schedule appointment? </label>
            <div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios2" type="radio" name="appointment_id" value="0" checked="checked" onclick="schedule_appointment(0)">
                        No
                    </label>
                </div>
            </div>
            
            <div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios2" type="radio" name="appointment_id" value="1" onclick="schedule_appointment(1)">
                        Yes
                    </label>
                </div>
            </div>
		</div>
        
        <div id="appointment_details" style="display:none;">
            <!--<div class="form-group">
                <label class="col-lg-4 control-label">Schedule: </label>
                
                <div class="col-lg-8">
                    <a onclick="check_date()" style="cursor:pointer;">[Show Doctor's Schedule]</a><br>
                    <div id="show_doctor" style="display:none;"> 
                        <select name="doctor" id="doctor" onChange="load_schedule()" class="form-control">
                            <option >----Select Doctor to View Schedule---</option>
                                <?php
                                    if(count($doctor) > 0){
                                        foreach($doctor as $row):
                                            $fname = $row->personnel_fname;
                                            $onames = $row->personnel_onames;
                                            $personnel_id = $row->personnel_id;
                                            echo "<option value=".$personnel_id.">".$onames."</option>";
                                        endforeach;
                                    }
                                ?>
                        </select>
                    </div>
                    <div  id="doctors_schedule"> </div>
                </div>
            </div>-->
            
            <div class="form-group">
                <label class="col-lg-4 control-label">Start time : </label>
            
                <div class="col-lg-8">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </span>
                        <input type="text" class="form-control" data-plugin-timepicker="" name="timepicker_start">
                    </div>
                </div>
            </div>
                
            <div class="form-group">
                <label class="col-lg-4 control-label">End time : </label>
                
                <div class="col-lg-8">		
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </span>
                        <input type="text" class="form-control" data-plugin-timepicker="" name="timepicker_end">
                    </div>
                </div>
            </div>
        </div>
	</div>
	<!-- end right -->
</div>
<!-- end of row -->
<br>
<br>
<br>
<div class="center-align">
	<input type="submit" value="Initiate Visit" class="btn btn-info btn-sm" onclick="return confirm('Are you sure you want to initiate this visit ? ')" />
</div>
<!-- <div class="center-align">
	<div class="alert alert-info center-align">Note: For Appointments ensure that you have filled in both sections on this page.</div>
</div> -->
<?php echo form_close();?>
				 <!-- end of form -->
        
<script type="text/javascript" charset="utf-8">
	$(function() {
   		$("#doctor_idd").customselect();
   
	});
	$(document).ready(function(){

		var insurance_company_id =document.getElementById("insurance_company_id").value;
		
		if(insurance_company_id == 0)
		{
			// this is not set yest
  			$('#insured_company').css('display', 'none');
		}
		else if(insurance_company_id == 1) 
		{
			$('#insured_company').css('display', 'none');
		}
		else if(insurance_company_id > 1)
		{
			// alert(insurance_company_id);
			$('#insured_company').css('display', 'block');
		}


		// do_patient_type_function(<?php echo $patient_type_id;?>);
  
	});


 function check_date(){
     var datess=document.getElementById("datepicker").value;
     if(datess){
	  $('#show_doctor').fadeToggle(1000); return false;
	 }
	 else{
	  alert('Select Date First')
	 }
}
function load_schedule(){
	var config_url = $('#config_url').val();
	var datess=document.getElementById("datepicker").value;
	var doctor=document.getElementById("doctor").value;
	var url= config_url+"/reception/doc_schedule/"+doctor+"/"+datess;
	
	  $('#doctors_schedule').load(url);
	  $('#doctors_schedule').fadeIn(1000); return false;	
}

function getXMLHTTP() {
 //fuction to return the xml http object
	var xmlhttp=false;	
	try{
		xmlhttp=new XMLHttpRequest();
	}
	catch(e)	{		
		try{			
			xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e){
			try{
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch(e1){
				xmlhttp=false;
			}
		}
	}
	 	
	return xmlhttp;
}



function getCity(strURL) {		
	
	var req = getXMLHTTP();
	if (req) {
		
		req.onreadystatechange = function() {
			if (req.readyState == 4) {
				// only if "OK"
				if (req.status == 200) {						
					document.getElementById('citydiv').innerHTML=req.responseText;						
				} else {
					alert("There was a problem while using XMLHTTP:\n" + req.statusText);
				}
			}				
		}			
		req.open("GET", strURL, true);
		req.send(null);
	}
			
}
function checks(patient_type){
	var patient_type=document.getElementById('patient_type').value;
	if(patient_type==0){
		alert('Ensure you have selected The patient type');
	}
	
}
function check_department_type()
{
	var myTarget = document.getElementById("department_id").value;
	var department_id = myTarget;

	// alert(myTarget);
	if(department_id == 2){department_id = 7;}
	//get department services
	$.get( "<?php echo site_url();?>reception/get_department_services/"+department_id, function( data ) 
	{
		$( "#department_services" ).html( data );
		
		var myTarget2 = document.getElementById("department_type");
		//var myTarget3 = document.getElementById("patient_type_div");
		// counseling department div
		//var myTarget4 = document.getElementById("counseling_department");
		// end of counseling department div
		
		if((myTarget==10) || (myTarget==4) || (myTarget==2))
		{
			myTarget2.style.display = 'block';
			//myTarget4.style.display = 'none';
			//myTarget3.style.display = 'none';	
		}
		else if(myTarget==12)
		{
			//myTarget4.style.display = 'block';
			myTarget2.style.display = 'none';
			//myTarget3.style.display = 'none';
		}
		else{
			myTarget2.style.display = 'none';
			//myTarget3.style.display = 'block';	
			//myTarget4.style.display = 'none';
		}
	});
}

$(document).on("change","select#patient_insurance_id",function(e)
{
	var patient_type_id = '<?php echo $patient_type_id;?>';
	var service_id = $(this).val();
	
	//get department services
	$.get( "<?php echo site_url();?>reception/get_services_charges/"+patient_type_id+"/"+service_id, function( data ) 
	{
		$( "#services_charges" ).html( data );
	});
});

$(document).on("change","select#department_services",function(e)
{
	var visit_type_id = $("select#visit_type_id").val();
	var service_id = $(this).val();
	// alert(service_id);
	if(visit_type_id == 2 && service_id == 46)
	{
		visit_type_id = 2;
	}
	else
	{
		visit_type_id = 1;
	}
	
	//get service charges
	$.get( "<?php echo site_url();?>reception/get_services_charges/"+visit_type_id+"/"+service_id, function( data ) 
	{
		$( "#services_charges" ).html( data );
		
		$("#services_charges").customselect();
	});
});

$(document).on("change","select#visit_type_id",function(e)
{
	var visit_type_id = $(this).val();
	
	if(visit_type_id != '1')
	{
		$('#insured_company').css('display', 'block');
		// $('#consultation').css('display', 'block');
	}
	else
	{
		$('#insured_company').css('display', 'none');
		// $('#consultation').css('display', 'block');
	}
	
	var department_id = $("select#department_id").val();
	
	//get department services
	//if(department_id == 2){department_id = 7}
	$.get( "<?php echo site_url();?>reception/get_department_services/"+department_id, function( data ) 
	{
		$( "#department_services" ).html( data );
	});
		
	var service_id = $("select#department_services").val();

	// var scheme_url = "<?php echo site_url();?>reception/get_insurance_schemes/"+visit_type_id+"/"+service_id;
	// alert(scheme_url);
	// $.get( scheme_url, function( data ) 
	// {
		
	// 	$( "#insurance_scheme" ).html( data );
	// 	$("#insurance_scheme").customselect();
	// });
	
	//get service charges
	if(visit_type_id == 2 && service_id == 46)
	{
		visit_type_id = 1;
	}
	else
	{
		visit_type_id = 1;
	}
	var services_url = "<?php echo site_url();?>reception/get_services_charges/"+visit_type_id+"/"+service_id;
	// alert(services_url);
	$.get( services_url , function( data ) 
	{
		
		$( "#services_charges" ).html( data );
	});


});

function do_patient_type_function(patient_type_id)
{
	if(patient_type_id == '4')
	{
		$('#insured_company').css('display', 'block');
		// $('#consultation').css('display', 'block');
	}
	else
	{
		$('#insured_company').css('display', 'none');
		// $('#consultation').css('display', 'block');
	}
	var config_url = $('#config_url').val();
	// var data_url = config_url+"/reception/load_charges/"+patient_type_id;
	
	// getCity(data_url);
}

function schedule_appointment(appointment_id)
{
	if(appointment_id == '1')
	{
		$('#appointment_details').css('display', 'block');
	}
	else
	{
		$('#appointment_details').css('display', 'none');
	}
}

function get_smart_details()
{

	var config_url = $('#config_url').val();
 	var url = config_url+"reception/get_smart_information";

 	var patient_number = $('#patient_number').val();

	$.ajax({
		type:'POST',
		url: url,
		data:{patient_number: patient_number},
		dataType: 'text',
		// processData: false,
		// contentType: false,
		success:function(data){
		  var data = jQuery.parseJSON(data);
		  if(data.message == 'success')	 
		  {
		  	// alert('You have successfully sent the message to the patient.');
		  	// $("#global_id").value(data.global_id);
		  	document.getElementById("global_id").value = data.global_id;
		  	document.getElementById("insurance_number").value = data.medicalaid_number;
		  	document.getElementById("limit").value = data.amount;
		  	document.getElementById("medicalaid_code").value = data.medicalaid_code;
		  	document.getElementById("medicalaid_plan").value = data.medicalaid_plan;
		  	document.getElementById("nr").value = data.nr;

		  	
		  }
		  else
		  {
		  	alert(data.result);
		  }
		},
	error: function(xhr, status, error) {
	alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	// document.getElementById("loader").style.display = "none";
	}
	});
}

</script>