<?php
//patient data
$row = $patient->row();
$patient_surname = $row->patient_surname;
$patient_othernames = $row->patient_othernames;
$title_id = $row->title_id;
$patient_date_of_birth = $row->patient_date_of_birth;
$gender_id = $row->gender_id;
$religion_id = $row->religion_id;
$civil_status_id = $row->civil_status_id;
$patient_email = $row->patient_email;
$patient_address = $row->patient_address;
$patient_postalcode = $row->patient_postalcode;
$patient_town = $row->patient_town;
$patient_phone1 = $row->patient_phone1;
$patient_phone2 = $row->patient_phone2;
$patient_kin_sname = $row->patient_kin_sname;
$patient_kin_othernames = $row->patient_kin_othernames;
$relationship_id = $row->relationship_id;
$patient_national_id = $row->patient_national_id;
$insurance_company_id = $row->insurance_company_id;
$patient_number = $row->patient_number;
$branch_id = $row->branch_id;
$next_of_kin_contact = $row->patient_kin_phonenumber1;
$current_patient_number = $row->current_patient_number;
$category_id = $row->category_id;
//echo $gender_id;
//repopulate data if validation errors occur
$validation_error = validation_errors();
                
if(!empty($validation_error))
{
    $patient_surname = set_value('patient_surname');
    $patient_othernames = set_value('patient_othernames');
    $title_id = set_value('title_id');
    $patient_date_of_birth = set_value('patient_dob');
    $gender_id = set_value('gender_id');
    $religion_id = set_value('religion_id');
    $civil_status_id = set_value('civil_status_id');
    $patient_email = set_value('patient_email');
    $patient_address = set_value('patient_address');
    $patient_postalcode = set_value('patient_postalcode');
    $patient_town = set_value('patient_town');
    $patient_phone1 = set_value('patient_phone1');
    $patient_phone2 = set_value('patient_phone2');
    $patient_kin_sname = set_value('patient_kin_sname');
    $patient_kin_othernames = set_value('patient_kin_othernames');
    $relationship_id = set_value('relationship_id');
    $insurance_company_id1 = set_value('insurance_company_id');
    $patient_national_id = set_value('patient_national_id');
    $next_of_kin_contact = set_value('next_of_kin_contact');
    $patient_number = set_value('patient_number');
    $branch_id = set_value('branch_id');

     $current_patient_number = set_value('current_patient_number');
     $category_id = set_value('category_id');

}
?>
 <section class="panel">
    <div class="row">
        <div class="col-md-12">

          <!-- Widget -->
         <header class="panel-heading">
              <h4 class="pull-left"><i class="icon-reorder"></i>Edit <?php echo $patient_surname.' '.$patient_othernames;?></h4>
              <div class="widget-icons pull-right">
                     <a href="<?php echo site_url();?>patients" class="btn btn-success btn-sm pull-right">  Patients List</a>
              </div>
              <div class="clearfix"></div>
        </header>             

            <!-- Widget content -->
        <div class="panel-body">
              <div class="padd">
              <div class="center-align">
                <?php
                    $error = $this->session->userdata('error_message');
                    $success = $this->session->userdata('success_message');
                    
                    if(!empty($error))
                    {
                        echo '<div class="alert alert-danger">'.$error.'</div>';
                        $this->session->unset_userdata('error_message');
                    }
                    
                    if(!empty($validation_error))
                    {
                        echo '<div class="alert alert-danger">'.$validation_error.'</div>';
                    }
                    
                    if(!empty($success))
                    {
                        echo '<div class="alert alert-success">'.$success.'</div>';
                        $this->session->unset_userdata('success_message');
                    }
                ?>
              </div>
                <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal"));?>
                    <div class="row">
                        <div class="col-md-6">
                          
                            <input type="hidden" class="form-control" name="category" placeholder="" value="<?php echo $category_id;?>" readonly>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Name: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_surname" placeholder="Names" value="<?php echo $patient_surname;?>">
                                </div>
                            </div>
                            
                            <div class="form-group" style="display: none">
                                <label class="col-md-4 control-label">Other Names: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_othernames" placeholder="Other Names" value="<?php echo $patient_othernames;?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">File Number: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_number" placeholder="Patient Number" value="<?php echo $patient_number;?>" readonly>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Date of Birth: </label>
                                
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="patient_dob" placeholder="Date of Birth" value="<?php echo $patient_date_of_birth;?>">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Gender: </label>
                                
                                <div class="col-md-8">
                                    <select class="form-control" name="gender_id">
                                         <option value="">Select a gender</option>
                                        <?php
                                            if($genders->num_rows() > 0)
                                            {
                                                $gender = $genders->result();
                                                
                                                foreach($gender as $res)
                                                {
                                                    $db_gender_id = $res->gender_id;
                                                    $gender_name = $res->gender_name;
                                                    
                                                    if($db_gender_id == $gender_id)
                                                    {
                                                        echo '<option value="'.$db_gender_id.'" selected>'.$gender_name.'</option>';
                                                    }
                                                    
                                                    else
                                                    {
                                                        echo '<option value="'.$db_gender_id.'">'.$gender_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            
                            
                           
                            <div class="form-group">
                                <label class="col-md-4 control-label">Email Address: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_email" placeholder="Email Address" value="<?php echo $patient_email;?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Primary Phone: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_phone1" placeholder="Primary Phone" value="<?php echo $patient_phone1;?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Other Phone: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_phone2" placeholder="Other Phone" value="<?php echo $patient_phone2;?>">
                                </div>
                            </div>
                            
                            
                        </div>
                        
                        <div class="col-md-6">
                            
                           
                            
                           
                             <div class="form-group">
                                <label class="col-md-4 control-label">Residence: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_town" placeholder="Residence" value="<?php echo $patient_town;?>">
                                </div>
                            </div>
                            
                           
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Next of Kin Surname: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_kin_sname" placeholder="Kin Surname" value="<?php echo $patient_kin_sname;?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Next of Kin Other Names: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="patient_kin_othernames" placeholder="Kin Other Names" value="<?php echo $patient_kin_othernames;?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Next of Kin Contact: </label>
                                
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="next_of_kin_contact" placeholder="" value="<?php echo $next_of_kin_contact;?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Relationship To Kin: </label>
                                
                                <div class="col-md-8">
                                    <select class="form-control" name="relationship_id">
                                        <?php
                                            if($relationships->num_rows() > 0)
                                            {
                                                $relationship = $relationships->result();
                                                
                                                foreach($relationship as $res)
                                                {
                                                    $db_relationship_id = $res->relationship_id;
                                                    $relationship_name = $res->relationship_name;
                                                    
                                                    if($db_relationship_id == $relationship_id)
                                                    {
                                                        echo '<option value="'.$db_relationship_id.'" selected>'.$relationship_name.'</option>';
                                                    }
                                                    
                                                    else
                                                    {
                                                        echo '<option value="'.$db_relationship_id.'">'.$relationship_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                       <div class="form-group">
                                <label class="col-md-4 control-label">Insurance : </label>
                                
                                <div class="col-md-8">
                                    <select class="form-control" name="insurance_company_id">
                                         <option value="0">Select an insurance Company</option>
                                        <?php
                                            if($insurance->num_rows() > 0)
                                            {
                                                $insurance = $insurance->result();
                                                
                                                foreach($insurance as $res)
                                                {
                                                    $visit_type_id1 = $res->visit_type_id;
                                                    $visit_type_name = $res->visit_type_name;
                                                    
                                                    if($visit_type_id1 == $insurance_company_id)
                                                    {
                                                        echo '<option value="'.$visit_type_id1.'" selected>'.$visit_type_name.'</option>';
                                                    }
                                                    
                                                    else
                                                    {
                                                        echo '<option value="'.$visit_type_id1.'">'.$visit_type_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                              <div class="form-group">
                                <label class="col-lg-4 control-label">How did you know about us? </label>
                                <div class="col-lg-6">
                                    <div id="places-list-view"></div>
                                     
                                    
                                </div>
                                <div class="col-md-2">
                                    <a class="btn btn-warning btn-sm" onclick="add_a_place()"><i class="fa fa-plus"></i> Add a place</a>
                                </div>
                            </div>

                                <div class="form-group">
                                    <div class="radio">
                                        <label class="col-lg-4 control-label">
                                            Specifiy
                                            
                                        </label>
                                          <div class="col-md-8">
                                                <input type="text" class="form-control" name="about_us_view" placeholder="Specify the person /location or how you knew about us">
                                            </div>
                                    </div>
                                </div> 
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Branch Code: </label>
                                
                                <div class="col-lg-8">
                                    <select class="form-control" name="branch_id" required="required">
                                        <option value="">---Select branch---</option>
                                        <?php
                                            $session_branch_id = $this->session->userdata('branch_id');
                                            if($branches->num_rows() > 0){
                                                foreach($branches->result() as $row):
                                                    $branch_name = $row->branch_name;
                                                    $branch_code = $row->branch_code;
                                                    $branch_id = $row->branch_id;

                                                    if($branch_id == $session_branch_id)
                                                    {
                                                        echo "<option value='".$branch_id."#".$branch_code."' selected>".$branch_name."</option>";
                                                    }
                                                    else
                                                    {
                                                        if($session_branch_id == 0)
                                                        {
                                                            echo "<option value='".$branch_id."#".$branch_code."'>".$branch_name."</option>";
                                                        }
                                                        
                                                    }
                                                endforeach;
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                  
                <br/>
                <?php 
                if($category_id == 3)
                {
                     ?>
                    <div class="center-align">
                        <button class="btn btn-warning btn-sm" type="submit" onclick="return confirm('Are you sure you want to confirm the patient details ?')">Confirm patient details </button>
                    </div>
                    <?php

                }else
                {
                    ?>
                    <div class="center-align">
                        <button class="btn btn-info btn-sm" type="submit">Edit Patient</button>
                    </div>
                    <?php

                }
                ?>
                
    <?php echo form_close();?>
              </div>
            </div>
            <!-- Widget ends -->

          </div>
        </div>
</section>


<script type="text/javascript">
    $(function() {
        get_all_places_list();
   
    });

    function add_a_place()
    {
        document.getElementById("sidebar-right").style.display = "block"; 
      // document.getElementById("existing-sidebar-div").style.display = "none"; 

      var config_url = $('#config_url').val();
    $.ajax({
            type:'POST',
            url: config_url+"reception/add_a_place",
            cache:false,
            contentType: false,
            processData: false,
            dataType: "text",
            success:function(data){
                 // var data = jQuery.parseJSON(data);
                // alert();
                // var status_event = data.status;

                // alert(data.results);


                 document.getElementById("current-sidebar-div").style.display = "block"; 
                $('#current-sidebar-div').html(data);

                get_all_places();

                $('.datepicker').datepicker({
                        format: 'yyyy-mm-dd'
                    });

                // $('.datepicker').datepicker();
                $('.timepicker').timepicker();

                
            }
        });
    }

    function get_all_places()
    {
        var config_url = $('#config_url').val();
        $.ajax({
            type:'POST',
            url: config_url+"reception/get_all_places",
            cache:false,
            contentType: false,
            processData: false,
            dataType: "text",
            success:function(data){
           
                $('#places-list').html(data);

            }
        });
    }


$(document).on("submit","form#add-place",function(e)
{


    e.preventDefault();
    

    var res = confirm('Are you sure you want to add a place. ?');


    if(res)
    {
        var form_data = new FormData(this);

        // alert(form_data);
        
        var config_url = $('#config_url').val();    

         var url = config_url+"reception/add_place";
         
           $.ajax({
           type:'POST',
           url: url,
           data:form_data,
           dataType: 'text',
           processData: false,
           contentType: false,
           success:function(data){
            var data = jQuery.parseJSON(data);
            
            if(data.message == "success")
            {
               get_all_places();
              get_all_places_list();
            }
            else
            {
                alert('Please ensure you have added included all the items');
            }
           
           },
           error: function(xhr, status, error) {
           alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
           
           }
           });
    }
     
    
   
    
});


function close_side_bar()
{
    // $('html').removeClass('sidebar-right-opened');
    document.getElementById("sidebar-right").style.display = "none"; 
    document.getElementById("current-sidebar-div").style.display = "none"; 
    // document.getElementById("existing-sidebar-div").style.display = "none"; 
    tinymce.remove();
}


function get_all_places_list()
{
    var config_url = $('#config_url').val();
    $.ajax({
        type:'POST',
        url: config_url+"reception/get_all_places_list",
        cache:false,
        contentType: false,
        processData: false,
        dataType: "text",
        success:function(data){
       
            $('#places-list-view').html(data);

        }
    });
}

function delete_place(place_id)
{

    var res = confirm('Are you sure you want to delete this place. ?');


    if(res)
    {
        var config_url = $('#config_url').val();    

         var url = config_url+"reception/delete_place/"+place_id;
         
           $.ajax({
           type:'POST',
           url: url,
           data:{place_id: place_id},
           dataType: 'text',
           processData: false,
           contentType: false,
           success:function(data){
            var data = jQuery.parseJSON(data);
            
            if(data.message == "success")
            {
               get_all_places();
               get_all_places_list();
            }
            else
            {
                alert('Please ensure you have added included all the items');
            }
           
           },
           error: function(xhr, status, error) {
           alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
           
           }
           });
    }

}

</script>
