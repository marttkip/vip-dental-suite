<?php

class Ledgers_model extends CI_Model
{

	public function get_child_accounts($parent_account_name)
    {
    	$this->db->from('account');
		$this->db->select('*');
		$this->db->where('account_name = "'.$parent_account_name.'" AND account.account_status = 1');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)  
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$account_id = $value->account_id;
			}
			//retrieve all users
			$this->db->from('account');
			$this->db->select('*');
			$this->db->where('parent_account = '.$account_id.' AND account.account_status = 1');
			$query = $this->db->get();
			
			return $query;    	


		}
		else
		{
			return FALSE;
		}

    }


    public function get_account_ledger_statement($account_id)
    {

    	$account_date_from = $this->session->userdata('account_date_from');
    	$account_date_to = $this->session->userdata('account_date_to');

    	if(!empty($account_date_from) AND !empty($account_date_to))
    	{
    		$add = ' AND v_account_ledger.transactionDate BETWEEN "'.$account_date_from.'"  AND "'.$account_date_to.'"';
    	}
    	else if(!empty($account_date_from) AND empty($account_date_to))
    	{
    		$add = ' AND v_account_ledger.transactionDate = "'.$account_date_from.'"';
    	}
    	else if(empty($account_date_from) AND !empty($account_date_to))
    	{
    		$add = ' AND v_account_ledger.transactionDate = "'.$account_date_to.'"';
    	}
    	else
    	{
    		$add = '';
    	}


    	$this->db->from('v_account_ledger');
		$this->db->select('*');
		$this->db->where('accountId = "'.$account_id.'" '.$add);
		$this->db->order_by('transactionDate','ASC');
		$query = $this->db->get();

		return $query;

    }


    public function export_account_ledger()
    {
    	$this->load->library('excel');
		
		$account = $this->session->userdata('account_id');
		$account_name = $this->session->userdata('account_name');

		$account_date_from = $this->session->userdata('account_date_from');
    	$account_date_to = $this->session->userdata('account_date_to');

    	if(!empty($account_date_from) AND !empty($account_date_to))
    	{
    		$add = ' AND v_account_ledger.transactionDate BETWEEN "'.$account_date_from.'"  AND "'.$account_date_to.'"';
    	}
    	else if(!empty($account_date_from) AND empty($account_date_to))
    	{
    		$add = ' AND v_account_ledger.transactionDate = "'.$account_date_from.'"';
    	}
    	else if(empty($account_date_from) AND !empty($account_date_to))
    	{
    		$add = ' AND v_account_ledger.transactionDate = "'.$account_date_to.'"';
    	}
    	else
    	{
    		$add = '';
    	}


		$this->db->from('v_account_ledger');
		$this->db->select('*');
		$this->db->where('accountId = "'.$account.'" '.$add);
		$this->db->order_by('transactionDate','ASC');
		$visits_query = $this->db->get();

		// var_dump($visits_query); die();
		$search_title = $this->session->userdata('search_title');

		$account_date_from = $this->session->userdata('account_date_from');
	    $account_date_to = $this->session->userdata('account_date_to');
		if(!empty($account_date_from) AND !empty($account_date_to))
		{
			$search_title .= ' FROM PERIOD BETWEEN '.$account_date_from.'  AND '.$account_date_to.'';
		}
		else if(!empty($account_date_from) AND empty($account_date_to))
		{
			$search_title .= ' FOR "'.$account_date_from.'"';
		}
		else if(empty($account_date_from) AND !empty($account_date_to))
		{
			$search_title .= ' FOR "'.$account_date_to.'"';
		}
		else
		{
			$search_title .= '';
		}
		
		$title = $search_title;
		$col_count = 0;
		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/

					
			$row_count = 0;
			$report[$row_count][$col_count] = '#';
			$col_count++;
			$report[$row_count][$col_count] = 'Transaction Date';
			$col_count++;
			$report[$row_count][$col_count] = 'Type';
			$col_count++;
			$report[$row_count][$col_count] = 'Description';
			$col_count++;
			$report[$row_count][$col_count] = 'Ref Code';
			$col_count++;
			$report[$row_count][$col_count] = 'Debit';
			$col_count++;
			$report[$row_count][$col_count] = 'Credit';
			$col_count++;
			$report[$row_count][$col_count] = 'Balance';
			$col_count++;
			//display all patient data in the leftmost columns

			$balance = 0;
			$total_dr = 0;
			$total_cr = 0;
			foreach($visits_query->result() as $value)
			{
				$row_count++;
				// $total_invoiced = 0;
				// $visit_date = date('jS M Y',strtotime($row->visit_date));
				// $visit_time = date('H:i a',strtotime($row->visit_time));
				// if($row->visit_time_out != '0000-00-00 00:00:00')
				// {
				// 	$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				// }
				// else
				// {
				// 	$visit_time_out = '-';
				// }
				
				$transactionId = $value->transactionId;
				$accountName = $value->accountName;
				$transactionDate = $value->transactionDate;
				$dr_amount = $value->dr_amount;
				$cr_amount = $value->cr_amount;
				$transactionDescription = $value->transactionDescription;
				$transactionName = $value->transactionClassification;
				$referenceCode = $value->referenceCode;
				$balance += $dr_amount - $cr_amount;
				$total_dr += $dr_amount;
				$total_cr += $cr_amount;

				//display the patient data
				$report[$row_count][$col_count] = $count;
				$col_count++;
				$report[$row_count][$col_count] = $transactionDate;
				$col_count++;
				$report[$row_count][$col_count] = $transactionName;
				$col_count++;
				$report[$row_count][$col_count] = $transactionDescription;
				$col_count++;
				$report[$row_count][$col_count] = $referenceCode;
				$col_count++;
				$report[$row_count][$col_count] = number_format($dr_amount,2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($cr_amount,2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($balance,2);
				$col_count++;
				
				
				
			}

			$row_count++;
			//display the patient data
			$report[$row_count][$col_count] = '';
			$col_count++;
			$report[$row_count][$col_count] = '';
			$col_count++;
			$report[$row_count][$col_count] = '';
			$col_count++;
			$report[$row_count][$col_count] = '';
			$col_count++;
			$report[$row_count][$col_count] = '';
			$col_count++;
			$report[$row_count][$col_count] = number_format($total_dr,2);
			$col_count++;
			$report[$row_count][$col_count] = number_format($total_cr,2);
			$col_count++;
			$report[$row_count][$col_count] = number_format($balance,2);
			$col_count++;
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	
    }
    public function get_all_parent_accounts()
	{
	 
	  $this->db->from('account');
	  $this->db->select('account.*');
	  $this->db->where('parent_account = 0');
	  // $this->db->join('account_staging','account_staging.account_id = account.account_id','LEFT');
	  $this->db->order_by('parent_account','ASC');
	  $query = $this->db->get();

	   return $query;

	}
    public function get_all_child_accounts($account_id = NULL)
	{
	 
	  $this->db->from('account');
	  $this->db->select('*');
	  $this->db->where('parent_account = '.$account_id);

	  $query = $this->db->get();

	   return $query;

	}

	public function get_account_transactions($accountId,$type=NULL)
	{

		if($type == 1)
		{
			$this->db->from('v_account_ledger_by_date');
			$this->db->select('SUM(v_account_ledger_by_date.dr_amount) AS dr_amount,SUM(v_account_ledger_by_date.cr_amount) AS cr_amount,v_account_ledger_by_date.accountName');
			$this->db->where('accountId = '.$accountId);
			$this->db->order_by('accountId','ASC');

			$query = $this->db->get();

			return $query;
		}
		else
		{
			$this->db->from('v_account_ledger_by_date');
			$this->db->select('*');
			$this->db->where('accountId = '.$accountId);
			$this->db->order_by('accountId','ASC');

			$query = $this->db->get();

			return $query;
		}

	}


	public function get_expense_account_transactions($accountId,$type=NULL)
	{

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (v_expenses_ledger.transactionDate >= \''.$date_from.'\' AND v_expenses_ledger.transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND v_expenses_ledger.transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND v_expenses_ledger.transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}




		if($type == 1)
		{
			$this->db->from('v_expenses_ledger');
			$this->db->select('SUM(v_expenses_ledger.dr_amount) AS dr_amount,SUM(v_expenses_ledger.cr_amount) AS cr_amount,v_expenses_ledger.accountName');
			$this->db->where('accountId = '.$accountId.$search_invoice_add);
			$this->db->order_by('accountId','ASC');
				$this->db->group_by('accountId');
			$query = $this->db->get();

			return $query;
		}
		else
		{
			$this->db->from('v_expenses_ledger');
			$this->db->select('*');
			$this->db->where('accountId = '.$accountId.$search_invoice_add);
			$this->db->order_by('accountId','ASC');
		

			$query = $this->db->get();

			return $query;
		}

	}
	public function get_receivables_transactions()
	{

		  $this->db->from('v_transactions_by_date,patients');
		  $this->db->where('v_transactions_by_date.patient_id = patients.patient_id');
		  $this->db->select('v_transactions_by_date.*,patients.patient_surname,patients.patient_othernames');
		  $query = $this->db->get();

		   return $query;

	}


	public function get_payable_transactions()
	{

		  $this->db->from('v_creditor_ledger_aging_by_date');
		  $this->db->select('*');
		  $this->db->where('(v_creditor_ledger_aging_by_date.transactionClassification = "Creditors Invoices" OR v_creditor_ledger_aging_by_date.transactionClassification = "Creditors Invoices Payments")');
		  $query = $this->db->get();

		   return $query;

	}


	public function get_payroll_transactions()
	{

		  $this->db->from('v_payroll_ledger_aging_by_date');
		  $this->db->select('*');
		  $this->db->where('(v_payroll_ledger_aging_by_date.transactionClassification = "Payroll Expense" OR v_payroll_ledger_aging_by_date.transactionClassification = "Payroll Payment")');
		  $query = $this->db->get();

		   return $query;

	}


	public function get_all_services()
	{

		 $this->db->from('service');
		  $this->db->select('*');
		  $this->db->where('service_status = 1 AND service_delete  = 0 ');
		  $this->db->order_by('service_id','ASC');

		  $query = $this->db->get();

		   return $query;

	}


	public function get_receivables_transactions_per_service($service_id = NULL,$type=NULL)
	{


		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  '  AND (transaction_date >= \''.$date_from.'\' AND transaction_date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = '  AND transaction_date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transaction_date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}


		


		if($type == 1)
		{
			$add = '';
			if(!empty($service_id))
			{
				$add = ' AND child_service = '.$service_id;
			}
			$this->db->where('v_transactions_by_date.child_service > 0 AND (v_transactions_by_date.transactionCategory = "Revenue"  OR v_transactions_by_date.transactionCategory = "Credit Note")  '.$add.$search_invoice_add);
			$this->db->from('v_transactions_by_date');
			$this->db->select('SUM(dr_amount) AS dr_amount, SUM(cr_amount) AS cr_amount');
			$this->db->group_by('v_transactions_by_date.child_service');
			$query = $this->db->get();

			return $query;
		}
		else
		{

			$add = '';
			if(!empty($service_id))
			{
				$add = ' AND child_service = '.$service_id;
			}
			$this->db->where('v_transactions_by_date.child_service > 0 AND (v_transactions_by_date.transactionCategory = "Revenue"  OR v_transactions_by_date.transactionCategory = "Credit Note") '.$add.$search_invoice_add);
			$this->db->from('v_transactions_by_date');
			$this->db->select('*');
			$query = $this->db->get();

			return $query;

		}
		

	}
	



	public function export_general_ledger()
	{


		$this->load->library('excel');
		
		$general_ledger_search_title =	$this->session->userdata('general_ledger_search_title');


		if(!empty($general_ledger_search_title))
		{
			$search_title = $general_ledger_search_title;
		}
		else 
		{
			$search_title = 'ALL TIME REPORT';
		}


	
		$visits_query = $this->db->get();

		// var_dump($visits_query); die();
		$search_title = $this->session->userdata('search_title');

		$account_date_from = $this->session->userdata('account_date_from');
	    $account_date_to = $this->session->userdata('account_date_to');
		if(!empty($account_date_from) AND !empty($account_date_to))
		{
			$search_title .= ' FROM PERIOD BETWEEN '.$account_date_from.'  AND '.$account_date_to.'';
		}
		else if(!empty($account_date_from) AND empty($account_date_to))
		{
			$search_title .= ' FOR "'.$account_date_from.'"';
		}
		else if(empty($account_date_from) AND !empty($account_date_to))
		{
			$search_title .= ' FOR "'.$account_date_to.'"';
		}
		else
		{
			$search_title .= '';
		}
		
		$title = $search_title;
		$col_count = 0;
		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/

					
			$row_count = 0;
			$report[$row_count][$col_count] = '#';
			$col_count++;
			$report[$row_count][$col_count] = 'Transaction Date';
			$col_count++;
			$report[$row_count][$col_count] = 'Type';
			$col_count++;
			$report[$row_count][$col_count] = 'Description';
			$col_count++;
			$report[$row_count][$col_count] = 'Ref Code';
			$col_count++;
			$report[$row_count][$col_count] = 'Debit';
			$col_count++;
			$report[$row_count][$col_count] = 'Credit';
			$col_count++;
			$report[$row_count][$col_count] = 'Balance';
			$col_count++;
			//display all patient data in the leftmost columns

			$balance = 0;
			$total_dr = 0;
			$total_cr = 0;
			foreach($visits_query->result() as $value)
			{
				$row_count++;
				// $total_invoiced = 0;
				// $visit_date = date('jS M Y',strtotime($row->visit_date));
				// $visit_time = date('H:i a',strtotime($row->visit_time));
				// if($row->visit_time_out != '0000-00-00 00:00:00')
				// {
				// 	$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				// }
				// else
				// {
				// 	$visit_time_out = '-';
				// }
				
				$transactionId = $value->transactionId;
				$accountName = $value->accountName;
				$transactionDate = $value->transactionDate;
				$dr_amount = $value->dr_amount;
				$cr_amount = $value->cr_amount;
				$transactionDescription = $value->transactionDescription;
				$transactionName = $value->transactionClassification;
				$referenceCode = $value->referenceCode;
				$balance += $dr_amount - $cr_amount;
				$total_dr += $dr_amount;
				$total_cr += $cr_amount;

				//display the patient data
				$report[$row_count][$col_count] = $count;
				$col_count++;
				$report[$row_count][$col_count] = $transactionDate;
				$col_count++;
				$report[$row_count][$col_count] = $transactionName;
				$col_count++;
				$report[$row_count][$col_count] = $transactionDescription;
				$col_count++;
				$report[$row_count][$col_count] = $referenceCode;
				$col_count++;
				$report[$row_count][$col_count] = number_format($dr_amount,2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($cr_amount,2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($balance,2);
				$col_count++;
				
				
				
			}

			$row_count++;
			//display the patient data
			$report[$row_count][$col_count] = '';
			$col_count++;
			$report[$row_count][$col_count] = '';
			$col_count++;
			$report[$row_count][$col_count] = '';
			$col_count++;
			$report[$row_count][$col_count] = '';
			$col_count++;
			$report[$row_count][$col_count] = '';
			$col_count++;
			$report[$row_count][$col_count] = number_format($total_dr,2);
			$col_count++;
			$report[$row_count][$col_count] = number_format($total_cr,2);
			$col_count++;
			$report[$row_count][$col_count] = number_format($balance,2);
			$col_count++;
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}


	
	public function get_account_ledger_by_accounts_opening_balance($array_leads,$year=NULL)
	{
		
		
		$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

		$patients = array();
		
		if($accounts_config_rs->num_rows() > 0)
		{
			foreach ($accounts_config_rs->result() as $key => $value) {
				// code...
				$staing_account_id = $value->account_id;
				$reference_name = $value->reference_name;

				$session_account[$reference_name] = $staing_account_id;

				
			}
		}

		$providers_liability_id = $session_account['providers_liability_id'];
		$providers_wht_id = $session_account['providers_wht_id'];
		$payroll_liability_id = $session_account['payroll_liability_id'];
		$accounts_payable_id = $session_account['accounts_payable_id'];
		$fixed_assets_id = $session_account['fixed_assets_id'];
		$accounts_receivable_id = $session_account['accounts_receivable_id'];
		$suppliers_wht_id = $session_account['suppliers_wht_id'];
		$income_account_id = $session_account['income_account_id'];
		$supplier_opening_balance_id= $session_account['supplier_opening_balance_id'];
		$bank_opening_balance_id = $session_account['bank_opening_balance_id'];
		$provider_opening_balance_id = $session_account['provider_opening_balance_id'];

		
		$date_from = $this->session->userdata('date_from_general_ledger');
		$date_to = $this->session->userdata('date_to_general_ledger');
		$add = '';

		if(!empty($year))
		{
			// $add = ' AND YEAR(transactionDate) = "'.$year.'"';
		}

		if(!empty($date_from) OR !empty($date_to))
    	{
    		
    		$add .=  ' AND DATE(transactionDate) < \''.$date_from.'\' ';
    		
    		
    	}
    	else if(!empty($date_from) AND empty($date_to))
    	{

    		
    		$add .= '  AND DATE(transactionDate) < \''.$date_from.'\'';
    		
    		
    	}
    	else if(empty($date_from) AND !empty($date_to))
    	{
    		
    		$add .= ' AND DATE(transactionDate) < \''.$date_to.'\'';
    		
    	}
    	else
    	{
    		$add .=  '  AND DATE(transactionDate) < \''.date('Y-m-01').'\'';
    	}

    	// if(!empty($year))
    	// {

    	// }

    		
    	$add_item = 'GROUP BY data.accountId';
    	
    	

    	
		$item = '	data.accountId AS accountId,
					data.accountName AS account_name,
				 	SUM(data.dr_amount) AS dr_amount,
				 	SUM(data.cr_amount) AS cr_amount';
    	
		$select  = "
					SELECT 
						".$item."
					FROM 

					(

						SELECT
							`account`.`account_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
						    '' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							account.account_id AS `accountId`,
							account.account_name AS `accountName`,
							CONCAT('Opening Balance as from',' ',`account`.`start_date`) AS `transactionName`,
							CONCAT('Opening Balance as from',' ',' ',`account`.`start_date`) AS `transactionDescription`,
							`account`.`account_opening_balance` AS `dr_amount`,
							'0' AS `cr_amount`,
							`account`.`start_date` AS `transactionDate`,
							`account`.`start_date` AS `createdAt`,
							`account`.`account_status` AS `status`,
							branch_id AS `branch_id`,
							'Income' AS `transactionCategory`,
							'Account Opening Balance' AS `transactionClassification`,
							'' AS `transactionTable`,
							'account' AS `referenceTable`
						FROM
						account,account_type

						WHERE 

						account_type.account_type_id = account.account_type_id AND account.account_opening_balance > 0 AND account.parent_account > 0  AND (account_type.account_type_name = 'Bank' OR account_type.account_type_name ='Capital')

						UNION ALL

	
						SELECT
							`account`.`account_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							account.account_id AS `accountId`,
							account.account_name AS `accountName`,
							CONCAT( 'Opening Balance as from', ' ', `account`.`start_date` ) AS `transactionName`,
							CONCAT( 'Opening Balance as from', ' ', ' ', `account`.`start_date` ) AS `transactionDescription`,
							0 AS `dr_amount`,
							(SELECT COALESCE(SUM(account_opening_balance),0) FROM account,account_type WHERE  account_type.account_type_id = account.account_type_id  AND account.account_opening_balance > 0  AND (
							account_type.account_type_name = 'Bank' 
							OR account_type.account_type_name = 'Capital') ) AS `cr_amount`,
							`account`.`start_date` AS `transactionDate`,
							`account`.`start_date` AS `createdAt`,
							`account`.`account_status` AS `status`,
							branch_id AS `branch_id`,
							'Income' AS `transactionCategory`,
							'Account Opening Balance' AS `transactionClassification`,
							'' AS `transactionTable`,
							'account' AS `referenceTable` 
						FROM
							account,
							account_type 
						WHERE
							account_type.account_type_id = account.account_type_id 
							AND account.parent_account > 0 
							AND account.account_id = ".$bank_opening_balance_id."

						



						UNION ALL

						SELECT
						  	`finance_transfered`.`finance_transfered_id` AS `transactionId`,
						  	`finance_transfer`.`finance_transfer_id` AS `referenceId`,
						  	'' AS `payingFor`,
						  	`finance_transfer`.`reference_number` AS `referenceCode`,
						  	`finance_transfer`.`document_number` AS `transactionCode`,
						  	'' AS `patient_id`,
						    '' AS `recepientId`,
						  	`account`.`parent_account` AS `accountParentId`,
						  	`account_type`.`account_type_name` AS `accountsclassfication`,
						  	`finance_transfered`.`account_to_id` AS `accountId`,
						  	`account`.`account_name` AS `accountName`,
						  	`finance_transfered`.`remarks` AS `transactionName`,
						  	 CONCAT('Amount Received from ',(SELECT account_name FROM account WHERE account_id = finance_transfer.account_from_id ),' Ref. ', `finance_transfer`.`reference_number`) AS `transactionDescription`,
						  	`finance_transfer`.`finance_transfer_amount` AS `cr_amount`,
						     0 AS `cr_amount`,
						  	`finance_transfer`.`transaction_date` AS `transactionDate`,
						  	`finance_transfer`.`created` AS `createdAt`,
						  	`finance_transfer`.`finance_transfer_status` AS `status`,
						  	`finance_transfer`.`branch_id` AS `branch_id`,
						  	'Transfer' AS `transactionCategory`,
						  	'Transfer' AS `transactionClassification`,
						  	'finance_transfer' AS `transactionTable`,
						  	'finance_transfered' AS `referenceTable`
						  FROM
						  `finance_transfer`,finance_transfered,account,account_type
						   WHERE finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id 
						  	AND account.account_id = finance_transfered.account_to_id
						  	AND account_type.account_type_id = account.account_type_id
						  	AND finance_transfer.finance_transfer_deleted = 0

					UNION ALL

						SELECT
						  	`finance_transfer`.`finance_transfer_id` AS `transactionId`,
						  	`finance_transfered`.`finance_transfered_id` AS `referenceId`,
						  	'' AS `payingFor`,
						  	`finance_transfer`.`reference_number` AS `referenceCode`,
						  	`finance_transfer`.`document_number` AS `transactionCode`,
						  	'' AS `patient_id`,
						    '' AS `recepientId`,
						  	`account`.`parent_account` AS `accountParentId`,
						  	`account_type`.`account_type_name` AS `accountsclassfication`,
						  	`finance_transfer`.`account_from_id` AS `accountId`,
						  	`account`.`account_name` AS `accountName`,
						  	`finance_transfer`.`remarks` AS `transactionName`,
						  	CONCAT(' Amount Transfered to ',(SELECT account_name FROM account WHERE account_id = finance_transfered.account_to_id )) AS `transactionDescription`,
						  	0 AS `dr_amount`,
						  	`finance_transfer`.`finance_transfer_amount` AS `cr_amount`,
						  	`finance_transfer`.`transaction_date` AS `transactionDate`,
						  	`finance_transfer`.`transaction_date` AS `transactionDate`,

						  	`finance_transfer`.`finance_transfer_status` AS `status`,
						  	`finance_transfer`.`branch_id` AS `branch_id`,
						  	'Transfer' AS `transactionCategory`,
						  	'Transfer' AS `transactionClassification`,
						  	'finance_transfered' AS `transactionTable`,
						  	'finance_transfer' AS `referenceTable`
						  FROM
							`finance_transfer`,finance_transfered,account,account_type
						  				
						  WHERE finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id 
						  	AND account.account_id = finance_transfer.account_from_id
						  	AND account_type.account_type_id = account.account_type_id
						  	AND finance_transfer.finance_transfer_deleted = 0

						UNION ALL


						SELECT
							`finance_purchase_payment`.`finance_purchase_payment_id` AS `transactionId`,
							'' AS `referenceId`,
							`finance_purchase`.`finance_purchase_id` AS `payingFor`,
							`finance_purchase`.`transaction_number` AS `referenceCode`,
							`finance_purchase`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
						  	finance_purchase.creditor_id AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`finance_purchase_payment`.`account_from_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`finance_purchase`.`finance_purchase_description` AS `transactionName`,
							CONCAT( `finance_purchase`.`finance_purchase_description`) AS `transactionDescription`,
							0 AS `dr_amount`,
							`finance_purchase_payment`.`amount_paid` AS `cr_amount`,
							`finance_purchase`.`transaction_date` AS `transactionDate`,
							`finance_purchase`.`created` AS `createdAt`,
							`finance_purchase_payment`.`finance_purchase_payment_status` AS `status`,
							`finance_purchase`.`branch_id` AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Purchase Payment' AS `transactionClassification`,
							'finance_purchase' AS `transactionTable`,
							'finance_purchase_payment' AS `referenceTable`
						FROM
						`finance_purchase_payment`,finance_purchase,account,account_type
						WHERE finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id AND finance_purchase.finance_purchase_deleted = 0 AND account.account_id = finance_purchase_payment.account_from_id
							AND finance_purchase.finance_purchase_delete = 0 AND account_type.account_type_id = account.account_type_id
							AND finance_purchase.account_to_id > 0 

						UNION ALL

						SELECT
							`finance_purchase`.`finance_purchase_id` AS `transactionId`,
							'' AS `referenceId`,
							`finance_purchase`.`finance_purchase_id` AS `payingFor`,
							`finance_purchase`.`transaction_number` AS `referenceCode`,
							`finance_purchase`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`finance_purchase`.`creditor_id` AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`finance_purchase`.`account_to_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`finance_purchase`.`finance_purchase_description` AS `transactionName`,
							CONCAT( `finance_purchase`.`finance_purchase_description`)  AS `transactionDescription`,
							`finance_purchase_payment`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`finance_purchase`.`transaction_date` AS `transactionDate`,
							`finance_purchase`.`created` AS `createdAt`,
							`finance_purchase`.`finance_purchase_status` AS `status`,
							`finance_purchase`.`branch_id` AS `branch_id`,
							'Expense' AS `transactionCategory`,
							'Purchases' AS `transactionClassification`,
							'finance_purchase' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							`finance_purchase_payment`,finance_purchase,account,account_type
						WHERE finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id AND finance_purchase.finance_purchase_deleted = 0 
						AND account.account_id = finance_purchase.account_to_id
							AND finance_purchase.finance_purchase_delete = 0
							AND account_type.account_type_id = account.account_type_id
							AND finance_purchase.account_to_id > 0 

						UNION ALL

					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor`.`opening_balance` AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`
					FROM
					creditor,account,account_type
					WHERE debit_id = 1 AND account.account_id = ".$supplier_opening_balance_id." AND account.account_type_id = account_type.account_type_id

					UNION ALL

					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						`creditor`.`opening_balance` AS `dr_amount`,
						'0' AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						 creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`
					FROM
					creditor,account,account_type
					WHERE debit_id = 2 AND account.account_id = ".$supplier_opening_balance_id." AND account.account_type_id = account_type.account_type_id


					UNION ALL


					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						`creditor`.`opening_balance` AS `dr_amount`,
						0 AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`
					FROM
					creditor,account,account_type
					WHERE debit_id = 1 AND account.account_id = ".$accounts_payable_id." AND account.account_type_id = account_type.account_type_id

					UNION ALL

					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor`.`opening_balance`  AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						 creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`
					FROM
					creditor,account,account_type
					WHERE debit_id = 2 AND account.account_id = ".$accounts_payable_id." AND account.account_type_id = account_type.account_type_id
				

					UNION ALL


					SELECT
						`creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
						`creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
						'' AS `payingFor`,
						`creditor_invoice`.`invoice_number` AS `referenceCode`,
						`creditor_invoice`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
					  `creditor_invoice`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`creditor_invoice_item`.`account_to_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_invoice_item`.`item_description` AS `transactionName`,
						CONCAT(`creditor_invoice_item`.`item_description`,' ',creditor_invoice.invoice_number) AS `transactionDescription`,
						`creditor_invoice_item`.`total_amount` AS `dr_amount`,
						'0' AS `cr_amount`,
						`creditor_invoice`.`transaction_date` AS `transactionDate`,
						`creditor_invoice`.`created` AS `createdAt`,
						`creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
						'creditor.branch_id' AS `branch_id`,
						'Cost of Goods' AS `transactionCategory`,
						'Creditors Invoices' AS `transactionClassification`,
						'creditor_invoice_item' AS `transactionTable`,
						'creditor_invoice' AS `referenceTable`
					FROM
						(
							(
								(
									`creditor_invoice_item`,creditor_invoice,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = creditor_invoice_item.account_to_id
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)
						)
					WHERE 

					creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id 
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date

				UNION ALL

				  SELECT
					`creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
					`creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
					`creditor_credit_note_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_credit_note`.`invoice_number` AS `referenceCode`,
					`creditor_credit_note`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_credit_note`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`creditor_credit_note`.`account_from_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_credit_note_item`.`description` AS `transactionName`,
					`creditor_credit_note_item`.`description` AS `transactionDescription`,
					0 AS `dr_amount`,
					`creditor_credit_note_item`.`credit_note_amount` AS `cr_amount`,
					`creditor_credit_note`.`transaction_date` AS `transactionDate`,
					`creditor_credit_note`.`created` AS `createdAt`,
					`creditor_invoice`.`transaction_date` AS `referenceDate`,
					`creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
					'Supplier Credit Note' AS `transactionCategory`,
					'Creditors Credit Notes' AS `transactionClassification`,
					'creditor_credit_note' AS `transactionTable`,
					'creditor_credit_note_item' AS `referenceTable`
				FROM
					(
						(
							(
								`creditor_credit_note_item`,creditor_credit_note,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = creditor_credit_note.account_from_id
								)
							)
							
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					)
				WHERE 
					creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id 
					AND creditor_credit_note.creditor_credit_note_status = 1
					AND creditor_invoice.creditor_invoice_id = creditor_credit_note_item.creditor_invoice_id
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date


				UNION ALL


				SELECT
						`creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
						`creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
						'' AS `payingFor`,
						`creditor_invoice`.`invoice_number` AS `referenceCode`,
						`creditor_invoice`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
					  `creditor_invoice`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`account`.`account_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_invoice_item`.`item_description` AS `transactionName`,
						CONCAT(`creditor_invoice_item`.`item_description`,' ',creditor_invoice.invoice_number) AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor_invoice_item`.`total_amount` AS `cr_amount`,
						`creditor_invoice`.`transaction_date` AS `transactionDate`,
						`creditor_invoice`.`created` AS `createdAt`,
						`creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
						'creditor.branch_id' AS `branch_id`,
						'Cost of Goods' AS `transactionCategory`,
						'Creditors Invoices' AS `transactionClassification`,
						'creditor_invoice_item' AS `transactionTable`,
						'creditor_invoice' AS `referenceTable`
					FROM
						(
							(
								(
									`creditor_invoice_item`,creditor_invoice,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = ".$accounts_payable_id."
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)
						)
					WHERE 

					creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id 
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date

				UNION ALL

				  SELECT
					`creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
					`creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
					`creditor_credit_note_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_credit_note`.`invoice_number` AS `referenceCode`,
					`creditor_credit_note`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_credit_note`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`account`.`account_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_credit_note_item`.`description` AS `transactionName`,
					`creditor_credit_note_item`.`description` AS `transactionDescription`,
					`creditor_credit_note_item`.`credit_note_amount` AS `dr_amount`,
					0 AS `cr_amount`,
					`creditor_credit_note`.`transaction_date` AS `transactionDate`,
					`creditor_credit_note`.`created` AS `createdAt`,
					`creditor_invoice`.`transaction_date` AS `referenceDate`,
					`creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
					'Supplier Credit Note' AS `transactionCategory`,
					'Creditors Credit Notes' AS `transactionClassification`,
					'creditor_credit_note' AS `transactionTable`,
					'creditor_credit_note_item' AS `referenceTable`
				FROM
					(
						(
							(
								`creditor_credit_note_item`,creditor_credit_note,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = ".$accounts_payable_id."
								)
							)
							
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					)
				WHERE 
					creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id 
					AND creditor_credit_note.creditor_credit_note_status = 1
					AND creditor_invoice.creditor_invoice_id = creditor_credit_note_item.creditor_invoice_id
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date



				UNION ALL


				SELECT
					`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
					`creditor_payment`.`creditor_payment_id` AS `referenceId`,
					`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_payment`.`reference_number` AS `referenceCode`,
					`creditor_payment`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_payment`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`creditor_payment`.`account_from_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_payment_item`.`description` AS `transactionName`,
					CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
					0 AS `dr_amount`,
					`creditor_payment_item`.`amount_paid` AS `cr_amount`,
					`creditor_payment`.`transaction_date` AS `transactionDate`,
					`creditor_payment`.`created` AS `createdAt`,
					`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
					'creditor_payment.branch_id' AS `branch_id`,
					'Creditor Payment' AS `transactionCategory`,
					'Creditors Invoices Payments' AS `transactionClassification`,
					'creditor_payment' AS `transactionTable`,
					'creditor_payment_item' AS `referenceTable`
				FROM
					(
						(
							(
								`creditor_payment_item`,creditor_payment,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = creditor_payment.account_from_id
								)
							)
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					
					)
					WHERE creditor_payment_item.invoice_type = 0 
					AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
					AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
					AND creditor_payment.creditor_payment_status = 1
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor_invoice.creditor_id = creditor.creditor_id
					AND creditor_payment.transaction_date >= creditor.start_date

				UNION ALL


				SELECT
					`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
					`creditor_payment`.`creditor_payment_id` AS `referenceId`,
					`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_payment`.`reference_number` AS `referenceCode`,
					`creditor_payment`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_payment`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`account`.`account_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_payment_item`.`description` AS `transactionName`,
					CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
					`creditor_payment_item`.`amount_paid` AS `dr_amount`,
					0 AS `cr_amount`,
					`creditor_payment`.`transaction_date` AS `transactionDate`,
					`creditor_payment`.`created` AS `createdAt`,
					`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
					'creditor_payment.branch_id' AS `branch_id`,
					'Creditor Payment' AS `transactionCategory`,
					'Creditors Invoices Payments' AS `transactionClassification`,
					'creditor_payment' AS `transactionTable`,
					'creditor_payment_item' AS `referenceTable`
				FROM
					(
						(
							(
								`creditor_payment_item`,creditor_payment,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = ".$accounts_payable_id."
								)
							)
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					
					)
					WHERE creditor_payment_item.invoice_type = 0 
					AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
					AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
					AND creditor_payment.creditor_payment_status = 1
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor_invoice.creditor_id = creditor.creditor_id
					AND creditor_payment.transaction_date >= creditor.start_date

				UNION ALL

					SELECT
						`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
						`creditor_payment`.`creditor_payment_id` AS `referenceId`,
						`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
						`creditor_payment`.`reference_number` AS `referenceCode`,
						`creditor_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
						`creditor_payment`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`creditor_payment`.`account_from_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_payment_item`.`description` AS `transactionName`,
						CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor_payment_item`.`amount_paid` AS `cr_amount`,
						`creditor_payment`.`transaction_date` AS `transactionDate`,
						`creditor_payment`.`created` AS `createdAt`,
						`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
						'creditor_payment.branch_id' AS `branch_id`,
						'Creditor Payment' AS `transactionCategory`,
						'Creditors Invoices Payments' AS `transactionClassification`,
						'creditor_payment' AS `transactionTable`,
						'creditor_payment_item' AS `referenceTable`
						FROM
						(
							(
								(
									`creditor_payment_item`,creditor_payment,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = creditor_payment.account_from_id
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)

						)
						WHERE creditor_payment_item.invoice_type = 2 
						AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
						AND creditor.creditor_id = creditor_payment_item.creditor_id
						AND creditor_payment.transaction_date >= creditor.start_date

				UNION ALL

					SELECT
						`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
						`creditor_payment`.`creditor_payment_id` AS `referenceId`,
						`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
						`creditor_payment`.`reference_number` AS `referenceCode`,
						`creditor_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
						`creditor_payment`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`account`.`account_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_payment_item`.`description` AS `transactionName`,
						CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
						`creditor_payment_item`.`amount_paid` AS `dr_amount`,
						0 AS `cr_amount`,
						`creditor_payment`.`transaction_date` AS `transactionDate`,
						`creditor_payment`.`created` AS `createdAt`,
						`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
						'creditor_payment.branch_id' AS `branch_id`,
						'Creditor Payment' AS `transactionCategory`,
						'Creditors Invoices Payments' AS `transactionClassification`,
						'creditor_payment' AS `transactionTable`,
						'creditor_payment_item' AS `referenceTable`
						FROM
						(
							(
								(
									`creditor_payment_item`,creditor_payment,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = ".$accounts_payable_id."
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)

						)
						WHERE creditor_payment_item.invoice_type = 2 
						AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
						AND creditor.creditor_id = creditor_payment_item.creditor_id
						AND creditor_payment.transaction_date >= creditor.start_date

					UNION ALL

						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`creditor_payment`.`account_from_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							0 AS `dr_amount`,
							`creditor_payment_item`.`amount_paid` AS `cr_amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							'creditor_payment.branch_id' AS `branch_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`
							FROM
							(
								(
									(
										`creditor_payment_item`,creditor_payment,creditor
										
									)
									JOIN account ON(
										(
											account.account_id = creditor_payment.account_from_id
										)
									)
								)
								JOIN `account_type` ON(
									(
										account_type.account_type_id = account.account_type_id
									)
								)
								
							)
							WHERE creditor_payment_item.invoice_type = 3
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
							AND creditor.creditor_id = creditor_payment_item.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date
					UNION ALL

						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							`creditor_payment_item`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							'creditor_payment.branch_id' AS `branch_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`
							FROM
							(
								(
									(
										`creditor_payment_item`,creditor_payment,creditor
										
									)
									JOIN account ON(
										(
											account.account_id = ".$accounts_payable_id."
										)
									)
								)
								JOIN `account_type` ON(
									(
										account_type.account_type_id = account.account_type_id
									)
								)
								
							)
							WHERE creditor_payment_item.invoice_type = 3
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
							AND creditor.creditor_id = creditor_payment_item.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date


						UNION ALL

							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							 	`orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Drug Purchase' AS `transactionName`,
								CONCAT('Purchase of supplies',' ',orders.supplier_invoice_number) AS `transactionDescription`,
								SUM(`order_supplier`.`less_vat`) AS `dr_amount`,
								0 AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Purchases' AS `transactionCategory`,
								'Supplies Invoices' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`
							FROM
								`order_supplier`,orders,account,order_item,product,account_type,creditor
										

							WHERE orders.is_store = 0 
								AND orders.supplier_id = creditor.creditor_id
								AND orders.order_id = order_supplier.order_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND product.product_id = order_item.product_id
								AND account_type.account_type_id = account.account_type_id
								AND account.account_id = orders.account_id
								AND orders.order_approval_status = 7 
								AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id

							UNION ALL



							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							 	`orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Drug Purchase' AS `transactionName`,
								CONCAT('Purchase of supplies',' ',orders.supplier_invoice_number) AS `transactionDescription`,
								0 AS `dr_amount`,
								SUM(`order_supplier`.`less_vat`) AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Purchases' AS `transactionCategory`,
								'Supplies Invoices' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`
							FROM
								`order_supplier`,orders,account,order_item,product,account_type,creditor
										

							WHERE orders.is_store = 0 
								AND orders.supplier_id = creditor.creditor_id
								AND orders.order_id = order_supplier.order_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND product.product_id = order_item.product_id
								AND account_type.account_type_id = account.account_type_id
								AND account.account_id = ".$accounts_payable_id."
								AND orders.order_approval_status = 7 
								AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id

							UNION ALL

							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								`orders`.`reference_number`  AS `transactionCode`,
								'' AS `patient_id`,
							  `orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`orders`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Credit' AS `transactionName`,
								CONCAT('Credit note of ',' ',`orders`.`reference_number`) AS `transactionDescription`,
								'0' AS `dr_amount`,
								SUM(`order_supplier`.`less_vat`) AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Income' AS `transactionCategory`,
								'Supplies Credit Note' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`

							FROM
								`order_supplier`,orders,order_item,product,account,account_type,creditor
								
								WHERE orders.is_store = 3
								AND orders.order_id = order_supplier.order_id
								AND account.account_id = orders.account_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND product.product_id = order_item.product_id
								AND account_type.account_type_id = account.account_type_id
								 AND orders.supplier_id = creditor.creditor_id and orders.order_approval_status = 7 AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								 AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id

							UNION ALL

							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								`orders`.`reference_number`  AS `transactionCode`,
								'' AS `patient_id`,
							  `orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Credit' AS `transactionName`,
								CONCAT('Credit note of ',' ',`orders`.`supplier_invoice_number`) AS `transactionDescription`,
								SUM(`order_supplier`.`less_vat`)  AS `dr_amount`,
								0 AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Income' AS `transactionCategory`,
								'Supplies Credit Note' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`

							FROM
								`order_supplier`,orders,account,order_item,product,account_type,creditor
											

								WHERE orders.is_store = 3
								AND account.account_id = ".$accounts_payable_id."
								AND account_type.account_type_id = account.account_type_id
								AND product.product_id = order_item.product_id
								AND orders.order_id = order_supplier.order_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND orders.supplier_id = creditor.creditor_id and orders.order_approval_status = 7 AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id



							UNION ALL

							 SELECT
								`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
								`creditor_payment`.`creditor_payment_id` AS `referenceId`,
								`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
								`creditor_payment`.`reference_number` AS `referenceCode`,
								`creditor_payment`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`creditor_payment`.`creditor_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								creditor_payment.account_from_id AS `accountId`,
								`account`.`account_name` AS `accountName`,
								`creditor_payment_item`.`description` AS `transactionName`,
								CONCAT('Payment to ', creditor.creditor_name ,'for ',`orders`.`supplier_invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
								0 AS `dr_amount`,
								`creditor_payment_item`.`amount_paid` AS `cr_amount`,
								`creditor_payment`.`transaction_date` AS `transactionDate`,
								`creditor_payment`.`created` AS `createdAt`,
								`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
								'creditor_payment.branch_id' AS `branch_id`,
								'Creditor Payment' AS `transactionCategory`,
								'Creditors Invoices Payments' AS `transactionClassification`,
								'creditor_payment' AS `transactionTable`,
								'creditor_payment_item' AS `referenceTable`
								FROM
								(
									(
										(
											`creditor_payment_item`,creditor_payment,orders,creditor
											
										)
										JOIN account ON(
											(
												account.account_id = creditor_payment.account_from_id
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
									
								)
							WHERE creditor_payment_item.invoice_type = 1 
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id  
							AND creditor_payment.creditor_payment_status = 1 
							AND orders.order_id = creditor_payment_item.creditor_invoice_id
							AND orders.supplier_id = creditor.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date

							UNION ALL

							SELECT
								`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
								`creditor_payment`.`creditor_payment_id` AS `referenceId`,
								`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
								`creditor_payment`.`reference_number` AS `referenceCode`,
								`creditor_payment`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`creditor_payment`.`creditor_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								`account`.`account_name` AS `accountName`,
								`creditor_payment_item`.`description` AS `transactionName`,
								CONCAT('Payment to ', creditor.creditor_name ,'for ',`orders`.`supplier_invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
								`creditor_payment_item`.`amount_paid` AS `dr_amount`,
								0 AS `cr_amount`,
								`creditor_payment`.`transaction_date` AS `transactionDate`,
								`creditor_payment`.`created` AS `createdAt`,
								`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
								'creditor_payment.branch_id' AS `branch_id`,
								'Creditor Payment' AS `transactionCategory`,
								'Creditors Invoices Payments' AS `transactionClassification`,
								'creditor_payment' AS `transactionTable`,
								'creditor_payment_item' AS `referenceTable`
								FROM
								(
									(
										(
											`creditor_payment_item`,creditor_payment,orders,creditor
											
										)
										JOIN account ON(
											(
												account.account_id = ".$accounts_payable_id."
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
									
								)
							WHERE creditor_payment_item.invoice_type = 1 
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id  
							AND creditor_payment.creditor_payment_status = 1 
							AND orders.order_id = creditor_payment_item.creditor_invoice_id
							AND orders.supplier_id = creditor.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date
								

					

						UNION ALL





						SELECT

							bank_reconcilliation.recon_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.account_name AS `accountName`,
							'' AS `transactionName`,
							CONCAT('Bank Charges ', account.account_name ) AS `transactionDescription`,
							0 AS `dr_amount`,
							SUM(bank_reconcilliation.service_charged) AS `cr_amount`,
							bank_reconcilliation.charged_date AS `transactionDate`,
							bank_reconcilliation.charged_date AS `createdAt`,
							bank_reconcilliation.recon_status AS `status`,
							2 AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							bank_reconcilliation,account,account_type
							WHERE bank_reconcilliation.recon_status = 2
							AND bank_reconcilliation.account_id = account.account_id
							AND account.account_type_id = account_type.account_type_id
						GROUP BY bank_reconcilliation.recon_id



						UNION ALL


						SELECT

							bank_reconcilliation.recon_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.account_name AS `accountName`,
							'' AS `transactionName`,
							CONCAT('Bank Charges ', account.account_name ) AS `transactionDescription`,
							SUM(bank_reconcilliation.service_charged) AS `dr_amount`,
							0 AS `cr_amount`,
							bank_reconcilliation.charged_date AS `transactionDate`,
							bank_reconcilliation.charged_date AS `createdAt`,
							bank_reconcilliation.recon_status AS `status`,
							2 AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							bank_reconcilliation,account,account_type
							WHERE bank_reconcilliation.recon_status = 2
							AND bank_reconcilliation.expense_account_id = account.account_id
							AND account.account_type_id = account_type.account_type_id
						GROUP BY bank_reconcilliation.recon_id

						UNION ALL 

						SELECT


							`journal_entry`.`journal_entry_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							journal_entry.document_number AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							journal_entry.account_from_id AS `accountId`,
							account.account_name AS `accountName`,
							'Journal Entry ' AS `transactionName`,
							CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
							`journal_entry`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`journal_entry`.`payment_date` AS `transactionDate`,
							`journal_entry`.`payment_date` AS `createdAt`,
							`journal_entry`.`journal_entry_status` AS `status`,
							2 AS `branch_id`,
							'Journal Credit' AS `transactionCategory`,
							'Journal' AS `transactionClassification`,
							'journal_entry' AS `transactionTable`,
							'account' AS `referenceTable`
						FROM
						journal_entry,account,account_type
						
						WHERE journal_entry.account_from_id  = account.account_id AND journal_entry.journal_entry_deleted = 0 
						AND account_type.account_type_id = account.account_type_id



						UNION ALL 


						SELECT
							`journal_entry`.`journal_entry_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							journal_entry.document_number AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							journal_entry.account_to_id AS `accountId`,
							account.account_name AS `accountName`,
							'Journal Entry ' AS `transactionName`,
							CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
							0 AS `dr_amount`,
							`journal_entry`.`amount_paid` AS `cr_amount`,
							`journal_entry`.`payment_date` AS `transactionDate`,
							`journal_entry`.`payment_date` AS `createdAt`,
							`journal_entry`.`journal_entry_status` AS `status`,
							2 AS `branch_id`,
							'Journal Debit' AS `transactionCategory`,
							'Journal' AS `transactionClassification`,
							'journal_entry' AS `transactionTable`,
							'account' AS `referenceTable`
						FROM
						journal_entry,account,account_type
						
						WHERE journal_entry.account_to_id  = account.account_id AND journal_entry.journal_entry_deleted = 0 
						AND account_type.account_type_id = account.account_type_id

						


						UNION ALL


						SELECT
								visit_invoice.visit_invoice_id AS transactionId,
								visit_charge.visit_charge_id  AS referenceId,
								'' AS `payingFor`,
								visit_invoice.visit_invoice_number AS referenceCode,
								'' AS transactionCode,
								visit_invoice.patient_id AS patientId,
								'' AS recepientId,

								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
							    account.account_id AS `accountId`,
							    `account`.`account_name` AS `accountName`,
							   	service_charge.service_charge_name AS transactionName,
								CONCAT( '<strong>Invoice </strong>  Invoice Number :  ', visit_invoice.visit_invoice_number,' <strong>Bill to:</strong>  ', visit_type.visit_type_name ) AS transactionDescription,
								0 AS dr_amount,
								visit_charge.visit_charge_amount*visit_charge.visit_charge_units AS cr_amount,
							    visit_invoice.created AS `transactionDate`,
							    visit_invoice.created AS `createdAt`,
							    1 AS `status`,
							    2 AS `branch_id`,
							    service.service_name AS `transactionCategory`,
							    'PATIENT INVOICE' AS `transactionClassification`,
							    'visit_invoice' AS `transactionTable`,
							    'visit_charge' AS `referenceTable`


						FROM
									visit_charge,visit_invoice,service_charge,visit_type,service,account,account_type,visit
									WHERE visit_charge.charged = 1  
									AND visit_invoice.visit_invoice_delete = 0
									AND visit_charge.visit_charge_delete = 0 
									AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id
									AND service_charge.service_charge_id = visit_charge.service_charge_id
									AND visit_type.visit_type_id = visit_invoice.bill_to
									AND service.service_id = service_charge.service_id
									AND account.account_id = service.account_id
									AND visit.visit_id = visit_charge.visit_id
									AND visit.visit_delete = 0
									AND account_type.account_type_id = account.account_type_id


						UNION ALL

						SELECT
							visit_credit_note.visit_credit_note_id AS transactionId,
							visit_invoice.visit_invoice_id AS referenceId,
							'' AS `payingFor`,
							visit_credit_note.visit_cr_note_number AS referenceCode,
							visit_credit_note.visit_cr_note_number AS transactionCode,
							visit_invoice.patient_id AS patient_id,
							'' AS recepientId,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
						    account.account_id AS `accountId`,
						    `account`.`account_name` AS `accountName`,
						    service_charge.service_charge_name AS transactionName,
							CONCAT('<strong>Credit Note for </strong>  Invoice Number: ', visit_invoice.visit_invoice_number,'<strong>Credit Note No:</strong> ',visit_credit_note.visit_cr_note_number) AS transactionDescription,
						   
							visit_credit_note_item.visit_cr_note_amount*visit_credit_note_item.visit_cr_note_units AS dr_amount,
							0 AS cr_amount,
						    visit_invoice.created AS `transactionDate`,
						    visit_credit_note.created AS `createdAt`,
						    1 AS `status`,
						    2 AS `branch_id`,
						    CONCAT(service.service_name,' Credit note') AS `transactionCategory`,
						    'INCOME CREDIT NOTE' AS `transactionClassification`,
						    'visit_invoice' AS `transactionTable`,
						    'visit_credit_note' AS `referenceTable`
						FROM
							visit_credit_note_item,visit_credit_note,visit_invoice,service_charge,service,account,account_type
							WHERE visit_credit_note.visit_cr_note_delete = 0 AND visit_credit_note_item.visit_cr_note_item_delete = 0 

							AND visit_invoice.visit_invoice_id = visit_credit_note.visit_invoice_id 
							AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id 
							AND service_charge.service_charge_id = visit_credit_note_item.service_charge_id
							AND service.service_id = service_charge.service_id
							AND account.account_id = service.account_id
							AND account_type.account_type_id = account.account_type_id



						UNION ALL



						SELECT
								visit_invoice.visit_invoice_id AS transactionId,
								visit_charge.visit_charge_id  AS referenceId,
								'' AS `payingFor`,
								visit_invoice.visit_invoice_number AS referenceCode,
								'' AS transactionCode,
								visit_invoice.patient_id AS patientId,
								'' AS recepientId,

								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
							    account.account_id AS `accountId`,
							    `account`.`account_name` AS `accountName`,
							   	service_charge.service_charge_name AS transactionName,
								CONCAT( '<strong>Invoice </strong>  Invoice Number :  ', visit_invoice.visit_invoice_number,' <strong>Bill to:</strong>  ', visit_type.visit_type_name ) AS transactionDescription,
								visit_charge.visit_charge_amount*visit_charge.visit_charge_units AS dr_amount,
								0 AS cr_amount,
							    visit_invoice.created AS `transactionDate`,
							    visit_invoice.created AS `createdAt`,
							    1 AS `status`,
							    2 AS `branch_id`,
							    service.service_name AS `transactionCategory`,
							    'PATIENT INVOICE' AS `transactionClassification`,
							    'visit_invoice' AS `transactionTable`,
							    'visit_charge' AS `referenceTable`


						FROM
									visit_charge,visit_invoice,service_charge,visit_type,service,account,account_type,visit
									WHERE visit_charge.charged = 1  
									AND visit_invoice.visit_invoice_delete = 0
									AND visit_charge.visit_charge_delete = 0 
									AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id
									AND service_charge.service_charge_id = visit_charge.service_charge_id
									AND visit_type.visit_type_id = visit_invoice.bill_to
									AND service.service_id = service_charge.service_id
									AND visit.visit_id = visit_charge.visit_id
									AND visit.visit_delete = 0
									AND account.account_id = ".$accounts_receivable_id."
									AND account_type.account_type_id = account.account_type_id


						UNION ALL

						SELECT
							visit_credit_note.visit_credit_note_id AS transactionId,
							visit_invoice.visit_invoice_id AS referenceId,
							'' AS `payingFor`,
							visit_credit_note.visit_cr_note_number AS referenceCode,
							visit_credit_note.visit_cr_note_number AS transactionCode,
							visit_invoice.patient_id AS patient_id,
							'' AS recepientId,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
						    account.account_id AS `accountId`,
						    `account`.`account_name` AS `accountName`,
						    service_charge.service_charge_name AS transactionName,
							CONCAT('<strong>Credit Note for </strong>  Invoice Number: ', visit_invoice.visit_invoice_number,'<strong>Credit Note No:</strong> ',visit_credit_note.visit_cr_note_number) AS transactionDescription,
						   
							0 AS dr_amount,
							visit_credit_note_item.visit_cr_note_amount*visit_credit_note_item.visit_cr_note_units  AS cr_amount,
						    visit_invoice.created AS `transactionDate`,
						    visit_credit_note.created AS `createdAt`,
						    1 AS `status`,
						    2 AS `branch_id`,
						    'INCOME CREDIT NOTE' AS `transactionCategory`,
						    'INCOME CREDIT NOTE' AS `transactionClassification`,
						    'visit_invoice' AS `transactionTable`,
						    'visit_credit_note' AS `referenceTable`
						FROM
							visit_credit_note_item,visit_credit_note,visit_invoice,service_charge,service,account,account_type
							WHERE visit_credit_note.visit_cr_note_delete = 0 AND visit_credit_note_item.visit_cr_note_item_delete = 0 

							AND visit_invoice.visit_invoice_id = visit_credit_note.visit_invoice_id 
							AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id 
							AND service_charge.service_charge_id = visit_credit_note_item.service_charge_id
							AND service.service_id = service_charge.service_id
							AND account.account_id = ".$accounts_receivable_id."
							AND account_type.account_type_id = account.account_type_id


							UNION ALL


							SELECT

								payments.payment_id AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								payments.confirm_number AS `referenceCode`,
								payments.confirm_number AS `transactionCode`,
								payments.patient_id AS `patient_id`,
							  	'' AS `recepientId`,
								account_type.account_type_id AS `accountParentId`,
								account_type.account_type_name AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number) AS `transactionDescription`,
								SUM(payment_item.payment_item_amount) AS `dr_amount`,
								0 AS `cr_amount`,
								payments.payment_date AS `transactionDate`,
								payments.payment_date AS `createdAt`,
								`payments`.`cancel` AS `status`,
								2 AS `branch_id`,
								'Patient Income' AS `transactionCategory`,
								'Payments' AS `transactionClassification`,
								'account_payments' AS `transactionTable`,
								'' AS `referenceTable`
							FROM
								payments,payment_item,payment_method,account,visit_invoice,account_type
								WHERE payments.cancel = 0 AND payments.payment_type = 1 AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND payment_item.invoice_type = 1
								AND payments.payment_id = payment_item.payment_id 
								AND payments.payment_method_id = payment_method.payment_method_id
								AND payment_method.account_id = account.account_id
								AND account.account_type_id = account_type.account_type_id
							GROUP BY payment_item.payment_id


							UNION ALL 


							SELECT

								payments.payment_id AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								payments.confirm_number AS `referenceCode`,
								payments.confirm_number AS `transactionCode`,
								payments.patient_id AS `patient_id`,
							  	'' AS `recepientId`,
								account_type.account_type_id AS `accountParentId`,
								account_type.account_type_name AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								account.parent_account AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Payment On Account') AS `transactionDescription`,
								SUM(payment_item.payment_item_amount) AS `dr_amount`,
								0 AS `cr_amount`,
								payments.payment_date AS `transactionDate`,
								payments.payment_date AS `createdAt`,
								`payments`.`cancel` AS `status`,
								2 AS `branch_id`,
								'Patient Income' AS `transactionCategory`,
								'Payments' AS `transactionClassification`,
								'account_payments' AS `transactionTable`,
								'' AS `referenceTable`
							FROM
								payments,payment_item,payment_method,account,account_type
								WHERE payments.cancel = 0 AND payments.payment_type = 1 AND payment_item.invoice_type = 3
								AND payments.payment_id = payment_item.payment_id 
								AND payments.payment_method_id = payment_method.payment_method_id
								AND payment_method.account_id = account.account_id
								AND account.account_type_id = account_type.account_type_id
							GROUP BY payment_item.payment_id


							UNION ALL

						SELECT

							payments.payment_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							payments.confirm_number AS `referenceCode`,
							payments.confirm_number AS `transactionCode`,
							payments.patient_id AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.account_name AS `accountName`,
							'' AS `transactionName`,
							CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number) AS `transactionDescription`,
							0 AS `dr_amount`,
							SUM(payment_item.payment_item_amount) AS `cr_amount`,
							payments.payment_date AS `transactionDate`,
							payments.payment_date AS `createdAt`,
							`payments`.`cancel` AS `status`,
							2 AS `branch_id`,
							'Patient Income' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							payments,payment_item,payment_method,account,visit_invoice,account_type
							WHERE payments.cancel = 0 AND payments.payment_type = 1 AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND payment_item.invoice_type = 1
							AND payments.payment_id = payment_item.payment_id 
							AND payments.payment_method_id = payment_method.payment_method_id
							AND account.account_id = ".$accounts_receivable_id."
							AND account.account_type_id = account_type.account_type_id
						GROUP BY payment_item.payment_id


						UNION ALL 


						SELECT

							payments.payment_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							payments.confirm_number AS `referenceCode`,
							payments.confirm_number AS `transactionCode`,
							payments.patient_id AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.parent_account AS `accountName`,
							'' AS `transactionName`,
							CONCAT('Payment On Account') AS `transactionDescription`,
							0 AS `dr_amount`,
							SUM(payment_item.payment_item_amount) AS `cr_amount`,
							payments.payment_date AS `transactionDate`,
							payments.payment_date AS `createdAt`,
							`payments`.`cancel` AS `status`,
							2 AS `branch_id`,
							'Patient Income' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							payments,payment_item,payment_method,account,account_type
							WHERE payments.cancel = 0 AND payments.payment_type = 1 AND payment_item.invoice_type = 3
							AND payments.payment_id = payment_item.payment_id 
							AND payments.payment_method_id = payment_method.payment_method_id
							AND account.account_id = ".$accounts_receivable_id."
							AND account.account_type_id = account_type.account_type_id
						GROUP BY payment_item.payment_id

						-- providers

						UNION ALL

							SELECT
								`provider`.`provider_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							    `provider`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
				  				`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider`.`opening_balance` AS `cr_amount`,
								`provider`.`start_date` AS `transactionDate`,
								`provider`.`start_date` AS `createdAt`,
								`provider`.`provider_status` AS `status`,
								2 AS `branch_id`,
								'Expense' AS `transactionCategory`,
								'provider Opening Balance' AS `transactionClassification`,
								'' AS `transactionTable`,
								'provider' AS `referenceTable`
							FROM
							provider,account,account_type
							WHERE  debit_id = 1 AND account.account_id = ".$provider_opening_balance_id." AND account.account_type_id = account_type.account_type_id

							UNION ALL


							SELECT
								`provider`.`provider_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							    `provider`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
				  				`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
								`provider`.`opening_balance` AS `dr_amount`,
								0 AS `cr_amount`,
								`provider`.`start_date` AS `transactionDate`,
								`provider`.`start_date` AS `createdAt`,
								`provider`.`provider_status` AS `status`,
								2 AS `branch_id`,
								'Expense' AS `transactionCategory`,
								'provider Opening Balance' AS `transactionClassification`,
								'' AS `transactionTable`,
								'provider' AS `referenceTable`
							FROM
							provider,account,account_type
							WHERE  debit_id = 1 AND account.account_id = ".$providers_liability_id." AND account.account_type_id = account_type.account_type_id


							UNION ALL


							SELECT
								`provider`.`provider_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							    `provider`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
				  				`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
								`provider`.`opening_balance` AS `dr_amount`,
								0 AS `cr_amount`,
								`provider`.`start_date` AS `transactionDate`,
								`provider`.`start_date` AS `createdAt`,
								`provider`.`provider_status` AS `status`,
								2 AS `branch_id`,
								'Expense' AS `transactionCategory`,
								'provider Opening Balance' AS `transactionClassification`,
								'' AS `transactionTable`,
								'provider' AS `referenceTable`
							FROM
							provider,account,account_type
							WHERE  debit_id = 2 AND account.account_id = ".$provider_opening_balance_id." AND account.account_type_id = account_type.account_type_id



							UNION ALL


							SELECT
								`provider`.`provider_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							    `provider`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
				  				`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider`.`opening_balance` AS `cr_amount`,
								`provider`.`start_date` AS `transactionDate`,
								`provider`.`start_date` AS `createdAt`,
								`provider`.`provider_status` AS `status`,
								2 AS `branch_id`,
								'Expense' AS `transactionCategory`,
								'provider Opening Balance' AS `transactionClassification`,
								'' AS `transactionTable`,
								'provider' AS `referenceTable`
							FROM
							provider,account,account_type
							WHERE  debit_id = 2 AND account.account_id = ".$providers_liability_id." AND account.account_type_id = account_type.account_type_id

						  UNION ALL

							SELECT
								`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
								`provider_invoice`.`provider_invoice_id` AS `referenceId`,
								'' AS `payingFor`,
								`provider_invoice`.`invoice_number` AS `referenceCode`,
								`provider_invoice`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  `provider_invoice`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`provider_invoice_item`.`account_to_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
								`provider_invoice_item`.`total_amount` AS `dr_amount`,
								'0' AS `cr_amount`,
								`provider_invoice`.`transaction_date` AS `transactionDate`,
								`provider_invoice`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
								'Expense' AS `transactionCategory`,
								'providers Invoices' AS `transactionClassification`,
								'provider_invoice_item' AS `transactionTable`,
								'provider_invoice' AS `referenceTable`
							FROM
								(
									(
										(
											`provider_invoice_item`,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = provider_invoice_item.account_to_id
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
							provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
							AND provider_invoice.provider_invoice_status = 1
							AND provider.provider_id = provider_invoice.provider_id 
							AND provider_invoice.transaction_date >= provider.start_date

						UNION ALL

						SELECT
								`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
								`provider_invoice`.`provider_invoice_id` AS `referenceId`,
								'' AS `payingFor`,
								`provider_invoice`.`invoice_number` AS `referenceCode`,
								`provider_invoice`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  `provider_invoice`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider_invoice_item`.`total_amount` AS `cr_amount`,
								`provider_invoice`.`transaction_date` AS `transactionDate`,
								`provider_invoice`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
								'Provider Liability' AS `transactionCategory`,
								'providers Invoices' AS `transactionClassification`,
								'provider_invoice_item' AS `transactionTable`,
								'provider_invoice' AS `referenceTable`
							FROM
								(
									(
										(
											`provider_invoice_item`,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = ".$providers_liability_id."
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
							provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
							AND provider_invoice.provider_invoice_status = 1
							AND provider.provider_id = provider_invoice.provider_id 
							AND provider_invoice.transaction_date >= provider.start_date


							UNION ALL

							SELECT
								`provider_credit_note_item`.`provider_credit_note_item_id` AS `transactionId`,
								`provider_credit_note`.`provider_credit_note_id` AS `referenceId`,
								`provider_credit_note_item`.`provider_invoice_id` AS `payingFor`,
								`provider_credit_note`.`invoice_number` AS `referenceCode`,
								`provider_credit_note`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  	`provider_credit_note`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`provider_credit_note`.`account_from_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider_credit_note_item`.`credit_note_amount` AS `cr_amount`,
								`provider_credit_note`.`transaction_date` AS `transactionDate`,
								`provider_credit_note`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_credit_note_item`.`provider_credit_note_item_status` AS `status`,
								'Provider Credit Note' AS `transactionCategory`,
								'Providers Credit Notes' AS `transactionClassification`,
								'provider_credit_note' AS `transactionTable`,
								'provider_credit_note_item' AS `referenceTable`
							FROM
								(
									(
										(
											`provider_credit_note_item`,provider_credit_note,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = provider_credit_note.account_from_id
											)
										)
										
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
								provider_credit_note.provider_credit_note_id = provider_credit_note_item.provider_credit_note_id 
								AND provider_credit_note.provider_credit_note_status = 1
								AND provider_invoice.provider_invoice_id = provider_credit_note_item.provider_invoice_id
								AND provider_invoice.provider_invoice_status = 1
								AND provider.provider_id = provider_invoice.provider_id 
								AND provider_invoice.transaction_date >= provider.start_date

							UNION ALL

							SELECT
								`provider_credit_note_item`.`provider_credit_note_item_id` AS `transactionId`,
								`provider_credit_note`.`provider_credit_note_id` AS `referenceId`,
								`provider_credit_note_item`.`provider_invoice_id` AS `payingFor`,
								`provider_credit_note`.`invoice_number` AS `referenceCode`,
								`provider_credit_note`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  	`provider_credit_note`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionDescription`,
								`provider_credit_note_item`.`credit_note_amount` AS `dr_amount`,
								0 AS `cr_amount`,
								`provider_credit_note`.`transaction_date` AS `transactionDate`,
								`provider_credit_note`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_credit_note_item`.`provider_credit_note_item_status` AS `status`,
								'Provider Credit Note' AS `transactionCategory`,
								'Providers Credit Notes' AS `transactionClassification`,
								'provider_credit_note' AS `transactionTable`,
								'provider_credit_note_item' AS `referenceTable`
							FROM
								(
									(
										(
											`provider_credit_note_item`,provider_credit_note,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = ".$providers_liability_id."
											)
										)
										
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
								provider_credit_note.provider_credit_note_id = provider_credit_note_item.provider_credit_note_id 
								AND provider_credit_note.provider_credit_note_status = 1
								AND provider_invoice.provider_invoice_id = provider_credit_note_item.provider_invoice_id
								AND provider_invoice.provider_invoice_status = 1
								AND provider.provider_id = provider_invoice.provider_id 
								AND provider_invoice.transaction_date >= provider.start_date

							UNION ALL
								
							SELECT
									`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
									`provider_payment`.`provider_payment_id` AS `referenceId`,
									`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
									`provider_payment`.`reference_number` AS `referenceCode`,
									`provider_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`provider_payment`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`provider_payment`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`provider_payment_item`.`description` AS `transactionName`,
									CONCAT('Provider payment for invoice of ',' ',`provider_invoice`.`invoice_number`)  AS `transactionDescription`,
									0 AS `dr_amount`,
									`provider_payment_item`.`amount_paid` AS `cr_amount`,
									`provider_payment`.`transaction_date` AS `transactionDate`,
									`provider_payment`.`created` AS `createdAt`,
									`provider_invoice`.`transaction_date` AS `referenceDate`,
									`provider_payment_item`.`provider_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'providers Invoices Payments' AS `transactionClassification`,
									'provider_payment' AS `transactionTable`,
									'provider_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_payment_item`,provider_payment,provider_invoice,provider
												
											)
											JOIN account ON(
												(
													account.account_id = provider_payment.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE provider_payment_item.invoice_type = 0 
									AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
									AND provider_payment.provider_payment_status = 1
									AND provider_invoice.provider_invoice_id = provider_payment_item.provider_invoice_id 
									AND provider_invoice.provider_invoice_status = 1  
									AND provider.provider_id = provider_invoice.provider_id AND provider_invoice.transaction_date >= provider.start_date
							UNION ALL
								
							SELECT
									`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
									`provider_payment`.`provider_payment_id` AS `referenceId`,
									`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
									`provider_payment`.`reference_number` AS `referenceCode`,
									`provider_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`provider_payment`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`provider_payment_item`.`description` AS `transactionName`,
									CONCAT('Provider payment for invoice of ',' ',`provider_invoice`.`invoice_number`)  AS `transactionDescription`,
									`provider_payment_item`.`amount_paid` AS `dr_amount`,
									0 AS `cr_amount`,
									`provider_payment`.`transaction_date` AS `transactionDate`,
									`provider_payment`.`created` AS `createdAt`,
									`provider_invoice`.`transaction_date` AS `referenceDate`,
									`provider_payment_item`.`provider_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'providers Invoices Payments' AS `transactionClassification`,
									'provider_payment' AS `transactionTable`,
									'provider_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_payment_item`,provider_payment,provider_invoice,provider
												
											)
											JOIN account ON(
												(
													account.account_id = ".$providers_liability_id."
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE provider_payment_item.invoice_type = 0 
									AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
									AND provider_payment.provider_payment_status = 1
									AND provider_invoice.provider_invoice_id = provider_payment_item.provider_invoice_id 
									AND provider_invoice.provider_invoice_status = 1  
									AND provider.provider_id = provider_invoice.provider_id AND provider_invoice.transaction_date >= provider.start_date

								
							UNION ALL

							SELECT
									`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
									`provider_payment`.`provider_payment_id` AS `referenceId`,
									`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
									`provider_payment`.`reference_number` AS `referenceCode`,
									`provider_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  `provider_payment`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`provider_payment`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`provider_payment_item`.`description` AS `transactionName`,
									CONCAT('Payment of opening balance')  AS `transactionDescription`,
									0 AS `dr_amount`,
									`provider_payment_item`.`amount_paid` AS `cr_amount`,
									`provider_payment`.`transaction_date` AS `transactionDate`,
									`provider_payment`.`created` AS `createdAt`,
									`provider`.`start_date` AS `referenceDate`,
									`provider_payment_item`.`provider_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'providers Invoices Payments' AS `transactionClassification`,
									'provider_payment' AS `transactionTable`,
									'provider_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_payment_item`,provider_payment,provider
												
											)
											JOIN account ON(
												(
													account.account_id = provider_payment.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE provider_payment_item.invoice_type = 2 
									AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
									AND provider_payment.provider_payment_status = 1
									AND provider.provider_id = provider_payment_item.provider_id
									AND provider_payment.transaction_date >= provider.start_date
								UNION ALL

							SELECT
									`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
									`provider_payment`.`provider_payment_id` AS `referenceId`,
									`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
									`provider_payment`.`reference_number` AS `referenceCode`,
									`provider_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  `provider_payment`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`provider_payment_item`.`description` AS `transactionName`,
									CONCAT('Payment of opening balance')  AS `transactionDescription`,
									`provider_payment_item`.`amount_paid` AS `dr_amount`,
									0 AS `cr_amount`,
									`provider_payment`.`transaction_date` AS `transactionDate`,
									`provider_payment`.`created` AS `createdAt`,
									`provider`.`start_date` AS `referenceDate`,
									`provider_payment_item`.`provider_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'providers Invoices Payments' AS `transactionClassification`,
									'provider_payment' AS `transactionTable`,
									'provider_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_payment_item`,provider_payment,provider
												
											)
											JOIN account ON(
												(
													account.account_id = ".$providers_liability_id."
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE provider_payment_item.invoice_type = 2 
									AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
									AND provider_payment.provider_payment_status = 1
									AND provider.provider_id = provider_payment_item.provider_id
									AND provider_payment.transaction_date >= provider.start_date

								UNION ALL

								SELECT
										`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
										`provider_payment`.`provider_payment_id` AS `referenceId`,
										`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
										`provider_payment`.`reference_number` AS `referenceCode`,
										`provider_payment`.`document_number` AS `transactionCode`,
										'' AS `patient_id`,
									  `provider_payment`.`provider_id` AS `recepientId`,
										`account`.`parent_account` AS `accountParentId`,
										`account_type`.`account_type_name` AS `accountsclassfication`,
										`provider_payment`.`account_from_id` AS `accountId`,
										`account`.`account_name` AS `accountName`,
										`provider_payment_item`.`description` AS `transactionName`,
										CONCAT('Payment on account')  AS `transactionDescription`,
										0 AS `dr_amount`,
										`provider_payment_item`.`amount_paid` AS `cr_amount`,
										`provider_payment`.`transaction_date` AS `transactionDate`,
										`provider_payment`.`created` AS `createdAt`,
										`provider_payment`.`created` AS `referenceDate`,
										`provider_payment_item`.`provider_payment_item_status` AS `status`,
										'Expense Payment' AS `transactionCategory`,
										'providers Invoices Payments' AS `transactionClassification`,
										'provider_payment' AS `transactionTable`,
										'provider_payment_item' AS `referenceTable`
									FROM
										(
											(
												(
													`provider_payment_item`,provider_payment,provider
													
												)
												JOIN account ON(
													(
														account.account_id = provider_payment.account_from_id
													)
												)
											)
											JOIN `account_type` ON(
												(
													account_type.account_type_id = account.account_type_id
												)
											)
											
										)
										WHERE provider_payment_item.invoice_type = 3 AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
											AND provider_payment.provider_payment_status = 1 
											AND provider.provider_id = provider_payment.provider_id AND provider_payment.transaction_date >= provider.start_date

								UNION ALL

								SELECT
										`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
										`provider_payment`.`provider_payment_id` AS `referenceId`,
										`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
										`provider_payment`.`reference_number` AS `referenceCode`,
										`provider_payment`.`document_number` AS `transactionCode`,
										'' AS `patient_id`,
									  `provider_payment`.`provider_id` AS `recepientId`,
										`account`.`parent_account` AS `accountParentId`,
										`account_type`.`account_type_name` AS `accountsclassfication`,
										`account`.`account_id` AS `accountId`,
										`account`.`account_name` AS `accountName`,
										`provider_payment_item`.`description` AS `transactionName`,
										CONCAT('Payment on account')  AS `transactionDescription`,
										`provider_payment_item`.`amount_paid` AS `dr_amount`,
										0 AS `cr_amount`,
										`provider_payment`.`transaction_date` AS `transactionDate`,
										`provider_payment`.`created` AS `createdAt`,
										`provider_payment`.`created` AS `referenceDate`,
										`provider_payment_item`.`provider_payment_item_status` AS `status`,
										'Expense Payment' AS `transactionCategory`,
										'providers Invoices Payments' AS `transactionClassification`,
										'provider_payment' AS `transactionTable`,
										'provider_payment_item' AS `referenceTable`
									FROM
										(
											(
												(
													`provider_payment_item`,provider_payment,provider
													
												)
												JOIN account ON(
													(
														account.account_id = ".$providers_liability_id."
													)
												)
											)
											JOIN `account_type` ON(
												(
													account_type.account_type_id = account.account_type_id
												)
											)
											
										)
										WHERE provider_payment_item.invoice_type = 3 AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
											AND provider_payment.provider_payment_status = 1 
											AND provider.provider_id = provider_payment.provider_id AND provider_payment.transaction_date >= provider.start_date
								
							

						
							
							UNION ALL


							SELECT
								`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
								`provider_invoice`.`provider_invoice_id` AS `referenceId`,
								'' AS `payingFor`,
								`provider_invoice`.`invoice_number` AS `referenceCode`,
								`provider_invoice`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  `provider_invoice`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider_invoice_item`.`vat_amount` AS `cr_amount`,
								`provider_invoice`.`transaction_date` AS `transactionDate`,
								`provider_invoice`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
								'Provider WHT' AS `transactionCategory`,
								'providers Invoices' AS `transactionClassification`,
								'provider_invoice_item' AS `transactionTable`,
								'provider_invoice' AS `referenceTable`
							FROM
								(
									(
										(
											`provider_invoice_item`,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = ".$providers_wht_id."
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
							provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
							AND provider_invoice.provider_invoice_status = 1
							AND provider.provider_id = provider_invoice.provider_id 
							AND provider_invoice.transaction_date >= provider.start_date
							AND provider_invoice_item.vat_amount > 0

					
							UNION ALL




							SELECT
								`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
								`provider_invoice`.`provider_invoice_id` AS `referenceId`,
								'' AS `payingFor`,
								`provider_invoice`.`invoice_number` AS `referenceCode`,
								`provider_invoice`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  `provider_invoice`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
								`provider_invoice_item`.`vat_amount` AS `dr_amount`,
								0  AS `cr_amount`,
								`provider_invoice`.`transaction_date` AS `transactionDate`,
								`provider_invoice`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
								'Provider WHT' AS `transactionCategory`,
								'providers Invoices' AS `transactionClassification`,
								'provider_invoice_item' AS `transactionTable`,
								'provider_invoice' AS `referenceTable`
							FROM
								(
									(
										(
											`provider_invoice_item`,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = ".$providers_liability_id."
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
							provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
							AND provider_invoice.provider_invoice_status = 1
							AND provider.provider_id = provider_invoice.provider_id 
							AND provider_invoice.transaction_date >= provider.start_date
							AND provider_invoice_item.vat_amount > 0

							

							UNION ALL

								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payroll') AS `transactionName`,
									CONCAT('Payroll') AS `transactionDescription`,
									payroll_summary.total_payroll AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'Payroll' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 1
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id


								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('PAYE') AS `transactionName`,
									CONCAT('PAYE') AS `transactionDescription`,
									payroll_summary.paye AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 2
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id

								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NSSF') AS `transactionName`,
									CONCAT('NSSF') AS `transactionDescription`,
									payroll_summary.nssf AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 3
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id


								UNION ALL




								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NHIF') AS `transactionName`,
									CONCAT('NHIF') AS `transactionDescription`,
									payroll_summary.nhif AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'NHIF' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 4
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id


							-- payroll liability

							UNION ALL


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payroll') AS `transactionName`,
									CONCAT('Payroll') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.total_payroll AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'Payroll' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 1
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id


								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('PAYE') AS `transactionName`,
									CONCAT('PAYE') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.paye AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 2
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id

								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NSSF') AS `transactionName`,
									CONCAT('NSSF') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.nssf AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 3
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id


								UNION ALL




								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NHIF') AS `transactionName`,
									CONCAT('NHIF') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.nhif AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'NHIF' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 4
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id

							UNION ALL

							SELECT
							    `payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
							    `payroll_payment`.`payroll_payment_id` AS `referenceId`,
							    `payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
							    `payroll_payment`.`reference_number` AS `referenceCode`,
							    `payroll_payment`.`document_number` AS `transactionCode`,
							    '' AS `patient_id`,
							    `statutory_accounts`.`statutory_account_id` AS `recepientId`,
							    `account`.`parent_account` AS `accountParentId`,
							    `account_type`.`account_type_name` AS `accountsclassfication`,
							    `payroll_payment`.`account_from_id` AS `accountId`,
							    `account`.`account_name` AS `accountName`,
							    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
							    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for))   AS `transactionDescription`,
							    0 AS `dr_amount`,
							    `payroll_payment_item`.`amount_paid` AS `cr_amount`,
							    `payroll_payment`.`transaction_date` AS `transactionDate`,
							    `payroll_payment`.`created` AS `createdAt`,
							    `payroll_payment_item`.`payroll_payment_item_status` AS `status`,
							    2 AS `branch_id`,
							    'Payroll Payment' AS `transactionCategory`,
							    'Creditors Invoices Payments' AS `transactionClassification`,
							    'payroll_payment' AS `transactionTable`,
							    'payroll_payment_item' AS `referenceTable`
							FROM
							    (
							      (
							        (
							          `payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts
							          
							        )
							        JOIN account ON(
							          (
							            account.account_id = payroll_payment.account_from_id
							          )
							        )
							      )
							      JOIN `account_type` ON(
							        (
							          account_type.account_type_id = account.account_type_id
							        )
							      )
							      
							    )
							    WHERE payroll_payment_item.invoice_type = 0 
							    AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
							    AND payroll_payment.payroll_payment_status = 1
							    AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_payment_id 
							    AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id

							UNION ALL

								SELECT
								    `payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
								    `payroll_payment`.`payroll_payment_id` AS `referenceId`,
								    `payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
								    `payroll_payment`.`reference_number` AS `referenceCode`,
								    `payroll_payment`.`document_number` AS `transactionCode`,
								    '' AS `patient_id`,
								    `statutory_accounts`.`statutory_account_id` AS `recepientId`,
								    `account`.`parent_account` AS `accountParentId`,
								    `account_type`.`account_type_name` AS `accountsclassfication`,
								    account.account_id AS `accountId`,
								    `account`.`account_name` AS `accountName`,
								    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
								    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for))   AS `transactionDescription`,
								    `payroll_payment_item`.`amount_paid` AS `dr_amount`,
								    0 AS `cr_amount`,
								    `payroll_payment`.`transaction_date` AS `transactionDate`,
								    `payroll_payment`.`created` AS `createdAt`,
								    `payroll_payment_item`.`payroll_payment_item_status` AS `status`,
								    2 AS `branch_id`,
								    'Payroll Payment' AS `transactionCategory`,
								    'Creditors Invoices Payments' AS `transactionClassification`,
								    'payroll_payment' AS `transactionTable`,
								    'payroll_payment_item' AS `referenceTable`
								  FROM
								    (
								      (
								        (
								          `payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts
								          
								        )
								        JOIN account ON(
								          (
								            account.account_id = ".$payroll_liability_id."
								          )
								        )
								      )
								      JOIN `account_type` ON(
								        (
								          account_type.account_type_id = account.account_type_id
								        )
								      )
								      
								    )
								    WHERE payroll_payment_item.invoice_type = 0 
								    AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
								    AND payroll_payment.payroll_payment_status = 1
								    AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_invoice_id 
								    AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id


								UNION ALL

								 SELECT
									asset_category.asset_category_id AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									assets_details.asset_serial_no AS `referenceCode`,
									assets_details.asset_serial_no AS `transactionCode`,
									assets_details.asset_id AS `patient_id`,
								  	assets_details.supplier_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
									CONCAT(assets_details.asset_name) AS `transactionName`,
									CONCAT(assets_details.asset_name) AS `transactionDescription`,
									(assets_details.asset_value) AS `dr_amount`,
									0 AS `cr_amount`,
									assets_details.asset_pd_period AS `transactionDate`,
									assets_details.asset_pd_period  AS `createdAt`,
									assets_details.asset_pd_period AS `referenceDate`,
									1 AS `status`,
									'FIXED ASSET' AS `transactionCategory`,
									'Asset Purchase' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
									assets_details,asset_category,account,account_type
									WHERE 
									assets_details.asset_category_id = asset_category.asset_category_id 
									AND account.account_id = asset_category.account_id
									AND account.account_type_id = account_type.account_type_id

								UNION ALL


								SELECT
									asset_category.asset_category_id AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									assets_details.asset_serial_no AS `referenceCode`,
									assets_details.asset_serial_no AS `transactionCode`,
									assets_details.asset_id AS `patient_id`,
								  	assets_details.supplier_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
									CONCAT(assets_details.asset_name) AS `transactionName`,
									CONCAT(assets_details.asset_name) AS `transactionDescription`,
									0 AS `dr_amount`,
									(assets_details.asset_value) AS `cr_amount`,
									assets_details.asset_pd_period AS `transactionDate`,
									assets_details.asset_pd_period  AS `createdAt`,
									assets_details.asset_pd_period AS `referenceDate`,
									1 AS `status`,
									'FIXED ASSET' AS `transactionCategory`,
									'Asset Purchase' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
									assets_details,asset_category,account,account_type
									WHERE 
									assets_details.asset_category_id = asset_category.asset_category_id
									AND assets_details.bill_asset = 1 
									AND account.account_id = ".$accounts_payable_id."
									AND account.account_type_id = account_type.account_type_id

							UNION ALL

								SELECT


									patients_journals.patient_journal_id AS transactionId,
									patients_journals.patient_journal_id AS referenceId,
									'' AS `payingFor`,
									'' AS referenceCode,
									'' AS `transactionCode`,
									patients_journals.patient_id AS patient_id,
									patients_journals.patient_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									account.account_name AS `accountName`,
									CONCAT('Patient Journals') AS `transactionName`,
									CONCAT('Patient Journals') AS `transactionDescription`,
									patients_journals.journal_amount AS `dr_amount`,
									0 AS `cr_amount`,
									patients_journals.journal_date AS `transactionDate`,
									patients_journals.journal_date  AS `createdAt`,
									patients_journals.journal_date AS `referenceDate`,
									1 AS `status`,
									'JOURNAL' AS `transactionCategory`,
									'JOURNAL' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
										patients_journals,visit_type,account,account_type
									WHERE 
									patients_journals.visit_type_id = visit_type.visit_type_id 
									AND patients_journals.visit_invoice_id > 0
									AND patients_journals.account_from_id = account.account_id
									AND account_type.account_type_id = account.account_type_id

							UNION ALL

								SELECT


									patients_journals.patient_journal_id AS transactionId,
									patients_journals.patient_journal_id AS referenceId,
									'' AS `payingFor`,
									'' AS referenceCode,
									'' AS `transactionCode`,
									patients_journals.patient_id AS patient_id,
									patients_journals.patient_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									account.account_name AS `accountName`,
									CONCAT('Patient Journals') AS `transactionName`,
									CONCAT('Patient Journals') AS `transactionDescription`,
									0 AS `dr_amount`,
									patients_journals.journal_amount AS `cr_amount`,
									patients_journals.journal_date AS `transactionDate`,
									patients_journals.journal_date  AS `createdAt`,
									patients_journals.journal_date AS `referenceDate`,
									1 AS `status`,
									'JOURNAL' AS `transactionCategory`,
									'JOURNAL' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
										patients_journals,visit_type,account,account_type
									WHERE 
									patients_journals.visit_type_id = visit_type.visit_type_id 
									AND patients_journals.visit_invoice_id > 0
									AND patients_journals.account_to_id = account.account_id
									AND account_type.account_type_id = account.account_type_id
								UNION ALL

								SELECT
									`loan_invoice_item`.`loan_invoice_item_id` AS `transactionId`,
									`loan_invoice`.`loan_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`loan_invoice`.`invoice_number` AS `referenceCode`,
									`loan_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_invoice`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`loan_invoice_item`.`item_description` AS `transactionName`,
									`loan_invoice_item`.`item_description` AS `transactionDescription`,
									 0 AS `dr_amount`,
									`loan_invoice_item`.`total_amount` AS `cr_amount`,
									`loan_invoice`.`transaction_date` AS `transactionDate`,
									`loan_invoice`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_invoice_item`.`loan_invoice_item_status` AS `status`,
									'Expense' AS `transactionCategory`,
									'loans Invoices' AS `transactionClassification`,
									'loan_invoice_item' AS `transactionTable`,
									'loan_invoice' AS `referenceTable`
								FROM
									(
										(
											(
												`loan_invoice_item`,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
									)
								WHERE 
								loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id 
								AND loan_invoice.loan_invoice_status = 1
								AND loan.loan_id = loan_invoice.loan_id 
								AND loan_invoice.transaction_date >= loan.start_date

							UNION ALL

								SELECT
									`loan_invoice_item`.`loan_invoice_item_id` AS `transactionId`,
									`loan_invoice`.`loan_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`loan_invoice`.`invoice_number` AS `referenceCode`,
									`loan_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_invoice`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan`.`account_to_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`loan_invoice_item`.`item_description` AS `transactionName`,
									`loan_invoice_item`.`item_description` AS `transactionDescription`,
									`loan_invoice_item`.`total_amount` AS `dr_amount`,
									'0' AS `cr_amount`,
									`loan_invoice`.`transaction_date` AS `transactionDate`,
									`loan_invoice`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_invoice_item`.`loan_invoice_item_status` AS `status`,
									'Expense' AS `transactionCategory`,
									'loans Invoices' AS `transactionClassification`,
									'loan_invoice_item' AS `transactionTable`,
									'loan_invoice' AS `referenceTable`
								FROM
									(
										(
											(
												`loan_invoice_item`,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan.account_to_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
									)
								WHERE 
								loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id 
								AND loan_invoice.loan_invoice_status = 1
								AND loan.loan_id = loan_invoice.loan_id 
								AND loan_invoice.transaction_date >= loan.start_date

								UNION ALL

								SELECT
									`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
									`loan_payment`.`loan_payment_id` AS `referenceId`,
									`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
									`loan_payment`.`reference_number` AS `referenceCode`,
									`loan_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_payment`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan_payment`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
									CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
									0 AS `dr_amount`,
									`loan_payment_item`.`amount_paid` AS `cr_amount`,
									`loan_payment`.`transaction_date` AS `transactionDate`,
									`loan_payment`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_payment_item`.`loan_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'loans Invoices Payments' AS `transactionClassification`,
									'loan_payment' AS `transactionTable`,
									'loan_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`loan_payment_item`,loan_payment,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan_payment.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE loan_payment_item.invoice_type = 0 
									AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
									AND loan_payment.loan_payment_status = 1
									AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
									AND loan_invoice.loan_invoice_status = 1  
									AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
									AND loan.account_from_id = loan_payment_item.account_to_id
									
							UNION ALL
							
							SELECT
									`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
									`loan_payment`.`loan_payment_id` AS `referenceId`,
									`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
									`loan_payment`.`reference_number` AS `referenceCode`,
									`loan_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_payment`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
									CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
									`loan_payment_item`.`amount_paid` AS `dr_amount`,
									0 AS `cr_amount`,
									`loan_payment`.`transaction_date` AS `transactionDate`,
									`loan_payment`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_payment_item`.`loan_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'loans Invoices Payments' AS `transactionClassification`,
									'loan_payment' AS `transactionTable`,
									'loan_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`loan_payment_item`,loan_payment,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE loan_payment_item.invoice_type = 0 
									AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
									AND loan_payment.loan_payment_status = 1
									AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
									AND loan_invoice.loan_invoice_status = 1  
									AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
									AND loan.account_from_id = loan_payment_item.account_to_id

									UNION ALL

									SELECT
											`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
											`loan_payment`.`loan_payment_id` AS `referenceId`,
											`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
											`loan_payment`.`reference_number` AS `referenceCode`,
											`loan_payment`.`document_number` AS `transactionCode`,
											'' AS `patient_id`,
												`loan_payment`.`loan_id` AS `recepientId`,
											`account`.`parent_account` AS `accountParentId`,
											`account_type`.`account_type_name` AS `accountsclassfication`,
											loan_payment_item.account_to_id AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
											CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
											`loan_payment_item`.`amount_paid` AS `dr_amount`,
											0 AS `cr_amount`,
											`loan_payment`.`transaction_date` AS `transactionDate`,
											`loan_payment`.`created` AS `createdAt`,
											`loan_invoice`.`transaction_date` AS `referenceDate`,
											`loan_payment_item`.`loan_payment_item_status` AS `status`,
											'Expense Payment' AS `transactionCategory`,
											'loans Invoices Payments' AS `transactionClassification`,
											'loan_payment' AS `transactionTable`,
											'loan_payment_item' AS `referenceTable`
										FROM
											(
												(
													(
														`loan_payment_item`,loan_payment,loan_invoice,loan,account
														
													)
												
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
											WHERE loan_payment_item.invoice_type = 0 
											AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
											AND loan_payment.loan_payment_status = 1
											AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
											AND loan_invoice.loan_invoice_status = 1  
											AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
											AND loan.account_from_id <> loan_payment_item.account_to_id
											AND account.account_id = loan_payment_item.account_to_id
											
											UNION ALL
											
											
											SELECT
											`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
											`loan_payment`.`loan_payment_id` AS `referenceId`,
											`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
											`loan_payment`.`reference_number` AS `referenceCode`,
											`loan_payment`.`document_number` AS `transactionCode`,
											'' AS `patient_id`,
												`loan_payment`.`loan_id` AS `recepientId`,
											`account`.`parent_account` AS `accountParentId`,
											`account_type`.`account_type_name` AS `accountsclassfication`,
											loan_payment.account_from_id AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
											CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
											0 AS `dr_amount`,
											`loan_payment_item`.`amount_paid` AS `cr_amount`,
											`loan_payment`.`transaction_date` AS `transactionDate`,
											`loan_payment`.`created` AS `createdAt`,
											`loan_invoice`.`transaction_date` AS `referenceDate`,
											`loan_payment_item`.`loan_payment_item_status` AS `status`,
											'Expense Payment' AS `transactionCategory`,
											'loans Invoices Payments' AS `transactionClassification`,
											'loan_payment' AS `transactionTable`,
											'loan_payment_item' AS `referenceTable`
										FROM
											(
												(
													(
														`loan_payment_item`,loan_payment,loan_invoice,loan,account
														
													)
												
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
											WHERE loan_payment_item.invoice_type = 0 
											AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
											AND loan_payment.loan_payment_status = 1
											AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
											AND loan_invoice.loan_invoice_status = 1  
											AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
											AND loan.account_from_id <> loan_payment_item.account_to_id
											AND account.account_id = loan_payment.account_from_id
										UNION ALL


										SELECT
											asset_category.asset_category_id AS `transactionId`,
											'' AS `referenceId`,
											'' AS `payingFor`,
											assets_details.asset_serial_no AS `referenceCode`,
											assets_details.asset_serial_no AS `transactionCode`,
											assets_details.asset_id AS `patient_id`,
												assets_details.supplier_id AS `recepientId`,
											account_type.account_type_id AS `accountParentId`,
											account_type.account_type_name AS `accountsclassfication`,
											`account`.`account_id` AS `accountId`,
											CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
											CONCAT(assets_details.asset_name) AS `transactionName`,
											CONCAT(assets_details.asset_name) AS `transactionDescription`,
											(asset_amortization.interest_amount) AS `dr_amount`,
											0 AS `cr_amount`,
											asset_amortization.date_approved AS `transactionDate`,
											asset_amortization.date_approved  AS `createdAt`,
											asset_amortization.date_approved AS `referenceDate`,
											1 AS `status`,
											'FIXED ASSET' AS `transactionCategory`,
											'ASSET DEPRECIATION' AS `transactionClassification`,
											'account_payments' AS `transactionTable`,
											'' AS `referenceTable`
										FROM
											assets_details,asset_category,account,account_type,asset_amortization
											WHERE 
											assets_details.asset_category_id = asset_category.asset_category_id 
											AND asset_amortization.expense_account_id = account.account_id
											AND account.account_type_id = account_type.account_type_id
											AND asset_amortization.asset_id = assets_details.asset_id
											AND asset_amortization.bill_status = 1
											
									UNION ALL

									SELECT
											asset_category.asset_category_id AS `transactionId`,
											'' AS `referenceId`,
											'' AS `payingFor`,
											assets_details.asset_serial_no AS `referenceCode`,
											assets_details.asset_serial_no AS `transactionCode`,
											assets_details.asset_id AS `patient_id`,
												assets_details.supplier_id AS `recepientId`,
											account_type.account_type_id AS `accountParentId`,
											account_type.account_type_name AS `accountsclassfication`,
											`account`.`account_id` AS `accountId`,
											CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
											CONCAT(assets_details.asset_name) AS `transactionName`,
											CONCAT(assets_details.asset_name) AS `transactionDescription`,
											0 AS `dr_amount`,
											(asset_amortization.interest_amount) AS `cr_amount`,
											asset_amortization.date_approved AS `transactionDate`,
											asset_amortization.date_approved  AS `createdAt`,
											asset_amortization.date_approved AS `referenceDate`,
											1 AS `status`,
											'FIXED ASSET' AS `transactionCategory`,
											'ASSET DEPRECIATION' AS `transactionClassification`,
											'account_payments' AS `transactionTable`,
											'' AS `referenceTable`
										FROM
											assets_details,asset_category,account,account_type,asset_amortization
											WHERE 
											assets_details.asset_category_id = asset_category.asset_category_id 
											AND asset_amortization.acc_depreciation_id = account.account_id
											AND account.account_type_id = account_type.account_type_id
											AND asset_amortization.asset_id = assets_details.asset_id
											AND asset_amortization.bill_status = 1


					) AS data WHERE data.accountId in ('".implode("','", $array_leads)."')  ".$add." ".$add_item."  ORDER BY data.transactionDate ASC

					";
	 

	  $query = $this->db->query($select);


	   return $query;

	}
	public function get_account_ledger_by_accounts($ledger_type=0,$type=0)
	{
		
		// var_dump("sasa");die();
		$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

		$patients = array();
		
		if($accounts_config_rs->num_rows() > 0)
		{
			foreach ($accounts_config_rs->result() as $key => $value) {
				// code...
				$staing_account_id = $value->account_id;
				$reference_name = $value->reference_name;

				$session_account[$reference_name] = $staing_account_id;

				
			}
		}

		$providers_liability_id = $session_account['providers_liability_id'];
		$providers_wht_id = $session_account['providers_wht_id'];
		$payroll_liability_id = $session_account['payroll_liability_id'];
		$accounts_payable_id = $session_account['accounts_payable_id'];
		$fixed_assets_id = $session_account['fixed_assets_id'];
		$accounts_receivable_id = $session_account['accounts_receivable_id'];
		$suppliers_wht_id = $session_account['suppliers_wht_id'];
		$income_account_id = $session_account['income_account_id'];
		$supplier_opening_balance_id= $session_account['supplier_opening_balance_id'];
		$bank_opening_balance_id = $session_account['bank_opening_balance_id'];
		$provider_opening_balance_id = $session_account['provider_opening_balance_id'];

		
		$date_from = $this->session->userdata('date_from_general_ledger');
		$date_to = $this->session->userdata('date_to_general_ledger');
		$add = '';
		if(!empty($date_from) OR !empty($date_to))
    	{
    		
    			$add =  '  AND DATE(transactionDate) >= \''.$date_from.'\' AND DATE(transactionDate) <= \''.$date_to.'\' ';
    		
    		
    	}
    	else if(!empty($date_from) AND empty($date_to))
    	{

    		
    			$add = '  AND DATE(transactionDate) = \''.$date_from.'\'';
    		
    		
    	}
    	else if(empty($date_from) AND !empty($date_to))
    	{
    		
    			$add = '  AND DATE(transactionDate) = \''.$date_to.'\'';
    		
    	}
    	else
    	{

    		
    			$add = ' AND DATE(transactionDate) >= "'.date('Y-m-01').'" AND DATE(transactionDate) <= "'.date('Y-m-d').'" ';
    		
    		// $add = '';
    		
    	}

		// $add = " AND data.transactionDate <= '2021-12-31'";
		if($ledger_type == 1)
    	{
    		
    		$add_item = 'GROUP BY data.accountId';
    	}
    	else
    	{
    		$add_item = '';
    	}

    	// $creditors = $this->load->view('financials/ledgers/creditors_sql', $session_account,true);
    	// var_dump($creditors);die();
    	if($ledger_type == 1)
    	{
			$item = '	data.accountId AS accountId,
						data.accountName AS account_name,
					 	SUM(data.dr_amount) AS dr_amount,
					 	SUM(data.cr_amount) AS cr_amount';
    	}
    	else
    	{
    		$item = '*';
    	}
		$select  = "
					SELECT 
						".$item."
					FROM 

					(

						SELECT
							`account`.`account_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
						    '' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							account.account_id AS `accountId`,
							account.account_name AS `accountName`,
							CONCAT('Opening Balance as from',' ',`account`.`start_date`) AS `transactionName`,
							CONCAT('Opening Balance as from',' ',' ',`account`.`start_date`) AS `transactionDescription`,
							`account`.`account_opening_balance` AS `dr_amount`,
							'0' AS `cr_amount`,
							`account`.`start_date` AS `transactionDate`,
							`account`.`start_date` AS `createdAt`,
							`account`.`account_status` AS `status`,
							branch_id AS `branch_id`,
							'Income' AS `transactionCategory`,
							'Account Opening Balance' AS `transactionClassification`,
							'' AS `transactionTable`,
							'account' AS `referenceTable`
						FROM
						account,account_type

						WHERE 

						account_type.account_type_id = account.account_type_id AND account.account_opening_balance > 0 AND account.parent_account > 0  AND (account_type.account_type_name = 'Bank' OR account_type.account_type_name ='Capital')

						UNION ALL

	
						SELECT
							`account`.`account_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							account.account_id AS `accountId`,
							account.account_name AS `accountName`,
							CONCAT( 'Opening Balance as from', ' ', `account`.`start_date` ) AS `transactionName`,
							CONCAT( 'Opening Balance as from', ' ', ' ', `account`.`start_date` ) AS `transactionDescription`,
							0 AS `dr_amount`,
							(SELECT COALESCE(SUM(account_opening_balance),0) FROM account,account_type WHERE  account_type.account_type_id = account.account_type_id  AND account.account_opening_balance > 0  AND (
							account_type.account_type_name = 'Bank' 
							OR account_type.account_type_name = 'Capital') ) AS `cr_amount`,
							`account`.`start_date` AS `transactionDate`,
							`account`.`start_date` AS `createdAt`,
							`account`.`account_status` AS `status`,
							branch_id AS `branch_id`,
							'Income' AS `transactionCategory`,
							'Account Opening Balance' AS `transactionClassification`,
							'' AS `transactionTable`,
							'account' AS `referenceTable` 
						FROM
							account,
							account_type 
						WHERE
							account_type.account_type_id = account.account_type_id 
							AND account.parent_account > 0 
							AND account.account_id = ".$bank_opening_balance_id." 



						UNION ALL

						SELECT
						  	`finance_transfered`.`finance_transfered_id` AS `transactionId`,
						  	`finance_transfer`.`finance_transfer_id` AS `referenceId`,
						  	'' AS `payingFor`,
						  	`finance_transfer`.`reference_number` AS `referenceCode`,
						  	`finance_transfer`.`document_number` AS `transactionCode`,
						  	'' AS `patient_id`,
						    '' AS `recepientId`,
						  	`account`.`parent_account` AS `accountParentId`,
						  	`account_type`.`account_type_name` AS `accountsclassfication`,
						  	`finance_transfered`.`account_to_id` AS `accountId`,
						  	`account`.`account_name` AS `accountName`,
						  	`finance_transfered`.`remarks` AS `transactionName`,
						  	 CONCAT('Amount Received from ',(SELECT account_name FROM account WHERE account_id = finance_transfer.account_from_id ),' Ref. ', `finance_transfer`.`reference_number`) AS `transactionDescription`,
						  	`finance_transfer`.`finance_transfer_amount` AS `cr_amount`,
						     0 AS `cr_amount`,
						  	`finance_transfer`.`transaction_date` AS `transactionDate`,
						  	`finance_transfer`.`created` AS `createdAt`,
						  	`finance_transfer`.`finance_transfer_status` AS `status`,
						  	`finance_transfer`.`branch_id` AS `branch_id`,
						  	'Transfer' AS `transactionCategory`,
						  	'Transfer' AS `transactionClassification`,
						  	'finance_transfer' AS `transactionTable`,
						  	'finance_transfered' AS `referenceTable`
						  FROM
						  `finance_transfer`,finance_transfered,account,account_type
						   WHERE finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id 
						  	AND account.account_id = finance_transfered.account_to_id
						  	AND account_type.account_type_id = account.account_type_id
						  	AND finance_transfer.finance_transfer_deleted = 0

					UNION ALL

						SELECT
						  	`finance_transfer`.`finance_transfer_id` AS `transactionId`,
						  	`finance_transfered`.`finance_transfered_id` AS `referenceId`,
						  	'' AS `payingFor`,
						  	`finance_transfer`.`reference_number` AS `referenceCode`,
						  	`finance_transfer`.`document_number` AS `transactionCode`,
						  	'' AS `patient_id`,
						    '' AS `recepientId`,
						  	`account`.`parent_account` AS `accountParentId`,
						  	`account_type`.`account_type_name` AS `accountsclassfication`,
						  	`finance_transfer`.`account_from_id` AS `accountId`,
						  	`account`.`account_name` AS `accountName`,
						  	`finance_transfer`.`remarks` AS `transactionName`,
						  	CONCAT(' Amount Transfered to ',(SELECT account_name FROM account WHERE account_id = finance_transfered.account_to_id )) AS `transactionDescription`,
						  	0 AS `dr_amount`,
						  	`finance_transfer`.`finance_transfer_amount` AS `cr_amount`,
						  	`finance_transfer`.`transaction_date` AS `transactionDate`,
						  	`finance_transfer`.`transaction_date` AS `transactionDate`,

						  	`finance_transfer`.`finance_transfer_status` AS `status`,
						  	`finance_transfer`.`branch_id` AS `branch_id`,
						  	'Transfer' AS `transactionCategory`,
						  	'Transfer' AS `transactionClassification`,
						  	'finance_transfered' AS `transactionTable`,
						  	'finance_transfer' AS `referenceTable`
						  FROM
							`finance_transfer`,finance_transfered,account,account_type
						  				
						  WHERE finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id 
						  	AND account.account_id = finance_transfer.account_from_id
						  	AND account_type.account_type_id = account.account_type_id
						  	AND finance_transfer.finance_transfer_deleted = 0

						UNION ALL


						SELECT
							`finance_purchase_payment`.`finance_purchase_payment_id` AS `transactionId`,
							'' AS `referenceId`,
							`finance_purchase`.`finance_purchase_id` AS `payingFor`,
							`finance_purchase`.`transaction_number` AS `referenceCode`,
							`finance_purchase`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
						  	finance_purchase.creditor_id AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`finance_purchase_payment`.`account_from_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`finance_purchase`.`finance_purchase_description` AS `transactionName`,
							CONCAT( `finance_purchase`.`finance_purchase_description`) AS `transactionDescription`,
							0 AS `dr_amount`,
							`finance_purchase_payment`.`amount_paid` AS `cr_amount`,
							`finance_purchase`.`transaction_date` AS `transactionDate`,
							`finance_purchase`.`created` AS `createdAt`,
							`finance_purchase_payment`.`finance_purchase_payment_status` AS `status`,
							`finance_purchase`.`branch_id` AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Purchase Payment' AS `transactionClassification`,
							'finance_purchase' AS `transactionTable`,
							'finance_purchase_payment' AS `referenceTable`
						FROM
						`finance_purchase_payment`,finance_purchase,account,account_type
						WHERE finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id AND finance_purchase.finance_purchase_deleted = 0 AND account.account_id = finance_purchase_payment.account_from_id
							AND finance_purchase.finance_purchase_delete = 0 AND account_type.account_type_id = account.account_type_id
							AND finance_purchase.account_to_id > 0 

						UNION ALL

						SELECT
							`finance_purchase`.`finance_purchase_id` AS `transactionId`,
							'' AS `referenceId`,
							`finance_purchase`.`finance_purchase_id` AS `payingFor`,
							`finance_purchase`.`transaction_number` AS `referenceCode`,
							`finance_purchase`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`finance_purchase`.`creditor_id` AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`finance_purchase`.`account_to_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`finance_purchase`.`finance_purchase_description` AS `transactionName`,
							CONCAT( `finance_purchase`.`finance_purchase_description`)  AS `transactionDescription`,
							`finance_purchase_payment`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`finance_purchase`.`transaction_date` AS `transactionDate`,
							`finance_purchase`.`created` AS `createdAt`,
							`finance_purchase`.`finance_purchase_status` AS `status`,
							`finance_purchase`.`branch_id` AS `branch_id`,
							'Expense' AS `transactionCategory`,
							'Purchases' AS `transactionClassification`,
							'finance_purchase' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							`finance_purchase_payment`,finance_purchase,account,account_type
						WHERE finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id AND finance_purchase.finance_purchase_deleted = 0 
						AND account.account_id = finance_purchase.account_to_id
							AND finance_purchase.finance_purchase_delete = 0
							AND account_type.account_type_id = account.account_type_id
							AND finance_purchase.account_to_id > 0 

						UNION ALL

					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor`.`opening_balance` AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`
					FROM
					creditor,account,account_type
					WHERE debit_id = 1 AND account.account_id = ".$supplier_opening_balance_id." AND account.account_type_id = account_type.account_type_id

					UNION ALL

					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						`creditor`.`opening_balance` AS `dr_amount`,
						'0' AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						 creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`
					FROM
					creditor,account,account_type
					WHERE debit_id = 2 AND account.account_id = ".$supplier_opening_balance_id." AND account.account_type_id = account_type.account_type_id


					UNION ALL


					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						`creditor`.`opening_balance` AS `dr_amount`,
						0 AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`
					FROM
					creditor,account,account_type
					WHERE debit_id = 1 AND account.account_id = ".$accounts_payable_id." AND account.account_type_id = account_type.account_type_id

					UNION ALL

					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor`.`opening_balance`  AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						 creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`
					FROM
					creditor,account,account_type
					WHERE debit_id = 2 AND account.account_id = ".$accounts_payable_id." AND account.account_type_id = account_type.account_type_id
				

					UNION ALL


					SELECT
						`creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
						`creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
						'' AS `payingFor`,
						`creditor_invoice`.`invoice_number` AS `referenceCode`,
						`creditor_invoice`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
					  `creditor_invoice`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`creditor_invoice_item`.`account_to_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_invoice_item`.`item_description` AS `transactionName`,
						CONCAT(`creditor_invoice_item`.`item_description`,' ',creditor_invoice.invoice_number) AS `transactionDescription`,
						`creditor_invoice_item`.`total_amount` AS `dr_amount`,
						'0' AS `cr_amount`,
						`creditor_invoice`.`transaction_date` AS `transactionDate`,
						`creditor_invoice`.`created` AS `createdAt`,
						`creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
						'creditor.branch_id' AS `branch_id`,
						'Cost of Goods' AS `transactionCategory`,
						'Creditors Invoices' AS `transactionClassification`,
						'creditor_invoice_item' AS `transactionTable`,
						'creditor_invoice' AS `referenceTable`
					FROM
						(
							(
								(
									`creditor_invoice_item`,creditor_invoice,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = creditor_invoice_item.account_to_id
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)
						)
					WHERE 

					creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id 
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date

				UNION ALL

				  SELECT
					`creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
					`creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
					`creditor_credit_note_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_credit_note`.`invoice_number` AS `referenceCode`,
					`creditor_credit_note`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_credit_note`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`creditor_credit_note`.`account_from_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_credit_note_item`.`description` AS `transactionName`,
					`creditor_credit_note_item`.`description` AS `transactionDescription`,
					0 AS `dr_amount`,
					`creditor_credit_note_item`.`credit_note_amount` AS `cr_amount`,
					`creditor_credit_note`.`transaction_date` AS `transactionDate`,
					`creditor_credit_note`.`created` AS `createdAt`,
					`creditor_invoice`.`transaction_date` AS `referenceDate`,
					`creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
					'Supplier Credit Note' AS `transactionCategory`,
					'Creditors Credit Notes' AS `transactionClassification`,
					'creditor_credit_note' AS `transactionTable`,
					'creditor_credit_note_item' AS `referenceTable`
				FROM
					(
						(
							(
								`creditor_credit_note_item`,creditor_credit_note,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = creditor_credit_note.account_from_id
								)
							)
							
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					)
				WHERE 
					creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id 
					AND creditor_credit_note.creditor_credit_note_status = 1
					AND creditor_invoice.creditor_invoice_id = creditor_credit_note_item.creditor_invoice_id
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date


				UNION ALL


				SELECT
						`creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
						`creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
						'' AS `payingFor`,
						`creditor_invoice`.`invoice_number` AS `referenceCode`,
						`creditor_invoice`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
					  `creditor_invoice`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`account`.`account_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_invoice_item`.`item_description` AS `transactionName`,
						CONCAT(`creditor_invoice_item`.`item_description`,' ',creditor_invoice.invoice_number) AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor_invoice_item`.`total_amount` AS `cr_amount`,
						`creditor_invoice`.`transaction_date` AS `transactionDate`,
						`creditor_invoice`.`created` AS `createdAt`,
						`creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
						'creditor.branch_id' AS `branch_id`,
						'Cost of Goods' AS `transactionCategory`,
						'Creditors Invoices' AS `transactionClassification`,
						'creditor_invoice_item' AS `transactionTable`,
						'creditor_invoice' AS `referenceTable`
					FROM
						(
							(
								(
									`creditor_invoice_item`,creditor_invoice,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = ".$accounts_payable_id."
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)
						)
					WHERE 

					creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id 
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date

				UNION ALL

				  SELECT
					`creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
					`creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
					`creditor_credit_note_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_credit_note`.`invoice_number` AS `referenceCode`,
					`creditor_credit_note`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_credit_note`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`account`.`account_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_credit_note_item`.`description` AS `transactionName`,
					`creditor_credit_note_item`.`description` AS `transactionDescription`,
					`creditor_credit_note_item`.`credit_note_amount` AS `dr_amount`,
					0 AS `cr_amount`,
					`creditor_credit_note`.`transaction_date` AS `transactionDate`,
					`creditor_credit_note`.`created` AS `createdAt`,
					`creditor_invoice`.`transaction_date` AS `referenceDate`,
					`creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
					'Supplier Credit Note' AS `transactionCategory`,
					'Creditors Credit Notes' AS `transactionClassification`,
					'creditor_credit_note' AS `transactionTable`,
					'creditor_credit_note_item' AS `referenceTable`
				FROM
					(
						(
							(
								`creditor_credit_note_item`,creditor_credit_note,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = ".$accounts_payable_id."
								)
							)
							
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					)
				WHERE 
					creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id 
					AND creditor_credit_note.creditor_credit_note_status = 1
					AND creditor_invoice.creditor_invoice_id = creditor_credit_note_item.creditor_invoice_id
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date



				UNION ALL


				SELECT
					`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
					`creditor_payment`.`creditor_payment_id` AS `referenceId`,
					`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_payment`.`reference_number` AS `referenceCode`,
					`creditor_payment`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_payment`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`creditor_payment`.`account_from_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_payment_item`.`description` AS `transactionName`,
					CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
					0 AS `dr_amount`,
					`creditor_payment_item`.`amount_paid` AS `cr_amount`,
					`creditor_payment`.`transaction_date` AS `transactionDate`,
					`creditor_payment`.`created` AS `createdAt`,
					`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
					'creditor_payment.branch_id' AS `branch_id`,
					'Creditor Payment' AS `transactionCategory`,
					'Creditors Invoices Payments' AS `transactionClassification`,
					'creditor_payment' AS `transactionTable`,
					'creditor_payment_item' AS `referenceTable`
				FROM
					(
						(
							(
								`creditor_payment_item`,creditor_payment,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = creditor_payment.account_from_id
								)
							)
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					
					)
					WHERE creditor_payment_item.invoice_type = 0 
					AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
					AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
					AND creditor_payment.creditor_payment_status = 1
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor_invoice.creditor_id = creditor.creditor_id
					AND creditor_payment.transaction_date >= creditor.start_date

				UNION ALL


				SELECT
					`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
					`creditor_payment`.`creditor_payment_id` AS `referenceId`,
					`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_payment`.`reference_number` AS `referenceCode`,
					`creditor_payment`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_payment`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`account`.`account_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_payment_item`.`description` AS `transactionName`,
					CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
					`creditor_payment_item`.`amount_paid` AS `dr_amount`,
					0 AS `cr_amount`,
					`creditor_payment`.`transaction_date` AS `transactionDate`,
					`creditor_payment`.`created` AS `createdAt`,
					`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
					'creditor_payment.branch_id' AS `branch_id`,
					'Creditor Payment' AS `transactionCategory`,
					'Creditors Invoices Payments' AS `transactionClassification`,
					'creditor_payment' AS `transactionTable`,
					'creditor_payment_item' AS `referenceTable`
				FROM
					(
						(
							(
								`creditor_payment_item`,creditor_payment,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = ".$accounts_payable_id."
								)
							)
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					
					)
					WHERE creditor_payment_item.invoice_type = 0 
					AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
					AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
					AND creditor_payment.creditor_payment_status = 1
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor_invoice.creditor_id = creditor.creditor_id
					AND creditor_payment.transaction_date >= creditor.start_date

				UNION ALL

					SELECT
						`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
						`creditor_payment`.`creditor_payment_id` AS `referenceId`,
						`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
						`creditor_payment`.`reference_number` AS `referenceCode`,
						`creditor_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
						`creditor_payment`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`creditor_payment`.`account_from_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_payment_item`.`description` AS `transactionName`,
						CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor_payment_item`.`amount_paid` AS `cr_amount`,
						`creditor_payment`.`transaction_date` AS `transactionDate`,
						`creditor_payment`.`created` AS `createdAt`,
						`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
						'creditor_payment.branch_id' AS `branch_id`,
						'Creditor Payment' AS `transactionCategory`,
						'Creditors Invoices Payments' AS `transactionClassification`,
						'creditor_payment' AS `transactionTable`,
						'creditor_payment_item' AS `referenceTable`
						FROM
						(
							(
								(
									`creditor_payment_item`,creditor_payment,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = creditor_payment.account_from_id
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)

						)
						WHERE creditor_payment_item.invoice_type = 2 
						AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
						AND creditor.creditor_id = creditor_payment_item.creditor_id
						AND creditor_payment.transaction_date >= creditor.start_date

				UNION ALL

					SELECT
						`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
						`creditor_payment`.`creditor_payment_id` AS `referenceId`,
						`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
						`creditor_payment`.`reference_number` AS `referenceCode`,
						`creditor_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
						`creditor_payment`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`account`.`account_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_payment_item`.`description` AS `transactionName`,
						CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
						`creditor_payment_item`.`amount_paid` AS `dr_amount`,
						0 AS `cr_amount`,
						`creditor_payment`.`transaction_date` AS `transactionDate`,
						`creditor_payment`.`created` AS `createdAt`,
						`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
						'creditor_payment.branch_id' AS `branch_id`,
						'Creditor Payment' AS `transactionCategory`,
						'Creditors Invoices Payments' AS `transactionClassification`,
						'creditor_payment' AS `transactionTable`,
						'creditor_payment_item' AS `referenceTable`
						FROM
						(
							(
								(
									`creditor_payment_item`,creditor_payment,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = ".$accounts_payable_id."
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)

						)
						WHERE creditor_payment_item.invoice_type = 2 
						AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
						AND creditor.creditor_id = creditor_payment_item.creditor_id
						AND creditor_payment.transaction_date >= creditor.start_date

					UNION ALL

						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`creditor_payment`.`account_from_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							0 AS `dr_amount`,
							`creditor_payment_item`.`amount_paid` AS `cr_amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							'creditor_payment.branch_id' AS `branch_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`
							FROM
							(
								(
									(
										`creditor_payment_item`,creditor_payment,creditor
										
									)
									JOIN account ON(
										(
											account.account_id = creditor_payment.account_from_id
										)
									)
								)
								JOIN `account_type` ON(
									(
										account_type.account_type_id = account.account_type_id
									)
								)
								
							)
							WHERE creditor_payment_item.invoice_type = 3
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
							AND creditor.creditor_id = creditor_payment_item.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date
					UNION ALL

						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							`creditor_payment_item`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							'creditor_payment.branch_id' AS `branch_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`
							FROM
							(
								(
									(
										`creditor_payment_item`,creditor_payment,creditor
										
									)
									JOIN account ON(
										(
											account.account_id = ".$accounts_payable_id."
										)
									)
								)
								JOIN `account_type` ON(
									(
										account_type.account_type_id = account.account_type_id
									)
								)
								
							)
							WHERE creditor_payment_item.invoice_type = 3
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
							AND creditor.creditor_id = creditor_payment_item.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date


						UNION ALL

							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							 	`orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Drug Purchase' AS `transactionName`,
								CONCAT('Purchase of supplies',' ',orders.supplier_invoice_number) AS `transactionDescription`,
								SUM(`order_supplier`.`less_vat`) AS `dr_amount`,
								0 AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Purchases' AS `transactionCategory`,
								'Supplies Invoices' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`
							FROM
								`order_supplier`,orders,account,order_item,product,account_type,creditor
										

							WHERE orders.is_store = 0 
								AND orders.supplier_id = creditor.creditor_id
								AND orders.order_id = order_supplier.order_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND product.product_id = order_item.product_id
								AND account_type.account_type_id = account.account_type_id
								AND account.account_id = orders.account_id
								AND orders.order_approval_status = 7 
								AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id

							UNION ALL



							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							 	`orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Drug Purchase' AS `transactionName`,
								CONCAT('Purchase of supplies',' ',orders.supplier_invoice_number) AS `transactionDescription`,
								0 AS `dr_amount`,
								SUM(`order_supplier`.`less_vat`) AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Purchases' AS `transactionCategory`,
								'Supplies Invoices' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`
							FROM
								`order_supplier`,orders,account,order_item,product,account_type,creditor
										

							WHERE orders.is_store = 0 
								AND orders.supplier_id = creditor.creditor_id
								AND orders.order_id = order_supplier.order_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND product.product_id = order_item.product_id
								AND account_type.account_type_id = account.account_type_id
								AND account.account_id = ".$accounts_payable_id."
								AND orders.order_approval_status = 7 
								AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id

							UNION ALL

							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								`orders`.`reference_number`  AS `transactionCode`,
								'' AS `patient_id`,
							  `orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`orders`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Credit' AS `transactionName`,
								CONCAT('Credit note of ',' ',`orders`.`reference_number`) AS `transactionDescription`,
								'0' AS `dr_amount`,
								SUM(`order_supplier`.`less_vat`) AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Income' AS `transactionCategory`,
								'Supplies Credit Note' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`

							FROM
								`order_supplier`,orders,order_item,product,account,account_type,creditor
								
								WHERE orders.is_store = 3
								AND orders.order_id = order_supplier.order_id
								AND account.account_id = orders.account_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND product.product_id = order_item.product_id
								AND account_type.account_type_id = account.account_type_id
								 AND orders.supplier_id = creditor.creditor_id and orders.order_approval_status = 7 AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								 AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id

							UNION ALL

							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								`orders`.`reference_number`  AS `transactionCode`,
								'' AS `patient_id`,
							  `orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Credit' AS `transactionName`,
								CONCAT('Credit note of ',' ',`orders`.`supplier_invoice_number`) AS `transactionDescription`,
								SUM(`order_supplier`.`less_vat`)  AS `dr_amount`,
								0 AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Income' AS `transactionCategory`,
								'Supplies Credit Note' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`

							FROM
								`order_supplier`,orders,account,order_item,product,account_type,creditor
											

								WHERE orders.is_store = 3
								AND account.account_id = ".$accounts_payable_id."
								AND account_type.account_type_id = account.account_type_id
								AND product.product_id = order_item.product_id
								AND orders.order_id = order_supplier.order_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND orders.supplier_id = creditor.creditor_id and orders.order_approval_status = 7 AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id



							UNION ALL

							 SELECT
								`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
								`creditor_payment`.`creditor_payment_id` AS `referenceId`,
								`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
								`creditor_payment`.`reference_number` AS `referenceCode`,
								`creditor_payment`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`creditor_payment`.`creditor_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								creditor_payment.account_from_id AS `accountId`,
								`account`.`account_name` AS `accountName`,
								`creditor_payment_item`.`description` AS `transactionName`,
								CONCAT('Payment to ', creditor.creditor_name ,'for ',`orders`.`supplier_invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
								0 AS `dr_amount`,
								`creditor_payment_item`.`amount_paid` AS `cr_amount`,
								`creditor_payment`.`transaction_date` AS `transactionDate`,
								`creditor_payment`.`created` AS `createdAt`,
								`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
								'creditor_payment.branch_id' AS `branch_id`,
								'Creditor Payment' AS `transactionCategory`,
								'Creditors Invoices Payments' AS `transactionClassification`,
								'creditor_payment' AS `transactionTable`,
								'creditor_payment_item' AS `referenceTable`
								FROM
								(
									(
										(
											`creditor_payment_item`,creditor_payment,orders,creditor
											
										)
										JOIN account ON(
											(
												account.account_id = creditor_payment.account_from_id
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
									
								)
							WHERE creditor_payment_item.invoice_type = 1 
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id  
							AND creditor_payment.creditor_payment_status = 1 
							AND orders.order_id = creditor_payment_item.creditor_invoice_id
							AND orders.supplier_id = creditor.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date

							UNION ALL

							SELECT
								`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
								`creditor_payment`.`creditor_payment_id` AS `referenceId`,
								`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
								`creditor_payment`.`reference_number` AS `referenceCode`,
								`creditor_payment`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`creditor_payment`.`creditor_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								`account`.`account_name` AS `accountName`,
								`creditor_payment_item`.`description` AS `transactionName`,
								CONCAT('Payment to ', creditor.creditor_name ,'for ',`orders`.`supplier_invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
								`creditor_payment_item`.`amount_paid` AS `dr_amount`,
								0 AS `cr_amount`,
								`creditor_payment`.`transaction_date` AS `transactionDate`,
								`creditor_payment`.`created` AS `createdAt`,
								`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
								'creditor_payment.branch_id' AS `branch_id`,
								'Creditor Payment' AS `transactionCategory`,
								'Creditors Invoices Payments' AS `transactionClassification`,
								'creditor_payment' AS `transactionTable`,
								'creditor_payment_item' AS `referenceTable`
								FROM
								(
									(
										(
											`creditor_payment_item`,creditor_payment,orders,creditor
											
										)
										JOIN account ON(
											(
												account.account_id = ".$accounts_payable_id."
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
									
								)
							WHERE creditor_payment_item.invoice_type = 1 
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id  
							AND creditor_payment.creditor_payment_status = 1 
							AND orders.order_id = creditor_payment_item.creditor_invoice_id
							AND orders.supplier_id = creditor.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date
								

					

						UNION ALL






						SELECT

							bank_reconcilliation.recon_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.account_name AS `accountName`,
							'' AS `transactionName`,
							CONCAT('Bank Charges ', account.account_name ) AS `transactionDescription`,
							0 AS `dr_amount`,
							SUM(bank_reconcilliation.service_charged) AS `cr_amount`,
							bank_reconcilliation.charged_date AS `transactionDate`,
							bank_reconcilliation.charged_date AS `createdAt`,
							bank_reconcilliation.recon_status AS `status`,
							2 AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							bank_reconcilliation,account,account_type
							WHERE bank_reconcilliation.recon_status = 2
							AND bank_reconcilliation.account_id = account.account_id
							AND account.account_type_id = account_type.account_type_id
						GROUP BY bank_reconcilliation.recon_id



						UNION ALL


						SELECT

							bank_reconcilliation.recon_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.account_name AS `accountName`,
							'' AS `transactionName`,
							CONCAT('Bank Charges ', account.account_name ) AS `transactionDescription`,
							SUM(bank_reconcilliation.service_charged) AS `dr_amount`,
							0 AS `cr_amount`,
							bank_reconcilliation.charged_date AS `transactionDate`,
							bank_reconcilliation.charged_date AS `createdAt`,
							bank_reconcilliation.recon_status AS `status`,
							2 AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							bank_reconcilliation,account,account_type
							WHERE bank_reconcilliation.recon_status = 2
							AND bank_reconcilliation.expense_account_id = account.account_id
							AND account.account_type_id = account_type.account_type_id
						GROUP BY bank_reconcilliation.recon_id

						UNION ALL 

						SELECT


							`journal_entry`.`journal_entry_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							journal_entry.document_number AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							journal_entry.account_from_id AS `accountId`,
							account.account_name AS `accountName`,
							'Journal Entry ' AS `transactionName`,
							CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
							`journal_entry`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`journal_entry`.`payment_date` AS `transactionDate`,
							`journal_entry`.`payment_date` AS `createdAt`,
							`journal_entry`.`journal_entry_status` AS `status`,
							2 AS `branch_id`,
							'Journal Credit' AS `transactionCategory`,
							'Journal' AS `transactionClassification`,
							'journal_entry' AS `transactionTable`,
							'account' AS `referenceTable`
						FROM
						journal_entry,account,account_type
						
						WHERE journal_entry.account_from_id  = account.account_id AND journal_entry.journal_entry_deleted = 0 
						AND account_type.account_type_id = account.account_type_id



						UNION ALL 


						SELECT
							`journal_entry`.`journal_entry_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							journal_entry.document_number AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							journal_entry.account_to_id AS `accountId`,
							account.account_name AS `accountName`,
							'Journal Entry ' AS `transactionName`,
							CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
							0 AS `dr_amount`,
							`journal_entry`.`amount_paid` AS `cr_amount`,
							`journal_entry`.`payment_date` AS `transactionDate`,
							`journal_entry`.`payment_date` AS `createdAt`,
							`journal_entry`.`journal_entry_status` AS `status`,
							2 AS `branch_id`,
							'Journal Debit' AS `transactionCategory`,
							'Journal' AS `transactionClassification`,
							'journal_entry' AS `transactionTable`,
							'account' AS `referenceTable`
						FROM
						journal_entry,account,account_type
						
						WHERE journal_entry.account_to_id  = account.account_id AND journal_entry.journal_entry_deleted = 0 
						AND account_type.account_type_id = account.account_type_id

						


						UNION ALL


						SELECT
								visit_invoice.visit_invoice_id AS transactionId,
								visit_charge.visit_charge_id  AS referenceId,
								'' AS `payingFor`,
								visit_invoice.visit_invoice_number AS referenceCode,
								'' AS transactionCode,
								visit_invoice.patient_id AS patientId,
								'' AS recepientId,

								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
							    account.account_id AS `accountId`,
							    `account`.`account_name` AS `accountName`,
							   	service_charge.service_charge_name AS transactionName,
								CONCAT( '<strong>Invoice </strong>  Invoice Number :  ', visit_invoice.visit_invoice_number,' <strong>Bill to:</strong>  ', visit_type.visit_type_name ) AS transactionDescription,
								0 AS dr_amount,
								visit_charge.visit_charge_amount*visit_charge.visit_charge_units AS cr_amount,
							    visit_invoice.created AS `transactionDate`,
							    visit_invoice.created AS `createdAt`,
							    1 AS `status`,
							    2 AS `branch_id`,
							    service.service_name AS `transactionCategory`,
							    'PATIENT INVOICE' AS `transactionClassification`,
							    'visit_invoice' AS `transactionTable`,
							    'visit_charge' AS `referenceTable`


						FROM
									visit_charge,visit_invoice,service_charge,visit_type,service,account,account_type,visit
									WHERE visit_charge.charged = 1  
									AND visit_invoice.visit_invoice_delete = 0
									AND visit_charge.visit_charge_delete = 0 
									AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id
									AND service_charge.service_charge_id = visit_charge.service_charge_id
									AND visit_type.visit_type_id = visit_invoice.bill_to
									AND service.service_id = service_charge.service_id
									AND account.account_id = service.account_id
									AND visit.visit_id = visit_charge.visit_id
									AND visit.visit_delete = 0
									AND account_type.account_type_id = account.account_type_id


						UNION ALL

						SELECT
							visit_credit_note.visit_credit_note_id AS transactionId,
							visit_invoice.visit_invoice_id AS referenceId,
							'' AS `payingFor`,
							visit_credit_note.visit_cr_note_number AS referenceCode,
							visit_credit_note.visit_cr_note_number AS transactionCode,
							visit_invoice.patient_id AS patient_id,
							'' AS recepientId,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
						    account.account_id AS `accountId`,
						    `account`.`account_name` AS `accountName`,
						    service_charge.service_charge_name AS transactionName,
							CONCAT('<strong>Credit Note for </strong>  Invoice Number: ', visit_invoice.visit_invoice_number,'<strong>Credit Note No:</strong> ',visit_credit_note.visit_cr_note_number) AS transactionDescription,
						   
							visit_credit_note_item.visit_cr_note_amount*visit_credit_note_item.visit_cr_note_units AS dr_amount,
							0 AS cr_amount,
						    visit_invoice.created AS `transactionDate`,
						    visit_credit_note.created AS `createdAt`,
						    1 AS `status`,
						    2 AS `branch_id`,
						    CONCAT(service.service_name,' Credit note') AS `transactionCategory`,
						    'INCOME CREDIT NOTE' AS `transactionClassification`,
						    'visit_invoice' AS `transactionTable`,
						    'visit_credit_note' AS `referenceTable`
						FROM
							visit_credit_note_item,visit_credit_note,visit_invoice,service_charge,service,account,account_type
							WHERE visit_credit_note.visit_cr_note_delete = 0 AND visit_credit_note_item.visit_cr_note_item_delete = 0 

							AND visit_invoice.visit_invoice_id = visit_credit_note.visit_invoice_id 
							AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id 
							AND service_charge.service_charge_id = visit_credit_note_item.service_charge_id
							AND service.service_id = service_charge.service_id
							AND account.account_id = service.account_id
							AND account_type.account_type_id = account.account_type_id



						UNION ALL



						SELECT
								visit_invoice.visit_invoice_id AS transactionId,
								visit_charge.visit_charge_id  AS referenceId,
								'' AS `payingFor`,
								visit_invoice.visit_invoice_number AS referenceCode,
								'' AS transactionCode,
								visit_invoice.patient_id AS patientId,
								'' AS recepientId,

								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
							    account.account_id AS `accountId`,
							    `account`.`account_name` AS `accountName`,
							   	service_charge.service_charge_name AS transactionName,
								CONCAT( '<strong>Invoice </strong>  Invoice Number :  ', visit_invoice.visit_invoice_number,' <strong>Bill to:</strong>  ', visit_type.visit_type_name ) AS transactionDescription,
								visit_charge.visit_charge_amount*visit_charge.visit_charge_units AS dr_amount,
								0 AS cr_amount,
							    visit_invoice.created AS `transactionDate`,
							    visit_invoice.created AS `createdAt`,
							    1 AS `status`,
							    2 AS `branch_id`,
							    service.service_name AS `transactionCategory`,
							    'PATIENT INVOICE' AS `transactionClassification`,
							    'visit_invoice' AS `transactionTable`,
							    'visit_charge' AS `referenceTable`


						FROM
									visit_charge,visit_invoice,service_charge,visit_type,service,account,account_type,visit
									WHERE visit_charge.charged = 1  
									AND visit_invoice.visit_invoice_delete = 0
									AND visit_charge.visit_charge_delete = 0 
									AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id
									AND service_charge.service_charge_id = visit_charge.service_charge_id
									AND visit_type.visit_type_id = visit_invoice.bill_to
									AND service.service_id = service_charge.service_id
									AND visit.visit_id = visit_charge.visit_id
									AND visit.visit_delete = 0
									AND account.account_id = ".$accounts_receivable_id."
									AND account_type.account_type_id = account.account_type_id


						UNION ALL

						SELECT
							visit_credit_note.visit_credit_note_id AS transactionId,
							visit_invoice.visit_invoice_id AS referenceId,
							'' AS `payingFor`,
							visit_credit_note.visit_cr_note_number AS referenceCode,
							visit_credit_note.visit_cr_note_number AS transactionCode,
							visit_invoice.patient_id AS patient_id,
							'' AS recepientId,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
						    account.account_id AS `accountId`,
						    `account`.`account_name` AS `accountName`,
						    service_charge.service_charge_name AS transactionName,
							CONCAT('<strong>Credit Note for </strong>  Invoice Number: ', visit_invoice.visit_invoice_number,'<strong>Credit Note No:</strong> ',visit_credit_note.visit_cr_note_number) AS transactionDescription,
						   
							0 AS dr_amount,
							visit_credit_note_item.visit_cr_note_amount*visit_credit_note_item.visit_cr_note_units  AS cr_amount,
						    visit_invoice.created AS `transactionDate`,
						    visit_credit_note.created AS `createdAt`,
						    1 AS `status`,
						    2 AS `branch_id`,
						    'INCOME CREDIT NOTE' AS `transactionCategory`,
						    'INCOME CREDIT NOTE' AS `transactionClassification`,
						    'visit_invoice' AS `transactionTable`,
						    'visit_credit_note' AS `referenceTable`
						FROM
							visit_credit_note_item,visit_credit_note,visit_invoice,service_charge,service,account,account_type
							WHERE visit_credit_note.visit_cr_note_delete = 0 AND visit_credit_note_item.visit_cr_note_item_delete = 0 

							AND visit_invoice.visit_invoice_id = visit_credit_note.visit_invoice_id 
							AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id 
							AND service_charge.service_charge_id = visit_credit_note_item.service_charge_id
							AND service.service_id = service_charge.service_id
							AND account.account_id = ".$accounts_receivable_id."
							AND account_type.account_type_id = account.account_type_id


							UNION ALL


							SELECT

								payments.payment_id AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								payments.confirm_number AS `referenceCode`,
								payments.confirm_number AS `transactionCode`,
								payments.patient_id AS `patient_id`,
							  	'' AS `recepientId`,
								account_type.account_type_id AS `accountParentId`,
								account_type.account_type_name AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number) AS `transactionDescription`,
								SUM(payment_item.payment_item_amount) AS `dr_amount`,
								0 AS `cr_amount`,
								payments.payment_date AS `transactionDate`,
								payments.payment_date AS `createdAt`,
								`payments`.`cancel` AS `status`,
								2 AS `branch_id`,
								'Patient Income' AS `transactionCategory`,
								'Payments' AS `transactionClassification`,
								'account_payments' AS `transactionTable`,
								'' AS `referenceTable`
							FROM
								payments,payment_item,payment_method,account,visit_invoice,account_type
								WHERE payments.cancel = 0 AND payments.payment_type = 1 AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND payment_item.invoice_type = 1
								AND payments.payment_id = payment_item.payment_id 
								AND payments.payment_method_id = payment_method.payment_method_id
								AND payment_method.account_id = account.account_id
								AND account.account_type_id = account_type.account_type_id
							GROUP BY payment_item.payment_id


							UNION ALL 


							SELECT

								payments.payment_id AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								payments.confirm_number AS `referenceCode`,
								payments.confirm_number AS `transactionCode`,
								payments.patient_id AS `patient_id`,
							  	'' AS `recepientId`,
								account_type.account_type_id AS `accountParentId`,
								account_type.account_type_name AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								account.parent_account AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Payment On Account') AS `transactionDescription`,
								SUM(payment_item.payment_item_amount) AS `dr_amount`,
								0 AS `cr_amount`,
								payments.payment_date AS `transactionDate`,
								payments.payment_date AS `createdAt`,
								`payments`.`cancel` AS `status`,
								2 AS `branch_id`,
								'Patient Income' AS `transactionCategory`,
								'Payments' AS `transactionClassification`,
								'account_payments' AS `transactionTable`,
								'' AS `referenceTable`
							FROM
								payments,payment_item,payment_method,account,account_type
								WHERE payments.cancel = 0 AND payments.payment_type = 1 AND payment_item.invoice_type = 3
								AND payments.payment_id = payment_item.payment_id 
								AND payments.payment_method_id = payment_method.payment_method_id
								AND payment_method.account_id = account.account_id
								AND account.account_type_id = account_type.account_type_id
							GROUP BY payment_item.payment_id


							UNION ALL

						SELECT

							payments.payment_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							payments.confirm_number AS `referenceCode`,
							payments.confirm_number AS `transactionCode`,
							payments.patient_id AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.account_name AS `accountName`,
							'' AS `transactionName`,
							CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number) AS `transactionDescription`,
							0 AS `dr_amount`,
							SUM(payment_item.payment_item_amount) AS `cr_amount`,
							payments.payment_date AS `transactionDate`,
							payments.payment_date AS `createdAt`,
							`payments`.`cancel` AS `status`,
							2 AS `branch_id`,
							'Patient Income' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							payments,payment_item,payment_method,account,visit_invoice,account_type
							WHERE payments.cancel = 0 AND payments.payment_type = 1 AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND payment_item.invoice_type = 1
							AND payments.payment_id = payment_item.payment_id 
							AND payments.payment_method_id = payment_method.payment_method_id
							AND account.account_id = ".$accounts_receivable_id."
							AND account.account_type_id = account_type.account_type_id
						GROUP BY payment_item.payment_id


							UNION ALL 


							SELECT

								payments.payment_id AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								payments.confirm_number AS `referenceCode`,
								payments.confirm_number AS `transactionCode`,
								payments.patient_id AS `patient_id`,
							  	'' AS `recepientId`,
								account_type.account_type_id AS `accountParentId`,
								account_type.account_type_name AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								account.parent_account AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Payment On Account') AS `transactionDescription`,
								0 AS `dr_amount`,
								SUM(payment_item.payment_item_amount) AS `cr_amount`,
								payments.payment_date AS `transactionDate`,
								payments.payment_date AS `createdAt`,
								`payments`.`cancel` AS `status`,
								2 AS `branch_id`,
								'Patient Income' AS `transactionCategory`,
								'Payments' AS `transactionClassification`,
								'account_payments' AS `transactionTable`,
								'' AS `referenceTable`
							FROM
								payments,payment_item,payment_method,account,account_type
								WHERE payments.cancel = 0 AND payments.payment_type = 1 AND payment_item.invoice_type = 3
								AND payments.payment_id = payment_item.payment_id 
								AND payments.payment_method_id = payment_method.payment_method_id
								AND account.account_id = ".$accounts_receivable_id."
								AND account.account_type_id = account_type.account_type_id
							GROUP BY payment_item.payment_id

							UNION ALL

								SELECT
									`provider`.`provider_id` AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
								    `provider`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
					  				`account_type`.`account_type_name` AS `accountsclassfication`,
									account.account_id AS `accountId`,
									account.account_name AS `accountName`,
									'' AS `transactionName`,
									CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
									0 AS `dr_amount`,
									`provider`.`opening_balance` AS `cr_amount`,
									`provider`.`start_date` AS `transactionDate`,
									`provider`.`start_date` AS `createdAt`,
									`provider`.`provider_status` AS `status`,
									2 AS `branch_id`,
									'Expense' AS `transactionCategory`,
									'provider Opening Balance' AS `transactionClassification`,
									'' AS `transactionTable`,
									'provider' AS `referenceTable`
								FROM
								provider,account,account_type
								WHERE  debit_id = 1 AND account.account_id = ".$provider_opening_balance_id." AND account.account_type_id = account_type.account_type_id

								UNION ALL


								SELECT
									`provider`.`provider_id` AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
								    `provider`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
					  				`account_type`.`account_type_name` AS `accountsclassfication`,
									account.account_id AS `accountId`,
									account.account_name AS `accountName`,
									'' AS `transactionName`,
									CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
									`provider`.`opening_balance` AS `dr_amount`,
									0 AS `cr_amount`,
									`provider`.`start_date` AS `transactionDate`,
									`provider`.`start_date` AS `createdAt`,
									`provider`.`provider_status` AS `status`,
									2 AS `branch_id`,
									'Expense' AS `transactionCategory`,
									'provider Opening Balance' AS `transactionClassification`,
									'' AS `transactionTable`,
									'provider' AS `referenceTable`
								FROM
								provider,account,account_type
								WHERE  debit_id = 1 AND account.account_id = ".$providers_liability_id." AND account.account_type_id = account_type.account_type_id


								UNION ALL


								SELECT
									`provider`.`provider_id` AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
								    `provider`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
					  				`account_type`.`account_type_name` AS `accountsclassfication`,
									account.account_id AS `accountId`,
									account.account_name AS `accountName`,
									'' AS `transactionName`,
									CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
									`provider`.`opening_balance` AS `dr_amount`,
									0 AS `cr_amount`,
									`provider`.`start_date` AS `transactionDate`,
									`provider`.`start_date` AS `createdAt`,
									`provider`.`provider_status` AS `status`,
									2 AS `branch_id`,
									'Expense' AS `transactionCategory`,
									'provider Opening Balance' AS `transactionClassification`,
									'' AS `transactionTable`,
									'provider' AS `referenceTable`
								FROM
								provider,account,account_type
								WHERE  debit_id = 2 AND account.account_id = ".$provider_opening_balance_id." AND account.account_type_id = account_type.account_type_id



								UNION ALL


								SELECT
									`provider`.`provider_id` AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
								    `provider`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
					  				`account_type`.`account_type_name` AS `accountsclassfication`,
									account.account_id AS `accountId`,
									account.account_name AS `accountName`,
									'' AS `transactionName`,
									CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
									0 AS `dr_amount`,
									`provider`.`opening_balance` AS `cr_amount`,
									`provider`.`start_date` AS `transactionDate`,
									`provider`.`start_date` AS `createdAt`,
									`provider`.`provider_status` AS `status`,
									2 AS `branch_id`,
									'Expense' AS `transactionCategory`,
									'provider Opening Balance' AS `transactionClassification`,
									'' AS `transactionTable`,
									'provider' AS `referenceTable`
								FROM
								provider,account,account_type
								WHERE  debit_id = 2 AND account.account_id = ".$providers_liability_id." AND account.account_type_id = account_type.account_type_id

							  UNION ALL

								SELECT
									`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
									`provider_invoice`.`provider_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`provider_invoice`.`invoice_number` AS `referenceCode`,
									`provider_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  `provider_invoice`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`provider_invoice_item`.`account_to_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionName`,
									CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
									`provider_invoice_item`.`total_amount` AS `dr_amount`,
									'0' AS `cr_amount`,
									`provider_invoice`.`transaction_date` AS `transactionDate`,
									`provider_invoice`.`created` AS `createdAt`,
									`provider_invoice`.`transaction_date` AS `referenceDate`,
									`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
									'Expense' AS `transactionCategory`,
									'providers Invoices' AS `transactionClassification`,
									'provider_invoice_item' AS `transactionTable`,
									'provider_invoice' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_invoice_item`,provider_invoice,provider
												
											)
											JOIN account ON(
												(
													account.account_id = provider_invoice_item.account_to_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
									)
								WHERE 
								provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
								AND provider_invoice.provider_invoice_status = 1
								AND provider.provider_id = provider_invoice.provider_id 
								AND provider_invoice.transaction_date >= provider.start_date

							UNION ALL

							SELECT
									`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
									`provider_invoice`.`provider_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`provider_invoice`.`invoice_number` AS `referenceCode`,
									`provider_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  `provider_invoice`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionName`,
									CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
									0 AS `dr_amount`,
									`provider_invoice_item`.`total_amount` AS `cr_amount`,
									`provider_invoice`.`transaction_date` AS `transactionDate`,
									`provider_invoice`.`created` AS `createdAt`,
									`provider_invoice`.`transaction_date` AS `referenceDate`,
									`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
									'Provider Liability' AS `transactionCategory`,
									'providers Invoices' AS `transactionClassification`,
									'provider_invoice_item' AS `transactionTable`,
									'provider_invoice' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_invoice_item`,provider_invoice,provider
												
											)
											JOIN account ON(
												(
													account.account_id = ".$providers_liability_id."
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
									)
								WHERE 
								provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
								AND provider_invoice.provider_invoice_status = 1
								AND provider.provider_id = provider_invoice.provider_id 
								AND provider_invoice.transaction_date >= provider.start_date


								UNION ALL

								SELECT
									`provider_credit_note_item`.`provider_credit_note_item_id` AS `transactionId`,
									`provider_credit_note`.`provider_credit_note_id` AS `referenceId`,
									`provider_credit_note_item`.`provider_invoice_id` AS `payingFor`,
									`provider_credit_note`.`invoice_number` AS `referenceCode`,
									`provider_credit_note`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`provider_credit_note`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`provider_credit_note`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionName`,
									CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionDescription`,
									0 AS `dr_amount`,
									`provider_credit_note_item`.`credit_note_amount` AS `cr_amount`,
									`provider_credit_note`.`transaction_date` AS `transactionDate`,
									`provider_credit_note`.`created` AS `createdAt`,
									`provider_invoice`.`transaction_date` AS `referenceDate`,
									`provider_credit_note_item`.`provider_credit_note_item_status` AS `status`,
									'Provider Credit Note' AS `transactionCategory`,
									'Providers Credit Notes' AS `transactionClassification`,
									'provider_credit_note' AS `transactionTable`,
									'provider_credit_note_item' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_credit_note_item`,provider_credit_note,provider_invoice,provider
												
											)
											JOIN account ON(
												(
													account.account_id = provider_credit_note.account_from_id
												)
											)
											
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
									)
								WHERE 
									provider_credit_note.provider_credit_note_id = provider_credit_note_item.provider_credit_note_id 
									AND provider_credit_note.provider_credit_note_status = 1
									AND provider_invoice.provider_invoice_id = provider_credit_note_item.provider_invoice_id
									AND provider_invoice.provider_invoice_status = 1
									AND provider.provider_id = provider_invoice.provider_id 
									AND provider_invoice.transaction_date >= provider.start_date

								UNION ALL

								SELECT
									`provider_credit_note_item`.`provider_credit_note_item_id` AS `transactionId`,
									`provider_credit_note`.`provider_credit_note_id` AS `referenceId`,
									`provider_credit_note_item`.`provider_invoice_id` AS `payingFor`,
									`provider_credit_note`.`invoice_number` AS `referenceCode`,
									`provider_credit_note`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`provider_credit_note`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionName`,
									CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionDescription`,
									`provider_credit_note_item`.`credit_note_amount` AS `dr_amount`,
									0 AS `cr_amount`,
									`provider_credit_note`.`transaction_date` AS `transactionDate`,
									`provider_credit_note`.`created` AS `createdAt`,
									`provider_invoice`.`transaction_date` AS `referenceDate`,
									`provider_credit_note_item`.`provider_credit_note_item_status` AS `status`,
									'Provider Credit Note' AS `transactionCategory`,
									'Providers Credit Notes' AS `transactionClassification`,
									'provider_credit_note' AS `transactionTable`,
									'provider_credit_note_item' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_credit_note_item`,provider_credit_note,provider_invoice,provider
												
											)
											JOIN account ON(
												(
													account.account_id = ".$providers_liability_id."
												)
											)
											
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
									)
								WHERE 
									provider_credit_note.provider_credit_note_id = provider_credit_note_item.provider_credit_note_id 
									AND provider_credit_note.provider_credit_note_status = 1
									AND provider_invoice.provider_invoice_id = provider_credit_note_item.provider_invoice_id
									AND provider_invoice.provider_invoice_status = 1
									AND provider.provider_id = provider_invoice.provider_id 
									AND provider_invoice.transaction_date >= provider.start_date

								UNION ALL

								SELECT
										`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
										`provider_payment`.`provider_payment_id` AS `referenceId`,
										`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
										`provider_payment`.`reference_number` AS `referenceCode`,
										`provider_payment`.`document_number` AS `transactionCode`,
										'' AS `patient_id`,
									  	`provider_payment`.`provider_id` AS `recepientId`,
										`account`.`parent_account` AS `accountParentId`,
										`account_type`.`account_type_name` AS `accountsclassfication`,
										`provider_payment`.`account_from_id` AS `accountId`,
										`account`.`account_name` AS `accountName`,
										`provider_payment_item`.`description` AS `transactionName`,
										CONCAT('Provider payment for invoice of ',' ',`provider_invoice`.`invoice_number`)  AS `transactionDescription`,
										0 AS `dr_amount`,
										`provider_payment_item`.`amount_paid` AS `cr_amount`,
										`provider_payment`.`transaction_date` AS `transactionDate`,
										`provider_payment`.`created` AS `createdAt`,
										`provider_invoice`.`transaction_date` AS `referenceDate`,
										`provider_payment_item`.`provider_payment_item_status` AS `status`,
										'Expense Payment' AS `transactionCategory`,
										'providers Invoices Payments' AS `transactionClassification`,
										'provider_payment' AS `transactionTable`,
										'provider_payment_item' AS `referenceTable`
									FROM
										(
											(
												(
													`provider_payment_item`,provider_payment,provider_invoice,provider
													
												)
												JOIN account ON(
													(
														account.account_id = provider_payment.account_from_id
													)
												)
											)
											JOIN `account_type` ON(
												(
													account_type.account_type_id = account.account_type_id
												)
											)
											
										)
										WHERE provider_payment_item.invoice_type = 0 
										AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
										AND provider_payment.provider_payment_status = 1
										AND provider_invoice.provider_invoice_id = provider_payment_item.provider_invoice_id 
										AND provider_invoice.provider_invoice_status = 1  
										AND provider.provider_id = provider_invoice.provider_id AND provider_invoice.transaction_date >= provider.start_date

								UNION ALL

								SELECT
										`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
										`provider_payment`.`provider_payment_id` AS `referenceId`,
										`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
										`provider_payment`.`reference_number` AS `referenceCode`,
										`provider_payment`.`document_number` AS `transactionCode`,
										'' AS `patient_id`,
									  	`provider_payment`.`provider_id` AS `recepientId`,
										`account`.`parent_account` AS `accountParentId`,
										`account_type`.`account_type_name` AS `accountsclassfication`,
										`account`.`account_id` AS `accountId`,
										`account`.`account_name` AS `accountName`,
										`provider_payment_item`.`description` AS `transactionName`,
										CONCAT('Provider payment for invoice of ',' ',`provider_invoice`.`invoice_number`)  AS `transactionDescription`,
										`provider_payment_item`.`amount_paid` AS `dr_amount`,
										0 AS `cr_amount`,
										`provider_payment`.`transaction_date` AS `transactionDate`,
										`provider_payment`.`created` AS `createdAt`,
										`provider_invoice`.`transaction_date` AS `referenceDate`,
										`provider_payment_item`.`provider_payment_item_status` AS `status`,
										'Expense Payment' AS `transactionCategory`,
										'providers Invoices Payments' AS `transactionClassification`,
										'provider_payment' AS `transactionTable`,
										'provider_payment_item' AS `referenceTable`
									FROM
										(
											(
												(
													`provider_payment_item`,provider_payment,provider_invoice,provider
													
												)
												JOIN account ON(
													(
														account.account_id = ".$providers_liability_id."
													)
												)
											)
											JOIN `account_type` ON(
												(
													account_type.account_type_id = account.account_type_id
												)
											)
											
										)
										WHERE provider_payment_item.invoice_type = 0 
										AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
										AND provider_payment.provider_payment_status = 1
										AND provider_invoice.provider_invoice_id = provider_payment_item.provider_invoice_id 
										AND provider_invoice.provider_invoice_status = 1  
										AND provider.provider_id = provider_invoice.provider_id AND provider_invoice.transaction_date >= provider.start_date

									
								UNION ALL

								SELECT
										`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
										`provider_payment`.`provider_payment_id` AS `referenceId`,
										`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
										`provider_payment`.`reference_number` AS `referenceCode`,
										`provider_payment`.`document_number` AS `transactionCode`,
										'' AS `patient_id`,
									  `provider_payment`.`provider_id` AS `recepientId`,
										`account`.`parent_account` AS `accountParentId`,
										`account_type`.`account_type_name` AS `accountsclassfication`,
										`provider_payment`.`account_from_id` AS `accountId`,
										`account`.`account_name` AS `accountName`,
										`provider_payment_item`.`description` AS `transactionName`,
										CONCAT('Payment of opening balance')  AS `transactionDescription`,
										0 AS `dr_amount`,
										`provider_payment_item`.`amount_paid` AS `cr_amount`,
										`provider_payment`.`transaction_date` AS `transactionDate`,
										`provider_payment`.`created` AS `createdAt`,
										`provider`.`start_date` AS `referenceDate`,
										`provider_payment_item`.`provider_payment_item_status` AS `status`,
										'Expense Payment' AS `transactionCategory`,
										'providers Invoices Payments' AS `transactionClassification`,
										'provider_payment' AS `transactionTable`,
										'provider_payment_item' AS `referenceTable`
									FROM
										(
											(
												(
													`provider_payment_item`,provider_payment,provider
													
												)
												JOIN account ON(
													(
														account.account_id = provider_payment.account_from_id
													)
												)
											)
											JOIN `account_type` ON(
												(
													account_type.account_type_id = account.account_type_id
												)
											)
											
										)
										WHERE provider_payment_item.invoice_type = 2 
										AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
										AND provider_payment.provider_payment_status = 1
										AND provider.provider_id = provider_payment_item.provider_id
										AND provider_payment.transaction_date >= provider.start_date
									UNION ALL

								SELECT
										`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
										`provider_payment`.`provider_payment_id` AS `referenceId`,
										`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
										`provider_payment`.`reference_number` AS `referenceCode`,
										`provider_payment`.`document_number` AS `transactionCode`,
										'' AS `patient_id`,
									  `provider_payment`.`provider_id` AS `recepientId`,
										`account`.`parent_account` AS `accountParentId`,
										`account_type`.`account_type_name` AS `accountsclassfication`,
										`account`.`account_id` AS `accountId`,
										`account`.`account_name` AS `accountName`,
										`provider_payment_item`.`description` AS `transactionName`,
										CONCAT('Payment of opening balance')  AS `transactionDescription`,
										`provider_payment_item`.`amount_paid` AS `dr_amount`,
										0 AS `cr_amount`,
										`provider_payment`.`transaction_date` AS `transactionDate`,
										`provider_payment`.`created` AS `createdAt`,
										`provider`.`start_date` AS `referenceDate`,
										`provider_payment_item`.`provider_payment_item_status` AS `status`,
										'Expense Payment' AS `transactionCategory`,
										'providers Invoices Payments' AS `transactionClassification`,
										'provider_payment' AS `transactionTable`,
										'provider_payment_item' AS `referenceTable`
									FROM
										(
											(
												(
													`provider_payment_item`,provider_payment,provider
													
												)
												JOIN account ON(
													(
														account.account_id = ".$providers_liability_id."
													)
												)
											)
											JOIN `account_type` ON(
												(
													account_type.account_type_id = account.account_type_id
												)
											)
											
										)
										WHERE provider_payment_item.invoice_type = 2 
										AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
										AND provider_payment.provider_payment_status = 1
										AND provider.provider_id = provider_payment_item.provider_id
										AND provider_payment.transaction_date >= provider.start_date

									UNION ALL

									SELECT
											`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
											`provider_payment`.`provider_payment_id` AS `referenceId`,
											`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
											`provider_payment`.`reference_number` AS `referenceCode`,
											`provider_payment`.`document_number` AS `transactionCode`,
											'' AS `patient_id`,
										  `provider_payment`.`provider_id` AS `recepientId`,
											`account`.`parent_account` AS `accountParentId`,
											`account_type`.`account_type_name` AS `accountsclassfication`,
											`provider_payment`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											`provider_payment_item`.`description` AS `transactionName`,
											CONCAT('Payment on account')  AS `transactionDescription`,
											0 AS `dr_amount`,
											`provider_payment_item`.`amount_paid` AS `cr_amount`,
											`provider_payment`.`transaction_date` AS `transactionDate`,
											`provider_payment`.`created` AS `createdAt`,
											`provider_payment`.`created` AS `referenceDate`,
											`provider_payment_item`.`provider_payment_item_status` AS `status`,
											'Expense Payment' AS `transactionCategory`,
											'providers Invoices Payments' AS `transactionClassification`,
											'provider_payment' AS `transactionTable`,
											'provider_payment_item' AS `referenceTable`
										FROM
											(
												(
													(
														`provider_payment_item`,provider_payment,provider
														
													)
													JOIN account ON(
														(
															account.account_id = provider_payment.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
											WHERE provider_payment_item.invoice_type = 3 AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
												AND provider_payment.provider_payment_status = 1 
												AND provider.provider_id = provider_payment.provider_id AND provider_payment.transaction_date >= provider.start_date
									UNION ALL

									SELECT
											`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
											`provider_payment`.`provider_payment_id` AS `referenceId`,
											`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
											`provider_payment`.`reference_number` AS `referenceCode`,
											`provider_payment`.`document_number` AS `transactionCode`,
											'' AS `patient_id`,
										  `provider_payment`.`provider_id` AS `recepientId`,
											`account`.`parent_account` AS `accountParentId`,
											`account_type`.`account_type_name` AS `accountsclassfication`,
											`account`.`account_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											`provider_payment_item`.`description` AS `transactionName`,
											CONCAT('Payment on account')  AS `transactionDescription`,
											`provider_payment_item`.`amount_paid` AS `dr_amount`,
											0 AS `cr_amount`,
											`provider_payment`.`transaction_date` AS `transactionDate`,
											`provider_payment`.`created` AS `createdAt`,
											`provider_payment`.`created` AS `referenceDate`,
											`provider_payment_item`.`provider_payment_item_status` AS `status`,
											'Expense Payment' AS `transactionCategory`,
											'providers Invoices Payments' AS `transactionClassification`,
											'provider_payment' AS `transactionTable`,
											'provider_payment_item' AS `referenceTable`
										FROM
											(
												(
													(
														`provider_payment_item`,provider_payment,provider
														
													)
													JOIN account ON(
														(
															account.account_id = ".$providers_liability_id."
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
											WHERE provider_payment_item.invoice_type = 3 AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
												AND provider_payment.provider_payment_status = 1 
												AND provider.provider_id = provider_payment.provider_id AND provider_payment.transaction_date >= provider.start_date
									
								UNION ALL


								SELECT
									`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
									`provider_invoice`.`provider_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`provider_invoice`.`invoice_number` AS `referenceCode`,
									`provider_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  `provider_invoice`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionName`,
									CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
									0 AS `dr_amount`,
									`provider_invoice_item`.`vat_amount` AS `cr_amount`,
									`provider_invoice`.`transaction_date` AS `transactionDate`,
									`provider_invoice`.`created` AS `createdAt`,
									`provider_invoice`.`transaction_date` AS `referenceDate`,
									`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
									'Provider WHT' AS `transactionCategory`,
									'providers Invoices' AS `transactionClassification`,
									'provider_invoice_item' AS `transactionTable`,
									'provider_invoice' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_invoice_item`,provider_invoice,provider
												
											)
											JOIN account ON(
												(
													account.account_id = ".$providers_wht_id."
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
									)
								WHERE 
								provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
								AND provider_invoice.provider_invoice_status = 1
								AND provider.provider_id = provider_invoice.provider_id 
								AND provider_invoice.transaction_date >= provider.start_date
								AND provider_invoice_item.vat_amount > 0

						
								UNION ALL




								SELECT
									`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
									`provider_invoice`.`provider_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`provider_invoice`.`invoice_number` AS `referenceCode`,
									`provider_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  `provider_invoice`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionName`,
									CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
									`provider_invoice_item`.`vat_amount` AS `dr_amount`,
									0  AS `cr_amount`,
									`provider_invoice`.`transaction_date` AS `transactionDate`,
									`provider_invoice`.`created` AS `createdAt`,
									`provider_invoice`.`transaction_date` AS `referenceDate`,
									`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
									'Provider WHT' AS `transactionCategory`,
									'providers Invoices' AS `transactionClassification`,
									'provider_invoice_item' AS `transactionTable`,
									'provider_invoice' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_invoice_item`,provider_invoice,provider
												
											)
											JOIN account ON(
												(
													account.account_id = ".$providers_liability_id."
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
									)
								WHERE 
								provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
								AND provider_invoice.provider_invoice_status = 1
								AND provider.provider_id = provider_invoice.provider_id 
								AND provider_invoice.transaction_date >= provider.start_date
								AND provider_invoice_item.vat_amount > 0

								

							UNION ALL

								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payroll') AS `transactionName`,
									CONCAT('Payroll') AS `transactionDescription`,
									payroll_summary.total_payroll AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'Payroll' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 1
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id


								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('PAYE') AS `transactionName`,
									CONCAT('PAYE') AS `transactionDescription`,
									payroll_summary.paye AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 2
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id

								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NSSF') AS `transactionName`,
									CONCAT('NSSF') AS `transactionDescription`,
									payroll_summary.nssf AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 3
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id


								UNION ALL




								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NHIF') AS `transactionName`,
									CONCAT('NHIF') AS `transactionDescription`,
									payroll_summary.nhif AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'NHIF' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 4
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id



							UNION ALL


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payroll') AS `transactionName`,
									CONCAT('Payroll') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.total_payroll AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'Payroll' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 1
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id


								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('PAYE') AS `transactionName`,
									CONCAT('PAYE') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.paye AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 2
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id

								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NSSF') AS `transactionName`,
									CONCAT('NSSF') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.nssf AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 3
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id


								UNION ALL




								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NHIF') AS `transactionName`,
									CONCAT('NHIF') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.nhif AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'NHIF' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 4
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id

							UNION ALL

							SELECT
							    `payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
							    `payroll_payment`.`payroll_payment_id` AS `referenceId`,
							    `payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
							    `payroll_payment`.`reference_number` AS `referenceCode`,
							    `payroll_payment`.`document_number` AS `transactionCode`,
							    '' AS `patient_id`,
							    `statutory_accounts`.`statutory_account_id` AS `recepientId`,
							    `account`.`parent_account` AS `accountParentId`,
							    `account_type`.`account_type_name` AS `accountsclassfication`,
							    `payroll_payment`.`account_from_id` AS `accountId`,
							    `account`.`account_name` AS `accountName`,
							    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
							    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for))   AS `transactionDescription`,
							    0 AS `dr_amount`,
							    `payroll_payment_item`.`amount_paid` AS `cr_amount`,
							    `payroll_payment`.`transaction_date` AS `transactionDate`,
							    `payroll_payment`.`created` AS `createdAt`,
							    `payroll_payment_item`.`payroll_payment_item_status` AS `status`,
							    2 AS `branch_id`,
							    'Payroll Payment' AS `transactionCategory`,
							    'Creditors Invoices Payments' AS `transactionClassification`,
							    'payroll_payment' AS `transactionTable`,
							    'payroll_payment_item' AS `referenceTable`
							FROM
							    (
							      (
							        (
							          `payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts
							          
							        )
							        JOIN account ON(
							          (
							            account.account_id = payroll_payment.account_from_id
							          )
							        )
							      )
							      JOIN `account_type` ON(
							        (
							          account_type.account_type_id = account.account_type_id
							        )
							      )
							      
							    )
							    WHERE payroll_payment_item.invoice_type = 0 
							    AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
							    AND payroll_payment.payroll_payment_status = 1
							    AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_payment_id 
							    AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id

							UNION ALL

								SELECT
								    `payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
								    `payroll_payment`.`payroll_payment_id` AS `referenceId`,
								    `payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
								    `payroll_payment`.`reference_number` AS `referenceCode`,
								    `payroll_payment`.`document_number` AS `transactionCode`,
								    '' AS `patient_id`,
								    `statutory_accounts`.`statutory_account_id` AS `recepientId`,
								    `account`.`parent_account` AS `accountParentId`,
								    `account_type`.`account_type_name` AS `accountsclassfication`,
								    account.account_id AS `accountId`,
								    `account`.`account_name` AS `accountName`,
								    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
								    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for))   AS `transactionDescription`,
								    `payroll_payment_item`.`amount_paid` AS `dr_amount`,
								    0 AS `cr_amount`,
								    `payroll_payment`.`transaction_date` AS `transactionDate`,
								    `payroll_payment`.`created` AS `createdAt`,
								    `payroll_payment_item`.`payroll_payment_item_status` AS `status`,
								    2 AS `branch_id`,
								    'Payroll Payment' AS `transactionCategory`,
								    'Creditors Invoices Payments' AS `transactionClassification`,
								    'payroll_payment' AS `transactionTable`,
								    'payroll_payment_item' AS `referenceTable`
								  FROM
								    (
								      (
								        (
								          `payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts
								          
								        )
								        JOIN account ON(
								          (
								            account.account_id = ".$payroll_liability_id."
								          )
								        )
								      )
								      JOIN `account_type` ON(
								        (
								          account_type.account_type_id = account.account_type_id
								        )
								      )
								      
								    )
								    WHERE payroll_payment_item.invoice_type = 0 
								    AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
								    AND payroll_payment.payroll_payment_status = 1
								    AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_invoice_id 
								    AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id


								UNION ALL

								 SELECT
									asset_category.asset_category_id AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									assets_details.asset_serial_no AS `referenceCode`,
									assets_details.asset_serial_no AS `transactionCode`,
									assets_details.asset_id AS `patient_id`,
								  	assets_details.supplier_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
									CONCAT(assets_details.asset_name) AS `transactionName`,
									CONCAT(assets_details.asset_name) AS `transactionDescription`,
									(assets_details.asset_value) AS `dr_amount`,
									0 AS `cr_amount`,
									assets_details.asset_pd_period AS `transactionDate`,
									assets_details.asset_pd_period  AS `createdAt`,
									assets_details.asset_pd_period AS `referenceDate`,
									1 AS `status`,
									'FIXED ASSET' AS `transactionCategory`,
									'Asset Purchase' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
									assets_details,asset_category,account,account_type
									WHERE 
									assets_details.asset_category_id = asset_category.asset_category_id 
									AND account.account_id = asset_category.account_id
									AND account.account_type_id = account_type.account_type_id

								UNION ALL


								SELECT
									asset_category.asset_category_id AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									assets_details.asset_serial_no AS `referenceCode`,
									assets_details.asset_serial_no AS `transactionCode`,
									assets_details.asset_id AS `patient_id`,
								  	assets_details.supplier_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
									CONCAT(assets_details.asset_name) AS `transactionName`,
									CONCAT(assets_details.asset_name) AS `transactionDescription`,
									0 AS `dr_amount`,
									(assets_details.asset_value) AS `cr_amount`,
									assets_details.asset_pd_period AS `transactionDate`,
									assets_details.asset_pd_period  AS `createdAt`,
									assets_details.asset_pd_period AS `referenceDate`,
									1 AS `status`,
									'FIXED ASSET' AS `transactionCategory`,
									'Asset Purchase' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
									assets_details,asset_category,account,account_type
									WHERE 
									assets_details.asset_category_id = asset_category.asset_category_id 
									AND assets_details.bill_asset = 1
									AND account.account_id = ".$accounts_payable_id."
									AND account.account_type_id = account_type.account_type_id

							UNION ALL

								SELECT


									patients_journals.patient_journal_id AS transactionId,
									patients_journals.patient_journal_id AS referenceId,
									'' AS `payingFor`,
									'' AS referenceCode,
									'' AS `transactionCode`,
									patients_journals.patient_id AS patient_id,
									patients_journals.patient_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									account.account_name AS `accountName`,
									CONCAT('Patient Journals') AS `transactionName`,
									CONCAT('Patient Journals') AS `transactionDescription`,
									patients_journals.journal_amount AS `dr_amount`,
									0 AS `cr_amount`,
									patients_journals.journal_date AS `transactionDate`,
									patients_journals.journal_date  AS `createdAt`,
									patients_journals.journal_date AS `referenceDate`,
									1 AS `status`,
									'JOURNAL' AS `transactionCategory`,
									'JOURNAL' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
										patients_journals,visit_type,account,account_type
									WHERE 
									patients_journals.visit_type_id = visit_type.visit_type_id 
									AND patients_journals.visit_invoice_id > 0
									AND patients_journals.account_from_id = account.account_id
									AND account_type.account_type_id = account.account_type_id

							UNION ALL

								SELECT


									patients_journals.patient_journal_id AS transactionId,
									patients_journals.patient_journal_id AS referenceId,
									'' AS `payingFor`,
									'' AS referenceCode,
									'' AS `transactionCode`,
									patients_journals.patient_id AS patient_id,
									patients_journals.patient_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									account.account_name AS `accountName`,
									CONCAT('Patient Journals') AS `transactionName`,
									CONCAT('Patient Journals') AS `transactionDescription`,
									0 AS `dr_amount`,
									patients_journals.journal_amount AS `cr_amount`,
									patients_journals.journal_date AS `transactionDate`,
									patients_journals.journal_date  AS `createdAt`,
									patients_journals.journal_date AS `referenceDate`,
									1 AS `status`,
									'JOURNAL' AS `transactionCategory`,
									'JOURNAL' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
										patients_journals,visit_type,account,account_type
									WHERE 
									patients_journals.visit_type_id = visit_type.visit_type_id 
									AND patients_journals.visit_invoice_id > 0
									AND patients_journals.account_to_id = account.account_id
									AND account_type.account_type_id = account.account_type_id

								UNION ALL

								SELECT
									`loan_invoice_item`.`loan_invoice_item_id` AS `transactionId`,
									`loan_invoice`.`loan_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`loan_invoice`.`invoice_number` AS `referenceCode`,
									`loan_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_invoice`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`loan_invoice_item`.`item_description` AS `transactionName`,
									`loan_invoice_item`.`item_description` AS `transactionDescription`,
									 0 AS `dr_amount`,
									`loan_invoice_item`.`total_amount` AS `cr_amount`,
									`loan_invoice`.`transaction_date` AS `transactionDate`,
									`loan_invoice`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_invoice_item`.`loan_invoice_item_status` AS `status`,
									'Expense' AS `transactionCategory`,
									'loans Invoices' AS `transactionClassification`,
									'loan_invoice_item' AS `transactionTable`,
									'loan_invoice' AS `referenceTable`
								FROM
									(
										(
											(
												`loan_invoice_item`,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
									)
								WHERE 
								loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id 
								AND loan_invoice.loan_invoice_status = 1
								AND loan.loan_id = loan_invoice.loan_id 
								AND loan_invoice.transaction_date >= loan.start_date

							UNION ALL

								SELECT
									`loan_invoice_item`.`loan_invoice_item_id` AS `transactionId`,
									`loan_invoice`.`loan_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`loan_invoice`.`invoice_number` AS `referenceCode`,
									`loan_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_invoice`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan`.`account_to_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`loan_invoice_item`.`item_description` AS `transactionName`,
									`loan_invoice_item`.`item_description` AS `transactionDescription`,
									`loan_invoice_item`.`total_amount` AS `dr_amount`,
									'0' AS `cr_amount`,
									`loan_invoice`.`transaction_date` AS `transactionDate`,
									`loan_invoice`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_invoice_item`.`loan_invoice_item_status` AS `status`,
									'Expense' AS `transactionCategory`,
									'loans Invoices' AS `transactionClassification`,
									'loan_invoice_item' AS `transactionTable`,
									'loan_invoice' AS `referenceTable`
								FROM
									(
										(
											(
												`loan_invoice_item`,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan.account_to_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
									)
								WHERE 
								loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id 
								AND loan_invoice.loan_invoice_status = 1
								AND loan.loan_id = loan_invoice.loan_id 
								AND loan_invoice.transaction_date >= loan.start_date

								UNION ALL

								SELECT
									`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
									`loan_payment`.`loan_payment_id` AS `referenceId`,
									`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
									`loan_payment`.`reference_number` AS `referenceCode`,
									`loan_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_payment`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan_payment`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
									CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
									0 AS `dr_amount`,
									`loan_payment_item`.`amount_paid` AS `cr_amount`,
									`loan_payment`.`transaction_date` AS `transactionDate`,
									`loan_payment`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_payment_item`.`loan_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'loans Invoices Payments' AS `transactionClassification`,
									'loan_payment' AS `transactionTable`,
									'loan_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`loan_payment_item`,loan_payment,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan_payment.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE loan_payment_item.invoice_type = 0 
									AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
									AND loan_payment.loan_payment_status = 1
									AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
									AND loan_invoice.loan_invoice_status = 1  
									AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
									AND loan.account_from_id = loan_payment_item.account_to_id
									
							UNION ALL
							
							SELECT
									`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
									`loan_payment`.`loan_payment_id` AS `referenceId`,
									`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
									`loan_payment`.`reference_number` AS `referenceCode`,
									`loan_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_payment`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
									CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
									`loan_payment_item`.`amount_paid` AS `dr_amount`,
									0 AS `cr_amount`,
									`loan_payment`.`transaction_date` AS `transactionDate`,
									`loan_payment`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_payment_item`.`loan_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'loans Invoices Payments' AS `transactionClassification`,
									'loan_payment' AS `transactionTable`,
									'loan_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`loan_payment_item`,loan_payment,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE loan_payment_item.invoice_type = 0 
									AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
									AND loan_payment.loan_payment_status = 1
									AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
									AND loan_invoice.loan_invoice_status = 1  
									AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
									AND loan.account_from_id = loan_payment_item.account_to_id

									UNION ALL

									SELECT
											`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
											`loan_payment`.`loan_payment_id` AS `referenceId`,
											`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
											`loan_payment`.`reference_number` AS `referenceCode`,
											`loan_payment`.`document_number` AS `transactionCode`,
											'' AS `patient_id`,
												`loan_payment`.`loan_id` AS `recepientId`,
											`account`.`parent_account` AS `accountParentId`,
											`account_type`.`account_type_name` AS `accountsclassfication`,
											loan_payment_item.account_to_id AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
											CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
											`loan_payment_item`.`amount_paid` AS `dr_amount`,
											0 AS `cr_amount`,
											`loan_payment`.`transaction_date` AS `transactionDate`,
											`loan_payment`.`created` AS `createdAt`,
											`loan_invoice`.`transaction_date` AS `referenceDate`,
											`loan_payment_item`.`loan_payment_item_status` AS `status`,
											'Expense Payment' AS `transactionCategory`,
											'loans Invoices Payments' AS `transactionClassification`,
											'loan_payment' AS `transactionTable`,
											'loan_payment_item' AS `referenceTable`
										FROM
											(
												(
													(
														`loan_payment_item`,loan_payment,loan_invoice,loan,account
														
													)
												
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
											WHERE loan_payment_item.invoice_type = 0 
											AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
											AND loan_payment.loan_payment_status = 1
											AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
											AND loan_invoice.loan_invoice_status = 1  
											AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
											AND loan.account_from_id <> loan_payment_item.account_to_id
											AND account.account_id = loan_payment_item.account_to_id
											
											UNION ALL
											
											
											SELECT
												`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
												`loan_payment`.`loan_payment_id` AS `referenceId`,
												`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
												`loan_payment`.`reference_number` AS `referenceCode`,
												`loan_payment`.`document_number` AS `transactionCode`,
												'' AS `patient_id`,
													`loan_payment`.`loan_id` AS `recepientId`,
												`account`.`parent_account` AS `accountParentId`,
												`account_type`.`account_type_name` AS `accountsclassfication`,
												loan_payment.account_from_id AS `accountId`,
												`account`.`account_name` AS `accountName`,
												CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
												CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
												0 AS `dr_amount`,
												`loan_payment_item`.`amount_paid` AS `cr_amount`,
												`loan_payment`.`transaction_date` AS `transactionDate`,
												`loan_payment`.`created` AS `createdAt`,
												`loan_invoice`.`transaction_date` AS `referenceDate`,
												`loan_payment_item`.`loan_payment_item_status` AS `status`,
												'Expense Payment' AS `transactionCategory`,
												'loans Invoices Payments' AS `transactionClassification`,
												'loan_payment' AS `transactionTable`,
												'loan_payment_item' AS `referenceTable`
											FROM
												(
													(
														(
															`loan_payment_item`,loan_payment,loan_invoice,loan,account
															
														)
													
													)
													JOIN `account_type` ON(
														(
															account_type.account_type_id = account.account_type_id
														)
													)
													
												)
												WHERE loan_payment_item.invoice_type = 0 
												AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
												AND loan_payment.loan_payment_status = 1
												AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
												AND loan_invoice.loan_invoice_status = 1  
												AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
												AND loan.account_from_id <> loan_payment_item.account_to_id
												AND account.account_id = loan_payment.account_from_id

											UNION ALL


											SELECT
												asset_category.asset_category_id AS `transactionId`,
												'' AS `referenceId`,
												'' AS `payingFor`,
												assets_details.asset_serial_no AS `referenceCode`,
												assets_details.asset_serial_no AS `transactionCode`,
												assets_details.asset_id AS `patient_id`,
													assets_details.supplier_id AS `recepientId`,
												account_type.account_type_id AS `accountParentId`,
												account_type.account_type_name AS `accountsclassfication`,
												`account`.`account_id` AS `accountId`,
												CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
												CONCAT(assets_details.asset_name) AS `transactionName`,
												CONCAT(assets_details.asset_name) AS `transactionDescription`,
												(asset_amortization.interest_amount) AS `dr_amount`,
												0 AS `cr_amount`,
												asset_amortization.date_approved AS `transactionDate`,
												asset_amortization.date_approved  AS `createdAt`,
												asset_amortization.date_approved AS `referenceDate`,
												1 AS `status`,
												'FIXED ASSET' AS `transactionCategory`,
												'ASSET DEPRECIATION' AS `transactionClassification`,
												'account_payments' AS `transactionTable`,
												'' AS `referenceTable`
											FROM
												assets_details,asset_category,account,account_type,asset_amortization
												WHERE 
												assets_details.asset_category_id = asset_category.asset_category_id 
												AND asset_amortization.expense_account_id = account.account_id
												AND account.account_type_id = account_type.account_type_id
												AND asset_amortization.asset_id = assets_details.asset_id
												AND asset_amortization.bill_status = 1
												
										UNION ALL

										SELECT
												asset_category.asset_category_id AS `transactionId`,
												'' AS `referenceId`,
												'' AS `payingFor`,
												assets_details.asset_serial_no AS `referenceCode`,
												assets_details.asset_serial_no AS `transactionCode`,
												assets_details.asset_id AS `patient_id`,
													assets_details.supplier_id AS `recepientId`,
												account_type.account_type_id AS `accountParentId`,
												account_type.account_type_name AS `accountsclassfication`,
												`account`.`account_id` AS `accountId`,
												CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
												CONCAT(assets_details.asset_name) AS `transactionName`,
												CONCAT(assets_details.asset_name) AS `transactionDescription`,
												0 AS `dr_amount`,
												(asset_amortization.interest_amount) AS `cr_amount`,
												asset_amortization.date_approved AS `transactionDate`,
												asset_amortization.date_approved  AS `createdAt`,
												asset_amortization.date_approved AS `referenceDate`,
												1 AS `status`,
												'FIXED ASSET' AS `transactionCategory`,
												'ASSET DEPRECIATION' AS `transactionClassification`,
												'account_payments' AS `transactionTable`,
												'' AS `referenceTable`
											FROM
												assets_details,asset_category,account,account_type,asset_amortization
												WHERE 
												assets_details.asset_category_id = asset_category.asset_category_id 
												AND asset_amortization.acc_depreciation_id = account.account_id
												AND account.account_type_id = account_type.account_type_id
												AND asset_amortization.asset_id = assets_details.asset_id
												AND asset_amortization.bill_status = 1



					) AS data WHERE data.accountId > 0 ".$add." ".$add_item."  ORDER BY data.transactionDate ASC

					";
	 

	  $query = $this->db->query($select);


	   return $query;

	}

	public function get_account_ledger_by_accounts_opening_periods($array_leads,$year=NULL)
	{
		
		$end_month = $this->session->userdata('end_of_year_month');
		$end_day = $this->session->userdata('end_of_year_day');
		
		$this->db->where('branch_id',2);
		$query = $this->db->get('branch');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$end_month = $value->end_of_year_month;
				$end_day = $value->end_of_year_day;
			}
		}
		
		$financial_year_date = $year."-".$end_month."-".$end_day."";


		$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

		$patients = array();
		
		if($accounts_config_rs->num_rows() > 0)
		{
			foreach ($accounts_config_rs->result() as $key => $value) {
				// code...
				$staing_account_id = $value->account_id;
				$reference_name = $value->reference_name;

				$session_account[$reference_name] = $staing_account_id;

				
			}
		}

		$providers_liability_id = $session_account['providers_liability_id'];
		$providers_wht_id = $session_account['providers_wht_id'];
		$payroll_liability_id = $session_account['payroll_liability_id'];
		$accounts_payable_id = $session_account['accounts_payable_id'];
		$fixed_assets_id = $session_account['fixed_assets_id'];
		$accounts_receivable_id = $session_account['accounts_receivable_id'];
		$suppliers_wht_id = $session_account['suppliers_wht_id'];
		$income_account_id = $session_account['income_account_id'];
		$supplier_opening_balance_id= $session_account['supplier_opening_balance_id'];
		$bank_opening_balance_id = $session_account['bank_opening_balance_id'];
		$provider_opening_balance_id = $session_account['provider_opening_balance_id'];

		// $financial_year_date = "2021-12-31";
		$date_from = $this->session->userdata('date_from_general_ledger');
		$date_to = $this->session->userdata('date_to_general_ledger');
		$add = '';

		if(!empty($date_from) OR !empty($date_to))
    	{
    		
    		$add =  '  AND DATE(transactionDate) > \''.$financial_year_date.'\' AND DATE(transactionDate) < \''.$date_from.'\' ';
    		
    		
    	}
    	else if(!empty($date_from) AND empty($date_to))
    	{

    		
    		$add = ' AND DATE(transactionDate) > \''.$financial_year_date.'\' AND DATE(transactionDate) < \''.$date_from.'\'';
    		
    		
    	}
    	else if(empty($date_from) AND !empty($date_to))
    	{
    		
    		$add = ' AND DATE(transactionDate) > \''.$financial_year_date.'\' AND DATE(transactionDate) < \''.$date_to.'\'';
    		
    	}
    	else
    	{
    		$add =  ' AND DATE(transactionDate) > \''.$financial_year_date.'\' AND DATE(transactionDate) < \''.date('Y-m-01').'\'';
    	}

    		
    	$add_item = 'GROUP BY data.accountId';
    	
    	

    	
		$item = '	data.accountId AS accountId,
					data.accountName AS account_name,
				 	SUM(data.dr_amount) AS dr_amount,
				 	SUM(data.cr_amount) AS cr_amount';
    	
		$select  = "
					SELECT 
						".$item."
					FROM 

					(

						SELECT
							`account`.`account_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
						    '' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							account.account_id AS `accountId`,
							account.account_name AS `accountName`,
							CONCAT('Opening Balance as from',' ',`account`.`start_date`) AS `transactionName`,
							CONCAT('Opening Balance as from',' ',' ',`account`.`start_date`) AS `transactionDescription`,
							`account`.`account_opening_balance` AS `dr_amount`,
							'0' AS `cr_amount`,
							`account`.`start_date` AS `transactionDate`,
							`account`.`start_date` AS `createdAt`,
							`account`.`account_status` AS `status`,
							branch_id AS `branch_id`,
							'Income' AS `transactionCategory`,
							'Account Opening Balance' AS `transactionClassification`,
							'' AS `transactionTable`,
							'account' AS `referenceTable`
						FROM
						account,account_type

						WHERE 

						account_type.account_type_id = account.account_type_id AND account.account_opening_balance > 0 AND account.parent_account > 0  AND (account_type.account_type_name = 'Bank' OR account_type.account_type_name ='Capital')

						UNION ALL

	
						SELECT
							`account`.`account_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							account.account_id AS `accountId`,
							account.account_name AS `accountName`,
							CONCAT( 'Opening Balance as from', ' ', `account`.`start_date` ) AS `transactionName`,
							CONCAT( 'Opening Balance as from', ' ', ' ', `account`.`start_date` ) AS `transactionDescription`,
							0 AS `dr_amount`,
							(SELECT COALESCE(SUM(account_opening_balance),0) FROM account,account_type WHERE  account_type.account_type_id = account.account_type_id  AND account.account_opening_balance > 0  AND (
							account_type.account_type_name = 'Bank' 
							OR account_type.account_type_name = 'Capital') ) AS `cr_amount`,
							`account`.`start_date` AS `transactionDate`,
							`account`.`start_date` AS `createdAt`,
							`account`.`account_status` AS `status`,
							branch_id AS `branch_id`,
							'Income' AS `transactionCategory`,
							'Account Opening Balance' AS `transactionClassification`,
							'' AS `transactionTable`,
							'account' AS `referenceTable` 
						FROM
							account,
							account_type 
						WHERE
							account_type.account_type_id = account.account_type_id 
							AND account.parent_account > 0 
							AND account.account_id = ".$bank_opening_balance_id." 



						UNION ALL

						SELECT
						  	`finance_transfered`.`finance_transfered_id` AS `transactionId`,
						  	`finance_transfer`.`finance_transfer_id` AS `referenceId`,
						  	'' AS `payingFor`,
						  	`finance_transfer`.`reference_number` AS `referenceCode`,
						  	`finance_transfer`.`document_number` AS `transactionCode`,
						  	'' AS `patient_id`,
						    '' AS `recepientId`,
						  	`account`.`parent_account` AS `accountParentId`,
						  	`account_type`.`account_type_name` AS `accountsclassfication`,
						  	`finance_transfered`.`account_to_id` AS `accountId`,
						  	`account`.`account_name` AS `accountName`,
						  	`finance_transfered`.`remarks` AS `transactionName`,
						  	 CONCAT('Amount Received from ',(SELECT account_name FROM account WHERE account_id = finance_transfer.account_from_id ),' Ref. ', `finance_transfer`.`reference_number`) AS `transactionDescription`,
						  	`finance_transfer`.`finance_transfer_amount` AS `cr_amount`,
						     0 AS `cr_amount`,
						  	`finance_transfer`.`transaction_date` AS `transactionDate`,
						  	`finance_transfer`.`created` AS `createdAt`,
						  	`finance_transfer`.`finance_transfer_status` AS `status`,
						  	`finance_transfer`.`branch_id` AS `branch_id`,
						  	'Transfer' AS `transactionCategory`,
						  	'Transfer' AS `transactionClassification`,
						  	'finance_transfer' AS `transactionTable`,
						  	'finance_transfered' AS `referenceTable`
						  FROM
						  `finance_transfer`,finance_transfered,account,account_type
						   WHERE finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id 
						  	AND account.account_id = finance_transfered.account_to_id
						  	AND account_type.account_type_id = account.account_type_id
						  	AND finance_transfer.finance_transfer_deleted = 0

					UNION ALL

						SELECT
						  	`finance_transfer`.`finance_transfer_id` AS `transactionId`,
						  	`finance_transfered`.`finance_transfered_id` AS `referenceId`,
						  	'' AS `payingFor`,
						  	`finance_transfer`.`reference_number` AS `referenceCode`,
						  	`finance_transfer`.`document_number` AS `transactionCode`,
						  	'' AS `patient_id`,
						    '' AS `recepientId`,
						  	`account`.`parent_account` AS `accountParentId`,
						  	`account_type`.`account_type_name` AS `accountsclassfication`,
						  	`finance_transfer`.`account_from_id` AS `accountId`,
						  	`account`.`account_name` AS `accountName`,
						  	`finance_transfer`.`remarks` AS `transactionName`,
						  	CONCAT(' Amount Transfered to ',(SELECT account_name FROM account WHERE account_id = finance_transfered.account_to_id )) AS `transactionDescription`,
						  	0 AS `dr_amount`,
						  	`finance_transfer`.`finance_transfer_amount` AS `cr_amount`,
						  	`finance_transfer`.`transaction_date` AS `transactionDate`,
						  	`finance_transfer`.`transaction_date` AS `transactionDate`,

						  	`finance_transfer`.`finance_transfer_status` AS `status`,
						  	`finance_transfer`.`branch_id` AS `branch_id`,
						  	'Transfer' AS `transactionCategory`,
						  	'Transfer' AS `transactionClassification`,
						  	'finance_transfered' AS `transactionTable`,
						  	'finance_transfer' AS `referenceTable`
						  FROM
							`finance_transfer`,finance_transfered,account,account_type
						  				
						  WHERE finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id 
						  	AND account.account_id = finance_transfer.account_from_id
						  	AND account_type.account_type_id = account.account_type_id
						  	AND finance_transfer.finance_transfer_deleted = 0

						UNION ALL


						SELECT
							`finance_purchase_payment`.`finance_purchase_payment_id` AS `transactionId`,
							'' AS `referenceId`,
							`finance_purchase`.`finance_purchase_id` AS `payingFor`,
							`finance_purchase`.`transaction_number` AS `referenceCode`,
							`finance_purchase`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
						  	finance_purchase.creditor_id AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`finance_purchase_payment`.`account_from_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`finance_purchase`.`finance_purchase_description` AS `transactionName`,
							CONCAT( `finance_purchase`.`finance_purchase_description`) AS `transactionDescription`,
							0 AS `dr_amount`,
							`finance_purchase_payment`.`amount_paid` AS `cr_amount`,
							`finance_purchase`.`transaction_date` AS `transactionDate`,
							`finance_purchase`.`created` AS `createdAt`,
							`finance_purchase_payment`.`finance_purchase_payment_status` AS `status`,
							`finance_purchase`.`branch_id` AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Purchase Payment' AS `transactionClassification`,
							'finance_purchase' AS `transactionTable`,
							'finance_purchase_payment' AS `referenceTable`
						FROM
						`finance_purchase_payment`,finance_purchase,account,account_type
						WHERE finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id AND finance_purchase.finance_purchase_deleted = 0 AND account.account_id = finance_purchase_payment.account_from_id
							AND finance_purchase.finance_purchase_delete = 0 AND account_type.account_type_id = account.account_type_id
							AND finance_purchase.account_to_id > 0 

						UNION ALL

						SELECT
							`finance_purchase`.`finance_purchase_id` AS `transactionId`,
							'' AS `referenceId`,
							`finance_purchase`.`finance_purchase_id` AS `payingFor`,
							`finance_purchase`.`transaction_number` AS `referenceCode`,
							`finance_purchase`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`finance_purchase`.`creditor_id` AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`finance_purchase`.`account_to_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`finance_purchase`.`finance_purchase_description` AS `transactionName`,
							CONCAT( `finance_purchase`.`finance_purchase_description`)  AS `transactionDescription`,
							`finance_purchase_payment`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`finance_purchase`.`transaction_date` AS `transactionDate`,
							`finance_purchase`.`created` AS `createdAt`,
							`finance_purchase`.`finance_purchase_status` AS `status`,
							`finance_purchase`.`branch_id` AS `branch_id`,
							'Expense' AS `transactionCategory`,
							'Purchases' AS `transactionClassification`,
							'finance_purchase' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							`finance_purchase_payment`,finance_purchase,account,account_type
						WHERE finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id AND finance_purchase.finance_purchase_deleted = 0 
						AND account.account_id = finance_purchase.account_to_id
							AND finance_purchase.finance_purchase_delete = 0
							AND account_type.account_type_id = account.account_type_id
							AND finance_purchase.account_to_id > 0 

						UNION ALL

					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor`.`opening_balance` AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`
					FROM
					creditor,account,account_type
					WHERE debit_id = 1 AND account.account_id = ".$supplier_opening_balance_id." AND account.account_type_id = account_type.account_type_id

					UNION ALL

					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						`creditor`.`opening_balance` AS `dr_amount`,
						'0' AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						 creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`
					FROM
					creditor,account,account_type
					WHERE debit_id = 2 AND account.account_id = ".$supplier_opening_balance_id." AND account.account_type_id = account_type.account_type_id


					UNION ALL


					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						`creditor`.`opening_balance` AS `dr_amount`,
						0 AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`
					FROM
					creditor,account,account_type
					WHERE debit_id = 1 AND account.account_id = ".$accounts_payable_id." AND account.account_type_id = account_type.account_type_id

					UNION ALL

					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor`.`opening_balance`  AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						 creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`
					FROM
					creditor,account,account_type
					WHERE debit_id = 2 AND account.account_id = ".$accounts_payable_id." AND account.account_type_id = account_type.account_type_id
				

					UNION ALL


					SELECT
						`creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
						`creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
						'' AS `payingFor`,
						`creditor_invoice`.`invoice_number` AS `referenceCode`,
						`creditor_invoice`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
					  `creditor_invoice`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`creditor_invoice_item`.`account_to_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_invoice_item`.`item_description` AS `transactionName`,
						CONCAT(`creditor_invoice_item`.`item_description`,' ',creditor_invoice.invoice_number) AS `transactionDescription`,
						`creditor_invoice_item`.`total_amount` AS `dr_amount`,
						'0' AS `cr_amount`,
						`creditor_invoice`.`transaction_date` AS `transactionDate`,
						`creditor_invoice`.`created` AS `createdAt`,
						`creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
						'creditor.branch_id' AS `branch_id`,
						'Cost of Goods' AS `transactionCategory`,
						'Creditors Invoices' AS `transactionClassification`,
						'creditor_invoice_item' AS `transactionTable`,
						'creditor_invoice' AS `referenceTable`
					FROM
						(
							(
								(
									`creditor_invoice_item`,creditor_invoice,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = creditor_invoice_item.account_to_id
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)
						)
					WHERE 

					creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id 
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date

				UNION ALL

				  SELECT
					`creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
					`creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
					`creditor_credit_note_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_credit_note`.`invoice_number` AS `referenceCode`,
					`creditor_credit_note`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_credit_note`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`creditor_credit_note`.`account_from_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_credit_note_item`.`description` AS `transactionName`,
					`creditor_credit_note_item`.`description` AS `transactionDescription`,
					0 AS `dr_amount`,
					`creditor_credit_note_item`.`credit_note_amount` AS `cr_amount`,
					`creditor_credit_note`.`transaction_date` AS `transactionDate`,
					`creditor_credit_note`.`created` AS `createdAt`,
					`creditor_invoice`.`transaction_date` AS `referenceDate`,
					`creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
					'Supplier Credit Note' AS `transactionCategory`,
					'Creditors Credit Notes' AS `transactionClassification`,
					'creditor_credit_note' AS `transactionTable`,
					'creditor_credit_note_item' AS `referenceTable`
				FROM
					(
						(
							(
								`creditor_credit_note_item`,creditor_credit_note,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = creditor_credit_note.account_from_id
								)
							)
							
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					)
				WHERE 
					creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id 
					AND creditor_credit_note.creditor_credit_note_status = 1
					AND creditor_invoice.creditor_invoice_id = creditor_credit_note_item.creditor_invoice_id
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date


				UNION ALL


				SELECT
						`creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
						`creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
						'' AS `payingFor`,
						`creditor_invoice`.`invoice_number` AS `referenceCode`,
						`creditor_invoice`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
					  `creditor_invoice`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`account`.`account_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_invoice_item`.`item_description` AS `transactionName`,
						CONCAT(`creditor_invoice_item`.`item_description`,' ',creditor_invoice.invoice_number) AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor_invoice_item`.`total_amount` AS `cr_amount`,
						`creditor_invoice`.`transaction_date` AS `transactionDate`,
						`creditor_invoice`.`created` AS `createdAt`,
						`creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
						'creditor.branch_id' AS `branch_id`,
						'Cost of Goods' AS `transactionCategory`,
						'Creditors Invoices' AS `transactionClassification`,
						'creditor_invoice_item' AS `transactionTable`,
						'creditor_invoice' AS `referenceTable`
					FROM
						(
							(
								(
									`creditor_invoice_item`,creditor_invoice,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = ".$accounts_payable_id."
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)
						)
					WHERE 

					creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id 
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date

				UNION ALL

				  SELECT
					`creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
					`creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
					`creditor_credit_note_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_credit_note`.`invoice_number` AS `referenceCode`,
					`creditor_credit_note`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_credit_note`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`account`.`account_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_credit_note_item`.`description` AS `transactionName`,
					`creditor_credit_note_item`.`description` AS `transactionDescription`,
					`creditor_credit_note_item`.`credit_note_amount` AS `dr_amount`,
					0 AS `cr_amount`,
					`creditor_credit_note`.`transaction_date` AS `transactionDate`,
					`creditor_credit_note`.`created` AS `createdAt`,
					`creditor_invoice`.`transaction_date` AS `referenceDate`,
					`creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
					'Supplier Credit Note' AS `transactionCategory`,
					'Creditors Credit Notes' AS `transactionClassification`,
					'creditor_credit_note' AS `transactionTable`,
					'creditor_credit_note_item' AS `referenceTable`
				FROM
					(
						(
							(
								`creditor_credit_note_item`,creditor_credit_note,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = ".$accounts_payable_id."
								)
							)
							
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					)
				WHERE 
					creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id 
					AND creditor_credit_note.creditor_credit_note_status = 1
					AND creditor_invoice.creditor_invoice_id = creditor_credit_note_item.creditor_invoice_id
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date



				UNION ALL


				SELECT
					`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
					`creditor_payment`.`creditor_payment_id` AS `referenceId`,
					`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_payment`.`reference_number` AS `referenceCode`,
					`creditor_payment`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_payment`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`creditor_payment`.`account_from_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_payment_item`.`description` AS `transactionName`,
					CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
					0 AS `dr_amount`,
					`creditor_payment_item`.`amount_paid` AS `cr_amount`,
					`creditor_payment`.`transaction_date` AS `transactionDate`,
					`creditor_payment`.`created` AS `createdAt`,
					`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
					'creditor_payment.branch_id' AS `branch_id`,
					'Creditor Payment' AS `transactionCategory`,
					'Creditors Invoices Payments' AS `transactionClassification`,
					'creditor_payment' AS `transactionTable`,
					'creditor_payment_item' AS `referenceTable`
				FROM
					(
						(
							(
								`creditor_payment_item`,creditor_payment,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = creditor_payment.account_from_id
								)
							)
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					
					)
					WHERE creditor_payment_item.invoice_type = 0 
					AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
					AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
					AND creditor_payment.creditor_payment_status = 1
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor_invoice.creditor_id = creditor.creditor_id
					AND creditor_payment.transaction_date >= creditor.start_date

				UNION ALL


				SELECT
					`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
					`creditor_payment`.`creditor_payment_id` AS `referenceId`,
					`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_payment`.`reference_number` AS `referenceCode`,
					`creditor_payment`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_payment`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`account`.`account_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_payment_item`.`description` AS `transactionName`,
					CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
					`creditor_payment_item`.`amount_paid` AS `dr_amount`,
					0 AS `cr_amount`,
					`creditor_payment`.`transaction_date` AS `transactionDate`,
					`creditor_payment`.`created` AS `createdAt`,
					`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
					'creditor_payment.branch_id' AS `branch_id`,
					'Creditor Payment' AS `transactionCategory`,
					'Creditors Invoices Payments' AS `transactionClassification`,
					'creditor_payment' AS `transactionTable`,
					'creditor_payment_item' AS `referenceTable`
				FROM
					(
						(
							(
								`creditor_payment_item`,creditor_payment,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = ".$accounts_payable_id."
								)
							)
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					
					)
					WHERE creditor_payment_item.invoice_type = 0 
					AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
					AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
					AND creditor_payment.creditor_payment_status = 1
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor_invoice.creditor_id = creditor.creditor_id
					AND creditor_payment.transaction_date >= creditor.start_date

				UNION ALL

					SELECT
						`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
						`creditor_payment`.`creditor_payment_id` AS `referenceId`,
						`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
						`creditor_payment`.`reference_number` AS `referenceCode`,
						`creditor_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
						`creditor_payment`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`creditor_payment`.`account_from_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_payment_item`.`description` AS `transactionName`,
						CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor_payment_item`.`amount_paid` AS `cr_amount`,
						`creditor_payment`.`transaction_date` AS `transactionDate`,
						`creditor_payment`.`created` AS `createdAt`,
						`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
						'creditor_payment.branch_id' AS `branch_id`,
						'Creditor Payment' AS `transactionCategory`,
						'Creditors Invoices Payments' AS `transactionClassification`,
						'creditor_payment' AS `transactionTable`,
						'creditor_payment_item' AS `referenceTable`
						FROM
						(
							(
								(
									`creditor_payment_item`,creditor_payment,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = creditor_payment.account_from_id
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)

						)
						WHERE creditor_payment_item.invoice_type = 2 
						AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
						AND creditor.creditor_id = creditor_payment_item.creditor_id
						AND creditor_payment.transaction_date >= creditor.start_date

				UNION ALL

					SELECT
						`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
						`creditor_payment`.`creditor_payment_id` AS `referenceId`,
						`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
						`creditor_payment`.`reference_number` AS `referenceCode`,
						`creditor_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
						`creditor_payment`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`account`.`account_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_payment_item`.`description` AS `transactionName`,
						CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
						`creditor_payment_item`.`amount_paid` AS `dr_amount`,
						0 AS `cr_amount`,
						`creditor_payment`.`transaction_date` AS `transactionDate`,
						`creditor_payment`.`created` AS `createdAt`,
						`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
						'creditor_payment.branch_id' AS `branch_id`,
						'Creditor Payment' AS `transactionCategory`,
						'Creditors Invoices Payments' AS `transactionClassification`,
						'creditor_payment' AS `transactionTable`,
						'creditor_payment_item' AS `referenceTable`
						FROM
						(
							(
								(
									`creditor_payment_item`,creditor_payment,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = ".$accounts_payable_id."
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)

						)
						WHERE creditor_payment_item.invoice_type = 2 
						AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
						AND creditor.creditor_id = creditor_payment_item.creditor_id
						AND creditor_payment.transaction_date >= creditor.start_date

					UNION ALL

						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`creditor_payment`.`account_from_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							0 AS `dr_amount`,
							`creditor_payment_item`.`amount_paid` AS `cr_amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							'creditor_payment.branch_id' AS `branch_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`
							FROM
							(
								(
									(
										`creditor_payment_item`,creditor_payment,creditor
										
									)
									JOIN account ON(
										(
											account.account_id = creditor_payment.account_from_id
										)
									)
								)
								JOIN `account_type` ON(
									(
										account_type.account_type_id = account.account_type_id
									)
								)
								
							)
							WHERE creditor_payment_item.invoice_type = 3
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
							AND creditor.creditor_id = creditor_payment_item.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date
					UNION ALL

						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							`creditor_payment_item`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							'creditor_payment.branch_id' AS `branch_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`
							FROM
							(
								(
									(
										`creditor_payment_item`,creditor_payment,creditor
										
									)
									JOIN account ON(
										(
											account.account_id = ".$accounts_payable_id."
										)
									)
								)
								JOIN `account_type` ON(
									(
										account_type.account_type_id = account.account_type_id
									)
								)
								
							)
							WHERE creditor_payment_item.invoice_type = 3
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
							AND creditor.creditor_id = creditor_payment_item.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date


						UNION ALL

							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							 	`orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Drug Purchase' AS `transactionName`,
								CONCAT('Purchase of supplies',' ',orders.supplier_invoice_number) AS `transactionDescription`,
								SUM(`order_supplier`.`less_vat`) AS `dr_amount`,
								0 AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Purchases' AS `transactionCategory`,
								'Supplies Invoices' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`
							FROM
								`order_supplier`,orders,account,order_item,product,account_type,creditor
										

							WHERE orders.is_store = 0 
								AND orders.supplier_id = creditor.creditor_id
								AND orders.order_id = order_supplier.order_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND product.product_id = order_item.product_id
								AND account_type.account_type_id = account.account_type_id
								AND account.account_id = orders.account_id
								AND orders.order_approval_status = 7 
								AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id

							UNION ALL



							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							 	`orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Drug Purchase' AS `transactionName`,
								CONCAT('Purchase of supplies',' ',orders.supplier_invoice_number) AS `transactionDescription`,
								0 AS `dr_amount`,
								SUM(`order_supplier`.`less_vat`) AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Purchases' AS `transactionCategory`,
								'Supplies Invoices' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`
							FROM
								`order_supplier`,orders,account,order_item,product,account_type,creditor
										

							WHERE orders.is_store = 0 
								AND orders.supplier_id = creditor.creditor_id
								AND orders.order_id = order_supplier.order_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND product.product_id = order_item.product_id
								AND account_type.account_type_id = account.account_type_id
								AND account.account_id = ".$accounts_payable_id."
								AND orders.order_approval_status = 7 
								AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id

							UNION ALL

							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								`orders`.`reference_number`  AS `transactionCode`,
								'' AS `patient_id`,
							  `orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`orders`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Credit' AS `transactionName`,
								CONCAT('Credit note of ',' ',`orders`.`reference_number`) AS `transactionDescription`,
								'0' AS `dr_amount`,
								SUM(`order_supplier`.`less_vat`) AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Income' AS `transactionCategory`,
								'Supplies Credit Note' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`

							FROM
								`order_supplier`,orders,order_item,product,account,account_type,creditor
								
								WHERE orders.is_store = 3
								AND orders.order_id = order_supplier.order_id
								AND account.account_id = orders.account_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND product.product_id = order_item.product_id
								AND account_type.account_type_id = account.account_type_id
								 AND orders.supplier_id = creditor.creditor_id and orders.order_approval_status = 7 AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								 AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id

							UNION ALL

							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								`orders`.`reference_number`  AS `transactionCode`,
								'' AS `patient_id`,
							  `orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Credit' AS `transactionName`,
								CONCAT('Credit note of ',' ',`orders`.`supplier_invoice_number`) AS `transactionDescription`,
								SUM(`order_supplier`.`less_vat`)  AS `dr_amount`,
								0 AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Income' AS `transactionCategory`,
								'Supplies Credit Note' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`

							FROM
								`order_supplier`,orders,account,order_item,product,account_type,creditor
											

								WHERE orders.is_store = 3
								AND account.account_id = ".$accounts_payable_id."
								AND account_type.account_type_id = account.account_type_id
								AND product.product_id = order_item.product_id
								AND orders.order_id = order_supplier.order_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND orders.supplier_id = creditor.creditor_id and orders.order_approval_status = 7 AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id



							UNION ALL

							 SELECT
								`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
								`creditor_payment`.`creditor_payment_id` AS `referenceId`,
								`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
								`creditor_payment`.`reference_number` AS `referenceCode`,
								`creditor_payment`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`creditor_payment`.`creditor_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								creditor_payment.account_from_id AS `accountId`,
								`account`.`account_name` AS `accountName`,
								`creditor_payment_item`.`description` AS `transactionName`,
								CONCAT('Payment to ', creditor.creditor_name ,'for ',`orders`.`supplier_invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
								0 AS `dr_amount`,
								`creditor_payment_item`.`amount_paid` AS `cr_amount`,
								`creditor_payment`.`transaction_date` AS `transactionDate`,
								`creditor_payment`.`created` AS `createdAt`,
								`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
								'creditor_payment.branch_id' AS `branch_id`,
								'Creditor Payment' AS `transactionCategory`,
								'Creditors Invoices Payments' AS `transactionClassification`,
								'creditor_payment' AS `transactionTable`,
								'creditor_payment_item' AS `referenceTable`
								FROM
								(
									(
										(
											`creditor_payment_item`,creditor_payment,orders,creditor
											
										)
										JOIN account ON(
											(
												account.account_id = creditor_payment.account_from_id
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
									
								)
							WHERE creditor_payment_item.invoice_type = 1 
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id  
							AND creditor_payment.creditor_payment_status = 1 
							AND orders.order_id = creditor_payment_item.creditor_invoice_id
							AND orders.supplier_id = creditor.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date

							UNION ALL

							SELECT
								`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
								`creditor_payment`.`creditor_payment_id` AS `referenceId`,
								`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
								`creditor_payment`.`reference_number` AS `referenceCode`,
								`creditor_payment`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`creditor_payment`.`creditor_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								`account`.`account_name` AS `accountName`,
								`creditor_payment_item`.`description` AS `transactionName`,
								CONCAT('Payment to ', creditor.creditor_name ,'for ',`orders`.`supplier_invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
								`creditor_payment_item`.`amount_paid` AS `dr_amount`,
								0 AS `cr_amount`,
								`creditor_payment`.`transaction_date` AS `transactionDate`,
								`creditor_payment`.`created` AS `createdAt`,
								`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
								'creditor_payment.branch_id' AS `branch_id`,
								'Creditor Payment' AS `transactionCategory`,
								'Creditors Invoices Payments' AS `transactionClassification`,
								'creditor_payment' AS `transactionTable`,
								'creditor_payment_item' AS `referenceTable`
								FROM
								(
									(
										(
											`creditor_payment_item`,creditor_payment,orders,creditor
											
										)
										JOIN account ON(
											(
												account.account_id = ".$accounts_payable_id."
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
									
								)
							WHERE creditor_payment_item.invoice_type = 1 
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id  
							AND creditor_payment.creditor_payment_status = 1 
							AND orders.order_id = creditor_payment_item.creditor_invoice_id
							AND orders.supplier_id = creditor.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date
								

					

						UNION ALL




						SELECT

							bank_reconcilliation.recon_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.account_name AS `accountName`,
							'' AS `transactionName`,
							CONCAT('Bank Charges ', account.account_name ) AS `transactionDescription`,
							0 AS `dr_amount`,
							SUM(bank_reconcilliation.service_charged) AS `cr_amount`,
							bank_reconcilliation.charged_date AS `transactionDate`,
							bank_reconcilliation.charged_date AS `createdAt`,
							bank_reconcilliation.recon_status AS `status`,
							2 AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							bank_reconcilliation,account,account_type
							WHERE bank_reconcilliation.recon_status = 2
							AND bank_reconcilliation.account_id = account.account_id
							AND account.account_type_id = account_type.account_type_id
						GROUP BY bank_reconcilliation.recon_id



						UNION ALL


						SELECT

							bank_reconcilliation.recon_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.account_name AS `accountName`,
							'' AS `transactionName`,
							CONCAT('Bank Charges ', account.account_name ) AS `transactionDescription`,
							SUM(bank_reconcilliation.service_charged) AS `dr_amount`,
							0 AS `cr_amount`,
							bank_reconcilliation.charged_date AS `transactionDate`,
							bank_reconcilliation.charged_date AS `createdAt`,
							bank_reconcilliation.recon_status AS `status`,
							2 AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							bank_reconcilliation,account,account_type
							WHERE bank_reconcilliation.recon_status = 2
							AND bank_reconcilliation.expense_account_id = account.account_id
							AND account.account_type_id = account_type.account_type_id
						GROUP BY bank_reconcilliation.recon_id

						UNION ALL 

						SELECT


							`journal_entry`.`journal_entry_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							journal_entry.document_number AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							journal_entry.account_from_id AS `accountId`,
							account.account_name AS `accountName`,
							'Journal Entry ' AS `transactionName`,
							CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
							`journal_entry`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`journal_entry`.`payment_date` AS `transactionDate`,
							`journal_entry`.`payment_date` AS `createdAt`,
							`journal_entry`.`journal_entry_status` AS `status`,
							2 AS `branch_id`,
							'Journal Credit' AS `transactionCategory`,
							'Journal' AS `transactionClassification`,
							'journal_entry' AS `transactionTable`,
							'account' AS `referenceTable`
						FROM
						journal_entry,account,account_type
						
						WHERE journal_entry.account_from_id  = account.account_id AND journal_entry.journal_entry_deleted = 0 
						AND account_type.account_type_id = account.account_type_id



						UNION ALL 


						SELECT
							`journal_entry`.`journal_entry_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							journal_entry.document_number AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							journal_entry.account_to_id AS `accountId`,
							account.account_name AS `accountName`,
							'Journal Entry ' AS `transactionName`,
							CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
							0 AS `dr_amount`,
							`journal_entry`.`amount_paid` AS `cr_amount`,
							`journal_entry`.`payment_date` AS `transactionDate`,
							`journal_entry`.`payment_date` AS `createdAt`,
							`journal_entry`.`journal_entry_status` AS `status`,
							2 AS `branch_id`,
							'Journal Debit' AS `transactionCategory`,
							'Journal' AS `transactionClassification`,
							'journal_entry' AS `transactionTable`,
							'account' AS `referenceTable`
						FROM
						journal_entry,account,account_type
						
						WHERE journal_entry.account_to_id  = account.account_id AND journal_entry.journal_entry_deleted = 0 
						AND account_type.account_type_id = account.account_type_id

						


						UNION ALL


						SELECT
								visit_invoice.visit_invoice_id AS transactionId,
								visit_charge.visit_charge_id  AS referenceId,
								'' AS `payingFor`,
								visit_invoice.visit_invoice_number AS referenceCode,
								'' AS transactionCode,
								visit_invoice.patient_id AS patientId,
								'' AS recepientId,

								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
							    account.account_id AS `accountId`,
							    `account`.`account_name` AS `accountName`,
							   	service_charge.service_charge_name AS transactionName,
								CONCAT( '<strong>Invoice </strong>  Invoice Number :  ', visit_invoice.visit_invoice_number,' <strong>Bill to:</strong>  ', visit_type.visit_type_name ) AS transactionDescription,
								0 AS dr_amount,
								visit_charge.visit_charge_amount*visit_charge.visit_charge_units AS cr_amount,
							    visit_invoice.created AS `transactionDate`,
							    visit_invoice.created AS `createdAt`,
							    1 AS `status`,
							    2 AS `branch_id`,
							    service.service_name AS `transactionCategory`,
							    'PATIENT INVOICE' AS `transactionClassification`,
							    'visit_invoice' AS `transactionTable`,
							    'visit_charge' AS `referenceTable`


						FROM
									visit_charge,visit_invoice,service_charge,visit_type,service,account,account_type,visit
									WHERE visit_charge.charged = 1  
									AND visit_invoice.visit_invoice_delete = 0
									AND visit_charge.visit_charge_delete = 0 
									AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id
									AND service_charge.service_charge_id = visit_charge.service_charge_id
									AND visit_type.visit_type_id = visit_invoice.bill_to
									AND service.service_id = service_charge.service_id
									AND account.account_id = service.account_id
									AND visit.visit_id = visit_charge.visit_id
									AND visit.visit_delete = 0
									AND account_type.account_type_id = account.account_type_id


						UNION ALL

						SELECT
							visit_credit_note.visit_credit_note_id AS transactionId,
							visit_invoice.visit_invoice_id AS referenceId,
							'' AS `payingFor`,
							visit_credit_note.visit_cr_note_number AS referenceCode,
							visit_credit_note.visit_cr_note_number AS transactionCode,
							visit_invoice.patient_id AS patient_id,
							'' AS recepientId,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
						    account.account_id AS `accountId`,
						    `account`.`account_name` AS `accountName`,
						    service_charge.service_charge_name AS transactionName,
							CONCAT('<strong>Credit Note for </strong>  Invoice Number: ', visit_invoice.visit_invoice_number,'<strong>Credit Note No:</strong> ',visit_credit_note.visit_cr_note_number) AS transactionDescription,
						   
							visit_credit_note_item.visit_cr_note_amount*visit_credit_note_item.visit_cr_note_units AS dr_amount,
							0 AS cr_amount,
						    visit_invoice.created AS `transactionDate`,
						    visit_credit_note.created AS `createdAt`,
						    1 AS `status`,
						    2 AS `branch_id`,
						    CONCAT(service.service_name,' Credit note') AS `transactionCategory`,
						    'INCOME CREDIT NOTE' AS `transactionClassification`,
						    'visit_invoice' AS `transactionTable`,
						    'visit_credit_note' AS `referenceTable`
						FROM
							visit_credit_note_item,visit_credit_note,visit_invoice,service_charge,service,account,account_type
							WHERE visit_credit_note.visit_cr_note_delete = 0 AND visit_credit_note_item.visit_cr_note_item_delete = 0 

							AND visit_invoice.visit_invoice_id = visit_credit_note.visit_invoice_id 
							AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id 
							AND service_charge.service_charge_id = visit_credit_note_item.service_charge_id
							AND service.service_id = service_charge.service_id
							AND account.account_id = service.account_id
							AND account_type.account_type_id = account.account_type_id



						UNION ALL



						SELECT
								visit_invoice.visit_invoice_id AS transactionId,
								visit_charge.visit_charge_id  AS referenceId,
								'' AS `payingFor`,
								visit_invoice.visit_invoice_number AS referenceCode,
								'' AS transactionCode,
								visit_invoice.patient_id AS patientId,
								'' AS recepientId,

								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
							    account.account_id AS `accountId`,
							    `account`.`account_name` AS `accountName`,
							   	service_charge.service_charge_name AS transactionName,
								CONCAT( '<strong>Invoice </strong>  Invoice Number :  ', visit_invoice.visit_invoice_number,' <strong>Bill to:</strong>  ', visit_type.visit_type_name ) AS transactionDescription,
								visit_charge.visit_charge_amount*visit_charge.visit_charge_units AS dr_amount,
								0 AS cr_amount,
							    visit_invoice.created AS `transactionDate`,
							    visit_invoice.created AS `createdAt`,
							    1 AS `status`,
							    2 AS `branch_id`,
							    service.service_name AS `transactionCategory`,
							    'PATIENT INVOICE' AS `transactionClassification`,
							    'visit_invoice' AS `transactionTable`,
							    'visit_charge' AS `referenceTable`


						FROM
									visit_charge,visit_invoice,service_charge,visit_type,service,account,account_type,visit
									WHERE visit_charge.charged = 1  
									AND visit_invoice.visit_invoice_delete = 0
									AND visit_charge.visit_charge_delete = 0 
									AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id
									AND service_charge.service_charge_id = visit_charge.service_charge_id
									AND visit_type.visit_type_id = visit_invoice.bill_to
									AND service.service_id = service_charge.service_id
									AND visit.visit_id = visit_charge.visit_id
									AND visit.visit_delete = 0
									AND account.account_id = ".$accounts_receivable_id."
									AND account_type.account_type_id = account.account_type_id


						UNION ALL

						SELECT
							visit_credit_note.visit_credit_note_id AS transactionId,
							visit_invoice.visit_invoice_id AS referenceId,
							'' AS `payingFor`,
							visit_credit_note.visit_cr_note_number AS referenceCode,
							visit_credit_note.visit_cr_note_number AS transactionCode,
							visit_invoice.patient_id AS patient_id,
							'' AS recepientId,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
						    account.account_id AS `accountId`,
						    `account`.`account_name` AS `accountName`,
						    service_charge.service_charge_name AS transactionName,
							CONCAT('<strong>Credit Note for </strong>  Invoice Number: ', visit_invoice.visit_invoice_number,'<strong>Credit Note No:</strong> ',visit_credit_note.visit_cr_note_number) AS transactionDescription,
						   
							0 AS dr_amount,
							visit_credit_note_item.visit_cr_note_amount*visit_credit_note_item.visit_cr_note_units  AS cr_amount,
						    visit_invoice.created AS `transactionDate`,
						    visit_credit_note.created AS `createdAt`,
						    1 AS `status`,
						    2 AS `branch_id`,
						    'INCOME CREDIT NOTE' AS `transactionCategory`,
						    'INCOME CREDIT NOTE' AS `transactionClassification`,
						    'visit_invoice' AS `transactionTable`,
						    'visit_credit_note' AS `referenceTable`
						FROM
							visit_credit_note_item,visit_credit_note,visit_invoice,service_charge,service,account,account_type
							WHERE visit_credit_note.visit_cr_note_delete = 0 AND visit_credit_note_item.visit_cr_note_item_delete = 0 

							AND visit_invoice.visit_invoice_id = visit_credit_note.visit_invoice_id 
							AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id 
							AND service_charge.service_charge_id = visit_credit_note_item.service_charge_id
							AND service.service_id = service_charge.service_id
							AND account.account_id = ".$accounts_receivable_id."
							AND account_type.account_type_id = account.account_type_id


							UNION ALL


							SELECT

								payments.payment_id AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								payments.confirm_number AS `referenceCode`,
								payments.confirm_number AS `transactionCode`,
								payments.patient_id AS `patient_id`,
							  	'' AS `recepientId`,
								account_type.account_type_id AS `accountParentId`,
								account_type.account_type_name AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number) AS `transactionDescription`,
								SUM(payment_item.payment_item_amount) AS `dr_amount`,
								0 AS `cr_amount`,
								payments.payment_date AS `transactionDate`,
								payments.payment_date AS `createdAt`,
								`payments`.`cancel` AS `status`,
								2 AS `branch_id`,
								'Patient Income' AS `transactionCategory`,
								'Payments' AS `transactionClassification`,
								'account_payments' AS `transactionTable`,
								'' AS `referenceTable`
							FROM
								payments,payment_item,payment_method,account,visit_invoice,account_type
								WHERE payments.cancel = 0 AND payments.payment_type = 1 AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND payment_item.invoice_type = 1
								AND payments.payment_id = payment_item.payment_id 
								AND payments.payment_method_id = payment_method.payment_method_id
								AND payment_method.account_id = account.account_id
								AND account.account_type_id = account_type.account_type_id
							GROUP BY payment_item.payment_id


							UNION ALL 


							SELECT

								payments.payment_id AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								payments.confirm_number AS `referenceCode`,
								payments.confirm_number AS `transactionCode`,
								payments.patient_id AS `patient_id`,
							  	'' AS `recepientId`,
								account_type.account_type_id AS `accountParentId`,
								account_type.account_type_name AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								account.parent_account AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Payment On Account') AS `transactionDescription`,
								SUM(payment_item.payment_item_amount) AS `dr_amount`,
								0 AS `cr_amount`,
								payments.payment_date AS `transactionDate`,
								payments.payment_date AS `createdAt`,
								`payments`.`cancel` AS `status`,
								2 AS `branch_id`,
								'Patient Income' AS `transactionCategory`,
								'Payments' AS `transactionClassification`,
								'account_payments' AS `transactionTable`,
								'' AS `referenceTable`
							FROM
								payments,payment_item,payment_method,account,account_type
								WHERE payments.cancel = 0 AND payments.payment_type = 1 AND payment_item.invoice_type = 3
								AND payments.payment_id = payment_item.payment_id 
								AND payments.payment_method_id = payment_method.payment_method_id
								AND payment_method.account_id = account.account_id
								AND account.account_type_id = account_type.account_type_id
							GROUP BY payment_item.payment_id


							UNION ALL

						SELECT

							payments.payment_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							payments.confirm_number AS `referenceCode`,
							payments.confirm_number AS `transactionCode`,
							payments.patient_id AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.account_name AS `accountName`,
							'' AS `transactionName`,
							CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number) AS `transactionDescription`,
							0 AS `dr_amount`,
							SUM(payment_item.payment_item_amount) AS `cr_amount`,
							payments.payment_date AS `transactionDate`,
							payments.payment_date AS `createdAt`,
							`payments`.`cancel` AS `status`,
							2 AS `branch_id`,
							'Patient Income' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							payments,payment_item,payment_method,account,visit_invoice,account_type
							WHERE payments.cancel = 0 AND payments.payment_type = 1 AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND payment_item.invoice_type = 1
							AND payments.payment_id = payment_item.payment_id 
							AND payments.payment_method_id = payment_method.payment_method_id
							AND account.account_id = ".$accounts_receivable_id."
							AND account.account_type_id = account_type.account_type_id
						GROUP BY payment_item.payment_id


						UNION ALL 


						SELECT

							payments.payment_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							payments.confirm_number AS `referenceCode`,
							payments.confirm_number AS `transactionCode`,
							payments.patient_id AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.parent_account AS `accountName`,
							'' AS `transactionName`,
							CONCAT('Payment On Account') AS `transactionDescription`,
							0 AS `dr_amount`,
							SUM(payment_item.payment_item_amount) AS `cr_amount`,
							payments.payment_date AS `transactionDate`,
							payments.payment_date AS `createdAt`,
							`payments`.`cancel` AS `status`,
							2 AS `branch_id`,
							'Patient Income' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							payments,payment_item,payment_method,account,account_type
							WHERE payments.cancel = 0 AND payments.payment_type = 1 AND payment_item.invoice_type = 3
							AND payments.payment_id = payment_item.payment_id 
							AND payments.payment_method_id = payment_method.payment_method_id
							AND account.account_id = ".$accounts_receivable_id."
							AND account.account_type_id = account_type.account_type_id
						GROUP BY payment_item.payment_id




						UNION ALL

						SELECT
								`provider`.`provider_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							    `provider`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
				  				`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider`.`opening_balance` AS `cr_amount`,
								`provider`.`start_date` AS `transactionDate`,
								`provider`.`start_date` AS `createdAt`,
								`provider`.`provider_status` AS `status`,
								2 AS `branch_id`,
								'Expense' AS `transactionCategory`,
								'provider Opening Balance' AS `transactionClassification`,
								'' AS `transactionTable`,
								'provider' AS `referenceTable`
							FROM
							provider,account,account_type
							WHERE  debit_id = 1 AND account.account_id = ".$provider_opening_balance_id." AND account.account_type_id = account_type.account_type_id

							UNION ALL


							SELECT
								`provider`.`provider_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							    `provider`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
				  				`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
								`provider`.`opening_balance` AS `dr_amount`,
								0 AS `cr_amount`,
								`provider`.`start_date` AS `transactionDate`,
								`provider`.`start_date` AS `createdAt`,
								`provider`.`provider_status` AS `status`,
								2 AS `branch_id`,
								'Expense' AS `transactionCategory`,
								'provider Opening Balance' AS `transactionClassification`,
								'' AS `transactionTable`,
								'provider' AS `referenceTable`
							FROM
							provider,account,account_type
							WHERE  debit_id = 1 AND account.account_id = ".$providers_liability_id." AND account.account_type_id = account_type.account_type_id


							UNION ALL


							SELECT
								`provider`.`provider_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							    `provider`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
				  				`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
								`provider`.`opening_balance` AS `dr_amount`,
								0 AS `cr_amount`,
								`provider`.`start_date` AS `transactionDate`,
								`provider`.`start_date` AS `createdAt`,
								`provider`.`provider_status` AS `status`,
								2 AS `branch_id`,
								'Expense' AS `transactionCategory`,
								'provider Opening Balance' AS `transactionClassification`,
								'' AS `transactionTable`,
								'provider' AS `referenceTable`
							FROM
							provider,account,account_type
							WHERE  debit_id = 2 AND account.account_id = ".$provider_opening_balance_id." AND account.account_type_id = account_type.account_type_id



							UNION ALL


							SELECT
								`provider`.`provider_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							    `provider`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
				  				`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider`.`opening_balance` AS `cr_amount`,
								`provider`.`start_date` AS `transactionDate`,
								`provider`.`start_date` AS `createdAt`,
								`provider`.`provider_status` AS `status`,
								2 AS `branch_id`,
								'Expense' AS `transactionCategory`,
								'provider Opening Balance' AS `transactionClassification`,
								'' AS `transactionTable`,
								'provider' AS `referenceTable`
							FROM
							provider,account,account_type
							WHERE  debit_id = 2 AND account.account_id = ".$providers_liability_id." AND account.account_type_id = account_type.account_type_id

						  UNION ALL

							SELECT
								`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
								`provider_invoice`.`provider_invoice_id` AS `referenceId`,
								'' AS `payingFor`,
								`provider_invoice`.`invoice_number` AS `referenceCode`,
								`provider_invoice`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  `provider_invoice`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`provider_invoice_item`.`account_to_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
								`provider_invoice_item`.`total_amount` AS `dr_amount`,
								'0' AS `cr_amount`,
								`provider_invoice`.`transaction_date` AS `transactionDate`,
								`provider_invoice`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
								'Expense' AS `transactionCategory`,
								'providers Invoices' AS `transactionClassification`,
								'provider_invoice_item' AS `transactionTable`,
								'provider_invoice' AS `referenceTable`
							FROM
								(
									(
										(
											`provider_invoice_item`,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = provider_invoice_item.account_to_id
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
							provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
							AND provider_invoice.provider_invoice_status = 1
							AND provider.provider_id = provider_invoice.provider_id 
							AND provider_invoice.transaction_date >= provider.start_date

						UNION ALL

						SELECT
								`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
								`provider_invoice`.`provider_invoice_id` AS `referenceId`,
								'' AS `payingFor`,
								`provider_invoice`.`invoice_number` AS `referenceCode`,
								`provider_invoice`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  `provider_invoice`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider_invoice_item`.`total_amount` AS `cr_amount`,
								`provider_invoice`.`transaction_date` AS `transactionDate`,
								`provider_invoice`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
								'Provider Liability' AS `transactionCategory`,
								'providers Invoices' AS `transactionClassification`,
								'provider_invoice_item' AS `transactionTable`,
								'provider_invoice' AS `referenceTable`
							FROM
								(
									(
										(
											`provider_invoice_item`,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = ".$providers_liability_id."
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
							provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
							AND provider_invoice.provider_invoice_status = 1
							AND provider.provider_id = provider_invoice.provider_id 
							AND provider_invoice.transaction_date >= provider.start_date


							UNION ALL

							SELECT
								`provider_credit_note_item`.`provider_credit_note_item_id` AS `transactionId`,
								`provider_credit_note`.`provider_credit_note_id` AS `referenceId`,
								`provider_credit_note_item`.`provider_invoice_id` AS `payingFor`,
								`provider_credit_note`.`invoice_number` AS `referenceCode`,
								`provider_credit_note`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  	`provider_credit_note`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`provider_credit_note`.`account_from_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider_credit_note_item`.`credit_note_amount` AS `cr_amount`,
								`provider_credit_note`.`transaction_date` AS `transactionDate`,
								`provider_credit_note`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_credit_note_item`.`provider_credit_note_item_status` AS `status`,
								'Provider Credit Note' AS `transactionCategory`,
								'Providers Credit Notes' AS `transactionClassification`,
								'provider_credit_note' AS `transactionTable`,
								'provider_credit_note_item' AS `referenceTable`
							FROM
								(
									(
										(
											`provider_credit_note_item`,provider_credit_note,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = provider_credit_note.account_from_id
											)
										)
										
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
								provider_credit_note.provider_credit_note_id = provider_credit_note_item.provider_credit_note_id 
								AND provider_credit_note.provider_credit_note_status = 1
								AND provider_invoice.provider_invoice_id = provider_credit_note_item.provider_invoice_id
								AND provider_invoice.provider_invoice_status = 1
								AND provider.provider_id = provider_invoice.provider_id 
								AND provider_invoice.transaction_date >= provider.start_date


							UNION ALL

							SELECT
								`provider_credit_note_item`.`provider_credit_note_item_id` AS `transactionId`,
								`provider_credit_note`.`provider_credit_note_id` AS `referenceId`,
								`provider_credit_note_item`.`provider_invoice_id` AS `payingFor`,
								`provider_credit_note`.`invoice_number` AS `referenceCode`,
								`provider_credit_note`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  	`provider_credit_note`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionDescription`,
								`provider_credit_note_item`.`credit_note_amount` AS `dr_amount`,
								0 AS `cr_amount`,
								`provider_credit_note`.`transaction_date` AS `transactionDate`,
								`provider_credit_note`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_credit_note_item`.`provider_credit_note_item_status` AS `status`,
								'Provider Credit Note' AS `transactionCategory`,
								'Providers Credit Notes' AS `transactionClassification`,
								'provider_credit_note' AS `transactionTable`,
								'provider_credit_note_item' AS `referenceTable`
							FROM
								(
									(
										(
											`provider_credit_note_item`,provider_credit_note,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = ".$providers_liability_id."
											)
										)
										
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
								provider_credit_note.provider_credit_note_id = provider_credit_note_item.provider_credit_note_id 
								AND provider_credit_note.provider_credit_note_status = 1
								AND provider_invoice.provider_invoice_id = provider_credit_note_item.provider_invoice_id
								AND provider_invoice.provider_invoice_status = 1
								AND provider.provider_id = provider_invoice.provider_id 
								AND provider_invoice.transaction_date >= provider.start_date

							UNION ALL

							SELECT
									`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
									`provider_payment`.`provider_payment_id` AS `referenceId`,
									`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
									`provider_payment`.`reference_number` AS `referenceCode`,
									`provider_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`provider_payment`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`provider_payment`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`provider_payment_item`.`description` AS `transactionName`,
									CONCAT('Provider payment for invoice of ',' ',`provider_invoice`.`invoice_number`)  AS `transactionDescription`,
									0 AS `dr_amount`,
									`provider_payment_item`.`amount_paid` AS `cr_amount`,
									`provider_payment`.`transaction_date` AS `transactionDate`,
									`provider_payment`.`created` AS `createdAt`,
									`provider_invoice`.`transaction_date` AS `referenceDate`,
									`provider_payment_item`.`provider_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'providers Invoices Payments' AS `transactionClassification`,
									'provider_payment' AS `transactionTable`,
									'provider_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_payment_item`,provider_payment,provider_invoice,provider
												
											)
											JOIN account ON(
												(
													account.account_id = provider_payment.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE provider_payment_item.invoice_type = 0 
									AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
									AND provider_payment.provider_payment_status = 1
									AND provider_invoice.provider_invoice_id = provider_payment_item.provider_invoice_id 
									AND provider_invoice.provider_invoice_status = 1  
									AND provider.provider_id = provider_invoice.provider_id AND provider_invoice.transaction_date >= provider.start_date
								UNION ALL

							SELECT
									`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
									`provider_payment`.`provider_payment_id` AS `referenceId`,
									`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
									`provider_payment`.`reference_number` AS `referenceCode`,
									`provider_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`provider_payment`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									account.account_id AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`provider_payment_item`.`description` AS `transactionName`,
									CONCAT('Provider payment for invoice of ',' ',`provider_invoice`.`invoice_number`)  AS `transactionDescription`,
									`provider_payment_item`.`amount_paid` AS `dr_amount`,
									0 AS `cr_amount`,
									`provider_payment`.`transaction_date` AS `transactionDate`,
									`provider_payment`.`created` AS `createdAt`,
									`provider_invoice`.`transaction_date` AS `referenceDate`,
									`provider_payment_item`.`provider_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'providers Invoices Payments' AS `transactionClassification`,
									'provider_payment' AS `transactionTable`,
									'provider_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_payment_item`,provider_payment,provider_invoice,provider
												
											)
											JOIN account ON(
												(
													account.account_id = ".$providers_liability_id."
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE provider_payment_item.invoice_type = 0 
									AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
									AND provider_payment.provider_payment_status = 1
									AND provider_invoice.provider_invoice_id = provider_payment_item.provider_invoice_id 
									AND provider_invoice.provider_invoice_status = 1  
									AND provider.provider_id = provider_invoice.provider_id AND provider_invoice.transaction_date >= provider.start_date

								
							UNION ALL

							SELECT
									`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
									`provider_payment`.`provider_payment_id` AS `referenceId`,
									`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
									`provider_payment`.`reference_number` AS `referenceCode`,
									`provider_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  `provider_payment`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`provider_payment`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`provider_payment_item`.`description` AS `transactionName`,
									CONCAT('Payment of opening balance')  AS `transactionDescription`,
									0 AS `dr_amount`,
									`provider_payment_item`.`amount_paid` AS `cr_amount`,
									`provider_payment`.`transaction_date` AS `transactionDate`,
									`provider_payment`.`created` AS `createdAt`,
									`provider`.`start_date` AS `referenceDate`,
									`provider_payment_item`.`provider_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'providers Invoices Payments' AS `transactionClassification`,
									'provider_payment' AS `transactionTable`,
									'provider_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_payment_item`,provider_payment,provider
												
											)
											JOIN account ON(
												(
													account.account_id = provider_payment.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE provider_payment_item.invoice_type = 2 
									AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
									AND provider_payment.provider_payment_status = 1
									AND provider.provider_id = provider_payment_item.provider_id
									AND provider_payment.transaction_date >= provider.start_date

								UNION ALL

							SELECT
									`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
									`provider_payment`.`provider_payment_id` AS `referenceId`,
									`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
									`provider_payment`.`reference_number` AS `referenceCode`,
									`provider_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  `provider_payment`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									account.account_id AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`provider_payment_item`.`description` AS `transactionName`,
									CONCAT('Payment of opening balance')  AS `transactionDescription`,
									`provider_payment_item`.`amount_paid` AS `dr_amount`,
									0 AS `cr_amount`,
									`provider_payment`.`transaction_date` AS `transactionDate`,
									`provider_payment`.`created` AS `createdAt`,
									`provider`.`start_date` AS `referenceDate`,
									`provider_payment_item`.`provider_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'providers Invoices Payments' AS `transactionClassification`,
									'provider_payment' AS `transactionTable`,
									'provider_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_payment_item`,provider_payment,provider
												
											)
											JOIN account ON(
												(
													account.account_id = ".$providers_liability_id."
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE provider_payment_item.invoice_type = 2 
									AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
									AND provider_payment.provider_payment_status = 1
									AND provider.provider_id = provider_payment_item.provider_id
									AND provider_payment.transaction_date >= provider.start_date

								UNION ALL

								SELECT
										`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
										`provider_payment`.`provider_payment_id` AS `referenceId`,
										`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
										`provider_payment`.`reference_number` AS `referenceCode`,
										`provider_payment`.`document_number` AS `transactionCode`,
										'' AS `patient_id`,
									  `provider_payment`.`provider_id` AS `recepientId`,
										`account`.`parent_account` AS `accountParentId`,
										`account_type`.`account_type_name` AS `accountsclassfication`,
										`provider_payment`.`account_from_id` AS `accountId`,
										`account`.`account_name` AS `accountName`,
										`provider_payment_item`.`description` AS `transactionName`,
										CONCAT('Payment on account')  AS `transactionDescription`,
										0 AS `dr_amount`,
										`provider_payment_item`.`amount_paid` AS `cr_amount`,
										`provider_payment`.`transaction_date` AS `transactionDate`,
										`provider_payment`.`created` AS `createdAt`,
										`provider_payment`.`created` AS `referenceDate`,
										`provider_payment_item`.`provider_payment_item_status` AS `status`,
										'Expense Payment' AS `transactionCategory`,
										'providers Invoices Payments' AS `transactionClassification`,
										'provider_payment' AS `transactionTable`,
										'provider_payment_item' AS `referenceTable`
									FROM
										(
											(
												(
													`provider_payment_item`,provider_payment,provider
													
												)
												JOIN account ON(
													(
														account.account_id = provider_payment.account_from_id
													)
												)
											)
											JOIN `account_type` ON(
												(
													account_type.account_type_id = account.account_type_id
												)
											)
											
										)
										WHERE provider_payment_item.invoice_type = 3 AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
											AND provider_payment.provider_payment_status = 1 
											AND provider.provider_id = provider_payment.provider_id AND provider_payment.transaction_date >= provider.start_date
								UNION ALL

								SELECT
										`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
										`provider_payment`.`provider_payment_id` AS `referenceId`,
										`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
										`provider_payment`.`reference_number` AS `referenceCode`,
										`provider_payment`.`document_number` AS `transactionCode`,
										'' AS `patient_id`,
									  `provider_payment`.`provider_id` AS `recepientId`,
										`account`.`parent_account` AS `accountParentId`,
										`account_type`.`account_type_name` AS `accountsclassfication`,
										`account`.`account_id` AS `accountId`,
										`account`.`account_name` AS `accountName`,
										`provider_payment_item`.`description` AS `transactionName`,
										CONCAT('Payment on account')  AS `transactionDescription`,
										`provider_payment_item`.`amount_paid` AS `dr_amount`,
										0 AS `cr_amount`,
										`provider_payment`.`transaction_date` AS `transactionDate`,
										`provider_payment`.`created` AS `createdAt`,
										`provider_payment`.`created` AS `referenceDate`,
										`provider_payment_item`.`provider_payment_item_status` AS `status`,
										'Expense Payment' AS `transactionCategory`,
										'providers Invoices Payments' AS `transactionClassification`,
										'provider_payment' AS `transactionTable`,
										'provider_payment_item' AS `referenceTable`
									FROM
										(
											(
												(
													`provider_payment_item`,provider_payment,provider
													
												)
												JOIN account ON(
													(
														account.account_id = ".$providers_liability_id."
													)
												)
											)
											JOIN `account_type` ON(
												(
													account_type.account_type_id = account.account_type_id
												)
											)
											
										)
										WHERE provider_payment_item.invoice_type = 3 AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
											AND provider_payment.provider_payment_status = 1 
											AND provider.provider_id = provider_payment.provider_id AND provider_payment.transaction_date >= provider.start_date
								

							
							UNION ALL


							SELECT
								`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
								`provider_invoice`.`provider_invoice_id` AS `referenceId`,
								'' AS `payingFor`,
								`provider_invoice`.`invoice_number` AS `referenceCode`,
								`provider_invoice`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  `provider_invoice`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider_invoice_item`.`vat_amount` AS `cr_amount`,
								`provider_invoice`.`transaction_date` AS `transactionDate`,
								`provider_invoice`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
								'Provider WHT' AS `transactionCategory`,
								'providers Invoices' AS `transactionClassification`,
								'provider_invoice_item' AS `transactionTable`,
								'provider_invoice' AS `referenceTable`
							FROM
								(
									(
										(
											`provider_invoice_item`,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = ".$providers_wht_id."
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
							provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
							AND provider_invoice.provider_invoice_status = 1
							AND provider.provider_id = provider_invoice.provider_id 
							AND provider_invoice.transaction_date >= provider.start_date
							AND provider_invoice_item.vat_amount > 0

					
							UNION ALL




							SELECT
								`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
								`provider_invoice`.`provider_invoice_id` AS `referenceId`,
								'' AS `payingFor`,
								`provider_invoice`.`invoice_number` AS `referenceCode`,
								`provider_invoice`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  `provider_invoice`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
								`provider_invoice_item`.`vat_amount` AS `dr_amount`,
								0  AS `cr_amount`,
								`provider_invoice`.`transaction_date` AS `transactionDate`,
								`provider_invoice`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
								'Provider WHT' AS `transactionCategory`,
								'providers Invoices' AS `transactionClassification`,
								'provider_invoice_item' AS `transactionTable`,
								'provider_invoice' AS `referenceTable`
							FROM
								(
									(
										(
											`provider_invoice_item`,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = ".$providers_liability_id."
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
							provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
							AND provider_invoice.provider_invoice_status = 1
							AND provider.provider_id = provider_invoice.provider_id 
							AND provider_invoice.transaction_date >= provider.start_date
							AND provider_invoice_item.vat_amount > 0

							

							UNION ALL

								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payroll') AS `transactionName`,
									CONCAT('Payroll') AS `transactionDescription`,
									payroll_summary.total_payroll AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'Payroll' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 1
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id


								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('PAYE') AS `transactionName`,
									CONCAT('PAYE') AS `transactionDescription`,
									payroll_summary.paye AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 2
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id

								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NSSF') AS `transactionName`,
									CONCAT('NSSF') AS `transactionDescription`,
									payroll_summary.nssf AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 3
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id


								UNION ALL




								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NHIF') AS `transactionName`,
									CONCAT('NHIF') AS `transactionDescription`,
									payroll_summary.nhif AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'NHIF' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 4
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id



							UNION ALL


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payroll') AS `transactionName`,
									CONCAT('Payroll') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.total_payroll AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'Payroll' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 1
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id


								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('PAYE') AS `transactionName`,
									CONCAT('PAYE') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.paye AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 2
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id

								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NSSF') AS `transactionName`,
									CONCAT('NSSF') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.nssf AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 3
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id


								UNION ALL




								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NHIF') AS `transactionName`,
									CONCAT('NHIF') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.nhif AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'NHIF' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 4
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id

							UNION ALL

							SELECT
							    `payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
							    `payroll_payment`.`payroll_payment_id` AS `referenceId`,
							    `payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
							    `payroll_payment`.`reference_number` AS `referenceCode`,
							    `payroll_payment`.`document_number` AS `transactionCode`,
							    '' AS `patient_id`,
							    `statutory_accounts`.`statutory_account_id` AS `recepientId`,
							    `account`.`parent_account` AS `accountParentId`,
							    `account_type`.`account_type_name` AS `accountsclassfication`,
							    `payroll_payment`.`account_from_id` AS `accountId`,
							    `account`.`account_name` AS `accountName`,
							    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
							    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for))   AS `transactionDescription`,
							    0 AS `dr_amount`,
							    `payroll_payment_item`.`amount_paid` AS `cr_amount`,
							    `payroll_payment`.`transaction_date` AS `transactionDate`,
							    `payroll_payment`.`created` AS `createdAt`,
							    `payroll_payment_item`.`payroll_payment_item_status` AS `status`,
							    2 AS `branch_id`,
							    'Payroll Payment' AS `transactionCategory`,
							    'Creditors Invoices Payments' AS `transactionClassification`,
							    'payroll_payment' AS `transactionTable`,
							    'payroll_payment_item' AS `referenceTable`
							FROM
							    (
							      (
							        (
							          `payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts
							          
							        )
							        JOIN account ON(
							          (
							            account.account_id = payroll_payment.account_from_id
							          )
							        )
							      )
							      JOIN `account_type` ON(
							        (
							          account_type.account_type_id = account.account_type_id
							        )
							      )
							      
							    )
							    WHERE payroll_payment_item.invoice_type = 0 
							    AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
							    AND payroll_payment.payroll_payment_status = 1
							    AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_payment_id 
							    AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id

							UNION ALL

								SELECT
								    `payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
								    `payroll_payment`.`payroll_payment_id` AS `referenceId`,
								    `payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
								    `payroll_payment`.`reference_number` AS `referenceCode`,
								    `payroll_payment`.`document_number` AS `transactionCode`,
								    '' AS `patient_id`,
								    `statutory_accounts`.`statutory_account_id` AS `recepientId`,
								    `account`.`parent_account` AS `accountParentId`,
								    `account_type`.`account_type_name` AS `accountsclassfication`,
								    account.account_id AS `accountId`,
								    `account`.`account_name` AS `accountName`,
								    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
								    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for))   AS `transactionDescription`,
								    `payroll_payment_item`.`amount_paid` AS `dr_amount`,
								    0 AS `cr_amount`,
								    `payroll_payment`.`transaction_date` AS `transactionDate`,
								    `payroll_payment`.`created` AS `createdAt`,
								    `payroll_payment_item`.`payroll_payment_item_status` AS `status`,
								    2 AS `branch_id`,
								    'Payroll Payment' AS `transactionCategory`,
								    'Creditors Invoices Payments' AS `transactionClassification`,
								    'payroll_payment' AS `transactionTable`,
								    'payroll_payment_item' AS `referenceTable`
								  FROM
								    (
								      (
								        (
								          `payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts
								          
								        )
								        JOIN account ON(
								          (
								            account.account_id = ".$payroll_liability_id."
								          )
								        )
								      )
								      JOIN `account_type` ON(
								        (
								          account_type.account_type_id = account.account_type_id
								        )
								      )
								      
								    )
								    WHERE payroll_payment_item.invoice_type = 0 
								    AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
								    AND payroll_payment.payroll_payment_status = 1
								    AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_invoice_id 
								    AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id


								UNION ALL

								 SELECT
									asset_category.asset_category_id AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									assets_details.asset_serial_no AS `referenceCode`,
									assets_details.asset_serial_no AS `transactionCode`,
									assets_details.asset_id AS `patient_id`,
								  	assets_details.supplier_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
									CONCAT(assets_details.asset_name) AS `transactionName`,
									CONCAT(assets_details.asset_name) AS `transactionDescription`,
									(assets_details.asset_value) AS `dr_amount`,
									0 AS `cr_amount`,
									assets_details.asset_pd_period AS `transactionDate`,
									assets_details.asset_pd_period  AS `createdAt`,
									assets_details.asset_pd_period AS `referenceDate`,
									1 AS `status`,
									'FIXED ASSET' AS `transactionCategory`,
									'Asset Purchase' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
									assets_details,asset_category,account,account_type
									WHERE 
									assets_details.asset_category_id = asset_category.asset_category_id 
									AND account.account_id = asset_category.account_id
									AND account.account_type_id = account_type.account_type_id

								UNION ALL


								SELECT
									asset_category.asset_category_id AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									assets_details.asset_serial_no AS `referenceCode`,
									assets_details.asset_serial_no AS `transactionCode`,
									assets_details.asset_id AS `patient_id`,
								  	assets_details.supplier_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
									CONCAT(assets_details.asset_name) AS `transactionName`,
									CONCAT(assets_details.asset_name) AS `transactionDescription`,
									0 AS `dr_amount`,
									(assets_details.asset_value) AS `cr_amount`,
									assets_details.asset_pd_period AS `transactionDate`,
									assets_details.asset_pd_period  AS `createdAt`,
									assets_details.asset_pd_period AS `referenceDate`,
									1 AS `status`,
									'FIXED ASSET' AS `transactionCategory`,
									'Asset Purchase' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
									assets_details,asset_category,account,account_type
									WHERE 
									assets_details.asset_category_id = asset_category.asset_category_id 
									AND assets_details.bill_asset = 1
									AND account.account_id = ".$accounts_payable_id."
									AND account.account_type_id = account_type.account_type_id
									

							UNION ALL

								SELECT


									patients_journals.patient_journal_id AS transactionId,
									patients_journals.patient_journal_id AS referenceId,
									'' AS `payingFor`,
									'' AS referenceCode,
									'' AS `transactionCode`,
									patients_journals.patient_id AS patient_id,
									patients_journals.patient_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									account.account_name AS `accountName`,
									CONCAT('Patient Journals') AS `transactionName`,
									CONCAT('Patient Journals') AS `transactionDescription`,
									patients_journals.journal_amount AS `dr_amount`,
									0 AS `cr_amount`,
									patients_journals.journal_date AS `transactionDate`,
									patients_journals.journal_date  AS `createdAt`,
									patients_journals.journal_date AS `referenceDate`,
									1 AS `status`,
									'JOURNAL' AS `transactionCategory`,
									'JOURNAL' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
										patients_journals,visit_type,account,account_type
									WHERE 
									patients_journals.visit_type_id = visit_type.visit_type_id 
									AND patients_journals.visit_invoice_id > 0
									AND patients_journals.account_from_id = account.account_id
									AND account_type.account_type_id = account.account_type_id

							UNION ALL

								SELECT


									patients_journals.patient_journal_id AS transactionId,
									patients_journals.patient_journal_id AS referenceId,
									'' AS `payingFor`,
									'' AS referenceCode,
									'' AS `transactionCode`,
									patients_journals.patient_id AS patient_id,
									patients_journals.patient_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									account.account_name AS `accountName`,
									CONCAT('Patient Journals') AS `transactionName`,
									CONCAT('Patient Journals') AS `transactionDescription`,
									0 AS `dr_amount`,
									patients_journals.journal_amount AS `cr_amount`,
									patients_journals.journal_date AS `transactionDate`,
									patients_journals.journal_date  AS `createdAt`,
									patients_journals.journal_date AS `referenceDate`,
									1 AS `status`,
									'JOURNAL' AS `transactionCategory`,
									'JOURNAL' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
										patients_journals,visit_type,account,account_type
									WHERE 
									patients_journals.visit_type_id = visit_type.visit_type_id 
									AND patients_journals.visit_invoice_id > 0
									AND patients_journals.account_to_id = account.account_id
									AND account_type.account_type_id = account.account_type_id

								UNION ALL

								SELECT
									`loan_invoice_item`.`loan_invoice_item_id` AS `transactionId`,
									`loan_invoice`.`loan_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`loan_invoice`.`invoice_number` AS `referenceCode`,
									`loan_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_invoice`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`loan_invoice_item`.`item_description` AS `transactionName`,
									`loan_invoice_item`.`item_description` AS `transactionDescription`,
									 0 AS `dr_amount`,
									`loan_invoice_item`.`total_amount` AS `cr_amount`,
									`loan_invoice`.`transaction_date` AS `transactionDate`,
									`loan_invoice`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_invoice_item`.`loan_invoice_item_status` AS `status`,
									'Expense' AS `transactionCategory`,
									'loans Invoices' AS `transactionClassification`,
									'loan_invoice_item' AS `transactionTable`,
									'loan_invoice' AS `referenceTable`
								FROM
									(
										(
											(
												`loan_invoice_item`,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
									)
								WHERE 
								loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id 
								AND loan_invoice.loan_invoice_status = 1
								AND loan.loan_id = loan_invoice.loan_id 
								AND loan_invoice.transaction_date >= loan.start_date

							UNION ALL

								SELECT
									`loan_invoice_item`.`loan_invoice_item_id` AS `transactionId`,
									`loan_invoice`.`loan_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`loan_invoice`.`invoice_number` AS `referenceCode`,
									`loan_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_invoice`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan`.`account_to_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`loan_invoice_item`.`item_description` AS `transactionName`,
									`loan_invoice_item`.`item_description` AS `transactionDescription`,
									`loan_invoice_item`.`total_amount` AS `dr_amount`,
									'0' AS `cr_amount`,
									`loan_invoice`.`transaction_date` AS `transactionDate`,
									`loan_invoice`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_invoice_item`.`loan_invoice_item_status` AS `status`,
									'Expense' AS `transactionCategory`,
									'loans Invoices' AS `transactionClassification`,
									'loan_invoice_item' AS `transactionTable`,
									'loan_invoice' AS `referenceTable`
								FROM
									(
										(
											(
												`loan_invoice_item`,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan.account_to_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
									)
								WHERE 
								loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id 
								AND loan_invoice.loan_invoice_status = 1
								AND loan.loan_id = loan_invoice.loan_id 
								AND loan_invoice.transaction_date >= loan.start_date

								UNION ALL

								SELECT
									`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
									`loan_payment`.`loan_payment_id` AS `referenceId`,
									`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
									`loan_payment`.`reference_number` AS `referenceCode`,
									`loan_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_payment`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan_payment`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
									CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
									0 AS `dr_amount`,
									`loan_payment_item`.`amount_paid` AS `cr_amount`,
									`loan_payment`.`transaction_date` AS `transactionDate`,
									`loan_payment`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_payment_item`.`loan_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'loans Invoices Payments' AS `transactionClassification`,
									'loan_payment' AS `transactionTable`,
									'loan_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`loan_payment_item`,loan_payment,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan_payment.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE loan_payment_item.invoice_type = 0 
									AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
									AND loan_payment.loan_payment_status = 1
									AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
									AND loan_invoice.loan_invoice_status = 1  
									AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
									AND loan.account_from_id = loan_payment_item.account_to_id
									
							UNION ALL
							
							SELECT
									`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
									`loan_payment`.`loan_payment_id` AS `referenceId`,
									`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
									`loan_payment`.`reference_number` AS `referenceCode`,
									`loan_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_payment`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
									CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
									`loan_payment_item`.`amount_paid` AS `dr_amount`,
									0 AS `cr_amount`,
									`loan_payment`.`transaction_date` AS `transactionDate`,
									`loan_payment`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_payment_item`.`loan_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'loans Invoices Payments' AS `transactionClassification`,
									'loan_payment' AS `transactionTable`,
									'loan_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`loan_payment_item`,loan_payment,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE loan_payment_item.invoice_type = 0 
									AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
									AND loan_payment.loan_payment_status = 1
									AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
									AND loan_invoice.loan_invoice_status = 1  
									AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
									AND loan.account_from_id = loan_payment_item.account_to_id

									UNION ALL

									SELECT
											`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
											`loan_payment`.`loan_payment_id` AS `referenceId`,
											`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
											`loan_payment`.`reference_number` AS `referenceCode`,
											`loan_payment`.`document_number` AS `transactionCode`,
											'' AS `patient_id`,
												`loan_payment`.`loan_id` AS `recepientId`,
											`account`.`parent_account` AS `accountParentId`,
											`account_type`.`account_type_name` AS `accountsclassfication`,
											loan_payment_item.account_to_id AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
											CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
											`loan_payment_item`.`amount_paid` AS `dr_amount`,
											0 AS `cr_amount`,
											`loan_payment`.`transaction_date` AS `transactionDate`,
											`loan_payment`.`created` AS `createdAt`,
											`loan_invoice`.`transaction_date` AS `referenceDate`,
											`loan_payment_item`.`loan_payment_item_status` AS `status`,
											'Expense Payment' AS `transactionCategory`,
											'loans Invoices Payments' AS `transactionClassification`,
											'loan_payment' AS `transactionTable`,
											'loan_payment_item' AS `referenceTable`
										FROM
											(
												(
													(
														`loan_payment_item`,loan_payment,loan_invoice,loan,account
														
													)
												
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
											WHERE loan_payment_item.invoice_type = 0 
											AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
											AND loan_payment.loan_payment_status = 1
											AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
											AND loan_invoice.loan_invoice_status = 1  
											AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
											AND loan.account_from_id <> loan_payment_item.account_to_id
											AND account.account_id = loan_payment_item.account_to_id
											
											UNION ALL
											
											
											SELECT
											`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
											`loan_payment`.`loan_payment_id` AS `referenceId`,
											`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
											`loan_payment`.`reference_number` AS `referenceCode`,
											`loan_payment`.`document_number` AS `transactionCode`,
											'' AS `patient_id`,
												`loan_payment`.`loan_id` AS `recepientId`,
											`account`.`parent_account` AS `accountParentId`,
											`account_type`.`account_type_name` AS `accountsclassfication`,
											loan_payment.account_from_id AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
											CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
											0 AS `dr_amount`,
											`loan_payment_item`.`amount_paid` AS `cr_amount`,
											`loan_payment`.`transaction_date` AS `transactionDate`,
											`loan_payment`.`created` AS `createdAt`,
											`loan_invoice`.`transaction_date` AS `referenceDate`,
											`loan_payment_item`.`loan_payment_item_status` AS `status`,
											'Expense Payment' AS `transactionCategory`,
											'loans Invoices Payments' AS `transactionClassification`,
											'loan_payment' AS `transactionTable`,
											'loan_payment_item' AS `referenceTable`
										FROM
											(
												(
													(
														`loan_payment_item`,loan_payment,loan_invoice,loan,account
														
													)
												
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
											WHERE loan_payment_item.invoice_type = 0 
											AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
											AND loan_payment.loan_payment_status = 1
											AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
											AND loan_invoice.loan_invoice_status = 1  
											AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
											AND loan.account_from_id <> loan_payment_item.account_to_id
											AND account.account_id = loan_payment.account_from_id

										UNION ALL


										SELECT
											asset_category.asset_category_id AS `transactionId`,
											'' AS `referenceId`,
											'' AS `payingFor`,
											assets_details.asset_serial_no AS `referenceCode`,
											assets_details.asset_serial_no AS `transactionCode`,
											assets_details.asset_id AS `patient_id`,
												assets_details.supplier_id AS `recepientId`,
											account_type.account_type_id AS `accountParentId`,
											account_type.account_type_name AS `accountsclassfication`,
											`account`.`account_id` AS `accountId`,
											CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
											CONCAT(assets_details.asset_name) AS `transactionName`,
											CONCAT(assets_details.asset_name) AS `transactionDescription`,
											(asset_amortization.interest_amount) AS `dr_amount`,
											0 AS `cr_amount`,
											asset_amortization.date_approved AS `transactionDate`,
											asset_amortization.date_approved  AS `createdAt`,
											asset_amortization.date_approved AS `referenceDate`,
											1 AS `status`,
											'FIXED ASSET' AS `transactionCategory`,
											'ASSET DEPRECIATION' AS `transactionClassification`,
											'account_payments' AS `transactionTable`,
											'' AS `referenceTable`
										FROM
											assets_details,asset_category,account,account_type,asset_amortization
											WHERE 
											assets_details.asset_category_id = asset_category.asset_category_id 
											AND asset_amortization.expense_account_id = account.account_id
											AND account.account_type_id = account_type.account_type_id
											AND asset_amortization.asset_id = assets_details.asset_id
											AND asset_amortization.bill_status = 1
											
									UNION ALL

									SELECT
											asset_category.asset_category_id AS `transactionId`,
											'' AS `referenceId`,
											'' AS `payingFor`,
											assets_details.asset_serial_no AS `referenceCode`,
											assets_details.asset_serial_no AS `transactionCode`,
											assets_details.asset_id AS `patient_id`,
												assets_details.supplier_id AS `recepientId`,
											account_type.account_type_id AS `accountParentId`,
											account_type.account_type_name AS `accountsclassfication`,
											`account`.`account_id` AS `accountId`,
											CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
											CONCAT(assets_details.asset_name) AS `transactionName`,
											CONCAT(assets_details.asset_name) AS `transactionDescription`,
											0 AS `dr_amount`,
											(asset_amortization.interest_amount) AS `cr_amount`,
											asset_amortization.date_approved AS `transactionDate`,
											asset_amortization.date_approved  AS `createdAt`,
											asset_amortization.date_approved AS `referenceDate`,
											1 AS `status`,
											'FIXED ASSET' AS `transactionCategory`,
											'ASSET DEPRECIATION' AS `transactionClassification`,
											'account_payments' AS `transactionTable`,
											'' AS `referenceTable`
										FROM
											assets_details,asset_category,account,account_type,asset_amortization
											WHERE 
											assets_details.asset_category_id = asset_category.asset_category_id 
											AND asset_amortization.acc_depreciation_id = account.account_id
											AND account.account_type_id = account_type.account_type_id
											AND asset_amortization.asset_id = assets_details.asset_id
											AND asset_amortization.bill_status = 1

					) AS data WHERE data.accountId in ('".implode("','", $array_leads)."')  ".$add." ".$add_item."  ORDER BY data.transactionDate ASC

					";
	 

	  $query = $this->db->query($select);


	   return $query;

	}

	public function get_transactions_profit_and_loss($array_leads,$checked = NULL,$date_from=NULL,$date_to = NULL)
	{
		
		
		$financial_year_date = date('Y')."-12-31";


		$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

		$patients = array();
		
		if($accounts_config_rs->num_rows() > 0)
		{
			foreach ($accounts_config_rs->result() as $key => $value) {
				// code...
				$staing_account_id = $value->account_id;
				$reference_name = $value->reference_name;

				$session_account[$reference_name] = $staing_account_id;

				
			}
		}

		$providers_liability_id = $session_account['providers_liability_id'];
		$providers_wht_id = $session_account['providers_wht_id'];
		$payroll_liability_id = $session_account['payroll_liability_id'];
		$accounts_payable_id = $session_account['accounts_payable_id'];
		$fixed_assets_id = $session_account['fixed_assets_id'];
		$accounts_receivable_id = $session_account['accounts_receivable_id'];
		$suppliers_wht_id = $session_account['suppliers_wht_id'];
		$income_account_id = $session_account['income_account_id'];
		$supplier_opening_balance_id= $session_account['supplier_opening_balance_id'];
		$bank_opening_balance_id = $session_account['bank_opening_balance_id'];

		$provider_opening_balance_id = $session_account['provider_opening_balance_id'];

		// $financial_year_date = "2021-12-31";
		// $date_from = $this->session->userdata('date_from_general_ledger');
		// $date_to = $this->session->userdata('date_to_general_ledger');
		$add = '';

		if(!empty($date_from) OR !empty($date_to))
    	{
    		
    		$add =  ' DATE(data.transactionDate) >= \''.$date_from.'\' AND DATE(data.transactionDate) <= \''.$date_to.'\' AND ';
    		
    		
    	}
    	
    	

    		
    
    	
    	


    	if($checked == 1)
    	{
    		$item = '	YEAR(data.transactionDate) AS year,
    					data.accountId AS accountId,
						data.accountName AS account_name,
					 	SUM(data.dr_amount) AS dr_amount,
					 	SUM(data.cr_amount) AS cr_amount';

			$add_item = 'GROUP BY data.accountId';
    	}
    	else
    	{
    		$item = '
				 	MAX(data.transactionDate) AS max_transaction_date,
				 	MIN(data.transactionDate) AS min_transaction_date';

			$add_item = '';
    	}
		
    	
		$select  = "
					SELECT 
						".$item."
					FROM 

					(

						SELECT
							`account`.`account_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
						    '' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							account.account_id AS `accountId`,
							account.account_name AS `accountName`,
							CONCAT('Opening Balance as from',' ',`account`.`start_date`) AS `transactionName`,
							CONCAT('Opening Balance as from',' ',' ',`account`.`start_date`) AS `transactionDescription`,
							`account`.`account_opening_balance` AS `dr_amount`,
							'0' AS `cr_amount`,
							`account`.`start_date` AS `transactionDate`,
							`account`.`start_date` AS `createdAt`,
							`account`.`account_status` AS `status`,
							branch_id AS `branch_id`,
							'Income' AS `transactionCategory`,
							'Account Opening Balance' AS `transactionClassification`,
							'' AS `transactionTable`,
							'account' AS `referenceTable`
						FROM
						account,account_type

						WHERE 

						account_type.account_type_id = account.account_type_id AND account.account_opening_balance > 0 AND account.parent_account > 0  AND (account_type.account_type_name = 'Bank' OR account_type.account_type_name ='Capital')

						UNION ALL

	
						SELECT
							`account`.`account_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							account.account_id AS `accountId`,
							account.account_name AS `accountName`,
							CONCAT( 'Opening Balance as from', ' ', `account`.`start_date` ) AS `transactionName`,
							CONCAT( 'Opening Balance as from', ' ', ' ', `account`.`start_date` ) AS `transactionDescription`,
							0 AS `dr_amount`,
							(SELECT COALESCE(SUM(account_opening_balance),0) FROM account,account_type WHERE  account_type.account_type_id = account.account_type_id  AND account.account_opening_balance > 0  AND (
							account_type.account_type_name = 'Bank' 
							OR account_type.account_type_name = 'Capital') ) AS `cr_amount`,
							`account`.`start_date` AS `transactionDate`,
							`account`.`start_date` AS `createdAt`,
							`account`.`account_status` AS `status`,
							branch_id AS `branch_id`,
							'Income' AS `transactionCategory`,
							'Account Opening Balance' AS `transactionClassification`,
							'' AS `transactionTable`,
							'account' AS `referenceTable` 
						FROM
							account,
							account_type 
						WHERE
							account_type.account_type_id = account.account_type_id 
							AND account.parent_account > 0 
							AND account.account_id = ".$bank_opening_balance_id." 



						UNION ALL

						SELECT
						  	`finance_transfered`.`finance_transfered_id` AS `transactionId`,
						  	`finance_transfer`.`finance_transfer_id` AS `referenceId`,
						  	'' AS `payingFor`,
						  	`finance_transfer`.`reference_number` AS `referenceCode`,
						  	`finance_transfer`.`document_number` AS `transactionCode`,
						  	'' AS `patient_id`,
						    '' AS `recepientId`,
						  	`account`.`parent_account` AS `accountParentId`,
						  	`account_type`.`account_type_name` AS `accountsclassfication`,
						  	`finance_transfered`.`account_to_id` AS `accountId`,
						  	`account`.`account_name` AS `accountName`,
						  	`finance_transfered`.`remarks` AS `transactionName`,
						  	 CONCAT('Amount Received from ',(SELECT account_name FROM account WHERE account_id = finance_transfer.account_from_id ),' Ref. ', `finance_transfer`.`reference_number`) AS `transactionDescription`,
						  	`finance_transfer`.`finance_transfer_amount` AS `cr_amount`,
						     0 AS `cr_amount`,
						  	`finance_transfer`.`transaction_date` AS `transactionDate`,
						  	`finance_transfer`.`created` AS `createdAt`,
						  	`finance_transfer`.`finance_transfer_status` AS `status`,
						  	`finance_transfer`.`branch_id` AS `branch_id`,
						  	'Transfer' AS `transactionCategory`,
						  	'Transfer' AS `transactionClassification`,
						  	'finance_transfer' AS `transactionTable`,
						  	'finance_transfered' AS `referenceTable`
						  FROM
						  `finance_transfer`,finance_transfered,account,account_type
						   WHERE finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id 
						  	AND account.account_id = finance_transfered.account_to_id
						  	AND account_type.account_type_id = account.account_type_id
						  	AND finance_transfer.finance_transfer_deleted = 0

					UNION ALL

						SELECT
						  	`finance_transfer`.`finance_transfer_id` AS `transactionId`,
						  	`finance_transfered`.`finance_transfered_id` AS `referenceId`,
						  	'' AS `payingFor`,
						  	`finance_transfer`.`reference_number` AS `referenceCode`,
						  	`finance_transfer`.`document_number` AS `transactionCode`,
						  	'' AS `patient_id`,
						    '' AS `recepientId`,
						  	`account`.`parent_account` AS `accountParentId`,
						  	`account_type`.`account_type_name` AS `accountsclassfication`,
						  	`finance_transfer`.`account_from_id` AS `accountId`,
						  	`account`.`account_name` AS `accountName`,
						  	`finance_transfer`.`remarks` AS `transactionName`,
						  	CONCAT(' Amount Transfered to ',(SELECT account_name FROM account WHERE account_id = finance_transfered.account_to_id )) AS `transactionDescription`,
						  	0 AS `dr_amount`,
						  	`finance_transfer`.`finance_transfer_amount` AS `cr_amount`,
						  	`finance_transfer`.`transaction_date` AS `transactionDate`,
						  	`finance_transfer`.`transaction_date` AS `transactionDate`,

						  	`finance_transfer`.`finance_transfer_status` AS `status`,
						  	`finance_transfer`.`branch_id` AS `branch_id`,
						  	'Transfer' AS `transactionCategory`,
						  	'Transfer' AS `transactionClassification`,
						  	'finance_transfered' AS `transactionTable`,
						  	'finance_transfer' AS `referenceTable`
						  FROM
							`finance_transfer`,finance_transfered,account,account_type
						  				
						  WHERE finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id 
						  	AND account.account_id = finance_transfer.account_from_id
						  	AND account_type.account_type_id = account.account_type_id
						  	AND finance_transfer.finance_transfer_deleted = 0

						UNION ALL


						SELECT
							`finance_purchase_payment`.`finance_purchase_payment_id` AS `transactionId`,
							'' AS `referenceId`,
							`finance_purchase`.`finance_purchase_id` AS `payingFor`,
							`finance_purchase`.`transaction_number` AS `referenceCode`,
							`finance_purchase`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
						  	finance_purchase.creditor_id AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`finance_purchase_payment`.`account_from_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`finance_purchase`.`finance_purchase_description` AS `transactionName`,
							CONCAT( `finance_purchase`.`finance_purchase_description`) AS `transactionDescription`,
							0 AS `dr_amount`,
							`finance_purchase_payment`.`amount_paid` AS `cr_amount`,
							`finance_purchase`.`transaction_date` AS `transactionDate`,
							`finance_purchase`.`created` AS `createdAt`,
							`finance_purchase_payment`.`finance_purchase_payment_status` AS `status`,
							`finance_purchase`.`branch_id` AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Purchase Payment' AS `transactionClassification`,
							'finance_purchase' AS `transactionTable`,
							'finance_purchase_payment' AS `referenceTable`
						FROM
						`finance_purchase_payment`,finance_purchase,account,account_type
						WHERE finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id AND finance_purchase.finance_purchase_deleted = 0 AND account.account_id = finance_purchase_payment.account_from_id
							AND finance_purchase.finance_purchase_delete = 0 AND account_type.account_type_id = account.account_type_id
							AND finance_purchase.account_to_id > 0 

						UNION ALL

						SELECT
							`finance_purchase`.`finance_purchase_id` AS `transactionId`,
							'' AS `referenceId`,
							`finance_purchase`.`finance_purchase_id` AS `payingFor`,
							`finance_purchase`.`transaction_number` AS `referenceCode`,
							`finance_purchase`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`finance_purchase`.`creditor_id` AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`finance_purchase`.`account_to_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`finance_purchase`.`finance_purchase_description` AS `transactionName`,
							CONCAT( `finance_purchase`.`finance_purchase_description`)  AS `transactionDescription`,
							`finance_purchase_payment`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`finance_purchase`.`transaction_date` AS `transactionDate`,
							`finance_purchase`.`created` AS `createdAt`,
							`finance_purchase`.`finance_purchase_status` AS `status`,
							`finance_purchase`.`branch_id` AS `branch_id`,
							'Expense' AS `transactionCategory`,
							'Purchases' AS `transactionClassification`,
							'finance_purchase' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							`finance_purchase_payment`,finance_purchase,account,account_type
						WHERE finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id AND finance_purchase.finance_purchase_deleted = 0 
						AND account.account_id = finance_purchase.account_to_id
							AND finance_purchase.finance_purchase_delete = 0
							AND account_type.account_type_id = account.account_type_id
							AND finance_purchase.account_to_id > 0 

						UNION ALL

					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor`.`opening_balance` AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`
					FROM
					creditor,account,account_type
					WHERE debit_id = 1 AND account.account_id = ".$supplier_opening_balance_id." AND account.account_type_id = account_type.account_type_id

					UNION ALL

					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						`creditor`.`opening_balance` AS `dr_amount`,
						'0' AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						 creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`
					FROM
					creditor,account,account_type
					WHERE debit_id = 2 AND account.account_id = ".$supplier_opening_balance_id." AND account.account_type_id = account_type.account_type_id


					UNION ALL


					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						`creditor`.`opening_balance` AS `dr_amount`,
						0 AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`
					FROM
					creditor,account,account_type
					WHERE debit_id = 1 AND account.account_id = ".$accounts_payable_id." AND account.account_type_id = account_type.account_type_id

					UNION ALL

					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor`.`opening_balance`  AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						 creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`
					FROM
					creditor,account,account_type
					WHERE debit_id = 2 AND account.account_id = ".$accounts_payable_id." AND account.account_type_id = account_type.account_type_id
				

					UNION ALL


					SELECT
						`creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
						`creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
						'' AS `payingFor`,
						`creditor_invoice`.`invoice_number` AS `referenceCode`,
						`creditor_invoice`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
					  `creditor_invoice`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`creditor_invoice_item`.`account_to_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_invoice_item`.`item_description` AS `transactionName`,
						CONCAT(`creditor_invoice_item`.`item_description`,' ',creditor_invoice.invoice_number) AS `transactionDescription`,
						`creditor_invoice_item`.`total_amount` AS `dr_amount`,
						'0' AS `cr_amount`,
						`creditor_invoice`.`transaction_date` AS `transactionDate`,
						`creditor_invoice`.`created` AS `createdAt`,
						`creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
						'creditor.branch_id' AS `branch_id`,
						'Cost of Goods' AS `transactionCategory`,
						'Creditors Invoices' AS `transactionClassification`,
						'creditor_invoice_item' AS `transactionTable`,
						'creditor_invoice' AS `referenceTable`
					FROM
						(
							(
								(
									`creditor_invoice_item`,creditor_invoice,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = creditor_invoice_item.account_to_id
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)
						)
					WHERE 

					creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id 
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date

				UNION ALL

				  SELECT
					`creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
					`creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
					`creditor_credit_note_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_credit_note`.`invoice_number` AS `referenceCode`,
					`creditor_credit_note`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_credit_note`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`creditor_credit_note`.`account_from_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_credit_note_item`.`description` AS `transactionName`,
					`creditor_credit_note_item`.`description` AS `transactionDescription`,
					0 AS `dr_amount`,
					`creditor_credit_note_item`.`credit_note_amount` AS `cr_amount`,
					`creditor_credit_note`.`transaction_date` AS `transactionDate`,
					`creditor_credit_note`.`created` AS `createdAt`,
					`creditor_invoice`.`transaction_date` AS `referenceDate`,
					`creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
					'Supplier Credit Note' AS `transactionCategory`,
					'Creditors Credit Notes' AS `transactionClassification`,
					'creditor_credit_note' AS `transactionTable`,
					'creditor_credit_note_item' AS `referenceTable`
				FROM
					(
						(
							(
								`creditor_credit_note_item`,creditor_credit_note,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = creditor_credit_note.account_from_id
								)
							)
							
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					)
				WHERE 
					creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id 
					AND creditor_credit_note.creditor_credit_note_status = 1
					AND creditor_invoice.creditor_invoice_id = creditor_credit_note_item.creditor_invoice_id
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date


				UNION ALL


				SELECT
						`creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
						`creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
						'' AS `payingFor`,
						`creditor_invoice`.`invoice_number` AS `referenceCode`,
						`creditor_invoice`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
					  `creditor_invoice`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`account`.`account_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_invoice_item`.`item_description` AS `transactionName`,
						CONCAT(`creditor_invoice_item`.`item_description`,' ',creditor_invoice.invoice_number) AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor_invoice_item`.`total_amount` AS `cr_amount`,
						`creditor_invoice`.`transaction_date` AS `transactionDate`,
						`creditor_invoice`.`created` AS `createdAt`,
						`creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
						'creditor.branch_id' AS `branch_id`,
						'Cost of Goods' AS `transactionCategory`,
						'Creditors Invoices' AS `transactionClassification`,
						'creditor_invoice_item' AS `transactionTable`,
						'creditor_invoice' AS `referenceTable`
					FROM
						(
							(
								(
									`creditor_invoice_item`,creditor_invoice,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = ".$accounts_payable_id."
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)
						)
					WHERE 

					creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id 
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date

				UNION ALL

				  SELECT
					`creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
					`creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
					`creditor_credit_note_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_credit_note`.`invoice_number` AS `referenceCode`,
					`creditor_credit_note`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_credit_note`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`account`.`account_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_credit_note_item`.`description` AS `transactionName`,
					`creditor_credit_note_item`.`description` AS `transactionDescription`,
					`creditor_credit_note_item`.`credit_note_amount` AS `dr_amount`,
					0 AS `cr_amount`,
					`creditor_credit_note`.`transaction_date` AS `transactionDate`,
					`creditor_credit_note`.`created` AS `createdAt`,
					`creditor_invoice`.`transaction_date` AS `referenceDate`,
					`creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
					'Supplier Credit Note' AS `transactionCategory`,
					'Creditors Credit Notes' AS `transactionClassification`,
					'creditor_credit_note' AS `transactionTable`,
					'creditor_credit_note_item' AS `referenceTable`
				FROM
					(
						(
							(
								`creditor_credit_note_item`,creditor_credit_note,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = ".$accounts_payable_id."
								)
							)
							
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					)
				WHERE 
					creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id 
					AND creditor_credit_note.creditor_credit_note_status = 1
					AND creditor_invoice.creditor_invoice_id = creditor_credit_note_item.creditor_invoice_id
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date



				UNION ALL


				SELECT
					`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
					`creditor_payment`.`creditor_payment_id` AS `referenceId`,
					`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_payment`.`reference_number` AS `referenceCode`,
					`creditor_payment`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_payment`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`creditor_payment`.`account_from_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_payment_item`.`description` AS `transactionName`,
					CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
					0 AS `dr_amount`,
					`creditor_payment_item`.`amount_paid` AS `cr_amount`,
					`creditor_payment`.`transaction_date` AS `transactionDate`,
					`creditor_payment`.`created` AS `createdAt`,
					`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
					'creditor_payment.branch_id' AS `branch_id`,
					'Creditor Payment' AS `transactionCategory`,
					'Creditors Invoices Payments' AS `transactionClassification`,
					'creditor_payment' AS `transactionTable`,
					'creditor_payment_item' AS `referenceTable`
				FROM
					(
						(
							(
								`creditor_payment_item`,creditor_payment,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = creditor_payment.account_from_id
								)
							)
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					
					)
					WHERE creditor_payment_item.invoice_type = 0 
					AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
					AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
					AND creditor_payment.creditor_payment_status = 1
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor_invoice.creditor_id = creditor.creditor_id
					AND creditor_payment.transaction_date >= creditor.start_date

				UNION ALL


				SELECT
					`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
					`creditor_payment`.`creditor_payment_id` AS `referenceId`,
					`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_payment`.`reference_number` AS `referenceCode`,
					`creditor_payment`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_payment`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`account`.`account_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_payment_item`.`description` AS `transactionName`,
					CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
					`creditor_payment_item`.`amount_paid` AS `dr_amount`,
					0 AS `cr_amount`,
					`creditor_payment`.`transaction_date` AS `transactionDate`,
					`creditor_payment`.`created` AS `createdAt`,
					`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
					'creditor_payment.branch_id' AS `branch_id`,
					'Creditor Payment' AS `transactionCategory`,
					'Creditors Invoices Payments' AS `transactionClassification`,
					'creditor_payment' AS `transactionTable`,
					'creditor_payment_item' AS `referenceTable`
				FROM
					(
						(
							(
								`creditor_payment_item`,creditor_payment,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = ".$accounts_payable_id."
								)
							)
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					
					)
					WHERE creditor_payment_item.invoice_type = 0 
					AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
					AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
					AND creditor_payment.creditor_payment_status = 1
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor_invoice.creditor_id = creditor.creditor_id
					AND creditor_payment.transaction_date >= creditor.start_date

				UNION ALL

					SELECT
						`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
						`creditor_payment`.`creditor_payment_id` AS `referenceId`,
						`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
						`creditor_payment`.`reference_number` AS `referenceCode`,
						`creditor_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
						`creditor_payment`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`creditor_payment`.`account_from_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_payment_item`.`description` AS `transactionName`,
						CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor_payment_item`.`amount_paid` AS `cr_amount`,
						`creditor_payment`.`transaction_date` AS `transactionDate`,
						`creditor_payment`.`created` AS `createdAt`,
						`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
						'creditor_payment.branch_id' AS `branch_id`,
						'Creditor Payment' AS `transactionCategory`,
						'Creditors Invoices Payments' AS `transactionClassification`,
						'creditor_payment' AS `transactionTable`,
						'creditor_payment_item' AS `referenceTable`
						FROM
						(
							(
								(
									`creditor_payment_item`,creditor_payment,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = creditor_payment.account_from_id
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)

						)
						WHERE creditor_payment_item.invoice_type = 2 
						AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
						AND creditor.creditor_id = creditor_payment_item.creditor_id
						AND creditor_payment.transaction_date >= creditor.start_date

				UNION ALL

					SELECT
						`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
						`creditor_payment`.`creditor_payment_id` AS `referenceId`,
						`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
						`creditor_payment`.`reference_number` AS `referenceCode`,
						`creditor_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
						`creditor_payment`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`account`.`account_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_payment_item`.`description` AS `transactionName`,
						CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
						`creditor_payment_item`.`amount_paid` AS `dr_amount`,
						0 AS `cr_amount`,
						`creditor_payment`.`transaction_date` AS `transactionDate`,
						`creditor_payment`.`created` AS `createdAt`,
						`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
						'creditor_payment.branch_id' AS `branch_id`,
						'Creditor Payment' AS `transactionCategory`,
						'Creditors Invoices Payments' AS `transactionClassification`,
						'creditor_payment' AS `transactionTable`,
						'creditor_payment_item' AS `referenceTable`
						FROM
						(
							(
								(
									`creditor_payment_item`,creditor_payment,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = ".$accounts_payable_id."
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)

						)
						WHERE creditor_payment_item.invoice_type = 2 
						AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
						AND creditor.creditor_id = creditor_payment_item.creditor_id
						AND creditor_payment.transaction_date >= creditor.start_date

					UNION ALL

						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`creditor_payment`.`account_from_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							0 AS `dr_amount`,
							`creditor_payment_item`.`amount_paid` AS `cr_amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							'creditor_payment.branch_id' AS `branch_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`
							FROM
							(
								(
									(
										`creditor_payment_item`,creditor_payment,creditor
										
									)
									JOIN account ON(
										(
											account.account_id = creditor_payment.account_from_id
										)
									)
								)
								JOIN `account_type` ON(
									(
										account_type.account_type_id = account.account_type_id
									)
								)
								
							)
							WHERE creditor_payment_item.invoice_type = 3
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
							AND creditor.creditor_id = creditor_payment_item.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date
					UNION ALL

						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							`creditor_payment_item`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							'creditor_payment.branch_id' AS `branch_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`
							FROM
							(
								(
									(
										`creditor_payment_item`,creditor_payment,creditor
										
									)
									JOIN account ON(
										(
											account.account_id = ".$accounts_payable_id."
										)
									)
								)
								JOIN `account_type` ON(
									(
										account_type.account_type_id = account.account_type_id
									)
								)
								
							)
							WHERE creditor_payment_item.invoice_type = 3
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
							AND creditor.creditor_id = creditor_payment_item.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date


						UNION ALL

							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							 	`orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Drug Purchase' AS `transactionName`,
								CONCAT('Purchase of supplies',' ',orders.supplier_invoice_number) AS `transactionDescription`,
								SUM(`order_supplier`.`less_vat`) AS `dr_amount`,
								0 AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Purchases' AS `transactionCategory`,
								'Supplies Invoices' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`
							FROM
								`order_supplier`,orders,account,order_item,product,account_type,creditor
										

							WHERE orders.is_store = 0 
								AND orders.supplier_id = creditor.creditor_id
								AND orders.order_id = order_supplier.order_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND product.product_id = order_item.product_id
								AND account_type.account_type_id = account.account_type_id
								AND account.account_id = orders.account_id
								AND orders.order_approval_status = 7 
								AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id

							UNION ALL



							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							 	`orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Drug Purchase' AS `transactionName`,
								CONCAT('Purchase of supplies',' ',orders.supplier_invoice_number) AS `transactionDescription`,
								0 AS `dr_amount`,
								SUM(`order_supplier`.`less_vat`) AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Purchases' AS `transactionCategory`,
								'Supplies Invoices' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`
							FROM
								`order_supplier`,orders,account,order_item,product,account_type,creditor
										

							WHERE orders.is_store = 0 
								AND orders.supplier_id = creditor.creditor_id
								AND orders.order_id = order_supplier.order_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND product.product_id = order_item.product_id
								AND account_type.account_type_id = account.account_type_id
								AND account.account_id = ".$accounts_payable_id."
								AND orders.order_approval_status = 7 
								AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id

							UNION ALL

							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								`orders`.`reference_number`  AS `transactionCode`,
								'' AS `patient_id`,
							  `orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`orders`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Credit' AS `transactionName`,
								CONCAT('Credit note of ',' ',`orders`.`reference_number`) AS `transactionDescription`,
								'0' AS `dr_amount`,
								SUM(`order_supplier`.`less_vat`) AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Income' AS `transactionCategory`,
								'Supplies Credit Note' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`

							FROM
								`order_supplier`,orders,order_item,product,account,account_type,creditor
								
								WHERE orders.is_store = 3
								AND orders.order_id = order_supplier.order_id
								AND account.account_id = orders.account_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND product.product_id = order_item.product_id
								AND account_type.account_type_id = account.account_type_id
								 AND orders.supplier_id = creditor.creditor_id and orders.order_approval_status = 7 AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								 AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id

							UNION ALL

							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								`orders`.`reference_number`  AS `transactionCode`,
								'' AS `patient_id`,
							  `orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Credit' AS `transactionName`,
								CONCAT('Credit note of ',' ',`orders`.`supplier_invoice_number`) AS `transactionDescription`,
								SUM(`order_supplier`.`less_vat`)  AS `dr_amount`,
								0 AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Income' AS `transactionCategory`,
								'Supplies Credit Note' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`

							FROM
								`order_supplier`,orders,account,order_item,product,account_type,creditor
											

								WHERE orders.is_store = 3
								AND account.account_id = ".$accounts_payable_id."
								AND account_type.account_type_id = account.account_type_id
								AND product.product_id = order_item.product_id
								AND orders.order_id = order_supplier.order_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND orders.supplier_id = creditor.creditor_id and orders.order_approval_status = 7 AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id



							UNION ALL

							 SELECT
								`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
								`creditor_payment`.`creditor_payment_id` AS `referenceId`,
								`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
								`creditor_payment`.`reference_number` AS `referenceCode`,
								`creditor_payment`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`creditor_payment`.`creditor_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								creditor_payment.account_from_id AS `accountId`,
								`account`.`account_name` AS `accountName`,
								`creditor_payment_item`.`description` AS `transactionName`,
								CONCAT('Payment to ', creditor.creditor_name ,'for ',`orders`.`supplier_invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
								0 AS `dr_amount`,
								`creditor_payment_item`.`amount_paid` AS `cr_amount`,
								`creditor_payment`.`transaction_date` AS `transactionDate`,
								`creditor_payment`.`created` AS `createdAt`,
								`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
								'creditor_payment.branch_id' AS `branch_id`,
								'Creditor Payment' AS `transactionCategory`,
								'Creditors Invoices Payments' AS `transactionClassification`,
								'creditor_payment' AS `transactionTable`,
								'creditor_payment_item' AS `referenceTable`
								FROM
								(
									(
										(
											`creditor_payment_item`,creditor_payment,orders,creditor
											
										)
										JOIN account ON(
											(
												account.account_id = creditor_payment.account_from_id
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
									
								)
							WHERE creditor_payment_item.invoice_type = 1 
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id  
							AND creditor_payment.creditor_payment_status = 1 
							AND orders.order_id = creditor_payment_item.creditor_invoice_id
							AND orders.supplier_id = creditor.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date

							UNION ALL

							SELECT
								`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
								`creditor_payment`.`creditor_payment_id` AS `referenceId`,
								`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
								`creditor_payment`.`reference_number` AS `referenceCode`,
								`creditor_payment`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`creditor_payment`.`creditor_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								`account`.`account_name` AS `accountName`,
								`creditor_payment_item`.`description` AS `transactionName`,
								CONCAT('Payment to ', creditor.creditor_name ,'for ',`orders`.`supplier_invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
								`creditor_payment_item`.`amount_paid` AS `dr_amount`,
								0 AS `cr_amount`,
								`creditor_payment`.`transaction_date` AS `transactionDate`,
								`creditor_payment`.`created` AS `createdAt`,
								`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
								'creditor_payment.branch_id' AS `branch_id`,
								'Creditor Payment' AS `transactionCategory`,
								'Creditors Invoices Payments' AS `transactionClassification`,
								'creditor_payment' AS `transactionTable`,
								'creditor_payment_item' AS `referenceTable`
								FROM
								(
									(
										(
											`creditor_payment_item`,creditor_payment,orders,creditor
											
										)
										JOIN account ON(
											(
												account.account_id = ".$accounts_payable_id."
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
									
								)
							WHERE creditor_payment_item.invoice_type = 1 
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id  
							AND creditor_payment.creditor_payment_status = 1 
							AND orders.order_id = creditor_payment_item.creditor_invoice_id
							AND orders.supplier_id = creditor.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date
								

					

						UNION ALL




						SELECT

							bank_reconcilliation.recon_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.account_name AS `accountName`,
							'' AS `transactionName`,
							CONCAT('Bank Charges ', account.account_name ) AS `transactionDescription`,
							0 AS `dr_amount`,
							SUM(bank_reconcilliation.service_charged) AS `cr_amount`,
							bank_reconcilliation.charged_date AS `transactionDate`,
							bank_reconcilliation.charged_date AS `createdAt`,
							bank_reconcilliation.recon_status AS `status`,
							2 AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							bank_reconcilliation,account,account_type
							WHERE bank_reconcilliation.recon_status = 2
							AND bank_reconcilliation.account_id = account.account_id
							AND account.account_type_id = account_type.account_type_id
						GROUP BY bank_reconcilliation.recon_id



						UNION ALL


						SELECT

							bank_reconcilliation.recon_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.account_name AS `accountName`,
							'' AS `transactionName`,
							CONCAT('Bank Charges ', account.account_name ) AS `transactionDescription`,
							SUM(bank_reconcilliation.service_charged) AS `dr_amount`,
							0 AS `cr_amount`,
							bank_reconcilliation.charged_date AS `transactionDate`,
							bank_reconcilliation.charged_date AS `createdAt`,
							bank_reconcilliation.recon_status AS `status`,
							2 AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							bank_reconcilliation,account,account_type
							WHERE bank_reconcilliation.recon_status = 2
							AND bank_reconcilliation.expense_account_id = account.account_id
							AND account.account_type_id = account_type.account_type_id
						GROUP BY bank_reconcilliation.recon_id

						UNION ALL 

						SELECT


							`journal_entry`.`journal_entry_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							journal_entry.document_number AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							journal_entry.account_from_id AS `accountId`,
							account.account_name AS `accountName`,
							'Journal Entry ' AS `transactionName`,
							CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
							`journal_entry`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`journal_entry`.`payment_date` AS `transactionDate`,
							`journal_entry`.`payment_date` AS `createdAt`,
							`journal_entry`.`journal_entry_status` AS `status`,
							2 AS `branch_id`,
							'Journal Credit' AS `transactionCategory`,
							'Journal' AS `transactionClassification`,
							'journal_entry' AS `transactionTable`,
							'account' AS `referenceTable`
						FROM
						journal_entry,account,account_type
						
						WHERE journal_entry.account_from_id  = account.account_id AND journal_entry.journal_entry_deleted = 0 
						AND account_type.account_type_id = account.account_type_id



						UNION ALL 


						SELECT
							`journal_entry`.`journal_entry_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							journal_entry.document_number AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							journal_entry.account_to_id AS `accountId`,
							account.account_name AS `accountName`,
							'Journal Entry ' AS `transactionName`,
							CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
							0 AS `dr_amount`,
							`journal_entry`.`amount_paid` AS `cr_amount`,
							`journal_entry`.`payment_date` AS `transactionDate`,
							`journal_entry`.`payment_date` AS `createdAt`,
							`journal_entry`.`journal_entry_status` AS `status`,
							2 AS `branch_id`,
							'Journal Debit' AS `transactionCategory`,
							'Journal' AS `transactionClassification`,
							'journal_entry' AS `transactionTable`,
							'account' AS `referenceTable`
						FROM
						journal_entry,account,account_type
						
						WHERE journal_entry.account_to_id  = account.account_id AND journal_entry.journal_entry_deleted = 0 
						AND account_type.account_type_id = account.account_type_id

						


						UNION ALL


						SELECT
								visit_invoice.visit_invoice_id AS transactionId,
								visit_charge.visit_charge_id  AS referenceId,
								'' AS `payingFor`,
								visit_invoice.visit_invoice_number AS referenceCode,
								'' AS transactionCode,
								visit_invoice.patient_id AS patientId,
								'' AS recepientId,

								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
							    account.account_id AS `accountId`,
							    `account`.`account_name` AS `accountName`,
							   	service_charge.service_charge_name AS transactionName,
								CONCAT( '<strong>Invoice </strong>  Invoice Number :  ', visit_invoice.visit_invoice_number,' <strong>Bill to:</strong>  ', visit_type.visit_type_name ) AS transactionDescription,
								0 AS dr_amount,
								visit_charge.visit_charge_amount*visit_charge.visit_charge_units AS cr_amount,
							    visit_invoice.created AS `transactionDate`,
							    visit_invoice.created AS `createdAt`,
							    1 AS `status`,
							    2 AS `branch_id`,
							    service.service_name AS `transactionCategory`,
							    'PATIENT INVOICE' AS `transactionClassification`,
							    'visit_invoice' AS `transactionTable`,
							    'visit_charge' AS `referenceTable`


						FROM
									visit_charge,visit_invoice,service_charge,visit_type,service,account,account_type,visit
									WHERE visit_charge.charged = 1  
									AND visit_invoice.visit_invoice_delete = 0
									AND visit_charge.visit_charge_delete = 0 
									AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id
									AND service_charge.service_charge_id = visit_charge.service_charge_id
									AND visit_type.visit_type_id = visit_invoice.bill_to
									AND service.service_id = service_charge.service_id
									AND account.account_id = service.account_id
									AND visit.visit_id = visit_charge.visit_id
									AND visit.visit_delete = 0
									AND account_type.account_type_id = account.account_type_id


						UNION ALL

						SELECT
							visit_credit_note.visit_credit_note_id AS transactionId,
							visit_invoice.visit_invoice_id AS referenceId,
							'' AS `payingFor`,
							visit_credit_note.visit_cr_note_number AS referenceCode,
							visit_credit_note.visit_cr_note_number AS transactionCode,
							visit_invoice.patient_id AS patient_id,
							'' AS recepientId,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
						    account.account_id AS `accountId`,
						    `account`.`account_name` AS `accountName`,
						    service_charge.service_charge_name AS transactionName,
							CONCAT('<strong>Credit Note for </strong>  Invoice Number: ', visit_invoice.visit_invoice_number,'<strong>Credit Note No:</strong> ',visit_credit_note.visit_cr_note_number) AS transactionDescription,
						   
							visit_credit_note_item.visit_cr_note_amount*visit_credit_note_item.visit_cr_note_units AS dr_amount,
							0 AS cr_amount,
						    visit_invoice.created AS `transactionDate`,
						    visit_credit_note.created AS `createdAt`,
						    1 AS `status`,
						    2 AS `branch_id`,
						    CONCAT(service.service_name,' Credit note') AS `transactionCategory`,
						    'INCOME CREDIT NOTE' AS `transactionClassification`,
						    'visit_invoice' AS `transactionTable`,
						    'visit_credit_note' AS `referenceTable`
						FROM
							visit_credit_note_item,visit_credit_note,visit_invoice,service_charge,service,account,account_type
							WHERE visit_credit_note.visit_cr_note_delete = 0 AND visit_credit_note_item.visit_cr_note_item_delete = 0 

							AND visit_invoice.visit_invoice_id = visit_credit_note.visit_invoice_id 
							AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id 
							AND service_charge.service_charge_id = visit_credit_note_item.service_charge_id
							AND service.service_id = service_charge.service_id
							AND account.account_id = service.account_id
							AND account_type.account_type_id = account.account_type_id



						UNION ALL



						SELECT
								visit_invoice.visit_invoice_id AS transactionId,
								visit_charge.visit_charge_id  AS referenceId,
								'' AS `payingFor`,
								visit_invoice.visit_invoice_number AS referenceCode,
								'' AS transactionCode,
								visit_invoice.patient_id AS patientId,
								'' AS recepientId,

								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
							    account.account_id AS `accountId`,
							    `account`.`account_name` AS `accountName`,
							   	service_charge.service_charge_name AS transactionName,
								CONCAT( '<strong>Invoice </strong>  Invoice Number :  ', visit_invoice.visit_invoice_number,' <strong>Bill to:</strong>  ', visit_type.visit_type_name ) AS transactionDescription,
								visit_charge.visit_charge_amount*visit_charge.visit_charge_units AS dr_amount,
								0 AS cr_amount,
							    visit_invoice.created AS `transactionDate`,
							    visit_invoice.created AS `createdAt`,
							    1 AS `status`,
							    2 AS `branch_id`,
							    service.service_name AS `transactionCategory`,
							    'PATIENT INVOICE' AS `transactionClassification`,
							    'visit_invoice' AS `transactionTable`,
							    'visit_charge' AS `referenceTable`


						FROM
									visit_charge,visit_invoice,service_charge,visit_type,service,account,account_type,visit
									WHERE visit_charge.charged = 1  
									AND visit_invoice.visit_invoice_delete = 0
									AND visit_charge.visit_charge_delete = 0 
									AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id
									AND service_charge.service_charge_id = visit_charge.service_charge_id
									AND visit_type.visit_type_id = visit_invoice.bill_to
									AND service.service_id = service_charge.service_id
									AND visit.visit_id = visit_charge.visit_id
									AND visit.visit_delete = 0
									AND account.account_id = ".$accounts_receivable_id."
									AND account_type.account_type_id = account.account_type_id


						UNION ALL

						SELECT
							visit_credit_note.visit_credit_note_id AS transactionId,
							visit_invoice.visit_invoice_id AS referenceId,
							'' AS `payingFor`,
							visit_credit_note.visit_cr_note_number AS referenceCode,
							visit_credit_note.visit_cr_note_number AS transactionCode,
							visit_invoice.patient_id AS patient_id,
							'' AS recepientId,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
						    account.account_id AS `accountId`,
						    `account`.`account_name` AS `accountName`,
						    service_charge.service_charge_name AS transactionName,
							CONCAT('<strong>Credit Note for </strong>  Invoice Number: ', visit_invoice.visit_invoice_number,'<strong>Credit Note No:</strong> ',visit_credit_note.visit_cr_note_number) AS transactionDescription,
						   
							0 AS dr_amount,
							visit_credit_note_item.visit_cr_note_amount*visit_credit_note_item.visit_cr_note_units  AS cr_amount,
						    visit_invoice.created AS `transactionDate`,
						    visit_credit_note.created AS `createdAt`,
						    1 AS `status`,
						    2 AS `branch_id`,
						    'INCOME CREDIT NOTE' AS `transactionCategory`,
						    'INCOME CREDIT NOTE' AS `transactionClassification`,
						    'visit_invoice' AS `transactionTable`,
						    'visit_credit_note' AS `referenceTable`
						FROM
							visit_credit_note_item,visit_credit_note,visit_invoice,service_charge,service,account,account_type
							WHERE visit_credit_note.visit_cr_note_delete = 0 AND visit_credit_note_item.visit_cr_note_item_delete = 0 

							AND visit_invoice.visit_invoice_id = visit_credit_note.visit_invoice_id 
							AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id 
							AND service_charge.service_charge_id = visit_credit_note_item.service_charge_id
							AND service.service_id = service_charge.service_id
							AND account.account_id = ".$accounts_receivable_id."
							AND account_type.account_type_id = account.account_type_id


							UNION ALL


							SELECT

								payments.payment_id AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								payments.confirm_number AS `referenceCode`,
								payments.confirm_number AS `transactionCode`,
								payments.patient_id AS `patient_id`,
							  	'' AS `recepientId`,
								account_type.account_type_id AS `accountParentId`,
								account_type.account_type_name AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number) AS `transactionDescription`,
								SUM(payment_item.payment_item_amount) AS `dr_amount`,
								0 AS `cr_amount`,
								payments.payment_date AS `transactionDate`,
								payments.payment_date AS `createdAt`,
								`payments`.`cancel` AS `status`,
								2 AS `branch_id`,
								'Patient Income' AS `transactionCategory`,
								'Payments' AS `transactionClassification`,
								'account_payments' AS `transactionTable`,
								'' AS `referenceTable`
							FROM
								payments,payment_item,payment_method,account,visit_invoice,account_type
								WHERE payments.cancel = 0 AND payments.payment_type = 1 AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND payment_item.invoice_type = 1
								AND payments.payment_id = payment_item.payment_id 
								AND payments.payment_method_id = payment_method.payment_method_id
								AND payment_method.account_id = account.account_id
								AND account.account_type_id = account_type.account_type_id
							GROUP BY payment_item.payment_id


							UNION ALL 


							SELECT

								payments.payment_id AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								payments.confirm_number AS `referenceCode`,
								payments.confirm_number AS `transactionCode`,
								payments.patient_id AS `patient_id`,
							  	'' AS `recepientId`,
								account_type.account_type_id AS `accountParentId`,
								account_type.account_type_name AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								account.parent_account AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Payment On Account') AS `transactionDescription`,
								SUM(payment_item.payment_item_amount) AS `dr_amount`,
								0 AS `cr_amount`,
								payments.payment_date AS `transactionDate`,
								payments.payment_date AS `createdAt`,
								`payments`.`cancel` AS `status`,
								2 AS `branch_id`,
								'Patient Income' AS `transactionCategory`,
								'Payments' AS `transactionClassification`,
								'account_payments' AS `transactionTable`,
								'' AS `referenceTable`
							FROM
								payments,payment_item,payment_method,account,account_type
								WHERE payments.cancel = 0 AND payments.payment_type = 1 AND payment_item.invoice_type = 3
								AND payments.payment_id = payment_item.payment_id 
								AND payments.payment_method_id = payment_method.payment_method_id
								AND payment_method.account_id = account.account_id
								AND account.account_type_id = account_type.account_type_id
							GROUP BY payment_item.payment_id


							UNION ALL

						SELECT

							payments.payment_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							payments.confirm_number AS `referenceCode`,
							payments.confirm_number AS `transactionCode`,
							payments.patient_id AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.account_name AS `accountName`,
							'' AS `transactionName`,
							CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number) AS `transactionDescription`,
							0 AS `dr_amount`,
							SUM(payment_item.payment_item_amount) AS `cr_amount`,
							payments.payment_date AS `transactionDate`,
							payments.payment_date AS `createdAt`,
							`payments`.`cancel` AS `status`,
							2 AS `branch_id`,
							'Patient Income' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							payments,payment_item,payment_method,account,visit_invoice,account_type
							WHERE payments.cancel = 0 AND payments.payment_type = 1 AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND payment_item.invoice_type = 1
							AND payments.payment_id = payment_item.payment_id 
							AND payments.payment_method_id = payment_method.payment_method_id
							AND account.account_id = ".$accounts_receivable_id."
							AND account.account_type_id = account_type.account_type_id
						GROUP BY payment_item.payment_id


						UNION ALL 


						SELECT

							payments.payment_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							payments.confirm_number AS `referenceCode`,
							payments.confirm_number AS `transactionCode`,
							payments.patient_id AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.parent_account AS `accountName`,
							'' AS `transactionName`,
							CONCAT('Payment On Account') AS `transactionDescription`,
							0 AS `dr_amount`,
							SUM(payment_item.payment_item_amount) AS `cr_amount`,
							payments.payment_date AS `transactionDate`,
							payments.payment_date AS `createdAt`,
							`payments`.`cancel` AS `status`,
							2 AS `branch_id`,
							'Patient Income' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							payments,payment_item,payment_method,account,account_type
							WHERE payments.cancel = 0 AND payments.payment_type = 1 AND payment_item.invoice_type = 3
							AND payments.payment_id = payment_item.payment_id 
							AND payments.payment_method_id = payment_method.payment_method_id
							AND account.account_id = ".$accounts_receivable_id."
							AND account.account_type_id = account_type.account_type_id
						GROUP BY payment_item.payment_id


						UNION ALL

						SELECT
								`provider`.`provider_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							    `provider`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
				  				`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider`.`opening_balance` AS `cr_amount`,
								`provider`.`start_date` AS `transactionDate`,
								`provider`.`start_date` AS `createdAt`,
								`provider`.`provider_status` AS `status`,
								2 AS `branch_id`,
								'Expense' AS `transactionCategory`,
								'provider Opening Balance' AS `transactionClassification`,
								'' AS `transactionTable`,
								'provider' AS `referenceTable`
							FROM
							provider,account,account_type
							WHERE  debit_id = 1 AND account.account_id = ".$provider_opening_balance_id." AND account.account_type_id = account_type.account_type_id

							UNION ALL


							SELECT
								`provider`.`provider_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							    `provider`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
				  				`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
								`provider`.`opening_balance` AS `dr_amount`,
								0 AS `cr_amount`,
								`provider`.`start_date` AS `transactionDate`,
								`provider`.`start_date` AS `createdAt`,
								`provider`.`provider_status` AS `status`,
								2 AS `branch_id`,
								'Expense' AS `transactionCategory`,
								'provider Opening Balance' AS `transactionClassification`,
								'' AS `transactionTable`,
								'provider' AS `referenceTable`
							FROM
							provider,account,account_type
							WHERE  debit_id = 1 AND account.account_id = ".$providers_liability_id." AND account.account_type_id = account_type.account_type_id


							UNION ALL


							SELECT
								`provider`.`provider_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							    `provider`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
				  				`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
								`provider`.`opening_balance` AS `dr_amount`,
								0 AS `cr_amount`,
								`provider`.`start_date` AS `transactionDate`,
								`provider`.`start_date` AS `createdAt`,
								`provider`.`provider_status` AS `status`,
								2 AS `branch_id`,
								'Expense' AS `transactionCategory`,
								'provider Opening Balance' AS `transactionClassification`,
								'' AS `transactionTable`,
								'provider' AS `referenceTable`
							FROM
							provider,account,account_type
							WHERE  debit_id = 2 AND account.account_id = ".$provider_opening_balance_id." AND account.account_type_id = account_type.account_type_id



							UNION ALL


							SELECT
								`provider`.`provider_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							    `provider`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
				  				`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider`.`opening_balance` AS `cr_amount`,
								`provider`.`start_date` AS `transactionDate`,
								`provider`.`start_date` AS `createdAt`,
								`provider`.`provider_status` AS `status`,
								2 AS `branch_id`,
								'Expense' AS `transactionCategory`,
								'provider Opening Balance' AS `transactionClassification`,
								'' AS `transactionTable`,
								'provider' AS `referenceTable`
							FROM
							provider,account,account_type
							WHERE  debit_id = 2 AND account.account_id = ".$providers_liability_id." AND account.account_type_id = account_type.account_type_id


						  UNION ALL

							SELECT
								`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
								`provider_invoice`.`provider_invoice_id` AS `referenceId`,
								'' AS `payingFor`,
								`provider_invoice`.`invoice_number` AS `referenceCode`,
								`provider_invoice`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  `provider_invoice`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`provider_invoice_item`.`account_to_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
								`provider_invoice_item`.`total_amount` AS `dr_amount`,
								'0' AS `cr_amount`,
								`provider_invoice`.`transaction_date` AS `transactionDate`,
								`provider_invoice`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
								'Expense' AS `transactionCategory`,
								'providers Invoices' AS `transactionClassification`,
								'provider_invoice_item' AS `transactionTable`,
								'provider_invoice' AS `referenceTable`
							FROM
								(
									(
										(
											`provider_invoice_item`,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = provider_invoice_item.account_to_id
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
							provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
							AND provider_invoice.provider_invoice_status = 1
							AND provider.provider_id = provider_invoice.provider_id 
							AND provider_invoice.transaction_date >= provider.start_date

						UNION ALL

						SELECT
								`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
								`provider_invoice`.`provider_invoice_id` AS `referenceId`,
								'' AS `payingFor`,
								`provider_invoice`.`invoice_number` AS `referenceCode`,
								`provider_invoice`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  `provider_invoice`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider_invoice_item`.`total_amount` AS `cr_amount`,
								`provider_invoice`.`transaction_date` AS `transactionDate`,
								`provider_invoice`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
								'Provider Liability' AS `transactionCategory`,
								'providers Invoices' AS `transactionClassification`,
								'provider_invoice_item' AS `transactionTable`,
								'provider_invoice' AS `referenceTable`
							FROM
								(
									(
										(
											`provider_invoice_item`,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = ".$providers_liability_id."
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
							provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
							AND provider_invoice.provider_invoice_status = 1
							AND provider.provider_id = provider_invoice.provider_id 
							AND provider_invoice.transaction_date >= provider.start_date


							UNION ALL

							SELECT
								`provider_credit_note_item`.`provider_credit_note_item_id` AS `transactionId`,
								`provider_credit_note`.`provider_credit_note_id` AS `referenceId`,
								`provider_credit_note_item`.`provider_invoice_id` AS `payingFor`,
								`provider_credit_note`.`invoice_number` AS `referenceCode`,
								`provider_credit_note`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  	`provider_credit_note`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`provider_credit_note`.`account_from_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider_credit_note_item`.`credit_note_amount` AS `cr_amount`,
								`provider_credit_note`.`transaction_date` AS `transactionDate`,
								`provider_credit_note`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_credit_note_item`.`provider_credit_note_item_status` AS `status`,
								'Provider Credit Note' AS `transactionCategory`,
								'Providers Credit Notes' AS `transactionClassification`,
								'provider_credit_note' AS `transactionTable`,
								'provider_credit_note_item' AS `referenceTable`
							FROM
								(
									(
										(
											`provider_credit_note_item`,provider_credit_note,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = provider_credit_note.account_from_id
											)
										)
										
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
								provider_credit_note.provider_credit_note_id = provider_credit_note_item.provider_credit_note_id 
								AND provider_credit_note.provider_credit_note_status = 1
								AND provider_invoice.provider_invoice_id = provider_credit_note_item.provider_invoice_id
								AND provider_invoice.provider_invoice_status = 1
								AND provider.provider_id = provider_invoice.provider_id 
								AND provider_invoice.transaction_date >= provider.start_date

							UNION ALL

							SELECT
								`provider_credit_note_item`.`provider_credit_note_item_id` AS `transactionId`,
								`provider_credit_note`.`provider_credit_note_id` AS `referenceId`,
								`provider_credit_note_item`.`provider_invoice_id` AS `payingFor`,
								`provider_credit_note`.`invoice_number` AS `referenceCode`,
								`provider_credit_note`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  	`provider_credit_note`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionDescription`,
								`provider_credit_note_item`.`credit_note_amount` AS `dr_amount`,
								0 AS `cr_amount`,
								`provider_credit_note`.`transaction_date` AS `transactionDate`,
								`provider_credit_note`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_credit_note_item`.`provider_credit_note_item_status` AS `status`,
								'Provider Credit Note' AS `transactionCategory`,
								'Providers Credit Notes' AS `transactionClassification`,
								'provider_credit_note' AS `transactionTable`,
								'provider_credit_note_item' AS `referenceTable`
							FROM
								(
									(
										(
											`provider_credit_note_item`,provider_credit_note,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = ".$providers_liability_id."
											)
										)
										
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
								provider_credit_note.provider_credit_note_id = provider_credit_note_item.provider_credit_note_id 
								AND provider_credit_note.provider_credit_note_status = 1
								AND provider_invoice.provider_invoice_id = provider_credit_note_item.provider_invoice_id
								AND provider_invoice.provider_invoice_status = 1
								AND provider.provider_id = provider_invoice.provider_id 
								AND provider_invoice.transaction_date >= provider.start_date


							UNION ALL



								

							SELECT
									`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
									`provider_payment`.`provider_payment_id` AS `referenceId`,
									`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
									`provider_payment`.`reference_number` AS `referenceCode`,
									`provider_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`provider_payment`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`provider_payment`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`provider_payment_item`.`description` AS `transactionName`,
									CONCAT('Provider payment for invoice of ',' ',`provider_invoice`.`invoice_number`)  AS `transactionDescription`,
									0 AS `dr_amount`,
									`provider_payment_item`.`amount_paid` AS `cr_amount`,
									`provider_payment`.`transaction_date` AS `transactionDate`,
									`provider_payment`.`created` AS `createdAt`,
									`provider_invoice`.`transaction_date` AS `referenceDate`,
									`provider_payment_item`.`provider_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'providers Invoices Payments' AS `transactionClassification`,
									'provider_payment' AS `transactionTable`,
									'provider_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_payment_item`,provider_payment,provider_invoice,provider
												
											)
											JOIN account ON(
												(
													account.account_id = provider_payment.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE provider_payment_item.invoice_type = 0 
									AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
									AND provider_payment.provider_payment_status = 1
									AND provider_invoice.provider_invoice_id = provider_payment_item.provider_invoice_id 
									AND provider_invoice.provider_invoice_status = 1  
									AND provider.provider_id = provider_invoice.provider_id AND provider_invoice.transaction_date >= provider.start_date

							UNION ALL



								

							SELECT
									`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
									`provider_payment`.`provider_payment_id` AS `referenceId`,
									`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
									`provider_payment`.`reference_number` AS `referenceCode`,
									`provider_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`provider_payment`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									account.account_id AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`provider_payment_item`.`description` AS `transactionName`,
									CONCAT('Provider payment for invoice of ',' ',`provider_invoice`.`invoice_number`)  AS `transactionDescription`,
									`provider_payment_item`.`amount_paid` AS `dr_amount`,
									0 AS `cr_amount`,
									`provider_payment`.`transaction_date` AS `transactionDate`,
									`provider_payment`.`created` AS `createdAt`,
									`provider_invoice`.`transaction_date` AS `referenceDate`,
									`provider_payment_item`.`provider_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'providers Invoices Payments' AS `transactionClassification`,
									'provider_payment' AS `transactionTable`,
									'provider_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_payment_item`,provider_payment,provider_invoice,provider
												
											)
											JOIN account ON(
												(
													account.account_id = ".$providers_liability_id."
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE provider_payment_item.invoice_type = 0 
									AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
									AND provider_payment.provider_payment_status = 1
									AND provider_invoice.provider_invoice_id = provider_payment_item.provider_invoice_id 
									AND provider_invoice.provider_invoice_status = 1  
									AND provider.provider_id = provider_invoice.provider_id AND provider_invoice.transaction_date >= provider.start_date

								
							UNION ALL

							SELECT
									`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
									`provider_payment`.`provider_payment_id` AS `referenceId`,
									`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
									`provider_payment`.`reference_number` AS `referenceCode`,
									`provider_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  `provider_payment`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`provider_payment`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`provider_payment_item`.`description` AS `transactionName`,
									CONCAT('Payment of opening balance')  AS `transactionDescription`,
									0 AS `dr_amount`,
									`provider_payment_item`.`amount_paid` AS `cr_amount`,
									`provider_payment`.`transaction_date` AS `transactionDate`,
									`provider_payment`.`created` AS `createdAt`,
									`provider`.`start_date` AS `referenceDate`,
									`provider_payment_item`.`provider_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'providers Invoices Payments' AS `transactionClassification`,
									'provider_payment' AS `transactionTable`,
									'provider_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_payment_item`,provider_payment,provider
												
											)
											JOIN account ON(
												(
													account.account_id = provider_payment.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE provider_payment_item.invoice_type = 2 
									AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
									AND provider_payment.provider_payment_status = 1
									AND provider.provider_id = provider_payment_item.provider_id
									AND provider_payment.transaction_date >= provider.start_date
								UNION ALL

							SELECT
									`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
									`provider_payment`.`provider_payment_id` AS `referenceId`,
									`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
									`provider_payment`.`reference_number` AS `referenceCode`,
									`provider_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  `provider_payment`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									account.account_id AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`provider_payment_item`.`description` AS `transactionName`,
									CONCAT('Payment of opening balance')  AS `transactionDescription`,
									`provider_payment_item`.`amount_paid` AS `dr_amount`,
									0 AS `cr_amount`,
									`provider_payment`.`transaction_date` AS `transactionDate`,
									`provider_payment`.`created` AS `createdAt`,
									`provider`.`start_date` AS `referenceDate`,
									`provider_payment_item`.`provider_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'providers Invoices Payments' AS `transactionClassification`,
									'provider_payment' AS `transactionTable`,
									'provider_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_payment_item`,provider_payment,provider
												
											)
											JOIN account ON(
												(
													account.account_id = ".$providers_liability_id."
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE provider_payment_item.invoice_type = 2 
									AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
									AND provider_payment.provider_payment_status = 1
									AND provider.provider_id = provider_payment_item.provider_id
									AND provider_payment.transaction_date >= provider.start_date

								UNION ALL

								SELECT
										`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
										`provider_payment`.`provider_payment_id` AS `referenceId`,
										`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
										`provider_payment`.`reference_number` AS `referenceCode`,
										`provider_payment`.`document_number` AS `transactionCode`,
										'' AS `patient_id`,
									  `provider_payment`.`provider_id` AS `recepientId`,
										`account`.`parent_account` AS `accountParentId`,
										`account_type`.`account_type_name` AS `accountsclassfication`,
										`provider_payment`.`account_from_id` AS `accountId`,
										`account`.`account_name` AS `accountName`,
										`provider_payment_item`.`description` AS `transactionName`,
										CONCAT('Payment on account')  AS `transactionDescription`,
										0 AS `dr_amount`,
										`provider_payment_item`.`amount_paid` AS `cr_amount`,
										`provider_payment`.`transaction_date` AS `transactionDate`,
										`provider_payment`.`created` AS `createdAt`,
										`provider_payment`.`created` AS `referenceDate`,
										`provider_payment_item`.`provider_payment_item_status` AS `status`,
										'Expense Payment' AS `transactionCategory`,
										'providers Invoices Payments' AS `transactionClassification`,
										'provider_payment' AS `transactionTable`,
										'provider_payment_item' AS `referenceTable`
									FROM
										(
											(
												(
													`provider_payment_item`,provider_payment,provider
													
												)
												JOIN account ON(
													(
														account.account_id = provider_payment.account_from_id
													)
												)
											)
											JOIN `account_type` ON(
												(
													account_type.account_type_id = account.account_type_id
												)
											)
											
										)
										WHERE provider_payment_item.invoice_type = 3 AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
											AND provider_payment.provider_payment_status = 1 
											AND provider.provider_id = provider_payment.provider_id AND provider_payment.transaction_date >= provider.start_date
							UNION ALL

							SELECT
									`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
									`provider_payment`.`provider_payment_id` AS `referenceId`,
									`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
									`provider_payment`.`reference_number` AS `referenceCode`,
									`provider_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  `provider_payment`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`provider_payment_item`.`description` AS `transactionName`,
									CONCAT('Payment on account')  AS `transactionDescription`,
									`provider_payment_item`.`amount_paid` AS `dr_amount`,
									0 AS `cr_amount`,
									`provider_payment`.`transaction_date` AS `transactionDate`,
									`provider_payment`.`created` AS `createdAt`,
									`provider_payment`.`created` AS `referenceDate`,
									`provider_payment_item`.`provider_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'providers Invoices Payments' AS `transactionClassification`,
									'provider_payment' AS `transactionTable`,
									'provider_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`provider_payment_item`,provider_payment,provider
												
											)
											JOIN account ON(
												(
													account.account_id = ".$providers_liability_id."
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE provider_payment_item.invoice_type = 3 AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
										AND provider_payment.provider_payment_status = 1 
										AND provider.provider_id = provider_payment.provider_id AND provider_payment.transaction_date >= provider.start_date
								
							

							
							UNION ALL


							SELECT
								`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
								`provider_invoice`.`provider_invoice_id` AS `referenceId`,
								'' AS `payingFor`,
								`provider_invoice`.`invoice_number` AS `referenceCode`,
								`provider_invoice`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  `provider_invoice`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider_invoice_item`.`vat_amount` AS `cr_amount`,
								`provider_invoice`.`transaction_date` AS `transactionDate`,
								`provider_invoice`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
								'Provider WHT' AS `transactionCategory`,
								'providers Invoices' AS `transactionClassification`,
								'provider_invoice_item' AS `transactionTable`,
								'provider_invoice' AS `referenceTable`
							FROM
								(
									(
										(
											`provider_invoice_item`,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = ".$providers_wht_id."
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
							provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
							AND provider_invoice.provider_invoice_status = 1
							AND provider.provider_id = provider_invoice.provider_id 
							AND provider_invoice.transaction_date >= provider.start_date
							AND provider_invoice_item.vat_amount > 0

					
							UNION ALL




							SELECT
								`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
								`provider_invoice`.`provider_invoice_id` AS `referenceId`,
								'' AS `payingFor`,
								`provider_invoice`.`invoice_number` AS `referenceCode`,
								`provider_invoice`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  `provider_invoice`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
								`provider_invoice_item`.`vat_amount` AS `dr_amount`,
								0  AS `cr_amount`,
								`provider_invoice`.`transaction_date` AS `transactionDate`,
								`provider_invoice`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
								'Provider WHT' AS `transactionCategory`,
								'providers Invoices' AS `transactionClassification`,
								'provider_invoice_item' AS `transactionTable`,
								'provider_invoice' AS `referenceTable`
							FROM
								(
									(
										(
											`provider_invoice_item`,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = ".$providers_liability_id."
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
							provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
							AND provider_invoice.provider_invoice_status = 1
							AND provider.provider_id = provider_invoice.provider_id 
							AND provider_invoice.transaction_date >= provider.start_date
							AND provider_invoice_item.vat_amount > 0

							

							UNION ALL

								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payroll') AS `transactionName`,
									CONCAT('Payroll') AS `transactionDescription`,
									payroll_summary.total_payroll AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'Payroll' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 1
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id


								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('PAYE') AS `transactionName`,
									CONCAT('PAYE') AS `transactionDescription`,
									payroll_summary.paye AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 2
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id

								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NSSF') AS `transactionName`,
									CONCAT('NSSF') AS `transactionDescription`,
									payroll_summary.nssf AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 3
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id


								UNION ALL




								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NHIF') AS `transactionName`,
									CONCAT('NHIF') AS `transactionDescription`,
									payroll_summary.nhif AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'NHIF' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 4
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id


							UNION ALL


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payroll') AS `transactionName`,
									CONCAT('Payroll') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.total_payroll AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'Payroll' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 1
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id


								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('PAYE') AS `transactionName`,
									CONCAT('PAYE') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.paye AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 2
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id

								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NSSF') AS `transactionName`,
									CONCAT('NSSF') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.nssf AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 3
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id


								UNION ALL




								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NHIF') AS `transactionName`,
									CONCAT('NHIF') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.nhif AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'NHIF' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 4
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id

							UNION ALL

							SELECT
							    `payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
							    `payroll_payment`.`payroll_payment_id` AS `referenceId`,
							    `payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
							    `payroll_payment`.`reference_number` AS `referenceCode`,
							    `payroll_payment`.`document_number` AS `transactionCode`,
							    '' AS `patient_id`,
							    `statutory_accounts`.`statutory_account_id` AS `recepientId`,
							    `account`.`parent_account` AS `accountParentId`,
							    `account_type`.`account_type_name` AS `accountsclassfication`,
							    `payroll_payment`.`account_from_id` AS `accountId`,
							    `account`.`account_name` AS `accountName`,
							    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
							    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for))   AS `transactionDescription`,
							    0 AS `dr_amount`,
							    `payroll_payment_item`.`amount_paid` AS `cr_amount`,
							    `payroll_payment`.`transaction_date` AS `transactionDate`,
							    `payroll_payment`.`created` AS `createdAt`,
							    `payroll_payment_item`.`payroll_payment_item_status` AS `status`,
							    2 AS `branch_id`,
							    'Payroll Payment' AS `transactionCategory`,
							    'Creditors Invoices Payments' AS `transactionClassification`,
							    'payroll_payment' AS `transactionTable`,
							    'payroll_payment_item' AS `referenceTable`
							FROM
							    (
							      (
							        (
							          `payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts
							          
							        )
							        JOIN account ON(
							          (
							            account.account_id = payroll_payment.account_from_id
							          )
							        )
							      )
							      JOIN `account_type` ON(
							        (
							          account_type.account_type_id = account.account_type_id
							        )
							      )
							      
							    )
							    WHERE payroll_payment_item.invoice_type = 0 
							    AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
							    AND payroll_payment.payroll_payment_status = 1
							    AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_payment_id 
							    AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id

							UNION ALL

								SELECT
								    `payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
								    `payroll_payment`.`payroll_payment_id` AS `referenceId`,
								    `payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
								    `payroll_payment`.`reference_number` AS `referenceCode`,
								    `payroll_payment`.`document_number` AS `transactionCode`,
								    '' AS `patient_id`,
								    `statutory_accounts`.`statutory_account_id` AS `recepientId`,
								    `account`.`parent_account` AS `accountParentId`,
								    `account_type`.`account_type_name` AS `accountsclassfication`,
								    account.account_id AS `accountId`,
								    `account`.`account_name` AS `accountName`,
								    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
								    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for))   AS `transactionDescription`,
								    `payroll_payment_item`.`amount_paid` AS `dr_amount`,
								    0 AS `cr_amount`,
								    `payroll_payment`.`transaction_date` AS `transactionDate`,
								    `payroll_payment`.`created` AS `createdAt`,
								    `payroll_payment_item`.`payroll_payment_item_status` AS `status`,
								    2 AS `branch_id`,
								    'Payroll Payment' AS `transactionCategory`,
								    'Creditors Invoices Payments' AS `transactionClassification`,
								    'payroll_payment' AS `transactionTable`,
								    'payroll_payment_item' AS `referenceTable`
								  FROM
								    (
								      (
								        (
								          `payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts
								          
								        )
								        JOIN account ON(
								          (
								            account.account_id = ".$payroll_liability_id."
								          )
								        )
								      )
								      JOIN `account_type` ON(
								        (
								          account_type.account_type_id = account.account_type_id
								        )
								      )
								      
								    )
								    WHERE payroll_payment_item.invoice_type = 0 
								    AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
								    AND payroll_payment.payroll_payment_status = 1
								    AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_invoice_id 
								    AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id


								UNION ALL

								 SELECT
									asset_category.asset_category_id AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									assets_details.asset_serial_no AS `referenceCode`,
									assets_details.asset_serial_no AS `transactionCode`,
									assets_details.asset_id AS `patient_id`,
								  	assets_details.supplier_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
									CONCAT(assets_details.asset_name) AS `transactionName`,
									CONCAT(assets_details.asset_name) AS `transactionDescription`,
									(assets_details.asset_value) AS `dr_amount`,
									0 AS `cr_amount`,
									assets_details.asset_pd_period AS `transactionDate`,
									assets_details.asset_pd_period  AS `createdAt`,
									assets_details.asset_pd_period AS `referenceDate`,
									1 AS `status`,
									'FIXED ASSET' AS `transactionCategory`,
									'Asset Purchase' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
									assets_details,asset_category,account,account_type
									WHERE 
									assets_details.asset_category_id = asset_category.asset_category_id 
									AND account.account_id = asset_category.account_id
									AND account.account_type_id = account_type.account_type_id

								UNION ALL


								SELECT
									asset_category.asset_category_id AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									assets_details.asset_serial_no AS `referenceCode`,
									assets_details.asset_serial_no AS `transactionCode`,
									assets_details.asset_id AS `patient_id`,
								  	assets_details.supplier_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
									CONCAT(assets_details.asset_name) AS `transactionName`,
									CONCAT(assets_details.asset_name) AS `transactionDescription`,
									0 AS `dr_amount`,
									(assets_details.asset_value) AS `cr_amount`,
									assets_details.asset_pd_period AS `transactionDate`,
									assets_details.asset_pd_period  AS `createdAt`,
									assets_details.asset_pd_period AS `referenceDate`,
									1 AS `status`,
									'FIXED ASSET' AS `transactionCategory`,
									'Asset Purchase' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
									assets_details,asset_category,account,account_type
									WHERE 
									assets_details.asset_category_id = asset_category.asset_category_id 
									AND assets_details.bill_asset = 1
									AND account.account_id = ".$accounts_payable_id."
									AND account.account_type_id = account_type.account_type_id

							UNION ALL

								SELECT


									patients_journals.patient_journal_id AS transactionId,
									patients_journals.patient_journal_id AS referenceId,
									'' AS `payingFor`,
									'' AS referenceCode,
									'' AS `transactionCode`,
									patients_journals.patient_id AS patient_id,
									patients_journals.patient_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									account.account_name AS `accountName`,
									CONCAT('Patient Journals') AS `transactionName`,
									CONCAT('Patient Journals') AS `transactionDescription`,
									patients_journals.journal_amount AS `dr_amount`,
									0 AS `cr_amount`,
									patients_journals.journal_date AS `transactionDate`,
									patients_journals.journal_date  AS `createdAt`,
									patients_journals.journal_date AS `referenceDate`,
									1 AS `status`,
									'JOURNAL' AS `transactionCategory`,
									'JOURNAL' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
										patients_journals,visit_type,account,account_type
									WHERE 
									patients_journals.visit_type_id = visit_type.visit_type_id 
									AND patients_journals.visit_invoice_id > 0
									AND patients_journals.account_from_id = account.account_id
									AND account_type.account_type_id = account.account_type_id

							UNION ALL

								SELECT


									patients_journals.patient_journal_id AS transactionId,
									patients_journals.patient_journal_id AS referenceId,
									'' AS `payingFor`,
									'' AS referenceCode,
									'' AS `transactionCode`,
									patients_journals.patient_id AS patient_id,
									patients_journals.patient_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									account.account_name AS `accountName`,
									CONCAT('Patient Journals') AS `transactionName`,
									CONCAT('Patient Journals') AS `transactionDescription`,
									0 AS `dr_amount`,
									patients_journals.journal_amount AS `cr_amount`,
									patients_journals.journal_date AS `transactionDate`,
									patients_journals.journal_date  AS `createdAt`,
									patients_journals.journal_date AS `referenceDate`,
									1 AS `status`,
									'JOURNAL' AS `transactionCategory`,
									'JOURNAL' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
										patients_journals,visit_type,account,account_type
									WHERE 
									patients_journals.visit_type_id = visit_type.visit_type_id 
									AND patients_journals.visit_invoice_id > 0
									AND patients_journals.account_to_id = account.account_id
									AND account_type.account_type_id = account.account_type_id

								UNION ALL

								SELECT
									`loan_invoice_item`.`loan_invoice_item_id` AS `transactionId`,
									`loan_invoice`.`loan_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`loan_invoice`.`invoice_number` AS `referenceCode`,
									`loan_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_invoice`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`loan_invoice_item`.`item_description` AS `transactionName`,
									`loan_invoice_item`.`item_description` AS `transactionDescription`,
									 0 AS `dr_amount`,
									`loan_invoice_item`.`total_amount` AS `cr_amount`,
									`loan_invoice`.`transaction_date` AS `transactionDate`,
									`loan_invoice`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_invoice_item`.`loan_invoice_item_status` AS `status`,
									'Expense' AS `transactionCategory`,
									'loans Invoices' AS `transactionClassification`,
									'loan_invoice_item' AS `transactionTable`,
									'loan_invoice' AS `referenceTable`
								FROM
									(
										(
											(
												`loan_invoice_item`,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
									)
								WHERE 
								loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id 
								AND loan_invoice.loan_invoice_status = 1
								AND loan.loan_id = loan_invoice.loan_id 
								AND loan_invoice.transaction_date >= loan.start_date

							UNION ALL

								SELECT
									`loan_invoice_item`.`loan_invoice_item_id` AS `transactionId`,
									`loan_invoice`.`loan_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`loan_invoice`.`invoice_number` AS `referenceCode`,
									`loan_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_invoice`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan`.`account_to_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`loan_invoice_item`.`item_description` AS `transactionName`,
									`loan_invoice_item`.`item_description` AS `transactionDescription`,
									`loan_invoice_item`.`total_amount` AS `dr_amount`,
									'0' AS `cr_amount`,
									`loan_invoice`.`transaction_date` AS `transactionDate`,
									`loan_invoice`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_invoice_item`.`loan_invoice_item_status` AS `status`,
									'Expense' AS `transactionCategory`,
									'loans Invoices' AS `transactionClassification`,
									'loan_invoice_item' AS `transactionTable`,
									'loan_invoice' AS `referenceTable`
								FROM
									(
										(
											(
												`loan_invoice_item`,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan.account_to_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
									)
								WHERE 
								loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id 
								AND loan_invoice.loan_invoice_status = 1
								AND loan.loan_id = loan_invoice.loan_id 
								AND loan_invoice.transaction_date >= loan.start_date

								UNION ALL

								SELECT
									`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
									`loan_payment`.`loan_payment_id` AS `referenceId`,
									`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
									`loan_payment`.`reference_number` AS `referenceCode`,
									`loan_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_payment`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan_payment`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
									CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
									0 AS `dr_amount`,
									`loan_payment_item`.`amount_paid` AS `cr_amount`,
									`loan_payment`.`transaction_date` AS `transactionDate`,
									`loan_payment`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_payment_item`.`loan_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'loans Invoices Payments' AS `transactionClassification`,
									'loan_payment' AS `transactionTable`,
									'loan_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`loan_payment_item`,loan_payment,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan_payment.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE loan_payment_item.invoice_type = 0 
									AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
									AND loan_payment.loan_payment_status = 1
									AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
									AND loan_invoice.loan_invoice_status = 1  
									AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
									AND loan.account_from_id = loan_payment_item.account_to_id
									
							UNION ALL
							
							SELECT
									`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
									`loan_payment`.`loan_payment_id` AS `referenceId`,
									`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
									`loan_payment`.`reference_number` AS `referenceCode`,
									`loan_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_payment`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
									CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
									`loan_payment_item`.`amount_paid` AS `dr_amount`,
									0 AS `cr_amount`,
									`loan_payment`.`transaction_date` AS `transactionDate`,
									`loan_payment`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_payment_item`.`loan_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'loans Invoices Payments' AS `transactionClassification`,
									'loan_payment' AS `transactionTable`,
									'loan_payment_item' AS `referenceTable`
								FROM
									(
										(
											(
												`loan_payment_item`,loan_payment,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE loan_payment_item.invoice_type = 0 
									AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
									AND loan_payment.loan_payment_status = 1
									AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
									AND loan_invoice.loan_invoice_status = 1  
									AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
									AND loan.account_from_id = loan_payment_item.account_to_id

									UNION ALL

									SELECT
											`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
											`loan_payment`.`loan_payment_id` AS `referenceId`,
											`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
											`loan_payment`.`reference_number` AS `referenceCode`,
											`loan_payment`.`document_number` AS `transactionCode`,
											'' AS `patient_id`,
												`loan_payment`.`loan_id` AS `recepientId`,
											`account`.`parent_account` AS `accountParentId`,
											`account_type`.`account_type_name` AS `accountsclassfication`,
											loan_payment_item.account_to_id AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
											CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
											`loan_payment_item`.`amount_paid` AS `dr_amount`,
											0 AS `cr_amount`,
											`loan_payment`.`transaction_date` AS `transactionDate`,
											`loan_payment`.`created` AS `createdAt`,
											`loan_invoice`.`transaction_date` AS `referenceDate`,
											`loan_payment_item`.`loan_payment_item_status` AS `status`,
											'Expense Payment' AS `transactionCategory`,
											'loans Invoices Payments' AS `transactionClassification`,
											'loan_payment' AS `transactionTable`,
											'loan_payment_item' AS `referenceTable`
										FROM
											(
												(
													(
														`loan_payment_item`,loan_payment,loan_invoice,loan,account
														
													)
												
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
											WHERE loan_payment_item.invoice_type = 0 
											AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
											AND loan_payment.loan_payment_status = 1
											AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
											AND loan_invoice.loan_invoice_status = 1  
											AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
											AND loan.account_from_id <> loan_payment_item.account_to_id
											AND account.account_id = loan_payment_item.account_to_id
											
											UNION ALL
											
											
											SELECT
											`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
											`loan_payment`.`loan_payment_id` AS `referenceId`,
											`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
											`loan_payment`.`reference_number` AS `referenceCode`,
											`loan_payment`.`document_number` AS `transactionCode`,
											'' AS `patient_id`,
												`loan_payment`.`loan_id` AS `recepientId`,
											`account`.`parent_account` AS `accountParentId`,
											`account_type`.`account_type_name` AS `accountsclassfication`,
											loan_payment.account_from_id AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
											CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
											0 AS `dr_amount`,
											`loan_payment_item`.`amount_paid` AS `cr_amount`,
											`loan_payment`.`transaction_date` AS `transactionDate`,
											`loan_payment`.`created` AS `createdAt`,
											`loan_invoice`.`transaction_date` AS `referenceDate`,
											`loan_payment_item`.`loan_payment_item_status` AS `status`,
											'Expense Payment' AS `transactionCategory`,
											'loans Invoices Payments' AS `transactionClassification`,
											'loan_payment' AS `transactionTable`,
											'loan_payment_item' AS `referenceTable`
										FROM
											(
												(
													(
														`loan_payment_item`,loan_payment,loan_invoice,loan,account
														
													)
												
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
											WHERE loan_payment_item.invoice_type = 0 
											AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
											AND loan_payment.loan_payment_status = 1
											AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
											AND loan_invoice.loan_invoice_status = 1  
											AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
											AND loan.account_from_id <> loan_payment_item.account_to_id
											AND account.account_id = loan_payment.account_from_id

										UNION ALL


											SELECT
												asset_category.asset_category_id AS `transactionId`,
												'' AS `referenceId`,
												'' AS `payingFor`,
												assets_details.asset_serial_no AS `referenceCode`,
												assets_details.asset_serial_no AS `transactionCode`,
												assets_details.asset_id AS `patient_id`,
													assets_details.supplier_id AS `recepientId`,
												account_type.account_type_id AS `accountParentId`,
												account_type.account_type_name AS `accountsclassfication`,
												`account`.`account_id` AS `accountId`,
												CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
												CONCAT(assets_details.asset_name) AS `transactionName`,
												CONCAT(assets_details.asset_name) AS `transactionDescription`,
												(asset_amortization.interest_amount) AS `dr_amount`,
												0 AS `cr_amount`,
												asset_amortization.date_approved AS `transactionDate`,
												asset_amortization.date_approved  AS `createdAt`,
												asset_amortization.date_approved AS `referenceDate`,
												1 AS `status`,
												'FIXED ASSET' AS `transactionCategory`,
												'ASSET DEPRECIATION' AS `transactionClassification`,
												'account_payments' AS `transactionTable`,
												'' AS `referenceTable`
											FROM
												assets_details,asset_category,account,account_type,asset_amortization
												WHERE 
												assets_details.asset_category_id = asset_category.asset_category_id 
												AND asset_amortization.expense_account_id = account.account_id
												AND account.account_type_id = account_type.account_type_id
												AND asset_amortization.asset_id = assets_details.asset_id
												AND asset_amortization.bill_status = 1
												
										UNION ALL

										SELECT
												asset_category.asset_category_id AS `transactionId`,
												'' AS `referenceId`,
												'' AS `payingFor`,
												assets_details.asset_serial_no AS `referenceCode`,
												assets_details.asset_serial_no AS `transactionCode`,
												assets_details.asset_id AS `patient_id`,
													assets_details.supplier_id AS `recepientId`,
												account_type.account_type_id AS `accountParentId`,
												account_type.account_type_name AS `accountsclassfication`,
												`account`.`account_id` AS `accountId`,
												CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
												CONCAT(assets_details.asset_name) AS `transactionName`,
												CONCAT(assets_details.asset_name) AS `transactionDescription`,
												0 AS `dr_amount`,
												(asset_amortization.interest_amount) AS `cr_amount`,
												asset_amortization.date_approved AS `transactionDate`,
												asset_amortization.date_approved  AS `createdAt`,
												asset_amortization.date_approved AS `referenceDate`,
												1 AS `status`,
												'FIXED ASSET' AS `transactionCategory`,
												'ASSET DEPRECIATION' AS `transactionClassification`,
												'account_payments' AS `transactionTable`,
												'' AS `referenceTable`
											FROM
												assets_details,asset_category,account,account_type,asset_amortization
												WHERE 
												assets_details.asset_category_id = asset_category.asset_category_id 
												AND asset_amortization.acc_depreciation_id = account.account_id
												AND account.account_type_id = account_type.account_type_id
												AND asset_amortization.asset_id = assets_details.asset_id
												AND asset_amortization.bill_status = 1


					) AS data WHERE ".$add."  data.transactionDate <> '0000-00-00' AND data.accountId in ('".implode("','", $array_leads)."')   ".$add_item."   ORDER BY data.transactionDate ASC

					";
	 

	  $query = $this->db->query($select);


	   return $query;

	}


	public function get_transactions_fixed_assets($array_leads,$checked = NULL)
	{
		
		
		$financial_year_date = date('Y')."-12-31";


		$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

		$patients = array();
		
		if($accounts_config_rs->num_rows() > 0)
		{
			foreach ($accounts_config_rs->result() as $key => $value) {
				// code...
				$staing_account_id = $value->account_id;
				$reference_name = $value->reference_name;

				$session_account[$reference_name] = $staing_account_id;

				
			}
		}

		$providers_liability_id = $session_account['providers_liability_id'];
		$providers_wht_id = $session_account['providers_wht_id'];
		$payroll_liability_id = $session_account['payroll_liability_id'];
		$accounts_payable_id = $session_account['accounts_payable_id'];
		$fixed_assets_id = $session_account['fixed_assets_id'];
		$accounts_receivable_id = $session_account['accounts_receivable_id'];
		$suppliers_wht_id = $session_account['suppliers_wht_id'];
		$income_account_id = $session_account['income_account_id'];
		$supplier_opening_balance_id= $session_account['supplier_opening_balance_id'];
		$provider_opening_balance_id= $session_account['provider_opening_balance_id'];
		$bank_opening_balance_id = $session_account['bank_opening_balance_id'];

		$financial_year_date = "2021-12-31";
		// $date_from = $this->session->userdata('date_from_general_ledger');
		// $date_to = $this->session->userdata('date_to_general_ledger');
		$add = '';

		// if(!empty($date_from) OR !empty($date_to))
	  //   	{
	    		
	  //   		$add =  ' DATE(data.transactionDate) > \''.$date_from.'\' AND DATE(data.transactionDate) <= \''.$date_to.'\' AND ';
	    		
	    		
	  //   	}
	    	
	    	

	    		
	    
	    	
	    	


	   //  	if($checked == 1)
	   //  	{
	   //  		$item = '	
	   //  					data.accountId AS accountId,
				// 			data.accountName AS account_name,
				// 		 	SUM(data.dr_amount) AS dr_amount,
				// 		 	SUM(data.cr_amount) AS cr_amount';

				// $add_item = 'GROUP BY data.accountId';
	   //  	}
	   //  	else
	   //  	{
	    		$item = '*';

				$add_item = '';
	    	// }
		
    	
		$select  = "
					SELECT 
						".$item."
					FROM 

					(

						SELECT
							`account`.`account_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
						    '' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							account.account_id AS `accountId`,
							account.account_name AS `accountName`,
							CONCAT('Opening Balance as from',' ',`account`.`start_date`) AS `transactionName`,
							CONCAT('Opening Balance as from',' ',' ',`account`.`start_date`) AS `transactionDescription`,
							`account`.`account_opening_balance` AS `dr_amount`,
							'0' AS `cr_amount`,
							`account`.`start_date` AS `transactionDate`,
							`account`.`start_date` AS `createdAt`,
							`account`.`account_status` AS `status`,
							branch_id AS `branch_id`,
							'Income' AS `transactionCategory`,
							'Account Opening Balance' AS `transactionClassification`,
							'' AS `transactionTable`,
							'account' AS `referenceTable`,
							'' AS category_id
						FROM
						account,account_type

						WHERE 

						account_type.account_type_id = account.account_type_id AND account.account_opening_balance > 0 AND account.parent_account > 0  AND (account_type.account_type_name = 'Bank' OR account_type.account_type_name ='Capital')

						UNION ALL

	
						SELECT
							`account`.`account_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							account.account_id AS `accountId`,
							account.account_name AS `accountName`,
							CONCAT( 'Opening Balance as from', ' ', `account`.`start_date` ) AS `transactionName`,
							CONCAT( 'Opening Balance as from', ' ', ' ', `account`.`start_date` ) AS `transactionDescription`,
							0 AS `dr_amount`,
							(SELECT COALESCE(SUM(account_opening_balance),0) FROM account,account_type WHERE  account_type.account_type_id = account.account_type_id  AND account.account_opening_balance > 0  AND (
							account_type.account_type_name = 'Bank' 
							OR account_type.account_type_name = 'Capital') ) AS `cr_amount`,
							`account`.`start_date` AS `transactionDate`,
							`account`.`start_date` AS `createdAt`,
							`account`.`account_status` AS `status`,
							branch_id AS `branch_id`,
							'Income' AS `transactionCategory`,
							'Account Opening Balance' AS `transactionClassification`,
							'' AS `transactionTable`,
							'account' AS `referenceTable`,
							'' AS category_id 
						FROM
							account,
							account_type 
						WHERE
							account_type.account_type_id = account.account_type_id 
							AND account.parent_account > 0 
							AND account.account_id = ".$bank_opening_balance_id." 



						UNION ALL

						SELECT
						  	`finance_transfered`.`finance_transfered_id` AS `transactionId`,
						  	`finance_transfer`.`finance_transfer_id` AS `referenceId`,
						  	'' AS `payingFor`,
						  	`finance_transfer`.`reference_number` AS `referenceCode`,
						  	`finance_transfer`.`document_number` AS `transactionCode`,
						  	'' AS `patient_id`,
						    '' AS `recepientId`,
						  	`account`.`parent_account` AS `accountParentId`,
						  	`account_type`.`account_type_name` AS `accountsclassfication`,
						  	`finance_transfered`.`account_to_id` AS `accountId`,
						  	`account`.`account_name` AS `accountName`,
						  	`finance_transfered`.`remarks` AS `transactionName`,
						  	 CONCAT('Amount Received from ',(SELECT account_name FROM account WHERE account_id = finance_transfer.account_from_id ),' Ref. ', `finance_transfer`.`reference_number`) AS `transactionDescription`,
						  	`finance_transfer`.`finance_transfer_amount` AS `cr_amount`,
						     0 AS `cr_amount`,
						  	`finance_transfer`.`transaction_date` AS `transactionDate`,
						  	`finance_transfer`.`created` AS `createdAt`,
						  	`finance_transfer`.`finance_transfer_status` AS `status`,
						  	`finance_transfer`.`branch_id` AS `branch_id`,
						  	'Transfer' AS `transactionCategory`,
						  	'Transfer' AS `transactionClassification`,
						  	'finance_transfer' AS `transactionTable`,
						  	'finance_transfered' AS `referenceTable`,
						  	'' AS category_id
						  FROM
						  `finance_transfer`,finance_transfered,account,account_type
						   WHERE finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id 
						  	AND account.account_id = finance_transfered.account_to_id
						  	AND account_type.account_type_id = account.account_type_id
						  	AND finance_transfer.finance_transfer_deleted = 0

					UNION ALL

						SELECT
						  	`finance_transfer`.`finance_transfer_id` AS `transactionId`,
						  	`finance_transfered`.`finance_transfered_id` AS `referenceId`,
						  	'' AS `payingFor`,
						  	`finance_transfer`.`reference_number` AS `referenceCode`,
						  	`finance_transfer`.`document_number` AS `transactionCode`,
						  	'' AS `patient_id`,
						    '' AS `recepientId`,
						  	`account`.`parent_account` AS `accountParentId`,
						  	`account_type`.`account_type_name` AS `accountsclassfication`,
						  	`finance_transfer`.`account_from_id` AS `accountId`,
						  	`account`.`account_name` AS `accountName`,
						  	`finance_transfer`.`remarks` AS `transactionName`,
						  	CONCAT(' Amount Transfered to ',(SELECT account_name FROM account WHERE account_id = finance_transfered.account_to_id )) AS `transactionDescription`,
						  	0 AS `dr_amount`,
						  	`finance_transfer`.`finance_transfer_amount` AS `cr_amount`,
						  	`finance_transfer`.`transaction_date` AS `transactionDate`,
						  	`finance_transfer`.`transaction_date` AS `transactionDate`,

						  	`finance_transfer`.`finance_transfer_status` AS `status`,
						  	`finance_transfer`.`branch_id` AS `branch_id`,
						  	'Transfer' AS `transactionCategory`,
						  	'Transfer' AS `transactionClassification`,
						  	'finance_transfered' AS `transactionTable`,
						  	'finance_transfer' AS `referenceTable`,
						  	'' AS category_id
						  FROM
							`finance_transfer`,finance_transfered,account,account_type
						  				
						  WHERE finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id 
						  	AND account.account_id = finance_transfer.account_from_id
						  	AND account_type.account_type_id = account.account_type_id
						  	AND finance_transfer.finance_transfer_deleted = 0

						UNION ALL


						SELECT
							`finance_purchase_payment`.`finance_purchase_payment_id` AS `transactionId`,
							'' AS `referenceId`,
							`finance_purchase`.`finance_purchase_id` AS `payingFor`,
							`finance_purchase`.`transaction_number` AS `referenceCode`,
							`finance_purchase`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
						  	finance_purchase.creditor_id AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`finance_purchase_payment`.`account_from_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`finance_purchase`.`finance_purchase_description` AS `transactionName`,
							CONCAT( `finance_purchase`.`finance_purchase_description`) AS `transactionDescription`,
							0 AS `dr_amount`,
							`finance_purchase_payment`.`amount_paid` AS `cr_amount`,
							`finance_purchase`.`transaction_date` AS `transactionDate`,
							`finance_purchase`.`created` AS `createdAt`,
							`finance_purchase_payment`.`finance_purchase_payment_status` AS `status`,
							`finance_purchase`.`branch_id` AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Purchase Payment' AS `transactionClassification`,
							'finance_purchase' AS `transactionTable`,
							'finance_purchase_payment' AS `referenceTable`,
							'' AS category_id
						FROM
						`finance_purchase_payment`,finance_purchase,account,account_type
						WHERE finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id AND finance_purchase.finance_purchase_deleted = 0 AND account.account_id = finance_purchase_payment.account_from_id
							AND finance_purchase.finance_purchase_delete = 0 AND account_type.account_type_id = account.account_type_id
							AND finance_purchase.account_to_id > 0 

						UNION ALL

						SELECT
							`finance_purchase`.`finance_purchase_id` AS `transactionId`,
							'' AS `referenceId`,
							`finance_purchase`.`finance_purchase_id` AS `payingFor`,
							`finance_purchase`.`transaction_number` AS `referenceCode`,
							`finance_purchase`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`finance_purchase`.`creditor_id` AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`finance_purchase`.`account_to_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`finance_purchase`.`finance_purchase_description` AS `transactionName`,
							CONCAT( `finance_purchase`.`finance_purchase_description`)  AS `transactionDescription`,
							`finance_purchase_payment`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`finance_purchase`.`transaction_date` AS `transactionDate`,
							`finance_purchase`.`created` AS `createdAt`,
							`finance_purchase`.`finance_purchase_status` AS `status`,
							`finance_purchase`.`branch_id` AS `branch_id`,
							'Expense' AS `transactionCategory`,
							'Purchases' AS `transactionClassification`,
							'finance_purchase' AS `transactionTable`,
							'' AS `referenceTable`,
							'' AS category_id
						FROM
							`finance_purchase_payment`,finance_purchase,account,account_type
						WHERE finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id AND finance_purchase.finance_purchase_deleted = 0 
						AND account.account_id = finance_purchase.account_to_id
							AND finance_purchase.finance_purchase_delete = 0
							AND account_type.account_type_id = account.account_type_id
							AND finance_purchase.account_to_id > 0 

						UNION ALL

					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor`.`opening_balance` AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`,
						'' AS category_id
					FROM
					creditor,account,account_type
					WHERE debit_id = 1 AND account.account_id = ".$supplier_opening_balance_id." AND account.account_type_id = account_type.account_type_id

					UNION ALL

					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						`creditor`.`opening_balance` AS `dr_amount`,
						'0' AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						 creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`,
						'' AS category_id
					FROM
					creditor,account,account_type
					WHERE debit_id = 2 AND account.account_id = ".$supplier_opening_balance_id." AND account.account_type_id = account_type.account_type_id


					UNION ALL


					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						`creditor`.`opening_balance` AS `dr_amount`,
						0 AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`,
						'' AS category_id
					FROM
					creditor,account,account_type
					WHERE debit_id = 1 AND account.account_id = ".$accounts_payable_id." AND account.account_type_id = account_type.account_type_id

					UNION ALL

					SELECT
						`creditor`.`creditor_id` AS `transactionId`,
						'' AS `referenceId`,
						'' AS `payingFor`,
						'' AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					    `creditor`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
				  		`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						account.account_name AS `accountName`,
						'' AS `transactionName`,
						CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor`.`opening_balance`  AS `cr_amount`,
						`creditor`.`start_date` AS `transactionDate`,
						`creditor`.`start_date` AS `createdAt`,
						`creditor`.`creditor_status` AS `status`,
						 creditor.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Creditor Opening Balance' AS `transactionClassification`,
						'' AS `transactionTable`,
						'creditor' AS `referenceTable`,
						'' AS category_id
					FROM
					creditor,account,account_type
					WHERE debit_id = 2 AND account.account_id = ".$accounts_payable_id." AND account.account_type_id = account_type.account_type_id
				

					UNION ALL


					SELECT
						`creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
						`creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
						'' AS `payingFor`,
						`creditor_invoice`.`invoice_number` AS `referenceCode`,
						`creditor_invoice`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
					  `creditor_invoice`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`creditor_invoice_item`.`account_to_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_invoice_item`.`item_description` AS `transactionName`,
						CONCAT(`creditor_invoice_item`.`item_description`,' ',creditor_invoice.invoice_number) AS `transactionDescription`,
						`creditor_invoice_item`.`total_amount` AS `dr_amount`,
						'0' AS `cr_amount`,
						`creditor_invoice`.`transaction_date` AS `transactionDate`,
						`creditor_invoice`.`created` AS `createdAt`,
						`creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
						'creditor.branch_id' AS `branch_id`,
						'Cost of Goods' AS `transactionCategory`,
						'Creditors Invoices' AS `transactionClassification`,
						'creditor_invoice_item' AS `transactionTable`,
						'creditor_invoice' AS `referenceTable`,
						'' AS category_id
					FROM
						(
							(
								(
									`creditor_invoice_item`,creditor_invoice,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = creditor_invoice_item.account_to_id
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)
						)
					WHERE 

					creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id 
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date

				UNION ALL

				  SELECT
					`creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
					`creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
					`creditor_credit_note_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_credit_note`.`invoice_number` AS `referenceCode`,
					`creditor_credit_note`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_credit_note`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`creditor_credit_note`.`account_from_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_credit_note_item`.`description` AS `transactionName`,
					`creditor_credit_note_item`.`description` AS `transactionDescription`,
					0 AS `dr_amount`,
					`creditor_credit_note_item`.`credit_note_amount` AS `cr_amount`,
					`creditor_credit_note`.`transaction_date` AS `transactionDate`,
					`creditor_credit_note`.`created` AS `createdAt`,
					`creditor_invoice`.`transaction_date` AS `referenceDate`,
					`creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
					'Supplier Credit Note' AS `transactionCategory`,
					'Creditors Credit Notes' AS `transactionClassification`,
					'creditor_credit_note' AS `transactionTable`,
					'creditor_credit_note_item' AS `referenceTable`,
					'' AS category_id
				FROM
					(
						(
							(
								`creditor_credit_note_item`,creditor_credit_note,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = creditor_credit_note.account_from_id
								)
							)
							
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					)
				WHERE 
					creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id 
					AND creditor_credit_note.creditor_credit_note_status = 1
					AND creditor_invoice.creditor_invoice_id = creditor_credit_note_item.creditor_invoice_id
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date


				UNION ALL


				SELECT
						`creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
						`creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
						'' AS `payingFor`,
						`creditor_invoice`.`invoice_number` AS `referenceCode`,
						`creditor_invoice`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
					  `creditor_invoice`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`account`.`account_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_invoice_item`.`item_description` AS `transactionName`,
						CONCAT(`creditor_invoice_item`.`item_description`,' ',creditor_invoice.invoice_number) AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor_invoice_item`.`total_amount` AS `cr_amount`,
						`creditor_invoice`.`transaction_date` AS `transactionDate`,
						`creditor_invoice`.`created` AS `createdAt`,
						`creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
						'creditor.branch_id' AS `branch_id`,
						'Cost of Goods' AS `transactionCategory`,
						'Creditors Invoices' AS `transactionClassification`,
						'creditor_invoice_item' AS `transactionTable`,
						'creditor_invoice' AS `referenceTable`,
						'' AS category_id
					FROM
						(
							(
								(
									`creditor_invoice_item`,creditor_invoice,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = ".$accounts_payable_id."
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)
						)
					WHERE 

					creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id 
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date

				UNION ALL

				  SELECT
					`creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
					`creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
					`creditor_credit_note_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_credit_note`.`invoice_number` AS `referenceCode`,
					`creditor_credit_note`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_credit_note`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`account`.`account_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_credit_note_item`.`description` AS `transactionName`,
					`creditor_credit_note_item`.`description` AS `transactionDescription`,
					`creditor_credit_note_item`.`credit_note_amount` AS `dr_amount`,
					0 AS `cr_amount`,
					`creditor_credit_note`.`transaction_date` AS `transactionDate`,
					`creditor_credit_note`.`created` AS `createdAt`,
					`creditor_invoice`.`transaction_date` AS `referenceDate`,
					`creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
					'Supplier Credit Note' AS `transactionCategory`,
					'Creditors Credit Notes' AS `transactionClassification`,
					'creditor_credit_note' AS `transactionTable`,
					'creditor_credit_note_item' AS `referenceTable`,
					'' AS category_id
				FROM
					(
						(
							(
								`creditor_credit_note_item`,creditor_credit_note,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = ".$accounts_payable_id."
								)
							)
							
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					)
				WHERE 
					creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id 
					AND creditor_credit_note.creditor_credit_note_status = 1
					AND creditor_invoice.creditor_invoice_id = creditor_credit_note_item.creditor_invoice_id
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor.creditor_id = creditor_invoice.creditor_id 
					AND creditor_invoice.transaction_date >= creditor.start_date



				UNION ALL


				SELECT
					`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
					`creditor_payment`.`creditor_payment_id` AS `referenceId`,
					`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_payment`.`reference_number` AS `referenceCode`,
					`creditor_payment`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_payment`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`creditor_payment`.`account_from_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_payment_item`.`description` AS `transactionName`,
					CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
					0 AS `dr_amount`,
					`creditor_payment_item`.`amount_paid` AS `cr_amount`,
					`creditor_payment`.`transaction_date` AS `transactionDate`,
					`creditor_payment`.`created` AS `createdAt`,
					`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
					'creditor_payment.branch_id' AS `branch_id`,
					'Creditor Payment' AS `transactionCategory`,
					'Creditors Invoices Payments' AS `transactionClassification`,
					'creditor_payment' AS `transactionTable`,
					'creditor_payment_item' AS `referenceTable`,
					'' AS category_id
				FROM
					(
						(
							(
								`creditor_payment_item`,creditor_payment,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = creditor_payment.account_from_id
								)
							)
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					
					)
					WHERE creditor_payment_item.invoice_type = 0 
					AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
					AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
					AND creditor_payment.creditor_payment_status = 1
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor_invoice.creditor_id = creditor.creditor_id
					AND creditor_payment.transaction_date >= creditor.start_date

				UNION ALL


				SELECT
					`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
					`creditor_payment`.`creditor_payment_id` AS `referenceId`,
					`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_payment`.`reference_number` AS `referenceCode`,
					`creditor_payment`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
				  	`creditor_payment`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`account`.`account_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_payment_item`.`description` AS `transactionName`,
					CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
					`creditor_payment_item`.`amount_paid` AS `dr_amount`,
					0 AS `cr_amount`,
					`creditor_payment`.`transaction_date` AS `transactionDate`,
					`creditor_payment`.`created` AS `createdAt`,
					`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
					'creditor_payment.branch_id' AS `branch_id`,
					'Creditor Payment' AS `transactionCategory`,
					'Creditors Invoices Payments' AS `transactionClassification`,
					'creditor_payment' AS `transactionTable`,
					'creditor_payment_item' AS `referenceTable`,
					'' AS category_id
				FROM
					(
						(
							(
								`creditor_payment_item`,creditor_payment,creditor_invoice,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = ".$accounts_payable_id."
								)
							)
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
					
					)
					WHERE creditor_payment_item.invoice_type = 0 
					AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
					AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
					AND creditor_payment.creditor_payment_status = 1
					AND creditor_invoice.creditor_invoice_status = 1
					AND creditor_invoice.creditor_id = creditor.creditor_id
					AND creditor_payment.transaction_date >= creditor.start_date

				UNION ALL

					SELECT
						`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
						`creditor_payment`.`creditor_payment_id` AS `referenceId`,
						`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
						`creditor_payment`.`reference_number` AS `referenceCode`,
						`creditor_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
						`creditor_payment`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`creditor_payment`.`account_from_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_payment_item`.`description` AS `transactionName`,
						CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor_payment_item`.`amount_paid` AS `cr_amount`,
						`creditor_payment`.`transaction_date` AS `transactionDate`,
						`creditor_payment`.`created` AS `createdAt`,
						`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
						'creditor_payment.branch_id' AS `branch_id`,
						'Creditor Payment' AS `transactionCategory`,
						'Creditors Invoices Payments' AS `transactionClassification`,
						'creditor_payment' AS `transactionTable`,
						'creditor_payment_item' AS `referenceTable`,
						'' AS category_id
						FROM
						(
							(
								(
									`creditor_payment_item`,creditor_payment,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = creditor_payment.account_from_id
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)

						)
						WHERE creditor_payment_item.invoice_type = 2 
						AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
						AND creditor.creditor_id = creditor_payment_item.creditor_id
						AND creditor_payment.transaction_date >= creditor.start_date

				UNION ALL

					SELECT
						`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
						`creditor_payment`.`creditor_payment_id` AS `referenceId`,
						`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
						`creditor_payment`.`reference_number` AS `referenceCode`,
						`creditor_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
						`creditor_payment`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`account`.`account_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_payment_item`.`description` AS `transactionName`,
						CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
						`creditor_payment_item`.`amount_paid` AS `dr_amount`,
						0 AS `cr_amount`,
						`creditor_payment`.`transaction_date` AS `transactionDate`,
						`creditor_payment`.`created` AS `createdAt`,
						`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
						'creditor_payment.branch_id' AS `branch_id`,
						'Creditor Payment' AS `transactionCategory`,
						'Creditors Invoices Payments' AS `transactionClassification`,
						'creditor_payment' AS `transactionTable`,
						'creditor_payment_item' AS `referenceTable`,
						'' AS category_id
						FROM
						(
							(
								(
									`creditor_payment_item`,creditor_payment,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = ".$accounts_payable_id."
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)

						)
						WHERE creditor_payment_item.invoice_type = 2 
						AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
						AND creditor.creditor_id = creditor_payment_item.creditor_id
						AND creditor_payment.transaction_date >= creditor.start_date

					UNION ALL

						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`creditor_payment`.`account_from_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							0 AS `dr_amount`,
							`creditor_payment_item`.`amount_paid` AS `cr_amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							'creditor_payment.branch_id' AS `branch_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`,
							'' AS category_id
							FROM
							(
								(
									(
										`creditor_payment_item`,creditor_payment,creditor
										
									)
									JOIN account ON(
										(
											account.account_id = creditor_payment.account_from_id
										)
									)
								)
								JOIN `account_type` ON(
									(
										account_type.account_type_id = account.account_type_id
									)
								)
								
							)
							WHERE creditor_payment_item.invoice_type = 3
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
							AND creditor.creditor_id = creditor_payment_item.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date
					UNION ALL

						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							`account`.`account_name` AS `accountName`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							`creditor_payment_item`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							'creditor_payment.branch_id' AS `branch_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`,
							'' AS category_id
							FROM
							(
								(
									(
										`creditor_payment_item`,creditor_payment,creditor
										
									)
									JOIN account ON(
										(
											account.account_id = ".$accounts_payable_id."
										)
									)
								)
								JOIN `account_type` ON(
									(
										account_type.account_type_id = account.account_type_id
									)
								)
								
							)
							WHERE creditor_payment_item.invoice_type = 3
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
							AND creditor.creditor_id = creditor_payment_item.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date


						UNION ALL

							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							 	`orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Drug Purchase' AS `transactionName`,
								CONCAT('Purchase of supplies',' ',orders.supplier_invoice_number) AS `transactionDescription`,
								SUM(`order_supplier`.`less_vat`) AS `dr_amount`,
								0 AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Purchases' AS `transactionCategory`,
								'Supplies Invoices' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`,
								'' AS category_id
							FROM
								`order_supplier`,orders,account,order_item,product,account_type,creditor
										

							WHERE orders.is_store = 0 
								AND orders.supplier_id = creditor.creditor_id
								AND orders.order_id = order_supplier.order_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND product.product_id = order_item.product_id
								AND account_type.account_type_id = account.account_type_id
								AND account.account_id = orders.account_id
								AND orders.order_approval_status = 7 
								AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id

							UNION ALL



							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							 	`orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Drug Purchase' AS `transactionName`,
								CONCAT('Purchase of supplies',' ',orders.supplier_invoice_number) AS `transactionDescription`,
								0 AS `dr_amount`,
								SUM(`order_supplier`.`less_vat`) AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Purchases' AS `transactionCategory`,
								'Supplies Invoices' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`,
								'' AS category_id
							FROM
								`order_supplier`,orders,account,order_item,product,account_type,creditor
										

							WHERE orders.is_store = 0 
								AND orders.supplier_id = creditor.creditor_id
								AND orders.order_id = order_supplier.order_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND product.product_id = order_item.product_id
								AND account_type.account_type_id = account.account_type_id
								AND account.account_id = ".$accounts_payable_id."
								AND orders.order_approval_status = 7 
								AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id

							UNION ALL

							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								`orders`.`reference_number`  AS `transactionCode`,
								'' AS `patient_id`,
							  `orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`orders`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Credit' AS `transactionName`,
								CONCAT('Credit note of ',' ',`orders`.`reference_number`) AS `transactionDescription`,
								'0' AS `dr_amount`,
								SUM(`order_supplier`.`less_vat`) AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Income' AS `transactionCategory`,
								'Supplies Credit Note' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`,
								'' AS category_id

							FROM
								`order_supplier`,orders,order_item,product,account,account_type,creditor
								
								WHERE orders.is_store = 3
								AND orders.order_id = order_supplier.order_id
								AND account.account_id = orders.account_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND product.product_id = order_item.product_id
								AND account_type.account_type_id = account.account_type_id
								 AND orders.supplier_id = creditor.creditor_id and orders.order_approval_status = 7 AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								 AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id

							UNION ALL

							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`orders`.`order_id` AS `referenceId`,
								'' AS `payingFor`,
								`orders`.`supplier_invoice_number` AS `referenceCode`,
								`orders`.`reference_number`  AS `transactionCode`,
								'' AS `patient_id`,
							  `orders`.`supplier_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								'Credit' AS `transactionName`,
								CONCAT('Credit note of ',' ',`orders`.`supplier_invoice_number`) AS `transactionDescription`,
								SUM(`order_supplier`.`less_vat`)  AS `dr_amount`,
								0 AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`orders`.`created` AS `createdAt`,
								`orders`.`supplier_invoice_date` AS `referenceDate`,
								`orders`.`order_approval_status` AS `status`,
								'Income' AS `transactionCategory`,
								'Supplies Credit Note' AS `transactionClassification`,
								'order_supplier' AS `transactionTable`,
								'orders' AS `referenceTable`,
								'' AS category_id

							FROM
								`order_supplier`,orders,account,order_item,product,account_type,creditor
											

								WHERE orders.is_store = 3
								AND account.account_id = ".$accounts_payable_id."
								AND account_type.account_type_id = account.account_type_id
								AND product.product_id = order_item.product_id
								AND orders.order_id = order_supplier.order_id
								AND order_item.order_item_id = order_supplier.order_item_id
								AND orders.supplier_id = creditor.creditor_id and orders.order_approval_status = 7 AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
								AND orders.supplier_invoice_date >= creditor.start_date
								GROUP BY order_supplier.order_id



							UNION ALL

							 SELECT
								`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
								`creditor_payment`.`creditor_payment_id` AS `referenceId`,
								`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
								`creditor_payment`.`reference_number` AS `referenceCode`,
								`creditor_payment`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`creditor_payment`.`creditor_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								creditor_payment.account_from_id AS `accountId`,
								`account`.`account_name` AS `accountName`,
								`creditor_payment_item`.`description` AS `transactionName`,
								CONCAT('Payment to ', creditor.creditor_name ,'for ',`orders`.`supplier_invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
								0 AS `dr_amount`,
								`creditor_payment_item`.`amount_paid` AS `cr_amount`,
								`creditor_payment`.`transaction_date` AS `transactionDate`,
								`creditor_payment`.`created` AS `createdAt`,
								`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
								'creditor_payment.branch_id' AS `branch_id`,
								'Creditor Payment' AS `transactionCategory`,
								'Creditors Invoices Payments' AS `transactionClassification`,
								'creditor_payment' AS `transactionTable`,
								'creditor_payment_item' AS `referenceTable`,
								'' AS category_id
								FROM
								(
									(
										(
											`creditor_payment_item`,creditor_payment,orders,creditor
											
										)
										JOIN account ON(
											(
												account.account_id = creditor_payment.account_from_id
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
									
								)
							WHERE creditor_payment_item.invoice_type = 1 
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id  
							AND creditor_payment.creditor_payment_status = 1 
							AND orders.order_id = creditor_payment_item.creditor_invoice_id
							AND orders.supplier_id = creditor.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date

							UNION ALL

							SELECT
								`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
								`creditor_payment`.`creditor_payment_id` AS `referenceId`,
								`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
								`creditor_payment`.`reference_number` AS `referenceCode`,
								`creditor_payment`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`creditor_payment`.`creditor_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								`account`.`account_name` AS `accountName`,
								`creditor_payment_item`.`description` AS `transactionName`,
								CONCAT('Payment to ', creditor.creditor_name ,'for ',`orders`.`supplier_invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
								`creditor_payment_item`.`amount_paid` AS `dr_amount`,
								0 AS `cr_amount`,
								`creditor_payment`.`transaction_date` AS `transactionDate`,
								`creditor_payment`.`created` AS `createdAt`,
								`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
								'creditor_payment.branch_id' AS `branch_id`,
								'Creditor Payment' AS `transactionCategory`,
								'Creditors Invoices Payments' AS `transactionClassification`,
								'creditor_payment' AS `transactionTable`,
								'creditor_payment_item' AS `referenceTable`,
								'' AS category_id
								FROM
								(
									(
										(
											`creditor_payment_item`,creditor_payment,orders,creditor
											
										)
										JOIN account ON(
											(
												account.account_id = ".$accounts_payable_id."
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
									
								)
							WHERE creditor_payment_item.invoice_type = 1 
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id  
							AND creditor_payment.creditor_payment_status = 1 
							AND orders.order_id = creditor_payment_item.creditor_invoice_id
							AND orders.supplier_id = creditor.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date
								

					

						UNION ALL




						SELECT

							bank_reconcilliation.recon_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.account_name AS `accountName`,
							'' AS `transactionName`,
							CONCAT('Bank Charges ', account.account_name ) AS `transactionDescription`,
							0 AS `dr_amount`,
							SUM(bank_reconcilliation.service_charged) AS `cr_amount`,
							bank_reconcilliation.charged_date AS `transactionDate`,
							bank_reconcilliation.charged_date AS `createdAt`,
							bank_reconcilliation.recon_status AS `status`,
							2 AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`,
							'' AS category_id
						FROM
							bank_reconcilliation,account,account_type
							WHERE bank_reconcilliation.recon_status = 2
							AND bank_reconcilliation.account_id = account.account_id
							AND account.account_type_id = account_type.account_type_id
						GROUP BY bank_reconcilliation.recon_id



						UNION ALL


						SELECT

							bank_reconcilliation.recon_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.account_name AS `accountName`,
							'' AS `transactionName`,
							CONCAT('Bank Charges ', account.account_name ) AS `transactionDescription`,
							SUM(bank_reconcilliation.service_charged) AS `dr_amount`,
							0 AS `cr_amount`,
							bank_reconcilliation.charged_date AS `transactionDate`,
							bank_reconcilliation.charged_date AS `createdAt`,
							bank_reconcilliation.recon_status AS `status`,
							2 AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`,
							'' AS category_id
						FROM
							bank_reconcilliation,account,account_type
							WHERE bank_reconcilliation.recon_status = 2
							AND bank_reconcilliation.expense_account_id = account.account_id
							AND account.account_type_id = account_type.account_type_id
						GROUP BY bank_reconcilliation.recon_id

						UNION ALL 

						SELECT


							`journal_entry`.`journal_entry_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							journal_entry.document_number AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							journal_entry.account_from_id AS `accountId`,
							account.account_name AS `accountName`,
							'Journal Entry ' AS `transactionName`,
							CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
							`journal_entry`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`journal_entry`.`payment_date` AS `transactionDate`,
							`journal_entry`.`payment_date` AS `createdAt`,
							`journal_entry`.`journal_entry_status` AS `status`,
							2 AS `branch_id`,
							'Journal Credit' AS `transactionCategory`,
							'Journal' AS `transactionClassification`,
							'journal_entry' AS `transactionTable`,
							'account' AS `referenceTable`,
							'' AS category_id
						FROM
						journal_entry,account,account_type
						
						WHERE journal_entry.account_from_id  = account.account_id AND journal_entry.journal_entry_deleted = 0 
						AND account_type.account_type_id = account.account_type_id



						UNION ALL 


						SELECT
							`journal_entry`.`journal_entry_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							journal_entry.document_number AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							account.parent_account AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
							journal_entry.account_to_id AS `accountId`,
							account.account_name AS `accountName`,
							'Journal Entry ' AS `transactionName`,
							CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
							0 AS `dr_amount`,
							`journal_entry`.`amount_paid` AS `cr_amount`,
							`journal_entry`.`payment_date` AS `transactionDate`,
							`journal_entry`.`payment_date` AS `createdAt`,
							`journal_entry`.`journal_entry_status` AS `status`,
							2 AS `branch_id`,
							'Journal Debit' AS `transactionCategory`,
							'Journal' AS `transactionClassification`,
							'journal_entry' AS `transactionTable`,
							'account' AS `referenceTable`,
							'' AS category_id
						FROM
						journal_entry,account,account_type
						
						WHERE journal_entry.account_to_id  = account.account_id AND journal_entry.journal_entry_deleted = 0 
						AND account_type.account_type_id = account.account_type_id

						


						UNION ALL


						SELECT
								visit_invoice.visit_invoice_id AS transactionId,
								visit_charge.visit_charge_id  AS referenceId,
								'' AS `payingFor`,
								visit_invoice.visit_invoice_number AS referenceCode,
								'' AS transactionCode,
								visit_invoice.patient_id AS patientId,
								'' AS recepientId,

								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
							    account.account_id AS `accountId`,
							    `account`.`account_name` AS `accountName`,
							   	service_charge.service_charge_name AS transactionName,
								CONCAT( '<strong>Invoice </strong>  Invoice Number :  ', visit_invoice.visit_invoice_number,' <strong>Bill to:</strong>  ', visit_type.visit_type_name ) AS transactionDescription,
								0 AS dr_amount,
								visit_charge.visit_charge_amount*visit_charge.visit_charge_units AS cr_amount,
							    visit_invoice.created AS `transactionDate`,
							    visit_invoice.created AS `createdAt`,
							    1 AS `status`,
							    2 AS `branch_id`,
							    service.service_name AS `transactionCategory`,
							    'PATIENT INVOICE' AS `transactionClassification`,
							    'visit_invoice' AS `transactionTable`,
							    'visit_charge' AS `referenceTable`,
							    '' AS category_id


						FROM
									visit_charge,visit_invoice,service_charge,visit_type,service,account,account_type,visit
									WHERE visit_charge.charged = 1  
									AND visit_invoice.visit_invoice_delete = 0
									AND visit_charge.visit_charge_delete = 0 
									AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id
									AND service_charge.service_charge_id = visit_charge.service_charge_id
									AND visit_type.visit_type_id = visit_invoice.bill_to
									AND service.service_id = service_charge.service_id
									AND account.account_id = service.account_id
									AND visit.visit_id = visit_charge.visit_id
									AND visit.visit_delete = 0
									AND account_type.account_type_id = account.account_type_id


						UNION ALL

						SELECT
							visit_credit_note.visit_credit_note_id AS transactionId,
							visit_invoice.visit_invoice_id AS referenceId,
							'' AS `payingFor`,
							visit_credit_note.visit_cr_note_number AS referenceCode,
							visit_credit_note.visit_cr_note_number AS transactionCode,
							visit_invoice.patient_id AS patient_id,
							'' AS recepientId,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
						    account.account_id AS `accountId`,
						    `account`.`account_name` AS `accountName`,
						    service_charge.service_charge_name AS transactionName,
							CONCAT('<strong>Credit Note for </strong>  Invoice Number: ', visit_invoice.visit_invoice_number,'<strong>Credit Note No:</strong> ',visit_credit_note.visit_cr_note_number) AS transactionDescription,
						   
							visit_credit_note_item.visit_cr_note_amount*visit_credit_note_item.visit_cr_note_units AS dr_amount,
							0 AS cr_amount,
						    visit_invoice.created AS `transactionDate`,
						    visit_credit_note.created AS `createdAt`,
						    1 AS `status`,
						    2 AS `branch_id`,
						    CONCAT(service.service_name,' Credit note') AS `transactionCategory`,
						    'INCOME CREDIT NOTE' AS `transactionClassification`,
						    'visit_invoice' AS `transactionTable`,
						    'visit_credit_note' AS `referenceTable`,
						    '' AS category_id
						FROM
							visit_credit_note_item,visit_credit_note,visit_invoice,service_charge,service,account,account_type
							WHERE visit_credit_note.visit_cr_note_delete = 0 AND visit_credit_note_item.visit_cr_note_item_delete = 0 

							AND visit_invoice.visit_invoice_id = visit_credit_note.visit_invoice_id 
							AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id 
							AND service_charge.service_charge_id = visit_credit_note_item.service_charge_id
							AND service.service_id = service_charge.service_id
							AND account.account_id = service.account_id
							AND account_type.account_type_id = account.account_type_id



						UNION ALL



						SELECT
								visit_invoice.visit_invoice_id AS transactionId,
								visit_charge.visit_charge_id  AS referenceId,
								'' AS `payingFor`,
								visit_invoice.visit_invoice_number AS referenceCode,
								'' AS transactionCode,
								visit_invoice.patient_id AS patientId,
								'' AS recepientId,

								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
							    account.account_id AS `accountId`,
							    `account`.`account_name` AS `accountName`,
							   	service_charge.service_charge_name AS transactionName,
								CONCAT( '<strong>Invoice </strong>  Invoice Number :  ', visit_invoice.visit_invoice_number,' <strong>Bill to:</strong>  ', visit_type.visit_type_name ) AS transactionDescription,
								visit_charge.visit_charge_amount*visit_charge.visit_charge_units AS dr_amount,
								0 AS cr_amount,
							    visit_invoice.created AS `transactionDate`,
							    visit_invoice.created AS `createdAt`,
							    1 AS `status`,
							    2 AS `branch_id`,
							    service.service_name AS `transactionCategory`,
							    'PATIENT INVOICE' AS `transactionClassification`,
							    'visit_invoice' AS `transactionTable`,
							    'visit_charge' AS `referenceTable`,
							    '' AS category_id


						FROM
									visit_charge,visit_invoice,service_charge,visit_type,service,account,account_type,visit
									WHERE visit_charge.charged = 1  
									AND visit_invoice.visit_invoice_delete = 0
									AND visit_charge.visit_charge_delete = 0 
									AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id
									AND service_charge.service_charge_id = visit_charge.service_charge_id
									AND visit_type.visit_type_id = visit_invoice.bill_to
									AND service.service_id = service_charge.service_id
									AND visit.visit_id = visit_charge.visit_id
									AND visit.visit_delete = 0
									AND account.account_id = ".$accounts_receivable_id."
									AND account_type.account_type_id = account.account_type_id


						UNION ALL

						SELECT
							visit_credit_note.visit_credit_note_id AS transactionId,
							visit_invoice.visit_invoice_id AS referenceId,
							'' AS `payingFor`,
							visit_credit_note.visit_cr_note_number AS referenceCode,
							visit_credit_note.visit_cr_note_number AS transactionCode,
							visit_invoice.patient_id AS patient_id,
							'' AS recepientId,
							`account`.`parent_account` AS `accountParentId`,
							`account_type`.`account_type_name` AS `accountsclassfication`,
						    account.account_id AS `accountId`,
						    `account`.`account_name` AS `accountName`,
						    service_charge.service_charge_name AS transactionName,
							CONCAT('<strong>Credit Note for </strong>  Invoice Number: ', visit_invoice.visit_invoice_number,'<strong>Credit Note No:</strong> ',visit_credit_note.visit_cr_note_number) AS transactionDescription,
						   
							0 AS dr_amount,
							visit_credit_note_item.visit_cr_note_amount*visit_credit_note_item.visit_cr_note_units  AS cr_amount,
						    visit_invoice.created AS `transactionDate`,
						    visit_credit_note.created AS `createdAt`,
						    1 AS `status`,
						    2 AS `branch_id`,
						    'INCOME CREDIT NOTE' AS `transactionCategory`,
						    'INCOME CREDIT NOTE' AS `transactionClassification`,
						    'visit_invoice' AS `transactionTable`,
						    'visit_credit_note' AS `referenceTable`,
						    '' AS category_id
						FROM
							visit_credit_note_item,visit_credit_note,visit_invoice,service_charge,service,account,account_type
							WHERE visit_credit_note.visit_cr_note_delete = 0 AND visit_credit_note_item.visit_cr_note_item_delete = 0 

							AND visit_invoice.visit_invoice_id = visit_credit_note.visit_invoice_id 
							AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id 
							AND service_charge.service_charge_id = visit_credit_note_item.service_charge_id
							AND service.service_id = service_charge.service_id
							AND account.account_id = ".$accounts_receivable_id."
							AND account_type.account_type_id = account.account_type_id


							UNION ALL


							SELECT

								payments.payment_id AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								payments.confirm_number AS `referenceCode`,
								payments.confirm_number AS `transactionCode`,
								payments.patient_id AS `patient_id`,
							  	'' AS `recepientId`,
								account_type.account_type_id AS `accountParentId`,
								account_type.account_type_name AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number) AS `transactionDescription`,
								SUM(payment_item.payment_item_amount) AS `dr_amount`,
								0 AS `cr_amount`,
								payments.payment_date AS `transactionDate`,
								payments.payment_date AS `createdAt`,
								`payments`.`cancel` AS `status`,
								2 AS `branch_id`,
								'Patient Income' AS `transactionCategory`,
								'Payments' AS `transactionClassification`,
								'account_payments' AS `transactionTable`,
								'' AS `referenceTable`,
								'' AS category_id
							FROM
								payments,payment_item,payment_method,account,visit_invoice,account_type
								WHERE payments.cancel = 0 AND payments.payment_type = 1 AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND payment_item.invoice_type = 1
								AND payments.payment_id = payment_item.payment_id 
								AND payments.payment_method_id = payment_method.payment_method_id
								AND payment_method.account_id = account.account_id
								AND account.account_type_id = account_type.account_type_id
							GROUP BY payment_item.payment_id


							UNION ALL 


							SELECT

								payments.payment_id AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								payments.confirm_number AS `referenceCode`,
								payments.confirm_number AS `transactionCode`,
								payments.patient_id AS `patient_id`,
							  	'' AS `recepientId`,
								account_type.account_type_id AS `accountParentId`,
								account_type.account_type_name AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								account.parent_account AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Payment On Account') AS `transactionDescription`,
								SUM(payment_item.payment_item_amount) AS `dr_amount`,
								0 AS `cr_amount`,
								payments.payment_date AS `transactionDate`,
								payments.payment_date AS `createdAt`,
								`payments`.`cancel` AS `status`,
								2 AS `branch_id`,
								'Patient Income' AS `transactionCategory`,
								'Payments' AS `transactionClassification`,
								'account_payments' AS `transactionTable`,
								'' AS `referenceTable`,
								'' AS category_id
							FROM
								payments,payment_item,payment_method,account,account_type
								WHERE payments.cancel = 0 AND payments.payment_type = 1 AND payment_item.invoice_type = 3
								AND payments.payment_id = payment_item.payment_id 
								AND payments.payment_method_id = payment_method.payment_method_id
								AND payment_method.account_id = account.account_id
								AND account.account_type_id = account_type.account_type_id
							GROUP BY payment_item.payment_id


							UNION ALL

						SELECT

							payments.payment_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							payments.confirm_number AS `referenceCode`,
							payments.confirm_number AS `transactionCode`,
							payments.patient_id AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.account_name AS `accountName`,
							'' AS `transactionName`,
							CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number) AS `transactionDescription`,
							0 AS `dr_amount`,
							SUM(payment_item.payment_item_amount) AS `cr_amount`,
							payments.payment_date AS `transactionDate`,
							payments.payment_date AS `createdAt`,
							`payments`.`cancel` AS `status`,
							2 AS `branch_id`,
							'Patient Income' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`,
							'' AS category_id
						FROM
							payments,payment_item,payment_method,account,visit_invoice,account_type
							WHERE payments.cancel = 0 AND payments.payment_type = 1 AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND payment_item.invoice_type = 1
							AND payments.payment_id = payment_item.payment_id 
							AND payments.payment_method_id = payment_method.payment_method_id
							AND account.account_id = ".$accounts_receivable_id."
							AND account.account_type_id = account_type.account_type_id
						GROUP BY payment_item.payment_id


						UNION ALL 


						SELECT

							payments.payment_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							payments.confirm_number AS `referenceCode`,
							payments.confirm_number AS `transactionCode`,
							payments.patient_id AS `patient_id`,
						  	'' AS `recepientId`,
							account_type.account_type_id AS `accountParentId`,
							account_type.account_type_name AS `accountsclassfication`,
							`account`.`account_id` AS `accountId`,
							account.parent_account AS `accountName`,
							'' AS `transactionName`,
							CONCAT('Payment On Account') AS `transactionDescription`,
							0 AS `dr_amount`,
							SUM(payment_item.payment_item_amount) AS `cr_amount`,
							payments.payment_date AS `transactionDate`,
							payments.payment_date AS `createdAt`,
							`payments`.`cancel` AS `status`,
							2 AS `branch_id`,
							'Patient Income' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`,
							'' AS category_id
						FROM
							payments,payment_item,payment_method,account,account_type
							WHERE payments.cancel = 0 AND payments.payment_type = 1 AND payment_item.invoice_type = 3
							AND payments.payment_id = payment_item.payment_id 
							AND payments.payment_method_id = payment_method.payment_method_id
							AND account.account_id = ".$accounts_receivable_id."
							AND account.account_type_id = account_type.account_type_id
						GROUP BY payment_item.payment_id


						UNION ALL

							SELECT
								`provider`.`provider_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							    `provider`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
				  				`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider`.`opening_balance` AS `cr_amount`,
								`provider`.`start_date` AS `transactionDate`,
								`provider`.`start_date` AS `createdAt`,
								`provider`.`provider_status` AS `status`,
								2 AS `branch_id`,
								'Expense' AS `transactionCategory`,
								'provider Opening Balance' AS `transactionClassification`,
								'' AS `transactionTable`,
								'provider' AS `referenceTable`,
								'' AS category_id
							FROM
							provider,account,account_type
							WHERE  debit_id = 1 AND account.account_id = ".$provider_opening_balance_id." AND account.account_type_id = account_type.account_type_id

							UNION ALL


							SELECT
								`provider`.`provider_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							    `provider`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
				  				`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
								`provider`.`opening_balance` AS `dr_amount`,
								0 AS `cr_amount`,
								`provider`.`start_date` AS `transactionDate`,
								`provider`.`start_date` AS `createdAt`,
								`provider`.`provider_status` AS `status`,
								2 AS `branch_id`,
								'Expense' AS `transactionCategory`,
								'provider Opening Balance' AS `transactionClassification`,
								'' AS `transactionTable`,
								'provider' AS `referenceTable`,
								'' AS category_id
							FROM
							provider,account,account_type
							WHERE  debit_id = 1 AND account.account_id = ".$providers_liability_id." AND account.account_type_id = account_type.account_type_id


							UNION ALL


							SELECT
								`provider`.`provider_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							    `provider`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
				  				`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
								`provider`.`opening_balance` AS `dr_amount`,
								0 AS `cr_amount`,
								`provider`.`start_date` AS `transactionDate`,
								`provider`.`start_date` AS `createdAt`,
								`provider`.`provider_status` AS `status`,
								2 AS `branch_id`,
								'Expense' AS `transactionCategory`,
								'provider Opening Balance' AS `transactionClassification`,
								'' AS `transactionTable`,
								'provider' AS `referenceTable`,
								'' AS category_id
							FROM
							provider,account,account_type
							WHERE  debit_id = 2 AND account.account_id = ".$provider_opening_balance_id." AND account.account_type_id = account_type.account_type_id



							UNION ALL


							SELECT
								`provider`.`provider_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
							    `provider`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
				  				`account_type`.`account_type_name` AS `accountsclassfication`,
								account.account_id AS `accountId`,
								account.account_name AS `accountName`,
								'' AS `transactionName`,
								CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider`.`opening_balance` AS `cr_amount`,
								`provider`.`start_date` AS `transactionDate`,
								`provider`.`start_date` AS `createdAt`,
								`provider`.`provider_status` AS `status`,
								2 AS `branch_id`,
								'Expense' AS `transactionCategory`,
								'provider Opening Balance' AS `transactionClassification`,
								'' AS `transactionTable`,
								'provider' AS `referenceTable`,
								'' AS category_id
							FROM
							provider,account,account_type
							WHERE  debit_id = 2 AND account.account_id = ".$providers_liability_id." AND account.account_type_id = account_type.account_type_id

							
						  UNION ALL

							SELECT
								`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
								`provider_invoice`.`provider_invoice_id` AS `referenceId`,
								'' AS `payingFor`,
								`provider_invoice`.`invoice_number` AS `referenceCode`,
								`provider_invoice`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  `provider_invoice`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`provider_invoice_item`.`account_to_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
								`provider_invoice_item`.`total_amount` AS `dr_amount`,
								'0' AS `cr_amount`,
								`provider_invoice`.`transaction_date` AS `transactionDate`,
								`provider_invoice`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
								'Expense' AS `transactionCategory`,
								'providers Invoices' AS `transactionClassification`,
								'provider_invoice_item' AS `transactionTable`,
								'provider_invoice' AS `referenceTable`,
								'' AS category_id
							FROM
								(
									(
										(
											`provider_invoice_item`,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = provider_invoice_item.account_to_id
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
							provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
							AND provider_invoice.provider_invoice_status = 1
							AND provider.provider_id = provider_invoice.provider_id 
							AND provider_invoice.transaction_date >= provider.start_date

						UNION ALL

						SELECT
								`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
								`provider_invoice`.`provider_invoice_id` AS `referenceId`,
								'' AS `payingFor`,
								`provider_invoice`.`invoice_number` AS `referenceCode`,
								`provider_invoice`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  `provider_invoice`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider_invoice_item`.`total_amount` AS `cr_amount`,
								`provider_invoice`.`transaction_date` AS `transactionDate`,
								`provider_invoice`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
								'Provider Liability' AS `transactionCategory`,
								'providers Invoices' AS `transactionClassification`,
								'provider_invoice_item' AS `transactionTable`,
								'provider_invoice' AS `referenceTable`,
								'' AS category_id
							FROM
								(
									(
										(
											`provider_invoice_item`,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = ".$providers_liability_id."
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
							provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
							AND provider_invoice.provider_invoice_status = 1
							AND provider.provider_id = provider_invoice.provider_id 
							AND provider_invoice.transaction_date >= provider.start_date


							UNION ALL

							SELECT
								`provider_credit_note_item`.`provider_credit_note_item_id` AS `transactionId`,
								`provider_credit_note`.`provider_credit_note_id` AS `referenceId`,
								`provider_credit_note_item`.`provider_invoice_id` AS `payingFor`,
								`provider_credit_note`.`invoice_number` AS `referenceCode`,
								`provider_credit_note`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  	`provider_credit_note`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`provider_credit_note`.`account_from_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider_credit_note_item`.`credit_note_amount` AS `cr_amount`,
								`provider_credit_note`.`transaction_date` AS `transactionDate`,
								`provider_credit_note`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_credit_note_item`.`provider_credit_note_item_status` AS `status`,
								'Provider Credit Note' AS `transactionCategory`,
								'Providers Credit Notes' AS `transactionClassification`,
								'provider_credit_note' AS `transactionTable`,
								'provider_credit_note_item' AS `referenceTable`,
								'' AS category_id
							FROM
								(
									(
										(
											`provider_credit_note_item`,provider_credit_note,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = provider_credit_note.account_from_id
											)
										)
										
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
								provider_credit_note.provider_credit_note_id = provider_credit_note_item.provider_credit_note_id 
								AND provider_credit_note.provider_credit_note_status = 1
								AND provider_invoice.provider_invoice_id = provider_credit_note_item.provider_invoice_id
								AND provider_invoice.provider_invoice_status = 1
								AND provider.provider_id = provider_invoice.provider_id 
								AND provider_invoice.transaction_date >= provider.start_date

							UNION ALL

							SELECT
								`provider_credit_note_item`.`provider_credit_note_item_id` AS `transactionId`,
								`provider_credit_note`.`provider_credit_note_id` AS `referenceId`,
								`provider_credit_note_item`.`provider_invoice_id` AS `payingFor`,
								`provider_credit_note`.`invoice_number` AS `referenceCode`,
								`provider_credit_note`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  	`provider_credit_note`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionDescription`,
								`provider_credit_note_item`.`credit_note_amount` AS `dr_amount`,
								0 AS `cr_amount`,
								`provider_credit_note`.`transaction_date` AS `transactionDate`,
								`provider_credit_note`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_credit_note_item`.`provider_credit_note_item_status` AS `status`,
								'Provider Credit Note' AS `transactionCategory`,
								'Providers Credit Notes' AS `transactionClassification`,
								'provider_credit_note' AS `transactionTable`,
								'provider_credit_note_item' AS `referenceTable`,
								'' AS category_id
							FROM
								(
									(
										(
											`provider_credit_note_item`,provider_credit_note,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = ".$providers_liability_id."
											)
										)
										
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
								provider_credit_note.provider_credit_note_id = provider_credit_note_item.provider_credit_note_id 
								AND provider_credit_note.provider_credit_note_status = 1
								AND provider_invoice.provider_invoice_id = provider_credit_note_item.provider_invoice_id
								AND provider_invoice.provider_invoice_status = 1
								AND provider.provider_id = provider_invoice.provider_id 
								AND provider_invoice.transaction_date >= provider.start_date

							UNION ALL

							SELECT
									`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
									`provider_payment`.`provider_payment_id` AS `referenceId`,
									`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
									`provider_payment`.`reference_number` AS `referenceCode`,
									`provider_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`provider_payment`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`provider_payment`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`provider_payment_item`.`description` AS `transactionName`,
									CONCAT('Provider payment for invoice of ',' ',`provider_invoice`.`invoice_number`)  AS `transactionDescription`,
									0 AS `dr_amount`,
									`provider_payment_item`.`amount_paid` AS `cr_amount`,
									`provider_payment`.`transaction_date` AS `transactionDate`,
									`provider_payment`.`created` AS `createdAt`,
									`provider_invoice`.`transaction_date` AS `referenceDate`,
									`provider_payment_item`.`provider_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'providers Invoices Payments' AS `transactionClassification`,
									'provider_payment' AS `transactionTable`,
									'provider_payment_item' AS `referenceTable`,
									'' AS category_id
								FROM
									(
										(
											(
												`provider_payment_item`,provider_payment,provider_invoice,provider
												
											)
											JOIN account ON(
												(
													account.account_id = provider_payment.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE provider_payment_item.invoice_type = 0 
									AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
									AND provider_payment.provider_payment_status = 1
									AND provider_invoice.provider_invoice_id = provider_payment_item.provider_invoice_id 
									AND provider_invoice.provider_invoice_status = 1  
									AND provider.provider_id = provider_invoice.provider_id AND provider_invoice.transaction_date >= provider.start_date

							UNION ALL

							SELECT
									`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
									`provider_payment`.`provider_payment_id` AS `referenceId`,
									`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
									`provider_payment`.`reference_number` AS `referenceCode`,
									`provider_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`provider_payment`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`provider_payment`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`provider_payment_item`.`description` AS `transactionName`,
									CONCAT('Provider payment for invoice of ',' ',`provider_invoice`.`invoice_number`)  AS `transactionDescription`,
									`provider_payment_item`.`amount_paid` AS `dr_amount`,
									0 AS `cr_amount`,
									`provider_payment`.`transaction_date` AS `transactionDate`,
									`provider_payment`.`created` AS `createdAt`,
									`provider_invoice`.`transaction_date` AS `referenceDate`,
									`provider_payment_item`.`provider_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'providers Invoices Payments' AS `transactionClassification`,
									'provider_payment' AS `transactionTable`,
									'provider_payment_item' AS `referenceTable`,
									'' AS category_id
								FROM
									(
										(
											(
												`provider_payment_item`,provider_payment,provider_invoice,provider
												
											)
											JOIN account ON(
												(
													account.account_id = ".$providers_liability_id."
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE provider_payment_item.invoice_type = 0 
									AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
									AND provider_payment.provider_payment_status = 1
									AND provider_invoice.provider_invoice_id = provider_payment_item.provider_invoice_id 
									AND provider_invoice.provider_invoice_status = 1  
									AND provider.provider_id = provider_invoice.provider_id AND provider_invoice.transaction_date >= provider.start_date

							UNION ALL

							SELECT
									`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
									`provider_payment`.`provider_payment_id` AS `referenceId`,
									`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
									`provider_payment`.`reference_number` AS `referenceCode`,
									`provider_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  `provider_payment`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`provider_payment`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`provider_payment_item`.`description` AS `transactionName`,
									CONCAT('Payment of opening balance')  AS `transactionDescription`,
									0 AS `dr_amount`,
									`provider_payment_item`.`amount_paid` AS `cr_amount`,
									`provider_payment`.`transaction_date` AS `transactionDate`,
									`provider_payment`.`created` AS `createdAt`,
									`provider`.`start_date` AS `referenceDate`,
									`provider_payment_item`.`provider_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'providers Invoices Payments' AS `transactionClassification`,
									'provider_payment' AS `transactionTable`,
									'provider_payment_item' AS `referenceTable`,
									'' AS category_id
								FROM
									(
										(
											(
												`provider_payment_item`,provider_payment,provider
												
											)
											JOIN account ON(
												(
													account.account_id = provider_payment.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE provider_payment_item.invoice_type = 2 
									AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
									AND provider_payment.provider_payment_status = 1
									AND provider.provider_id = provider_payment_item.provider_id
									AND provider_payment.transaction_date >= provider.start_date


								UNION ALL

							SELECT
									`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
									`provider_payment`.`provider_payment_id` AS `referenceId`,
									`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
									`provider_payment`.`reference_number` AS `referenceCode`,
									`provider_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  `provider_payment`.`provider_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`provider_payment`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`provider_payment_item`.`description` AS `transactionName`,
									CONCAT('Payment of opening balance')  AS `transactionDescription`,
									`provider_payment_item`.`amount_paid` AS `dr_amount`,
									0 AS `cr_amount`,
									`provider_payment`.`transaction_date` AS `transactionDate`,
									`provider_payment`.`created` AS `createdAt`,
									`provider`.`start_date` AS `referenceDate`,
									`provider_payment_item`.`provider_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'providers Invoices Payments' AS `transactionClassification`,
									'provider_payment' AS `transactionTable`,
									'provider_payment_item' AS `referenceTable`,
									'' AS category_id
								FROM
									(
										(
											(
												`provider_payment_item`,provider_payment,provider
												
											)
											JOIN account ON(
												(
													account.account_id = ".$providers_liability_id."
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE provider_payment_item.invoice_type = 2 
									AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
									AND provider_payment.provider_payment_status = 1
									AND provider.provider_id = provider_payment_item.provider_id
									AND provider_payment.transaction_date >= provider.start_date

								UNION ALL

								SELECT
										`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
										`provider_payment`.`provider_payment_id` AS `referenceId`,
										`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
										`provider_payment`.`reference_number` AS `referenceCode`,
										`provider_payment`.`document_number` AS `transactionCode`,
										'' AS `patient_id`,
									  `provider_payment`.`provider_id` AS `recepientId`,
										`account`.`parent_account` AS `accountParentId`,
										`account_type`.`account_type_name` AS `accountsclassfication`,
										`provider_payment`.`account_from_id` AS `accountId`,
										`account`.`account_name` AS `accountName`,
										`provider_payment_item`.`description` AS `transactionName`,
										CONCAT('Payment on account')  AS `transactionDescription`,
										0 AS `dr_amount`,
										`provider_payment_item`.`amount_paid` AS `cr_amount`,
										`provider_payment`.`transaction_date` AS `transactionDate`,
										`provider_payment`.`created` AS `createdAt`,
										`provider_payment`.`created` AS `referenceDate`,
										`provider_payment_item`.`provider_payment_item_status` AS `status`,
										'Expense Payment' AS `transactionCategory`,
										'providers Invoices Payments' AS `transactionClassification`,
										'provider_payment' AS `transactionTable`,
										'provider_payment_item' AS `referenceTable`,
										'' AS category_id
									FROM
										(
											(
												(
													`provider_payment_item`,provider_payment,provider
													
												)
												JOIN account ON(
													(
														account.account_id = provider_payment.account_from_id
													)
												)
											)
											JOIN `account_type` ON(
												(
													account_type.account_type_id = account.account_type_id
												)
											)
											
										)
										WHERE provider_payment_item.invoice_type = 3 AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
											AND provider_payment.provider_payment_status = 1 
											AND provider.provider_id = provider_payment.provider_id AND provider_payment.transaction_date >= provider.start_date
									UNION ALL

								SELECT
										`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
										`provider_payment`.`provider_payment_id` AS `referenceId`,
										`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
										`provider_payment`.`reference_number` AS `referenceCode`,
										`provider_payment`.`document_number` AS `transactionCode`,
										'' AS `patient_id`,
									  `provider_payment`.`provider_id` AS `recepientId`,
										`account`.`parent_account` AS `accountParentId`,
										`account_type`.`account_type_name` AS `accountsclassfication`,
										`provider_payment`.`account_from_id` AS `accountId`,
										`account`.`account_name` AS `accountName`,
										`provider_payment_item`.`description` AS `transactionName`,
										CONCAT('Payment on account')  AS `transactionDescription`,
										`provider_payment_item`.`amount_paid` AS `dr_amount`,
										0 AS `cr_amount`,
										`provider_payment`.`transaction_date` AS `transactionDate`,
										`provider_payment`.`created` AS `createdAt`,
										`provider_payment`.`created` AS `referenceDate`,
										`provider_payment_item`.`provider_payment_item_status` AS `status`,
										'Expense Payment' AS `transactionCategory`,
										'providers Invoices Payments' AS `transactionClassification`,
										'provider_payment' AS `transactionTable`,
										'provider_payment_item' AS `referenceTable`,
										'' AS category_id
									FROM
										(
											(
												(
													`provider_payment_item`,provider_payment,provider
													
												)
												JOIN account ON(
													(
														account.account_id = ".$providers_liability_id."
													)
												)
											)
											JOIN `account_type` ON(
												(
													account_type.account_type_id = account.account_type_id
												)
											)
											
										)
										WHERE provider_payment_item.invoice_type = 3 AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
											AND provider_payment.provider_payment_status = 1 
											AND provider.provider_id = provider_payment.provider_id AND provider_payment.transaction_date >= provider.start_date
								
							
							UNION ALL


							SELECT
								`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
								`provider_invoice`.`provider_invoice_id` AS `referenceId`,
								'' AS `payingFor`,
								`provider_invoice`.`invoice_number` AS `referenceCode`,
								`provider_invoice`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  `provider_invoice`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
								0 AS `dr_amount`,
								`provider_invoice_item`.`vat_amount` AS `cr_amount`,
								`provider_invoice`.`transaction_date` AS `transactionDate`,
								`provider_invoice`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
								'Provider WHT' AS `transactionCategory`,
								'providers Invoices' AS `transactionClassification`,
								'provider_invoice_item' AS `transactionTable`,
								'provider_invoice' AS `referenceTable`,
								'' AS category_id
							FROM
								(
									(
										(
											`provider_invoice_item`,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = ".$providers_wht_id."
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
							provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
							AND provider_invoice.provider_invoice_status = 1
							AND provider.provider_id = provider_invoice.provider_id 
							AND provider_invoice.transaction_date >= provider.start_date
							AND provider_invoice_item.vat_amount > 0

					
							UNION ALL




							SELECT
								`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
								`provider_invoice`.`provider_invoice_id` AS `referenceId`,
								'' AS `payingFor`,
								`provider_invoice`.`invoice_number` AS `referenceCode`,
								`provider_invoice`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
							  `provider_invoice`.`provider_id` AS `recepientId`,
								`account`.`parent_account` AS `accountParentId`,
								`account_type`.`account_type_name` AS `accountsclassfication`,
								`account`.`account_id` AS `accountId`,
								`account`.`account_name` AS `accountName`,
								CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
								`provider_invoice_item`.`vat_amount` AS `dr_amount`,
								0  AS `cr_amount`,
								`provider_invoice`.`transaction_date` AS `transactionDate`,
								`provider_invoice`.`created` AS `createdAt`,
								`provider_invoice`.`transaction_date` AS `referenceDate`,
								`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
								'Provider WHT' AS `transactionCategory`,
								'providers Invoices' AS `transactionClassification`,
								'provider_invoice_item' AS `transactionTable`,
								'provider_invoice' AS `referenceTable`,
								'' AS category_id
							FROM
								(
									(
										(
											`provider_invoice_item`,provider_invoice,provider
											
										)
										JOIN account ON(
											(
												account.account_id = ".$providers_liability_id."
											)
										)
									)
									JOIN `account_type` ON(
										(
											account_type.account_type_id = account.account_type_id
										)
									)
								)
							WHERE 
							provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
							AND provider_invoice.provider_invoice_status = 1
							AND provider.provider_id = provider_invoice.provider_id 
							AND provider_invoice.transaction_date >= provider.start_date
							AND provider_invoice_item.vat_amount > 0

							

							UNION ALL

								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payroll') AS `transactionName`,
									CONCAT('Payroll') AS `transactionDescription`,
									payroll_summary.total_payroll AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'Payroll' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`,
									'' AS category_id
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 1
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id


								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('PAYE') AS `transactionName`,
									CONCAT('PAYE') AS `transactionDescription`,
									payroll_summary.paye AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`,
									'' AS category_id
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 2
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id

								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NSSF') AS `transactionName`,
									CONCAT('NSSF') AS `transactionDescription`,
									payroll_summary.nssf AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`,
									'' AS category_id
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 3
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id


								UNION ALL




								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NHIF') AS `transactionName`,
									CONCAT('NHIF') AS `transactionDescription`,
									payroll_summary.nhif AS `dr_amount`,
									0 AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'NHIF' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`,
									'' AS category_id
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 4
								AND account.account_id = statutory_accounts.account_id
								AND account_type.account_type_id = account.account_type_id


							-- payroll liability

							UNION ALL


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payroll') AS `transactionName`,
									CONCAT('Payroll') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.total_payroll AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'Payroll' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`,
									'' AS category_id
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 1
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id


								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('PAYE') AS `transactionName`,
									CONCAT('PAYE') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.paye AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`,
									'' AS category_id
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 2
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id

								UNION ALL 


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NSSF') AS `transactionName`,
									CONCAT('NSSF') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.nssf AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'PAYE' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`,
									'' AS category_id
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 3
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id


								UNION ALL




								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('NHIF') AS `transactionName`,
									CONCAT('NHIF') AS `transactionDescription`,
									0 AS `dr_amount`,
									payroll_summary.nhif AS `cr_amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									2 AS branch_id,
									'NHIF' AS `transactionCategory`,
									'Purchases' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`,
									'' AS category_id
								FROM
								payroll_summary,payroll,statutory_accounts,account,account_type
								WHERE payroll.payroll_id = payroll_summary.payroll_id
								AND statutory_accounts.statutory_account_id = 4
								AND account.account_id = ".$payroll_liability_id."
								AND account_type.account_type_id = account.account_type_id

							UNION ALL

							SELECT
							    `payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
							    `payroll_payment`.`payroll_payment_id` AS `referenceId`,
							    `payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
							    `payroll_payment`.`reference_number` AS `referenceCode`,
							    `payroll_payment`.`document_number` AS `transactionCode`,
							    '' AS `patient_id`,
							    `statutory_accounts`.`statutory_account_id` AS `recepientId`,
							    `account`.`parent_account` AS `accountParentId`,
							    `account_type`.`account_type_name` AS `accountsclassfication`,
							    `payroll_payment`.`account_from_id` AS `accountId`,
							    `account`.`account_name` AS `accountName`,
							    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
							    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for))   AS `transactionDescription`,
							    0 AS `dr_amount`,
							    `payroll_payment_item`.`amount_paid` AS `cr_amount`,
							    `payroll_payment`.`transaction_date` AS `transactionDate`,
							    `payroll_payment`.`created` AS `createdAt`,
							    `payroll_payment_item`.`payroll_payment_item_status` AS `status`,
							    2 AS `branch_id`,
							    'Payroll Payment' AS `transactionCategory`,
							    'Creditors Invoices Payments' AS `transactionClassification`,
							    'payroll_payment' AS `transactionTable`,
							    'payroll_payment_item' AS `referenceTable`,
							    '' AS category_id
							FROM
							    (
							      (
							        (
							          `payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts
							          
							        )
							        JOIN account ON(
							          (
							            account.account_id = payroll_payment.account_from_id
							          )
							        )
							      )
							      JOIN `account_type` ON(
							        (
							          account_type.account_type_id = account.account_type_id
							        )
							      )
							      
							    )
							    WHERE payroll_payment_item.invoice_type = 0 
							    AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
							    AND payroll_payment.payroll_payment_status = 1
							    AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_payment_id 
							    AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id

							UNION ALL

								SELECT
								    `payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
								    `payroll_payment`.`payroll_payment_id` AS `referenceId`,
								    `payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
								    `payroll_payment`.`reference_number` AS `referenceCode`,
								    `payroll_payment`.`document_number` AS `transactionCode`,
								    '' AS `patient_id`,
								    `statutory_accounts`.`statutory_account_id` AS `recepientId`,
								    `account`.`parent_account` AS `accountParentId`,
								    `account_type`.`account_type_name` AS `accountsclassfication`,
								    account.account_id AS `accountId`,
								    `account`.`account_name` AS `accountName`,
								    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
								    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for))   AS `transactionDescription`,
								    `payroll_payment_item`.`amount_paid` AS `dr_amount`,
								    0 AS `cr_amount`,
								    `payroll_payment`.`transaction_date` AS `transactionDate`,
								    `payroll_payment`.`created` AS `createdAt`,
								    `payroll_payment_item`.`payroll_payment_item_status` AS `status`,
								    2 AS `branch_id`,
								    'Payroll Payment' AS `transactionCategory`,
								    'Creditors Invoices Payments' AS `transactionClassification`,
								    'payroll_payment' AS `transactionTable`,
								    'payroll_payment_item' AS `referenceTable`,
								    '' AS category_id
								  FROM
								    (
								      (
								        (
								          `payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts
								          
								        )
								        JOIN account ON(
								          (
								            account.account_id = ".$payroll_liability_id."
								          )
								        )
								      )
								      JOIN `account_type` ON(
								        (
								          account_type.account_type_id = account.account_type_id
								        )
								      )
								      
								    )
								    WHERE payroll_payment_item.invoice_type = 0 
								    AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
								    AND payroll_payment.payroll_payment_status = 1
								    AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_invoice_id 
								    AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id


								UNION ALL

								 SELECT
									asset_category.asset_category_id AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									assets_details.asset_serial_no AS `referenceCode`,
									assets_details.asset_serial_no AS `transactionCode`,
									assets_details.asset_id AS `patient_id`,
								  	assets_details.supplier_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									CONCAT(account.account_name,' ',assets_details.asset_name) AS `accountName`,
									CONCAT(assets_details.asset_name) AS `transactionName`,
									CONCAT(assets_details.asset_name) AS `transactionDescription`,
									(assets_details.asset_value) AS `dr_amount`,
									0 AS `cr_amount`,
									assets_details.asset_pd_period AS `transactionDate`,
									assets_details.asset_pd_period  AS `createdAt`,
									assets_details.asset_pd_period AS `referenceDate`,
									1 AS `status`,
									'FIXED ASSET' AS `transactionCategory`,
									'Asset Purchase' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`,
									'' AS category_id
								FROM
									assets_details,asset_category,account,account_type
									WHERE 
									assets_details.asset_category_id = asset_category.asset_category_id 
									AND account.account_id = asset_category.account_id
									AND account.account_type_id = account_type.account_type_id

								UNION ALL


								SELECT
									asset_category.asset_category_id AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									assets_details.asset_serial_no AS `referenceCode`,
									assets_details.asset_serial_no AS `transactionCode`,
									assets_details.asset_id AS `patient_id`,
								  	assets_details.supplier_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									CONCAT(account.account_name,' ',assets_details.asset_name) AS `accountName`,
									CONCAT(assets_details.asset_name) AS `transactionName`,
									CONCAT(assets_details.asset_name) AS `transactionDescription`,
									0 AS `dr_amount`,
									(assets_details.asset_value) AS `cr_amount`,
									assets_details.asset_pd_period AS `transactionDate`,
									assets_details.asset_pd_period  AS `createdAt`,
									assets_details.asset_pd_period AS `referenceDate`,
									1 AS `status`,
									'FIXED ASSET' AS `transactionCategory`,
									'Asset Purchase' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`,
									'' AS category_id
								FROM
									assets_details,asset_category,account,account_type
									WHERE 
									assets_details.asset_category_id = asset_category.asset_category_id 
									AND assets_details.bill_asset = 1
									AND account.account_id = ".$accounts_payable_id."
									AND account.account_type_id = account_type.account_type_id

							UNION ALL

								SELECT


									patients_journals.patient_journal_id AS transactionId,
									patients_journals.patient_journal_id AS referenceId,
									'' AS `payingFor`,
									'' AS referenceCode,
									'' AS `transactionCode`,
									patients_journals.patient_id AS patient_id,
									patients_journals.patient_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									account.account_name AS `accountName`,
									CONCAT('Patient Journals') AS `transactionName`,
									CONCAT('Patient Journals') AS `transactionDescription`,
									patients_journals.journal_amount AS `dr_amount`,
									0 AS `cr_amount`,
									patients_journals.journal_date AS `transactionDate`,
									patients_journals.journal_date  AS `createdAt`,
									patients_journals.journal_date AS `referenceDate`,
									1 AS `status`,
									'JOURNAL' AS `transactionCategory`,
									'JOURNAL' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`,
									'' AS category_id
								FROM
										patients_journals,visit_type,account,account_type
									WHERE 
									patients_journals.visit_type_id = visit_type.visit_type_id 
									AND patients_journals.visit_invoice_id > 0
									AND patients_journals.account_from_id = account.account_id
									AND account_type.account_type_id = account.account_type_id

							UNION ALL

								SELECT


									patients_journals.patient_journal_id AS transactionId,
									patients_journals.patient_journal_id AS referenceId,
									'' AS `payingFor`,
									'' AS referenceCode,
									'' AS `transactionCode`,
									patients_journals.patient_id AS patient_id,
									patients_journals.patient_id AS `recepientId`,
									account_type.account_type_id AS `accountParentId`,
									account_type.account_type_name AS `accountsclassfication`,
									`account`.`account_id` AS `accountId`,
									account.account_name AS `accountName`,
									CONCAT('Patient Journals') AS `transactionName`,
									CONCAT('Patient Journals') AS `transactionDescription`,
									0 AS `dr_amount`,
									patients_journals.journal_amount AS `cr_amount`,
									patients_journals.journal_date AS `transactionDate`,
									patients_journals.journal_date  AS `createdAt`,
									patients_journals.journal_date AS `referenceDate`,
									1 AS `status`,
									'JOURNAL' AS `transactionCategory`,
									'JOURNAL' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`,
									'' AS category_id
								FROM
										patients_journals,visit_type,account,account_type
									WHERE 
									patients_journals.visit_type_id = visit_type.visit_type_id 
									AND patients_journals.visit_invoice_id > 0
									AND patients_journals.account_to_id = account.account_id
									AND account_type.account_type_id = account.account_type_id

								UNION ALL

								SELECT
									`loan_invoice_item`.`loan_invoice_item_id` AS `transactionId`,
									`loan_invoice`.`loan_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`loan_invoice`.`invoice_number` AS `referenceCode`,
									`loan_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_invoice`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`loan_invoice_item`.`item_description` AS `transactionName`,
									`loan_invoice_item`.`item_description` AS `transactionDescription`,
									 0 AS `dr_amount`,
									`loan_invoice_item`.`total_amount` AS `cr_amount`,
									`loan_invoice`.`transaction_date` AS `transactionDate`,
									`loan_invoice`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_invoice_item`.`loan_invoice_item_status` AS `status`,
									'Expense' AS `transactionCategory`,
									'loans Invoices' AS `transactionClassification`,
									'loan_invoice_item' AS `transactionTable`,
									'loan_invoice' AS `referenceTable`,
									'' AS category_id
								FROM
									(
										(
											(
												`loan_invoice_item`,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
									)
								WHERE 
								loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id 
								AND loan_invoice.loan_invoice_status = 1
								AND loan.loan_id = loan_invoice.loan_id 
								AND loan_invoice.transaction_date >= loan.start_date

							UNION ALL

								SELECT
									`loan_invoice_item`.`loan_invoice_item_id` AS `transactionId`,
									`loan_invoice`.`loan_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`loan_invoice`.`invoice_number` AS `referenceCode`,
									`loan_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_invoice`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan`.`account_to_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									`loan_invoice_item`.`item_description` AS `transactionName`,
									`loan_invoice_item`.`item_description` AS `transactionDescription`,
									`loan_invoice_item`.`total_amount` AS `dr_amount`,
									'0' AS `cr_amount`,
									`loan_invoice`.`transaction_date` AS `transactionDate`,
									`loan_invoice`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_invoice_item`.`loan_invoice_item_status` AS `status`,
									'Expense' AS `transactionCategory`,
									'loans Invoices' AS `transactionClassification`,
									'loan_invoice_item' AS `transactionTable`,
									'loan_invoice' AS `referenceTable`,
									'' AS category_id
								FROM
									(
										(
											(
												`loan_invoice_item`,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan.account_to_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
									)
								WHERE 
								loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id 
								AND loan_invoice.loan_invoice_status = 1
								AND loan.loan_id = loan_invoice.loan_id 
								AND loan_invoice.transaction_date >= loan.start_date

								UNION ALL

								SELECT
									`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
									`loan_payment`.`loan_payment_id` AS `referenceId`,
									`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
									`loan_payment`.`reference_number` AS `referenceCode`,
									`loan_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_payment`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan_payment`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
									CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
									0 AS `dr_amount`,
									`loan_payment_item`.`amount_paid` AS `cr_amount`,
									`loan_payment`.`transaction_date` AS `transactionDate`,
									`loan_payment`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_payment_item`.`loan_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'loans Invoices Payments' AS `transactionClassification`,
									'loan_payment' AS `transactionTable`,
									'loan_payment_item' AS `referenceTable`,
									'' AS category_id
								FROM
									(
										(
											(
												`loan_payment_item`,loan_payment,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan_payment.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE loan_payment_item.invoice_type = 0 
									AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
									AND loan_payment.loan_payment_status = 1
									AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
									AND loan_invoice.loan_invoice_status = 1  
									AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
									AND loan.account_from_id = loan_payment_item.account_to_id
									
							UNION ALL
							
							SELECT
									`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
									`loan_payment`.`loan_payment_id` AS `referenceId`,
									`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
									`loan_payment`.`reference_number` AS `referenceCode`,
									`loan_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
								  	`loan_payment`.`loan_id` AS `recepientId`,
									`account`.`parent_account` AS `accountParentId`,
									`account_type`.`account_type_name` AS `accountsclassfication`,
									`loan`.`account_from_id` AS `accountId`,
									`account`.`account_name` AS `accountName`,
									CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
									CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
									`loan_payment_item`.`amount_paid` AS `dr_amount`,
									0 AS `cr_amount`,
									`loan_payment`.`transaction_date` AS `transactionDate`,
									`loan_payment`.`created` AS `createdAt`,
									`loan_invoice`.`transaction_date` AS `referenceDate`,
									`loan_payment_item`.`loan_payment_item_status` AS `status`,
									'Expense Payment' AS `transactionCategory`,
									'loans Invoices Payments' AS `transactionClassification`,
									'loan_payment' AS `transactionTable`,
									'loan_payment_item' AS `referenceTable`,
									'' AS category_id
								FROM
									(
										(
											(
												`loan_payment_item`,loan_payment,loan_invoice,loan
												
											)
											JOIN account ON(
												(
													account.account_id = loan.account_from_id
												)
											)
										)
										JOIN `account_type` ON(
											(
												account_type.account_type_id = account.account_type_id
											)
										)
										
									)
									WHERE loan_payment_item.invoice_type = 0 
									AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
									AND loan_payment.loan_payment_status = 1
									AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
									AND loan_invoice.loan_invoice_status = 1  
									AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
									AND loan.account_from_id = loan_payment_item.account_to_id

									UNION ALL

									SELECT
											`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
											`loan_payment`.`loan_payment_id` AS `referenceId`,
											`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
											`loan_payment`.`reference_number` AS `referenceCode`,
											`loan_payment`.`document_number` AS `transactionCode`,
											'' AS `patient_id`,
												`loan_payment`.`loan_id` AS `recepientId`,
											`account`.`parent_account` AS `accountParentId`,
											`account_type`.`account_type_name` AS `accountsclassfication`,
											loan_payment_item.account_to_id AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
											CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
											`loan_payment_item`.`amount_paid` AS `dr_amount`,
											0 AS `cr_amount`,
											`loan_payment`.`transaction_date` AS `transactionDate`,
											`loan_payment`.`created` AS `createdAt`,
											`loan_invoice`.`transaction_date` AS `referenceDate`,
											`loan_payment_item`.`loan_payment_item_status` AS `status`,
											'Expense Payment' AS `transactionCategory`,
											'loans Invoices Payments' AS `transactionClassification`,
											'loan_payment' AS `transactionTable`,
											'loan_payment_item' AS `referenceTable`,
											'' AS category_id
										FROM
											(
												(
													(
														`loan_payment_item`,loan_payment,loan_invoice,loan,account
														
													)
												
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
											WHERE loan_payment_item.invoice_type = 0 
											AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
											AND loan_payment.loan_payment_status = 1
											AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
											AND loan_invoice.loan_invoice_status = 1  
											AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
											AND loan.account_from_id <> loan_payment_item.account_to_id
											AND account.account_id = loan_payment_item.account_to_id
											
											UNION ALL
											
											
											SELECT
											`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
											`loan_payment`.`loan_payment_id` AS `referenceId`,
											`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
											`loan_payment`.`reference_number` AS `referenceCode`,
											`loan_payment`.`document_number` AS `transactionCode`,
											'' AS `patient_id`,
												`loan_payment`.`loan_id` AS `recepientId`,
											`account`.`parent_account` AS `accountParentId`,
											`account_type`.`account_type_name` AS `accountsclassfication`,
											loan_payment.account_from_id AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT('Payment from ',' ',`account`.`account_name`) AS `transactionName`,
											CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
											0 AS `dr_amount`,
											`loan_payment_item`.`amount_paid` AS `cr_amount`,
											`loan_payment`.`transaction_date` AS `transactionDate`,
											`loan_payment`.`created` AS `createdAt`,
											`loan_invoice`.`transaction_date` AS `referenceDate`,
											`loan_payment_item`.`loan_payment_item_status` AS `status`,
											'Expense Payment' AS `transactionCategory`,
											'loans Invoices Payments' AS `transactionClassification`,
											'loan_payment' AS `transactionTable`,
											'loan_payment_item' AS `referenceTable`,
											'' AS category_id
										FROM
											(
												(
													(
														`loan_payment_item`,loan_payment,loan_invoice,loan,account
														
													)
												
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
											WHERE loan_payment_item.invoice_type = 0 
											AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id 
											AND loan_payment.loan_payment_status = 1
											AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id 
											AND loan_invoice.loan_invoice_status = 1  
											AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
											AND loan.account_from_id <> loan_payment_item.account_to_id
											AND account.account_id = loan_payment.account_from_id

									UNION ALL


									SELECT
										asset_category.asset_category_id AS `transactionId`,
										'' AS `referenceId`,
										'' AS `payingFor`,
										assets_details.asset_serial_no AS `referenceCode`,
										assets_details.asset_serial_no AS `transactionCode`,
										assets_details.asset_id AS `patient_id`,
											assets_details.supplier_id AS `recepientId`,
										account_type.account_type_id AS `accountParentId`,
										account_type.account_type_name AS `accountsclassfication`,
										`account`.`account_id` AS `accountId`,
										CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
										CONCAT(assets_details.asset_name) AS `transactionName`,
										CONCAT(assets_details.asset_name) AS `transactionDescription`,
										(asset_amortization.interest_amount) AS `dr_amount`,
										0 AS `cr_amount`,
										asset_amortization.date_approved AS `transactionDate`,
										asset_amortization.date_approved  AS `createdAt`,
										asset_amortization.date_approved AS `referenceDate`,
										1 AS `status`,
										'FIXED ASSET' AS `transactionCategory`,
										'ASSET DEPRECIATION' AS `transactionClassification`,
										'account_payments' AS `transactionTable`,
										'' AS `referenceTable`,
										assets_details.asset_category_id AS category_id
									FROM
										assets_details,asset_category,account,account_type,asset_amortization
										WHERE 
										assets_details.asset_category_id = asset_category.asset_category_id 
										AND asset_amortization.expense_account_id = account.account_id
										AND account.account_type_id = account_type.account_type_id
										AND asset_amortization.asset_id = assets_details.asset_id
										AND asset_amortization.bill_status = 1
										
								UNION ALL

								SELECT
										asset_category.asset_category_id AS `transactionId`,
										'' AS `referenceId`,
										'' AS `payingFor`,
										assets_details.asset_serial_no AS `referenceCode`,
										assets_details.asset_serial_no AS `transactionCode`,
										assets_details.asset_id AS `patient_id`,
										assets_details.supplier_id AS `recepientId`,
										account_type.account_type_id AS `accountParentId`,
										account_type.account_type_name AS `accountsclassfication`,
										`account`.`account_id` AS `accountId`,
										CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
										CONCAT(assets_details.asset_name) AS `transactionName`,
										CONCAT(assets_details.asset_name) AS `transactionDescription`,
										0 AS `dr_amount`,
										(asset_amortization.interest_amount) AS `cr_amount`,
										asset_amortization.date_approved AS `transactionDate`,
										asset_amortization.date_approved  AS `createdAt`,
										asset_amortization.date_approved AS `referenceDate`,
										1 AS `status`,
										'FIXED ASSET' AS `transactionCategory`,
										'ASSET DEPRECIATION' AS `transactionClassification`,
										'account_payments' AS `transactionTable`,
										'' AS `referenceTable`,
										assets_details.asset_category_id AS category_id
									FROM
										assets_details,asset_category,account,account_type,asset_amortization
										WHERE 
										assets_details.asset_category_id = asset_category.asset_category_id 
										AND asset_amortization.acc_depreciation_id = account.account_id
										AND account.account_type_id = account_type.account_type_id
										AND asset_amortization.asset_id = assets_details.asset_id
										AND asset_amortization.bill_status = 1
										


					) AS data WHERE ".$add."  data.transactionDate <> '0000-00-00' AND data.accountId in ('".implode("','", $array_leads)."')   ".$add_item."   ORDER BY data.transactionDate ASC

					";
	 

	  $query = $this->db->query($select);


	   return $query;

	}

	public function get_transactions_fixed_assets_category($array_leads,$checked = NULL)
	{
		
		
		$financial_year_date = date('Y')."-12-31";


		$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

		$patients = array();
		
		if($accounts_config_rs->num_rows() > 0)
		{
			foreach ($accounts_config_rs->result() as $key => $value) {
				// code...
				$staing_account_id = $value->account_id;
				$reference_name = $value->reference_name;

				$session_account[$reference_name] = $staing_account_id;

				
			}
		}

		$providers_liability_id = $session_account['providers_liability_id'];
		$providers_wht_id = $session_account['providers_wht_id'];
		$payroll_liability_id = $session_account['payroll_liability_id'];
		$accounts_payable_id = $session_account['accounts_payable_id'];
		$fixed_assets_id = $session_account['fixed_assets_id'];
		$accounts_receivable_id = $session_account['accounts_receivable_id'];
		$suppliers_wht_id = $session_account['suppliers_wht_id'];
		$income_account_id = $session_account['income_account_id'];
		$supplier_opening_balance_id= $session_account['supplier_opening_balance_id'];
		$bank_opening_balance_id = $session_account['bank_opening_balance_id'];
		$acc_depreciation_id = $session_account['acc_depreciation_id'];
		$asset_depreciation_id = $session_account['asset_depreciation_id'];

		$financial_year_date = "2021-12-31";
		// $date_from = $this->session->userdata('date_from_general_ledger');
		// $date_to = $this->session->userdata('date_to_general_ledger');
		$add = '';

		// if(!empty($date_from) OR !empty($date_to))
	  //   	{
	    		
	  //   		$add =  ' DATE(data.transactionDate) > \''.$date_from.'\' AND DATE(data.transactionDate) <= \''.$date_to.'\' AND ';
	    		
	    		
	  //   	}
	    	
	    	

	    		
	    
	    	
	    	


	    	if($checked == 1)
	    	{
	    		$item = '	
	    					data.accountId AS accountId,
							data.accountName AS account_name,
							data.category_id AS category_id,
						 	SUM(data.dr_amount) AS dr_amount,
						 	SUM(data.cr_amount) AS cr_amount';

				$add_item = 'GROUP BY data.accountId,data.category_id';
	    	}
	    	else
	    	{
	    		$item = '*';
				$add_item = '';
	    	}
		
    	
		$select  = "
					SELECT 
						".$item."
					FROM 

					(

						

									SELECT
										asset_category.asset_category_id AS `transactionId`,
										'' AS `referenceId`,
										'' AS `payingFor`,
										assets_details.asset_serial_no AS `referenceCode`,
										assets_details.asset_serial_no AS `transactionCode`,
										assets_details.asset_id AS `patient_id`,
											assets_details.supplier_id AS `recepientId`,
										account_type.account_type_id AS `accountParentId`,
										account_type.account_type_name AS `accountsclassfication`,
										`account`.`account_id` AS `accountId`,
										CONCAT(account.account_name,' : ',assets_details.asset_name) AS `accountName`,
										CONCAT(assets_details.asset_name) AS `transactionName`,
										CONCAT(assets_details.asset_name) AS `transactionDescription`,
										(asset_amortization.interest_amount) AS `dr_amount`,
										0 AS `cr_amount`,
										asset_amortization.date_approved AS `transactionDate`,
										asset_amortization.date_approved  AS `createdAt`,
										asset_amortization.date_approved AS `referenceDate`,
										1 AS `status`,
										'FIXED ASSET' AS `transactionCategory`,
										'ASSET DEPRECIATION' AS `transactionClassification`,
										'account_payments' AS `transactionTable`,
										'' AS `referenceTable`,
										assets_details.asset_category_id AS category_id
									FROM
										assets_details,asset_category,account,account_type,asset_amortization
										WHERE 
										assets_details.asset_category_id = asset_category.asset_category_id 
										AND asset_amortization.expense_account_id = account.account_id
										AND account.account_type_id = account_type.account_type_id
										AND asset_amortization.asset_id = assets_details.asset_id
										AND asset_amortization.bill_status = 1
										
								UNION ALL

								SELECT
										asset_category.asset_category_id AS `transactionId`,
										'' AS `referenceId`,
										'' AS `payingFor`,
										assets_details.asset_serial_no AS `referenceCode`,
										assets_details.asset_serial_no AS `transactionCode`,
										assets_details.asset_id AS `patient_id`,
										assets_details.supplier_id AS `recepientId`,
										account_type.account_type_id AS `accountParentId`,
										account_type.account_type_name AS `accountsclassfication`,
										`account`.`account_id` AS `accountId`,
										CONCAT(asset_category.asset_category_name,' : DEPRECIATION') AS `accountName`,
										CONCAT(assets_details.asset_name) AS `transactionName`,
										CONCAT(assets_details.asset_name) AS `transactionDescription`,
										0 AS `dr_amount`,
										(asset_amortization.interest_amount) AS `cr_amount`,
										asset_amortization.date_approved AS `transactionDate`,
										asset_amortization.date_approved  AS `createdAt`,
										asset_amortization.date_approved AS `referenceDate`,
										1 AS `status`,
										'FIXED ASSET' AS `transactionCategory`,
										'ASSET DEPRECIATION' AS `transactionClassification`,
										'account_payments' AS `transactionTable`,
										'' AS `referenceTable`,
										assets_details.asset_category_id AS category_id
									FROM
										assets_details,asset_category,account,account_type,asset_amortization
										WHERE 
										assets_details.asset_category_id = asset_category.asset_category_id 
										AND asset_amortization.acc_depreciation_id = account.account_id
										AND account.account_type_id = account_type.account_type_id
										AND asset_amortization.asset_id = assets_details.asset_id
										AND asset_amortization.bill_status = 1
										


					) AS data WHERE ".$add."  data.transactionDate <> '0000-00-00' AND data.accountId = ".$asset_depreciation_id."   ".$add_item."   ORDER BY data.transactionDate ASC

					";
	 

	  $query = $this->db->query($select);


	   return $query;

	}

	public function close_ledger_report()
	{
		$this->session->unset_userdata('general_ledger_search');
		$this->session->unset_userdata('date_from_general_ledger');
		$this->session->unset_userdata('date_to_general_ledger');
		$this->session->unset_userdata('general_ledger_search_title');
	}

	public function get_account_staging_id($account_id)
	{
		$this->db->where('account_id = '.$account_id);
		$query_one = $this->db->get('account_staging');

		if($query_one->num_rows() > 0)
		{
			foreach ($query_one->result() as $key => $value_two) {
				$parent_account = $value_two->account_id;
			}

		}
		else
		{

				$this->db->where('parent_account > 0 AND account_id = '.$account_id);
				$query = $this->db->get('account');

				if($query->num_rows() > 0)
				{
					foreach ($query->result() as $key => $value) {
						// code...
						$parent_account = $value->parent_account;
					}
				}
				else
				{
					$parent_account = $account_id;
				}
		}
		return $parent_account;

	}


	
}

?>