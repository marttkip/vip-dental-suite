<?php

$grand_income = 0;
$income_result = '';



$revenue_rs = $this->ledgers_model->get_all_services();


if($revenue_rs->num_rows() > 0)
{
	$balance = 0;
	$total_amount = 0;
	// var_dump($revenue_rs->result());die();
	foreach ($revenue_rs->result() as $key => $value5) {
		# code...
		// get all transactions
		$service_id = $value5->service_id;
		$department_id = $value5->department_id;
		$service_name = $value5->service_name;



		// update the service Details
		$revenue_items_rs = $this->ledgers_model->get_receivables_transactions_per_service($service_id,1);


		if($revenue_items_rs->num_rows() > 0)
		{
			$balance = 0;
			$total_amount = 0;
			// var_dump($revenue_items_rs->result());die();
			foreach ($revenue_items_rs->result() as $key => $value6) 
			{

				// $dr_amount = $value6->dr_amount;
				$dr_amount = $value6->dr_amount;
				$cr_amount = $value6->cr_amount;
				
				$total_income = $dr_amount-$cr_amount;
				$grand_income += $total_income;

				$income_result .='<tr>
									<td class="text-left">'.strtoupper($service_name).'</td>
									<td class="text-right">'.number_format($total_income,2).'</td>
									</tr>';

			

			}
			
			
		}
	}

}
$income_result .='<tr>
						<td class="text-left"><b>INCOME</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_income,2).'</b></td>
					</tr>';
$other_income_id = $this->company_financial_model->get_parent_account_id('Other Incomes');

$other_account_rs = $this->ledgers_model->get_all_child_accounts($other_income_id);

$grand_other_income = 0;

if($other_account_rs->num_rows() > 0)
{
	$balance = 0;
	$total_amount = 0;
 	foreach ($other_account_rs->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;
 		

		$expense_of_account_rs = $this->ledgers_model->get_account_transactions($account_id);

			// var_dump($parent_account_id);die();
		if($expense_of_account_rs->num_rows() > 0)
		{
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
				$accountName = $value4->accountName;

				$total_other_income = $cr_amount;
				$grand_other_income += $total_other_income;

				$income_result .='<tr>
									<td class="text-left">'.strtoupper($account_name).'</td>
									<td class="text-right">'.number_format($total_other_income,2).'</td>
									</tr>';
			

			}
			
		}
	}
}


$income_result .='<tr>
						<td class="text-left"><b>OTHER INCOME</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_other_income,2).'</b></td>
					</tr>';

$income_result .='<tr>
						<td class="text-left"><b>TOTAL INCOME</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_income+$grand_other_income,2).'</b></td>
					</tr>';


$salary = $this->company_financial_model->get_salary_expenses();
// $nssf = $this->company_financial_model->get_statutories(1);
// $nhif = $this->company_financial_model->get_statutories(2);
// $paye_amount = $this->company_financial_model->get_statutories(3);
$relief =0;// $this->company_financial_model->get_statutories(4);
$loans = 0;//$this->company_financial_model->get_statutories(5);

// $paye = $paye_amount - $relief;

$salary -= $relief;
$other_deductions = $salary;// - ($nssf+$nhif+$paye_amount+$relief);

// $total_operational_amount += $salary+$nssf+$nhif+$paye_amount;
$total_operational_amount += $salary;
// $operation_result .= $non_pharm;
$operation_result .='<tr>
						<td class="text-left"><b>Total Operation Cost</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($total_operational_amount,2).'</b></td>
					</tr>';

// get cost of goods
$parent_account_id = $this->company_financial_model->get_parent_account_id('Cost of Goods');

$account_rs = $this->ledgers_model->get_all_child_accounts($parent_account_id);

// var_dump($account_rs->result());die();
$goods_result ='';
$grand_goods = 0;
if($account_rs->num_rows() > 0)
{
 	foreach ($account_rs->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;


		$expense_of_account_rs = $this->ledgers_model->get_expense_account_transactions($account_id,1);

			// var_dump($parent_account_id);die();
		if($expense_of_account_rs->num_rows() > 0)
		{
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
			

				

				$total_goods = $dr_amount-$cr_amount;
				$grand_goods += $total_goods;
				// $grand_balance += $balance;
				$goods_result .='<tr>
									<td class="text-left">'.strtoupper($account_name).'</td>
									<td class="text-right">'.number_format($total_goods,2).'</td>
									</tr>';
			

			}
			

		}
	}
}
$goods_result .='<tr>
						<td class="text-left"><b>TOTAL GOODS SOLD</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_goods,2).'</b></td>
					</tr>';


$parent_account_id2 = $this->company_financial_model->get_parent_account_id('Expense Accounts');
$account_rs2 = $this->ledgers_model->get_all_child_accounts($parent_account_id2);

$grand_expense = 0;
$operation_result ='';
$total_operational_amount = 0;
if($account_rs2->num_rows() > 0)
{
 	foreach ($account_rs2->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;
 		 
		$expense_of_account_rs = $this->ledgers_model->get_expense_account_transactions($account_id,1);
		// var_dump($account_rs2);die();

		if($expense_of_account_rs->num_rows() > 0)
		{

			
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
				$accountName = $value4->accountName;

				$total_expense = $dr_amount-$cr_amount;
				$grand_expense += $total_expense;

				
				$operation_result .='<tr>
										<td class="text-left">'.strtoupper($account_name).'</td>
										<td class="text-right">'.number_format($total_expense,2).'</td>
									</tr>';

			}
			

		}
	}
}


$parent_account_id2 = $this->company_financial_model->get_parent_account_id('Payroll');
$account_rs2 = $this->ledgers_model->get_all_child_accounts($parent_account_id2);

// $grand_expense = 0;
if($account_rs2->num_rows() > 0)
{
 	foreach ($account_rs2->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;
 		 
		$expense_of_account_rs = $this->ledgers_model->get_expense_account_transactions($account_id,1);
		// var_dump($account_rs2);die();

		if($expense_of_account_rs->num_rows() > 0)
		{

			
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
				$accountName = $value4->accountName;

				$total_expense = $dr_amount-$cr_amount;
				$grand_expense += $total_expense;

				
				$operation_result .='<tr>
										<td class="text-left">'.strtoupper($account_name).'</td>
										<td class="text-right">'.number_format($total_expense,2).'</td>
									</tr>';

			}
			

		}
	}
}

$operation_result .='<tr>
						<td class="text-left"><b>TOTAL OPERATING EXPENSE</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_expense,2).'</b></td>
					</tr>';

$statement = $this->session->userdata('income_statement_title_search');

// var_dump($statement);die();

if(!empty($statement))
{
	$checked = $statement;
}
else {
	$checked = 'Reporting period: '.date('M j, Y', strtotime(date('Y-01-01'))).' to ' .date('M j, Y', strtotime(date('Y-m-d')));
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | P & L</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}

			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>

      <div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align" style="padding: 5px;">
            	<strong>PROFIT AND LOSS STATEMENT</strong><br>

            	<?php
            	$search_title = $this->session->userdata('income_statement_title_search');

      				 if(empty($search_title))
      				 {
      				 	$search_title = "";
      				 }
      				 else
      				 {
      				 	$search_title =$search_title;
      				 }
				 echo $search_title;
            	?>

            </div>
        </div>

    	<div class="row">
        	<div style="margin: auto;max-width: 500px;">
						<div class="col-md-12" style="margin-top: 10px;">
						
						<table class="table" id="testTable">
							<tr>
								<th style="width: 100%" colspan="2"> <h6> <strong>INCOME</strong></h6> </th>
							</tr>
							<tr>
								<th style="width: 60%"> ITEM </th>
								<th style="width: 40%">AMOUNT</th>
							</tr>

							<tbody>
								<?php echo $income_result;?>
							</tbody>
							<tr>
								<th style="width: 100%" colspan="2"> <h6> <strong>COST OF SALES</strong></h6> </th>
							</tr>
							<tr>
								<th style="width: 60%"> ITEM </th>
								<th style="width: 40%">AMOUNT</th>
							</tr>
							<tbody>									
																
								<?php echo $goods_result?>
							</tbody>

							
							<tr>
								<th style="width: 100%" colspan="2"> <h6> <strong>EXPENSES</strong></h6> </th>
							</tr>
							<tr>
								<th style="width: 60%"> ITEM </th>
								<th style="width: 40%">AMOUNT</th>
							</tr>
							<tbody>									
																
								<?php echo $operation_result;?>
							</tbody>
							<tbody>
								<tr>
									<td style="width: 60%"><strong>NET PROFIT</strong></td>
									<td style="width: 40%" class="text-right"><strong style="border-top: 2px solid #000">Ksh. <?php echo number_format($total_income - $grand_expense - $grand_goods,2)?></strong></td>
								</tr>
							</tbody>
						</table>
						<a href="#" onclick="javascript:xport.toCSV('testTable');">XLS</a>
						</div>
            </div>
        </div>

    	<div class="row" style="font-style:italic; font-size:11px;">
        	<div class="col-sm-12">
                <div class="col-sm-10 pull-left">
                    <strong>Prepared by: </strong>
                </div>
                <div class="col-sm-2 pull-right">
                    <?php echo date('jS M Y H:i a'); ?>
                </div>
            </div>

        </div>
    </body>

</html>

<script type="text/javascript">
	var xport = {
  _fallbacktoCSV: true,
  toXLS: function(tableId, filename) {
    this._filename = (typeof filename == 'undefined') ? tableId : filename;

    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls');
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }

    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it

      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};

</script>
