<!-- search -->

<div class="row">
	<div class="col-md-2">
	</div>
    <div class="col-md-8">
    	<?php
    	$statement = $this->session->userdata('income_statement_title_search');

		// var_dump($statement);die();

		if(!empty($statement))
		{
			$checked = $statement;
		}
		else {
			$checked = 'Reporting period: '.date('M j, Y', strtotime(date('Y-01-01'))).' to ' .date('M j, Y', strtotime(date('Y-m-d')));
		}


		?>

		<div class="text-center">
			<h3 class="box-title">SERVICE BILLS</h3>
			<h5 class="box-title"> <?php echo $checked?></h5>
		</div>


        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            	  <a href="<?php echo site_url();?>company-financials/profit-and-loss"  class="btn btn-sm btn-warning pull-right" style="margin-top:-25px;margin-left:5px" > Back to P & L </a>
            	 <a href="<?php echo site_url();?>financials/company_financial/export_services_bills/<?php echo $department_id;?>"  class="btn btn-sm btn-success pull-right" style="margin-top:-25px;" download> Export Patients </a>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
          <h5 class="center-align"><?php echo $this->session->userdata('search_title');?></h5>
<?php
		$result = '';
		$search = $this->session->userdata('debtors_search_query');
		if(!empty($search))
		{
			echo '<a href="'.site_url().'accounting/reports/close_reports_search" class="btn btn-sm btn-warning">Close Search</a>';
		}
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
				'
					<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Charge Date</th>
						  <th>Patient</th>
						  <th>Category</th>
						  <th>Service</th>
						  <th>Invoice Number</th>
						  <th>Billed Amount</th>
						  <th>Total</th>
						  
				';
				
			$result .= '
						</tr>
					  </thead>
					  <tbody>
			';
			
			// $personnel_query = $this->accounting_model->get_all_personnel();
			$total_waiver = 0;
			$total_payments = 0;
			$total_invoice = 0;
			$total_balance = 0;
			$total_rejected_amount = 0;
			$total_cash_balance = 0;
			$total_insurance_payments =0;
			$total_insurance_invoice =0;
			foreach ($query->result() as $row)
			{
				$total_invoiced = 0;
				$visit_date = date('jS M Y',strtotime($row->transaction_date));
				
				$invoice_date = date('jS M Y',strtotime($row->transaction_date));
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$reference_code = $row->reference_code;
				$payment_type_name = $row->payment_type_name;
				$patient_surname = $row->patient_surname;
				$patient_othernames = $row->patient_othernames;
				$transaction_name = $row->transaction_name;
				$dr_amount = $row->dr_amount;
				$cr_amount = -$row->cr_amount;



				$doctor = $row->personnel_onames.' '.$row->personnel_fname;
				
				$count++;
				
				//payment data
				$charges = '';
				
			

				$total_invoice += $dr_amount+$cr_amount;			
				$total_balance += $dr_amount+$cr_amount;
				
				$result .= 
					'
						<tr>
							<td>'.$count.'</td>
							<td>'.$invoice_date.'</td>
							<td>'.$patient_surname.' '.$patient_othernames.'</td>
							<td>'.$payment_type_name.'</td>
							<td>'.$transaction_name.'</td>
							<td>'.$reference_code.'</td>
							<td>'.number_format($dr_amount+$cr_amount,2).'</td>
							<td>'.number_format($total_balance,2).'</td>
							';
					
				
				
			}

			$result .= 
					'
						<tr>
							<td colspan=7> Totals</td>
							<td><strong>'.number_format(round($total_balance),2).'</strong></td>
						</tr> 
				';
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no visits";
		}
		
		echo $result;
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
    </div>
    <div class="col-md-2">
	</div>
  </div>