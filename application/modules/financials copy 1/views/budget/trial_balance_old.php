<input type="hidden" name="budget_year" id="budget_year" value="<?php echo $budget_year;?>">

<?php

$fixed_asset_result = '<tbody>';
$total_debits = 0;
$total_credits = 0;


$accounts_config_rs = $this->db->get('account_staging');

$patients = array();

if($accounts_config_rs->num_rows() > 0)
{
	foreach ($accounts_config_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$reference_name = $value->reference_name;

		$session_account[$reference_name] = $staing_account_id;

		
	}
}

$providers_liability_id = $session_account['providers_liability_id'];
$providers_wht_id = $session_account['providers_wht_id'];
$payroll_liability_id = $session_account['payroll_liability_id'];
$accounts_payable_id = $session_account['accounts_payable_id'];
$fixed_assets_id = $session_account['fixed_assets_id'];
$accounts_receivable_id = $session_account['accounts_receivable_id'];
$suppliers_wht_id = $session_account['suppliers_wht_id'];
$income_account_id = $session_account['income_account_id'];




// opening tb




$accounts_receivable_rs = $this->ledgers_model->get_account_ledger($accounts_receivable_id,1);

if(!empty($accounts_receivable_rs))
{	

	foreach ($accounts_receivable_rs->result() as $key => $value) {
		// code...
		$dr_amount = $value->dr_amount;
		$cr_amount = $value->cr_amount;
		$accounts_receivable = $dr_amount - $cr_amount;
	}
	$credit =0;
	if($accounts_receivable > 0)
	{
		$debit = $accounts_receivable;
		$total_debits += $debit;
	}
	else
	{
		$credit = -$accounts_receivable;
		$total_credits += $credit;
	}
	


	$fixed_asset_result .='<tr>
							<td class="text-left"><strong>RECEIVABLES</strong></td>
							<td class="text-right">
							<a href="'.site_url().'account-transactions/'.$accounts_receivable_id.'" > '.number_format($debit,2).' </a>
							</td>
							<td class="text-right">
							<a href="'.site_url().'account-transactions/'.$accounts_receivable_id.'" >'.number_format($credit,2).'</a>
							</td>
							</tr>';
}


// var_dump($suppliers_wht_id);die();
$accounts_receivable_rs = $this->ledgers_model->get_account_ledger($suppliers_wht_id,1);

$suppliers_wht = 0;
$accounts_suppliers_wht =0;
$debit = 0;
$credit = 0;

if(!empty($accounts_receivable_rs))
{	

	foreach ($accounts_receivable_rs->result() as $key => $value) {
		// code...
		$dr_amount = $value->dr_amount;
		$cr_amount = $value->cr_amount;
		$accounts_suppliers_wht = $dr_amount - $cr_amount;
	}

	if($accounts_suppliers_wht > 0)
	{
		$debit = $accounts_suppliers_wht;
		$total_debits += $debit;
	}
	else
	{
		$credit = -$accounts_suppliers_wht;
		$total_credits += $credit;
	}
	
	// $suppliers_wht = $cr_amount - $dr_amount;

	$fixed_asset_result .='<tr>
								<td class="text-left"><strong>SUPPLIERS WHT TAX</strong></td>
								<td class="text-right">
								<a href="'.site_url().'account-transactions/'.$suppliers_wht_id.'" > '.number_format($debit,2).' </a>
								</td>
								<td class="text-right">
								<a href="'.site_url().'account-transactions/'.$suppliers_wht_id.'" >'.number_format($credit,2).'</a>
								</td>
							</tr>';
	


}


$accounts_receivable_rs = $this->ledgers_model->get_account_ledger($providers_wht_id,1);

$suppliers_wht = 0;
$account_providers_wht =0;
$debit = 0;
$credit = 0;

if(!empty($accounts_receivable_rs))
{	

	foreach ($accounts_receivable_rs->result() as $key => $value) {
		// code...
		$dr_amount = $value->dr_amount;
		$cr_amount = $value->cr_amount;
		$account_providers_wht = $dr_amount - $cr_amount;
	}

	if($account_providers_wht > 0)
	{
		$debit = $account_providers_wht;
		$total_debits += $debit;
	}
	else
	{
		$credit = -$account_providers_wht;
		$total_credits += $credit;
	}
	
	// $suppliers_wht = $cr_amount - $dr_amount;

	$fixed_asset_result .='<tr>
								<td class="text-left"><strong>PROVIDERS WHT TAX</strong></td>
								<td class="text-right">
								<a href="'.site_url().'account-transactions/'.$suppliers_wht_id.'" > '.number_format($debit,2).' </a>
								</td>
								<td class="text-right">
								<a href="'.site_url().'account-transactions/'.$suppliers_wht_id.'" >'.number_format($credit,2).'</a>
								</td>
							</tr>';
	


}



// var_dump($fixed_asset_result);die();
$accounts_payable_rs = $this->ledgers_model->get_account_ledger($accounts_payable_id,1);
// var_dump($accounts_receivable);die();
$debit = 0;
$credit = 0;
if(!empty($accounts_payable_rs))
{

	foreach ($accounts_payable_rs->result() as $key => $value) {
		// code...
		$dr_amount = $value->dr_amount;
		$cr_amount = $value->cr_amount;
		$accounts_payable =  $cr_amount - $dr_amount;
	}

	if($accounts_payable > 0)
	{
		$credit = $accounts_payable;
		$total_credits += $credit;
	
	}
	else
	{
		$debit = -$accounts_payable;
		$total_debits += $debit;
	}
	


	$fixed_asset_result .='<tr>
							<td class="text-left"><strong>PAYABLES</strong></td>
							<td class="text-right">
								<a href="'.site_url().'account-transactions/'.$accounts_payable_id.'" >'.number_format($debit,2).'</a>
							</td>
							<td class="text-right">
								<a href="'.site_url().'account-transactions/'.$accounts_payable_id.'" >'.number_format($credit,2).'</a>
							</td>
							</tr>';
}


$providers_payable_rs = $this->ledgers_model->get_account_ledger($providers_liability_id,1);
// var_dump($accounts_receivable);die();
$debit = 0;
$credit = 0;
$accounts_providers = 0;
if(!empty($providers_payable_rs))
{

	foreach ($providers_payable_rs->result() as $key => $value) {
		// code...
		$dr_amount = $value->dr_amount;
		$cr_amount = $value->cr_amount;
		$accounts_providers =  $cr_amount - $dr_amount;
	}

	if($accounts_providers > 0)
	{
		$credit = $accounts_providers;
		$total_credits += $credit;
	
	}
	else
	{
		$debit = -$accounts_providers;
		$total_debits += $debit;
	}
	




	$fixed_asset_result .='<tr>
							<td class="text-left"><strong>PROVIDERS</strong></td>
							<td class="text-right">
								<a href="'.site_url().'account-transactions/'.$providers_liability_id.'" >'.number_format($debit,2).'</a>
							</td>
							<td class="text-right">
								<a href="'.site_url().'account-transactions/'.$providers_liability_id.'" >'.number_format($credit,2).'</a>
							</td>
							</tr>';
}


$accounts_payroll_rs = $this->ledgers_model->get_account_ledger($payroll_liability_id,1);
// var_dump($accounts_receivable);die();
$debit = 0;
$credit = 0;
if(!empty($accounts_payroll_rs))
{

	foreach ($accounts_payroll_rs->result() as $key => $value) {
		// code...
		$dr_amount = $value->dr_amount;
		$cr_amount = $value->cr_amount;
		$accounts_payroll =  $cr_amount - $dr_amount;
	}

	if($accounts_payroll > 0)
	{
		$credit = $accounts_payroll;
		$total_credits += $credit;
	
	}
	else
	{
		$debit = -$accounts_payroll;
		$total_debits += $debit;
	}
	




	$fixed_asset_result .='<tr>
							<td class="text-left"><strong>PAYROLL</strong></td>
							<td class="text-right">
								<a href="'.site_url().'account-transactions/'.$payroll_liability_id.'" >'.number_format($debit,2).'</a>
							</td>
							<td class="text-right">
								<a href="'.site_url().'account-transactions/'.$payroll_liability_id.'" >'.number_format($credit,2).'</a>
							</td>
							</tr>';
}




$parent_account_id = $this->company_financial_model->get_account_id('Bank');

$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($parent_account_id);
$cash_in_bank = '';
$total_income = 0;

if($children_account->num_rows() > 0)
{
	foreach ($children_account->result() as $key => $value) {
		# code...
		$account_name = $value->account_name;
		$account_id = $value->account_id;

		$debit = 0;
		$credit = 0;

		$account_rs = $this->ledgers_model->get_account_ledger($account_id,1);

		if($account_rs->num_rows() > 0)
		{
			foreach ($account_rs->result() as $key => $value) {
				// code...
				$dr_amount = $value->dr_amount;
				// $account_name = $value->account_name;
				$cr_amount = $value->cr_amount;
				$total_amount = $dr_amount - $cr_amount;


				if($total_amount > 0)
				{
					$debit = $total_amount;
					$total_debits += $debit;
				}
				else
				{
					$credit = -$total_amount;
					$total_credits += $credit;
				}

				$fixed_asset_result .='<tr>
											<td class="text-left">'.strtoupper($account_name).' </td>
											<td class="text-right">
												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($debit,2).'</a>
											</td>
											<td class="text-right">
												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($credit,2).'</a>
											</td>
										</tr>';


			}


		}

	}

}

$parent_account_id = $this->company_financial_model->get_account_id('Cost of Goods');

$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($parent_account_id);
$cash_in_bank = '';
$total_income = 0;

if($children_account->num_rows() > 0)
{
	$fixed_asset_result .='<tr>
							<th class="text-left" colspan="3">'.strtoupper('COST OF GOODS').' </th>
							
						</tr>';
	foreach ($children_account->result() as $key => $value) {
		# code...
		$account_name = $value->account_name;
		$account_id = $value->account_id;

		$debit = 0;
		$credit = 0;

		$account_rs = $this->ledgers_model->get_account_ledger($account_id,1);

		if($account_rs->num_rows() > 0)
		{
			

			foreach ($account_rs->result() as $key => $value) {
				// code...
				$dr_amount = $value->dr_amount;
				// $account_name = $value->account_name;
				$cr_amount = $value->cr_amount;
				$total_amount = $dr_amount - $cr_amount;


				if($total_amount > 0)
				{
					$debit = $total_amount;
					$total_debits += $debit;
				}
				else
				{
					$credit = -$total_amount;
					$total_credits += $credit;
				}

				$fixed_asset_result .='<tr>
											<td class="text-left">'.strtoupper($account_name).' </td>
											<td class="text-right">
												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($debit,2).'</a>
											</td>
											<td class="text-right">
												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($credit,2).'</a>
											</td>
										</tr>';


			}
		


		}

	}
	$fixed_asset_result .='<tr>
								<th class="text-left" colspan="3"> </th>
							</tr>';

}



$parent_account_id = $this->company_financial_model->get_account_id('Expense Accounts');

$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($parent_account_id);
$cash_in_bank = '';
$total_income = 0;

if($children_account->num_rows() > 0)
{
	$fixed_asset_result .='<tr>
							<th class="text-left" colspan="3">'.strtoupper('EXPENSE ACCOUNTS').' </th>
							
						</tr>';
	foreach ($children_account->result() as $key => $value) {
		# code...
		$account_name = $value->account_name;
		$account_id = $value->account_id;

		$debit = 0;
		$credit = 0;

		$account_rs = $this->ledgers_model->get_account_ledger($account_id,1);

		if($account_rs->num_rows() > 0)
		{
			

			foreach ($account_rs->result() as $key => $value) {
				// code...
				$dr_amount = $value->dr_amount;
				// $account_name = $value->account_name;
				$cr_amount = $value->cr_amount;
				$total_amount = $dr_amount - $cr_amount;


				if($total_amount > 0)
				{
					$debit = $total_amount;
					$total_debits += $debit;
				}
				else
				{
					$credit = -$total_amount;
					$total_credits += $credit;
				}

				$fixed_asset_result .='<tr>
											<td class="text-left">'.strtoupper($account_name).' </td>
											<td class="text-right">
												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($debit,2).'</a>
											</td>
											<td class="text-right">
												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($credit,2).'</a>
											</td>
										</tr>';


			}
		


		}

	}
	$fixed_asset_result .='<tr>
								<th class="text-left" colspan="3"> </th>
							</tr>';

}



$parent_account_id = $this->company_financial_model->get_account_id('Opening Balances');

$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($parent_account_id);
$cash_in_bank = '';
$total_income = 0;

if($children_account->num_rows() > 0)
{
	$fixed_asset_result .='<tr>
							<th class="text-left" colspan="3">'.strtoupper('Opening Balances').' </th>
							
						</tr>';
	foreach ($children_account->result() as $key => $value) {
		# code...
		$account_name = $value->account_name;
		$account_id = $value->account_id;

		$debit = 0;
		$credit = 0;

		$account_rs = $this->ledgers_model->get_account_ledger($account_id,1);

		if($account_rs->num_rows() > 0)
		{
			

			foreach ($account_rs->result() as $key => $value) {
				// code...
				$dr_amount = $value->dr_amount;
				// $account_name = $value->account_name;
				$cr_amount = $value->cr_amount;
				$total_amount = $dr_amount - $cr_amount;


				if($total_amount > 0)
				{
					$debit = $total_amount;
					$total_debits += $debit;
				}
				else
				{
					$credit = -$total_amount;
					$total_credits += $credit;
				}

				$fixed_asset_result .='<tr>
											<td class="text-left">'.strtoupper($account_name).' </td>
											<td class="text-right">
												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($debit,2).'</a>
											</td>
											<td class="text-right">
												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($credit,2).'</a>
											</td>
										</tr>';


			}
		


		}

	}
	$fixed_asset_result .='<tr>
								<th class="text-left" colspan="3"> </th>
							</tr>';

}



// consultant





$parent_account_id = $this->company_financial_model->get_account_id('Payroll');

$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($parent_account_id);
$cash_in_bank = '';
$total_income = 0;

if($children_account->num_rows() > 0)
{
	$fixed_asset_result .='<tr>
							<th class="text-left" colspan="3">'.strtoupper('Payroll').' </th>
							
						</tr>';
	foreach ($children_account->result() as $key => $value) {
		# code...
		$account_name = $value->account_name;
		$account_id = $value->account_id;

		$debit = 0;
		$credit = 0;

		$account_rs = $this->ledgers_model->get_account_ledger($account_id,1);

		if($account_rs->num_rows() > 0)
		{
			

			foreach ($account_rs->result() as $key => $value) {
				// code...
				$dr_amount = $value->dr_amount;
				// $account_name = $value->account_name;
				$cr_amount = $value->cr_amount;
				$total_amount = $dr_amount - $cr_amount;


				if($total_amount > 0)
				{
					$debit = $total_amount;
					$total_debits += $debit;
				}
				else
				{
					$credit = -$total_amount;
					$total_credits += $credit;
				}

				$fixed_asset_result .='<tr>
											<td class="text-left">'.strtoupper($account_name).' </td>
											<td class="text-right">
												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($debit,2).'</a>
											</td>
											<td class="text-right">
												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($credit,2).'</a>
											</td>
										</tr>';


			}
		


		}

	}
	$fixed_asset_result .='<tr>
								<th class="text-left" colspan="3"> </th>
							</tr>';

}


// $parent_account_id = $this->company_financial_model->get_account_id('LIABILITIES');

// $children_account = $this->company_financial_model->get_child_accounts_by_parent_id($parent_account_id);
// $cash_in_bank = '';
// $total_income = 0;

// if($children_account->num_rows() > 0)
// {
// 	$fixed_asset_result .='<tr>
// 							<th class="text-left" colspan="3">'.strtoupper('LIABILITIES').' </th>
							
// 						</tr>';
// 	foreach ($children_account->result() as $key => $value) {
// 		# code...
// 		$account_name = $value->account_name;
// 		$account_id = $value->account_id;

// 		$debit = 0;
// 		$credit = 0;

// 		$account_rs = $this->ledgers_model->get_account_ledger($account_id,1);

// 		if($account_rs->num_rows() > 0)
// 		{
			

// 			foreach ($account_rs->result() as $key => $value) {
// 				// code...
// 				$dr_amount = $value->dr_amount;
// 				// $account_name = $value->account_name;
// 				$cr_amount = $value->cr_amount;
// 				$total_amount = $dr_amount - $cr_amount;


// 				if($total_amount > 0)
// 				{
// 					$debit = $total_amount;
// 					$total_debits += $debit;
// 				}
// 				else
// 				{
// 					$credit = -$total_amount;
// 					$total_credits += $credit;
// 				}

// 				$fixed_asset_result .='<tr>
// 											<td class="text-left">'.strtoupper($account_name).' </td>
// 											<td class="text-right">
// 												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($debit,2).'</a>
// 											</td>
// 											<td class="text-right">
// 												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($credit,2).'</a>
// 											</td>
// 										</tr>';


// 			}
		


// 		}

// 	}
// 	$fixed_asset_result .='<tr>
// 								<th class="text-left" colspan="3"> </th>
// 							</tr>';

// }


$parent_account_id = $this->company_financial_model->get_account_id('INCOMES');

$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($parent_account_id);
$cash_in_bank = '';
$total_income = 0;

if($children_account->num_rows() > 0)
{
	$fixed_asset_result .='<tr>
							<th class="text-left" colspan="3">'.strtoupper('INCOME').' </th>
							
						</tr>';
	foreach ($children_account->result() as $key => $value) {
		# code...
		$account_name = $value->account_name;
		$account_id = $value->account_id;

		$debit = 0;
		$credit = 0;

		$account_rs = $this->ledgers_model->get_account_ledger($account_id,1);

		if($account_rs->num_rows() > 0)
		{
			

			foreach ($account_rs->result() as $key => $value) {
				// code...
				$dr_amount = $value->dr_amount;
				// $account_name = $value->account_name;
				$cr_amount = $value->cr_amount;
				$total_amount = $dr_amount - $cr_amount;


				if($total_amount > 0)
				{
					$debit = $total_amount;
					$total_debits += $debit;
				}
				else
				{
					$credit = -$total_amount;
					$total_credits += $credit;
				}

				$fixed_asset_result .='<tr>
											<td class="text-left">'.strtoupper($account_name).' </td>
											<td class="text-right">
												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($debit,2).'</a>
											</td>
											<td class="text-right">
												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($credit,2).'</a>
											</td>
										</tr>';


			}
		


		}

	}
	$fixed_asset_result .='<tr>
								<th class="text-left" colspan="3"> </th>
							</tr>';

}


$parent_account_id = $this->company_financial_model->get_account_id('Fixed Assets');
// var_dump($parent_account_id);die();
$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($parent_account_id);
$cash_in_bank = '';
$total_income = 0;

if($children_account->num_rows() > 0)
{
	$fixed_asset_result .='<tr>
							<th class="text-left" colspan="3">'.strtoupper('Fixed Assets').' </th>
							
						</tr>';
	foreach ($children_account->result() as $key => $value) {
		# code...
		$account_name = $value->account_name;
		$account_id = $value->account_id;

		$debit = 0;
		$credit = 0;

		// var_dump($account_id);die();

		$account_rs = $this->ledgers_model->get_account_ledger($account_id,1);

		if($account_rs->num_rows() > 0)
		{
			

			foreach ($account_rs->result() as $key => $value) {
				// code...
				$dr_amount = $value->dr_amount;
				// $account_name = $value->account_name;
				$cr_amount = $value->cr_amount;
				$total_amount = $dr_amount - $cr_amount;


				if($total_amount > 0)
				{
					$debit = $total_amount;
					$total_debits += $debit;
				}
				else
				{
					$credit = -$total_amount;
					$total_credits += $credit;
				}

				$fixed_asset_result .='<tr>
											<td class="text-left">'.strtoupper($account_name).' </td>
											<td class="text-right">
												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($debit,2).'</a>
											</td>
											<td class="text-right">
												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($credit,2).'</a>
											</td>
										</tr>';


			}
		


		}

	}
	$fixed_asset_result .='<tr>
								<th class="text-left" colspan="3"> </th>
							</tr>';

}


$parent_account_id = $this->company_financial_model->get_account_id('DIRECTORS CAPITAL ACCOUNT');
// var_dump($parent_account_id);die();
$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($parent_account_id);
$cash_in_bank = '';
$total_income = 0;

if($children_account->num_rows() > 0)
{
	$fixed_asset_result .='<tr>
							<th class="text-left" colspan="3">'.strtoupper('DIRECTORS CAPITAL ACCOUNT').' </th>
							
						</tr>';
	foreach ($children_account->result() as $key => $value) {
		# code...
		$account_name = $value->account_name;
		$account_id = $value->account_id;

		$debit = 0;
		$credit = 0;

		$account_rs = $this->ledgers_model->get_account_ledger($account_id,1);

		if($account_rs->num_rows() > 0)
		{
			

			foreach ($account_rs->result() as $key => $value) {
				// code...
				$dr_amount = $value->dr_amount;
				// $account_name = $value->account_name;
				$cr_amount = $value->cr_amount;
				$total_amount = $dr_amount - $cr_amount;


				if($total_amount > 0)
				{
					$debit = $total_amount;
					$total_debits += $debit;
				}
				else
				{
					$credit = -$total_amount;
					$total_credits += $credit;
				}

				// var_dump($cr_amount);die();

				$fixed_asset_result .='<tr>
											<td class="text-left">'.strtoupper($account_name).' </td>
											<td class="text-right">
												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($debit,2).'</a>
											</td>
											<td class="text-right">
												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($credit,2).'</a>
											</td>
										</tr>';


			}
		


		}

	}
	$fixed_asset_result .='<tr>
								<th class="text-left" colspan="3"> </th>
							</tr>';

}



// $parent_account_id = $this->company_financial_model->get_account_id('SHAREHOLDERS LOAN');
// // var_dump($parent_account_id);die();
// $children_account = $this->company_financial_model->get_child_accounts_by_parent_id($parent_account_id);
// $cash_in_bank = '';
// $total_income = 0;

// if($children_account->num_rows() > 0)
// {
// 	$fixed_asset_result .='<tr>
// 							<th class="text-left" colspan="3">'.strtoupper('SHAREHOLDERS LOAN').' </th>
							
// 						</tr>';
// 	foreach ($children_account->result() as $key => $value) {
// 		# code...
// 		$account_name = $value->account_name;
// 		$account_id = $value->account_id;

// 		$debit = 0;
// 		$credit = 0;

// 		$account_rs = $this->ledgers_model->get_account_ledger($account_id,1);

// 		if($account_rs->num_rows() > 0)
// 		{
			

// 			foreach ($account_rs->result() as $key => $value) {
// 				// code...
// 				$dr_amount = $value->dr_amount;
// 				// $account_name = $value->account_name;
// 				$cr_amount = $value->cr_amount;
// 				$total_amount = $dr_amount - $cr_amount;


// 				if($total_amount > 0)
// 				{
// 					$debit = $total_amount;
// 					$total_debits += $debit;
// 				}
// 				else
// 				{
// 					$credit = -$total_amount;
// 					$total_credits += $credit;
// 				}

// 				// var_dump($cr_amount);die();

// 				$fixed_asset_result .='<tr>
// 											<td class="text-left">'.strtoupper($account_name).' </td>
// 											<td class="text-right">
// 												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($debit,2).'</a>
// 											</td>
// 											<td class="text-right">
// 												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($credit,2).'</a>
// 											</td>
// 										</tr>';


// 			}
		


// 		}

// 	}
// 	$fixed_asset_result .='<tr>
// 								<th class="text-left" colspan="3"> </th>
// 							</tr>';

// }












$fixed_asset_result .='
						</tbody>
						<tfoot>
						<tr>
								<th class="text-left">SUBTOTAL</th>
								<th class="text-right">
								'.number_format($total_debits,2).'
								</th>
								<th class="text-right">
								'.number_format($total_credits,2).'
								</th>
							</tr>';
$fixed_asset_result .='
						</tfoot>';

?>
<div class="row">
	
		<div class="col-md-4">
			<section class="panel">
				<header class="panel-heading">
						<h5 class="pull-left"><i class="icon-reorder"></i>Search </h5>
						<div class="clearfix"></div>
				</header>
				<!-- /.box-header -->
				<div class="panel-body">
					<?php echo form_open("financials/budget/search_trial_balance", array("class" => "form-horizontal"));?>
		      		<div class="row">
		      			<div class="form-group">
							<label class="col-md-4 control-label">Date From: </label>

							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon">
											<i class="fa fa-calendar"></i>
									</span>
									<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Date From" value="" autocomplete="off">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Date To: </label>

							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon">
											<i class="fa fa-calendar"></i>
									</span>
									<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder=" Date to" value="" autocomplete="off">
								</div>
							</div>
						</div>
						<div class="form-group">
			                <div class="col-lg-8 col-lg-offset-2">
			                	<div class="center-align">
			                   		<button type="submit" class="btn btn-info">Search</button>
			    				</div>
			                </div>
			            </div>
		      		</div>
		      		<?php echo form_close();?>
		      	</div>
		    </section>




			<div class="text-center">
				<h4 class="box-title">Trial Balance</h4>
				<h5 class="box-title"> <?php echo $this->session->userdata('general_ledger_search_title');?> </h5>
				<h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
			</div>

			<div class="col-md-12">
				<div class="form-group">
			        <a href="<?php echo site_url().'print-trial-balance'?>" target="_blank" class="btn btn-md btn-warning col-md-12"><i class="fa fa-print"></i> Print Trail Balance</a>
			  	</div>
			    <div class="form-group">
				      <a href="<?php echo site_url().'company-financials/profit-and-loss'?>"  class="btn btn-md btn-info col-md-12"><i class="fa fa-file"></i> Income Statement</a>
				</div>
				<div class="form-group">
				      <a href="<?php echo site_url().'company-financials/balance-sheet'?>"  class="btn btn-md btn-primary col-md-12"><i class="fa fa-file"></i> Balance Sheet</a>
				</div>
				<div class="form-group">
				      <a href="<?php echo site_url().'company-financials/general-ledger'?>"  class="btn btn-md btn-success col-md-12"><i class="fa fa-file"></i> General Ledger</a>
				</div>
		  
			</div>
		</div>
		<div class="col-md-8">
			<div class="panel-body" style="height:85vh;overflow-y: scroll;padding:0px;">
				<table class="table  table-striped table-condensed table-linked" style="margin-left: 10px">

		    		<thead>
						<tr>
		        			<th class="text-left">ACCOUNT NAME</th>
							<th class="text-right">DEBIT</th>
							<th class="text-right">CREDIT</th>
						</tr>
					</thead>
					
							<?php
							echo $fixed_asset_result;

							?>
					
				</table>
			</div>
		</div>
		
		
	
	
</div>
<script type="text/javascript">
	$(function() {
		// var budget_year = <?php echo $budget_year?>;
			var budget_year = document.getElementById("budget_year").value;

		// alert(budget_year);
		get_year_budget(budget_year);
		
	});

	function get_year_budget(budget_year)
	{
		var config_url = $('#config_url').val();
	 	var url = config_url+"financials/budget/get_year_budget_summary/"+budget_year;
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#budget-table").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}
			$("#title-div").html('REPORT FOR '+budget_year);

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
	}
	function add_budget_item(budget_year)
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = $('#config_url').val();
		var data_url = config_url+"financials/budget/add_budget_item/"+budget_year;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			$('.datepicker').datepicker({
					    format: 'yyyy-mm-dd'
					});



			$('.timepicker').timepicker({
			    timeFormat: 'h:mm p',
			    interval: 60,
			    minTime: '10',
			    maxTime: '6:00pm',
			    defaultTime: '11',
			    startTime: '10:00',
			    dynamic: false,
			    dropdown: true,
			    scrollbar: true
			});
			// alert(data);
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
	}
	function close_side_bar()
	{
		// $('html').removeClass('sidebar-right-opened');
		document.getElementById("sidebar-right").style.display = "none"; 
		document.getElementById("current-sidebar-div").style.display = "none"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 
		tinymce.remove();
	}

	$(document).on("submit","form#confirm-budget-item",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);
		var budget_year = $('#budget_year').val();	
		var budget_month = $('#budget_month').val();	
		var account_id = $('#account_id').val();	
		var config_url = $('#config_url').val();

		var url = config_url+"financials/budget/confirm_budget_item/"+budget_year;
		 
		 // alert(url);
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:form_data,
	   dataType: 'text',
	   processData: false,
	   contentType: false,
	   success:function(data){
	      var data = jQuery.parseJSON(data);
	    
	      	if(data.status == "success")
			{
				
				
				// close_side_bar();
				get_budget_items(budget_year,budget_month,account_id);
				get_year_budget(budget_year);


				
			}
			else
			{
				alert(data.message);
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
		 
		
	   
		
	});
	function delete_budget_item(budget_item_id,budget_year,month,account_id)
	{

		var res = confirm('Are you sure you want to delete this entry ?');


		if(res)
		{
			var config_url = $('#config_url').val();
			var data_url = config_url+"financials/budget/delete_budget_item/"+budget_item_id;
			//window.alert(data_url);
			$.ajax({
			type:'POST',
			url: data_url,
			data:{appointment_id: 1},
			dataType: 'text',
			success:function(data){
				

				 get_budget_items(budget_year,month,account_id);
				 get_year_budget(budget_year);
				},
				error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

			});
		}
		

	}

	function edit_budget_item(month,account_id,budget_year)
	{
		close_side_bar();
		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = $('#config_url').val();
		var data_url = config_url+"financials/budget/add_budget_item/"+budget_year+"/"+month+"/"+account_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			 get_budget_items(budget_year,month,account_id);
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});

	}

	function get_budget_items(budget_year,month,account_id)
	{

		var config_url = $('#config_url').val();
	 	var url = config_url+"financials/budget/get_budget_list/"+budget_year+"/"+month+"/"+account_id;
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#visit-payment-div").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}
			// $("#title-div").html('REPORT FOR '+budget_year);

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
	}

		var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};

</script>