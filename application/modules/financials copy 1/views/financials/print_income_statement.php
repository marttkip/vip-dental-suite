
<?php

$this->db->where('account_staging_delete = 0');
$accounts_config_rs = $this->db->get('account_staging');

$patients = array();

if($accounts_config_rs->num_rows() > 0)
{
	foreach ($accounts_config_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$reference_name = $value->reference_name;

		$session_account[$reference_name] = $staing_account_id;

		
	}
}

$providers_liability_id = $session_account['providers_liability_id'];
$providers_wht_id = $session_account['providers_wht_id'];
$payroll_liability_id = $session_account['payroll_liability_id'];
$accounts_payable_id = $session_account['accounts_payable_id'];
$fixed_assets_id = $session_account['fixed_assets_id'];
$accounts_receivable_id = $session_account['accounts_receivable_id'];
$suppliers_wht_id = $session_account['suppliers_wht_id'];
$income_account_id = $session_account['income_account_id'];
$cost_of_goods_id = $session_account['cost_of_goods_id'];
$expense_account_id = $session_account['expense_account_id'];
$other_income_account_id = $session_account['other_income_account_id'];


$grand_income = 0;
$income_result = '';


$account_report_rs = $this->company_financial_model->get_staging_accounts(2);

$arrOrphansIncome = array();
if($account_report_rs->num_rows() > 0)
{

	foreach ($account_report_rs->result() as $key => $value) {
		// code...
		$account_id = $value->account_id;
		$has_children = $value->has_children;


		if($has_children)
		{
			// check if account has children
			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($account_id);
				

			if($children_account->num_rows() > 0)
			{	

				foreach ($children_account->result() as $key_two => $value_two) {
					// code...
					$child_account_id = $value_two->account_id;
					$arrOrphansIncome[$child_account_id] =  $child_account_id;
				}

				
			}
			else
			{
				$arrOrphansIncome[$value->account_id] =  $value->account_id;
			}

		}
		else
		{
			$arrOrphansIncome[$value->account_id] =  $value->account_id;
		}

		

		
	}

}

$arrBranchesIncome = array();
foreach($arrOrphansIncome as $serialIncome){
	$arrSerial = explode(".", $serialIncome);
	$new_serial = "";
	foreach($arrSerial as $part){
		$new_serial .= (strlen($new_serial)>0?".":"") . $part;

		if(!in_array($new_serial, $arrBranchesIncome))
			array_push($arrBranchesIncome, $new_serial);
	}
}

// var_dump($arrBranchesIncome);die();
$query_three = $this->ledgers_model->get_account_ledger_by_accounts_opening_periods($arrBranchesIncome);

$grouped_array_income = array();
foreach ($query_three->result() as $element_three) {
    $grouped_array_income[$element_three->accountId][] = $element_three;
}



$all_transacted_rs = $this->ledgers_model->get_account_ledger_by_accounts(1);


$arr = json_decode(json_encode ( $all_transacted_rs->result() ) , true);

$grouped_array = array();
foreach ($all_transacted_rs->result() as $element) {
	
    $grouped_array[$element->accountId][] = $element;
}


// all income



$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($income_account_id);


if($children_account->num_rows() > 0)
{	
	
	foreach ($children_account->result() as $key => $value) {

		$account_name = $value->account_name;
		$account_id = $value->account_id;

		// $second_array = count($grouped_array_income[$account_id]);
		$opening_balance = 0;
		$total_income = 0;
		// if($second_array > 0)
		// {

		// 	$debit_opening = 0;
		// 	$credit_opening = 0;
		// 	for ($z=0; $z < $second_array; $z++) { 

		// 		$opening_dr_amount = $grouped_array_income[$account_id][$z]->dr_amount;
		// 	    $opening_cr_amount = $grouped_array_income[$account_id][$z]->cr_amount;
		// 	    $opening_balance = $opening_dr_amount - $opening_cr_amount;

		// 	    if($opening_balance > 0)
		// 	    {
		// 	    	$debit_opening = $opening_balance;
		// 			// $total_debits += $debit_opening;
				

		// 			$opening = number_format($debit_opening,2);
		// 	    }
		// 	    else
		// 		{
		// 			$credit_opening = -$opening_balance;
		// 			// $total_credits += $credit_opening;
		// 			$opening = "(".number_format($credit_opening,2).")";

		// 		}


		// 	}
		// }

		// if($opening_balance < 0)
		// {
		// 	$opening_balance = -$opening_balance;
		// }

		// $grand_income += $opening_balance;
		// $total_income +=  $opening_balance;


		$first_array = count($grouped_array[$account_id]);
		$debit = 0;
		$credit = 0;
		if($first_array > 0)
		{

			for ($i=0; $i < $first_array; $i++) { 
		  		$dr_amount = $grouped_array[$account_id][$i]->dr_amount;
			    $cr_amount = $grouped_array[$account_id][$i]->cr_amount;
			    // $accountName = $grouped_array[$account_id][$i]->accountName;
			    // $total_amount = $dr_amount - $cr_amount;


				
				$total_income = $cr_amount-$dr_amount;
				$grand_income += $total_income;

				


			}

		}

		$income_result .='<tr>
							<td class="text-left">'.strtoupper($account_name).'</td>
							<td class="text-right"><a href="'.site_url().'account-transactions/'.$account_id.'" >'.number_format($total_income,2).'</a></td>
						</tr>';
	}
}

$income_result .='<tr>
					<td class="text-left"><b>INCOME</b></td>
					<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_income,2).'</b></td>
				</tr>';


$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($other_income_account_id);

$grand_other_income =0;
if($children_account->num_rows() > 0)
{	
	
	foreach ($children_account->result() as $key => $value) {

		$account_name = $value->account_name;
		$account_id = $value->account_id;


		$second_array = count($grouped_array_income[$account_id]);
		$opening_balance = 0;
		// $total_income = 0;
		// if($second_array > 0)
		// {

		// 	$debit_opening = 0;
		// 	$credit_opening = 0;
		// 	for ($z=0; $z < $second_array; $z++) { 

		// 		$opening_dr_amount = $grouped_array_income[$account_id][$z]->dr_amount;
		// 	    $opening_cr_amount = $grouped_array_income[$account_id][$z]->cr_amount;
		// 	    $opening_balance = $opening_dr_amount - $opening_cr_amount;

		// 	    if($opening_balance > 0)
		// 	    {
		// 	    	$debit_opening = $opening_balance;
		// 			// $total_debits += $debit_opening;
				

		// 			$opening = number_format($debit_opening,2);
		// 	    }
		// 	    else
		// 		{
		// 			$credit_opening = -$opening_balance;
		// 			// $total_credits += $credit_opening;
		// 			$opening = "(".number_format($credit_opening,2).")";

		// 		}


		// 	}
		// }

		// if($opening_balance < 0)
		// {
		// 	$opening_balance = -$opening_balance;
		// }

		// $grand_income += $opening_balance;
		// $total_income +=  $opening_balance;
		// $grand_other_income += $opening_balance;
		// $total_other_income += $opening_balance;


		$first_array = count($grouped_array[$account_id]);
		$debit = 0;
		$credit = 0;
		if($first_array > 0)
		{

			for ($i=0; $i < $first_array; $i++) { 
		  		$dr_amount = $grouped_array[$account_id][$i]->dr_amount;
			    $cr_amount = $grouped_array[$account_id][$i]->cr_amount;
			    // $accountName = $grouped_array[$account_id][$i]->accountName;
			    // $total_amount = $dr_amount - $cr_amount;

			    $total_other_income = $cr_amount-$dr_amount;
				$grand_other_income += $total_other_income;
				
				$total_income = $cr_amount-$dr_amount;
				$grand_income += $total_income;

				


			}

		}
		$income_result .='<tr>
								<td class="text-left">'.strtoupper($account_name).'</td>
								<td class="text-right"><a href="'.site_url().'account-transactions/'.$account_id.'" >'.number_format($total_income,2).'</a></td>
							</tr>';
	}
}


$income_result .='<tr>
						<td class="text-left"><b>OTHER INCOME</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_other_income,2).'</b></td>
					</tr>';

$income_result .='<tr>
						<td class="text-left"><b>TOTAL INCOME</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_income+$grand_other_income,2).'</b></td>
					</tr>';


$operation_result .='<tr>
						<td class="text-left"><b>Total Operation Cost</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($total_operational_amount,2).'</b></td>
					</tr>';






// get cost of goods


$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($cost_of_goods_id);

$goods_result ='';
$grand_goods = 0;
if($children_account->num_rows() > 0)
{	
	
	foreach ($children_account->result() as $key => $value) {

		$account_name = $value->account_name;
		$account_id = $value->account_id;

		$second_array = count($grouped_array_income[$account_id]);
		$opening_balance = 0;
		$total_goods = 0;
		// if($second_array > 0)
		// {

		// 	$debit_opening = 0;
		// 	$credit_opening = 0;
		// 	for ($z=0; $z < $second_array; $z++) { 

		// 		$opening_dr_amount = $grouped_array_income[$account_id][$z]->dr_amount;
		// 	    $opening_cr_amount = $grouped_array_income[$account_id][$z]->cr_amount;
		// 	    $opening_balance = $opening_dr_amount - $opening_cr_amount;

		// 	    if($opening_balance > 0)
		// 	    {
		// 	    	$debit_opening = $opening_balance;
		// 			// $total_debits += $debit_opening;
				

		// 			$opening = number_format($debit_opening,2);
		// 	    }
		// 	    else
		// 		{
		// 			$credit_opening = -$opening_balance;
		// 			// $total_credits += $credit_opening;
		// 			$opening = "(".number_format($credit_opening,2).")";

		// 		}


		// 	}
		// }

		// if($opening_balance < 0)
		// {
		// 	$opening_balance = -$opening_balance;
		// }

		// $total_goods += $opening_balance;
		// $grand_goods +=  $opening_balance;


		$first_array = count($grouped_array[$account_id]);
	
		if($first_array > 0)
		{

			for ($i=0; $i < $first_array; $i++) { 
		  		$dr_amount = $grouped_array[$account_id][$i]->dr_amount;
			    $cr_amount = $grouped_array[$account_id][$i]->cr_amount;
			    // $accountName = $grouped_array[$account_id][$i]->accountName;
			    $total_amount = $dr_amount - $cr_amount;

			    $total_goods += $total_amount;
				$grand_goods += $total_amount;
				// $grand_balance += $balance;
				
			}

		}
		$goods_result .='<tr>
							<td class="text-left">'.strtoupper($account_name).'</td>
							<td class="text-right"><a href="'.site_url().'account-transactions/'.$account_id.'"  target="_blank">'.number_format($total_goods,2).'</a></td>
						</tr>';
	}
}
$goods_result .='<tr>
						<td class="text-left"><b>TOTAL DIRECT COSTS</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_goods,2).'</b></td>
					</tr>';






// expense accounts


$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($expense_account_id);

$grand_expense = 0;
$operation_result ='';
$total_operational_amount = 0;


if($children_account->num_rows() > 0)
{	
	
	foreach ($children_account->result() as $key => $value) {

		$account_name = $value->account_name;
		$account_id = $value->account_id;

		// $second_array = count($grouped_array_income[$account_id]);
		$opening_balance = 0;
		$total_expense = 0;
		// if($second_array > 0)
		// {

		// 	$debit_opening = 0;
		// 	$credit_opening = 0;
		// 	for ($z=0; $z < $second_array; $z++) { 

		// 		$opening_dr_amount = $grouped_array_income[$account_id][$z]->dr_amount;
		// 	    $opening_cr_amount = $grouped_array_income[$account_id][$z]->cr_amount;
		// 	    $opening_balance = $opening_dr_amount - $opening_cr_amount;

		// 	    if($opening_balance > 0)
		// 	    {
		// 	    	$debit_opening = $opening_balance;
		// 			// $total_debits += $debit_opening;
				

		// 			$opening = number_format($debit_opening,2);
		// 	    }
		// 	    else
		// 		{
		// 			$credit_opening = -$opening_balance;
		// 			// $total_credits += $credit_opening;
		// 			$opening = "(".number_format($credit_opening,2).")";

		// 		}


		// 	}
		// }

		// if($opening_balance < 0)
		// {
		// 	$opening_balance = -$opening_balance;
		// }

		// $total_expense += $opening_balance;
		// $grand_expense +=  $opening_balance;


		$first_array = count($grouped_array[$account_id]);
	
		if($first_array > 0)
		{

			for ($i=0; $i < $first_array; $i++) { 
		  		$dr_amount = $grouped_array[$account_id][$i]->dr_amount;
			    $cr_amount = $grouped_array[$account_id][$i]->cr_amount;
			    
			    $total_amount =  $dr_amount-$cr_amount;
			    $total_expense += $total_amount;


				$grand_expense += $total_amount;

				
				



			}

		}
		$operation_result .='<tr>
								<td class="text-left">'.strtoupper($account_name).'</td>
								<td class="text-right"><a href="'.site_url().'account-transactions/'.$account_id.'" >'.number_format($total_expense,2).'</a></td>
							</tr>';
	}
}




$operation_result .='<tr>
						<td class="text-left"><b>TOTAL OPERATING EXPENSE</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_expense,2).'</b></td>
					</tr>';


$statement = $this->session->userdata('income_statement_title_search');

// var_dump($statement);die();

if(!empty($statement))
{
	$checked = $statement;
}
else {
	$checked = 'Reporting period: '.date('M j, Y', strtotime(date('Y-01-01'))).' to ' .date('M j, Y', strtotime(date('Y-m-d')));
}


$closing_stock =  $this->company_financial_model->get_opening_stock_value();


?>

<?php
	$date_from = $this->session->userdata('date_from_general_ledger');
	$general_ledger_search_title =	$this->session->userdata('general_ledger_search_title');


	if(!empty($general_ledger_search_title))
	{
		$search_title = $general_ledger_search_title;
	}
	else 
	{
		$search_title = 'ALL TIME REPORT';
	}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | P & L</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}

			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>

      <div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align" style="padding: 5px;">
            	<strong>PROFIT AND LOSS STATEMENT</strong><br>

            	<?php
            	// $search_title = $this->session->userdata('balance_sheet_title_search');

      			// 	 if(empty($search_title))
      			// 	 {
      			// 	 	$search_title = "";
      			// 	 }
      			// 	 else
      			// 	 {
      			// 	 	$search_title =$search_title;
      			// 	 }
				//  echo $search_title;
            	?>

            </div>
        </div>

    	<div class="row">
        	<div style="margin: auto;max-width: 700px;">
						<div class="col-md-12" style="margin-top: 10px;">
						
						<table class="table" id="testTable">
							<tr>
								<th style="width: 100%" colspan="2"> <h6> <strong>INCOME</strong></h6> </th>
							</tr>
							<tr>
								<th style="width: 60%"> ACCOUNT </th>
								<th style="width: 40%">BALANCE</th>
							</tr>

							<tbody>
								<?php echo $income_result;?>
							</tbody>
							<tr>
								<th style="width: 100%" colspan="2"> <h6> <strong>DIRECT COSTS</strong></h6> </th>
							</tr>		
							<tr>
								<th style="width: 60%"> ACCOUNT </th>
								<th style="width: 40%">BALANCE</th>
							</tr>
							<tbody>									
																
								<?php echo $goods_result;?>
							</tbody>
						
							



							<tr>
								<th style="width: 100%" colspan="2"> <h6> <strong>OPERATING EXPENSE</strong></h6> </th>
							</tr>
							<tr>
								<th style="width: 60%"> ACCOUNT </th>
								<th style="width: 40%">BALANCE</th>
							</tr>
							<tbody>									
																
								<?php echo $operation_result;?>
							</tbody>
							
							<tbody>
								<tr>
									<td style="width: 60%"><strong>NET PROFIT</strong></td>
									<td style="width: 40%" class="text-right"><strong style="border-top: 2px solid #000">Ksh. <?php echo number_format($grand_income - $grand_goods - $grand_expense,2)?></strong></td>
								</tr>
							</tbody>
						</table>
						<a href="#" onclick="javascript:xport.toCSV('testTable');">XLS</a>
						</div>
            </div>
        </div>

    	<div class="row" style="font-style:italic; font-size:11px;">
        	<div class="col-sm-12">
                <div class="col-sm-10 pull-left">
                    <strong>Prepared by: </strong>
                </div>
                <div class="col-sm-2 pull-right">
                    <?php echo date('jS M Y H:i a'); ?>
                </div>
            </div>

        </div>
    </body>

</html>

<script type="text/javascript">
	var xport = {
  _fallbacktoCSV: true,
  toXLS: function(tableId, filename) {
    this._filename = (typeof filename == 'undefined') ? tableId : filename;

    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls');
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }

    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it

      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};

</script>
