
<section class="panel" >
	<div class="panel-body" style="height:90vh;overflow-y: scroll;overflow-x: none;">
		<div style="padding-left:10px;padding-right:10px">
	        <div class="col-md-12">
	        	<h4 style="font-weight:bold">ADD STAGING ACCOUNTS</h4>
	        	<br>
	        	<div class="row">
	                <div class="padd">
	                	<?php 
						    echo form_open("reception/register_other_patient", array("class" => "form-horizontal","id"=>"add-staging-account"));
						   
						?>
	                	<div class="col-md-12">
	                		<div class="col-md-6">
				                <div class="form-group">
				                    <label class="col-md-4 control-label">Name: *</label>
				                    
				                    <div class="col-md-8">
					                	<input type="text" class="form-control" name="account_staging_name" id="account_staging_name" value="" autocomplete="off" required></td>
					                </div>
					            </div>
	                		</div>
	                		<div class="col-md-6">
                			   <div class="form-group">
				                    <label class="col-md-4 control-label">Account: *</label>
				                    <div class="col-md-8">
			                			<select id="account_id" name="account_id" class="form-control" required>                                    
				                            <?php
				                             $v_data['accounts'] = $accounts = $this->company_financial_model->get_all_accounts();
				                             if($accounts->num_rows() > 0)
				                             {
				                                 foreach($accounts->result() as $row):
				                                     // $company_name = $row->company_name;
				                                     $account_name = $row->account_name;
				                                     $account_id = $row->account_id;

				                                     $parent_account = $row->parent_account;

				                                     if($parent_account != $current_parent)
				                                     {
				                                     	  $account_from_name = $this->company_financial_model->get_account_name($parent_account);
				                                     	  $changed .= '<optgroup label="'.$account_from_name.'">';
				                                     }

				                                    
				                                   	 	$changed .= "<option value=".$account_id."> ".$account_name."</option>";
				                                   	 

				                                   	 $current_parent = $parent_account;
				                                   	 if($parent_account != $current_parent)
				                                     {
				                                     	$changed .= '</optgroup>';
				                                     }	
				                                 endforeach;
				                             }
				                             echo $changed;
				                             ?> 
				                        </select>
				                    </div>
				                </div>
	                		
	                		</div>
	                		
	                	</div>

	                	<div class="col-md-12">
	                		<div class="col-md-12 center-align">
	                		
	                			<button class="btn btn-sm btn-warning" type="submit" onclick="return confirm('Are you sure you want to add this account ..?')"> ADD ACCOUNT</button>
	                		</div>
	                		
	                	</div>

	                	</form>

	                </div>
	            </div>
	       	</div>
	   	</div>
	</div>
</section>