<?php
$date_from = $this->session->userdata('date_from_general_ledger');
$explode = explode('-', $date_from);
$current_year = $explode[0];
$previous_year_earnings = $explode[0] -1;
$projected_account_id = $account_id;
	$date_from = $this->session->userdata('date_from_general_ledger');
	$date_to = $this->session->userdata('date_to_general_ledger');
	$add = '';
	if(!empty($date_from) OR !empty($date_to))
	{

		$date_from = $date_from;
		$date_to = $date_to;

	}

	else
	{

		$date_from = date('Y-m-01');
		$date_to = date('Y-m-d');
		

	}

	$search_title = $this->session->userdata('general_ledger_search_title');

?>
<?php

$grand_income = 0;
$income_result = '';


$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

$patients = array();

if($accounts_config_rs->num_rows() > 0)
{
	foreach ($accounts_config_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$reference_name = $value->reference_name;

		$session_account[$reference_name] = $staing_account_id;

		
	}
}

$providers_liability_id = $session_account['providers_liability_id'];
$providers_wht_id = $session_account['providers_wht_id'];
$payroll_liability_id = $session_account['payroll_liability_id'];
$accounts_payable_id = $session_account['accounts_payable_id'];
$fixed_assets_id = $session_account['fixed_assets_id'];
$accounts_receivable_id = $session_account['accounts_receivable_id'];
$suppliers_wht_id = $session_account['suppliers_wht_id'];
$income_account_id = $session_account['income_account_id'];
$cost_of_goods_id = $session_account['cost_of_goods_id'];
$expense_account_id = $session_account['expense_account_id'];
$other_income_account_id = $session_account['other_income_account_id'];
$bank_id = $session_account['bank_id'];
$capital_account_id  = $session_account['capital_account_id'];
$loans_account_id  = $session_account['loans_account_id'];
$acc_depreciation_id = $session_account['acc_depreciation_id'];
$asset_depreciation_id = $session_account['asset_depreciation_id'];

// all income accounts

$account_report_rs = $this->company_financial_model->get_staging_accounts(2);

$arrOrphansIncome = array();
if($account_report_rs->num_rows() > 0)
{

	foreach ($account_report_rs->result() as $key => $value) {
		// code...
		$account_id = $value->account_id;
		$has_children = $value->has_children;


		if($has_children)
		{

			// check if account has children
			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($account_id);
				

			if($children_account->num_rows() > 0)
			{	

				foreach ($children_account->result() as $key_two => $value_two) {
					// code...
					$child_account_id = $value_two->account_id;
					$arrOrphansIncome[$child_account_id] =  $child_account_id;
				}

				
			}
			else
			{
				$arrOrphansIncome[$value->account_id] =  $value->account_id;
			}
		}
		else
		{
			$arrOrphansIncome[$value->account_id] =  $value->account_id;
		}

		
	}

}

$arrBranchesIncome = array();
foreach($arrOrphansIncome as $serialIncome){
	$arrSerial = explode(".", $serialIncome);
	$new_serial = "";
	foreach($arrSerial as $part){
		$new_serial .= (strlen($new_serial)>0?".":"") . $part;

		if(!in_array($new_serial, $arrBranchesIncome))
			array_push($arrBranchesIncome, $new_serial);
	}
}

$query_three = $this->ledgers_model->get_account_ledger_by_accounts_opening_periods($arrBranchesIncome,$previous_year_earnings);

$grouped_array_income = array();
foreach ($query_three->result() as $element_three) {
    $grouped_array_income[$element_three->accountId][] = $element_three;
}


// var_dump($arrBranchesIncome);die();

// var_dump($arrBranchesIncome);die();

// $end_month = '12';
// $end_day = '31';

// $explode_min = explode('-', $date_from);
// $loop_min = $explode_min[0];
// $year_explode = $explode_min[0] - 2;
// $month_explode = $explode_min[1];
// $start_date = $year_explode.'-'.$end_month.'-'.$end_day;
// $next_date = $year_explode+1;
// $end_date = $next_date.'-'.$end_month.'-'.$end_day;

// var_dump($end_date);die();
$query_four = $this->ledgers_model->get_transactions_profit_and_loss($arrBranchesIncome,1,$date_from,$date_to);

// $query_four = $this->ledgers_model->get_account_ledger_by_accounts_opening_balance($arrBranchesIncome,$current_year);

// $current_year
foreach ($query_four->result() as $element_four) {
    $grouped_array_years[$element_four->accountId][] = $element_four;
}

$all_transacted_rs = $this->ledgers_model->get_account_ledger_by_accounts(1);


$arr = json_decode(json_encode ( $all_transacted_rs->result() ) , true);

$grouped_array = array();
foreach ($all_transacted_rs->result() as $element) {
	
    $grouped_array[$element->accountId][] = $element;
}


$this->db->where('bs_account_status = 1 AND bs_account_type_id > 0  AND account_staging_delete = 0 AND bs_account_income_status > 0');
$this->db->order_by('bs_account_position','ASC');
$accounts_staging_rs = $this->db->get('account_staging');
// var_dump($accounts_staging_rs->result());die();
$total_income = 0;
$total_debits =0;
$total_credits = 0;
if($accounts_staging_rs->num_rows() > 0)
{
	foreach ($accounts_staging_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$account_staging_name = $value->account_staging_name;
		$bs_account_type_id = $value->bs_account_type_id;
		$has_children = $value->has_children;
		$bs_account_balance_report = $value->bs_account_balance_report;
		$bs_account_income_status = $value->bs_account_income_status;

		if($bs_account_type_id == 3 AND ($has_children == 1 OR $has_children == 0) AND $bs_account_balance_report == 2)
		{

			

			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($staing_account_id);
			

			if($children_account->num_rows() > 0)
			{	
				$fixed_asset_result .='<tr>
											<th class="text-left" colspan="3">'.strtoupper($account_staging_name).' </th>
											
										</tr>';
				foreach ($children_account->result() as $key => $value) {
					# code...
					$account_name = $value->account_name;
					$account_id = $value->account_id;




					// get the account opening balance
				 	$second_array = count($grouped_array_income[$account_id]);
					$opening_balance = 0;
					if($second_array > 0)
					{

						$debit_opening = 0;
						$credit_opening = 0;
						for ($z=0; $z < $second_array; $z++) { 

							$opening_dr_amount = $grouped_array_income[$account_id][$z]->dr_amount;
						    $opening_cr_amount = $grouped_array_income[$account_id][$z]->cr_amount;
						    $opening_balance = $opening_dr_amount - $opening_cr_amount;

						    if($opening_balance > 0)
						    {
						    	$debit_opening = $opening_balance;
								// $total_debits += $debit_opening;
							

								$opening = number_format($debit_opening,2);
						    }
						    else
							{
								$credit_opening = -$opening_balance;
								// $total_credits += $credit_opening;
								$opening = "(".number_format($credit_opening,2).")";

							}


						}
					}

			

					$first_array = count($grouped_array[$account_id]);
					$debit = 0;
					$credit = 0;
					$total_amount = 0;

				
					if($first_array > 0 OR $second_array > 0)
					{

						for ($i=0; $i < $first_array; $i++) { 


					  		$dr_amount = $grouped_array[$account_id][$i]->dr_amount;
						    $cr_amount = $grouped_array[$account_id][$i]->cr_amount;
						    $total_amount += $dr_amount - $cr_amount;

						}
					}
					$total_amount += $opening_balance;
					if($total_amount > 0)
					{
						$debit = $total_amount;
						$total_debits += $debit;
					}
					else
					{
						$credit = -$total_amount;
						$total_credits += $credit;
					}


				}
			}

		}


	}
}
$total_profit_period = $total_credits - $total_debits;
// var_dump($total_profit_period);die();

$explode_current = explode('-',$date_from);

$current_year = $explode_current[0];
$previous_year = $current_year - 1;
// var_dump($previous_year);die();
$total_profit = $this->company_financial_model->get_retained_earning($previous_year,1);





$all_transacted_rs = $this->ledgers_model->get_account_ledger_by_accounts(1);


$arr = json_decode(json_encode ( $all_transacted_rs->result() ) , true);

$grouped_array = array();
foreach ($all_transacted_rs->result() as $element) {
	
    $grouped_array[$element->accountId][] = $element;
}

// var_dump($grouped_array);die();

?>
<?php

// var_dump($grouped_array);die();
$account_report_rs = $this->company_financial_model->get_staging_accounts(1);


$arrOrphans = array();
if($account_report_rs->num_rows() > 0)
{

	foreach ($account_report_rs->result() as $key => $value) {
		// code...
		$account_id = $value->account_id;

		$has_children = $value->has_children;


		if($has_children)
		{
			// check if account has children
			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($account_id);
				

			if($children_account->num_rows() > 0)
			{	

				foreach ($children_account->result() as $key_two => $value_two) {
					// code...
					$child_account_id = $value_two->account_id;
					$arrOrphans[$child_account_id] =  $child_account_id;
				}

				
			}
			else
			{
				$arrOrphans[$value->account_id] =  $value->account_id;
			}
		}
		else
		{
			$arrOrphans[$value->account_id] =  $value->account_id;
		}
		

		
	}

}


$arrBranches = array();
foreach($arrOrphans as $serial){
	$arrSerial = explode(".", $serial);
	$new_serial = "";
	foreach($arrSerial as $part){
		$new_serial .= (strlen($new_serial)>0?".":"") . $part;

		if(!in_array($new_serial, $arrBranches))
			array_push($arrBranches, $new_serial);
	}
}


// var_dump($arrBranches);die();
$query_one = $this->ledgers_model->get_account_ledger_by_accounts_opening_balance($arrBranches);
// var_dump($query_one->result());die();
$grouped_array_old = array();
foreach ($query_one->result() as $element_two) {
    $grouped_array_old[$element_two->accountId][] = $element_two;
}
// var_dump($grouped_array_old);die();


$cash_in_bank = '';
$total_income = 0;

$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($bank_id);
			

if($children_account->num_rows() > 0)
{	

	foreach ($children_account->result() as $key => $value) {
		# code...
		$account_name = $value->account_name;
		$account_id = $value->account_id;


		$second_array = count($grouped_array_old[$account_id]);
		$opening_balance = 0;
		$account_money = 0;
		if($second_array > 0)
		{

			$debit_opening = 0;
			$credit_opening = 0;
			for ($z=0; $z < $second_array; $z++) { 

				$opening_dr_amount = $grouped_array_old[$account_id][$z]->dr_amount;
			    $opening_cr_amount = $grouped_array_old[$account_id][$z]->cr_amount;
			    $opening_balance = $opening_dr_amount - $opening_cr_amount;

			    if($opening_balance > 0)
			    {
			    	$debit_opening = $opening_balance;
					// $total_debits += $debit_opening;
				

					$opening = number_format($debit_opening,2);
			    }
			    else
				{
					$credit_opening = -$opening_balance;
					// $total_credits += $credit_opening;
					$opening = "(".number_format($credit_opening,2).")";

				}


			}
		}
		// if($opening_balance < 0)
		// {
		// 	$opening_balance =0;
		// }
		$account_money += $opening_balance;
		$total_income += $opening_balance;

		$first_array = count($grouped_array[$account_id]);
		$debit = 0;
		$credit = 0;
		if($first_array > 0)
		{

			for ($i=0; $i < $first_array; $i++) { 


		  		$dr_amount = $grouped_array[$account_id][$i]->dr_amount;
			    $cr_amount = $grouped_array[$account_id][$i]->cr_amount;
			    $total_amount = $dr_amount - $cr_amount;

			    // $total_amount += $opening_balance;
				if($total_amount > 0)
				{
					$debit = $total_amount;
					$total_debits += $debit;
				}
				else
				{
					$credit = -$total_amount;
					$total_credits += $credit;
				}
				// $total_amount = $debit - $credit;
				$account_money += $total_amount;
				$total_income += $total_amount;

				


			}

		}
		$cash_in_bank .='<tr>
									<td class="text-left">'.strtoupper($account_name).' </td>
									<td class="text-right">
										<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($account_money,2).'</a>
									</td>
								</tr>';

	}

	// $cash_in_bank .='<tr>
	// 					<td class="text-left"><b>TOTAL BANK BALANCE</b></td>
	// 					<td class="text-right"><b class="match">'.number_format($total_income,2).'</b></td>
	// 					</tr>';
}






// all fixed assets accounts

$account_report_rs = $this->company_financial_model->get_staging_accounts(3);

$arrOrphansFixed = array();
if($account_report_rs->num_rows() > 0)
{

	foreach ($account_report_rs->result() as $key => $value) {
		// code...
		$account_id = $value->account_id;
		$has_children = $value->has_children;


		if($has_children)
		{

			// check if account has children
			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($account_id);
				

			if($children_account->num_rows() > 0)
			{	

				foreach ($children_account->result() as $key_two => $value_two) {
					// code...
					$child_account_id = $value_two->account_id;
					$arrOrphansFixed[$child_account_id] =  $child_account_id;
				}

				
			}
			else
			{
				$arrOrphansFixed[$value->account_id] =  $value->account_id;
			}
		}
		else
		{
			$arrOrphansFixed[$value->account_id] =  $value->account_id;
		}

		
	}

}



$arrBranchesFixed = array();
foreach($arrOrphansFixed as $serialBranches){
	$arrSerial = explode(".", $serialBranches);
	$new_serial = "";
	foreach($arrSerial as $part){
		$new_serial .= (strlen($new_serial)>0?".":"") . $part;

		if(!in_array($new_serial, $arrBranchesFixed))
			array_push($arrBranchesFixed, $new_serial);
	}
}

$query_four = $this->ledgers_model->get_transactions_fixed_assets($arrBranchesFixed,1);

$grouped_array_fixed = array();

foreach ($query_four->result() as $element_five) {
    $grouped_array_fixed[$element_five->patient_id][] = $element_five;
   
}



$query_four_category = $this->ledgers_model->get_transactions_fixed_assets_category($arrBranchesFixed,1);

$grouped_array_fixed_category = array();

foreach ($query_four_category->result() as $element_seven) {
    $grouped_array_fixed_category[$element_seven->accountId][$element_seven->category_id] = $element_seven;
   
}


// var_dump($grouped_array_fixed_category);die();


// end of fixed assets






// var_dump($suppliers_wht_id);die();

$second_array = count($grouped_array_old[$suppliers_wht_id]);
$opening_balance = 0;
if($second_array > 0)
{

	$debit_opening = 0;
	$credit_opening = 0;
	for ($z=0; $z < $second_array; $z++) { 

		$opening_dr_amount = $grouped_array_old[$suppliers_wht_id][$z]->dr_amount;
	    $opening_cr_amount = $grouped_array_old[$suppliers_wht_id][$z]->cr_amount;
	    $opening_balance =  $opening_cr_amount - $opening_dr_amount;

	    if($opening_balance > 0)
	    {
	    	$debit_opening = $opening_balance;
			// $total_debits += $debit_opening;
		

			$opening = number_format($debit_opening,2);
	    }
	    else
		{
			$credit_opening = -$opening_balance;
			// $total_credits += $credit_opening;
			$opening = "(".number_format($credit_opening,2).")";

		}

	}
}

$first_array = count($grouped_array[$suppliers_wht_id]);
$suppliers_wht = $opening_balance;
$debit = 0;
$credit = 0;
if($first_array > 0)
{

	for ($i=0; $i < $first_array; $i++) { 


  		$dr_amount = $grouped_array[$suppliers_wht_id][$i]->dr_amount;
	    $cr_amount = $grouped_array[$suppliers_wht_id][$i]->cr_amount;
	    // $accounts_receivable = $dr_amount - $cr_amount;
		
		$debit = $dr_amount;
		$credit = $cr_amount;
		$suppliers_wht += $credit - $debit;
		// $suppliers_wht -= $opening_balance;
	}
}



// providers wht
// get the account opening balance
$second_array = count($grouped_array_old[$providers_wht_id]);
$opening_balance = 0;
if($second_array > 0)
{

	$debit_opening = 0;
	$credit_opening = 0;
	for ($z=0; $z < $second_array; $z++) { 

		$opening_dr_amount = $grouped_array_old[$providers_wht_id][$z]->dr_amount;
	    $opening_cr_amount = $grouped_array_old[$providers_wht_id][$z]->cr_amount;
	    $opening_balance =  $opening_cr_amount - $opening_dr_amount;

	    if($opening_balance > 0)
	    {
	    	$debit_opening = $opening_balance;
			// $total_debits += $debit_opening;
		

			$opening = number_format($debit_opening,2);
	    }
	    else
		{
			$credit_opening = -$opening_balance;
			// $total_credits += $credit_opening;
			$opening = "(".number_format($credit_opening,2).")";

		}

	}
}

$first_array = count($grouped_array[$providers_wht_id]);
$providers_wht = $opening_balance;
$debit = 0;
$credit = 0;
if($first_array > 0)
{

	for ($i=0; $i < $first_array; $i++) { 


  		$dr_amount = $grouped_array[$providers_wht_id][$i]->dr_amount;
	    $cr_amount = $grouped_array[$providers_wht_id][$i]->cr_amount;
	    // $accounts_receivable = $dr_amount - $cr_amount;
		
		$debit = $dr_amount;
		$credit = $cr_amount;
		$providers_wht += $credit - $debit;

		// $providers_wht -= $opening_balance;
	}
}



// accounts receivables
// get the account opening balance
$second_array = count($grouped_array_old[$accounts_receivable_id]);
$opening_balance = 0;
if($second_array > 0)
{

	$debit_opening = 0;
	$credit_opening = 0;
	for ($z=0; $z < $second_array; $z++) { 

		$opening_dr_amount = $grouped_array_old[$accounts_receivable_id][$z]->dr_amount;
	    $opening_cr_amount = $grouped_array_old[$accounts_receivable_id][$z]->cr_amount;
	    $opening_balance = $opening_dr_amount - $opening_cr_amount;

	    if($opening_balance > 0)
	    {
	    	$debit_opening = $opening_balance;
			// $total_debits += $debit_opening;
		

			$opening = number_format($debit_opening,2);
	    }
	    else
		{
			$credit_opening = -$opening_balance;
			// $total_credits += $credit_opening;
			$opening = "(".number_format($credit_opening,2).")";

		}

		

	}
}
$first_array = count($grouped_array[$accounts_receivable_id]);
$accounts_receivable = 0;
$debit = 0;
$credit = 0;
if($first_array > 0)
{

	for ($i=0; $i < $first_array; $i++) { 


  		$dr_amount = $grouped_array[$accounts_receivable_id][$i]->dr_amount;
	    $cr_amount = $grouped_array[$accounts_receivable_id][$i]->cr_amount;
	    $accounts_receivable = $dr_amount - $cr_amount;
		

		$debit = $dr_amount;
		$credit = $cr_amount;
		$accounts_receivable = $debit - $credit;

		$accounts_receivable += $opening_balance;
	}
	$total_income += $accounts_receivable;
	$cash_in_bank .= '<tr>
						<td class="text-left">ACCOUNTS RECEIVABLES</td>
						<td class="text-right"><a href="'.site_url().'account-transactions/'.$accounts_receivable_id.'" target="_blank">'.number_format($accounts_receivable,2).'</a></td>
					</tr>';
}



// accounts payble

// get the account opening balance
$second_array = count($grouped_array_old[$accounts_payable_id]);
$opening_balance = 0;
if($second_array > 0)
{

	$debit_opening = 0;
	$credit_opening = 0;
	for ($z=0; $z < $second_array; $z++) { 

		$opening_dr_amount = $grouped_array_old[$accounts_payable_id][$z]->dr_amount;
	    $opening_cr_amount = $grouped_array_old[$accounts_payable_id][$z]->cr_amount;
	    $opening_balance = $opening_dr_amount - $opening_cr_amount;

	    if($opening_balance > 0)
	    {
	    	$debit_opening = $opening_balance;
			// $total_debits += $debit_opening;
		

			$opening = number_format($debit_opening,2);
	    }
	    else
		{
			$credit_opening = -$opening_balance;
			// $total_credits += $credit_opening;
			$opening = "(".number_format($credit_opening,2).")";

		}

		

	}
}
$first_array = count($grouped_array[$accounts_payable_id]);
$accounts_payable = 0;
$debit = 0;
$credit = 0;
if($first_array > 0)
{

	for ($i=0; $i < $first_array; $i++) { 


  		$dr_amount = $grouped_array[$accounts_payable_id][$i]->dr_amount;
	    $cr_amount = $grouped_array[$accounts_payable_id][$i]->cr_amount;
	    $accounts_payable = $dr_amount - $cr_amount;
		
		$debit = $dr_amount;
		$credit = $cr_amount;
		$accounts_payable = $credit - $debit;
		$accounts_payable -= $opening_balance;
	}
}


// providers liability
// get the account opening balance
$second_array = count($grouped_array_old[$providers_liability_id]);

$opening_balance = 0;
if($second_array > 0)
{

	$debit_opening = 0;
	$credit_opening = 0;
	for ($z=0; $z < $second_array; $z++) { 

		$opening_dr_amount = $grouped_array_old[$providers_liability_id][$z]->dr_amount;
	    $opening_cr_amount = $grouped_array_old[$providers_liability_id][$z]->cr_amount;
	    $opening_balance = $opening_cr_amount - $opening_dr_amount;

	    if($opening_balance > 0)
	    {
	    	$debit_opening = $opening_balance;
			// $total_debits += $debit_opening;
		

			$opening = number_format($debit_opening,2);
	    }
	    else
		{
			$credit_opening = -$opening_balance;
			// $total_credits += $credit_opening;
			$opening = "(".number_format($credit_opening,2).")";

		}

		

	}
}

$first_array = count($grouped_array[$providers_liability_id]);
$accounts_providers = $opening_balance;
$debit = 0;
$credit = 0;
if($first_array > 0)
{

	for ($i=0; $i < $first_array; $i++) { 


  		$dr_amount = $grouped_array[$providers_liability_id][$i]->dr_amount;
	    $cr_amount = $grouped_array[$providers_liability_id][$i]->cr_amount;
	
		
		$debit = $dr_amount;
		$credit = $cr_amount;
		$accounts_providers += $credit - $debit;
		// $accounts_providers -= $opening_balance;
	}
}

// var_dump($accounts_providers);die();

// payroll liability


$second_array = count($grouped_array_old[$payroll_liability_id]);

// var_dump($payroll_liability_id);die();
$opening_balance = 0;
if($second_array > 0)
{

	$debit_opening = 0;
	$credit_opening = 0;
	for ($z=0; $z < $second_array; $z++) { 

		$opening_dr_amount = $grouped_array_old[$payroll_liability_id][$z]->dr_amount;
	    $opening_cr_amount = $grouped_array_old[$payroll_liability_id][$z]->cr_amount;
	    $opening_balance = $opening_cr_amount - $opening_dr_amount;

	    if($opening_balance > 0)
	    {
	    	$debit_opening = $opening_balance;
			// $total_debits += $debit_opening;
		

			$opening = number_format($debit_opening,2);
	    }
	    else
		{
			$credit_opening = -$opening_balance;
			// $total_credits += $credit_opening;
			$opening = "(".number_format($credit_opening,2).")";

		}

		

	}
}

$first_array = count($grouped_array[$payroll_liability_id]);
$payroll_liability = $opening_balance;
$debit = 0;
$credit = 0;
if($first_array > 0)
{

	for ($i=0; $i < $first_array; $i++) { 


  		$dr_amount = $grouped_array[$payroll_liability_id][$i]->dr_amount;
	    $cr_amount = $grouped_array[$payroll_liability_id][$i]->cr_amount;
	    // $payroll_liability = $dr_amount - $cr_amount;
		
		$debit = $dr_amount;
		$credit = $cr_amount;
		$payroll_liability += $credit - $debit;

		// $payroll_liability -= $opening_balance;
	}
}




$equity_rs = $this->company_financial_model->get_parent_accounts_by_type('Equity');
// var_dump($equity_rs->result());die();

$share_capital_list = '';
$total_share_capital = 0;
if($equity_rs->num_rows() > 0)
{
	foreach ($equity_rs->result() as $key => $value) {
		# code...
		
		$parent_account_name = $value->account_name;
		$parent_account_id = $value->account_id;
	
		$child_accounts_rs = $this->company_financial_model->get_child_accounts_by_parent_id($parent_account_id);

		if($child_accounts_rs->num_rows() > 0)
		{
			$item = '<i class="fa fa-angle-right"></i> ';
		}
		else
		{
			$item = '';
		}
		$total_equity_category = 0;
		$account_amount_quity = 0;
	
		if($child_accounts_rs->num_rows() > 0)
		{


			$equity_asset_child = '<tbody class="collapse" id="'.$parent_account_id.'">';


			foreach ($child_accounts_rs->result() as $key => $value2) {
				# code...
				$child_account_name = $value2->account_name;
				$child_account_id = $value2->account_id;

				$second_array = count($grouped_array_income[$child_account_id]);
				$opening_balance = 0;
				$account_amount_quity = 0;
				if($second_array > 0)
				{

					$debit_opening = 0;
					$credit_opening = 0;
					for ($z=0; $z < $second_array; $z++) { 

						$opening_dr_amount = $grouped_array_income[$child_account_id][$z]->dr_amount;
					    $opening_cr_amount = $grouped_array_income[$child_account_id][$z]->cr_amount;
					    $opening_balance = $opening_dr_amount - $opening_cr_amount;

					    if($opening_balance > 0)
					    {
					    	$debit_opening = $opening_balance;
							// $total_debits += $debit_opening;
							$opening = number_format($debit_opening,2);
					    }
					    else
						{
							$credit_opening = -$opening_balance;
							// $total_credits += $credit_opening;
							$opening = "(".number_format($credit_opening,2).")";

						}


					}
				}

				if($opening_balance < 0)
				{
					$opening_balance = -$opening_balance;
				}
				$total_share_capital += $opening_balance;
				$account_amount_quity += $opening_balance;
				$total_equity_category += $opening_balance;
				// var_dump($opening_balance);die();

				$first_array = count($grouped_array[$child_account_id]);
				// var_dump($first_array);die();
				$debit = 0;
				$credit = 0;
				if($first_array > 0)
				{

					for ($i=0; $i < $first_array; $i++) { 


				  		$dr_amount = $grouped_array[$child_account_id][$i]->dr_amount;
					    $cr_amount = $grouped_array[$child_account_id][$i]->cr_amount;
					    $total_amount = $dr_amount - $cr_amount;

					 
						if($total_amount > 0)
						{
							$debit = $total_amount;
							$total_debits += $debit;
						}
						else
						{
							$credit = -$total_amount;
							$total_credits += $credit;

						}

						$account_amount_quity += -$total_amount;
						// $account_amount_quity += $opening_balance;
						$total_share_capital += -$total_amount;
						$total_equity_category += -$total_amount;
						
					}
				}
				$equity_asset_child .='<tr>
											<td class="text-left">'.strtoupper($child_account_name).' </td>
											<td class="text-right" colspan="" ><a href="'.site_url().'account-transactions/'.$child_account_id.'">'.number_format($account_amount_quity,2).'</a></td>
										</tr>';

			




			}
			$equity_asset_child .= '</tbody>';
		}


		// parent accounts 
		$share_capital_list .='<tr data-toggle="collapse" data-target="#'.$parent_account_id.'">
									<td class="text-left" colspan="" > '.$item.strtoupper($parent_account_name).' </td>
									<td class="text-right" colspan="" >'.number_format($total_equity_category,2).'</td>
								</tr>';

		$share_capital_list .= $equity_asset_child;
		// get all child accounts


		
	
	}



}


$assets_rs = $this->company_financial_model->get_parent_accounts_by_type('Fixed Assets');


$fixed_asset_result = '';
$total_fixed = 0;
if($assets_rs->num_rows() > 0)
{
	foreach ($assets_rs->result() as $key => $value) {
		# code...
		
		$parent_account_name = $value->account_name;
		$parent_account_id = $value->account_id;

		$child_accounts_rs = $this->company_financial_model->get_assets_child_accounts_by_parent_id($parent_account_id);

		if($child_accounts_rs->num_rows() > 0)
		{
			$item = '<i class="fa fa-angle-right"></i> ';
		}
		else
		{
			$item = '';
		}
		$total_fixed_category = 0;
		
		if($child_accounts_rs->num_rows() > 0)
		{
			$total_count = $child_accounts_rs->num_rows();
			$counter = 0;
			$fixed_asset_child = '<tbody class="collapse" id="'.$parent_account_id.'">';

				
			foreach ($child_accounts_rs->result() as $key => $value2) {
				# code...
				$child_account_name = $value2->account_name;
				$child_account_id = $value2->account_id;
				$asset_name = $value2->asset_name;
				$asset_value = $value2->asset_value;
				$asset_id = $value2->asset_id;
				$asset_category_id = $value2->asset_category_id;
				$asset_category_name = $value2->asset_category_name;
				
				if($counter == 0)
				{
					$fixed_asset_child .='<tr>
											<td class="text-left" style="padding-left:5px !important; ">'.strtoupper($asset_category_name).' </td>
											<td class="text-right" colspan="" ></td>
										</tr>';
				}
				$child_account_name .= ' : '.$asset_name;

				// var_dump($grouped_array);die();
				// var_dump($grouped_array[$child_account_id]);die();
				$second_array = count($grouped_array_old[$child_account_id]);
				$opening_balance = 0;
				if($second_array > 0)
				{

					$debit_opening = 0;
					$credit_opening = 0;
					for ($z=0; $z < $second_array; $z++) { 

						$opening_dr_amount = $grouped_array_old[$child_account_id][$z]->dr_amount;
					    $opening_cr_amount = $grouped_array_old[$child_account_id][$z]->cr_amount;
					    $opening_balance = $opening_dr_amount - $opening_cr_amount;

					    if($opening_balance > 0)
					    {
					    	$debit_opening = $opening_balance;
							// $total_debits += $debit_opening;
						

							$opening = number_format($debit_opening,2);
					    }
					    else
						{
							$credit_opening = -$opening_balance;
							// $total_credits += $credit_opening;
							$opening = "(".number_format($credit_opening,2).")";

						}


					}
				}
				$account_amount += $opening_balance;

				$first_array = count($grouped_array_fixed[$asset_id]);
		
				$debit = 0;
				$credit = 0;
				if($first_array > 0)
				{

					for ($i=0; $i < $first_array; $i++) { 


				  		$dr_amount = $grouped_array_fixed[$asset_id][$i]->dr_amount;
					    $cr_amount = $grouped_array_fixed[$asset_id][$i]->cr_amount;
					   


						$total_amount = $dr_amount - $cr_amount;
				

						if($total_amount > 0)
						{
							$debit = $total_amount;
							$total_debits += $debit;
						}
						else
						{
							$credit = -$total_amount;
							$total_credits += $credit;
						}
						$account_amount = $debit-$credit;

						$total_fixed_category += $account_amount;
						
					}
				}
				$counter++;


				$fixed_asset_child .='<tr>
											<td class="text-left" style="padding-left:10px !important;">'.strtoupper($child_account_name).' </td>
											<td class="text-right" colspan="" ><a href="'.site_url().'account-transactions/'.$child_account_id.'">'.number_format($account_amount,2).'</a></td>
										</tr>';

				if($counter == $total_count)
				{
					// var_dump($child_account_id);die();
					// var_dump($grouped_array_fixed_category[$acc_depreciation_id][$asset_category_id]);die();
						$second_array = count($grouped_array_fixed_category[$asset_depreciation_id][$asset_category_id]);
		
						$debit = 0;
						$credit = 0;
						if($second_array > 0)
						{

							// for ($i=0; $i < $second_array; $i++) { 

								$dr_amount = 0;
							    $cr_amount = $grouped_array_fixed_category[$asset_depreciation_id][$asset_category_id]->cr_amount;
							   
							    // var_dump($cr_amount);die();

								$total_amount = $dr_amount - $cr_amount;
								
								

								if($total_amount > 0)
								{
									$debit = $total_amount;
									$total_debits += $debit;
								}
								else
								{
									$credit = -$total_amount;
									$total_credits += $credit;
								}
								$account_amount = $debit-$credit;

								$total_fixed_category += $account_amount;
								
							// }
						}

					$fixed_asset_child .='<tr>
											<td class="text-left" style="padding-left:10px !important;">'.strtoupper($asset_category_name).' : DEPRECIATION </td>
											<td class="text-right" colspan="" ><a href="'.site_url().'account-transactions/'.$child_account_id.'">('.number_format($credit,2).')</a></td>
										</tr>';
				}



			}
			$fixed_asset_child .= '</tbody>';
		}



		// parent accounts 
		$fixed_asset_result .='<tr data-toggle="collapse" data-target="#'.$parent_account_id.'">
									<td class="text-left" colspan="" > '.$item.strtoupper($parent_account_name).'</td>
									<td class="text-right" colspan="" >'.number_format($total_fixed_category,2).'</td>
								</tr>';

		$fixed_asset_result .= $fixed_asset_child;
		// get all child accounts

		$total_fixed += $total_fixed_category;
		
	
	}



}

$loans_liability = '';
$total_loans_liability = 0;

$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($loans_account_id);
			

if($children_account->num_rows() > 0)
{	

	foreach ($children_account->result() as $key => $value) {
		# code...
		$account_name = $value->account_name;
		$account_id = $value->account_id;


		$second_array = count($grouped_array_old[$account_id]);
		$opening_balance = 0;
		if($second_array > 0)
		{

			$debit_opening = 0;
			$credit_opening = 0;
			for ($z=0; $z < $second_array; $z++) { 

				$opening_dr_amount = $grouped_array_old[$account_id][$z]->dr_amount;
			    $opening_cr_amount = $grouped_array_old[$account_id][$z]->cr_amount;
			    $opening_balance = $opening_dr_amount - $opening_cr_amount;

			    if($opening_balance > 0)
			    {
			    	$debit_opening = $opening_balance;
					// $total_debits += $debit_opening;
				

					$opening = number_format($debit_opening,2);
			    }
			    else
				{
					$credit_opening = -$opening_balance;
					// $total_credits += $credit_opening;
					$opening = "(".number_format($credit_opening,2).")";

				}


			}
		}

		$first_array = count($grouped_array[$account_id]);
		$debit = 0;
		$credit = 0;
		if($first_array > 0)
		{

			for ($i=0; $i < $first_array; $i++) { 


		  		$dr_amount = $grouped_array[$account_id][$i]->dr_amount;
			    $cr_amount = $grouped_array[$account_id][$i]->cr_amount;
			    $total_amount = $dr_amount - $cr_amount;

		

				$total_amount += $opening_balance;
				if($total_amount > 0)
				{
					$debit = $total_amount;
					// $total_debits += $debit;
					$account_amount = -$debit;
				}
				else
				{
					$credit = -$total_amount;
					// var_dump($credit);die();
					// $total_credits += $credit;
					$account_amount = $credit;
				}
				// $account_amount = $debit-$credit;

				$total_loans_liability += $account_amount;

				$loans_liability .='<tr>
									<td class="text-left">'.strtoupper($account_name).' </td>
									<td class="text-right">
										<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($account_amount,2).'</a>
									</td>
								</tr>';


			}

		}

	}


}

$total_assets = $accounts_receivable+$total_income;

$total_liability = $accounts_payable + $suppliers_wht +$providers_wht + $accounts_providers + $payroll_liability ;
$current_year_earnings = $total_assets + $total_fixed - $total_liability;

$search = $this->session->userdata('balance_sheet_title_search');

if(!empty($search))
{
	$balance_sheet_search = ucfirst($search);
}
else {

	
	$balance_sheet_search = 'Reporting as for: '.date("Y-01-01").' to '.date("Y-m-d");
}

$total_current_assets = $total_fixed+$total_income+$accounts_receivable;
?>

<?php
	$date_from = $this->session->userdata('date_from_general_ledger');
	$general_ledger_search_title =	$this->session->userdata('general_ledger_search_title');


	if(!empty($general_ledger_search_title))
	{
		$search_title = $general_ledger_search_title;
	}
	else 
	{
		$search_title = 'ALL TIME REPORT';
	}

?>


<style>
	td .match
	{
		border-top: #000 2px solid !important;
	}
</style>

<div class="col-md-4">

	<?php echo $this->load->view('search/search_balance_sheet','', true);?>


	<div class="text-center">
		<h4 class="box-title">Balance Sheet</h4>
		<h5 class="box-title"> <?php echo $search_title?> </h5>
		<h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
	</div>
	<?php
	$search_title = 'BALANCE SHEET '.$search_title;
	?>
		<div class="col-md-12">
			<div class="col-md-12" style="margin-bottom:20px">
			<div class="col-md-6">
				<div class="form-group">
					  <a onclick="javascript:xport.toCSV('<?php echo $search_title?>');" target="_blank" class="btn btn-md btn-success col-md-12"><i class="fa fa-print"></i> EXPORT BALANCE SHEET</a>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<a href="<?php echo site_url().'print-balance-sheet'?>" target="_blank" class="btn btn-md btn-warning col-md-12"><i class="fa fa-print"></i> Print Balance Sheet</a>
				</div>
			</div>
			
		</div>
		<br>
		<div class="col-md-12">
			
		    <div class="form-group">
			      <a href="<?php echo site_url().'company-financials/profit-and-loss'?>"  class="btn btn-md btn-info col-md-12"><i class="fa fa-file"></i> Income Statement</a>
			</div>
			<div class="form-group">
			      <a href="<?php echo site_url().'company-financials/trial-balance'?>"  class="btn btn-md btn-primary col-md-12"><i class="fa fa-file"></i> Trial Balance</a>
			</div>
			<div class="form-group">
			      <a href="<?php echo site_url().'company-financials/general-ledger'?>"  class="btn btn-md btn-success col-md-12"><i class="fa fa-file"></i> General Ledger</a>
			</div>
  		</div>
	</div>

   

	
</div>
<div class="col-md-8">
	

	<section class="panel">
		<div class="panel-body" style="height:85vh;overflow-y:scroll;padding:0px;">
    	<!-- <h4 class="box-title">Asssets</h4> -->
    	<!-- <h4 class="box-title">Fixed Assets</h4> -->
    	<table class="table  table-striped table-condensed table-bordered" id="<?php echo $search_title?>">
    		<thead>

				<tr>
					<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">ASSETS</th>
				</tr>
				
				<tr>
					<th class="text-left" colspan="2" >CURRENT ASSETS</th>
				</tr>
				
			</thead>

			<tbody>
				<?php echo $cash_in_bank?>
				<tr>
					<td  style="padding-left:20px !important; " >TOTAL CURRENT ASSETS</td>
					<th class="text-right" style="border-top: 2px solid #000;"><?php echo number_format($total_income,2)?></th>
				</tr>
			
				<tr>
					<th class="text-left" colspan="2" >LONG TERM ASSETS</th>
				</tr>
				<?php
					echo $fixed_asset_result;

				?>
				<tr>
					<td style="padding-left:20px !important; ">TOTAL FIXED ASSETS</td>
					<th class="text-right" style="border-top: 2px solid #000;"><?php echo number_format($total_fixed,2)?></th>
				</tr>


				<tr>
					<th style="padding-left:10px !important; ">TOTAL ASSETS</th>
					<th class="text-right" style="border-top: 2px solid #000;border-bottom: 2px solid #000;"><?php echo number_format($total_fixed + $total_income,2)?></th>
				</tr>

				<tr>
					<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">LIABILITIES</th>
				</tr>
				
				<tr>
					<th class="text-left" colspan="2" >CURRENT LIABILITIES : </th>
				</tr>

				<tr>
					<td class="text-left">ACCOUNTS PAYABLES</td>
					<td class="text-right"><a href="<?php echo site_url().'account-transactions/'.$accounts_payable_id?>" target="_blank" ><?php echo number_format($accounts_payable,2)?></a> </td>
				</tr>
				<tr>
					<td class="text-left">PROVIDERS</td>
					<td class="text-right"><a href="<?php echo site_url().'account-transactions/'.$providers_liability_id?>" target="_blank"><?php echo number_format($accounts_providers,2)?></a> </td>
				</tr>
				<tr>
					<td class="text-left">PAYROLL LIABILITY</td>
					<td class="text-right"><a href="<?php echo site_url().'account-transactions/'.$payroll_liability_id?>" target="_blank" ><?php echo number_format($payroll_liability,2)?></a> </td>
				</tr>
				<tr>
					<td class="text-left">PROVIDERS TAX</td>
					<td class="text-right"><a href="<?php echo site_url().'account-transactions/'.$providers_wht_id?>" target="_blank" ><?php echo number_format($providers_wht,2)?></a> </td>
				</tr>
				<tr>
					<td class="text-left">SUPPLIERS TAX</td>
					<td class="text-right"><a href="<?php echo site_url().'account-transactions/'.$suppliers_wht_id?>" target="_blank" ><?php echo number_format($suppliers_wht,2)?></a> </td>
				</tr>
				<tr>
					 <td style="padding-left:20px !important; ">TOTAL CURRENT LIABILITIES</td>
					 <td class="text-right"><b class="match"><?php echo number_format($total_liability,2);?></b></td>
				</tr>
				<tr>
					<th class="text-left" colspan="2" >LONG TERM LIABILITIES : </th>
				</tr>
				<?php echo $loans_liability?>
				<tr>
					<td style="padding-left:20px !important; ">TOTAL LONG TERM LIABILITIES</td>
					<th class="text-right"><b class="match"><?php echo number_format($total_loans_liability,2);?></b></th>
				</tr>
				<tr>
					<th style="padding-left:10px !important; ">TOTAL LIABILITIES</th>
					<th class="text-right" style="border-top: 2px solid #000;border-bottom: 2px solid #000;"><?php echo number_format($total_liability + $total_loans_liability,2)?></th>
				</tr>

				<tr>
					<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">OWNERS EQUITY</th>
				</tr>
				<?php echo $share_capital_list;?>
				<tr>
        			<td class="text-left" colspan="2">RETAINED EARNINGS</td>
					
				</tr>

				<tr>
        			<td style="padding-left:10px !important; ">PROFIT FOR ALL PRIOR YEARS</td>
					<td class="text-right"><a href="<?php echo site_url().'company-financials/profit-and-loss'?>" ><?php echo number_format($total_profit,2)?></a></td>
				</tr>

				<tr>
        			<td style="padding-left:10px !important; ">PROFIT BETWEEN <?php echo $date_from?> TO <?php echo $date_to?></td>
					<td class="text-right"><a href="<?php echo site_url().'company-financials/profit-and-loss'?>" ><?php echo number_format($total_profit_period,2)?></a></td>
				</tr>
				<tr>
					<th style="padding-left:20px !important; ">TOTAL OWNERS EQUITY</th>
					<th class="text-right" style="border-top: 2px solid #000;border-bottom: 2px solid #000;"><?php echo number_format($total_share_capital+$total_profit + $total_profit_period,2)?></th>
				</tr>
				<tr>
					<th >TOTAL LIABILITIES & OWNERS EQUITY</th>
					<th class="text-right" style="border-top: 2px solid #000;border-bottom: 2px solid #000;"><?php echo number_format($total_liability + $total_loans_liability+$total_share_capital+$total_profit+$total_profit_period,2)?></th>
				</tr>

			</tbody>

			
				
		
		</table>
		
    </div>

	</section>
</div>


<script type="text/javascript">
		var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};


</script>

