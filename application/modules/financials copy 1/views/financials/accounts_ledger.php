
<?php
$date_from = $this->session->userdata('date_from_general_ledger');
$explode = explode('-', $date_from);
$previous_year_earnings = $explode[0] -1;
$projected_account_id = $account_id;
	$date_from = $this->session->userdata('date_from_general_ledger');
	$date_to = $this->session->userdata('date_to_general_ledger');
	$add = '';
	if(!empty($date_from) OR !empty($date_to))
	{

		$date_from = $date_from;
		$date_to = $date_to;

	}

	else
	{

		$date_from = date('Y-m-01');
		$date_to = date('Y-m-d');
		

	}

	$search_title = $this->session->userdata('general_ledger_search_title');

?>

<?php
$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

$patients = array();

if($accounts_config_rs->num_rows() > 0)
{
	foreach ($accounts_config_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$reference_name = $value->reference_name;

		$session_account[$reference_name] = $staing_account_id;

		
	}
}

$providers_liability_id = $session_account['providers_liability_id'];
$providers_wht_id = $session_account['providers_wht_id'];
$payroll_liability_id = $session_account['payroll_liability_id'];
$accounts_payable_id = $session_account['accounts_payable_id'];
$fixed_assets_id = $session_account['fixed_assets_id'];
$accounts_receivable_id = $session_account['accounts_receivable_id'];
$suppliers_wht_id = $session_account['suppliers_wht_id'];
$income_account_id = $session_account['income_account_id'];



$all_transacted_rs = $this->ledgers_model->get_account_ledger_by_accounts();


$arr = json_decode(json_encode ( $all_transacted_rs->result() ) , true);

$grouped_array = array();
foreach ($all_transacted_rs->result() as $element) {
	
    $grouped_array[$element->accountId][] = $element;
}

$account_report_rs = $this->company_financial_model->get_staging_accounts(1);


$arrOrphans = array();
if($account_report_rs->num_rows() > 0)
{

	foreach ($account_report_rs->result() as $key => $value) {
		// code...
		$account_id = $value->account_id;

		$has_children = $value->has_children;


		if($has_children)
		{
			// check if account has children
			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($account_id);
				

			if($children_account->num_rows() > 0)
			{	

				foreach ($children_account->result() as $key_two => $value_two) {
					// code...
					$child_account_id = $value_two->account_id;
					$arrOrphans[$child_account_id] =  $child_account_id;
				}

				
			}
			else
			{
				$arrOrphans[$value->account_id] =  $value->account_id;
			}
		}
		else
		{
			$arrOrphans[$value->account_id] =  $value->account_id;
		}
		

		
	}

}


$arrBranches = array();
foreach($arrOrphans as $serial){
	$arrSerial = explode(".", $serial);
	$new_serial = "";
	foreach($arrSerial as $part){
		$new_serial .= (strlen($new_serial)>0?".":"") . $part;

		if(!in_array($new_serial, $arrBranches))
			array_push($arrBranches, $new_serial);
	}
}

$query_one = $this->ledgers_model->get_account_ledger_by_accounts_opening_balance($arrBranches);

$grouped_array_old = array();
foreach ($query_one->result() as $element_two) {
    $grouped_array_old[$element_two->accountId][] = $element_two;
}







// all income accounts

$account_report_rs = $this->company_financial_model->get_staging_accounts(2);

$arrOrphansIncome = array();
if($account_report_rs->num_rows() > 0)
{

	foreach ($account_report_rs->result() as $key => $value) {
		// code...
		$account_id = $value->account_id;
		$has_children = $value->has_children;


		if($has_children)
		{

			// check if account has children
			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($account_id);
				

			if($children_account->num_rows() > 0)
			{	

				foreach ($children_account->result() as $key_two => $value_two) {
					// code...
					$child_account_id = $value_two->account_id;
					$arrOrphansIncome[$child_account_id] =  $child_account_id;
				}

				
			}
			else
			{
				$arrOrphansIncome[$value->account_id] =  $value->account_id;
			}
		}
		else
		{
			$arrOrphansIncome[$value->account_id] =  $value->account_id;
		}

		
	}

}

$arrBranchesIncome = array();
foreach($arrOrphansIncome as $serialIncome){
	$arrSerial = explode(".", $serialIncome);
	$new_serial = "";
	foreach($arrSerial as $part){
		$new_serial .= (strlen($new_serial)>0?".":"") . $part;

		if(!in_array($new_serial, $arrBranchesIncome))
			array_push($arrBranchesIncome, $new_serial);
	}
}

// var_dump($arrBranchesIncome);die();
// $query_three = $this->ledgers_model->get_account_ledger_by_accounts_opening_periods($arrBranchesIncome,$previous_year_earnings);

// $grouped_array_income = array();
// foreach ($query_three->result() as $element_three) {
//     $grouped_array_income[$element_three->accountId][] = $element_three;
// }



$query_three = $this->ledgers_model->get_account_ledger_by_accounts_opening_periods($arrBranchesIncome,$previous_year_earnings);

$grouped_array_income = array();
foreach ($query_three->result() as $element_three) {
    $grouped_array_income[$element_three->accountId][] = $element_three;
}


$max_rs = $this->ledgers_model->get_transactions_profit_and_loss($arrBranchesIncome);


$end_month = $this->session->userdata('end_of_year_month');
$end_day = $this->session->userdata('end_of_year_day');
$grouped_array_years = array();

if($max_rs->num_rows() > 0)
{
	foreach ($max_rs->result() as $key => $value_four) {
		// code...
		$max_transaction_date = $value_four->max_transaction_date;
		$min_transaction_date = $value_four->min_transaction_date;
	}
	
	if(!empty($min_transaction_date))
	{
		// explode(delimiter, string)
	
		$explode_min = explode('-', $min_transaction_date);
		$loop_min = $explode_min[0];
		$year_explode = $explode_min[0] - 1;
		$month_explode = $explode_min[1];

		$loop_years = date('Y') - $loop_min;

			// var_dump($year_explode);die();

		for ($i=0; $i < $loop_years; $i++) { 
			// code...
			$year_start = $year_explode + $i;
			// $start_year = $i-1;
			$start_date = $year_start.'-'.$end_month.'-'.$end_day;
			$next_date = $year_start+1;
			$end_date = $next_date.'-'.$end_month.'-'.$end_day;


			// var_dump($start_date);die();
			$query_four = $this->ledgers_model->get_transactions_profit_and_loss($arrBranchesIncome,1,$start_date,$end_date);


			foreach ($query_four->result() as $element_four) {
			    $grouped_array_years[$element_four->accountId][] = $element_four;
			}

		}


	}

	
}


// get parent account or account linked

$account_staged = $this->ledgers_model->get_account_staging_id($projected_account_id);

// var_dump($account_staged);die();


$this->db->where('bs_account_status = 1 AND bs_account_type_id > 0  AND account_staging_delete = 0 AND account_id = '.$account_staged);
$this->db->order_by('bs_account_position','ASC');
$accounts_staging_rs = $this->db->get('account_staging');
// var_dump($accounts_staging_rs->result());die();
$running_balance = 0;
if($accounts_staging_rs->num_rows() > 0)
{
	foreach ($accounts_staging_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$account_staging_name = $value->account_staging_name;
		$bs_account_type_id = $value->bs_account_type_id;
		$has_children = $value->has_children;
		$bs_account_balance_report = $value->bs_account_balance_report;
		$sub_total = 0;
	
		if($bs_account_type_id == 1 AND $has_children == 0 AND $bs_account_balance_report == 0)
		{
			$debit = 0;
			$credit = 0;
			$first_array = count($grouped_array[$projected_account_id]);

			if($first_array > 0)
			{

				for ($i=0; $i < $first_array; $i++) { 


			  		$dr_amount = $grouped_array[$projected_account_id][$i]->dr_amount;
				    $cr_amount = $grouped_array[$projected_account_id][$i]->cr_amount;
				    $accounts_receivable = $dr_amount - $cr_amount;

				    // $credit =0;
					if($accounts_receivable > 0)
					{
						$debit = $accounts_receivable;
						$total_debits += $debit;
					}
					else
					{
						$credit = -$accounts_receivable;
						$total_credits += $credit;
					}


					
					$fixed_asset_result .='<tr>
												<td class="text-left"><strong>'.$account_staging_name.'</strong></td>
												<td class="text-center">
													<a href="'.site_url().'account-transactions/'.$projected_account_id.'" > '.number_format($debit,2).' </a>
												</td>
												<td class="text-center">
													<a href="'.site_url().'account-transactions/'.$projected_account_id.'" >'.number_format($credit,2).'</a>
												</td>
											</tr>';
				}

				
			}

			// unset($grouped_array);

		}



		else if($bs_account_type_id == 1 AND $has_children == 0 AND $bs_account_balance_report == 1)
		{
			$sub_total = 0;

			// get the account opening balance
		 	$second_array = count($grouped_array_old[$projected_account_id]);
			$opening_balance = 0;
			$account_value = 0;
			if($second_array > 0)
			{

				$debit_opening = 0;
				$credit_opening = 0;
				for ($z=0; $z < $second_array; $z++) { 

					$opening_dr_amount = $grouped_array_old[$projected_account_id][$z]->dr_amount;
				    $opening_cr_amount = $grouped_array_old[$projected_account_id][$z]->cr_amount;
				    $opening_balance = $opening_dr_amount - $opening_cr_amount;

				    if($opening_balance > 0)
				    {
				    	$debit_opening = $opening_balance;
						// $total_debits += $debit_opening;
					

						$opening = number_format($debit_opening,2);
				    }
				    else
					{
						$credit_opening = -$opening_balance;

						// $total_credits += $credit_opening;
						$opening = "(".number_format($credit_opening,2).")";

					}
					// $running_balance += $opening_balance;
					
					

				}
			}


			$fixed_asset_result .='<tr class="primary">
																<td><strong>'.$account_staging_name.' </strong></td>
																<td class="text-center" colspan="4"></td>
																<td class="center-align">'.number_format($opening_balance,2).'</td>
															</tr>';
			$debit = 0;
			$credit = 0;
			$first_array = count($grouped_array[$projected_account_id]);
			 $sub_total += $opening_balance;
			 $running_balance += $opening_balance;
			if($first_array > 0)
			{

				for ($i=0; $i < $first_array; $i++) { 


			  		$dr_amount = $grouped_array[$projected_account_id][$i]->dr_amount;
				    $cr_amount = -$grouped_array[$projected_account_id][$i]->cr_amount;
				    $accounts_receivable = $dr_amount - $cr_amount;
				    $accounts_receivable += $opening_balance;
				    // $credit =0;
						if($accounts_receivable > 0)
						{
							$debit = $accounts_receivable;
							$total_debits += $debit;
						}
						else
						{
							$credit = -$accounts_receivable;
							$total_credits += $credit;
						}
					  $transactionDescription = $grouped_array[$projected_account_id][$i]->transactionDescription;
				    $referenceCode = $grouped_array[$projected_account_id][$i]->referenceCode;
				    $transactionDate = $grouped_array[$projected_account_id][$i]->transactionDate;
				    $accountName = $grouped_array[$projected_account_id][$i]->accountName;
				    $transactionCategory = $grouped_array[$projected_account_id][$i]->transactionCategory;


				   
				    	
				    $account_value = $dr_amount+$cr_amount;
				   	
				   

				    $running_balance += $account_value;
				    $sub_total += $account_value;

					$fixed_asset_result .= '<tr>
							        		  <td > '.$transactionCategory.' s</td>
							        			<td > '.$transactionDate.'</td>
														<td > '.$referenceCode.'</td>
														<td > '.$transactionDescription.'</td>
														<td class="center-align">'.number_format($account_value,2).'</td>
									        	<td class="center-align">'.number_format($running_balance,2).'</td>
												</tr>';


				}

				
			}
			$fixed_asset_result .= '<tr class="success">
										        		  <td > <strong>SUB TOTAL '.$account_staging_name.'</strong></td>
										        			<td > </td>
																	<td > </td>
																	<td > </td>
																	<td class="center-align"></td>
												        	<td class="center-align">'.number_format($sub_total,2).'</td>
															</tr>';

		}


		else if($bs_account_type_id == 2 AND $has_children == 1 AND $bs_account_balance_report == 0)
		{
			
			// var_dump($projected_account_id);die();

			

					$sub_total = 0;
					$first_array = count($grouped_array[$projected_account_id]);
					$debit = 0;
					$credit = 0;
					if($first_array > 0)
					{

						for ($i=0; $i < $first_array; $i++) { 


					  		$dr_amount = $grouped_array[$projected_account_id][$i]->dr_amount;
						    $cr_amount = $grouped_array[$projected_account_id][$i]->cr_amount;
						    $total_amount = $dr_amount - $cr_amount;


							if($total_amount > 0)
							{
								$debit = $total_amount;
								$total_debits += $debit;
							}
							else
							{
								$credit = -$total_amount;
								$total_credits += $credit;
							}







							$fixed_asset_result .='<tr>
																					<td class="text-left">'.strtoupper($account_name).' </td>
																					<td class="text-center">
																						<a href="'.site_url().'account-transactions/'.$projected_account_id.'">'.number_format($debit,2).'</a>
																					</td>
																					<td class="text-center">
																						<a href="'.site_url().'account-transactions/'.$projected_account_id.'">'.number_format($credit,2).'</a>
																					</td>
																				</tr>';


						}

					}

				
		}



		else if($bs_account_type_id == 3 AND $has_children == 1 AND $bs_account_balance_report == 1)
		{

			

					$sub_total = 0;
					// get the account opening balance
				 	$second_array = count($grouped_array_old[$projected_account_id]);
					$opening_balance = 0;
					if($second_array > 0)
					{

						$debit_opening = 0;
						$credit_opening = 0;
						for ($z=0; $z < $second_array; $z++) { 

							$opening_dr_amount = $grouped_array_old[$projected_account_id][$z]->dr_amount;
						    $opening_cr_amount = $grouped_array_old[$projected_account_id][$z]->cr_amount;
						    $opening_balance = $opening_dr_amount - $opening_cr_amount;

						    if($opening_balance > 0)
						    {
						    	$debit_opening = $opening_balance;
								// $total_debits += $debit_opening;
							

								$opening = number_format($debit_opening,2);
						    }
						    else
							{
								$credit_opening = -$opening_balance;
								// $total_credits += $credit_opening;
								$opening = "(".number_format($credit_opening,2).")";

							}


						}
					}
					$fixed_asset_result .='<tr class="primary">
												<td><strong>'.$account_name.'</strong></td>
												<td class="text-center" colspan="4"></td>
												<td class="center-align">'.number_format($opening_balance,2).'</td>
											</tr>';

					$sub_total += $opening_balance;
					$first_array = count($grouped_array[$projected_account_id]);
					$debit = 0;
					$credit = 0;
					$total_amount = 0;
					if($first_array > 0 OR $second_array > 0)
					{

						for ($i=0; $i < $first_array; $i++) { 


					  		$dr_amount = $grouped_array[$projected_account_id][$i]->dr_amount;
						    $cr_amount = -$grouped_array[$projected_account_id][$i]->cr_amount;
						    $total_amount = $dr_amount - $cr_amount;

						    // $credit =0;
								if($accounts_receivable > 0)
								{
									$debit = $accounts_receivable;
									$total_debits += $debit;
								}
								else
								{
									$credit = -$accounts_receivable;
									$total_credits += $credit;
								}
							  $transactionDescription = $grouped_array[$projected_account_id][$i]->transactionDescription;
						    $referenceCode = $grouped_array[$projected_account_id][$i]->referenceCode;
						    $transactionDate = $grouped_array[$projected_account_id][$i]->transactionDate;
						    $accountName = $grouped_array[$projected_account_id][$i]->accountName;
						    $transactionCategory = $grouped_array[$projected_account_id][$i]->transactionCategory;


						    
						    	
						    $account_value = $dr_amount+$cr_amount;
						    

						    $running_balance += $account_value;
						    $sub_total += $account_value;
								

								$fixed_asset_result .= '<tr>
										        		  <td > '.$transactionCategory.'</td>
										        			<td > '.$transactionDate.'</td>
																	<td > '.$referenceCode.'</td>
																	<td > '.$transactionDescription.'</td>
																	<td class="center-align">'.number_format($account_value,2).'</td>
												        	<td class="center-align">'.number_format($sub_total,2).'</td>
														</tr>';


						}

					


					}

						$fixed_asset_result .= '<tr class="success">
													        		  <td > <strong>SUB TOTAL '.$account_name.'</strong></td>
													        			<td > </td>
																				<td > </td>
																				<td > </td>
																				<td class="center-align"></td>
															        	<td class="center-align">'.number_format($sub_total,2).'</td>
																		</tr>';


				
			

		}



		// all p and l accounts

		
		else if($bs_account_type_id == 3 AND $has_children == 1 AND $bs_account_balance_report == 2)
		{

			
			
				$third_array = count($grouped_array_years[$projected_account_id]);
				
				if($third_array > 0)
				{

				
					for ($k=0; $k < $third_array; $k++) { 

						$opening_dr_amount = $grouped_array_years[$projected_account_id][$k]->dr_amount;
					    $opening_cr_amount = $grouped_array_years[$projected_account_id][$k]->cr_amount;
					    $ending_year = $grouped_array_years[$projected_account_id][$k]->year;
					    $year_ending_balance = $opening_dr_amount - $opening_cr_amount;


					    $fixed_asset_result .='<tr class="info">
													<td colspan="5" ><strong>CLOSING BALANCE AS AT '.$ending_year.' '.$end_month.' '.$end_day.'  </strong></td>
													<td class="center-align">'.number_format($year_ending_balance,2).'</td>
												</tr>';
					  
					}
				}
		


					$sub_total = 0;
					// get the account opening balance
				 	$second_array = count($grouped_array_income[$projected_account_id]);
					$opening_balance = 0;
					if($second_array > 0)
					{

						$debit_opening = 0;
						$credit_opening = 0;
						for ($z=0; $z < $second_array; $z++) { 

							$opening_dr_amount = $grouped_array_income[$projected_account_id][$z]->dr_amount;
						    $opening_cr_amount = $grouped_array_income[$projected_account_id][$z]->cr_amount;
						    $opening_balance = $opening_dr_amount - $opening_cr_amount;

						    if($opening_balance > 0)
						    {
						    	$debit_opening = $opening_balance;
							
							

								$opening = number_format($debit_opening,2);
						    }
						    else
							{
								$credit_opening = -$opening_balance;
				
								$opening = "(".number_format($credit_opening,2).")";

							}

						}
					}
					$fixed_asset_result .='<tr class="primary">
												<td><strong>'.$account_name.' </strong></td>
												<td class="text-center" colspan="4"></td>
												<td class="center-align">'.number_format($opening_balance,2).'</td>
											</tr>';

					$sub_total += $opening_balance;

					$first_array = count($grouped_array[$projected_account_id]);
					$debit = 0;
					$credit = 0;
					$total_amount = 0;
					if($first_array > 0 OR $second_array > 0)
					{

						for ($i=0; $i < $first_array; $i++) { 


					  		$dr_amount = $grouped_array[$projected_account_id][$i]->dr_amount;
						    $cr_amount = -$grouped_array[$projected_account_id][$i]->cr_amount;
						    $total_amount = $dr_amount - $cr_amount;

						    $total_amount += $opening_balance;




								if($total_amount > 0)
								{
									$debit = $total_amount;
									$total_debits += $debit;
								}
								else
								{
									$credit = -$total_amount;
									$total_credits += $credit;
								}




						    $transactionDescription = $grouped_array[$projected_account_id][$i]->transactionDescription;
						    $referenceCode = $grouped_array[$projected_account_id][$i]->referenceCode;
						    $transactionDate = $grouped_array[$projected_account_id][$i]->transactionDate;
						    $accountName = $grouped_array[$projected_account_id][$i]->accountName;
						    $transactionCategory = $grouped_array[$projected_account_id][$i]->transactionCategory;


						    
						    	
						    $account_value = $dr_amount+$cr_amount;
						    

						    $running_balance += $account_value;
						    $sub_total += $account_value;
							

								$fixed_asset_result .= '<tr>
															        		  <td > '.$transactionCategory.'</td>
															        			<td > '.$transactionDate.'</td>
																						<td > '.$referenceCode.'</td>
																						<td > '.$transactionDescription.'</td>
																						<td class="center-align">'.number_format($account_value,2).'</td>
																	        	<td class="center-align">'.number_format($sub_total,2).'</td>
																				</tr>';


						}

					


					}

					$fixed_asset_result .= '<tr class="success">
													        		  <td > <strong>TOTAL '.$account_name.'</strong></td>
												        					<td > </td>
																			<td > </td>
																			<td > </td>
																			<td class="center-align"> '.number_format($running_balance,2).'</td>
															        	<td class="center-align">'.number_format($sub_total,2).'</td>
																		</tr>';


		}


		// type == 3


		
	}
}


?>


<!--end reports -->
<div class="row">
	<div class="col-md-2">
	</div>
    <div class="col-md-8">

        <section class="panel ">
            <header class="panel-heading">

                <h2 class="panel-title"><?php echo strtoupper($title);?></h2>
                <a href="<?php echo site_url();?>company-financials/balance-sheet"  class="btn btn-sm btn-info pull-right" style="margin-top:-25px;margin-left:5px" > Back to Balance Sheet </a>
               
            </header>

            <div class="panel-body">


			<?php
			if(!empty($ledger_search))
			{
				?>
                <a href="<?php echo base_url().'accounting/petty_cash/close_expense_ledger';?>" class="btn btn-sm btn-danger"><i class="fa fa-cancel"></i> Close Search</a>
                <?php
			}
			$error = $this->session->userdata('error_message');
			$success = $this->session->userdata('success_message');

			if(!empty($error))
			{
				echo '<div class="alert alert-danger">'.$error.'</div>';
				$this->session->unset_userdata('error_message');
			}

			if(!empty($success))
			{
				echo '<div class="alert alert-success">'.$success.'</div>';
				$this->session->unset_userdata('success_message');
			}

			// echo $result;



?>			
				<table class="table table-hover table-bordered ">
				 	<thead>
						<tr>
						  <th></th>
						  <th>TRANSACTION DATE</th>
						  <th>REFERENCE</th>
						  <th>DESCRIPTION</th>
			              <th>AMOUNT</th>
			              <th>ARREARS</th>
						</tr>
					 </thead>
				  	<tbody>
	          			<?php echo $fixed_asset_result;?>
					</tbody>
				</table>

          	</div>
		</section>
    </div>
    <div class="col-md-2">
	</div>
</div>

<script type="text/javascript">



	$(document).on("change","select#transaction_type_id",function(e)
	{
		var transaction_type_id = $(this).val();

		if(transaction_type_id == '1')
		{
			// deposit
			$('#from_account_div').css('display', 'block');
			$('#account_to_div').css('display', 'block');
			// $('#consultation').css('display', 'block');
		}
		else if(transaction_type_id == '2')
		{
			// expenditure
			$('#from_account_div').css('display', 'block');
			$('#account_to_div').css('display', 'none');
			// $('#consultation').css('display', 'block');
		}
		else
		{
			$('#from_account_div').css('display', 'none');
			$('#account_to_div').css('display', 'none');
		}


	});
</script>
