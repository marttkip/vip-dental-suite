<?php
$view_staging_rs = $this->company_financial_model->get_all_staging_accounts();
?>
<table class="table table-condensed table-bordered ">
 	<thead>
		<tr>
		  <th>Staging Name</th>
		  <th>Account Name</th>
		  <th>Account</th>
		  <th>Reference Name</th>
		  <th>Account Status</th>
		  <th>Account Type</th>
		  <th>Has Childred</th>
		  <th>Balance Sheet Position</th>
		  <th>Balance on Report</th>
		  <th>Income Statement Status</th>
		</tr>
	 </thead>
  	<tbody>	

  		<?php

  		if($view_staging_rs->num_rows() > 0)
  		{
  			foreach ($view_staging_rs->result() as $key => $value) {
  				// code...

  				$account_staging_id = $value->account_staging_id;
				$account_staging_name = $value->account_staging_name;
				$has_children = $value->bs_account_type_id;
				$has_children = $value->has_children;
				$account_name = $value->account_name;
				$account_id = $value->account_id;
				$bs_account_position = $value->bs_account_position;
				$bs_account_status = $value->bs_account_status;
				$bs_account_balance_report = $value->bs_account_balance_report;
				$bs_account_type_id = $value->bs_account_type_id;
				$reference_name = $value->reference_name;
				$account_staged = $value->account_id;
				$bs_account_income_status = $value->bs_account_income_status;



				?>
				<tr>
					<td class="warning">
						<input type="text" class="form-control" name="account_staging_name<?php echo $account_staging_id?>" id="account_staging_name<?php echo $account_staging_id?>" value="<?php echo $account_staging_name?>" onkeyup="update_staging_accounts(<?php echo $account_staging_id?>)"></td>
					<td class="primary">
						<?php echo $account_name?>

                    </td>
                    <td><input type="text" class="form-control" name="account_id<?php echo $account_staging_id?>" id="account_id<?php echo $account_staging_id?>" value="<?php echo $account_id?>" onkeyup="update_staging_accounts(<?php echo $account_staging_id?>)"></td>
					<td><input type="text" class="form-control" name="reference_name<?php echo $account_staging_id?>" id="reference_name<?php echo $account_staging_id?>" value="<?php echo $reference_name?>" onkeyup="update_staging_accounts(<?php echo $account_staging_id?>)"></td>

					<td><input type="text" class="form-control"  name="bs_account_status<?php echo $account_staging_id?>" id="bs_account_status<?php echo $account_staging_id?>" value="<?php echo $bs_account_status?>" onkeyup="update_staging_accounts(<?php echo $account_staging_id?>)"></td>
					<td><input type="text" class="form-control"  name="bs_account_type_id<?php echo $account_staging_id?>" id="bs_account_type_id<?php echo $account_staging_id?>" value="<?php echo $bs_account_type_id?>" onkeyup="update_staging_accounts(<?php echo $account_staging_id?>)"></td>

					<td><input type="text" class="form-control"  name="has_children<?php echo $account_staging_id?>" id="has_children<?php echo $account_staging_id?>" value="<?php echo $has_children;?>" onkeyup="update_staging_accounts(<?php echo $account_staging_id?>)"></td>
					<td><input type="text" class="form-control"  name="bs_account_position<?php echo $account_staging_id?>" id="bs_account_position<?php echo $account_staging_id?>" value="<?php echo $bs_account_position;?>" onkeyup="update_staging_accounts(<?php echo $account_staging_id?>)"></td>
					<td><input type="text" class="form-control"  name="bs_account_balance_report<?php echo $account_staging_id?>" id="bs_account_balance_report<?php echo $account_staging_id?>" value="<?php echo $bs_account_balance_report;?>" onkeyup="update_staging_accounts(<?php echo $account_staging_id?>)"></td>
					<td><input type="text" class="form-control"  name="bs_account_income_status<?php echo $account_staging_id?>" id="bs_account_income_status<?php echo $account_staging_id?>" value="<?php echo $bs_account_income_status;?>" onkeyup="update_staging_accounts(<?php echo $account_staging_id?>)"></td>


					<td><a onclick="remove_staging_account(<?php echo $account_staging_id?>)"><i class="fa fa-trash"></i></a></td>
				</tr>


				<?php
  			}
  		}
  		?>
		
	</tbody>
</table>