
<!-- search -->
<?php //echo $this->load->view('search_accounts', '', TRUE);?>


        

<?php
		
		$result = '';
		$query = $this->company_financial_model->get_all_account_types();
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Code</th>
						<th>Account</th>
						<th>Status</th>					
						<th colspan="4">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			foreach ($query->result() as $row)
			{
				// $account_id = $row->account_id;
				// $account_name = $row->account_name;
				$account_type_id = $row->account_type_id;
				$account_type_name = $row->account_type_name;
				$account_type_code = $row->account_type_code;
				// $account_opening_balance = $row->account_opening_balance;
				// $account_status = $row->account_status;
				// $parent_account = $row->parent_account;
				$parent_account_name = '';


				$result .='<tr >
								<th class="text-left warning"   > '.strtoupper($account_type_code).'</th>
								<th class="text-left warning"  colspan="5"  > '.strtoupper($account_type_name).'</th>
								
							</tr>';

				$assets_rs = $this->company_financial_model->get_parent_accounts_by_type($account_type_name);


				$total_fixed = 0;
				if($assets_rs->num_rows() > 0)
				{
					foreach ($assets_rs->result() as $key => $value) {
						# code...
						
						$parent_account_name = $value->account_name;
						$parent_account_code = $value->account_code;
						$parent_account_id = $value->account_id;
						$parent_account_status = $value->account_status;

						$child_accounts_rs = $this->company_financial_model->get_child_accounts_by_parent_id($parent_account_id);

						if($child_accounts_rs->num_rows() > 0)
						{
							$item = '<i class="fa fa-arrow-right"></i> ';
						}
						else
						{
							$item = '';
						}
						$total_fixed_category = 0;
						
						if($parent_account_status == 0)
						{
							$status = '<span class="label label-default">Deactivated</span>';
							$button = '<a class="btn btn-sm btn-info" href="'.site_url().'accounting/charts-of-accounts/activate-account/'.$parent_account_id.'" onclick="return confirm(\'Do you want to activate '.$parent_account_name.'?\');" title="Activate '.$parent_account_name.'"><i class="fa fa-thumbs-up"></i></a>';
						}
						//create activated status display
						else if($parent_account_status == 1)
						{
							$status = '<span class="label label-success">Active</span>';
							$button = '<a class="btn btn-sm btn-default" href="'.site_url().'accounting/charts-of-accounts/deactivate-account/'.$parent_account_id.'" onclick="return confirm(\'Do you want to deactivate '.$parent_account_name.'?\');" title="Deactivate '.$parent_account_name.'"><i class="fa fa-thumbs-down"></i></a>';
						}

						// parent accounts 
						$result .='<tr >
										<th class="text-left" colspan="" > '.$item.strtoupper($parent_account_code).'</th>
										<th class="text-left" colspan="" > '.$item.strtoupper($parent_account_name).'</th>
										<td>'.$status.'</td>
										<td><a href="'.site_url().'accounting/charts-of-accounts/edit-account/'.$parent_account_id.'" class="btn btn-sm btn-success" title="Edit '.$parent_account_name.'"><i class="fa fa-pencil"></i></a></td>
										<td>'.$button.'</td>
										<td><a href="'.site_url().'accounting/charts-of-accounts/delete-account/'.$parent_account_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$parent_account_name.'?\');" title="Delete '.$parent_account_name.'"><i class="fa fa-trash"></i></a></td>
									</tr>';
						if($child_accounts_rs->num_rows() > 0)
						{
					
							foreach ($child_accounts_rs->result() as $key => $value2) {
								# code...
								$child_account_name = $value2->account_name;
								$child_account_id = $value2->account_id;
								$child_account_status = $value2->account_status;
								$child_account_code = $value2->account_code;
								// $child_account_status = $value2->account_status;

								// $total_fixed_category += $account_amount;

								if($child_account_status == 0)
								{
									$status = '<span class="label label-default">Deactivated</span>';
									$button = '<a class="btn btn-sm btn-info" href="'.site_url().'accounting/charts-of-accounts/activate-account/'.$child_account_id.'" onclick="return confirm(\'Do you want to activate '.$child_account_name.'?\');" title="Activate '.$child_account_name.'"><i class="fa fa-thumbs-up"></i></a>';
								}
								//create activated status display
								else if($child_account_status == 1)
								{
									$status = '<span class="label label-success">Active</span>';
									$button = '<a class="btn btn-sm btn-default" href="'.site_url().'accounting/charts-of-accounts/deactivate-account/'.$child_account_id.'" onclick="return confirm(\'Do you want to deactivate '.$child_account_name.'?\');" title="Deactivate '.$child_account_name.'"><i class="fa fa-thumbs-down"></i></a>';
								}


								$result .='<tr>
												<td class="text-left" colspan="" ><i class="fa fa-angle-right"></i>  '.strtoupper($child_account_code).'</td>
												<td class="text-left" colspan="" ><i class="fa fa-angle-right"></i>  '.strtoupper($child_account_name).'</td>
												<td>'.$status.'</td>
												<td><a href="'.site_url().'accounting/charts-of-accounts/edit-account/'.$child_account_id.'" class="btn btn-sm btn-success" title="Edit '.$child_account_name.'"><i class="fa fa-pencil"></i></a></td>
												<td>'.$button.'</td>
												<td><a href="'.site_url().'accounting/charts-of-accounts/delete-account/'.$child_account_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$child_account_name.'?\');" title="Delete '.$child_account_name.'"><i class="fa fa-trash"></i></a></td>
											</tr>';



							}
							
						}

					}
				}
	
				// if($parent_account > 0)
				// {
				// 	$parent_account_name = $this->company_financial_model->get_parent_account($parent_account);
				// }
				
				// //status
				// if($account_status == 1)
				// {
				// 	$status = 'Active';
				// }
				// else
				// {
				// 	$status = 'Disabled';
				// }
				
				// //create deactivated status display
				
				$count++;
				// $result .= 
				// '
				// 	<tr>
				// 		<td>'.$count.'</td>
				// 		<td>'.$account_name.'</td>
				// 		<td>'.$parent_account_name.'</td>
				// 		<td>'.$account_type_name.'</td>
				// 		<td>'.$account_opening_balance.'</td>
				// 		<td>'.$status.'</td>
				// 		<td><a href="'.site_url().'accounting/charts-of-accounts/edit-account/'.$account_id.'" class="btn btn-sm btn-success" title="Edit '.$account_name.'"><i class="fa fa-pencil"></i></a></td>
				// 		<td>'.$button.'</td>
				// 		<td><a href="'.site_url().'accounting/charts-of-accounts/delete-account/'.$account_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$account_name.'?\');" title="Delete '.$account_name.'"><i class="fa fa-trash"></i></a></td>
				// 	</tr> 
				// ';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no personnel";
		}
?>
<div class="col-md-2">
			
</div>
<div class="col-md-8">

<section class="panel">
    <header class="panel-heading">						
        <h2 class="panel-title"><?php echo $title;?></h2>
        <div class="pull-right">
        	 <a href="<?php echo site_url();?>accounting/add-account" style="margin-top:-40px;" class="btn btn-sm btn-info"><i class="fa fa-plus"></i> Add Account</a>
        </div>
    </header>
    <div class="panel-body">
       <?php
        $success = $this->session->userdata('success_message');

        if(!empty($success))
        {
            echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
            $this->session->unset_userdata('success_message');
        }
        
        $error = $this->session->userdata('error_message');
        
        if(!empty($error))
        {
            echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
            $this->session->unset_userdata('error_message');
        }

        ?>
        <?php
					$search = $this->session->userdata('search_petty_cash1');
		
					if(!empty($search))
					{
						echo '<a href="'.site_url().'accounting/petty_cash/close_search_petty_cash" class="btn btn-warning btn-sm">Close Search</a>';
					}
					?>

		
	        <div class="table table-bordered table-striped table-condensed">
	            
	            <?php echo $result;?>
	    
	        </div>
	    
    </div>
    
</section>
</div>
<div class="col-md-2">
			
</div>