<?php

	
// get cost of goods
		$parent_account_id = $this->company_financial_model->get_parent_account_id('Payroll');

		$query = $this->ledgers_model->get_all_child_accounts($parent_account_id);


		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Account Name</th>
						
						<th colspan="2">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			
			foreach ($query->result() as $row)
			{
				
				$account_id = $row->account_id;
				$account_name = $row->account_name;


				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$account_name.'</td>
			          	<td><a href="'.site_url().'financials/disburse-mpesa/'.$account_id.'" class="btn btn-sm btn-success" title="M-PESA ><i class="fa fa-money"></i> M-PESA</a></td>
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no accounts";
		}
?>






<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
			<div class="panel-heading">
                <div class="panel-tools" style="color: #fff;">
                   
                </div>
                <?php echo $title;?>
                
            </div>

			<div class="panel-body">
		    	<?php
				$search = $this->session->userdata('customer_search_title2');
				
				if(!empty($search))
				{
					echo '<h6>Filtered by: '.$search.'</h6>';
					echo '<a href="'.site_url().'hr/customer/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
				}
		        $success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				?>
				<div class="row" style="margin-bottom:20px;">
                                   <div class="col-lg-2 col-lg-offset-10">
                                        <a href="<?php echo site_url();?>financials" class="btn btn-sm btn-success pull-right">Send ALL M-PESA</a>
                                     </div>
                                     
                            </div>       
				<div class="table-responsive">
		        	
					<?php echo $result;?>
			
		        </div>
			</div>
		    <div class="panel-footer">
		    	<?php if(isset($links)){echo $links;}?>
		    </div>
		 </div>
	</div>
</div>

