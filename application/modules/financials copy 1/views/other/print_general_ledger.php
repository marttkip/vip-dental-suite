<?php

                
    $this->session->set_userdata('general_ledger_search');

    $changed = '';


     $accounts = $this->ledgers_model->get_all_parent_accounts();
     $grand_amount = 0;
     $grand_balance = 0;
     $grand_dr = 0;
     $grand_cr = 0;


     if($accounts->num_rows() > 0)
     {
         foreach($accounts->result() as $row):
             // $company_name = $row->company_name;
             $parent_account_name = $row->account_name;
             $parent_account_id = $row->account_id;


            //                   if($parent_account_name == 'Bank')
            // {

                $changed .= '<tr>
                                <th  colspan="6">'.strtoupper($parent_account_name).'</th>
                           </tr>';
                $account_rs = $this->ledgers_model->get_all_child_accounts($parent_account_id);

                $parent_account_dr_amount = 0;
                $parent_account_cr_amount = 0;
                if($account_rs->num_rows() > 0)
                {


                    foreach ($account_rs->result() as $key => $value_six) {
                        // code...
                        $account_name = $value_six->account_name;
                        $account_id = $value_six->account_id;
                

                        
                        $account_ledger_rs = $this->ledgers_model->get_account_ledger($account_id, $date_to, $date_from);

                        $balance = 0;
                        $total_amount = 0;
                        $dr_amount = 0;
                        $cr_amount = 0;

                        $total_dr_amount = 0;
                        $total_cr_amount = 0;
                        if($account_ledger_rs->num_rows() > 0)
                        {
                            $changed .= '<tr>
                                            <th  colspan="6">'.strtoupper($account_name).'</th>
                                       </tr>';
                            foreach ($account_ledger_rs->result() as $key => $value) 
                            {

                                $dr_amount = $value->dr_amount;
                                $cr_amount = $value->cr_amount;
                                $transactionDescription = $value->transactionDescription;
                                $referenceCode = $value->referenceCode;
                                $transactionDate = $value->transactionDate;
                                $accountName = $value->accountName;
                                $transactionCategory = $value->transactionCategory;

                                $total_dr_amount += $dr_amount;
                                $total_cr_amount += $cr_amount;
                                $grand_dr += $dr_amount;
                                $grand_cr += $cr_amount;

                                $parent_account_dr_amount += $dr_amount;
                                $parent_account_cr_amount += $cr_amount;

                                $changed .= '<tr>
                                                <td > '.$transactionCategory.'</td>
                                                <td > '.$transactionDate.'</td>
                                                <td > '.$referenceCode.'</td>
                                                <td > '.$transactionDescription.'</td>
                                                <td class="center-align">'.number_format($dr_amount,2).'</td>
                                                <td class="center-align">'.number_format($cr_amount,2).'</td>
                                            </tr>';
                            }

                            $changed .= '<tr>
                                            <th >TOTAL '.strtoupper($accountName).'</th>
                                            <th ></th>
                                            <th ></th>
                                            <th ></th>
                                            <th class="center-align">'.number_format($total_dr_amount,2).'</th>
                                            
                                            <th class="center-align">'.number_format($total_cr_amount,2).'</th>
                                            
                                        </tr>';
                        }
                    }
                    $changed .= '<tr>
                                    <td colspan="6"></td>
                                </tr>';
                    $changed .= '<tr>
                                            <th >TOTAL '.strtoupper($parent_account_name).'</th>
                                            <th ></th>
                                            <th ></th>
                                            <th ></th>
                                            <th class="center-align">'.number_format($parent_account_dr_amount,2).'</th>
                                            
                                            <th class="center-align">'.number_format($parent_account_cr_amount,2).'</th>
                                            
                                        </tr>';

                }
            // }

           endforeach;


     }





      $changed .= '<tr>
                        <th >TOTAL</th>
                        <th ></th>
                        <th ></th>
                        <th ></th>
                        
                        <th style="border-top:1px solid #000;" class="center-align" >'.number_format($grand_dr,2).'</th>
                        <th style="border-top:1px solid #000;" class="center-align" >'.number_format($grand_cr,2).'</th>
                        
                    </tr>';
 
    ?>






<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | GENERAL LEDGER</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}

			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>

      <div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align" style="padding: 5px;">
            	<strong>GENERAL LEDGER</strong><br>

            	<?php
            	// $search_title = $this->session->userdata('income_statement_title_search');

      				 if(empty($search_title))
      				 {
      				 	$search_title = "";
      				 }
      				 else
      				 {
      				 	$search_title =$search_title;
      				 }
				 echo $search_title;
            	?>

            </div>
        </div>

    	<div class="row">
        	<div style="margin: auto;">
						<div class="col-md-12" style="margin-top: 10px;">
						
						<table class="table" id="testTable">
							<!-- <tr>
								<th style="width: 100%"> <h6> <strong>INCOME</strong></h6> </th>
							</tr> -->
                            <thead>
                                
                         
    							<tr>
    								<th > TYPE </th>
    								<th >DATE</th>
                                    <th >NUM</th>
                                    <th >NAME</th>
                                    <th >MEMO</th>
                                    <th >SPLIT</th>
                                    <th class="center-align">DEBIT</th>
    					            <th class="center-align">CREDIT</th>
    							</tr>
                            </thead>

							<tbody>
								<?php echo $changed;?>
							</tbody>
							

							
							
							
						</table>
						<a href="#" onclick="javascript:xport.toCSV('testTable');">XLS</a>
						</div>
            </div>
        </div>

    	<div class="row" style="font-style:italic; font-size:11px;">
        	<div class="col-sm-12">
                <div class="col-sm-10 pull-left">
                    <strong>Prepared by: </strong>
                </div>
                <div class="col-sm-2 pull-right">
                    <?php echo date('jS M Y H:i a'); ?>
                </div>
            </div>

        </div>
    </body>

</html>

<script type="text/javascript">
	var xport = {
  _fallbacktoCSV: true,
  toXLS: function(tableId, filename) {
    this._filename = (typeof filename == 'undefined') ? tableId : filename;

    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls');
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }

    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it

      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};

</script>
