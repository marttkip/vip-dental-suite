

<?php
	$date_from = $this->session->userdata('date_from_general_ledger');
	$date_to = $this->session->userdata('date_to_general_ledger');
	$add = '';
	if(!empty($date_from) OR !empty($date_to))
	{

		$date_from = $date_from;
		$date_to = $date_to;

	}

	else
	{

		$date_from = date('Y-m-01');
		$date_to = date('Y-m-d');
		

	}

	$search_title = $this->session->userdata('general_ledger_search_title');

?>

<div class="row">
	<div class="col-md-4">
		<?php echo $this->load->view('search/search_general_ledger','', true);?>
		<div class="text-center">
			<h3 class="box-title">General Ledger</h3>
			<h5 class="box-title"><?php echo $search_title?></h5>
			<h6 class="box-title">Created <?php echo date('jS M Y');?></h6>


			

		</div>
		<hr>
		<div class="col-md-12">
			<div class="form-group">
				<a href="#"  class="btn btn-md btn-warning  col-md-12" onclick="javascript:xport.toCSV('GENERAL LEDGER');">Export General Ledger</a>
			</div>
			<div class="form-group">
			      <a href="<?php echo site_url().'company-financials/profit-and-loss'?>"  class="btn btn-md btn-success col-md-12"><i class="fa fa-file"></i> Income Statement</a>
			</div>
			<div class="form-group">
			      <a href="<?php echo site_url().'company-financials/balance-sheet'?>"  class="btn btn-md btn-info col-md-12"><i class="fa fa-file"></i> Balance Sheet</a>
			</div>
			<div class="form-group">
			      <a href="<?php echo site_url().'company-financials/trial-balance'?>"  class="btn btn-md btn-primary col-md-12"><i class="fa fa-file"></i> Trial Balance</a>
			</div>
			
		</div>
	</div>


	<div class="col-md-8">
	
		<div class="box">
		    <div class="panel-body" style="height:85vh;overflow-y:scroll;padding:0px !important;">
		    	<!-- <h3 class="box-title">Revenue</h3> -->
		    	<table class="table  table-striped table-condensed table-linked" id="GENERAL LEDGER">
					<thead>
						<tr>
		        			<th >TYPE</th>
		        			<th >DATE</th>
									<th >NUM</th>
									<th >NAME</th>
									<th class="center-align">DEBIT</th>
									<th class="center-align">CREDIT</th>
						</tr>
					</thead>
					<tbody>

						<?php

						
						$this->session->set_userdata('general_ledger_search');

						$changed = '';
						$all_transacted_rs = $this->ledgers_model->get_account_ledger_by_accounts();


							$arr = json_decode(json_encode ( $all_transacted_rs->result() ) , true);

							$grouped_array = array();
							foreach ($all_transacted_rs->result() as $element) {
								
							    $grouped_array[$element->accountId][] = $element;
							}



						 $accounts = $this->ledgers_model->get_all_parent_accounts();
						 $grand_amount = 0;
						 $grand_balance = 0;
						 $grand_dr = 0;
						 $grand_cr = 0;


             if($accounts->num_rows() > 0)
             {
                 foreach($accounts->result() as $row):
                     // $company_name = $row->company_name;
                     $parent_account_name = $row->account_name;
                     $parent_account_id = $row->account_id;


                     

									$account_rs = $this->ledgers_model->get_all_child_accounts($parent_account_id);
									
									$parent_account_dr_amount = 0;
									$parent_account_cr_amount = 0;
									if($account_rs->num_rows() > 0)
									{
										$changed .= '<tr>
											        			<th  colspan="6" class="info">'.strtoupper($parent_account_name).'</th>
														   </tr>';

										foreach ($account_rs->result() as $key => $value_six) {
											// code...
											$account_name = $value_six->account_name;
			                $account_id = $value_six->account_id;
									
			                // get total rows for the account

			                $first_array = count($grouped_array[$account_id]);
			                $balance = 0;
											$total_amount = 0;
											$dr_amount = 0;
											$cr_amount = 0;

											$total_dr_amount = 0;
											$total_cr_amount = 0;
									    if($first_array > 0)
									    {
									    	$changed .= '<tr>
														        			<th  colspan="6" class="primary">'.strtoupper($account_name).'</th>
																	   </tr>';


											  for ($i=0; $i < $first_array; $i++) { 


											  		$dr_amount = $grouped_array[$account_id][$i]->dr_amount;
												    $cr_amount = $grouped_array[$account_id][$i]->cr_amount;
												    $transactionDescription = $grouped_array[$account_id][$i]->transactionDescription;
												    $referenceCode = $grouped_array[$account_id][$i]->referenceCode;
												    $transactionDate = $grouped_array[$account_id][$i]->transactionDate;
												    $accountName = $grouped_array[$account_id][$i]->accountName;
												    $transactionCategory = $grouped_array[$account_id][$i]->transactionCategory;



														$total_dr_amount += $dr_amount;
														$total_cr_amount += $cr_amount;
														$grand_dr += $dr_amount;
														$grand_cr += $cr_amount;

														$parent_account_dr_amount += $dr_amount;
														$parent_account_cr_amount += $cr_amount;

														$changed .= '<tr>
																        		  <td > '.$transactionCategory.'</td>
																        			<td > '.$transactionDate.'</td>
																							<td > '.$referenceCode.'</td>
																							<td > '.$transactionDescription.'</td>
																							<td class="center-align">'.number_format($dr_amount,2).'</td>
																		        	<td class="center-align">'.number_format($cr_amount,2).'</td>
																					</tr>';


											  }
											  $changed .= '<tr>
																				<th >TOTAL '.strtoupper($accountName).'</th>
															        			<th ></th>
																				<th ></th>
																				<th ></th>
																				<th class="center-align">'.number_format($total_dr_amount,2).'</th>
															        			
																				<th class="center-align">'.number_format($total_cr_amount,2).'</th>
																				
																			</tr>';

									    }

								    	

			             
									}
									$changed .= '<tr>
																	<td colspan="6"></td>
																</tr>';
									$changed .= '<tr>
																<th >TOTAL '.strtoupper($parent_account_name).'</th>
											        			<th ></th>
																<th ></th>
																<th ></th>
																<th class="center-align">'.number_format($parent_account_dr_amount,2).'</th>
											        			
																<th class="center-align">'.number_format($parent_account_cr_amount,2).'</th>
																
															</tr>';

									}
              endforeach;
             }

				$changed .= '</tbody>
		                  <tfoot>
			                  <tr>
												<th >TOTAL</th>
							        	<th ></th>
												<th ></th>
												<th ></th>
												
							        	<th style="border-top:1px solid #000;" class="center-align" >'.number_format($grand_dr,2).'</th>
												<th style="border-top:1px solid #000;" class="center-align" >'.number_format($grand_cr,2).'</th>
												
											</tr>
										</tfoot>';
		     echo $changed;
						?>
						
					
				</table>


		    </div>
		</div>
	</div>

</div>
<script type="text/javascript">
		var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};


</script>