

<?php
	$date_from = $this->session->userdata('date_from_general_ledger');
	$date_to = $this->session->userdata('date_to_general_ledger');
	$add = '';
	if(!empty($date_from) OR !empty($date_to))
	{

		$date_from = $date_from;
		$date_to = $date_to;

	}

	else
	{

		$date_from = date('Y-m-01');
		$date_to = date('Y-m-d');
		

	}

	$search_title = $this->session->userdata('general_ledger_search_title');

?>

<?php
$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

$patients = array();

if($accounts_config_rs->num_rows() > 0)
{
	foreach ($accounts_config_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$reference_name = $value->reference_name;

		$session_account[$reference_name] = $staing_account_id;

		
	}
}

$providers_liability_id = $session_account['providers_liability_id'];
$providers_wht_id = $session_account['providers_wht_id'];
$payroll_liability_id = $session_account['payroll_liability_id'];
$accounts_payable_id = $session_account['accounts_payable_id'];
$fixed_assets_id = $session_account['fixed_assets_id'];
$accounts_receivable_id = $session_account['accounts_receivable_id'];
$suppliers_wht_id = $session_account['suppliers_wht_id'];
$income_account_id = $session_account['income_account_id'];



$all_transacted_rs = $this->ledgers_model->get_account_ledger_by_accounts();


$arr = json_decode(json_encode ( $all_transacted_rs->result() ) , true);

$grouped_array = array();
foreach ($all_transacted_rs->result() as $element) {
	
    $grouped_array[$element->accountId][] = $element;
}

$account_report_rs = $this->company_financial_model->get_staging_accounts(1);


$arrOrphans = array();
if($account_report_rs->num_rows() > 0)
{

	foreach ($account_report_rs->result() as $key => $value) {
		// code...
		$account_id = $value->account_id;

		$has_children = $value->has_children;


		if($has_children)
		{
			// check if account has children
			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($account_id);
				

			if($children_account->num_rows() > 0)
			{	

				foreach ($children_account->result() as $key_two => $value_two) {
					// code...
					$child_account_id = $value_two->account_id;
					$arrOrphans[$child_account_id] =  $child_account_id;
				}

				
			}
			else
			{
				$arrOrphans[$value->account_id] =  $value->account_id;
			}
		}
		else
		{
			$arrOrphans[$value->account_id] =  $value->account_id;
		}
		

		
	}

}


$arrBranches = array();
foreach($arrOrphans as $serial){
	$arrSerial = explode(".", $serial);
	$new_serial = "";
	foreach($arrSerial as $part){
		$new_serial .= (strlen($new_serial)>0?".":"") . $part;

		if(!in_array($new_serial, $arrBranches))
			array_push($arrBranches, $new_serial);
	}
}

$query_one = $this->ledgers_model->get_account_ledger_by_accounts_opening_balance($arrBranches);

$grouped_array_old = array();
foreach ($query_one->result() as $element_two) {
    $grouped_array_old[$element_two->accountId][] = $element_two;
}







// all income accounts

$account_report_rs = $this->company_financial_model->get_staging_accounts(2);

$arrOrphansIncome = array();
if($account_report_rs->num_rows() > 0)
{

	foreach ($account_report_rs->result() as $key => $value) {
		// code...
		$account_id = $value->account_id;
		$has_children = $value->has_children;


		if($has_children)
		{

			// check if account has children
			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($account_id);
				

			if($children_account->num_rows() > 0)
			{	

				foreach ($children_account->result() as $key_two => $value_two) {
					// code...
					$child_account_id = $value_two->account_id;
					$arrOrphansIncome[$child_account_id] =  $child_account_id;
				}

				
			}
			else
			{
				$arrOrphansIncome[$value->account_id] =  $value->account_id;
			}
		}
		else
		{
			$arrOrphansIncome[$value->account_id] =  $value->account_id;
		}

		
	}

}

$arrBranchesIncome = array();
foreach($arrOrphansIncome as $serialIncome){
	$arrSerial = explode(".", $serialIncome);
	$new_serial = "";
	foreach($arrSerial as $part){
		$new_serial .= (strlen($new_serial)>0?".":"") . $part;

		if(!in_array($new_serial, $arrBranchesIncome))
			array_push($arrBranchesIncome, $new_serial);
	}
}

$query_three = $this->ledgers_model->get_account_ledger_by_accounts_opening_periods($arrBranchesIncome);

$grouped_array_income = array();
foreach ($query_three->result() as $element_three) {
    $grouped_array_income[$element_three->accountId][] = $element_three;
}




$this->db->where('bs_account_status = 1 AND bs_account_type_id > 0  AND account_staging_delete = 0');
$this->db->order_by('bs_account_position','ASC');
$accounts_staging_rs = $this->db->get('account_staging');
// var_dump($accounts_staging_rs->result());die();
$running_balance = 0;
if($accounts_staging_rs->num_rows() > 0)
{
	foreach ($accounts_staging_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$account_staging_name = $value->account_staging_name;
		$bs_account_type_id = $value->bs_account_type_id;
		$has_children = $value->has_children;
		$bs_account_balance_report = $value->bs_account_balance_report;
		$sub_total = 0;
	
		if($bs_account_type_id == 1 AND $has_children == 0 AND $bs_account_balance_report == 0)
		{
			$debit = 0;
			$credit = 0;
			$first_array = count($grouped_array[$staing_account_id]);

			if($first_array > 0)
			{

				for ($i=0; $i < $first_array; $i++) { 


			  		$dr_amount = $grouped_array[$staing_account_id][$i]->dr_amount;
				    $cr_amount = $grouped_array[$staing_account_id][$i]->cr_amount;
				    $accounts_receivable = $dr_amount - $cr_amount;

				    // $credit =0;
					if($accounts_receivable > 0)
					{
						$debit = $accounts_receivable;
						$total_debits += $debit;
					}
					else
					{
						$credit = -$accounts_receivable;
						$total_credits += $credit;
					}


					
					$fixed_asset_result .='<tr>
												<td class="text-left"><strong>'.$account_staging_name.'</strong></td>
												<td class="text-center">
													<a href="'.site_url().'account-transactions/'.$staing_account_id.'" > '.number_format($debit,2).' </a>
												</td>
												<td class="text-center">
													<a href="'.site_url().'account-transactions/'.$staing_account_id.'" >'.number_format($credit,2).'</a>
												</td>
											</tr>';
				}

				
			}

			// unset($grouped_array);

		}



		else if($bs_account_type_id == 1 AND $has_children == 0 AND $bs_account_balance_report == 1)
		{
			$sub_total = 0;
			// get the account opening balance
		 	$second_array = count($grouped_array_old[$staing_account_id]);
			$opening_balance = 0;
			if($second_array > 0)
			{

				$debit_opening = 0;
				$credit_opening = 0;
				for ($z=0; $z < $second_array; $z++) { 

					$opening_dr_amount = $grouped_array_old[$staing_account_id][$z]->dr_amount;
				    $opening_cr_amount = $grouped_array_old[$staing_account_id][$z]->cr_amount;
				    $opening_balance = $opening_dr_amount - $opening_cr_amount;

				    if($opening_balance > 0)
				    {
				    	$debit_opening = $opening_balance;
						// $total_debits += $debit_opening;
					

						$opening = number_format($debit_opening,2);
				    }
				    else
					{
						$credit_opening = -$opening_balance;

						// $total_credits += $credit_opening;
						$opening = "(".number_format($credit_opening,2).")";

					}
					// $running_balance += $opening_balance;
					
					

				}
			}
			$fixed_asset_result .='<tr class="primary">
																<td><strong>'.$account_staging_name.'</strong></td>
																<td class="text-center" colspan="4"></td>
																<td class="center-align">'.number_format($opening_balance,2).'</td>
															</tr>';
			$debit = 0;
			$credit = 0;
			$first_array = count($grouped_array[$staing_account_id]);

			if($first_array > 0)
			{

				for ($i=0; $i < $first_array; $i++) { 


			  		$dr_amount = $grouped_array[$staing_account_id][$i]->dr_amount;
				    $cr_amount = -$grouped_array[$staing_account_id][$i]->cr_amount;
				    $accounts_receivable = $dr_amount - $cr_amount;
				    $accounts_receivable += $opening_balance;
				    // $credit =0;
						if($accounts_receivable > 0)
						{
							$debit = $accounts_receivable;
							$total_debits += $debit;
						}
						else
						{
							$credit = -$accounts_receivable;
							$total_credits += $credit;
						}
					  $transactionDescription = $grouped_array[$staing_account_id][$i]->transactionDescription;
				    $referenceCode = $grouped_array[$staing_account_id][$i]->referenceCode;
				    $transactionDate = $grouped_array[$staing_account_id][$i]->transactionDate;
				    $accountName = $grouped_array[$staing_account_id][$i]->accountName;
				    $transactionCategory = $grouped_array[$staing_account_id][$i]->transactionCategory;


				   
				    	
				    $account_value = $dr_amount+$cr_amount;
				    $account_value += $opening_balance;

				    $running_balance += $account_value;
				    $sub_total += $account_value;
						

						$fixed_asset_result .= '<tr>
								        		  <td > '.$transactionCategory.'</td>
								        			<td > '.$transactionDate.'</td>
															<td > '.$referenceCode.'</td>
															<td > '.$transactionDescription.'</td>
															<td class="center-align">'.number_format($account_value,2).'</td>
										        	<td class="center-align">'.number_format($running_balance,2).'</td>
													</tr>';

					
					

				}

				
			}

			// unset($grouped_array);

			$fixed_asset_result .= '<tr class="success">
					        		  <td > <strong>SUB TOTAL '.$account_staging_name.'</strong></td>
					        			<td > </td>
												<td > </td>
												<td > </td>
												<td class="center-align"></td>
							        	<td class="center-align">'.number_format($sub_total,2).'</td>
										</tr>';

		}


		else if($bs_account_type_id == 2 AND $has_children == 1 AND $bs_account_balance_report == 0)
		{
			
			// var_dump($staing_account_id);die();

			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($staing_account_id);
			

			if($children_account->num_rows() > 0)
			{	
				$fixed_asset_result .='<tr>
											<th class="text-left" colspan="3">'.strtoupper($account_staging_name).' </th>
											
										</tr>';
				foreach ($children_account->result() as $key => $value) {
					# code...
					$account_name = $value->account_name;
					$account_id = $value->account_id;

					$sub_total = 0;
					$first_array = count($grouped_array[$account_id]);
					$debit = 0;
					$credit = 0;
					if($first_array > 0)
					{

						for ($i=0; $i < $first_array; $i++) { 


					  		$dr_amount = $grouped_array[$account_id][$i]->dr_amount;
						    $cr_amount = $grouped_array[$account_id][$i]->cr_amount;
						    $total_amount = $dr_amount - $cr_amount;


							if($total_amount > 0)
							{
								$debit = $total_amount;
								$total_debits += $debit;
							}
							else
							{
								$credit = -$total_amount;
								$total_credits += $credit;
							}







							$fixed_asset_result .='<tr>
														<td class="text-left">'.strtoupper($account_name).' </td>
														<td class="text-center">
															<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($debit,2).'</a>
														</td>
														<td class="text-center">
															<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($credit,2).'</a>
														</td>
													</tr>';


						}

					}

				}
			}

		}



		else if($bs_account_type_id == 3 AND $has_children == 1 AND $bs_account_balance_report == 1)
		{

			
		
			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($staing_account_id);
			

			if($children_account->num_rows() > 0)
			{	
				$fixed_asset_result .='<tr>
											<th class="text-left" colspan="3">'.strtoupper($account_staging_name).' </th>
											
										</tr>';
				foreach ($children_account->result() as $key => $value) {
					# code...
					$account_name = $value->account_name;
					$account_id = $value->account_id;

					$sub_total = 0;
					// get the account opening balance
				 	$second_array = count($grouped_array_old[$account_id]);
					$opening_balance = 0;
					if($second_array > 0)
					{

						$debit_opening = 0;
						$credit_opening = 0;
						for ($z=0; $z < $second_array; $z++) { 

							$opening_dr_amount = $grouped_array_old[$account_id][$z]->dr_amount;
						    $opening_cr_amount = $grouped_array_old[$account_id][$z]->cr_amount;
						    $opening_balance = $opening_dr_amount - $opening_cr_amount;

						    if($opening_balance > 0)
						    {
						    	$debit_opening = $opening_balance;
								// $total_debits += $debit_opening;
							

								$opening = number_format($debit_opening,2);
						    }
						    else
							{
								$credit_opening = -$opening_balance;
								// $total_credits += $credit_opening;
								$opening = "(".number_format($credit_opening,2).")";

							}


						}
					}
					$fixed_asset_result .='<tr class="primary">
																		<td><strong>'.$account_name.'</strong></td>
																		<td class="text-center" colspan="4"></td>
																		<td class="center-align">'.number_format($opening_balance,2).'</td>
																	</tr>';

					$sub_total += $opening_balance;
					$first_array = count($grouped_array[$account_id]);
					$debit = 0;
					$credit = 0;
					$total_amount = 0;
					if($first_array > 0 OR $second_array > 0)
					{

						for ($i=0; $i < $first_array; $i++) { 


					  		$dr_amount = $grouped_array[$account_id][$i]->dr_amount;
						    $cr_amount = -$grouped_array[$account_id][$i]->cr_amount;
						    $total_amount = $dr_amount - $cr_amount;

						    // $credit =0;
								if($accounts_receivable > 0)
								{
									$debit = $accounts_receivable;
									$total_debits += $debit;
								}
								else
								{
									$credit = -$accounts_receivable;
									$total_credits += $credit;
								}
							  $transactionDescription = $grouped_array[$account_id][$i]->transactionDescription;
						    $referenceCode = $grouped_array[$account_id][$i]->referenceCode;
						    $transactionDate = $grouped_array[$account_id][$i]->transactionDate;
						    $accountName = $grouped_array[$account_id][$i]->accountName;
						    $transactionCategory = $grouped_array[$account_id][$i]->transactionCategory;


						    
						    	
						    $account_value = $dr_amount+$cr_amount;
						    

						    $running_balance += $account_value;
						    $sub_total += $account_value;
								// $total_dr_amount += $dr_amount;
								// $total_cr_amount += $cr_amount;
								// $grand_dr += $dr_amount;
								// $grand_cr += $cr_amount;

								// $parent_account_dr_amount += $dr_amount;
								// $parent_account_cr_amount += $cr_amount;

								$fixed_asset_result .= '<tr>
															        		  <td > '.$transactionCategory.'</td>
															        			<td > '.$transactionDate.'</td>
																						<td > '.$referenceCode.'</td>
																						<td > '.$transactionDescription.'</td>
																						<td class="center-align">'.number_format($account_value,2).'</td>
																	        	<td class="center-align">'.number_format($sub_total,2).'</td>
																				</tr>';


						}

					


					}

						$fixed_asset_result .= '<tr class="success">
								        		  <td > <strong>SUB TOTAL '.$account_name.'</strong></td>
								        			<td > </td>
															<td > </td>
															<td > </td>
															<td class="center-align"></td>
										        	<td class="center-align">'.number_format($sub_total,2).'</td>
													</tr>';


				}
			}

		}



		// all p and l accounts

		
		else if($bs_account_type_id == 3 AND ($has_children == 1 OR $has_children == 0)  AND $bs_account_balance_report == 2)
		{

			
			
		
			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($staing_account_id);
			

			if($children_account->num_rows() > 0)
			{	
				$fixed_asset_result .='<tr>
											<th class="text-left" colspan="3">'.strtoupper($account_staging_name).' </th>
											
										</tr>';
				foreach ($children_account->result() as $key => $value) {
					# code...
					$account_name = $value->account_name;
					$account_id = $value->account_id;


					$sub_total = 0;
					// get the account opening balance
				 	$second_array = count($grouped_array_income[$account_id]);
					$opening_balance = 0;
					if($second_array > 0)
					{

						$debit_opening = 0;
						$credit_opening = 0;
						for ($z=0; $z < $second_array; $z++) { 

							$opening_dr_amount = $grouped_array_income[$account_id][$z]->dr_amount;
						    $opening_cr_amount = $grouped_array_income[$account_id][$z]->cr_amount;
						    $opening_balance = $opening_dr_amount - $opening_cr_amount;

						    if($opening_balance > 0)
						    {
						    	$debit_opening = $opening_balance;
							
							

								$opening = number_format($debit_opening,2);
						    }
						    else
							{
								$credit_opening = -$opening_balance;
				
								$opening = "(".number_format($credit_opening,2).")";

							}

						}
					}
					$fixed_asset_result .='<tr class="primary">
																		<td><strong>'.$account_name.'</strong></td>
																		<td class="text-center" colspan="4"></td>
																		<td class="center-align">'.number_format($opening_balance,2).'</td>
																	</tr>';

					$sub_total += $opening_balance;

					$first_array = count($grouped_array[$account_id]);
					$debit = 0;
					$credit = 0;
					$total_amount = 0;
					if($first_array > 0 OR $second_array > 0)
					{

						for ($i=0; $i < $first_array; $i++) { 


					  		$dr_amount = $grouped_array[$account_id][$i]->dr_amount;
						    $cr_amount = -$grouped_array[$account_id][$i]->cr_amount;
						    $total_amount = $dr_amount - $cr_amount;

						    $total_amount += $opening_balance;




								if($total_amount > 0)
								{
									$debit = $total_amount;
									$total_debits += $debit;
								}
								else
								{
									$credit = -$total_amount;
									$total_credits += $credit;
								}




						    $transactionDescription = $grouped_array[$account_id][$i]->transactionDescription;
						    $referenceCode = $grouped_array[$account_id][$i]->referenceCode;
						    $transactionDate = $grouped_array[$account_id][$i]->transactionDate;
						    $accountName = $grouped_array[$account_id][$i]->accountName;
						    $transactionCategory = $grouped_array[$account_id][$i]->transactionCategory;


						    
						    	
						    $account_value = $dr_amount+$cr_amount;
						    

						    $running_balance += $account_value;
						    $sub_total += $account_value;
							

								$fixed_asset_result .= '<tr>
										        		  <td > '.$transactionCategory.'</td>
										        			<td > '.$transactionDate.'</td>
																	<td > '.$referenceCode.'</td>
																	<td > '.$transactionDescription.'</td>
																	<td class="center-align">'.number_format($account_value,2).'</td>
												        	<td class="center-align">'.number_format($sub_total,2).'</td>
														</tr>';


						}

					    
						


					}

					$fixed_asset_result .= '<tr class="success">
													        		  <td > <strong>SUB TOTAL '.$account_name.'</strong></td>
													        			<td > </td>
																				<td > </td>
																				<td > </td>
																				<td class="center-align"></td>
															        	<td class="center-align">'.number_format($sub_total,2).'</td>
																		</tr>';


				}
			}
			else
			{

				// var_dump($staing_account_id);die();
				// get the account opening balance
				 	$second_array = count($grouped_array_income[$staing_account_id]);
					$opening_balance = 0;
					if($second_array > 0)
					{

						$debit_opening = 0;
						$credit_opening = 0;
						for ($z=0; $z < $second_array; $z++) { 

							$opening_dr_amount = $grouped_array_income[$staing_account_id][$z]->dr_amount;
						    $opening_cr_amount = $grouped_array_income[$staing_account_id][$z]->cr_amount;
						    $opening_balance = $opening_dr_amount - $opening_cr_amount;

						    if($opening_balance > 0)
						    {
						    	$debit_opening = $opening_balance;
								// $total_debits += $debit_opening;
							

								$opening = number_format($debit_opening,2);
						    }
						    else
							{
								$credit_opening = -$opening_balance;
								// $total_credits += $credit_opening;
								$opening = "(".number_format($credit_opening,2).")";

							}


						}
					}
					$fixed_asset_result .='<tr class="primary">
											<td><strong>'.$account_staging_name.'</strong></td>
											<td class="text-center" colspan="4"></td>
											<td class="center-align">'.number_format($opening_balance,2).'</td>
										</tr>';

					$first_array = count($grouped_array[$staing_account_id]);
					$debit = 0;
					$credit = 0;
					$total_amount = 0;
					if($first_array > 0 OR $second_array > 0)
					{

						for ($i=0; $i < $first_array; $i++) { 


					  		$dr_amount = $grouped_array[$staing_account_id][$i]->dr_amount;
						    $cr_amount = $grouped_array[$staing_account_id][$i]->cr_amount;
						    $total_amount = $dr_amount - $cr_amount;

						    // $debit = $dr_amount;
						    // $credit = $cr_amount;

						    // if($account_id == 271)
						    // {
						    // 	var_dump($total_amount);
						    // }
						}

					    $total_amount += $opening_balance;

					   	// get the account opening balance



						if($total_amount > 0)
						{
							$debit = $total_amount;
							$total_debits += $debit;
						}
						else
						{
							$credit = -$total_amount;
							$total_credits += $credit;
						}

						 $running_balance += $account_value;
					    $sub_total += $account_value;
						

							$fixed_asset_result .= '<tr>
									        		  <td > '.$transactionCategory.'</td>
									        			<td > '.$transactionDate.'</td>
																<td > '.$referenceCode.'</td>
																<td > '.$transactionDescription.'</td>
																<td class="center-align">'.number_format($account_value,2).'</td>
											        	<td class="center-align">'.number_format($sub_total,2).'</td>
													</tr>';
					


					}
			}
		}


		// type == 3


		
	}
}

?>

<div class="row">
	<div class="col-md-4">
		<?php echo $this->load->view('search/search_general_ledger','', true);?>
		<div class="text-center">
			<h3 class="box-title">General Ledger</h3>
			<h5 class="box-title"><?php echo $search_title?></h5>
			<h6 class="box-title">Created <?php echo date('jS M Y');?></h6>


			

		</div>
		<hr>
		<?php
		$search_title = 'GENERAL LEDGER '.$search_title;
		?>
		<div class="col-md-12">
			<div class="col-md-12" style="margin-bottom:20px">
				<div class="col-md-6">
					<div class="form-group">
						  <a onclick="javascript:xport.toCSV('<?php echo $search_title?>');" target="_blank" class="btn btn-md btn-success col-md-12"><i class="fa fa-print"></i> EXPORT G L</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<a href="<?php echo site_url().'print-trial-balance'?>" target="_blank" class="btn btn-md btn-warning col-md-12"><i class="fa fa-print"></i> Print G L</a>
					</div>
				</div>
				
			</div>
			<br>
			<div class="col-md-12">
				
				<div class="form-group">
				      <a href="<?php echo site_url().'company-financials/profit-and-loss'?>"  class="btn btn-md btn-success col-md-12"><i class="fa fa-file"></i> Income Statement</a>
				</div>
				<div class="form-group">
				      <a href="<?php echo site_url().'company-financials/balance-sheet'?>"  class="btn btn-md btn-info col-md-12"><i class="fa fa-file"></i> Balance Sheet</a>
				</div>
				<div class="form-group">
				      <a href="<?php echo site_url().'company-financials/trial-balance'?>"  class="btn btn-md btn-primary col-md-12"><i class="fa fa-file"></i> Trial Balance</a>
				</div>
			</div>
			
		</div>
	</div>


	<div class="col-md-8">
	
		<div class="box">
		    <div class="panel-body" style="height:85vh;overflow-y:scroll;padding:0px !important;">
		    	<table class="table  table-striped table-condensed table-linked" id="<?php echo $search_title?>">
					<thead>
						<tr>
		        			<th >TYPE</th>
		        			<th >DATE</th>
									<th >NUM</th>
									<th >NAME</th>
									<th class="center-align">AMOUNT</th>
									<th class="center-align">BALANCE</th>
						</tr>
					</thead>
					<tbody>
						<?php echo $fixed_asset_result;?>
					</tbody>
				</table>
		


		    </div>
		</div>
	</div>

</div>
<script type="text/javascript">
		var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};


</script>