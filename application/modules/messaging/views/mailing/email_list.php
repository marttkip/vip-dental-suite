<?php
// var_dump()

// $count_visit = $visit_list->num_rows();
$num_pages = $total_rows/$per_page;

if($num_pages < 1)
{
	$num_pages = 0;
}
$num_pages = round($num_pages);

if($page==0)
{
	$counted = 0;
}
else if($page > 0)
{
	$counted = $per_page*$page;
}
$result_view = '<ul id="" class="list-unstyled">';
if($email_query->num_rows() > 0)
{
	
	foreach ($email_query->result() as $key => $value) {
		# code...
		$email_id = $value->email_id;
		$email_description = $value->email_description;
		$email_from = $value->email_from;
		$email_cc = $value->email_cc;
		$email_to = $value->email_to;
		$email_to = $value->email_to;
		$email_status = $value->email_status;
		$created = $value->created;
		$last_modified = $value->last_modified;
		$email_deleted = $value->email_deleted;
		$patient_name = $value->patient_name;
		$email_subject = $value->email_subject;
		$category = $value->category;
		$department_name = $value->department_name;
		$department_id = $value->department_id;
		$sms = $value->sms;
		// $email_subject = $value->email_subject;
		$last_modified = date('jS M Y H:i A',strtotime($last_modified));


		if($email_status == 0)
		{
			$color = 'unread';
		}
		else 
		{
			$color = 'unread';

		}
		

		if($category == 2)
		{
			$email_subject = $sms;
		}
		else
		{
			$email_subject = $email_subject;
		}


		if($department_id == 0)
		{
			$department_name = ' - Administration';
		}
		else
		{
			$department_name = ' - '.$department_name;
		}

		if($category == 1)
		{
			$category_color = '#EA4C89';
		}
		else if($category == 2)
		{
			$category_color = '#0088CC';
		}
		else if($category == 0)
		{
			$category_color = '#800080';
		}
		$mini_desc = implode(' ', array_slice(explode(' ', $email_description), 0, 5));	
		
		$counted++;
		$result_view .= '
						  <li class="'.$color.'">
								<a onclick="get_email_details('.$email_id.','.$page.')">
									<div class="col-sender">
										<i class="mail-label" style="border-color: '.$category_color.'"></i>
										<div class="checkbox-custom checkbox-text-primary ib">
											<input type="checkbox" id="mail'.$email_id.'">
											<label for="mail1"></label>
										</div>
										<p class="m-0 ib">'.$patient_name.'</p>
									</div>
									<div class="col-mail">
										<p class="m-0 mail-content">
											<span class="subject">'.$email_subject.' '.$department_name.'</span>
										</p>
										<p class="m-0 mail-date">'.$last_modified.'</p>
									</div>
								</a>
							</li>';
	}
}
$result_view .= '</ul>';
echo $result_view;
?>

<div class="row">
	<div class="col-md-12" style="padding-right: 25px;">
		<div class="pull-right">
			<?php
				$link ='<ul style="list-style:none;">';
				// echo $page;
				if($num_pages > $page)
				{
					// echo "now ".$num_pages." ".$page;
					$last_page = $num_pages -1;

					if($page > 0 AND $page < $last_page)
					{
						// echo $page;
						$page++;
						// echo "now".$page;
						$previous = $page -2;
						$link .='<li onclick="get_email_list('.$previous.')" class="pull-left" style="margin-right:20px;" > <i class="fa fa-angle-left"></i> Back</li>  <li onclick="get_email_list('.$page.')" class="pull-right"> Next <i class="fa fa-angle-right"></i> </li>';
					}else if($page == $last_page)
					{
						$page++;

						$previous = $page -2;
						// echo "equal".$num_pages." ".$page;
						$link .='<li onclick="get_email_list('.$previous.')" class="pull-left"> <i class="fa fa-angle-left"></i> Back</li>';
					}
					else
					{
						$page++;
						$link .='<li onclick="get_email_list('.$page.')" class="pull-right"> Next <i class="fa fa-angle-right"></i> </li>';
					}
					// var_dump($link); die();
				}
				$link .='</ul>';
				echo $link;
				
			?>
		</div>
	</div>
</div>


