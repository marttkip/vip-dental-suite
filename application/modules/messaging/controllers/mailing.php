<?php
date_default_timezone_set('Africa/Nairobi');
error_reporting(0);
class Mailing extends MX_Controller
{
	var $csv_path;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('messaging_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('admin/email_model');

		$this->load->model('messaging/mailing_model');

		$this->load->model('reception/reception_model');



		$this->csv_path = realpath(APPPATH . '../assets/csv');
	}

	
	public function index()
	{
		$where = 'patient_id > 0 ';
		$total_contacts = $this->messaging_model->count_items('patients',$where);

		$sent_where = 'message_status = 1 ';
		$sent_messages = $this->messaging_model->count_items('messages',$sent_where);

		$unsent_where = 'message_status > 1 ';
		$unsent_messages = $this->messaging_model->count_items('messages',$unsent_where);

		// calculate total cost

		$cost = $this->messaging_model->get_total_cost();

		$total_amount = 0;//$this->messaging_model->get_amount_toped_up();
		$v_data['title'] = 'Dashboard';
		$data['title'] = 'Dashboard';
		$v_data['total_contacts'] = $total_contacts;
		$v_data['sent_messages'] = $sent_messages;
		$v_data['unsent_messages'] = $unsent_messages;
		$v_data['balance'] = $total_amount - $cost;

		$data['content'] = $this->load->view('mailing/mailbox', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}
	public function email_view()
	{
		$v_data['title'] = 'Emails';

		$v_data['title'] = 'Dashboard';
		$data['title'] = 'Dashboard';
		$page = NULL;
		if($page == NULL)
		{
			$page = 0;
		}

		$search = $this->input->post('search');

		$where='emails.email_id > 0';
		if(!empty($search))
		{
			$where .= ' AND (emails.patient_name LIKE \'%'.mysql_real_escape_string($search).'%\' OR emails.email_subject LIKE \'%'.mysql_real_escape_string($search).'%\' OR emails.email_from LIKE \'%'.mysql_real_escape_string($search).'%\' OR emails.email_description LIKE \'%'.mysql_real_escape_string($search).'%\') ';
		}
		// $page = 1;
		
		// var_dump($counted);
		$table= 'emails';
		
		$config["per_page"] = $v_data['per_page'] = $per_page = 10;
		if($page==0)
		{
			$counted = 0;
		}
		else if($page > 0)
		{
			$counted = $per_page*$page;
		}

		

		$v_data['page'] = $page;
		$page = $counted;
		$v_data['total_rows'] = $this->mailing_model->count_items($table, $where);
		$query = $this->mailing_model->get_all_emails($table, $where, $config["per_page"], $page);

		$v_data['email_query'] = $query;
		echo $this->load->view('mailing/mailing_list', $v_data, true);
    }

    public function emails_items($page=null)
	{

		if($page == NULL)
		{
			$page = 0;
		}

		$search = $this->input->post('search');

		$where='emails.email_id > 0';
		if(!empty($search))
		{
			$where .= ' AND (emails.patient_name LIKE \'%'.mysql_real_escape_string($search).'%\' OR emails.email_subject LIKE \'%'.mysql_real_escape_string($search).'%\' OR emails.email_from LIKE \'%'.mysql_real_escape_string($search).'%\') ';
		}
		// $page = 1;
		
		// var_dump($counted);
		$table= 'emails';
		
		$config["per_page"] = $v_data['per_page'] = $per_page = 15;
		if($page==0)
		{
			$counted = 0;
		}
		else if($page > 0)
		{
			$counted = $per_page*$page;
		}

		

		$v_data['page'] = $page;
		$page = $counted;
		$v_data['total_rows'] = $this->mailing_model->count_items($table, $where);
		$query = $this->mailing_model->get_all_emails($table, $where, $config["per_page"], $page);

		$v_data['email_query'] = $query;

		// var_dump($query);die();
		$v_data['title'] = 'Emails';
		echo $this->load->view('mailing/email_list', $v_data, true);
    }

    public function email_detail($email_id,$page)
    {
    	$v_data['title'] = 'Emails';
    	$v_data['page'] = $page;
    	$v_data['email_id'] = $email_id;
		echo $this->load->view('mailing/email_detail', $v_data, true);
    }

    public function compose_email_view()
    {
    	$patients_order = 'patient_id';		    
		$patients_where = 'patient_id > 0';
		$patients_table = 'patients';

		$patients_query = $this->reception_model->get_all_patients_details($patients_table, $patients_where,$patients_order);

		$rs14 = $patients_query->result();
		$patients = '';
		foreach ($rs14 as $patients_rs) :

		  $patients_id = $patients_rs->patient_id;
		  // $patients_id = $patients_rs->patient_id;
		  $patients_name = $patients_rs->patient_surname.' '.$patients_rs->patient_othernames.' - '.$patients_rs->patient_number.' Email '.$patients_rs->patient_email;

		  $patients .="<option value='".$patients_id."'>".$patients_name."</option>";

		endforeach;
		$v_data['patients'] = $patients;
    	$v_data['title'] = 'Compose Email';
		echo $this->load->view('mailing/compose_email', $v_data, true);

    }

    public function get_patient_email_detail($patient_id)
    {
    	$this->db->where('patient_id',$patient_id);
    	$query = $this->db->get('patients');
    	$patient_email = '';
    	if($query->num_rows() == 1)
    	{	
    		$row = $query->row();

    		$response['patient_email'] = $row->patient_email;
    		$response['patient_phone'] = $row->patient_phone1;
    		$response['message'] = 'success';
    		
    	}
    	else
    	{
    		$response['message'] = 'fail';
    	}
    	echo json_encode($response);
    }

    public function send_email_message()
    {
    	$category = $this->input->post('category');

    	if($category == 0)
    	{
    		$this->form_validation->set_rules('message', 'Message', 'required');
			$this->form_validation->set_rules('email_from', 'Email From', 'required');
			$this->form_validation->set_rules('patient_id', 'Patient', 'required|is_natural_no_zero');
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('sms', 'Message', 'required');
			$this->form_validation->set_rules('phone_number', 'Phone Number', 'required');
    	}
    	else if($category == 1)
    	{
    		$this->form_validation->set_rules('message', 'Message', 'required');
			$this->form_validation->set_rules('email_from', 'Email From', 'required');
			$this->form_validation->set_rules('patient_id', 'Patient', 'required|is_natural_no_zero');
			$this->form_validation->set_rules('email', 'Email', 'required');
    	}
    	else if($category == 2)
    	{
    		$this->form_validation->set_rules('sms', 'Message', 'required');
			$this->form_validation->set_rules('patient_id', 'Patient', 'required|is_natural_no_zero');
			$this->form_validation->set_rules('phone_number', 'Phone Number', 'required');
    	}
    	
		
		if ($this->form_validation->run() == FALSE)
		{
			$response['message'] = "fail";	
			$response['result'] =  validation_errors();	
		}
		else
		{	
			$patient_email = $this->input->post('email');
			$message = $this->input->post('message');
			$subject = $this->input->post('subject');

	    	if(!empty($patient_email) AND ($category == 0 OR $category == 1))
			{

				$email_message = $message;

				$message_result['subject'] = $this->input->post('subject');
				$v_data['persons'] = $email_message;
				$text =  $this->load->view('reception/emails_items',$v_data,true);

				// echo $text; die();
				$message_result['text'] = $text;
				$contacts = $this->site_model->get_contacts();
				$sender_email = $this->input->post('email_from');//$this->config->item('sender_email');//$contacts['email'];
				$shopping = "";
				$from = $sender_email; 
				
				$button = '';
				$sender['email']= $sender_email; 
				$sender['name'] = $contacts['company_name'];
				$receiver['name'] = $message_result['subject'];
				// $payslip = $title;

				$sender_email = $sender_email;
				$tenant_email = $patient_email.'/'.$sender_email;
				// var_dump($tenant_email); die();
				$email_array = explode('/', $tenant_email);
				$total_rows_email = count($email_array);

				for($x = 0; $x < $total_rows_email; $x++)
				{
					$receiver['email'] = $email_tenant = $email_array[$x];

					$this->email_model->send_sendgrid_mail($receiver, $sender, $message_result, NULL);
					
				}
				
			}

			if($category == 0 OR $category == 2)
			{
				$patient_phone = $phone_number = $this->input->post('phone_number');
				$sms = $this->input->post('sms');
				$patient_phone = str_replace(' ', '', $patient_phone);
				$sms_response = $this->messaging_model->sms($patient_phone,$sms);

				if($sms_response == "Success" OR $sms_response == "success")
				{
					$patient_array['sms_sent'] = 1;
				}
				else
				{
					$patient_array['sms_sent'] = 2;
				}
			}
			$patient_id = $this->input->post('patient_id');

			$this->db->where('patient_id',$patient_id);
	    	$query = $this->db->get('patients');
	    	$patient_email = '';
	    
    		$row = $query->row();

    		$patient_name = $row->patient_surname.' '.$row->patient_othernames;

			

			if($category == 0 OR $category == 2)
			{
				$patient_phone = $phone_number = $this->input->post('phone_number');
				$sms = $this->input->post('sms');

				$patient_array['phone_number'] = $phone_number;
				$patient_array['sms'] = $sms;
				$patient_array['patient_name'] = $patient_name;
				$patient_array['created'] = date('Y-m-d');
				$patient_array['last_modified'] = date('Y-m-d H:i:s');
				$patient_array['patient_id'] = $patient_id;
			}
			if($category == 0 OR $category == 1)
			{

				$patient_array['email_from'] = $sender_email;
				$patient_array['email_to'] = $tenant_email;
				$patient_array['email_subject'] = $subject;
				$patient_array['email_description'] = $message;
				$patient_array['email_status'] = 1;
				$patient_array['created_by'] = $this->session->userdata('personnel_id');
				$patient_array['created'] = date('Y-m-d');
				$patient_array['last_modified'] = date('Y-m-d H:i:s');
				$patient_array['patient_id'] = $patient_id;
				$patient_array['patient_name'] = $patient_name;
			}
			
			$patient_array['category'] = $category;
			$this->db->insert('emails',$patient_array);

			
			$response['message'] = "success";	
				
			
			
		}

		echo json_encode($response);

    }

    public function compose_message()
    {
    	// var_dump($patient_name);die();
		$data['title'] = 'Compose Message';
		// if($form_type_id == 1)
		// {
		// 	$data['title'] = 'Add Contacts';
		// }
		// else if($form_type_id == 2)
		// {
		// 	$data['title'] = 'Suppliers Contacts';
		// 	$creditor_where = 'creditor_id > 0';
		// 	$creditor_table = 'creditor';

		// 	$this->db->where($creditor_where);
		// 	$creditor_query = $this->db->get($creditor_table);

		// 	$data['creditor_query'] = $creditor_query;


		// }
		// else if($form_type_id == 3)
		// {
		// 	$data['title'] = 'Insurance Contacts';
		// 	$insurance_where = 'insurance_company_id > 0';
		// 	$insurance_table = 'insurance_company';

		// 	$this->db->where($insurance_where);
		// 	$insurance_query = $this->db->get($insurance_table);

		// 	$data['insurance_query'] = $insurance_query;
		// }
		$page = $this->load->view('sidebar/compose_message',$data);

		echo $page;
    }

    public function search_patients($message_type,$identifier)
    {
    	$symptoms_search = $this->input->post('query');
    	$date_from = $this->input->post('date_from');
    	$date_to = $this->input->post('date_to');
    	$doctor_id = $this->input->post('doctor_id');

    	$recall_list_id = $this->input->post('recall_list_id');

    	// $doctor_id = $this->input->post('doctor_id');
		$query = null;
		if(empty($symptoms_search) AND  (empty($date_from) OR $date_from == 'null') AND (empty($date_to) OR $date_to == 'null') AND $message_type == 0)
		{
			$lab_test_where = 'patients.patient_delete = 0';
			$lab_test_table = 'patients';

		

			// $lab_test_where .= ' AND (patient_surname LIKE \'%'.$symptoms_search.'%\' OR patient_number LIKE \'%'.$symptoms_search.'%\' OR patient_phone1 LIKE \'%'.$symptoms_search.'%\')';

			$this->db->where($lab_test_where);
			$this->db->limit(10);
			$query = $this->db->get($lab_test_table);
		}
		else if(!empty($symptoms_search) AND $message_type == 0 OR  (!empty($date_from) OR $date_from != 'null') OR (!empty($date_to) OR $date_to != 'null'))
		{
			$lab_test_where = 'patients.patient_delete = 0';
			$lab_test_table = 'patients';


			if(!empty($date_from) && !empty($date_to))
			{
				$visit_date = ' AND DATE(patients.patient_date) BETWEEN \''.$date_from.'\' AND \''.$date_to.'\'';
			}
			
			else if(!empty($date_from))
			{
				$visit_date = ' AND DATE(patients.patient_date) = \''.$date_from.'\'';
			}
			
			else if(!empty($date_to))
			{
				$visit_date = ' AND DATE(patients.patient_date) = \''.$date_to.'\'';
			}
			
			else
			{
				$visit_date = '';
			}


			//search surname
			
			$surnames = explode(" ",$symptoms_search);
			$total = count($surnames);
			
			$count = 1;
			$lab_test_where .= ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$lab_test_where .= ' (patients.patient_surname LIKE \'%'.$surnames[$r].'%\' OR patients.patient_othernames LIKE \'%'.$surnames[$r].'%\' OR patients.patient_number LIKE \'%'.$surnames[$r].'%\' OR patients.patient_phone1 LIKE \'%'.$surnames[$r].'%\')';
				}
				
				else
				{
					$lab_test_where .= ' (patients.patient_surname LIKE \'%'.$surnames[$r].'%\' OR patients.patient_othernames LIKE \'%'.$surnames[$r].'%\' OR patients.patient_number LIKE \'%'.$surnames[$r].'%\' OR patients.patient_phone1 LIKE \'%'.$surnames[$r].'%\') AND ';
				}
				$count++;
			}
			$lab_test_where .= ') ';
			
			$lab_test_where .= $visit_date;

			// $lab_test_where .= ' AND (patient_surname LIKE \'%'.$symptoms_search.'%\' OR patient_number LIKE \'%'.$symptoms_search.'%\' OR patient_phone1 LIKE \'%'.$symptoms_search.'%\')';

			$this->db->where($lab_test_where);
			$this->db->limit(10);
			$query = $this->db->get($lab_test_table);

		}
		else if((empty($symptoms_search) OR !empty($doctor_id) OR $doctor_id != 'null' OR  !empty($date_from) OR $date_from != 'null' OR !empty($date_to) OR $date_to != 'null') AND $message_type == 1)
		{

			if(!empty($date_from) && !empty($date_to))
			{
				$visit_date = ' AND appointments.appointment_date BETWEEN \''.$date_from.'\' AND \''.$date_to.'\'';
			}
			
			else if(!empty($date_from))
			{
				$visit_date = ' AND appointments.appointment_date = \''.$date_from.'\'';
			}
			
			else if(!empty($date_to))
			{
				$visit_date = ' AND appointments.appointment_date = \''.$date_to.'\'';
			}
			
			else
			{
				$visit_date = 'AND appointments.appointment_date >= "'.date('Y-m-d').'"';
			}
		
			$lab_test_where = 'appointments.appointment_delete = 0 AND visit.visit_id = appointments.visit_id AND  patients.patient_id = visit.patient_id '.$visit_date;
			$lab_test_table = 'appointments,visit,patients';

			$this->db->select('visit_date, time_start, time_end, appointments.*,appointments.appointment_id AS appointment,appointments.created as date_created,patients.patient_surname,patients.patient_othernames,patients.patient_id,patients.patient_number,patients.patient_phone1,patients.patient_email,personnel.personnel_fname,personnel.personnel_onames,patients.*');
			

			//search surname
			
			$surnames = explode(" ",$symptoms_search);
			$total = count($surnames);
			
			$count = 1;
			$lab_test_where .= ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$lab_test_where .= ' (patients.patient_surname LIKE \'%'.$surnames[$r].'%\' OR patients.patient_othernames LIKE \'%'.$surnames[$r].'%\' OR patients.patient_number LIKE \'%'.$surnames[$r].'%\' OR patients.patient_phone1 LIKE \'%'.$surnames[$r].'%\')';
				}
				
				else
				{
					$lab_test_where .= ' (patients.patient_surname LIKE \'%'.$surnames[$r].'%\' OR patients.patient_othernames LIKE \'%'.$surnames[$r].'%\' OR patients.patient_number LIKE \'%'.$surnames[$r].'%\' OR patients.patient_phone1 LIKE \'%'.$surnames[$r].'%\') AND ';
				}
				$count++;
			}
			$lab_test_where .= ') ';
			


			// $lab_test_where .= ' AND (patient_surname LIKE \'%'.$symptoms_search.'%\' OR patient_number LIKE \'%'.$symptoms_search.'%\' OR patient_phone1 LIKE \'%'.$symptoms_search.'%\')';			
			// $this->db->join('patients','patients.patient_id = visit.patient_id','left');
			$this->db->join('personnel','personnel.personnel_id = visit.personnel_id','left');


			$this->db->where($lab_test_where);
			$query = $this->db->get($lab_test_table);

		}

		else if((empty($symptoms_search) OR !empty($doctor_id) OR $doctor_id != 'null' OR !empty($recall_list_id) OR $recall_list_id != 'null' OR  !empty($date_from) OR $date_from != 'null' OR !empty($date_to) OR $date_to != 'null') AND $message_type == 2)
		{

			$visit_date = '';
			if(!empty($date_from) && !empty($date_to))
			{
				$visit_date = ' AND recall_list.period_date BETWEEN \''.$date_from.'\' AND \''.$date_to.'\'';
			}
			
			else if(!empty($date_from))
			{
				$visit_date = ' AND recall_list.period_date = \''.$date_from.'\'';
			}
			
			else if(!empty($date_to))
			{
				$visit_date = ' AND recall_list.period_date = \''.$date_to.'\'';
			}
			
			else
			{
				// $visit_date = 'AND recall_list.period_date >= "'.date('Y-m-d').'"';
			}

			if(!empty($recall_list_id) or $recall_list_id > 0)
			{
				$visit_date .= ' AND recall_list.list_id = '.$recall_list_id.'';
			}

			if(!empty($doctor_id) or $doctor_id > 0)
			{
				$visit_date .= ' AND recall_list.doctor_id = '.$doctor_id.'';
			}

		
			$lab_test_where = 'recall_list.list_id = schedule_list.list_id AND recall_list.patient_id = patients.patient_id '.$visit_date;
			$lab_test_table = 'recall_list,schedule_list,patients';

			$this->db->select('recall_list.*,recall_list.created as date_created,schedule_list.*,patients.patient_number,patients.patient_phone1,patients.patient_email,personnel.personnel_fname,personnel.personnel_onames,patients.*');
			

			//search surname
			
			$surnames = explode(" ",$symptoms_search);
			$total = count($surnames);
			
			$count = 1;
			$lab_test_where .= ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$lab_test_where .= ' (patients.patient_surname LIKE \'%'.$surnames[$r].'%\' OR patients.patient_othernames LIKE \'%'.$surnames[$r].'%\' OR patients.patient_number LIKE \'%'.$surnames[$r].'%\' OR patients.patient_phone1 LIKE \'%'.$surnames[$r].'%\')';
				}
				
				else
				{
					$lab_test_where .= ' (patients.patient_surname LIKE \'%'.$surnames[$r].'%\' OR patients.patient_othernames LIKE \'%'.$surnames[$r].'%\' OR patients.patient_number LIKE \'%'.$surnames[$r].'%\' OR patients.patient_phone1 LIKE \'%'.$surnames[$r].'%\') AND ';
				}
				$count++;
			}
			$lab_test_where .= ') ';
			


			// $lab_test_where .= ' AND (patient_surname LIKE \'%'.$symptoms_search.'%\' OR patient_number LIKE \'%'.$symptoms_search.'%\' OR patient_phone1 LIKE \'%'.$symptoms_search.'%\')';			
			// $this->db->join('patients','patients.patient_id = visit.patient_id','left');
			$this->db->join('personnel','personnel.personnel_id = recall_list.doctor_id','left');


			$this->db->where($lab_test_where);
			$query = $this->db->get($lab_test_table);


		}

		
		$data['query'] = $query;
		$data['identifier'] = $identifier;

		$data['message_type'] = $message_type;
		$page = $this->load->view('sidebar/search_patients',$data);

		echo $page;
    }

    public function message_patient($patient_id)
    {
    	$patient_name = '';
		$patient_phone1 = '';
		if($patient_id > 0)
		{
			$patient_order = 'patient_id';		    
			$patient_where = 'patient_id ='.$patient_id;
			$patient_table = 'patients';

			$patient_query = $this->reception_model->get_all_visit_type_details($patient_table, $patient_where,$patient_order);

			$rs14 = $patient_query->result();
		
			foreach ($rs14 as $patient_rs) :


			  $patient_id = $patient_rs->patient_id;
			  $patient_phone1 = $patient_rs->patient_phone1;
			  $patient_email = $patient_rs->patient_email;
			  $patient_name = $patient_rs->patient_surname.' '.$patient_rs->patient_othernames.' - '.$patient_rs->patient_number.' - '.$patient_rs->patient_phone1;
			endforeach;
		}

		$data['patient_id'] = $patient_id;
		$data['patient_name'] = $patient_name;
		$data['patient_email'] = $patient_email;
		$data['patient_phone1'] = $patient_phone1;
		$data['patient_phone1'] = $patient_phone1;
		$data['query'] = $patient_query;

		$page = $this->load->view('sidebar/compose_message_view',$data);

		echo $page;
    }
	public function add_message_template()
	{
		//form validation rules
		$this->form_validation->set_rules('template_description', 'Template Description', 'required|xss_clean');
		$this->form_validation->set_rules('template_code', 'Template Code', 'required|xss_clean');
		// $this->form_validation->set_message("is_unique", "A unique preffix is requred.");

		//if form has been submitted
		if ($this->form_validation->run())
		{

			if($this->messaging_model->add_message_template())
			{
				$response['status'] = 'success';
				$response['message'] = 'You have successfully added the template';
			}

			else
			{
				$response['status'] = 'fail';
				$response['message'] = 'Sorry please try again';
			}
		}
		else
		{
			// $this->session->set_userdata('error_message', validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();
		}

		echo json_encode($response);
	}


    public function search_templates()
    {
    	$symptoms_search = $this->input->post('query');
		$query = null;
		// var_dump($query);die();
		$lab_test_where = 'contact_category_id = 0';
		$lab_test_table = 'message_template';

		if(!empty($symptoms_search))
		{
			//search surname
			
			$surnames = explode(" ",$symptoms_search);
			$total = count($surnames);
			
			$count = 1;
			$lab_test_where .= ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$lab_test_where .= ' (message_template.message_template_code LIKE \'%'.$surnames[$r].'%\' )';
				}
				
				else
				{
					$lab_test_where .= ' (message_template.message_template_code LIKE \'%'.$surnames[$r].'%\' ) OR ';
				}
				$count++;
			}
			$lab_test_where .= ' ) ';
			$this->db->where($lab_test_where);
			$this->db->limit(10);
			$query = $this->db->get($lab_test_table);
		}
		else
		{
			$this->db->where($lab_test_where);
			$this->db->limit(10);
			$query = $this->db->get($lab_test_table);
		}
		
		$data['query'] = $query;
		$page = $this->load->view('sidebar/search_message_templates',$data);
		// var_dump($page);die();
		echo $page;
    }

    public function get_message_template($message_template_id)
    {
    	$lab_test_where = 'message_template_id = '.$message_template_id;
		$lab_test_table = 'message_template';
		$this->db->where($lab_test_where);
		$query = $this->db->get($lab_test_table);
		$message_template_description = '';
		$message_template_code = '';
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$message_template_id = $value->message_template_id;
				$message_template_description = $value->message_template_description;
				$message_template_code = $value->message_template_code;
			}
		}

		$response['subject'] = $message_template_code;
		$response['message'] = $message_template_description;
		echo json_encode($response);
    }

    public function send_message_items()
    {

    	$this->form_validation->set_rules('channel_id', 'channel ', 'required|xss_clean');
		$this->form_validation->set_rules('about', 'Message Description', 'required|xss_clean');
		// $this->form_validation->set_message("is_unique", "A unique preffix is requred.");
		 $message = $items_message = $this->input->post('about');
		 $channel_id = $this->input->post('channel_id');
		 $subject = $this->input->post('subject');
		 // $message_type_id = $this->input->post('appointment_id');
		 $contacts  = $this->site_model->get_contacts();
		//if form has been submitted
		if ($this->form_validation->run())
		{	

			$total_visits = sizeof($_POST['patients']);
			//check if any checkboxes have been ticked
			if($total_visits > 0)
			{
				for($r = 0; $r < $total_visits; $r++)
				{
				  $visit = $_POST['patients'];
				  $patients_data = $visit[$r];

				  $patients_explode = explode('#', $patients_data);


				  $patient_id = $patients_explode[0];
				  $patient_phone1 = $patients_explode[1];
				  $patient_name = $patients_explode[2];
				  $patient_email = $patients_explode[3];

				  $message = strip_tags($message);
				  // $patient_phone1 = '+254734808007';
				  // $patient_email = 'marttkip@gmail.com';



				  //check if card is held
				
				  if($channel_id == 0 )
				  {
					  	 $sms_response = $this->messaging_model->sms($patient_phone1,$message);


					  	 // send email
				  	 	$message_result['subject'] = $subject;
						$v_data['persons'] = $items_message;
						$text =  $this->load->view('reception/emails_items',$v_data,true);

						// echo $text; die();
						$message_result['text'] = $text;

						$sender_email = $this->config->item('sender_email');//$contacts['email'];
						$shopping = "";
						$from = $sender_email; 

						$button = '';
						$sender['email']= $sender_email; 
						$sender['name'] = $contacts['company_name'];
						$receiver['name'] = $message_result['subject'];
						// $payslip = $title;

						$sender_email = $sender_email;
						$tenant_email = $patient_email;
						
						$email_array = explode('/', $tenant_email);
						$total_rows_email = count($email_array);

						for($x = 0; $x < $total_rows_email; $x++)
						{
							$receiver['email'] = $email_tenant = $email_array[$x];

							$email_response = $this->email_model->send_sendgrid_mail($receiver, $sender, $message_result, NULL);
							// var_dump($email_response); die();
						}
						
				  }
				  else if($channel_id == 1)
				  {
				  	$sms_response = $this->messaging_model->sms($patient_phone1,$message);
				  }
				  else if($channel_id == 2)
				  {
				  	 	$message_result['subject'] = $subject;
						$v_data['persons'] = $message;
						$text =  $this->load->view('reception/emails_items',$v_data,true);

						// echo $text; die();
						$message_result['text'] = $text;

						$sender_email = $this->config->item('sender_email');//$contacts['email'];
						$shopping = "";
						$from = $sender_email; 

						$button = '';
						$sender['email']= $sender_email; 
						$sender['name'] = $contacts['company_name'];
						$receiver['name'] = $message_result['subject'];
						// $payslip = $title;

						$sender_email = $sender_email;
						$tenant_email = $patient_email;
						// var_dump($tenant_email); die();
						$email_array = explode('/', $tenant_email);
						$total_rows_email = count($email_array);

						for($x = 0; $x < $total_rows_email; $x++)
						{
							$receiver['email'] = $email_tenant = $email_array[$x];

							$this->email_model->send_sendgrid_mail($receiver, $sender, $message_result, NULL);
							
						}
				  }




				
				$patient_array['phone_number'] = $patient_phone1;
				$patient_array['sms'] = $message;
				$patient_array['patient_name'] = $patient_name;
				$patient_array['patient_id'] = $patient_id;
				$patient_array['email_from'] = $sender_email;
				$patient_array['email_to'] = $tenant_email;
				$patient_array['email_subject'] = $subject;
				$patient_array['email_description'] = $items_message;
				$patient_array['email_status'] = 1;
				$patient_array['created_by'] = $this->session->userdata('personnel_id');
				$patient_array['created'] = date('Y-m-d');
				$patient_array['last_modified'] = date('Y-m-d H:i:s');
				
				
				$patient_array['category'] = $channel_id;
				$this->db->insert('emails',$patient_array);


				  
				}

				$response['status'] = 'success';
				$response['message'] = 'You have successfully send the messages';
				 // var_dump($response);die();
			}
			else
			{
				$response['status'] = 'success';
				$response['message'] = 'Sorry Please try again';
			}

			
		}
		else
		{
			// $this->session->set_userdata('error_message', validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();
		}

		echo json_encode($response);

    }

    public function add_patient_to_list($patient_id,$identifier,$list_type)
    {
    	$created_by = $this->session->userdata('personnel_id');
    	$this->db->where('patient_id = '.$patient_id.' AND identifier = '.$identifier.' AND created_by =  '.$created_by);
    	$query = $this->db->get('picked_list');
    	$add_array['patient_id'] = $patient_id;
    	$add_array['identifier'] = $identifier;
    	$add_array['created_by'] = $created_by;
    	$add_array['created'] = date('Y-m-d');
    	$add_array['message_type'] = $list_type;
    	if($query->num_rows() == 0)
    	{
    		$this->db->insert('picked_list',$add_array);
    	}
    	

    }

    public function listed_patients($identifier,$message_type)
    {
    	$symptoms_search = $this->input->post('query');

		$query = null;
		$created_by = $this->session->userdata('personnel_id');
		$lab_test_where = 'patients.patient_delete = 0 AND patients.patient_id = picked_list.patient_id AND picked_list.identifier = '.$identifier.' AND message_type = '.$message_type.' ';
		$lab_test_table = 'patients,picked_list';
		if(!empty($symptoms_search))
		{
			

			//search surname
			
			$surnames = explode(" ",$symptoms_search);
			$total = count($surnames);
			
			$count = 1;
			$lab_test_where .= ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$lab_test_where .= ' (patients.patient_surname LIKE \'%'.$surnames[$r].'%\' OR patients.patient_othernames LIKE \'%'.$surnames[$r].'%\' OR patients.patient_number LIKE \'%'.$surnames[$r].'%\' OR patients.patient_phone1 LIKE \'%'.$surnames[$r].'%\')';
				}
				
				else
				{
					$lab_test_where .= ' (patients.patient_surname LIKE \'%'.$surnames[$r].'%\' OR patients.patient_othernames LIKE \'%'.$surnames[$r].'%\' OR patients.patient_number LIKE \'%'.$surnames[$r].'%\' OR patients.patient_phone1 LIKE \'%'.$surnames[$r].'%\') AND ';
				}
				$count++;
			}
			$lab_test_where .= ') ';
			


			// $lab_test_where .= ' AND (patient_surname LIKE \'%'.$symptoms_search.'%\' OR patient_number LIKE \'%'.$symptoms_search.'%\' OR patient_phone1 LIKE \'%'.$symptoms_search.'%\')';

			$this->db->where($lab_test_where);
			$this->db->limit(10);
			$query = $this->db->get($lab_test_table);

		}
		else
		{

			$this->db->where($lab_test_where);
			$this->db->limit(10);
			$query = $this->db->get($lab_test_table);

		}


		$data['query'] = $query;
		$data['identifier'] = $identifier;
		$page = $this->load->view('sidebar/listed_patients',$data);

		echo $page;
    }

    public function delete_from_list($picked_id)
    {

    	$this->db->where('picked_id',$picked_id);
    	$this->db->delete('picked_list');
    }


    public function add_patients_to_list($message_type,$identifier)
    {
    	$this->db->select('patients.patient_id');
    	$this->db->where('patient_delete = 0 AND category_id = 2 AND patients.patient_id NOT IN (SELECT picked_list.patient_id FROM picked_list WHERE identifier = '.$identifier.')');
    	$query = $this->db->get('patients');

    	if($query->num_rows() > 0)
    	{
    		foreach ($query->result() as $key => $value) {
    			# code...
    			$patient_id = $value->patient_id;

    			$created_by = $this->session->userdata('personnel_id');
		    
		    	$add_array['patient_id'] = $patient_id;
		    	$add_array['identifier'] = $identifier;
		    	$add_array['created_by'] = $created_by;
		    	$add_array['created'] = date('Y-m-d');
		    	$add_array['message_type'] = $message_type;
		    	
		    	$this->db->insert('picked_list',$add_array);
		    	
    		}
    		
    	}
    	
    }

    function search_listed_patients($message_type,$identifier)
    {
    	$symptoms_search = $this->input->post('query');

		$query = null;
		$created_by = $this->session->userdata('personnel_id');
		$lab_test_where = 'patients.patient_delete = 0 AND patients.patient_id = picked_list.patient_id AND picked_list.identifier = '.$identifier.' AND message_type = '.$message_type.' ';
		$lab_test_table = 'patients,picked_list';
		if(!empty($symptoms_search))
		{
			

			//search surname
			
			$surnames = explode(" ",$symptoms_search);
			$total = count($surnames);
			
			$count = 1;
			$lab_test_where .= ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$lab_test_where .= ' (patients.patient_surname LIKE \'%'.$surnames[$r].'%\' OR patients.patient_othernames LIKE \'%'.$surnames[$r].'%\' OR patients.patient_number LIKE \'%'.$surnames[$r].'%\' OR patients.patient_phone1 LIKE \'%'.$surnames[$r].'%\')';
				}
				
				else
				{
					$lab_test_where .= ' (patients.patient_surname LIKE \'%'.$surnames[$r].'%\' OR patients.patient_othernames LIKE \'%'.$surnames[$r].'%\' OR patients.patient_number LIKE \'%'.$surnames[$r].'%\' OR patients.patient_phone1 LIKE \'%'.$surnames[$r].'%\') AND ';
				}
				$count++;
			}
			$lab_test_where .= ') ';
			


			// $lab_test_where .= ' AND (patient_surname LIKE \'%'.$symptoms_search.'%\' OR patient_number LIKE \'%'.$symptoms_search.'%\' OR patient_phone1 LIKE \'%'.$symptoms_search.'%\')';

			$this->db->where($lab_test_where);
			$this->db->limit(10);
			$query = $this->db->get($lab_test_table);

		}
		else
		{

			$this->db->where($lab_test_where);
			$this->db->limit(10);
			$query = $this->db->get($lab_test_table);

		}


		$data['query'] = $query;
		$data['identifier'] = $identifier;
		$page = $this->load->view('sidebar/listed_patients',$data);

		echo $page;
    }
}
?>