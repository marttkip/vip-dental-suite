<?php

// error_reporting(E_ALL);
$services_result = $this->company_financial_model->get_all_service_types();
$service_result = '';
$total_service_invoice = 0;
$total_service_payment = 0;
$total_service_balance = 0;




if($services_result->num_rows() > 0)
{
	$result = $services_result->result();
	$grand_total = 0;			
	foreach($result as $res)
	{
		$service_id = $res->service_id;
		$service_name = $res->service_name;
		// $count++;
		
		//get service total
		$service_invoice = $this->company_financial_model->get_service_invoice_total($service_id);
		$service_payment = $this->company_financial_model->get_service_payments_total($service_id);
		$service_balance = abs($service_payment - $service_invoice);

		$total_service_invoice = $total_service_invoice + $service_invoice;
		$total_service_payment = $total_service_payment + $service_payment;
		$total_service_balance = $total_service_balance + $total_service_payment;
		
		$grand_total += $service_invoice;

	
		$service_result .='<tr>
							<td class="text-left">'.strtoupper($service_name).'</td>
							<td class="text-right">Ksh. '.number_format($service_invoice,2).'</td>
							</tr>';

			

	}
	// $undefined_payment = $this->company_financial_model->get_service_payments_total(0);

	// $service_result .='<div class="row">
	// 						<div class="col-md-7">
	// 							OTHER INCOME
	// 						</div>
	// 						<div class="col-md-5">
	// 							Ksh. '.number_format($undefined_payment,2).'
	// 						</div>
	// 					</div>';

	$service_result .='<tr>
						<td class="text-left"><b>TOTAL INCOME</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($total_service_invoice,2).'</b></td>
					</tr>';


}



					
$total_purchases = $this->company_financial_model->get_total_purchases();
$total_stock_value = $this->company_financial_model->get_stock_value();

$expenses_query = $this->company_financial_model->get_child_accounts("Expense Accounts");
$total_expenses = 0;
$expenses_list ='';

foreach($expenses_query->result() AS $key_old) 
{ 
	$account_id = $key_old->account_id;
// var_dump($account_id); die();
	$amount_value = $this->company_financial_model->get_total_expense_amount($account_id);

	$expenses_list .='<tr>
						<td class="text-left">'.strtoupper($key_old->account_name).'</td>
						<td class="text-right">'.number_format($amount_value,2).'</td>
						</tr>';
	$total_expenses += $amount_value;
}



?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | P & L</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}

			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>

      <div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align" style="padding: 5px;">
            	<strong>PROFIT AND LOSS STATEMENT</strong><br>

            	<?php
            	$search_title = $this->session->userdata('balance_sheet_title_search');

      				 if(empty($search_title))
      				 {
      				 	$search_title = "";
      				 }
      				 else
      				 {
      				 	$search_title =$search_title;
      				 }
				 echo $search_title;
            	?>

            </div>
        </div>

    	<div class="row">
        	<div style="margin: auto;max-width: 500px;">
						<div class="col-md-12" style="margin-top: 10px;">
						
						<table class="table" id="testTable">
							<tr>
								<th style="width: 100%" colspan="2"> <h6> <strong>INCOME</strong></h6> </th>
							</tr>
							<tr>
								<th style="width: 60%"> ACCOUNT </th>
								<th style="width: 40%">BALANCE</th>
							</tr>

							<tbody>
								<?php echo $service_result;?>
							</tbody>
							<tr>
								<th style="width: 100%" colspan="2"> <h6> <strong>DIRECT COSTS</strong></h6> </th>
							</tr>		
							<tr>
								<th style="width: 60%"> ACCOUNT </th>
								<th style="width: 40%">BALANCE</th>
							</tr>
							<tbody>									
																
								<?php echo number_format($total_stock_value,2);?>
							</tbody>
							<tbody>
								<tr>
									<td style="width: 60%"><strong>PURCHASES</strong></td>
									<td style="width: 40%" class="text-right"><strong style="border-top: 2px solid #000">Ksh. <?php echo number_format($total_purchases,2);?></strong></td>
								</tr>
							</tbody>
							<tbody>
								<tr>
									<td style="width: 60%"><strong>CURRENT STOCK</strong></td>
									<td style="width: 40%" class="text-right"><strong style="border-top: 2px solid #000">Ksh. <?php echo number_format($total_stock_value,2);?></strong></td>
								</tr>
							</tbody>
							<tbody>
								<tr>
									<td style="width: 60%"><strong>TOTAL GOODS SOLD</strong></td>
									<td style="width: 40%" class="text-right"><strong style="border-top: 2px solid #000">Ksh. <?php echo number_format($total_stock_value,2);?></strong></td>
								</tr>
							</tbody>
							<tr>
								<th style="width: 100%" colspan="2"> <h6> <strong>OPERATING EXPENSE</strong></h6> </th>
							</tr>
							<tr>
								<th style="width: 60%"> ACCOUNT </th>
								<th style="width: 40%">BALANCE</th>
							</tr>
							<tbody>									
																
								<?php echo $expenses_list;?>
							</tbody>
							<tbody>
								<tr>
									<td style="width: 60%"><strong>TOTAL EXPENSES</strong></td>
									<td style="width: 40%" class="text-right"><strong style="border-top: 2px solid #000">Ksh. <?php echo number_format($total_expenses,2)?></strong></td>
								</tr>
							</tbody>
							<tbody>
								<tr>
									<td style="width: 60%"><strong>NET PROFIT</strong></td>
									<td style="width: 40%" class="text-right"><strong style="border-top: 2px solid #000">Ksh. <?php echo number_format($total_stock_value+$total_service_balance + $undefined_payment - $total_expenses,2)?></strong></td>
								</tr>
							</tbody>
						</table>
						<a href="#" onclick="javascript:xport.toCSV('testTable');">XLS</a>
						</div>
            </div>
        </div>

    	<div class="row" style="font-style:italic; font-size:11px;">
        	<div class="col-sm-12">
                <div class="col-sm-10 pull-left">
                    <strong>Prepared by: </strong>
                </div>
                <div class="col-sm-2 pull-right">
                    <?php echo date('jS M Y H:i a'); ?>
                </div>
            </div>

        </div>
    </body>

</html>

<script type="text/javascript">
	var xport = {
  _fallbacktoCSV: true,
  toXLS: function(tableId, filename) {
    this._filename = (typeof filename == 'undefined') ? tableId : filename;

    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls');
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }

    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it

      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};

</script>
