<?php
$asset_query = $this->company_financial_model->get_all_fixed_categories();
$total_asset_value = 0;
$asset_list ='';
// var_dump($bank_balances_rs);die();

foreach($asset_query->result() AS $key_old) 
{ 
	$asset_category_id = $key_old->asset_category_id;
	$asset_category_name = $key_old->asset_category_name;


	$asset_category_value = $this->company_financial_model->get_category_value($asset_category_id);

		$asset_list .='<tr>
							<td class="text-left">'.strtoupper($asset_category_name).'</td>
							<td class="text-right">
							<'.number_format($asset_category_value,2).'
							</td>
							</tr>';

							$total_asset_value += $asset_category_value;	
	
	
}
		$asset_list .='<tr>
							<td class="text-left"><b>TOTAL FIXED ASSETS</b></td>
							<td class="text-right"><b class="match">'.number_format($total_asset_value,2).'</b></td>
							</tr>';



$visit_types_rs = $this->company_financial_model->get_visit_details();
$visit_results = '';
$total_balance = 0;
$total_invoices = 0;
$total_payments = 0;
$total_patients = 0;
// var_dump($bank_balances_rs);die();

if($visit_types_rs->num_rows() > 0)
{
	foreach ($visit_types_rs->result() as $key => $value) {
		# code...

		$visit_type_name = $value->visit_type_name;
		$visit_type_id = $value->visit_type_id;


		$table = 'visit';
		$where = 'visit.visit_delete = 0 AND visit_type = '.$visit_type_id.' ';
		$total_visit_type_patients = $this->company_financial_model->count_items($table,$where);

		// calculate invoiced amounts
		$report_response = $this->company_financial_model->get_visit_type_invoice($visit_type_id);

		$invoice_amount = $report_response['invoice_total'];
		$payments_value = $report_response['payments_value'];
		$balance = $report_response['balance'];

		// calculate amounts paid
		$visit_results .='<tr>
							<td class="text-left">'.strtoupper($visit_type_name).'</td>
							<td class="text-right" href="'.site_url().'visit-transactions/'.$visit_type_id.'">
								'.number_format($balance,2).'
							</td>
							</tr>';

		$total_patients = $total_patients + $total_visit_type_patients;
		$total_invoices = $total_invoices + $invoice_amount;
		$total_payments = $total_payments + $payments_value;
		$total_balance = $total_balance + $balance;
	}


	$visit_results .='<tr>
						<td class="text-left"><b>APPROPRIATION  FUND</b></td>
							<td class="text-right"><b class="match">Ksh. 0</b></td>
							<td class="text-left"><b>TOTAL ACCOUNTS PAYABLES</b></td>
							<td class="text-right"><b class="match">'.number_format($total_balance,2).'</b></td>
							</tr>';
}



$total_stock_value = $this->company_financial_model->get_stock_value();

// bank 

$query = $this->petty_cash_model->get_child_accounts("Bank");

$options2 = $query;
$bank_list = '';
$bank_total = 0;

foreach($options2->result() AS $key_old) 
{ 

	$account_id = $key_old->account_id;


	// calculate invoiced amounts


	$account_name = $key_old->account_name;

	if($account_name == "Cash Account")
	{
		//  get values of collection for the period stated

		$total_income = $this->company_financial_model->get_cash_collected($account_id,2);
		//normal payments
		$total_balance = $total_income['total_balance'];
		
	}
	else if($account_name =="Mpesa")
	{

        $total_income = $this->company_financial_model->get_cash_collected($account_id,5);
		//normal payments
		$total_balance = $total_income['total_balance'];

    }
	
	else
	{
		$report_response = $this->company_financial_model->get_account_balances($account_id);

		$total_payments = $report_response['total_payments'];
		$total_disbursed = $report_response['total_disbursed'];
		$total_balance = $report_response['total_balance'];
	}


		$bank_list .='<tr>
							<td class="text-left">'.strtoupper($key_old->account_name).'</td>
							<td class="text-right" href="'.site_url().'accounts-transactions/'.$account_id.'">
								'.number_format($total_balance,2).'
							</td>
							</tr>';

							$bank_total = $bank_total + $total_balance;
	
	
}

// var_dump($asset_list); die();
$suppliers_response = $this->company_financial_model->get_suppliers_balances();
$providers_response = $this->company_financial_model->get_providers_balances();

$capital = $this->petty_cash_model->get_account_deposit("Capital");
$amount_profit = $this->company_financial_model->get_profit_and_loss();



?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | BALANCE SHEET</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}

			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>

      <div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align" style="padding: 5px;">
            	<strong>BALANCE SHEET STATEMENT</strong><br>

            	<?php
            	
				 echo $balance_sheet_search;
            	?>

            </div>
        </div>

    	<div class="row">
        	<div style="margin: auto;max-width: 500px;">
				<div class="col-md-12">	
				<div class="row">							
					<h5> <strong>ASSETS</strong></h5>	
					<h6> <strong> FIXED ASSETS</strong></h6>							
				</div>	
				<table class="table">
					<thead>
						<th style="width: 60%"> ACCOUNT NAME </th>
						<th style="width: 40%">AMOUNT</th>
					</thead>
					
					<tbody>
						<?php echo $asset_list;?>
						<tr>
							<th class="text-left ">TOTAL FIXED ASSETS</th>
							<th class="text-right"><b class="match"><?php echo number_format($total_fixed,2);?></b></th>
						</tr>
					</tbody>
				</table>					
				</div>
				
				<div class="col-md-12">
					<div class="row">						
						<h6> <strong>CURRENT ASSETS</strong></h6>							
					</div>	
					<table class="table">
						
						
						<thead>
							<tr>
								<th class="text-left" colspan="2" >OTHER CURRENT ASSETS</th>
							</tr>
							
						</thead>
						
						<tbody>
							<?php echo $bank_balances_result?>
						</tbody>
						<thead>

							<tr>
								<th class="text-left" colspan="2" >CASH IN AT BANK AND IN HAND</th>
							</tr>
							
						</thead>

						<tbody>
							<?php echo $cash_in_bank?>
						</tbody>
					</table>
				</div>
				<div class="col-md-12">
						<table class="table">
						<tbody>
							<tr>
								<td style="width: 60%"><strong>TOTAL CURRENT ASSETS</strong></td>
								<td style="width: 40%" class="text-right"><strong style="border-top: 2px solid #000">Ksh. <?php echo number_format($total_assets + $total_fixed,2)?></strong></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-12">	
					<div class="row">							
						<h5> <strong>CURRENT LIABILITIES</strong></h5>	
						<h6> <strong>ACCOUNTS PAYABLES</strong></h6>							
					</div>	
					<table class="table">
						<thead>
							<th style="width: 60%"> ACCOUNT NAME </th>
							<th style="width: 40%">AMOUNT</th>
						</thead>
						
						<tbody>
							<tr>
								<td class="text-left">ACCOUNTS PAYABLE</td>
								<td class="text-right"><?php echo number_format($accounts_payable,2)?></td>
							</tr>
							
							<tr>
								<td class="text-left">TOTAL CURRENT LIABILITIES</td>
								<td class="text-right"><b class="match"><?php echo number_format($total_liability,2);?></b></td>
							</tr>
							
						</tbody>
					</table>					
				</div>
				<div class="col-md-12">
						<table class="table">
						<tbody>
							<tr>
								<td style="width: 60%"><strong>TOTAL LIABILITIES</strong></td>
								<td style="width: 40%" class="text-right"><strong style="border-top: 2px solid #000">Ksh. <?php echo number_format($total_liability,2)?></strong></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="col-md-12">
					<div class="row">						
						<h5> <strong>CAPITAL AND RESERVES</strong></h5>							
					</div>	
					<table class="table">
						<thead>
							<th style="width: 60%"> NAME </th>
							<th style="width: 40%">AMOUNT</th>
						</thead>
						
						<tbody>
							<tr>
			        			<td class="text-left">SHARE CAPITAL ACCOUNT </td>
								<td class="text-right"><?php echo number_format($total_share_capital,2)?></td>
							</tr>
							<tr>
								<td class="text-left">PROFIT OF THE YEAR</td>
								<td class="text-right"><?php echo number_format($current_year_earnings,2)?></td>
							</tr>
							<tr>
			        			<td class="text-left">SHARE HOLDER FUNDS</td>
										<td class="text-right"><?php echo number_format($total_liability,2)?></td>
							</tr>
						</tbody>
					</table>
				</div>

            </div>
        </div>

    	<div class="row" style="font-style:italic; font-size:11px;">
        	<div class="col-sm-12">
                <div class="col-sm-10 pull-left">
                    <strong>Prepared by: </strong>
                </div>
                <div class="col-sm-2 pull-right">
                    <?php echo date('jS M Y H:i a'); ?>
                </div>
            </div>

        </div>
    </body>

</html>
