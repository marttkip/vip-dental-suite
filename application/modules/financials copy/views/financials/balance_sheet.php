





<?php

$grand_income = 0;
$income_result = '';


$accounts_config_rs = $this->db->get('account_staging');

$patients = array();

if($accounts_config_rs->num_rows() > 0)
{
	foreach ($accounts_config_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$reference_name = $value->reference_name;

		$session_account[$reference_name] = $staing_account_id;

		
	}
}

$providers_liability_id = $session_account['providers_liability_id'];
$providers_wht_id = $session_account['providers_wht_id'];
$payroll_liability_id = $session_account['payroll_liability_id'];
$accounts_payable_id = $session_account['accounts_payable_id'];
$fixed_assets_id = $session_account['fixed_assets_id'];
$accounts_receivable_id = $session_account['accounts_receivable_id'];
$suppliers_wht_id = $session_account['suppliers_wht_id'];
$income_account_id = $session_account['income_account_id'];
$other_income_account_id = $session_account['other_income_account_id'];


$grand_income = 0;
$income_result = '';

$parent_account_id2 = $this->company_financial_model->get_parent_account_id('INCOMES');
$account_rs2 = $this->ledgers_model->get_all_child_accounts($parent_account_id2);
$balance = 0;
$total_amount = 0;
$grand_expense = 0;
$operation_result ='';
$total_operational_amount = 0;
if($account_rs2->num_rows() > 0)
{
 	foreach ($account_rs2->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;
 		 
		$expense_of_account_rs = $this->ledgers_model->get_account_ledger($account_id,1);
		// var_dump($account_rs2);die();

		if($expense_of_account_rs->num_rows() > 0)
		{

			
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
				$accountName = $value4->accountName;

				$total_income = $cr_amount-$dr_amount;
				$grand_income += $total_income;
				
				$income_result .='<tr>
										<td class="text-left">'.strtoupper($account_name).'</td>
										<td class="text-right"><a href="'.site_url().'accounting/expense-ledger/'.$account_id.'" >'.number_format($total_income,2).'</a></td>
									</tr>';

			}
			

		}
	}
}




$income_result .='<tr>
						<td class="text-left"><b>INCOME</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_income,2).'</b></td>
					</tr>';


$account_rs2 = $this->ledgers_model->get_all_child_accounts($other_income_account_id);
$balance = 0;
$total_amount = 0;
$total_operational_amount = 0;
if($account_rs2->num_rows() > 0)
{
 	foreach ($account_rs2->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;
 		 
		$expense_of_account_rs = $this->ledgers_model->get_account_ledger($account_id,1);
		// var_dump($account_rs2);die();

		if($expense_of_account_rs->num_rows() > 0)
		{

			
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
				$accountName = $value4->accountName;

				$total_other_income = $cr_amount-$dr_amount;
				$grand_other_income += $total_other_income;
				
				$income_result .='<tr>
										<td class="text-left">'.strtoupper($account_name).'</td>
										<td class="text-right"><a href="'.site_url().'accounting/expense-ledger/'.$account_id.'" >'.number_format($total_other_income,2).'</a></td>
									</tr>';

			}
			

		}
	}
}


$income_result .='<tr>
						<td class="text-left"><b>OTHER INCOME</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_other_income,2).'</b></td>
					</tr>';

$income_result .='<tr>
						<td class="text-left"><b>TOTAL INCOME</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_income+$grand_other_income,2).'</b></td>
					</tr>';


$operation_result .='<tr>
						<td class="text-left"><b>Total Operation Cost</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($total_operational_amount,2).'</b></td>
					</tr>';

// get cost of goods
$parent_account_id = $this->company_financial_model->get_parent_account_id('Cost of Goods');

$account_rs = $this->ledgers_model->get_all_child_accounts($parent_account_id);

$goods_result ='';
$grand_goods = 0;
if($account_rs->num_rows() > 0)
{
 	foreach ($account_rs->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;


 		$debit = 0;
		$credit = 0;

		$account_rs = $this->ledgers_model->get_account_ledger($account_id,1);

		if($account_rs->num_rows() > 0)
		{
			

			foreach ($account_rs->result() as $key => $value) {
				// code...
				$dr_amount = $value->dr_amount;
				$cr_amount = $value->cr_amount;
				
				$total_goods = $dr_amount-$cr_amount;
				$grand_goods += $total_goods;
				// $grand_balance += $balance;
				$goods_result .='<tr>
									<td class="text-left">'.strtoupper($account_name).'</td>
									<td class="text-right"><a href="'.site_url().'account-transactions/'.$account_id.'"  target="_blank">'.number_format($total_goods,2).'</a></td>
									</tr>';

			}
		


		}

		
	}
}
$goods_result .='<tr>
						<td class="text-left"><b>TOTAL GOODS SOLD</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_goods,2).'</b></td>
					</tr>';


$parent_account_id2 = $this->company_financial_model->get_parent_account_id('Expense Accounts');
$account_rs2 = $this->ledgers_model->get_all_child_accounts($parent_account_id2);

$grand_expense = 0;
$operation_result ='';
$total_operational_amount = 0;
if($account_rs2->num_rows() > 0)
{
 	foreach ($account_rs2->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;
 		 
		$expense_of_account_rs = $this->ledgers_model->get_account_ledger($account_id,1);
		// var_dump($account_rs2);die();

		if($expense_of_account_rs->num_rows() > 0)
		{

			
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
				$accountName = $value4->accountName;

				$total_expense = $dr_amount-$cr_amount;


				$grand_expense += $total_expense;

				
				$operation_result .='<tr>
										<td class="text-left">'.strtoupper($account_name).'</td>
										<td class="text-right"><a href="'.site_url().'accounting/expense-ledger/'.$account_id.'" >'.number_format($total_expense,2).'</a></td>
									</tr>';

			}
			

		}
	}
}


$parent_account_id2 = $this->company_financial_model->get_parent_account_id('Payroll');
$account_rs2 = $this->ledgers_model->get_all_child_accounts($parent_account_id2);

// $grand_expense = 0;
if($account_rs2->num_rows() > 0)
{
 	foreach ($account_rs2->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;
 		 
		$expense_of_account_rs = $this->ledgers_model->get_expense_account_transactions($account_id,1);
		// var_dump($account_rs2);die();

		if($expense_of_account_rs->num_rows() > 0)
		{

			
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
				$accountName = $value4->accountName;

				$total_expense = $dr_amount-$cr_amount;
				$grand_expense += $total_expense;

				
				$operation_result .='<tr>
										<td class="text-left">'.strtoupper($account_name).'</td>
										<td class="text-right"><a href="'.site_url().'accounting/expense-ledger/'.$account_id.'" >'.number_format($total_expense,2).'</a></td>
									</tr>';

			}
			

		}
	}
}



// var_dump($statement);die();

$total_profit = $grand_income - $grand_goods - $grand_expense;


?>
<?php




//  pnnda
$bank_balances_rs = $this->company_financial_model->get_account_value_by_type('Current Assets');
$bank_balances_result = '';
$total_income = 0;
// var_dump($bank_balances_rs);die();

if($bank_balances_rs->num_rows() > 0)
{
	foreach ($bank_balances_rs->result() as $key => $value) {
		# code...
		$total_amount = $value->total_amount;
		$transactionName = $value->accountName;
		$account_id = $value->account_id;
		$total_income += $total_amount;
		$bank_balances_result .='<tr>
							<td class="text-left">'.strtoupper($transactionName).'</td>
							<td class="text-right">
							<a href="'.site_url().'account-transactions/'.$account_id.'" >'.number_format($total_amount,2).'</a>
							</td>
							</tr>';
	}
	$bank_balances_result .='<tr>
							<td class="text-left"><b>TOTAL BANK BALANCE</b></td>
							<td class="text-right"><b class="match">'.number_format($total_income,2).'</b></td>
							</tr>';
}



$parent_account_id = $this->company_financial_model->get_account_id('Bank');

$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($parent_account_id);
$cash_in_bank = '';
$total_income = 0;

if($children_account->num_rows() > 0)
{
	foreach ($children_account->result() as $key => $value) {
		# code...
		$account_name = $value->account_name;
		$account_id = $value->account_id;

		$debit = 0;
		$credit = 0;

		$account_rs = $this->ledgers_model->get_account_ledger($account_id,1);

		if($account_rs->num_rows() > 0)
		{
			foreach ($account_rs->result() as $key => $value) {
				// code...
				$dr_amount = $value->dr_amount;
				// $account_name = $value->account_name;
				$cr_amount = $value->cr_amount;
				$total_amount = $dr_amount - $cr_amount;


				if($total_amount > 0)
				{
					$debit = $total_amount;
					$total_debits += $debit;
				}
				else
				{
					$credit = -$total_amount;
					$total_credits += $credit;
				}
				$total_amount = $debit - $credit;
				$total_income += $total_amount;

				$cash_in_bank .='<tr>
											<td class="text-left">'.strtoupper($account_name).' </td>
											<td class="text-right">
												<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($total_amount,2).'</a>
											</td>
										</tr>';


			}



		}

	}
	$cash_in_bank .='<tr>
							<td class="text-left"><b>TOTAL BANK BALANCE</b></td>
							<td class="text-right"><b class="match">'.number_format($total_income,2).'</b></td>
							</tr>';

}



$equity_rs = $this->company_financial_model->get_parent_accounts_by_type('Equity');


$share_capital_list = '';
$total_share_capital = 0;
if($equity_rs->num_rows() > 0)
{
	foreach ($equity_rs->result() as $key => $value) {
		# code...
		
		$parent_account_name = $value->account_name;
		$parent_account_id = $value->account_id;
	


		$child_accounts_rs = $this->company_financial_model->get_child_accounts_by_parent_id($parent_account_id);

		if($child_accounts_rs->num_rows() > 0)
		{
			$item = '<i class="fa fa-angle-right"></i> ';
		}
		else
		{
			$item = '';
		}
		$total_equity_category = 0;
	
		if($child_accounts_rs->num_rows() > 0)
		{


			$equity_asset_child = '<tbody class="collapse" id="'.$parent_account_id.'">';
			foreach ($child_accounts_rs->result() as $key => $value2) {
				# code...
				$child_account_name = $value2->account_name;
				$child_account_id = $value2->account_id;

				$account_rs = $this->ledgers_model->get_account_ledger($child_account_id,1);

				if($account_rs->num_rows() > 0)
				{
					

					foreach ($account_rs->result() as $key => $value) {
						// code...
						$dr_amount = $value->dr_amount;
						// $account_name = $value->account_name;
						$cr_amount = $value->cr_amount;
						$total_amount = $dr_amount - $cr_amount;


						if($total_amount > 0)
						{
							$debit = $total_amount;
							$total_debits += $debit;
						}
						else
						{
							$credit = -$total_amount;
							$total_credits += $credit;
						}
						$account_amount_quity = $credit-$debit;
						$total_share_capital += $account_amount_quity;
						$equity_asset_child .='<tr>
													<td class="text-left">'.strtoupper($child_account_name).' </td>
													<td class="text-right" colspan="" ><a href="'.site_url().'account-transactions/'.$child_account_id.'">'.number_format($account_amount_quity,2).'</a></td>
												</tr>';


					}
				


				}




			}
			$equity_asset_child .= '</tbody>';
		}


		// parent accounts 
		$share_capital_list .='<tr data-toggle="collapse" data-target="#'.$parent_account_id.'">
									<th class="text-left" colspan="" > '.$item.strtoupper($parent_account_name).'</th>
									<th class="text-right" colspan="" >'.number_format($total_share_capital,2).'</th>
								</tr>';

		$share_capital_list .= $equity_asset_child;
		// get all child accounts


		
	
	}



}

// var_dump($suppliers_wht_id);die();
$accounts_receivable_rs = $this->ledgers_model->get_account_ledger($suppliers_wht_id,1);

$suppliers_wht = 0;

if(!empty($accounts_receivable_rs))
{	

	foreach ($accounts_receivable_rs->result() as $key => $value) {
		// code...
		$dr_amount = $value->dr_amount;
		$cr_amount = $value->cr_amount;
		
	}

	$suppliers_wht = $cr_amount - $dr_amount;
	


}


$accounts_receivable_rs = $this->ledgers_model->get_account_ledger($providers_wht_id,1);

$providers_wht = 0;
if(!empty($accounts_receivable_rs))
{	

	foreach ($accounts_receivable_rs->result() as $key => $value) {
		// code...
		$dr_amount = $value->dr_amount;
		$cr_amount = $value->cr_amount;
		
	}

	$providers_wht = $cr_amount - $dr_amount;
	


}




$assets_rs = $this->company_financial_model->get_parent_accounts_by_type('Fixed Asset');


$fixed_asset_result = '';
$total_fixed = 0;
if($assets_rs->num_rows() > 0)
{
	foreach ($assets_rs->result() as $key => $value) {
		# code...
		
		$parent_account_name = $value->account_name;
		$parent_account_id = $value->account_id;
	

		$child_accounts_rs = $this->company_financial_model->get_child_accounts_by_parent_id($parent_account_id);

		if($child_accounts_rs->num_rows() > 0)
		{
			$item = '<i class="fa fa-angle-right"></i> ';
		}
		else
		{
			$item = '';
		}
		$total_fixed_category = 0;
		
		if($child_accounts_rs->num_rows() > 0)
		{
			$fixed_asset_child = '<tbody class="collapse" id="'.$parent_account_id.'">';
			foreach ($child_accounts_rs->result() as $key => $value2) {
				# code...
				$child_account_name = $value2->account_name;
				$child_account_id = $value2->account_id;

				$account_rs = $this->ledgers_model->get_account_ledger($child_account_id,1);

				if($account_rs->num_rows() > 0)
				{
					

					foreach ($account_rs->result() as $key => $value) {
						// code...
						$dr_amount = $value->dr_amount;
						// $account_name = $value->account_name;
						$cr_amount = $value->cr_amount;
						$total_amount = $dr_amount - $cr_amount;


						if($total_amount > 0)
						{
							$debit = $total_amount;
							$total_debits += $debit;
						}
						else
						{
							$credit = -$total_amount;
							$total_credits += $credit;
						}
						$account_amount = $debit-$credit;
						$total_fixed_category += $account_amount;
						$fixed_asset_child .='<tr>
													<td class="text-left">'.strtoupper($child_account_name).' </td>
													<td class="text-right" colspan="" ><a href="'.site_url().'account-transactions/'.$child_account_id.'">'.number_format($account_amount,2).'</a></td>
												</tr>';


					}
				


				}

				// $account_amount = $this->company_financial_model->get_account_value_by_account_id($child_account_id);
				// $total_fixed_category += $account_amount;
				// $fixed_asset_child .='<tr>
				// 							<td class="text-left" colspan="" > <i class="fa fa-arrow-right"></i> '.strtoupper($child_account_name).'</td>
				// 							<td class="text-right" colspan="" ><a href="'.site_url().'account-transactions/'.$child_account_id.'">'.number_format($account_amount,2).'</a></td>
				// 						</tr>';



			}
			$fixed_asset_child .= '</tbody>';
		}


		// parent accounts 
		$fixed_asset_result .='<tr data-toggle="collapse" data-target="#'.$parent_account_id.'">
									<th class="text-left" colspan="" > '.$item.strtoupper($parent_account_name).'</th>
									<th class="text-right" colspan="" >'.number_format($total_fixed_category,2).'</th>
								</tr>';

		$fixed_asset_result .= $fixed_asset_child;
		// get all child accounts

		$total_fixed += $total_fixed_category;
		
	
	}



}

// var_dump($assets_rs);die();



$capital_earned_rs = $this->company_financial_model->get_account_value_by_type('Asset');


// var_dump($capital_earned_rs);die();

if($capital_earned_rs->num_rows() > 0)
{
	foreach ($capital_earned_rs->result() as $key => $value) {
		# code...
		$total_amount = $value->total_amount;
		$transactionName = $value->accountName;
		$account_id = $value->account_id;
		

		$debit = 0;
		$credit = 0;
		if($total_amount > 0)
		{
			$debit = $total_amount;
			$total_debits += $debit;
		}
		else
		{
			$credit = -$total_amount;
			$total_credits += $credit;
		}

		if($transactionName == "Opening Balance")
		{
			$total_income += $total_amount;
			$bank_balances_result .='<tr>
							<td class="text-left">'.strtoupper($transactionName).'</td>
							<td class="text-right">
							<a href="'.site_url().'account-transactions/'.$account_id.'" >'.number_format($total_amount,2).'</a>
							</td>
							</tr>';

		}
		else
		{

			$total_fixed += $total_amount;
			$bank_balances_result .='<tr>
									<td class="text-left">'.strtoupper($transactionName).'</td>
									<td class="text-right">
									'.number_format($total_amount,2).'
									</td>
								</tr>';

		}
		
	}

}



$accounts_receivable_rs = $this->ledgers_model->get_account_ledger($accounts_receivable_id,1);

$$accounts_receivable = 0;
if(!empty($accounts_receivable_rs))
{	

	foreach ($accounts_receivable_rs->result() as $key => $value) {
		// code...
		$dr_amount = $value->dr_amount;
		$cr_amount = $value->cr_amount;
		$accounts_receivable = $dr_amount - $cr_amount;
	}

	if($accounts_receivable > 0)
	{
		$debit = $accounts_receivable;
		$total_debits += $debit;
	}
	else
	{
		$credit = -$accounts_receivable;
		$total_credits += $credit;
	}
	


	$accounts_receivable = $debit - $credit;
}



// var_dump($fixed_asset_result);die();
$accounts_payable_rs = $this->ledgers_model->get_account_ledger($accounts_payable_id,1);
// var_dump($accounts_receivable);die();
$debit = 0;
$credit = 0;
$accounts_payable =0;
if(!empty($accounts_payable_rs))
{

	foreach ($accounts_payable_rs->result() as $key => $value) {
		// code...
		$dr_amount = $value->dr_amount;
		$cr_amount = $value->cr_amount;
		// $accounts_payable =  $cr_amount - $dr_amount;
	}

	
	$accounts_payable  =  $cr_amount - $dr_amount;


}


$accounts_payable_rs = $this->ledgers_model->get_account_ledger($providers_liability_id,1);
// var_dump($accounts_receivable);die();
$debit = 0;
$credit = 0;
$accounts_providers ==0;
if(!empty($accounts_payable_rs))
{

	foreach ($accounts_payable_rs->result() as $key => $value) {
		// code...
		$dr_amount = $value->dr_amount;
		$cr_amount = $value->cr_amount;
		// $accounts_payable =  $cr_amount - $dr_amount;
	}

	


	$accounts_providers =  $cr_amount - $dr_amount;

}


$accounts_payable_rs = $this->ledgers_model->get_account_ledger($payroll_liability_id,1);
// var_dump($accounts_receivable);die();
$debit = 0;
$credit = 0;
if(!empty($accounts_payable_rs))
{

	foreach ($accounts_payable_rs->result() as $key => $value) {
		// code...
		$dr_amount = $value->dr_amount;
		$cr_amount = $value->cr_amount;
		// $accounts_payable =  $cr_amount - $dr_amount;
	}

	
	
	$payroll_liability  =   $cr_amount - $dr_amount;

}




// $wht_payable = $this->company_financial_model->get_total_wht_tax();
// $vat_payable = $this->company_financial_model->get_total_vat_tax();


$total_assets = $accounts_receivable+$total_income;

$total_liability = $accounts_payable + $suppliers_wht +$providers_wht + $accounts_providers + $payroll_liability;
$current_year_earnings = $total_assets + $total_fixed - $total_liability;

$search = $this->session->userdata('balance_sheet_title_search');

if(!empty($search))
{
	$balance_sheet_search = ucfirst($search);
}
else {

	
	$balance_sheet_search = 'Reporting as for: '.date("Y-01-01").' to '.date("Y-m-d");
}

$total_current_assets = $total_fixed+$total_income+$accounts_receivable;
?>

<?php
	$date_from = $this->session->userdata('date_from_general_ledger');
	$general_ledger_search_title =	$this->session->userdata('general_ledger_search_title');


	if(!empty($general_ledger_search_title))
	{
		$search_title = $general_ledger_search_title;
	}
	else 
	{
		$search_title = 'ALL TIME REPORT';
	}

?>


<style>
	td .match
	{
		border-top: #000 2px solid !important;
	}
</style>

<div class="col-md-4">

	<?php echo $this->load->view('search/search_balance_sheet','', true);?>


	<div class="text-center">
		<h4 class="box-title">Balance Sheet</h4>
		<h5 class="box-title"> <?php echo $search_title?> </h5>
		<h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
	</div>

	<div class="col-md-12">
		<div class="form-group">
	        <a href="<?php echo site_url().'print-balance-sheet'?>" target="_blank" class="btn btn-md btn-warning col-md-12"><i class="fa fa-print"></i> Print Balance Sheet</a>
	  	</div>
	    <div class="form-group">
		      <a href="<?php echo site_url().'company-financials/profit-and-loss'?>"  class="btn btn-md btn-info col-md-12"><i class="fa fa-file"></i> Income Statement</a>
		</div>
		<div class="form-group">
		      <a href="<?php echo site_url().'company-financials/trial-balance'?>"  class="btn btn-md btn-primary col-md-12"><i class="fa fa-file"></i> Trial Balance</a>
		</div>
		<div class="form-group">
		      <a href="<?php echo site_url().'company-financials/general-ledger'?>"  class="btn btn-md btn-success col-md-12"><i class="fa fa-file"></i> General Ledger</a>
		</div>
  
	</div>

   

	
</div>
<div class="col-md-8">
	

	<section class="panel">
		<div class="panel-body" style="height:85vh;overflow-y:scroll;">
    	<h4 class="box-title">Asssets</h4>
    	<!-- <h4 class="box-title">Fixed Assets</h4> -->
    	<table class="table  table-striped table-condensed" style="margin-left: 10px">

    		<thead>
				<tr>
					<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">Fixed Asset</th>
				</tr>
				<tr>
        			<th class="text-left">Account</th>
					<th class="text-right">Balance</th>
				</tr>
			</thead>
			<tbody>
					<?php
					echo $fixed_asset_result;

					?>
					
					

			</tbody>
		</table>
		<table class="table  table-striped table-condensed" style="margin-left: 10px">
			<thead>

				<tr>
					<th class="text-left" colspan="" >TOTAL FIXED ASSETS</th>
					<th class="text-right" colspan="" ><?php echo number_format($total_fixed,2)?></th>
				</tr>
			</thead>
		</table>




 
		<table class="table  table-striped table-condensed" style="margin-left: 10px">
			<thead>

				<tr>
					<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">Current Assets</th>
				</tr>
			</thead>
		</table>
		<table class="table  table-striped table-condensed" style="margin-left: 10px">
			<thead>
				<tr>
					<th class="text-left" colspan="2" >Other Current Assets</th>
				</tr>
				
			</thead>
			<tbody>
				<?php echo $bank_balances_result?>
			</tbody>
			<thead>

				<tr>
					<th class="text-left" colspan="2" >Cash in at Bank and in hand</th>
				</tr>
				
			</thead>

			<tbody>
				<?php echo $cash_in_bank?>
			</tbody>

			
			<tbody>
				<tr>
					<td class="text-left">ACCOUNTS RECEIVABLES</td>
					<td class="text-right"><a href="<?php echo site_url().'account-transactions/'.$accounts_receivable_id?>" target="_blank"><?php echo number_format($accounts_receivable,2)?></a></td>
				</tr>
				
			</tbody>
		</table>
		<table class="table  table-striped table-condensed" style="margin-left: 10px">
			<thead>

				<tr>
					<th class="text-left" colspan="" >TOTAL CURRENT ASSETS</th>
					<th class="text-right" colspan="" ><?php echo number_format($total_fixed+$total_income+$accounts_receivable,2);?></th>
				</tr>
			</thead>
		</table>


		<table class="table  table-striped table-condensed" style="margin-left: 10px">
			<thead>

				<tr>
					<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">Current Liabilities</th>
				</tr>
			</thead>
		</table>
		<table class="table  table-striped table-condensed" style="margin-left: 10px">
			
			<tbody>
				<tr>
					<td class="text-left">ACCOUNTS PAYABLES</td>
					<td class="text-right"><a href="<?php echo site_url().'account-transactions/'.$accounts_payable_id?>" target="_blank" ><?php echo number_format($accounts_payable,2)?></a> </td>
				</tr>
				<tr>
					<td class="text-left">PROVIDERS</td>
					<td class="text-right"><a href="<?php echo site_url().'account-transactions/'.$providers_liability_id?>" target="_blank"><?php echo number_format($accounts_providers,2)?></a> </td>
				</tr>
				<tr>
					<td class="text-left">PAYROLL LIABILITY</td>
					<td class="text-right"><a href="<?php echo site_url().'account-transactions/'.$payroll_liability_id?>" target="_blank" ><?php echo number_format($payroll_liability,2)?></a> </td>
				</tr>
				<tr>
					<td class="text-left">PROVIDERS TAX</td>
					<td class="text-right"><a href="<?php echo site_url().'account-transactions/'.$providers_wht_id?>" target="_blank" ><?php echo number_format($providers_wht,2)?></a> </td>
				</tr>
				<tr>
					<td class="text-left">SUPPLIERS TAX</td>
					<td class="text-right"><a href="<?php echo site_url().'account-transactions/'.$suppliers_wht_id?>" target="_blank" ><?php echo number_format($suppliers_wht,2)?></a> </td>
				</tr>
				<tr>
					 <th class="text-left">TOTAL CURRENT LIABILITIES</th>
					<td class="text-right"><b class="match"><?php echo number_format($total_liability,2);?></b></td>
				</tr>
			</tbody>
			
		</table>
		
		<table class="table  table-striped table-condensed" >
			<thead>

				
				<tr>
					<th class="text-left" colspan="" >NET ASSETS = (CURRENT ASSETS - CURRENT LIABILITIES)</th>
					<th class="text-right" colspan="" ><?php echo number_format($total_current_assets - $total_liability,2);?></th>
				</tr>
			</thead>
		</table>
		
		
		<h4 class="box-title">CAPITAL AND CAPITAL RESERVES</h4>
    	<table class="table  table-striped table-condensed" style="margin-left: 10px">
			<thead>
				<tr>
        			<th class="text-left">ACCOUNT</th>
							<th class="text-right">BALANCE</th>
				</tr>
			</thead>
			<tbody>
				<tr>
        			<td class="text-left">SHARE CAPITAL</td>
					<td class="text-right"><?php echo number_format(0,2)?></td>
				</tr>
				<?php echo $share_capital_list;?>
				<tr>
        			<td class="text-left">RETAINED EARNINGS</td>
					<td class="text-right"><a href="<?php echo site_url().'company-financials/profit-and-loss'?>" ><?php echo number_format($total_profit,2)?></a></td>
				</tr>
				<tr>
					<td class="text-left">TOTAL SHARE HOLDERS FUNDS</td>
					<td class="text-right"><?php echo number_format($total_share_capital+$total_profit,2)?></td>
				</tr>
				<tr>
					<td class="text-left">TOTAL SHARE HOLDER FUNDS PLUS LIABILITY</td>
					<td class="text-right"><?php echo number_format($total_share_capital+$total_profit+$total_liability,2)?></td>
				</tr>
				
			</tbody>
		</table>
    </div>
	</section>
</div>

