
<?php


$accounts_config_rs = $this->db->get('account_staging');

$patients = array();

if($accounts_config_rs->num_rows() > 0)
{
	foreach ($accounts_config_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$reference_name = $value->reference_name;

		$session_account[$reference_name] = $staing_account_id;

		
	}
}

$providers_liability_id = $session_account['providers_liability_id'];
$providers_wht_id = $session_account['providers_wht_id'];
$payroll_liability_id = $session_account['payroll_liability_id'];
$accounts_payable_id = $session_account['accounts_payable_id'];
$fixed_assets_id = $session_account['fixed_assets_id'];
$accounts_receivable_id = $session_account['accounts_receivable_id'];
$suppliers_wht_id = $session_account['suppliers_wht_id'];
$income_account_id = $session_account['income_account_id'];
$other_income_account_id = $session_account['other_income_account_id'];


$grand_income = 0;
$income_result = '';

$parent_account_id2 = $this->company_financial_model->get_parent_account_id('INCOMES');
$account_rs2 = $this->ledgers_model->get_all_child_accounts($parent_account_id2);
$balance = 0;
$total_amount = 0;
$grand_expense = 0;
$operation_result ='';
$total_operational_amount = 0;
if($account_rs2->num_rows() > 0)
{
 	foreach ($account_rs2->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;
 		 
		$expense_of_account_rs = $this->ledgers_model->get_account_ledger($account_id,1);
		// var_dump($account_rs2);die();

		if($expense_of_account_rs->num_rows() > 0)
		{

			
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
				$accountName = $value4->accountName;

				$total_income = $cr_amount-$dr_amount;
				$grand_income += $total_income;
				
				$income_result .='<tr>
										<td class="text-left">'.strtoupper($account_name).'</td>
										<td class="text-right"><a href="'.site_url().'account-transactions/'.$account_id.'" >'.number_format($total_income,2).'</a></td>
									</tr>';

			}
			

		}
	}
}




$income_result .='<tr>
						<td class="text-left"><b>INCOME</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_income,2).'</b></td>
					</tr>';

// var_dump($other_income_account_id);die();

// $parent_account_id2 = $this->company_financial_model->get_parent_account_id('INCOMES');
$account_rs2 = $this->ledgers_model->get_all_child_accounts($other_income_account_id);
$balance = 0;
$total_amount = 0;
$total_operational_amount = 0;
$grand_other_income =0;
if($account_rs2->num_rows() > 0)
{
 	foreach ($account_rs2->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;
 		 
		$expense_of_account_rs = $this->ledgers_model->get_account_ledger($account_id,1);
		// var_dump($account_rs2);die();

		if($expense_of_account_rs->num_rows() > 0)
		{

			
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
				$accountName = $value4->accountName;

				$total_other_income = $cr_amount-$dr_amount;
				$grand_other_income += $total_other_income;
				
				$income_result .='<tr>
										<td class="text-left">'.strtoupper($account_name).'</td>
										<td class="text-right"><a href="'.site_url().'account-transactions/'.$account_id.'" >'.number_format($total_other_income,2).'</a></td>
									</tr>';

			}
			

		}
	}
}


$income_result .='<tr>
						<td class="text-left"><b>OTHER INCOME</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_other_income,2).'</b></td>
					</tr>';

$income_result .='<tr>
						<td class="text-left"><b>TOTAL INCOME</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_income+$grand_other_income,2).'</b></td>
					</tr>';


$operation_result .='<tr>
						<td class="text-left"><b>Total Operation Cost</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($total_operational_amount,2).'</b></td>
					</tr>';

// get cost of goods
$parent_account_id = $this->company_financial_model->get_parent_account_id('Cost of Goods');

$account_rs = $this->ledgers_model->get_all_child_accounts($parent_account_id);

$goods_result ='';
$grand_goods = 0;
if($account_rs->num_rows() > 0)
{
 	foreach ($account_rs->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;


 		$debit = 0;
		$credit = 0;

		$account_rs = $this->ledgers_model->get_account_ledger($account_id,1);

		if($account_rs->num_rows() > 0)
		{
			

			foreach ($account_rs->result() as $key => $value) {
				// code...
				$dr_amount = $value->dr_amount;
				$cr_amount = $value->cr_amount;
				
				$total_goods = $dr_amount-$cr_amount;
				$grand_goods += $total_goods;
				// $grand_balance += $balance;
				$goods_result .='<tr>
									<td class="text-left">'.strtoupper($account_name).'</td>
									<td class="text-right"><a href="'.site_url().'account-transactions/'.$account_id.'"  target="_blank">'.number_format($total_goods,2).'</a></td>
									</tr>';

			}
		


		}

		
	}
}
$goods_result .='<tr>
						<td class="text-left"><b>TOTAL GOODS SOLD</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_goods,2).'</b></td>
					</tr>';


$parent_account_id2 = $this->company_financial_model->get_parent_account_id('Expense Accounts');
$account_rs2 = $this->ledgers_model->get_all_child_accounts($parent_account_id2);

$grand_expense = 0;
$operation_result ='';
$total_operational_amount = 0;
if($account_rs2->num_rows() > 0)
{
 	foreach ($account_rs2->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;
 		 
		$expense_of_account_rs = $this->ledgers_model->get_account_ledger($account_id,1);
		// var_dump($account_rs2);die();

		if($expense_of_account_rs->num_rows() > 0)
		{

			
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
				$accountName = $value4->accountName;

				$total_expense = $dr_amount-$cr_amount;


				$grand_expense += $total_expense;

				
				$operation_result .='<tr>
										<td class="text-left">'.strtoupper($account_name).'</td>
										<td class="text-right"><a href="'.site_url().'account-transactions/'.$account_id.'" >'.number_format($total_expense,2).'</a></td>
									</tr>';

			}
			

		}
	}
}


$parent_account_id2 = $this->company_financial_model->get_parent_account_id('Payroll');
$account_rs2 = $this->ledgers_model->get_all_child_accounts($parent_account_id2);

// $grand_expense = 0;
if($account_rs2->num_rows() > 0)
{
 	foreach ($account_rs2->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;
 		 
		$expense_of_account_rs = $this->ledgers_model->get_expense_account_transactions($account_id,1);
		// var_dump($account_rs2);die();

		if($expense_of_account_rs->num_rows() > 0)
		{

			
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
				$accountName = $value4->accountName;

				$total_expense = $dr_amount-$cr_amount;
				$grand_expense += $total_expense;

				
				$operation_result .='<tr>
										<td class="text-left">'.strtoupper($account_name).'</td>
										<td class="text-right"><a href="'.site_url().'account-transactions/'.$account_id.'" >'.number_format($total_expense,2).'</a></td>
									</tr>';

			}
			

		}
	}
}

$operation_result .='<tr>
						<td class="text-left"><b>TOTAL OPERATING EXPENSE</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_expense,2).'</b></td>
					</tr>';

$statement = $this->session->userdata('income_statement_title_search');

// var_dump($statement);die();

if(!empty($statement))
{
	$checked = $statement;
}
else {
	$checked = 'Reporting period: '.date('M j, Y', strtotime(date('Y-01-01'))).' to ' .date('M j, Y', strtotime(date('Y-m-d')));
}


$closing_stock =  $this->company_financial_model->get_opening_stock_value();


?>

<?php
	$date_from = $this->session->userdata('date_from_general_ledger');
	$general_ledger_search_title =	$this->session->userdata('general_ledger_search_title');


	if(!empty($general_ledger_search_title))
	{
		$search_title = $general_ledger_search_title;
	}
	else 
	{
		$search_title = 'ALL TIME REPORT';
	}

?>


<div class="col-md-4">
	<?php echo $this->load->view('search/search_profit_and_loss','', true);?>

	<div class="text-center">
		<h3 class="box-title">Income Statement</h3>
		<h5 class="box-title"> <?php echo $search_title?></h5>
		<h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
	</div>

	<div class="form-group">
	<?php 
			$search_status = $this->session->userdata('income_statement_search');

			if(!empty($search_status))
			{
				echo '<a href="'.site_url().'financials/company_financial/close_income_statement_search" class="btn btn-sm btn-warning">Close search</a>';
			}
			?>
	</div>
	<div class="form-group">
		  <a href="<?php echo site_url().'print-income-statement'?>" target="_blank" class="btn btn-md btn-warning col-md-12"><i class="fa fa-print"></i> Print P&L</a>
	</div>
	<div class="form-group">
	      <a href="<?php echo site_url().'company-financials/balance-sheet'?>"  class="btn btn-md btn-info col-md-12"><i class="fa fa-file"></i> Balance Sheet</a>
	</div>
	<div class="form-group">
	      <a href="<?php echo site_url().'company-financials/trial-balance'?>"  class="btn btn-md btn-primary col-md-12"><i class="fa fa-file"></i> Trial Balance</a>
	</div>
	<div class="form-group">
	      <a href="<?php echo site_url().'company-financials/general-ledger'?>"  class="btn btn-md btn-success col-md-12"><i class="fa fa-file"></i> General Ledger</a>
	</div>
	
</div>
<div class="col-md-8">

	<section class="panel">
			
			
				
			<!-- /.box-header -->
			<div class="panel-body" style="height:80vh;overflow-y:scroll;">

				<h5 class="box-title" style="background-color:#3c8dbc;color:#fff;padding:5px;">INCOME</h5>
		    	<table class="table  table-striped table-condensed">
					<thead>
						<tr>
		        			<th class="text-left">Account</th>
							<th class="text-right">Balance</th>
						</tr>
					</thead>
					<tbody>
						<?php echo $income_result;?>
					</tbody>
				</table>


				<h5 class="box-title" style="background-color:#3c8dbc;color:#fff;padding:5px;">DIRECT COSTS</h5>
		    	<table class="table  table-striped table-condensed">
					<thead>
						<tr>
		        			<th class="text-left">Account</th>
							<th class="text-right">Balance</th>
						</tr>
					</thead>
					<tbody>
						

						
						<?php echo $goods_result?>
						

						
					</tbody>
				</table>

				<h5 class="box-title" style="background-color:#3c8dbc;color:#fff;padding:5px;">OPERATING EXPENSE</h5>
		    	<table class="table  table-striped table-condensed">
					<thead>
						<tr>
		        			<th class="text-left">Account</th>
							<th class="text-right">Balance</th>
						</tr>
					</thead>
					<tbody>
						
						<?php echo $operation_result;?>

					</tbody>
				</table>

				<!-- <h5 class="box-title">INTEREST (INCOME), EXPENSE & TAXES</h5> -->
		    	<table class="table  table-striped table-condensed">
					<thead>
						<tr>
		        			<th class="text-left"></th>
							<th class="text-right"></th>
						</tr>
					</thead>
					<tbody>

						<tr>
		        			<th class="text-left"><strong>NET PROFIT</strong></th>
							<th class="text-right"><?php echo number_format($grand_income - $grand_goods - $grand_expense,2)?></th>
						</tr>
					</tbody>
				</table>
	    	</div>
	</section>
</div>

