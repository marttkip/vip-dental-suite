<?php echo $this->load->view('search/search_general_ledger','', true);?>


<div class="text-center">
	<h3 class="box-title">General Ledger</h3>
	<h5 class="box-title">Reporting period: January 01, 2019 to February 14, 2019</h5>
	<h6 class="box-title">Created February 14, 2019</h6>
</div>

<div class="box">
    <div class="panel-body">
    	<!-- <h3 class="box-title">Revenue</h3> -->
    	<table class="table  table-striped table-condensed">
			<thead>
				<tr>
        			<th >ACCOUNT</th>
        			<th >TYPE</th>
        			<th >DATE</th>
					<th >NUM</th>
					<th >NAME</th>
					<th >MEMO</th>
					<th >SPLIT</th>
					<th >DR</th>
					<th >CR</th>
				</tr>
			</thead>
			<tbody>

				<?php
				$changed = '';


				 $accounts = $this->ledgers_model->get_all_parent_accounts();
				 $grand_amount = 0;
				 $grand_balance = 0;
				 $grand_dr = 0;
				 $grand_cr = 0;
                 if($accounts->num_rows() > 0)
                 {
                     foreach($accounts->result() as $row):
                         // $company_name = $row->company_name;
                         $parent_account_name = $row->account_name;
                         $parent_account_id = $row->account_id;


                         
                         // if parent account == bank 
						if($parent_account_name == 'Bank')
						{

							$changed .= '<tr>
						        			<th  colspan="9">'.strtoupper($parent_account_name).'</th>
									   </tr>';

							 $account_rs = $this->ledgers_model->get_all_child_accounts($parent_account_id);
							$balance = 0;
							$total_amount = 0;

							$total_dr_amount = 0;
							$total_cr_amount = 0;
							$dr_amount = 0;
							$cr_amount = 0;
	                        if($account_rs->num_rows() > 0)
	                        {
	                         	foreach ($account_rs->result() as $key => $value) 
	                         	{
	                         		# code...

	                         		$account_name = $value->account_name;
	                         		$account_id = $value->account_id;
	                         		 $changed .= '<tr>
									        			<td  colspan="9">'.strtoupper($account_name).'</td>
												   </tr>';

									$items_of_account_rs = $this->ledgers_model->get_account_transactions($account_id);
									

									if($items_of_account_rs->num_rows() > 0)
									{
										
										foreach ($items_of_account_rs->result() as $key => $value2) 
										{
											# code...
											// get all transactions

											$dr_amount = $value2->dr_amount;
											$cr_amount = $value2->cr_amount;
											$transactionDescription = $value2->transactionDescription;
											$referenceCode = $value2->referenceCode;
											$transactionDate = $value2->transactionDate;
											$accountName = $value2->accountName;
											$accountId = $value2->accountId;
											$transactionClassification = $value2->transactionClassification;

											if($dr_amount > 0)
											{
												$amount = $dr_amount;
											}

											else if($cr_amount > 0)
											{
												$amount = -$cr_amount;
											}
											else
											{
												$amount = 0;
											}

											if($dr_amount < 0)
											{
												$cr_amount = -$dr_amount;
												$dr_amount = 0;

											}
											$total_dr_amount += $dr_amount;
											$total_cr_amount += $cr_amount;
											$balance += $amount;
											$total_amount += $amount;
											$grand_dr += $dr_amount;
				 							$grand_cr += $cr_amount;


											$changed .= '<tr>
															<td ></td>
										        			<td > '.$transactionClassification.'</td>
										        			<td > '.$transactionDate.'</td>
															<td > '.$referenceCode.'</td>
															<td > '.$transactionDescription.'</td>
										        			<td ></td>
															<td >'.$transactionDescription.'</td>
															<td class="center-align">'.number_format($dr_amount,2).'</td>
															<td class="center-align">'.number_format($cr_amount,2).'</td>
														
															
														</tr>';


										}
									

										

									}
								}
							}
						}

						if($parent_account_name == 'Accounts Receivables')
						{

							$changed .= '<tr>
						        			<th  colspan="9">'.strtoupper($parent_account_name).'</th>
									   	</tr>';
							$income_account_rs = $this->ledgers_model->get_receivables_transactions();

							$balance = 0;
							$total_amount = 0;

							$total_dr_amount = 0;
							$total_cr_amount = 0;
							$dr_amount = 0;
							$cr_amount = 0;
							if($income_account_rs->num_rows() > 0)
							{
								

								foreach ($income_account_rs->result() as $key => $value3) {
									# code...
									// get all transactions

									$dr_amount = $value3->dr_amount;
									$cr_amount = $value3->cr_amount;
									$transactionDescription = $value3->transaction_description;
									$referenceCode = $value3->reference_code;
									$transactionDate = $value3->transaction_date;
									$accountName = $value3->account_name;
									$transactionCategory = $value3->transactionCategory;
									$parent_service = $value3->parent_service;
									$patient_othernames = $value3->patient_othernames;
									$patient_surname = $value3->patient_surname;

									if($dr_amount > 0)
									{
										$amount = $dr_amount;
									}

									else if($cr_amount > 0)
									{
										$amount = -$cr_amount;
									}
									else
									{
										$amount = 0;
									}


									if($dr_amount < 0)
									{
										$cr_amount = -$dr_amount;
										$dr_amount = 0;

									}

									$balance += $amount;
									// var_dump($balance);die();

									// $total_amount += $amount;
									// $grand_balance += $balance;
									$total_dr_amount += $dr_amount;
									$total_cr_amount += $cr_amount;
									$grand_dr += $dr_amount;
				 					$grand_cr += $cr_amount;

									$changed .= '<tr>
													<td ></td>
								        			<td > '.$transactionCategory.'</td>
								        			<td > '.$transactionDate.'</td>
													<td > '.$referenceCode.'</td>
													<td > '.$patient_othernames.' </td>
								        			<td ></td>
													<td >'.$parent_service.'</td>
													<td class="center-align">'.number_format($dr_amount,2).'</td>
													<td class="center-align">'.number_format($cr_amount,2).'</td>
												
													
												</tr>';

								}

							

							}
							$grand_amount += $total_amount;
							$grand_balance += $balance;
							

						}
						if($parent_account_name == 'INCOMES')
						{


							$changed .= '<tr>
						        			<th  colspan="9">'.strtoupper($parent_account_name).'</th>
									   	</tr>';
							$revenue_rs = $this->ledgers_model->get_all_services();

							$balance = 0;
							$total_amount = 0;

							$total_dr_amount = 0;
							$total_cr_amount = 0;
							$dr_amount = 0;
							$cr_amount = 0;
							if($revenue_rs->num_rows() > 0)
							{
								

								// var_dump($revenue_rs->result());die();
								foreach ($revenue_rs->result() as $key => $value5) {
									# code...
									// get all transactions
									$service_id = $value5->service_id;
									$service_name = $value5->service_name;



									// update the service Details
									$revenue_items_rs = $this->ledgers_model->get_receivables_transactions_per_service($service_id);


									if($revenue_items_rs->num_rows() > 0)
									{
										
										// var_dump($revenue_items_rs->result());die();
										foreach ($revenue_items_rs->result() as $key => $value6) 
										{


											$dr_amount = $value6->dr_amount;
											$cr_amount = $value6->cr_amount;
											$transactionDescription = '';//$value6->transactionDescription;
											$referenceCode = '';//$value6->referenceCode;
											$transactionDate = '';//$value6->transactionDate;
											$accountName = '';//$value6->accountName;
											$transactionCategory = $value6->transactionCategory;

											// if($dr_amount > 0)
											// {
											// 	$amount = $dr_amount;
											// }

											// else if($cr_amount > 0)
											// {
											// 	$amount = -$cr_amount;
											// }
											// else
											// {
											// 	$amount = 0;
											// }
											// $amount = -$amount;


											if($cr_amount < 0)
											{
												$dr_amount = -$dr_amount;
												$cr_amount = 0;
											}

											if($dr_amount < 0)
											{
												$cr_amount = -$dr_amount;
												$dr_amount = 0;
											}

											$balance += $amount;
											$total_amount += $amount;

											$grand_balance += $balance;

											$total_dr_amount += $cr_amount;
											$total_cr_amount += $dr_amount;
											$grand_dr += $cr_amount;
				 							$grand_cr += $dr_amount;

											$changed .= '<tr>
															<td ></td>
										        			<td > '.$service_name.'</td>
										        			<td > '.$transactionDate.'</td>
															<td > '.$referenceCode.'</td>
															<td > '.$transactionDescription.'</td>
										        			<td ></td>
															<td ></td>
															<td class="center-align">'.number_format($cr_amount,2).'</td>

															<td class="center-align">'.number_format($dr_amount,2).'</td>
														</tr>';

										}
										
										
									}
								}

							}



						}

						// var_dump($grand_dr);die();
						if($parent_account_name == 'Expense Accounts')
						{
							$changed .= '<tr>
						        			<th  colspan="9">'.strtoupper($parent_account_name).'</th>
									   	</tr>';

							$account_rs = $this->ledgers_model->get_all_child_accounts($parent_account_id);
							$balance = 0;
							$total_amount = 0;

							$total_dr_amount = 0;
							$total_cr_amount = 0;
							$dr_amount = 0;
							$cr_amount = 0;
	                        if($account_rs->num_rows() > 0)
	                        {
	                         	foreach ($account_rs->result() as $key => $value) 
	                         	{
	                         		# code...

	                         		$account_name = $value->account_name;
	                         		$account_id = $value->account_id;
	                         		 
									$expense_of_account_rs = $this->ledgers_model->get_account_transactions($account_id);


									if($expense_of_account_rs->num_rows() > 0)
									{

										$changed .= '<tr>
									        			<td  colspan="9">'.strtoupper($account_name).'</td>
												   </tr>';
										
										// var_dump($expense_of_account_rs->result());die();
										foreach ($expense_of_account_rs->result() as $key => $value4) {
											# code...
											// get all transactions

											$dr_amount = $value4->dr_amount;
											$cr_amount = $value4->cr_amount;
											$transactionDescription = $value4->transactionDescription;
											$referenceCode = $value4->referenceCode;
											$transactionDate = $value4->transactionDate;
											$accountName = $value4->accountName;
											$transactionCategory = $value4->transactionCategory;

											if($dr_amount > 0)
											{
												$amount = $dr_amount;
											}

											else if($cr_amount > 0)
											{
												$amount = -$cr_amount;
											}
											else
											{
												$amount = 0;
											}

											if($dr_amount < 0)
											{
												$cr_amount = -$dr_amount;
												$dr_amount = 0;

											}
											$balance += $amount;
											$total_amount += $amount;

											$total_dr_amount += $dr_amount;
											$total_cr_amount += $cr_amount;
											$grand_dr += $dr_amount;
				 							$grand_cr += $cr_amount;

											// $grand_balance += $balance;

											$changed .= '<tr>
															<td ></td>
										        			<td > '.$transactionCategory.'</td>
										        			<td > '.$transactionDate.'</td>
															<td > '.$referenceCode.'</td>
															<td > '.$transactionDescription.'</td>
										        			<td ></td>
															<td ></td>
															<td class="center-align">'.number_format($dr_amount,2).'</td>
															<td class="center-align">'.number_format($cr_amount,2).'</td>
														</tr>';

										}
										

									}
								}
							}



						}
							if($parent_account_name == 'Payroll')
						{
							$changed .= '<tr>
						        			<th  colspan="9">'.strtoupper($parent_account_name).'</th>
									   	</tr>';

							$account_rs = $this->ledgers_model->get_all_child_accounts($parent_account_id);
							$balance = 0;
							$total_amount = 0;

							$total_dr_amount = 0;
							$total_cr_amount = 0;
							$dr_amount = 0;
							$cr_amount = 0;
	                        if($account_rs->num_rows() > 0)
	                        {
	                         	foreach ($account_rs->result() as $key => $value) 
	                         	{
	                         		# code...

	                         		$account_name = $value->account_name;
	                         		$account_id = $value->account_id;
	                         		 
									$expense_of_account_rs = $this->ledgers_model->get_expense_account_transactions($account_id);


									if($expense_of_account_rs->num_rows() > 0)
									{

										$changed .= '<tr>
									        			<td  colspan="9">'.strtoupper($account_name).'</td>
												   </tr>';
									
										// var_dump($expense_of_account_rs->result());die();
										foreach ($expense_of_account_rs->result() as $key => $value4) {
											# code...
											// get all transactions

											$dr_amount = $value4->dr_amount;
											$cr_amount = $value4->cr_amount;
											$transactionDescription = $value4->transactionDescription;
											$referenceCode = $value4->referenceCode;
											$transactionDate = $value4->transactionDate;
											$accountName = $value4->accountName;
											$transactionCategory = $value4->transactionCategory;

											if($dr_amount > 0)
											{
												$amount = $dr_amount;
											}

											else if($cr_amount > 0)
											{
												$amount = -$cr_amount;
											}
											else
											{
												$amount = 0;
											}

											$balance += $amount;
											$total_amount += $amount;

											$grand_balance += $balance;
											$total_dr_amount += $dr_amount;
											$total_cr_amount += $cr_amount;
											$grand_dr += $dr_amount;
				 							$grand_cr += $cr_amount;

											$changed .= '<tr>
															<td ></td>
										        			<td > '.$transactionCategory.'</td>
										        			<td > '.$transactionDate.'</td>
															<td > '.$referenceCode.'</td>
															<td > '.$transactionDescription.'</td>
										        			<td ></td>
															<td ></td>
															<td class="center-align">'.number_format($amount,2).'</td>
										        			<td class="center-align">'.number_format($balance,2).'</td>
														</tr>';

										}
										

									}
								}
							}



						}


						if($parent_account_name == 'Accounts Payable')
						{
							$changed .= '<tr>
						        			<th  colspan="9">'.strtoupper($parent_account_name).'</th>
									   	</tr>';
							$income_account_rs = $this->ledgers_model->get_payable_transactions();

							$balance = 0;
							$total_amount = 0;

							$total_dr_amount = 0;
							$total_cr_amount = 0;
							$dr_amount = 0;
							$cr_amount = 0;
							if($income_account_rs->num_rows() > 0)
							{
								
								foreach ($income_account_rs->result() as $key => $value3) {
									# code...
									// get all transactions

									$dr_amount = $value3->dr_amount;
									$cr_amount = $value3->cr_amount;
									$transactionDescription = '';//$value3->transaction_description;
									$referenceCode = '';//$value3->reference_code;
									$transactionDate = $value3->transactionDate;
									$accountName = '';//$value3->account_name;
									$transactionCategory = $value3->transactionCategory;

									if($dr_amount > 0)
									{
										$amount = -$dr_amount;
									}

									else if($cr_amount > 0)
									{
										$amount = +$cr_amount;
									}
									else
									{
										$amount = 0;
									}

									$balance += $amount;
									$total_amount += $amount;
									$grand_balance += $balance;
									$total_dr_amount += $dr_amount;
									$total_cr_amount += $cr_amount;
									$grand_dr += $dr_amount;
				 					$grand_cr += $cr_amount;

									$changed .= '<tr>
													<td ></td>
								        			<td > '.$transactionCategory.'</td>
								        			<td > '.$transactionDate.'</td>
													<td > '.$referenceCode.'</td>
													<td > '.$transactionDescription.'</td>
								        			<td ></td>
													<td ></td>
													<td class="center-align">'.number_format($amount,2).'</td>
								        			<td class="center-align">'.number_format($balance,2).'</td>
												
													
												</tr>';

								}

								
							}
							



						}
						if($parent_account_name == 'Payroll Liability')
						{

							$changed .= '<tr>
						        			<th  colspan="9">'.strtoupper($parent_account_name).'</th>
									   	</tr>';
							$income_account_rs = $this->ledgers_model->get_payroll_transactions();

							$balance = 0;
							$total_amount = 0;

							$total_dr_amount = 0;
							$total_cr_amount = 0;
							$dr_amount = 0;
							$cr_amount = 0;
							if($income_account_rs->num_rows() > 0)
							{
								
								foreach ($income_account_rs->result() as $key => $value3) {
									# code...
									// get all transactions

									$dr_amount = $value3->dr_amount;
									$cr_amount = $value3->cr_amount;
									$transactionDescription = '';//$value3->transaction_description;
									$referenceCode = '';//$value3->reference_code;
									$transactionDate = $value3->transactionDate;
									$accountName = '';//$value3->account_name;
									$transactionCategory = $value3->transactionCategory;

									if($dr_amount > 0)
									{
										$amount = -$dr_amount;
									}

									else if($cr_amount > 0)
									{
										$amount = +$cr_amount;
									}
									else
									{
										$amount = 0;
									}

									$balance += $amount;
									$total_amount += $amount;
									$grand_balance += $balance;
									$total_dr_amount += $dr_amount;
									$total_cr_amount += $cr_amount;

									$grand_dr += $dr_amount;
				 					$grand_cr += $cr_amount;
									$changed .= '<tr>
													<td ></td>
								        			<td > '.$transactionCategory.'</td>
								        			<td > '.$transactionDate.'</td>
													<td > '.$referenceCode.'</td>
													<td > '.$transactionDescription.'</td>
								        			<td ></td>
													<td ></td>
													<td class="center-align">'.number_format($amount,2).'</td>
								        			<td class="center-align">'.number_format($balance,2).'</td>
												
													
												</tr>';

								}

								

							}
							



						}
						if($parent_account_name == 'Cost of Goods')
						{
							$changed .= '<tr>
						        			<th  colspan="9">'.strtoupper($parent_account_name).'</th>
									   	</tr>';

							$account_rs = $this->ledgers_model->get_all_child_accounts($parent_account_id);
							$balance = 0;
							$total_amount = 0;

							$total_dr_amount = 0;
							$total_cr_amount = 0;
							$dr_amount = 0;
							$cr_amount = 0;
	                        if($account_rs->num_rows() > 0)
	                        {
	                         	foreach ($account_rs->result() as $key => $value) 
	                         	{
	                         		# code...

	                         		$account_name = $value->account_name;
	                         		$account_id = $value->account_id;
	                         		 $changed .= '<tr>
									        			<td  colspan="9">'.strtoupper($account_name).'</td>
												   </tr>';

									$expense_of_account_rs = $this->ledgers_model->get_account_transactions($account_id);

										// var_dump($parent_account_id);die();
									if($expense_of_account_rs->num_rows() > 0)
									{
										
										// var_dump($expense_of_account_rs->result());die();
										foreach ($expense_of_account_rs->result() as $key => $value4) {
											# code...
											// get all transactions

											$dr_amount = $value4->dr_amount;
											$cr_amount = $value4->cr_amount;
											$transactionDescription = $value4->transactionDescription;
											$referenceCode = $value4->referenceCode;
											$transactionDate = $value4->transactionDate;
											$accountName = $value4->accountName;
											$transactionCategory = $value4->transactionCategory;

											if($dr_amount > 0)
											{
												$amount = $dr_amount;
											}

											else if($cr_amount > 0)
											{
												$amount = -$cr_amount;
											}
											else
											{
												$amount = 0;
											}

											$balance += $amount;
											$total_amount += $amount;

											$total_dr_amount += $dr_amount;
											$total_cr_amount += $cr_amount;
											$grand_dr += $dr_amount;
				 							$grand_cr += $cr_amount;

											// $grand_balance += $balance;

											$changed .= '<tr>
															<td ></td>
										        			<td > '.$transactionCategory.'</td>
										        			<td > '.$transactionDate.'</td>
															<td > '.$referenceCode.'</td>
															<td > '.$transactionDescription.'</td>
										        			<td ></td>
															<td ></td>
															<td class="center-align">'.number_format($amount,2).'</td>
										        			<td class="center-align">'.number_format($balance,2).'</td>
														</tr>';

										}
										$grand_amount += $total_amount;
										$changed .= '<tr>
														<th >TOTAL '.strtoupper($account_name).'</th>
									        			<th ></th>
									        			<th ></th>
														<th ></th>
														<th ></th>
									        			<th ></th>
														<th ></th>
														<th class="center-align">'.number_format($total_dr_amount,2).'</th>
									        			
														<th class="center-align">'.number_format($total_cr_amount,2).'</th>
														
													</tr>';

									}
								}
							}



						}
						if($parent_account_name == 'Other Incomes')
						{

							// var_dump($parent_account_name);die();
							$changed .= '<tr>
						        			<th  colspan="9">'.strtoupper($parent_account_name).'</th>
									   	</tr>';

							$account_rs = $this->ledgers_model->get_all_child_accounts($parent_account_id);
							$balance = 0;
							$total_amount = 0;

							$total_dr_amount = 0;
							$total_cr_amount = 0;
							$dr_amount = 0;
							$cr_amount = 0;
	                        if($account_rs->num_rows() > 0)
	                        {
	                        	

	                         	foreach ($account_rs->result() as $key => $value) 
	                         	{
	                         		# code...

	                         		$account_name = $value->account_name;
	                         		$account_id = $value->account_id;
	                         		 $changed .= '<tr>
									        			<td  colspan="9">'.strtoupper($account_name).'</td>
												   </tr>';

									$expense_of_account_rs = $this->ledgers_model->get_account_transactions($account_id);

										// var_dump($parent_account_id);die();
									if($expense_of_account_rs->num_rows() > 0)
									{
										$balance = 0;
										$total_amount = 0;
										$total_dr_amount = 0;
										$total_cr_amount = 0;
										// var_dump($expense_of_account_rs->result());die();
										foreach ($expense_of_account_rs->result() as $key => $value4) {
											# code...
											// get all transactions

											$dr_amount = $value4->dr_amount;
											$cr_amount = $value4->cr_amount;
											$transactionDescription = $value4->transactionDescription;
											$referenceCode = $value4->referenceCode;
											$transactionDate = $value4->transactionDate;
											$accountName = $value4->accountName;
											$transactionCategory = $value4->transactionCategory;

											if($dr_amount > 0)
											{
												$amount = $dr_amount;
											}

											else if($cr_amount > 0)
											{
												$amount = -$cr_amount;
											}
											else
											{
												$amount = 0;
											}

											$balance += $amount;
											$total_amount += $amount;

											$grand_balance += $balance;

											$total_dr_amount += $dr_amount;
											$total_cr_amount += $cr_amount;
											$grand_dr += $dr_amount;
				 							$grand_cr += $cr_amount;

											$changed .= '<tr>
															<td ></td>
										        			<td > '.$transactionCategory.'</td>
										        			<td > '.$transactionDate.'</td>
															<td > '.$referenceCode.'</td>
															<td > '.$transactionDescription.'</td>
										        			<td ></td>
															<td ></td>
															<td class="center-align">'.number_format($amount,2).'</td>
										        			<td class="center-align">'.number_format($balance,2).'</td>
														</tr>';

										}
										

									}
								}
							}

						}
						if($parent_account_name == 'Assets')
						{

							// var_dump($parent_account_name);die();
							$changed .= '<tr>
						        			<th  colspan="9">'.strtoupper($parent_account_name).'</th>
									   	</tr>';

							$account_rs = $this->ledgers_model->get_all_child_accounts($parent_account_id);
							$balance = 0;
							$total_amount = 0;

							$total_dr_amount = 0;
							$total_cr_amount = 0;
							$dr_amount = 0;
							$cr_amount = 0;
	                        if($account_rs->num_rows() > 0)
	                        {
	                        	
	                         	foreach ($account_rs->result() as $key => $value) 
	                         	{
	                         		# code...

	                         		$account_name = $value->account_name;
	                         		$account_id = $value->account_id;
	                         		 $changed .= '<tr>
									        			<td  colspan="9">'.strtoupper($account_name).'</td>
												   </tr>';

									$expense_of_account_rs = $this->ledgers_model->get_account_transactions($account_id);

										// var_dump($parent_account_id);die();
									if($expense_of_account_rs->num_rows() > 0)
									{
										$balance = 0;
										$total_amount = 0;
										$total_dr_amount = 0;
										$total_cr_amount = 0;
										// var_dump($expense_of_account_rs->result());die();
										foreach ($expense_of_account_rs->result() as $key => $value4) {
											# code...
											// get all transactions

											$dr_amount = $value4->dr_amount;
											$cr_amount = $value4->cr_amount;
											$transactionDescription = $value4->transactionDescription;
											$referenceCode = $value4->referenceCode;
											$transactionDate = $value4->transactionDate;
											$accountName = $value4->accountName;
											$transactionCategory = $value4->transactionCategory;

											if($dr_amount > 0)
											{
												$amount = $dr_amount;
											}

											else if($cr_amount > 0)
											{
												$amount = -$cr_amount;
											}
											else
											{
												$amount = 0;
											}

											$balance += $amount;
											$total_amount += $amount;

											$grand_balance += $balance;

											$total_dr_amount += $dr_amount;
											$total_cr_amount += $cr_amount;
											$grand_dr += $dr_amount;
				 							$grand_cr += $cr_amount;

											$changed .= '<tr>
															<td ></td>
										        			<td > '.$transactionCategory.'</td>
										        			<td > '.$transactionDate.'</td>
															<td > '.$referenceCode.'</td>
															<td > '.$transactionDescription.'</td>
										        			<td ></td>
															<td ></td>
															<td class="center-align">'.number_format($amount,2).'</td>
										        			<td class="center-align">'.number_format($balance,2).'</td>
														</tr>';

										}
										

									}
								}
							}

						}

						if($parent_account_name == 'Capital')
						{

							// var_dump($parent_account_name);die();
							$changed .= '<tr>
						        			<th  colspan="9">'.strtoupper($parent_account_name).'</th>
									   	</tr>';

							$account_rs = $this->ledgers_model->get_all_child_accounts($parent_account_id);
							$balance = 0;
							$total_amount = 0;

							$total_dr_amount = 0;
							$total_cr_amount = 0;
							$dr_amount = 0;
							$cr_amount = 0;
	                        if($account_rs->num_rows() > 0)
	                        {
	                        
	                         	foreach ($account_rs->result() as $key => $value) 
	                         	{
	                         		# code...

	                         		$account_name = $value->account_name;
	                         		$account_id = $value->account_id;
	                         		 $changed .= '<tr>
									        			<td  colspan="9">'.strtoupper($account_name).'</td>
												   </tr>';

									$expense_of_account_rs = $this->ledgers_model->get_account_transactions($account_id);

										// var_dump($parent_account_id);die();
									if($expense_of_account_rs->num_rows() > 0)
									{
										$balance = 0;
										$total_amount = 0;
										// var_dump($expense_of_account_rs->result());die();
										foreach ($expense_of_account_rs->result() as $key => $value4) {
											# code...
											// get all transactions

											$dr_amount = $value4->dr_amount;
											$cr_amount = $value4->cr_amount;
											$transactionDescription = $value4->transactionDescription;
											$referenceCode = $value4->referenceCode;
											$transactionDate = $value4->transactionDate;
											$accountName = $value4->accountName;
											$transactionCategory = $value4->transactionCategory;

											if($dr_amount > 0)
											{
												$amount = $dr_amount;
											}

											else if($cr_amount > 0)
											{
												$amount = -$cr_amount;
											}
											else
											{
												$amount = 0;
											}

											$balance += $amount;
											$total_amount += $amount;

											$grand_balance += $balance;

											$total_dr_amount += $dr_amount;
											$total_cr_amount += $cr_amount;
											$grand_dr += $dr_amount;
				 							$grand_cr += $cr_amount;

											$changed .= '<tr>
															<td ></td>
										        			<td > '.$transactionCategory.'</td>
										        			<td > '.$transactionDate.'</td>
															<td > '.$referenceCode.'</td>
															<td > '.$transactionDescription.'</td>
										        			<td ></td>
															<td ></td>
															<td class="center-align">'.number_format($amount,2).'</td>
										        			<td class="center-align">'.number_format($balance,2).'</td>
														</tr>';

										}
										

									}
								}
							}

						}




                     	


						
					endforeach;


                 }





                  $changed .= '<tr>
									<th >TOTAL</th>
				        			<th ></th>
				        			<th ></th>
									<th ></th>
									<th ></th>
				        			<th ></th>
									<th ></th>
									
				        			<th style="border-top:1px solid #000;" class="center-align" >'.number_format($grand_dr,2).'</th>
									<th style="border-top:1px solid #000;" class="center-align" >'.number_format($grand_cr,2).'</th>
									
								</tr>';
                 echo $changed;
				?>
				
			</tbody>
		</table>


    </div>
</div>