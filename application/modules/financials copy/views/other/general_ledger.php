

<?php
	$date_from = $this->session->userdata('date_from_general_ledger');
	$general_ledger_search_title =	$this->session->userdata('general_ledger_search_title');


	if(!empty($general_ledger_search_title))
	{
		$search_title = $general_ledger_search_title;
	}
	else 
	{
		$search_title = 'ALL TIME REPORT';
	}

?>

<div class="row">
	<div class="col-md-4">
		<?php echo $this->load->view('search/search_general_ledger','', true);?>
		<div class="text-center">
			<h3 class="box-title">General Ledger</h3>
			<h5 class="box-title"><?php echo $search_title?></h5>
			<h6 class="box-title">Created <?php echo date('jS M Y');?></h6>


			

		</div>
		<hr>
		<div class="col-md-12">
			<div class="form-group">
				<a href="<?php echo site_url().'financials/company_financial/print_general_ledger'?>" target="_blank"  class="btn btn-md btn-warning  col-md-12">Print General Ledger</a>
			</div>
			<div class="form-group">
			      <a href="<?php echo site_url().'company-financials/profit-and-loss'?>"  class="btn btn-md btn-success col-md-12"><i class="fa fa-file"></i> Income Statement</a>
			</div>
			<div class="form-group">
			      <a href="<?php echo site_url().'company-financials/balance-sheet'?>"  class="btn btn-md btn-info col-md-12"><i class="fa fa-file"></i> Balance Sheet</a>
			</div>
			<div class="form-group">
			      <a href="<?php echo site_url().'company-financials/trial-balance'?>"  class="btn btn-md btn-primary col-md-12"><i class="fa fa-file"></i> Trial Balance</a>
			</div>
			
		</div>
	</div>


	<div class="col-md-8">
	
		<div class="box">
		    <div class="panel-body" style="height:85vh;overflow-y:scroll;">
		    	<!-- <h3 class="box-title">Revenue</h3> -->
		    	<table class="table  table-striped table-condensed">
					<thead>
						<tr>
		        			<th >TYPE</th>
		        			<th >DATE</th>
							<th >NUM</th>
							<th >NAME</th>
							<th class="center-align">DEBIT</th>
							<th class="center-align">CREDIT</th>
						</tr>
					</thead>
					<tbody>

						<?php

						
						$this->session->set_userdata('general_ledger_search');

						$changed = '';


						 $accounts = $this->ledgers_model->get_all_parent_accounts();
						 $grand_amount = 0;
						 $grand_balance = 0;
						 $grand_dr = 0;
						 $grand_cr = 0;


		                 if($accounts->num_rows() > 0)
		                 {
		                     foreach($accounts->result() as $row):
		                         // $company_name = $row->company_name;
		                         $parent_account_name = $row->account_name;
		                         $parent_account_id = $row->account_id;


		      					//                   if($parent_account_name == 'Bank')
								// {

									$changed .= '<tr>
								        			<th  colspan="6">'.strtoupper($parent_account_name).'</th>
											   </tr>';
									$account_rs = $this->ledgers_model->get_all_child_accounts($parent_account_id);

									$parent_account_dr_amount = 0;
									$parent_account_cr_amount = 0;
									if($account_rs->num_rows() > 0)
									{


										foreach ($account_rs->result() as $key => $value_six) {
											// code...
											$account_name = $value_six->account_name;
			                         		$account_id = $value_six->account_id;
									

			                         		
											$account_ledger_rs = $this->ledgers_model->get_account_ledger($account_id);

											$balance = 0;
											$total_amount = 0;
											$dr_amount = 0;
											$cr_amount = 0;

											$total_dr_amount = 0;
											$total_cr_amount = 0;
					                        if($account_ledger_rs->num_rows() > 0)
					                        {
					                        	$changed .= '<tr>
											        			<th  colspan="6">'.strtoupper($account_name).'</th>
														   </tr>';
					                         	foreach ($account_ledger_rs->result() as $key => $value) 
					                         	{

							                     	$dr_amount = $value->dr_amount;
													$cr_amount = $value->cr_amount;
													$transactionDescription = $value->transactionDescription;
													$referenceCode = $value->referenceCode;
													$transactionDate = $value->transactionDate;
													$accountName = $value->accountName;
													$transactionCategory = $value->transactionCategory;

													$total_dr_amount += $dr_amount;
													$total_cr_amount += $cr_amount;
													$grand_dr += $dr_amount;
													$grand_cr += $cr_amount;

													$parent_account_dr_amount += $dr_amount;
													$parent_account_cr_amount += $cr_amount;

													$changed .= '<tr>
												        			<td > '.$transactionCategory.'</td>
												        			<td > '.$transactionDate.'</td>
																	<td > '.$referenceCode.'</td>
																	<td > '.$transactionDescription.'</td>
																	<td class="center-align">'.number_format($dr_amount,2).'</td>
												        			<td class="center-align">'.number_format($cr_amount,2).'</td>
																</tr>';
												}

												$changed .= '<tr>
																<th >TOTAL '.strtoupper($accountName).'</th>
											        			<th ></th>
																<th ></th>
																<th ></th>
																<th class="center-align">'.number_format($total_dr_amount,2).'</th>
											        			
																<th class="center-align">'.number_format($total_cr_amount,2).'</th>
																
															</tr>';
											}
										}
										$changed .= '<tr>
														<td colspan="6"></td>
													</tr>';
										$changed .= '<tr>
																<th >TOTAL '.strtoupper($parent_account_name).'</th>
											        			<th ></th>
																<th ></th>
																<th ></th>
																<th class="center-align">'.number_format($parent_account_dr_amount,2).'</th>
											        			
																<th class="center-align">'.number_format($parent_account_cr_amount,2).'</th>
																
															</tr>';

									}
								// }

		                       endforeach;


		                 }





		                  $changed .= '<tr>
											<th >TOTAL</th>
						        			<th ></th>
											<th ></th>
											<th ></th>
											
						        			<th style="border-top:1px solid #000;" class="center-align" >'.number_format($grand_dr,2).'</th>
											<th style="border-top:1px solid #000;" class="center-align" >'.number_format($grand_cr,2).'</th>
											
										</tr>';
		                 echo $changed;
						?>
						
					</tbody>
				</table>


		    </div>
		</div>
	</div>

</div>