<?php

$recon_id = $recon_id;


$recon_rs = $this->reconcilliation_model->get_recon_details($recon_id);

if($recon_rs->num_rows() > 0)  
{
	foreach ($recon_rs->result() as $key => $value) {
		# code...
		$account_id = $value->account_id;
		$account_name = $value->account_name;
		$recon_date = $value->recon_date;
		$start_date = $value->start_date;
		$opening_balance = $value->opening_balance;
		$interest_earned = $value->interest_earned;
		$service_charged = $value->service_charged;
		$money_in = $value->money_in;
		$money_out = $value->money_out;
		$money_in += $value->interest_earned;
		$money_out += $value->service_charged;
		$cleared_balance = $value->cleared_balance;
		$ending_balance = $value->ending_balance;


	}
}


$total_cleared_transactions = ($money_in) - ($money_out);
$cleared_balance_checked = $total_cleared_transactions + $opening_balance;

// cleared details

if($recon_type == 1)
{
	$checks_response = $this->reconcilliation_model->get_all_uncleared_checks($recon_date,$recon_id,2); 
	$checks_query = $checks_response['query'];


	$deposits_response = $this->reconcilliation_model->get_all_uncleared_deposits($recon_date,$recon_id,2); 
	$deposits_query = $deposits_response['query'];

}




// get all uncleared  

$unclearead_checks_response = $this->reconcilliation_model->get_all_uncleared_checks($recon_date,$recon_id); 

$total_checks = $unclearead_checks_response['total_amount'];
$total_checks_items = $unclearead_checks_response['count'];


$unclearead_deposits_response = $this->reconcilliation_model->get_all_uncleared_deposits($recon_date,$recon_id); 

$total_deposits = $unclearead_deposits_response['total_amount'];
$total_deposits_items = $unclearead_deposits_response['count'];

$total_uncleared_transactions = $total_deposits + $total_checks;



$registered_balance = $cleared_balance + $total_uncleared_transactions;


// get all cleared 
$clearead_checks_response = $this->reconcilliation_model->get_all_uncleared_checks($recon_date,$recon_id,1); 

$total_cleared_checks = $clearead_checks_response['total_amount'];
$total_cleared_checks_items = $clearead_checks_response['count'];


$clearead_deposits_response = $this->reconcilliation_model->get_all_uncleared_deposits($recon_date,$recon_id,1); 

$total_cleared_deposits = $clearead_deposits_response['total_amount'];
$total_cleared_deposits_items = $clearead_deposits_response['count'];

$total_new_transactions = $total_cleared_deposits + $total_cleared_checks;
?>

<div class="row" style="margin-top:15px;">
	 <div class="col-md-2">
	 </div>
    <div class="col-md-8">

        <section class="panel">
            <header class="panel-heading">

                <h2 class="panel-title"><?php echo strtoupper(strtolower($account_name.' reconcilliation as at '.$recon_date));?></h2>
                <a class="btn btn-sm btn-primary pull-right" href="<?php echo site_url().'accounting/bank-reconcilliation'?>"  style="margin-top: -25px; margin-right: 5px;"><i class="fa fa-arrow-left"></i> Back to reconcilliations</a>

                <a class="btn btn-sm btn-warning pull-right" href="<?php echo site_url().'print-recon-report/'.$recon_id?>" target="_blank"  style="margin-top: -25px; margin-right: 5px;"><i class="fa fa-print"></i> Print Recon</a>
                
            </header>

            <div class="panel-body">
				<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');

				if(!empty($error))
				{
					echo '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}

				if(!empty($success))
				{
					echo '<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}

				$search = $this->session->userdata('vendor_expense_search');
				$search_title = $this->session->userdata('vendor_expense_title_search');

				if(!empty($search))
				{
					echo '
					<a href="'.site_url().'financials/company_financial/close_creditor_expense_ledger" class="btn btn-warning btn-sm ">Close Search</a>
					';
					echo $search_title;
				}
				$reconcilliation_rs = $this->reconcilliation_model->get_all_reconcilliations();
				?>
				<style type="text/css">
					td
					{
						text-align: center !important;
					}
				</style>


				<div class="col-md-12" >
					<?php
					if($recon_type != 1)
					{



					?>
						<table class="table table-hover table-bordered table-striped" id="testTable">
							
							<tbody>
								<tr>
									<th  width="80%">  <strong>BEGINING BALANCE</strong></th>
									<td  width="20%"> <?php echo number_format($opening_balance,2)?></td>
								</tr>
								<tr>
									<th colspan="2" style="padding-left:20px;"> Cleared Transactions </th>
								

								</tr>
								<tr>
									<th  style="padding-left:35px;"> Checks and payments  </th>
									<td  > <?php echo number_format(-$money_out,2)?></td>
								

								</tr>
								<tr>
									<th  style="padding-left:35px;"> Deposits and Credits  </th>
									<td  > <?php echo number_format($money_in,2)?></td>
								</tr>
								<tr>
									<th style="padding-left:20px;" > Total Cleared Transactions  </th>
									<td  style="border-top:2px solid !important;" > <?php echo number_format($total_cleared_transactions,2)?></td>
								</tr>
								<tr>
									<th > Cleared Balance  </th>
									<td  style="border-bottom:3px  double  !important;border-top:2px solid !important;"> <strong><?php echo number_format($cleared_balance_checked,2)?></strong></td>
								</tr>

								<!-- SDAK -->
								<tr>
									<th colspan="2" style="padding-left:20px;"> Uncleared Transactions </th>
								

								</tr>
								<tr>
									<th  style="padding-left:35px;"> Checks and payments - <?php echo $total_checks_items;?> items  </th>
									<td  > <?php echo number_format($total_checks,2)?></td>
								

								</tr>
								<tr>
									<th  style="padding-left:35px;"> Deposits and Credits  - <?php echo $total_deposits_items;?> items </th>
									<td  > <?php echo number_format($total_deposits,2);?></td>
								</tr>
								<tr>
									<th style="padding-left:20px;" > Total Uncleared Transactions  </th>
									<td  style="border-top:2px solid !important;" > <strong><?php echo number_format($total_uncleared_transactions,2)?></strong></td>
								</tr>
								<tr>
									<th > Register Balance as at <?php echo $recon_date;?> </th>
									<td style="border-bottom:3px  double  !important;border-top:2px solid !important;" > <strong><?php echo number_format($registered_balance,2)?></strong></td>
								</tr>

								<!-- new transactions -->

								<tr>
									<th colspan="2" style="padding-left:20px;"> New Transactions </th>
								

								</tr>
								<tr>
									<th  style="padding-left:35px;"> Checks and payments - <?php echo number_format($total_cleared_checks_items)?> items  </th>
									<td  > <?php echo number_format($total_cleared_checks,2)?></td>
								

								</tr>
								<tr>
									<th  style="padding-left:35px;"> Deposits and Credits  - <?php echo number_format($total_cleared_deposits_items)?> items  </th>
									<td  > <?php echo number_format($total_cleared_deposits,2)?></td>
								</tr>
								<tr>
									<th style="padding-left:20px;" > Total New Transactions  </th>
									<td style="border-top:2px solid !important;" > <strong><?php echo number_format($total_new_transactions,2)?></strong></td>
								</tr>
								<tr>
									<th > Ending Balance as at <?php echo $recon_date;?> </th>
									<td  style="border-bottom:3px  double  !important;border-top:2px solid !important;"> <strong><?php echo number_format($ending_balance,2)?></strong></td>
								</tr>

							</tbody>

							
						</table>
					<?php

					}
					else
					{
						?>
						<table class="table table-hover table-bordered table-striped" id="testTable">
							<thead>
								<th  > Type </th>
								<th  > Date</th>
								<th  > Num</th>
								<th  > Name</th>
								<th  > Clr</th>
								<th  > Amount</th>
								<th  > Balance</th>
							</thead>
							<tbody>
								<tr>
									<th colspan="6">  <strong>BEGINING BALANCE</strong></th>
									<td  width="1"> <?php echo number_format($opening_balance,2)?></td>
								</tr>
								<tr>
									<th colspan="7" style="padding-left:20px;"> Cleared Transactions </th>
								

								</tr>
								<tr>
									<th  style="padding-left:35px;" colspan="7"> Checks and payments - <?php echo $checks_query->num_rows().' items'?>  </th>
								

								</tr>

								<?php
									$checks_result = '';
									if($checks_query->num_rows() > 0)
									{
										$balance = 0;
										foreach ($checks_query->result() as $key => $value) {
											// code...
											

											$payment_date = $value->payment_date; 
											$cheque = $value->cheque; 
											$payee = $value->payee; 
											$type = $value->type; 
											$amount = $value->amount; 
											if($type == 1)
											{
												$type_name = 'Creditor Payment';
											}else if($type == 2)
											{
												$type_name = 'Direct Payment';
										

											}
											else if($type == 3)
											{
												$type_name = 'Transfer';
										

											}
											else if($type == 4)
											{
												$type_name = 'Payroll Payment';
										

											}
											else if($type == 5)
											{
												$type_name = 'Journal';
										

											}
											$balance += $amount;

											$checks_result .= '<tr>
																	<td>'.$type_name.'</td>
																	<td>'.date('jS M Y',strtotime($payment_date)).'</td>
																	<td>'.$cheque.'</td>
																	<td>'.$payee.'</td>
																	<td></td>
																	<td>'.number_format($amount,2).'</td>
																	<td>'.number_format($balance,2).'</td>
																</tr>';

										}
									}

									echo $checks_result;
								?>
								<tr>
									<th  style="padding-left:35px;"  colspan="7"> Deposits and Credits - <?php echo $deposits_query->num_rows().' items'?>  </th>
									
								</tr>
								<?php
									$deposits_result = '';
									if($deposits_query->num_rows() > 0)
									{
										$balance = 0;
										foreach ($deposits_query->result() as $key => $value) {
											// code...
											

											$payment_date = $value->payment_date; 
											$cheque = $value->cheque; 
											$payee = $value->payee; 
											$type = $value->type; 
											$amount = $value->amount; 

											if($type == 1)
											{
												$type_name = 'Transfer';
											}else if($type == 2)
											{
												$type_name = 'Journal';
										

											}
											
											

											$balance += $amount;

											$deposits_result .= '<tr>
																	<td>'.$type_name.'</td>
																	<td>'.date('jS M Y',strtotime($payment_date)).'</td>
																	<td>'.$cheque.'</td>
																	<td>'.$payee.'</td>
																	<td></td>
																	<td>'.number_format($amount,2).'</td>
																	<td>'.number_format($balance,2).'</td>
																</tr>';

										}
									}

									echo $deposits_result;
								?>
								<tr>
									<th style="padding-left:20px;"  colspan="6"> Total Cleared Transactions  </th>
									<td  style="border-top:2px solid !important;" > <?php echo number_format($total_cleared_transactions,2)?></td>
								</tr>
								<tr>
									<th colspan="6"> Cleared Balance  </th>
									<td  style="border-bottom:3px  double  !important;border-top:2px solid !important;"> <strong><?php echo number_format($cleared_balance_checked,2)?></strong></td>
								</tr>

								<!-- SDAK -->
								<tr>
									<th colspan="2" style="padding-left:20px;"> Uncleared Transactions </th>
								

								</tr>
								<tr>
									<th  style="padding-left:35px;" colspan="7"> Checks and payments - <?php echo $total_checks_items;?> items  </th>
								
								

								</tr>
								<?php
								$checks_query = $unclearead_checks_response['query'];
									$checks_result = '';
									if($checks_query->num_rows() > 0)
									{
										$balance = 0;
										foreach ($checks_query->result() as $key => $value) {
											// code...
											

											$payment_date = $value->payment_date; 
											$cheque = $value->cheque; 
											$payee = $value->payee; 
											$type = $value->type; 
											$amount = $value->amount; 
											if($type == 1)
											{
												$type_name = 'Creditor Payment';
											}else if($type == 2)
											{
												$type_name = 'Direct Payment';
										

											}
											else if($type == 3)
											{
												$type_name = 'Transfer';
										

											}
											else if($type == 4)
											{
												$type_name = 'Payroll Payment';
										

											}
											else if($type == 5)
											{
												$type_name = 'Journal';
										

											}
											$balance += $amount;

											$checks_result .= '<tr>
																	<td>'.$type_name.'</td>
																	<td>'.date('jS M Y',strtotime($payment_date)).'</td>
																	<td>'.$cheque.'</td>
																	<td>'.$payee.'</td>
																	<td></td>
																	<td>'.number_format($amount,2).'</td>
																	<td>'.number_format($balance,2).'</td>
																</tr>';

										}

									}

									echo $checks_result;
								?>


								<tr>
									<th  style="padding-left:35px;" colspan="7"> Deposits and Credits  - <?php echo $total_deposits_items;?> items </th>
						
								</tr>
								<?php
									$deposits_query = $unclearead_deposits_response['query'];
									$deposits_result = '';
									if($deposits_query->num_rows() > 0)
									{
										$balance = 0;
										foreach ($deposits_query->result() as $key => $value) {
											// code...
											

											$payment_date = $value->payment_date; 
											$cheque = $value->cheque; 
											$payee = $value->payee; 
											$type = $value->type; 
											$amount = $value->amount; 

											if($type == 1)
											{
												$type_name = 'Transfer';
											}else if($type == 2)
											{
												$type_name = 'Journal';
										

											}
											
											

											$balance += $amount;

											$deposits_result .= '<tr>
																	<td>'.$type_name.'</td>
																	<td>'.date('jS M Y',strtotime($payment_date)).'</td>
																	<td>'.$cheque.'</td>
																	<td>'.$payee.'</td>
																	<td></td>
																	<td>'.number_format($amount,2).'</td>
																	<td>'.number_format($balance,2).'</td>
																</tr>';

										}
									}

									echo $deposits_result;
								?>
								<tr>
									<th style="padding-left:20px;" colspan="6"> Total Uncleared Transactions  </th>
									<td  style="border-top:2px solid !important;" > <strong><?php echo number_format($total_uncleared_transactions,2)?></strong></td>
								</tr>
								<tr>
									<th colspan="6"> Register Balance as at <?php echo $recon_date;?> </th>
									<td style="border-bottom:3px  double  !important;border-top:2px solid !important;" > <strong><?php echo number_format($registered_balance,2)?></strong></td>
								</tr>

								<!-- new transactions -->

								<tr>
									<th colspan="7" style="padding-left:20px;" > New Transactions </th>
								

								</tr>
								<tr>
									<th  style="padding-left:35px;" colspan="7"> Checks and payments - <?php echo number_format($total_cleared_checks_items)?> items  </th>
								

								</tr>
								<?php
								$checks_query = $clearead_checks_response['query'];
									$checks_result = '';
									if($checks_query->num_rows() > 0)
									{
										$balance = 0;
										foreach ($checks_query->result() as $key => $value) {
											// code...
											

											$payment_date = $value->payment_date; 
											$cheque = $value->cheque; 
											$payee = $value->payee; 
											$type = $value->type; 
											$amount = $value->amount; 
											if($type == 1)
											{
												$type_name = 'Creditor Payment';
											}else if($type == 2)
											{
												$type_name = 'Direct Payment';
										

											}
											else if($type == 3)
											{
												$type_name = 'Transfer';
										

											}
											else if($type == 4)
											{
												$type_name = 'Payroll Payment';
										

											}
											else if($type == 5)
											{
												$type_name = 'Journal';
										

											}
											$balance += $amount;

											$checks_result .= '<tr>
																	<td>'.$type_name.'</td>
																	<td>'.date('jS M Y',strtotime($payment_date)).'</td>
																	<td>'.$cheque.'</td>
																	<td>'.$payee.'</td>
																	<td></td>
																	<td>'.number_format($amount,2).'</td>
																	<td>'.number_format($balance,2).'</td>
																</tr>';

										}
									}

									echo $checks_result;
								?>
								<tr>
									<th  style="padding-left:35px;"  colspan="7"> Deposits and Credits  - <?php echo number_format($total_cleared_deposits_items)?> items  </th>
								</tr>

								<?php
									$deposits_query = $clearead_deposits_response['query'];
									$deposits_result = '';
									if($deposits_query->num_rows() > 0)
									{
										$balance = 0;
										foreach ($deposits_query->result() as $key => $value) {
											// code...
											

											$payment_date = $value->payment_date; 
											$cheque = $value->cheque; 
											$payee = $value->payee; 
											$type = $value->type; 
											$amount = $value->amount; 

											if($type == 1)
											{
												$type_name = 'Transfer';
											}else if($type == 2)
											{
												$type_name = 'Journal';
										

											}
											
											

											$balance += $amount;

											$deposits_result .= '<tr>
																	<td>'.$type_name.'</td>
																	<td>'.date('jS M Y',strtotime($payment_date)).'</td>
																	<td>'.$cheque.'</td>
																	<td>'.$payee.'</td>
																	<td></td>
																	<td>'.number_format($amount,2).'</td>
																	<td>'.number_format($balance,2).'</td>
																</tr>';

										}
										// $deposits_result .= '<tr>
										// 							<th colspan="5">Grand Total</th>
																	
										// 							<td colspan="2">'.number_format($balance,2).'</td>
										// 						</tr>';
									}

									echo $deposits_result;
								?>
								
								<tr>
									<th style="padding-left:20px;" colspan="6"> Total New Transactions  </th>
									<td style="border-top:2px solid !important;" > <strong><?php echo number_format($total_new_transactions,2)?></strong></td>
								</tr>
								<tr>
									<th colspan="6"> Ending Balance as at <?php echo $recon_date;?> </th>
									<td  style="border-bottom:3px  double  !important;border-top:2px solid !important;"> <strong><?php echo number_format($ending_balance,2)?></strong></td>
								</tr>

							</tbody>

							
						</table>

						<?php
					}
					?>
					
          		</div>
			</section>
   	 </div>
     <div class="col-md-2">
	 </div>
</div>

