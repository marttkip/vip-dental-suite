<?php

class Reconcilliation_model extends CI_Model
{
	public function get_all_reconcilliations()
	{
		$this->db->where('bank_reconcilliation.account_id = account.account_id');
		$query = $this->db->get('bank_reconcilliation,account');

		return $query;
	}

	public function get_child_accounts($parent_account_name)
    {
    	$this->db->from('account');
		$this->db->select('*');
		$this->db->where('account_name = "'.$parent_account_name.'" AND account.account_status = 1');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)  
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$account_id = $value->account_id;
			}
			//retrieve all users
			$this->db->from('account');
			$this->db->select('*');
			$this->db->where('parent_account = '.$account_id.' AND account.account_status = 1');
			$query = $this->db->get();
			
			return $query;    	


		}
		else
		{
			return FALSE;
		}

    }

    public function add_reconcilliation()
    {
    	$data['account_id'] = $this->input->post('account_id');
    	$data['recon_date'] = $this->input->post('recon_date');
    	$data['opening_balance'] = $this->input->post('opening_balance');
    	$data['ending_balance'] = $this->input->post('ending_balance');
    	$data['interest_earned'] = $this->input->post('interest_earned');
    	$data['service_charged'] = $this->input->post('service_charged');
    	$data['interest_account_id'] = $this->input->post('interest_account_id');
    	$data['interest_date'] = $this->input->post('interest_date');
    	$data['charged_date'] = $this->input->post('charged_date');
    	$data['expense_account_id'] = $this->input->post('expense_account_id');
    	$data['created_by'] = $this->session->userdata('personnel_id');

    	if($this->db->insert('bank_reconcilliation',$data))
    	{
    		$recon_id = $this->db->insert_id();

    		return $recon_id;

    	}
    	else
    	{
    		return FALSE;
    	}

    }

    public function get_money_out($recon_id)
    {
 		$this->db->from('bank_reconcilliation');
		$this->db->select('*');
		$this->db->where('recon_id = '.$recon_id.'');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)  
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$account_id = $value->account_id;
			}
		}


	



		$select_statement = "
							SELECT
								data.transactionDate AS payment_date,
								data.referenceCode AS cheque,
								data.transactionDescription AS payee,
								data.amount_paid AS amount,
								data.transaction_type AS type,
								data.transactionid AS transactionid,
								data.accountId AS account_id,
								data.recon_id AS recon_id,
								data.recepientId AS recepientId
								FROM (
										SELECT
											`creditor_payment`.`creditor_payment_id` AS `transactionid`,
											`creditor_payment`.`reference_number` AS `referenceCode`,
										  	`creditor_payment`.`creditor_id` AS `recepientId`,
											`creditor_payment`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT(`creditor`.`creditor_name`)  AS `transactionDescription`,
											`creditor_payment`.`total_amount` AS `amount_paid`,
											`creditor_payment`.`transaction_date` AS `transactionDate`,
											1 AS `transaction_type`,
											creditor_payment.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 creditor_payment
														
													)
													JOIN account ON(
														(
															account.account_id = creditor_payment.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												JOIN `creditor` ON(
													(
														creditor.creditor_id = creditor_payment.creditor_id
													)
												)
											)
										WHERE creditor_payment.creditor_payment_status = 1
										AND creditor_payment.account_from_id = $account_id




										UNION ALL


										SELECT
											`provider_payment`.`provider_payment_id` AS `transactionid`,
											`provider_payment`.`reference_number` AS `referenceCode`,
										  	`provider_payment`.`provider_id` AS `recepientId`,
											`provider_payment`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT(`provider`.`provider_name`)  AS `transactionDescription`,
											`provider_payment`.`total_amount` AS `amount_paid`,
											`provider_payment`.`transaction_date` AS `transactionDate`,
											7 AS `transaction_type`,
											provider_payment.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 provider_payment
														
													)
													JOIN account ON(
														(
															account.account_id = provider_payment.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												JOIN `provider` ON(
													(
														provider.provider_id = provider_payment.provider_id
													)
												)
											)
										WHERE provider_payment.provider_payment_status = 1
										AND provider_payment.account_from_id = $account_id

										UNION ALL

										SELECT
											`account_payments`.`account_payment_id` AS `transactionid`,
											`account_payments`.`receipt_number` AS `referenceCode`,
										  	`account_payments`.`account_to_id` AS `recepientId`,
											`account_payments`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Direct Payment'  AS `transactionDescription`,
											`account_payments`.`amount_paid` AS `amount_paid`,
											`account_payments`.`payment_date` AS `transactionDate`,
											2 AS `transaction_type`,
											account_payments.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 account_payments
														
													)
													JOIN account ON(
														(
															account.account_id = account_payments.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE account_payments.account_payment_deleted = 0 
										AND account_payments.account_from_id = $account_id

										UNION ALL

										SELECT
											`finance_transfer`.`finance_transfer_id` AS `transactionid`,
											`finance_transfer`.`reference_number` AS `referenceCode`,
										  	`finance_transfered`.`account_to_id` AS `recepientId`,
											`finance_transfer`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Account Transfers'  AS `transactionDescription`,
											`finance_transfer`.`finance_transfer_amount` AS `amount_paid`,
											`finance_transfer`.`transaction_date` AS `transactionDate`,
											3 AS `transaction_type`,
											finance_transfer.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 finance_transfer,finance_transfered
														
													)
													JOIN account ON(
														(
															account.account_id = finance_transfer.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE finance_transfer.finance_transfer_deleted = 0 AND finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id AND finance_transfer.account_from_id = $account_id

										UNION ALL

										SELECT
											`journal_entry`.`journal_entry_id` AS `transactionid`,
											`journal_entry`.`document_number` AS `referenceCode`,
										  	`journal_entry`.`account_from_id` AS `recepientId`,
											`journal_entry`.`account_to_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Capital Injection'  AS `transactionDescription`,
											`journal_entry`.`amount_paid` AS `amount_paid`,
											`journal_entry`.`payment_date` AS `transactionDate`,
											5 AS `transaction_type`,
											journal_entry.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 journal_entry
														
													)
													JOIN account ON(
														(
															account.account_id = journal_entry.account_to_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE journal_entry.journal_entry_deleted = 0  AND journal_entry.account_to_id = $account_id


										UNION ALL

											SELECT

												`payroll_payment`.`payroll_payment_id`  AS `transactionid`,
												`payroll_payment`.`reference_number` AS `referenceCode`,
											  	`statutory_accounts`.`statutory_account_id`  AS `recepientId`,
												`payroll_payment`.`account_from_id` AS `accountId`,
												`account`.`account_name` AS `accountName`,
												CONCAT(`statutory_accounts`.`statutory_account_name`,'-',MONTH(payroll_summary.payroll_created_for),'-',YEAR(payroll_summary.payroll_created_for))  AS `transactionDescription`,
												`payroll_payment_item`.`amount_paid` AS `amount_paid`,
												`payroll_payment`.`transaction_date` AS `transactionDate`,
												4 AS `transaction_type`,
												payroll_payment.recon_id AS `recon_id`

												FROM
													(
														(
															(
																`payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts,payroll
																
															)
															JOIN account ON(
																(
																	account.account_id = payroll_payment.account_from_id
																)
															)
														)
														JOIN `account_type` ON(
															(
																account_type.account_type_id = account.account_type_id
															)
														)
														
													)
													WHERE payroll_payment_item.invoice_type = 0 

													AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
													AND payroll.payroll_id = payroll_summary.payroll_id
													AND payroll.payroll_status = 1
													AND payroll_payment.payroll_payment_status = 1
													AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_invoice_id
													AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id AND payroll_payment.account_from_id = $account_id

											GROUP BY payroll_payment_item.payroll_payment_id 


									UNION ALL


										SELECT

											`payroll_payment`.`payroll_payment_id`  AS `transactionid`,
											`payroll_payment`.`reference_number` AS `referenceCode`,
										  	`statutory_accounts`.`statutory_account_id`  AS `recepientId`,
											`payroll_payment`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT('Payroll Invoice',' ',`payroll_invoice`.`invoice_number`)  AS `transactionDescription`,
											`payroll_payment_item`.`amount_paid` AS `amount_paid`,
											`payroll_payment`.`transaction_date` AS `transactionDate`,
											4 AS `transaction_type`,
											payroll_payment.recon_id AS `recon_id`
										FROM
											(
												(
													(
														`payroll_payment_item`,payroll_payment,statutory_accounts,payroll_invoice
														
													)
													JOIN account ON(
														(
															account.account_id = payroll_payment.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
											WHERE payroll_payment_item.invoice_type = 1
											AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
											AND payroll_payment.payroll_payment_status = 1
											AND payroll_invoice.statutory_invoice_id = payroll_payment_item.payroll_invoice_id 
											AND statutory_accounts.statutory_account_id = payroll_invoice.statutory_id AND payroll_payment.account_from_id = $account_id 

									GROUP BY payroll_payment_item.payroll_payment_id 

									UNION ALL

										SELECT
											`finance_purchase`.`finance_purchase_id` AS `transactionid`,
											`finance_purchase`.`document_number` AS `referenceCode`,
												`finance_purchase_payment`.`account_from_id` AS `recepientId`,
											`finance_purchase_payment`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'M Petty Cash'  AS `transactionDescription`,
											`finance_purchase`.`finance_purchase_amount` AS `amount_paid`,
											`finance_purchase`.`transaction_date` AS `transactionDate`,
											6 AS `transaction_type`,
											finance_purchase.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 finance_purchase,finance_purchase_payment
														
													)
													JOIN account ON(
														(
															account.account_id = finance_purchase_payment.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE finance_purchase.finance_purchase_deleted = 0 AND finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id AND finance_purchase_payment.account_from_id = $account_id
									

									) AS data  WHERE (data.recon_id = 0 OR data.recon_id = ".$recon_id.") AND data.transactionDate >= '2020-12-01' ORDER BY data.transactionDate ASC ";
		
		$query = $this->db->query($select_statement);

		return $query;


    }


      public function get_money_in($recon_id)
    {
    	$this->db->from('bank_reconcilliation');
		$this->db->select('*');
		$this->db->where('recon_id = '.$recon_id.'');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)  
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$account_id = $value->account_id;
			}
		}

	



		$select_statement = "
							SELECT
								data.transactionDate AS payment_date,
								data.referenceCode AS cheque,
								data.transactionDescription AS payee,
								data.amount_paid AS amount,
								data.transaction_type AS type,
								data.transactionid AS transactionid,
								data.accountId AS account_id,
								data.recon_id AS recon_id,
								data.recepientId AS recepientId
								FROM (
										

										SELECT
											`finance_transfered`.`finance_transfered_id` AS `transactionid`,
											`finance_transfer`.`reference_number` AS `referenceCode`,
										  	`finance_transfer`.`account_from_id` AS `recepientId`,
											`finance_transfered`.`account_to_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Money Transfer'  AS `transactionDescription`,
											`finance_transfer`.`finance_transfer_amount` AS `amount_paid`,
											`finance_transfer`.`transaction_date` AS `transactionDate`,
											1 AS `transaction_type`,
											finance_transfered.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 finance_transfer,finance_transfered
														
													)
													JOIN account ON(
														(
															account.account_id = finance_transfered.account_to_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE finance_transfer.finance_transfer_deleted = 0 AND finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id  AND finance_transfered.account_to_id = $account_id

										UNION ALL

										SELECT
											`journal_entry`.`journal_entry_id` AS `transactionid`,
											`journal_entry`.`document_number` AS `referenceCode`,
										  	`journal_entry`.`account_from_id` AS `recepientId`,
											`journal_entry`.`account_to_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Capital Injection'  AS `transactionDescription`,
											`journal_entry`.`amount_paid` AS `amount_paid`,
											`journal_entry`.`payment_date` AS `transactionDate`,
											2 AS `transaction_type`,
											journal_entry.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 journal_entry
														
													)
													JOIN account ON(
														(
															account.account_id = journal_entry.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE journal_entry.journal_entry_deleted = 0  AND journal_entry.account_from_id = $account_id



										UNION ALL

										SELECT
											payments.payment_id AS `transactionid`,
											`payments`.`transaction_code` AS `referenceCode`,
										  	payments.patient_id AS `recepientId`,
											`payment_method`.`account_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT('PATIENT INVOICE PAYMENT',' ',visit_invoice.visit_invoice_number)  AS `transactionDescription`,
											SUM(payment_item.payment_item_amount) AS `amount_paid`,
											payments.payment_date AS `transactionDate`,
											3 AS `transaction_type`,
											payments.recon_id AS `recon_id`

										FROM
											payments,payment_item,payment_method,account,visit_invoice,account_type
											WHERE payments.cancel = 0 AND payments.payment_type = 1 AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND payment_item.invoice_type = 1
											AND payments.payment_id = payment_item.payment_id 
											AND payments.payment_method_id = payment_method.payment_method_id
											AND payment_method.account_id = account.account_id
											AND account.account_type_id = account_type.account_type_id
											AND account.account_id = $account_id
											AND payments.payment_method_id <> 9
										GROUP BY payment_item.payment_id


										UNION ALL

										SELECT
											batch_receipts.batch_receipt_id AS `transactionid`,
											`batch_receipts`.`receipt_number` AS `referenceCode`,
										  	`batch_receipts`.`insurance_id` AS `recepientId`,
											`batch_receipts`.`bank_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT('BATCH RECEIPT',' ',batch_receipts.receipt_number)  AS `transactionDescription`,
											batch_receipts.total_amount_paid AS `amount_paid`,
											batch_receipts.payment_date AS `transactionDate`,
											4 AS `transaction_type`,
											batch_receipts.recon_id AS `recon_id`

										FROM
											batch_receipts,account,account_type
											WHERE 
											batch_receipts.bank_id = account.account_id 
											AND batch_receipts.current_payment_status = 1 
											AND account.account_type_id = account_type.account_type_id
											AND account.account_id = $account_id
										
									

									) AS data WHERE (data.recon_id = 0 OR data.recon_id = ".$recon_id.")  AND data.transactionDate >= '2022-01-01' ORDER BY data.transactionDate ASC";
		
		$query = $this->db->query($select_statement);

		return $query;


    }

    public function get_recon_details($recon_id)
    {
    	$this->db->from('bank_reconcilliation,account');
		$this->db->select('*');
		$this->db->where('bank_reconcilliation.account_id = account.account_id AND recon_id = '.$recon_id.'');
		$query = $this->db->get();
		
		return $query;
    }


    public function get_money_out_total($recon_id)
    {
 		$this->db->from('bank_reconcilliation');
		$this->db->select('*');
		$this->db->where('recon_id = '.$recon_id.'');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)  
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$account_id = $value->account_id;
			}
		}


	



		$select_statement = "
							SELECT
								data.transactionDate AS payment_date,
								data.referenceCode AS cheque,
								data.transactionDescription AS payee,
								SUM(data.amount_paid) AS amount,
								data.transaction_type AS type,
								data.transactionid AS transactionid,
								data.accountId AS account_id
								FROM (
										SELECT
											`creditor_payment`.`creditor_payment_id` AS `transactionid`,
											`creditor_payment`.`reference_number` AS `referenceCode`,
										  	`creditor_payment`.`creditor_id` AS `recepientId`,
											`creditor_payment`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT(`creditor`.`creditor_name`)  AS `transactionDescription`,
											SUM(`creditor_payment`.`total_amount`) AS `amount_paid`,
											`creditor_payment`.`transaction_date` AS `transactionDate`,
											1 AS `transaction_type`
										FROM
											(
												(
													(
													 creditor_payment
														
													)
													JOIN account ON(
														(
															account.account_id = creditor_payment.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												JOIN `creditor` ON(
													(
														creditor.creditor_id = creditor_payment.creditor_id
													)
												)
											)
										WHERE creditor_payment.creditor_payment_status = 1
										AND creditor_payment.account_from_id = $account_id AND creditor_payment.recon_id = $recon_id 

										UNION ALL

										SELECT
											`account_payments`.`account_payment_id` AS `transactionid`,
											`account_payments`.`receipt_number` AS `referenceCode`,
										  	`account_payments`.`account_to_id` AS `recepientId`,
											`account_payments`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Direct Payment'  AS `transactionDescription`,
											SUM(`account_payments`.`amount_paid`) AS `amount_paid`,
											`account_payments`.`payment_date` AS `transactionDate`,
											2 AS `transaction_type`
										FROM
											(
												(
													(
													 account_payments
														
													)
													JOIN account ON(
														(
															account.account_id = account_payments.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE account_payments.account_payment_deleted = 0 
										AND account_payments.account_from_id = $account_id AND account_payments.recon_id = $recon_id 

										UNION ALL

										SELECT
											`finance_transfer`.`finance_transfer_id` AS `transactionid`,
											`finance_transfer`.`reference_number` AS `referenceCode`,
										  	`finance_transfered`.`account_to_id` AS `recepientId`,
											`finance_transfer`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Account Transfers'  AS `transactionDescription`,
											SUM(`finance_transfer`.`finance_transfer_amount`) AS `amount_paid`,
											`finance_transfer`.`transaction_date` AS `transactionDate`,
											2 AS `transaction_type`
										FROM
											(
												(
													(
													 finance_transfer,finance_transfered
														
													)
													JOIN account ON(
														(
															account.account_id = finance_transfer.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE finance_transfer.finance_transfer_deleted = 0 AND finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id AND finance_transfer.account_from_id = $account_id AND finance_transfer.recon_id = $recon_id 

									UNION ALL


										SELECT

										


											`journal_entry`.`journal_entry_id` AS `transactionid`,
											`journal_entry`.`document_number` AS `referenceCode`,
										  	`journal_entry`.`account_from_id` AS `recepientId`,
											`journal_entry`.`account_to_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Capital Injection'  AS `transactionDescription`,
											`journal_entry`.`amount_paid` AS `amount_paid`,
											`journal_entry`.`payment_date` AS `transactionDate`,
											2 AS `transaction_type`
										FROM
											(
												(
													(
													 journal_entry
														
													)
													JOIN account ON(
														(
															account.account_id = journal_entry.account_to_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE journal_entry.journal_entry_deleted = 0  AND journal_entry.account_to_id = $account_id AND journal_entry.recon_id = $recon_id 

										UNION ALL

										SELECT

												`payroll_payment`.`payroll_payment_id`  AS `transactionid`,
												`payroll_payment`.`reference_number` AS `referenceCode`,
											  	`statutory_accounts`.`statutory_account_id`  AS `recepientId`,
												`payroll_payment`.`account_from_id` AS `accountId`,
												`account`.`account_name` AS `accountName`,
												CONCAT(`statutory_accounts`.`statutory_account_name`,'-',MONTH(payroll_summary.payroll_created_for),'-',YEAR(payroll_summary.payroll_created_for))  AS `transactionDescription`,
												`payroll_payment_item`.`amount_paid` AS `amount_paid`,
												`payroll_payment`.`transaction_date` AS `transactionDate`,
												4 AS `transaction_type`

												FROM
													(
														(
															(
																`payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts,payroll
																
															)
															JOIN account ON(
																(
																	account.account_id = payroll_payment.account_from_id
																)
															)
														)
														JOIN `account_type` ON(
															(
																account_type.account_type_id = account.account_type_id
															)
														)
														
													)
													WHERE payroll_payment_item.invoice_type = 0 

													AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
													AND payroll.payroll_id = payroll_summary.payroll_id
													AND payroll.payroll_status = 1
													AND payroll_payment.payroll_payment_status = 1
													AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_invoice_id
													AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id AND payroll_payment.account_from_id = $account_id AND payroll_payment.recon_id = $recon_id
													GROUP BY payroll_payment_item.payroll_payment_id  


											UNION ALL


												SELECT

													`payroll_payment`.`payroll_payment_id`  AS `transactionid`,
													`payroll_payment`.`reference_number` AS `referenceCode`,
												  	`statutory_accounts`.`statutory_account_id`  AS `recepientId`,
													`payroll_payment`.`account_from_id` AS `accountId`,
													`account`.`account_name` AS `accountName`,
													CONCAT('Payroll Invoice',' ',`payroll_invoice`.`invoice_number`)  AS `transactionDescription`,
													`payroll_payment_item`.`amount_paid` AS `amount_paid`,
													`payroll_payment`.`transaction_date` AS `transactionDate`,
													4 AS `transaction_type`
												FROM
													(
														(
															(
																`payroll_payment_item`,payroll_payment,statutory_accounts,payroll_invoice
																
															)
															JOIN account ON(
																(
																	account.account_id = payroll_payment.account_from_id
																)
															)
														)
														JOIN `account_type` ON(
															(
																account_type.account_type_id = account.account_type_id
															)
														)
														
													)
													WHERE payroll_payment_item.invoice_type = 1
													AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
													AND payroll_payment.payroll_payment_status = 1
													AND payroll_invoice.statutory_invoice_id = payroll_payment_item.payroll_invoice_id 
													AND statutory_accounts.statutory_account_id = payroll_invoice.statutory_id  AND payroll_payment.account_from_id = $account_id  AND payroll_payment.recon_id = $recon_id
											GROUP BY payroll_payment_item.payroll_payment_id 

											UNION ALL

													SELECT
														`finance_purchase`.`finance_purchase_id` AS `transactionid`,
														`finance_purchase`.`document_number` AS `referenceCode`,
															`finance_purchase_payment`.`account_from_id` AS `recepientId`,
														`finance_purchase_payment`.`account_from_id` AS `accountId`,
														`account`.`account_name` AS `accountName`,
														'M Petty Cash'  AS `transactionDescription`,
														`finance_purchase`.`finance_purchase_amount` AS `amount_paid`,
														`finance_purchase`.`transaction_date` AS `transactionDate`,
														6 AS `transaction_type`
													FROM
														(
															(
																(
																 finance_purchase,finance_purchase_payment
																	
																)
																JOIN account ON(
																	(
																		account.account_id = finance_purchase_payment.account_from_id
																	)
																)
															)
															JOIN `account_type` ON(
																(
																	account_type.account_type_id = account.account_type_id
																)
															)
															
														)
													WHERE finance_purchase.finance_purchase_deleted = 0 AND finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id AND finance_purchase_payment.account_from_id = $account_id  AND finance_purchase.recon_id = $recon_id

									) AS data  ";
		
		$query = $this->db->query($select_statement);

		$amount = 0;

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$amount = $value->amount;
			}
		}

		// var_dump($amount);die();

		return $amount;


    }

    public function get_money_in_total($recon_id)
    {


    	$this->db->from('bank_reconcilliation');
		$this->db->select('*');
		$this->db->where('recon_id = '.$recon_id.'');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)  
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$account_id = $value->account_id;
			}
		}

	



		$select_statement = "
							SELECT
								data.transactionDate AS payment_date,
								data.referenceCode AS cheque,
								data.transactionDescription AS payee,
								SUM(data.amount_paid) AS amount,
								data.transaction_type AS type,
								data.transactionid AS transactionid,
								data.accountId AS account_id
								FROM (
										

										SELECT
											`finance_transfer`.`finance_transfer_id` AS `transactionid`,
											`finance_transfer`.`reference_number` AS `referenceCode`,
										  	`finance_transfer`.`account_from_id` AS `recepientId`,
											`finance_transfered`.`account_to_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Money Transfer'  AS `transactionDescription`,
											SUM(`finance_transfer`.`finance_transfer_amount`) AS `amount_paid`,
											`finance_transfer`.`transaction_date` AS `transactionDate`,
											1 AS `transaction_type`
										FROM
											(
												(
													(
													 finance_transfer,finance_transfered
														
													)
													JOIN account ON(
														(
															account.account_id = finance_transfered.account_to_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE finance_transfer.finance_transfer_deleted = 0 AND finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id  AND finance_transfered.account_to_id = $account_id AND finance_transfered.recon_id = $recon_id 

										UNION ALL

										SELECT
											`journal_entry`.`journal_entry_id` AS `transactionid`,
											`journal_entry`.`document_number` AS `referenceCode`,
										  	`journal_entry`.`account_from_id` AS `recepientId`,
											`journal_entry`.`account_to_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Capital Injection'  AS `transactionDescription`,
											SUM(`journal_entry`.`amount_paid`) AS `amount_paid`,
											`journal_entry`.`payment_date` AS `transactionDate`,
											2 AS `transaction_type`
										FROM
											(
												(
													(
													 journal_entry
														
													)
													JOIN account ON(
														(
															account.account_id = journal_entry.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE journal_entry.journal_entry_deleted = 0  AND journal_entry.account_from_id = $account_id AND journal_entry.recon_id = $recon_id 
									
										UNION ALL


										SELECT
											payments.payment_id AS `transactionid`,
											`payments`.`confirm_number` AS `referenceCode`,
										  	payments.patient_id AS `recepientId`,
											`payment_method`.`account_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT('PATIENT INVOICE PAYMENT',' ',visit_invoice.visit_invoice_number)  AS `transactionDescription`,
											SUM(payment_item.payment_item_amount) AS `amount_paid`,
											payments.payment_date AS `transactionDate`,
											3 AS `transaction_type`

										FROM
											payments,payment_item,payment_method,account,visit_invoice,account_type
											WHERE payments.cancel = 0 AND payments.payment_type = 1 AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND payment_item.invoice_type = 1
											AND payments.payment_id = payment_item.payment_id 
											AND payments.payment_method_id = payment_method.payment_method_id
											AND payment_method.account_id = account.account_id
											AND account.account_type_id = account_type.account_type_id
											AND payment_method.account_id = $account_id AND payments.recon_id = $recon_id 

											AND payments.payment_method_id <> 9

										GROUP BY payment_item.payment_id

										UNION ALL



										SELECT
											batch_receipts.batch_receipt_id AS `transactionid`,
											`batch_receipts`.`receipt_number` AS `referenceCode`,
										  	'' AS `recepientId`,
											`batch_receipts`.`bank_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT('BATCH RECEIPT',' ',batch_receipts.receipt_number)  AS `transactionDescription`,
											batch_receipts.total_amount_paid AS `amount_paid`,
											batch_receipts.payment_date AS `transactionDate`,
											4 AS `transaction_type`

										FROM
											batch_receipts,account,account_type
											WHERE 
											batch_receipts.bank_id = account.account_id 
											AND batch_receipts.current_payment_status = 1 
											AND account.account_type_id = account_type.account_type_id
											AND account.account_id = $account_id  AND batch_receipts.recon_id = $recon_id 


									) AS data";
		
		$query = $this->db->query($select_statement);


		$amount = 0;

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$amount = $value->amount;
			}
		}

		// var_dump($amount);die();

		return $amount;
    }

    function get_all_accounts($account_id=NULL)
	{
		
		 if(!empty($account_id))
	      {
	        $add = ' AND account_id <> '.$account_id;
	      }
	      else
	      {
	        $add ='';
	      }
	      $this->db->from('account');
	      $this->db->select('*');
	      $this->db->where('parent_account <> 0'.$add);
	      $this->db->order_by('parent_account','ASC');
	      $query = $this->db->get();

       return $query;
	}
	public function get_account_name($from_account_id)
	{
	$account_name = '';
	$this->db->select('account_name');
	$this->db->where('account_id = '.$from_account_id);
	$query = $this->db->get('account');

	$account_details = $query->row();
	$account_name = $account_details->account_name;

	return $account_name;
	}

	function get_all_uncleared_checks($recon_date,$recon_id,$type=null)
	{


		$this->db->from('bank_reconcilliation');
		$this->db->select('*');
		$this->db->where('recon_id = '.$recon_id.'');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)  
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$account_id = $value->account_id;
			}
		}

		if(empty($type))
		{
			$add = 'WHERE data.transactionDate <= "'.$recon_date.'" AND data.recon_id = 0';
		}
		else if($type == 1)
		{
			$add = 'WHERE data.transactionDate > "'.$recon_date.'" AND data.recon_id = 0';
		}
		else if($type == 2)
		{
			$add = 'WHERE  data.recon_id = '.$recon_id;
		}

	



		$select_statement = "
							SELECT
								data.transactionDate AS payment_date,
								data.referenceCode AS cheque,
								data.transactionDescription AS payee,
								data.amount_paid AS amount,
								data.transaction_type AS type,
								data.transactionid AS transactionid,
								data.accountId AS account_id,
								data.recon_id AS recon_id
								FROM (
										SELECT
											`creditor_payment`.`creditor_payment_id` AS `transactionid`,
											`creditor_payment`.`reference_number` AS `referenceCode`,
										  	`creditor_payment`.`creditor_id` AS `recepientId`,
											`creditor_payment`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT(`creditor`.`creditor_name`)  AS `transactionDescription`,
											`creditor_payment`.`total_amount` AS `amount_paid`,
											`creditor_payment`.`transaction_date` AS `transactionDate`,
											1 AS `transaction_type`,
											creditor_payment.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 creditor_payment
														
													)
													JOIN account ON(
														(
															account.account_id = creditor_payment.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												JOIN `creditor` ON(
													(
														creditor.creditor_id = creditor_payment.creditor_id
													)
												)
											)
										WHERE creditor_payment.creditor_payment_status = 1
										AND creditor_payment.account_from_id = $account_id

										UNION ALL

										SELECT
											`account_payments`.`account_payment_id` AS `transactionid`,
											`account_payments`.`receipt_number` AS `referenceCode`,
										  	`account_payments`.`account_to_id` AS `recepientId`,
											`account_payments`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Direct Payment'  AS `transactionDescription`,
											`account_payments`.`amount_paid` AS `amount_paid`,
											`account_payments`.`payment_date` AS `transactionDate`,
											2 AS `transaction_type`,
											account_payments.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 account_payments
														
													)
													JOIN account ON(
														(
															account.account_id = account_payments.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE account_payments.account_payment_deleted = 0 
										AND account_payments.account_from_id = $account_id

										UNION ALL

										SELECT
											`finance_transfer`.`finance_transfer_id` AS `transactionid`,
											`finance_transfer`.`reference_number` AS `referenceCode`,
										  	`finance_transfered`.`account_to_id` AS `recepientId`,
											`finance_transfer`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Account Transfers'  AS `transactionDescription`,
											`finance_transfer`.`finance_transfer_amount` AS `amount_paid`,
											`finance_transfer`.`transaction_date` AS `transactionDate`,
											3 AS `transaction_type`,
											finance_transfer.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 finance_transfer,finance_transfered
														
													)
													JOIN account ON(
														(
															account.account_id = finance_transfer.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE finance_transfer.finance_transfer_deleted = 0 AND finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id AND finance_transfer.account_from_id = $account_id

										UNION ALL

										SELECT
											`journal_entry`.`journal_entry_id` AS `transactionid`,
											`journal_entry`.`document_number` AS `referenceCode`,
										  	`journal_entry`.`account_from_id` AS `recepientId`,
											`journal_entry`.`account_to_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Capital Injection'  AS `transactionDescription`,
											`journal_entry`.`amount_paid` AS `amount_paid`,
											`journal_entry`.`payment_date` AS `transactionDate`,
											5 AS `transaction_type`,
											journal_entry.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 journal_entry
														
													)
													JOIN account ON(
														(
															account.account_id = journal_entry.account_to_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE journal_entry.journal_entry_deleted = 0  AND journal_entry.account_to_id = $account_id


										UNION ALL

										SELECT

												`payroll_payment`.`payroll_payment_id`  AS `transactionid`,
												`payroll_payment`.`reference_number` AS `referenceCode`,
											  	`statutory_accounts`.`statutory_account_id`  AS `recepientId`,
												`payroll_payment`.`account_from_id` AS `accountId`,
												`account`.`account_name` AS `accountName`,
												CONCAT(`statutory_accounts`.`statutory_account_name`,'-',MONTH(payroll_summary.payroll_created_for),'-',YEAR(payroll_summary.payroll_created_for))  AS `transactionDescription`,
												`payroll_payment_item`.`amount_paid` AS `amount_paid`,
												`payroll_payment`.`transaction_date` AS `transactionDate`,
												4 AS `transaction_type`,
												payroll_payment.recon_id AS `recon_id`

												FROM
													(
														(
															(
																`payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts,payroll
																
															)
															JOIN account ON(
																(
																	account.account_id = payroll_payment.account_from_id
																)
															)
														)
														JOIN `account_type` ON(
															(
																account_type.account_type_id = account.account_type_id
															)
														)
														
													)
													WHERE payroll_payment_item.invoice_type = 0 

													AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
													AND payroll.payroll_id = payroll_summary.payroll_id
													AND payroll.payroll_status = 1
													AND payroll_payment.payroll_payment_status = 1
													AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_invoice_id
													AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id AND payroll_payment.account_from_id = $account_id

											GROUP BY payroll_payment_item.payroll_payment_id 


									UNION ALL


										SELECT

											`payroll_payment`.`payroll_payment_id`  AS `transactionid`,
											`payroll_payment`.`reference_number` AS `referenceCode`,
										  	`statutory_accounts`.`statutory_account_id`  AS `recepientId`,
											`payroll_payment`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT('Payroll Invoice',' ',`payroll_invoice`.`invoice_number`)  AS `transactionDescription`,
											`payroll_payment_item`.`amount_paid` AS `amount_paid`,
											`payroll_payment`.`transaction_date` AS `transactionDate`,
											4 AS `transaction_type`,
											payroll_payment.recon_id AS `recon_id`
										FROM
											(
												(
													(
														`payroll_payment_item`,payroll_payment,statutory_accounts,payroll_invoice
														
													)
													JOIN account ON(
														(
															account.account_id = payroll_payment.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
											WHERE payroll_payment_item.invoice_type = 1
											AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
											AND payroll_payment.payroll_payment_status = 1
											AND payroll_invoice.statutory_invoice_id = payroll_payment_item.payroll_invoice_id 
											AND statutory_accounts.statutory_account_id = payroll_invoice.statutory_id AND payroll_payment.account_from_id = $account_id 

									GROUP BY payroll_payment_item.payroll_payment_id 
									

									) AS data ".$add."  ORDER BY data.transactionDate ASC ";
		
		$query = $this->db->query($select_statement);
		$count = $query->num_rows();
		$response['count'] = $count;
		$response['query'] = $query;
		
		$total_amount = 0;

		if($query->num_rows() > 0)
		{
			foreach($query->result() AS $key => $value)
			{
				$amount = -$value->amount;
				$total_amount += $amount;
			}
		}

		if(empty($amount))
		{
			$total_amount = 0;
		}
		$response['total_amount'] = $total_amount;
		return $response;

	}
	function get_all_uncleared_deposits($recon_date,$recon_id,$type=null)
	{


		$this->db->from('bank_reconcilliation');
		$this->db->select('*');
		$this->db->where('recon_id = '.$recon_id.'');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)  
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$account_id = $value->account_id;
			}
		}

		if(empty($type))
		{
			$add = 'WHERE data.transactionDate <= "'.$recon_date.'" AND data.recon_id = 0';
		}
		else if($type == 1)
		{
			$add = 'WHERE data.transactionDate > "'.$recon_date.'" AND data.recon_id = 0';
		}
		else if($type == 2)
		{
			$add = 'WHERE  data.recon_id = '.$recon_id;
		}

	



		$select_statement = "
							SELECT
								data.transactionDate AS payment_date,
								data.referenceCode AS cheque,
								data.transactionDescription AS payee,
								data.amount_paid AS amount,
								data.transaction_type AS type,
								data.transactionid AS transactionid,
								data.accountId AS account_id,
								data.recon_id AS recon_id
								FROM (
										

										SELECT
											`finance_transfered`.`finance_transfered_id` AS `transactionid`,
											`finance_transfer`.`reference_number` AS `referenceCode`,
										  	`finance_transfer`.`account_from_id` AS `recepientId`,
											`finance_transfered`.`account_to_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Money Transfer'  AS `transactionDescription`,
											`finance_transfer`.`finance_transfer_amount` AS `amount_paid`,
											`finance_transfer`.`transaction_date` AS `transactionDate`,
											1 AS `transaction_type`,
											finance_transfered.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 finance_transfer,finance_transfered
														
													)
													JOIN account ON(
														(
															account.account_id = finance_transfered.account_to_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE finance_transfer.finance_transfer_deleted = 0 AND finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id  AND finance_transfered.account_to_id = $account_id

										UNION ALL

										SELECT
											`journal_entry`.`journal_entry_id` AS `transactionid`,
											`journal_entry`.`document_number` AS `referenceCode`,
										  	`journal_entry`.`account_from_id` AS `recepientId`,
											`journal_entry`.`account_to_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Capital Injection'  AS `transactionDescription`,
											`journal_entry`.`amount_paid` AS `amount_paid`,
											`journal_entry`.`payment_date` AS `transactionDate`,
											2 AS `transaction_type`,
											journal_entry.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 journal_entry
														
													)
													JOIN account ON(
														(
															account.account_id = journal_entry.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE journal_entry.journal_entry_deleted = 0  AND journal_entry.account_from_id = $account_id

										UNION ALL


										SELECT
											payments.payment_id AS `transactionid`,
											`payments`.`confirm_number` AS `referenceCode`,
										  	payments.patient_id AS `recepientId`,
											`payment_method`.`account_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT('PATIENT INVOICE PAYMENT',' ',visit_invoice.visit_invoice_number)  AS `transactionDescription`,
											SUM(payment_item.payment_item_amount) AS `amount_paid`,
											payments.payment_date AS `transactionDate`,
											3 AS `transaction_type`,
											payments.recon_id AS recon_id
										FROM
											payments,payment_item,payment_method,account,visit_invoice,account_type
											WHERE payments.cancel = 0 AND payments.payment_type = 1 AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND payment_item.invoice_type = 1
											AND payments.payment_id = payment_item.payment_id 
											AND payments.payment_method_id = payment_method.payment_method_id
											AND payment_method.account_id = account.account_id
											AND account.account_type_id = account_type.account_type_id
											AND account.account_id = $account_id 
										GROUP BY payment_item.payment_id

									)  AS data ".$add." ORDER BY data.transactionDate ASC ";
		
		$query = $this->db->query($select_statement);
		$count = $query->num_rows();
		$response['count'] = $count;
		$response['query'] = $query;
		
		$total_amount = 0;

		if($query->num_rows() > 0)
		{
			foreach($query->result() AS $key => $value)
			{
				$amount = $value->amount;
				$total_amount += $amount;
			}
		}

		if(empty($amount))
		{
			$total_amount = 0;
		}
		$response['total_amount'] = $total_amount;
		return $response;

	}

	 public function edit_reconcilliation($recon_id)
    {
    	$data['account_id'] = $this->input->post('account_id');
    	$data['recon_date'] = $this->input->post('recon_date');
    	$data['opening_balance'] = $this->input->post('opening_balance');
    	$data['ending_balance'] = $this->input->post('ending_balance');
    	$data['interest_earned'] = $this->input->post('interest_earned');
    	$data['service_charged'] = $this->input->post('service_charged');
    	$data['interest_account_id'] = $this->input->post('interest_account_id');
    	$data['interest_date'] = $this->input->post('interest_date');
    	$data['charged_date'] = $this->input->post('charged_date');
    	$data['expense_account_id'] = $this->input->post('expense_account_id');
    	$data['created_by'] = $this->session->userdata('personnel_id');
    	$this->db->where('recon_id',$recon_id);
    	if($this->db->update('bank_reconcilliation',$data))
    	{
    		

    		return $recon_id;

    	}
    	else
    	{
    		return FALSE;
    	}

    }
}
?>