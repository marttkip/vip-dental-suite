<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/hr/controllers/hr.php";

class Leave extends hr 
{
	function __construct()
	{
		parent:: __construct();
	}
	
	function calender()
	{
		// var_dump(d)
		$v_data['title'] = $data['title'] = 'Leave Schedule';
		
		$v_data['personnel'] = $this->personnel_model->retrieve_personnel();
		$v_data['leave_types'] = $this->personnel_model->get_leave_types();
		
		$data['content'] = $this->load->view('leave/calender', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function leave_schedule()
	{
		$leave_result = $this->leave_model->get_assigned_leave();
		
		//initialize required variables
		$totals = '';
		$highest_bar = 0;
		$r = 0;
		$data = array();
		
		if($leave_result->num_rows() > 0)
		{
			$result = $leave_result->result();
			
			foreach($result as $res)
			{
				$personnel_id = $res->personnel_id;
				$personnel_fname = $res->personnel_fname;
				$personnel_onames = $res->personnel_onames;
				$leave_type_name = $res->leave_type_name;
				$leave_duration_status = $res->leave_duration_status;
				$start_date = $res->start_date;
				$end_date = $res->end_date;
				$start_date = date('D M d Y',strtotime($start_date)); 
				$end_date = date('D M d Y',strtotime($end_date)); 
				$time_start = $start_date.' 8:00 AM:00 GMT+0300'; 
				$time_end = $end_date.' 5:00 PM:00 GMT+0300';
				//$color = $this->reception_model->random_color();
				
				if($leave_duration_status == 1)
				{
					$color = '#0088CC';
				}
				
				else
				{
					$color = '#b71c1c';
				}
				$leave_days = $this->site_model->calculate_leave_days($start_date, $end_date);
				
				$data['title'][$r] = $personnel_fname.' '.$personnel_onames.' - '.$leave_type_name.'. '.$leave_days.' days';
				$data['start'][$r] = $time_start;
				$data['end'][$r] = $time_start;
				$data['backgroundColor'][$r] = $color;
				$data['borderColor'][$r] = $color;
				$data['allDay'][$r] = TRUE;
				$data['url'][$r] = site_url().'human-resource/personnel-leave-detail/'.$personnel_id;
				$r++;
			}
		}
		
		$data['total_events'] = $r;
		echo json_encode($data);
	}
	
	public function view_leave($date)
    {
        $v_data['leave'] = $this->leave_model->get_day_leave($date);
        $v_data['date'] = $date;
		
		$v_data['title'] = $data['title'] = 'Leave starting '.date('jS M Y',strtotime($date));
		
		$v_data['personnel'] = $this->personnel_model->retrieve_personnel();
		$v_data['leave_types'] = $this->personnel_model->get_leave_types();
		
		$data['content'] = $this->load->view('leave/list', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
    }
	
	function add_leave($date, $personnel_id)
	{
		$this->form_validation->set_rules('personnel_id', 'Personnel', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('leave_type_id', 'Leave Type', 'trim|numeric|required|xss_clean');
		// var_dump($_POST);die();
		if ($this->form_validation->run())
		{
			if($this->personnel_model->add_personnel_leave($this->input->post('personnel_id')))
			{

				// send leave 

				
				

				$this->session->set_userdata("success_message", "Personnel leave  added successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add personnel leave. Please try again");
			}
			
			redirect('human-resource/personnel-leave-detail/'.$personnel_id);
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$this->view_leave($date);
		}
	}

	/*
	*
	*	Delete an existing personnel
	*	@param int $personnel_id
	*
	*/
	public function activate_leave($personnel_leave_id, $personnel_id)
	{
		if($this->leave_model->activate_leave_duration($personnel_leave_id))
		{
			$this->session->set_userdata('success_message', 'Leave has been claimed');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Leave could not claimed');
		}
		redirect('human-resource/personnel-leave-detail/'.$personnel_id);
	}

	/*
	*
	*	Delete an existing personnel
	*	@param int $personnel_id
	*
	*/
	public function deactivate_leave($leave_duration_id, $personnel_id)
	{
		if($this->leave_model->deactivate_leave_duration($leave_duration_id))
		{
			$this->session->set_userdata('success_message', 'Leave has been unclaimed');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Leave could not unclaimed');
		}
		redirect('human-resource/personnel-leave-detail/'.$personnel_id);
	}

	/*
	*
	*	Delete an existing personnel
	*	@param int $personnel_id
	*
	*/
	public function delete_leave($leave_duration_id, $personnel_id)
	{
		if($this->leave_model->delete_leave_duration($leave_duration_id))
		{
			$this->session->set_userdata('success_message', 'Leave has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Leave could not deleted');
		}
		redirect('human-resource/personnel-leave-detail/'.$personnel_id);
	}
	
	function add_calender_leave()
	{
		$this->form_validation->set_rules('personnel_id', 'Personnel', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('leave_type_id', 'Leave Type', 'trim|numeric|required|xss_clean');

		if ($this->form_validation->run())
		{
			if($this->personnel_model->add_personnel_leave($this->input->post('personnel_id')))
			{
				$this->session->set_userdata("success_message", "Personnel leave  added successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add personnel leave. Please try again");
			}
			
			redirect('human-resource/leave');
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$this->calender();
		}
	}
	
	public function leave_application($leave_type_id)
	{
		$this->form_validation->set_rules('personnel_id', 'Personnel', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('leave_type_id', 'Leave Type', 'trim|numeric|required|xss_clean');
		
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		
		if($end_date < $start_date)
		{
			$this->session->set_userdata("error_message","The leave end date cannot be earlier than the start date.");
		}
		
		else if ($this->form_validation->run())
		{
			if($this->personnel_model->add_personnel_leave($this->input->post('personnel_id')))
			{

				$dt = date('2021-01-d');
				$first_day = $this->input->post('start_date');
				$last_day = $this->input->post('end_date');
				$branch_id = $this->session->userdata('branch_id');

				$first_name = $this->session->userdata('first_name');


				$leave_type_id = $this->input->post('leave_type_id');


				$this->db->where('leave_type_id',$leave_type_id);
				$leave_type_rs = $this->db->get('leave_type');

				if($leave_type_rs->num_rows() > 0)
				{
					foreach ($leave_type_rs->result() as $key => $value) {
						// code...
						$leave_type_name = $value->leave_type_name;
					}
				}

				$v_data['contacts'] = $contacts = $this->site_model->get_contacts($branch_id);
				
				$v_data['title'] = $branch_name.' Report for '.date('M 2021', strtotime($first_day));
				$data['title'] =  strtoupper($first_name).' '.strtoupper($leave_type_name).' LEAVE REQUEST';


				$v_data['first_day'] = $first_day;
				$v_data['last_day'] = $last_day;
				$v_data['branch_id'] = $branch_id;

				$v_data['text']  = '<strong>Hello Admin</strong>,<br>
									 <strong> '.ucfirst($leave_type_name).'</strong>  Leave Request for<br>
									 <strong>Start Date:</strong> '.date('jS M Y', strtotime($first_day)).'<br>
									 <strong>End Date:</strong> '.date('jS M Y', strtotime($last_day)).'<br>
									 <br>
									 Yours Truly</br>
									 '.strtoupper($first_name).'
									';

				// var_dump($v_data);die();
				$html = $this->load->view('leave/leave_request_mail', $v_data,true);
				// var_dump($html);die();

				$this->load->library('mpdf');
				
				$document_number = date("Ymdhis");
				$receipt_month_date = date('Y-m-d');


				$message['text'] =$text;


				$sender_email = 'info@secvate.com';//$this->config->item('sender_email');//$contacts['email'];
				$shopping = "";
				$from = $sender_email; 
				
				$button = '';
				$sender['email']= $sender_email; 
				$sender['name'] = $contacts['company_name'];
				$receiver['name'] = $subject;
				$payslip = $title;

				$sender_email = $sender_email;
				$tenant_email = $this->config->item('admins_email');
				$email_array = explode('/', $tenant_email);
				$total_rows_email = count($email_array);

				for($x = 0; $x < $total_rows_email; $x++)
				{
					$receiver['email'] = $email_tenant = $email_array[$x];


					$message['subject'] = $data['title'];
					$message['text'] = $html;
					$shopping = ""; 
					
					$button = '';
					
					$response = $this->email_model->send_sendgrid_mail_no_attachment($receiver, $sender, $message);
					// var_dump()
					
				}

				$this->session->set_userdata("success_message", "Leave application successfully pending approval.");
			
				redirect('my-profile');
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not apply for leave. Please try again");
			}
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		$v_data['title'] = $data['title'] = 'Leave Application';
		$v_data['leave_types'] = $this->personnel_model->get_leave_types();
		$v_data['personnel_id'] = $this->session->userdata('personnel_id');
		$v_data['selected_leave_type_id'] = $leave_type_id;
		
		$data['content'] = $this->load->view('leave/leave_application', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function personnel_leaves($personnel_id)
	{

		
		$v_data['personnel'] = $this->personnel_model->get_personnel($personnel_id);

		$v_data['leave'] = $this->personnel_model->get_personnel_leave($personnel_id);
		$v_data['leave_requests'] = $this->personnel_model->get_all_personnel_leaves($personnel_id);
		$v_data['leave_types'] = $this->personnel_model->get_leave_types();
		$row = $v_data['personnel']->row();
		$names  = $row->personnel_fname;
		$data['title'] = $names.' Leaves';
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('leave/personnel_leave', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}
}
?>