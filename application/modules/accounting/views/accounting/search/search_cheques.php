 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Search</h2>
    </header>
    
    <!-- Widget content -->
    <div class="panel-body">
        <div class="padd">
            <?php
            echo form_open("accounting/petty_cash/search_cheques", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Account From</label>
                        <div class="col-lg-8">
                            <select id="account_from_id" name="account_from_id" class="form-control">
                                <option value="0">--- Account ---</option>
                                <?php
                                if($accounts->num_rows() > 0)
                                 {
                                     foreach($accounts->result() as $row):
                                         // $company_name = $row->company_name;
                                         $account_name = $row->account_name;
                                         $account_id = $row->account_id;
                                         $parent_account = $row->parent_account;

                                         if($parent_account != $current_parent)
                                         {
                                              $account_from_name = $this->accounting_model->get_account_name($parent_account);
                                            $changed .= '<optgroup label="'.$account_from_name.'">';
                                         }

                                         $changed .= "<option value=".$account_id."> ".$account_name."</option>";
                                         $current_parent = $parent_account;
                                         if($parent_account != $current_parent)
                                         {
                                            $changed .= '</optgroup>';
                                         }

                                         
                                        
                                     endforeach;
                                 }

                                 echo $changed;
                                ?>
                            </select>
                        </div>
                    </div> 
                    
                </div>
                 <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Account To</label>
                        <div class="col-lg-8">
                            <select id="account_from_id" name="account_to_id" class="form-control">
                                <option value="0">--- Account ---</option>
                                 <?php
                                    $changed = '<option value="">--- Account ---</option>';
                                 if($accounts->num_rows() > 0)
                                 {
                                     foreach($accounts->result() as $row):
                                         // $company_name = $row->company_name;
                                         $account_name = $row->account_name;
                                         $account_id = $row->account_id;
                                         $parent_account = $row->parent_account;

                                         if($parent_account != $current_parent)
                                         {
                                              $account_from_name = $this->accounting_model->get_account_name($parent_account);
                                            $changed .= '<optgroup label="'.$account_from_name.'">';
                                         }

                                         $changed .= "<option value=".$account_id."> ".$account_name."</option>";
                                         $current_parent = $parent_account;
                                         if($parent_account != $current_parent)
                                         {
                                            $changed .= '</optgroup>';
                                         }

                                         
                                        
                                     endforeach;
                                 }
                                 echo $changed;
                                 ?>
                            </select>
                        </div>
                    </div> 
                    
                </div>
            	 <div class="col-md-2">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Date from: </label>
                        
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" id='datetimepicker1' data-plugin-datepicker class="form-control" name="date_from" placeholder="Date from" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-2">
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Date to: </label>
                        
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder="Date to" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
              
                <div class="col-md-2">
		            <div class="center-align">
		                <button type="submit" class="btn btn-info btn-sm">Search Account</button>
		            </div>
		        </div>	


            </div>
                       <?php
            echo form_close();
            ?>
        </div>
    </div>
</section>