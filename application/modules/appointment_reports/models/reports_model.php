
<?php

class Reports_model extends CI_Model 
{
	public function get_queue_total($branch_code = 'OSE', $date = NULL, $where = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		if($where == NULL)
		{
			$where = 'visit.branch_code = \''.$branch_code.'\' AND visit.close_card = 0 AND visit.visit_date = \''.$date.'\' AND visit.visit_delete = 0';
		}
		
		else
		{
			$where .= ' AND visit.branch_code = \''.$branch_code.'\' AND visit.visit_delete = 0 AND visit.close_card = 0 AND visit.visit_date = \''.$date.'\' ';
		}
		
		$this->db->select('COUNT(visit.visit_id) AS queue_total');
		$this->db->where($where);
		$query = $this->db->get('visit');
		
		$result = $query->row();
		
		return $result->queue_total;
	}
	
	public function get_daily_balance($date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		//select the user by email from the database
		$this->db->select('SUM(amount_paid) AS total_amount');
		$this->db->where('cancel = 0 AND payment_type = 1 AND payment_method_id = 2 AND payment_created = \''.$date.'\'');
		$this->db->from('payments');
		$query = $this->db->get();
		
		$result = $query->row();
		
		return $result->total_amount;
	}
	
	public function get_patients_total($branch_code = 'OSE', $date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		$this->db->select('COUNT(visit_id) AS patients_total');
		$this->db->where('visit.branch_code = \''.$branch_code.'\' AND visit_date = \''.$date.'\' AND visit.visit_delete = 0');
		$query = $this->db->get('visit');
		
		$result = $query->row();
		
		return $result->patients_total;
	}

	public function get_totals_items($where_item = NULL)
	{

		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 '.$where_item;
		$table = 'visit, patients, visit_type';


		$visit_report_search = $this->session->userdata('visit_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}

		$this->db->select('COUNT(visit_id) AS patients_total');
		$this->db->where($where);
		$query = $this->db->get($table);
		
		$result = $query->row();
		
		return $result->patients_total;
	}


	public function calculate_distict($item = NULL)
	{

		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0  AND visit.inpatient = 0';
		$table = 'visit, patients, visit_type';


		$visit_report_search = $this->session->userdata('visit_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}

		$this->db->select('visit.patient_id,rip_status');
		$this->db->where($where);
		if($item ==1)
		{
			$this->db->group_by('visit.patient_id');	
		}
		$query = $this->db->get($table);
		$response['total_count'] = $query->num_rows();
		$new_visit = 0;
		$repeat_visit = 0;
		$rip_number=0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$patient_id = $key->patient_id;
				$rip_status = $key->rip_status;

				$last_visit_rs = $this->reception_model->get_if_patients_first_visit($patient_id);

				// var_dump($last_visit_rs); die();
				if($last_visit_rs->num_rows() == 1)
				{	

					
					$new_visit++;
				}
				
				else if($last_visit_rs->num_rows() > 1)
				{	
					$repeat_visit++;
					
				}

				if($rip_status ==1)
				{
					$rip_number++;
				}

				
			}


		}

		$response['new_visit'] = $new_visit;
		$response['repeat_visit'] = $repeat_visit;
		$response['rip_number'] = $rip_number;
		
	
		
		return $response;
	}


	public function get_totals_inpatient_items($where_item = NULL)
	{

		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 '.$where_item;
		$table = 'visit, patients, visit_type';


		$inpatient_report_search = $this->session->userdata('inpatient_report_search');
		
		if(!empty($inpatient_report_search))
		{
			$where .= $inpatient_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}

		$this->db->select('COUNT(visit_id) AS patients_total');
		$this->db->where($where);
		$query = $this->db->get($table);
		
		$result = $query->row();
		
		return $result->patients_total;
	}


	public function calculate_distict_inpatient($item = NULL)
	{

		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0  AND visit.inpatient = 1';
		$table = 'visit, patients, visit_type';


		$inpatient_report_search = $this->session->userdata('inpatient_report_search');
		
		if(!empty($inpatient_report_search))
		{
			$where .= $inpatient_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}

		$this->db->select('visit.patient_id,rip_status');
		$this->db->where($where);
		if($item ==1)
		{
			$this->db->group_by('visit.patient_id');	
		}
		$query = $this->db->get($table);
		$response['total_count'] = $query->num_rows();
		$new_visit = 0;
		$repeat_visit = 0;
		$rip_number=0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$patient_id = $key->patient_id;
				$rip_status = $key->rip_status;

				$last_visit_rs = $this->reception_model->get_if_patients_first_visit($patient_id);

				// var_dump($last_visit_rs); die();
				if($last_visit_rs->num_rows() == 1)
				{	

					
					$new_visit++;
				}
				
				else if($last_visit_rs->num_rows() > 1)
				{	
					$repeat_visit++;
					
				}

				if($rip_status ==1)
				{
					$rip_number++;
				}

				
			}


		}

		$response['new_visit'] = $new_visit;
		$response['repeat_visit'] = $repeat_visit;
		$response['rip_number'] = $rip_number;
		
	
		
		return $response;
	}

	/*
	*	Retrieve visits
	*	@param string $table
	* 	@param string $where
	*	@param int $per_page
	* 	@param int $page
	*
	*/
	public function get_all_visits($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit.*, (visit.visit_time_out - visit.visit_time) AS waiting_time, patients.*, visit_type.visit_type_name');

		// $this->db->join('staff', 'staff.payroll_no = patients.strath_no', 'left');
		// $this->db->join('staff_dependant', 'staff_dependant.staff_dependant_id = patients.dependant_id', 'left');
		$this->db->where($where);
		$this->db->order_by('visit.visit_date, visit.visit_time','DESC');
		$this->db->group_by('visit.visit_id');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}


	/*
	*	Retrieve visits
	*	@param string $table
	* 	@param string $where
	*	@param int $per_page
	* 	@param int $page
	*
	*/
	public function get_all_patient_rip($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');

		// $this->db->join('staff', 'staff.payroll_no = patients.strath_no', 'left');
		// $this->db->join('staff_dependant', 'staff_dependant.staff_dependant_id = patients.dependant_id', 'left');
		$this->db->where($where);
		$this->db->order_by('patients.rip_date','ASC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function get_all_visits_content($table, $where, $order_by, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit.*, (visit.visit_time_out - visit.visit_time) AS waiting_time, patients.*, visit_type.visit_type_name');
		// $this->db->join('staff', 'staff.payroll_no = patients.strath_no', 'left');
		// $this->db->join('staff_dependant', 'staff_dependant.staff_dependant_id = patients.dependant_id', 'left');
		$this->db->where($where);
		$this->db->order_by('visit.visit_date, visit.visit_time','DESC');
		$this->db->group_by('visit.visit_id');
		$query = $this->db->get('');
		
		return $query;
	}

	public function get_all_sick_off_content($table, $where, $order_by, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('patient_leave.*,patient_leave.created_by AS personnel_id,patients.*, visit.department_name');
		$this->db->where($where);
		$this->db->order_by('patient_leave.start_date','DESC');
		$query = $this->db->get('');
		
		return $query;
	}

	public function get_all_visits_sick_offs($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('patient_leave.*, patient_leave.created_by AS personnel_id ,patients.*, visit.department_name, leave_type.leave_type_name');
		$this->db->where($where);
		$this->db->order_by('patient_leave.start_date','DESC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	public function get_all_departments()
	{
		$this->db->distinct('department_name');
		$this->db->select('department_name');
		$this->db->where('department_name IS NOT NULL');
		$query = $this->db->get('visit');
		//var_dump($query); die();
		return $query;
	}
	public function get_all_patient_leave($table, $where, $per_page, $page, $order, $order_method)
	{
		$this->db->from($table);
		//$this->db->join('staff', 'staff.payroll_no = patients.strath_no', 'left');
		$this->db->select('patient_leave.*, patients.*, visit.department_name, leave_type.leave_type_name');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function export_outpatient_report()
	{
		$this->load->library('excel');
		
		//get all transactions
		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 AND visit.inpatient = 0 AND patients.rip_status =0 AND (visit.close_card = 0 OR visit.close_card = 2)';
		$table = 'visit, patients, visit_type';
		$visit_report_search = $this->session->userdata('visit_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}
		
		$this->db->where($where);
		$this->db->order_by('visit.visit_date, visit.visit_time','DESC');
		$this->db->select('visit.*, (visit.visit_time_out - visit.visit_time) AS waiting_time, patients.*, visit_type.visit_type_name');
		$this->db->group_by('visit.visit_id');
		$visits_query = $this->db->get($table);
		
		$title = 'Outpatient Report';

		$personnel_query = $this->personnel_model->get_all_personnel();
		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/

			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Visit Date';
			$report[$row_count][2] = 'Patient No';
			$report[$row_count][3] = 'Patient Name';
			$report[$row_count][4] = 'Gender';
			$report[$row_count][5] = 'Age';
			$report[$row_count][6] = 'Chemo / Review';
			$report[$row_count][7] = 'Visit';
			$report[$row_count][8] = 'D X';
			$report[$row_count][9] = 'RIP';
			$report[$row_count][10] = 'Patient Type';
			$report[$row_count][11] = 'HC Time In';
			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;
				$visit_date =  date('jS M Y',strtotime($row->visit_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}

				
				
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$patient_number = $row->patient_number;

				$strath_no = $row->strath_no;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type_id;
				$visit_type = $row->visit_type;
				$gender_id = $row->gender_id;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				// $first_visit_department = $this->reception_model->first_department($visit_id);
				$visit_type_name = $row->visit_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;
				$last_visit = $row->last_visit;
				// $department_name = $row->department_name;
				$branch_code = $row->branch_code;
				$department = $row->department;
				$inpatient = $row->inpatient;
				// $relative_code = $row->relative_code;
				$referral_reason = $row->referral_reason;
				$rip_status = $row->rip_status;
				$rip_date = $row->rip_date;
				$visit_date1 = $row->visit_date;
				// var_dump($difference);
				if($rip_status == 1  AND $visit_date1 >= $rip_date)
				{
					$rip_status = 'RIP';
				}
				else
				{
					$rip_status = '';
				}
				
				//branch Code
				// if($branch_code =='OSE')
				// {
					$branch_code = 'Main HC';
				// }
				// else
				// {
				// 	$branch_code = 'Oserengoni';
				// }
				
				$close_card = $row->close_card;
				if($close_card == 1)
				{
					$visit_time_out = date('jS M Y H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}
				$last_visit_rs = $this->reception_model->get_if_patients_first_visit($patient_id);
				// var_dump($last_visit_rs); die();
				if($last_visit_rs->num_rows() > 1)
				{
					$last_visit_name = 'Re Visit';
				}
				
				else
				{
					$last_visit_name = 'First Visit';
				}

				if($gender_id == 1)
				{
					$gender = 'Male';
				}
				else
				{
					$gender = 'Female';
				}

				// this is to check for any credit note or debit notes
				$payments_value = $this->accounts_model->total_payments($visit_id);

				$invoice_total = $this->accounts_model->total_invoice($visit_id);

				$balance = $this->accounts_model->balance($payments_value,$invoice_total);
				// end of the debit and credit notes


				//creators and editors
				if($personnel_query->num_rows() > 0)
				{
					$personnel_result = $personnel_query->result();
					
					foreach($personnel_result as $adm)
					{
						$personnel_id2 = $adm->personnel_id;
						
						if($personnel_id == $personnel_id2)
						{
							$doctor = $adm->personnel_onames.' '.$adm->personnel_fname;
							break;
						}
						
						else
						{
							$doctor = '-';
						}
					}
				}
				
				else
				{
					$doctor = '-';
				}


				if($inpatient == 0)
				{
					$patient_type = 'Outpatient';
				}
				else
				{
					$patient_type = 'Inpatient';
				}
				

				$age = $this->reception_model->calculate_age($patient_date_of_birth);


				$diagnosis_rs = $this->nurse_model->get_visit_diagnosis($visit_id);
				$diagnosis = '';
				if($diagnosis_rs->num_rows() > 0)
				{
					foreach ($diagnosis_rs->result() as $key_other) {
						# code...
						$diseases_name = $key_other->diseases_name;
						$diseases_code = $key_other->diseases_code;

						$diagnosis .= $diseases_name.'  '.$diseases_code.' ';
					}
				}

				$count++;
				
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $visit_date;
				$report[$row_count][2] = $patient_number;
				$report[$row_count][3] = $patient_surname.' '.$patient_othernames;
				$report[$row_count][4] = $gender;
				$report[$row_count][5] = $age;
				$report[$row_count][6] = '-';
				$report[$row_count][7] = $last_visit_name;
				$report[$row_count][8] = $diagnosis;
				$report[$row_count][9] = $rip_status;
				$report[$row_count][10] = $patient_type;
				$report[$row_count][11] = $visit_time;
					
				
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	
	}

	public function export_inpatient_report()
	{
		$this->load->library('excel');
		
		//get all transactions
		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 AND visit.inpatient = 1 AND patients.rip_status =0 AND (visit.close_card = 0 OR visit.close_card = 2)';
		$table = 'visit, patients, visit_type';
		$inpatient_report_search = $this->session->userdata('inpatient_report_search');
		
		if(!empty($inpatient_report_search))
		{
			$where .= $inpatient_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';

		}
		
		$this->db->where($where);
		$this->db->order_by('visit.visit_date, visit.visit_time','DESC');
		$this->db->select('visit.*, (visit.visit_time_out - visit.visit_time) AS waiting_time, patients.*, visit_type.visit_type_name');
		$this->db->group_by('visit.visit_id');
		$visits_query = $this->db->get($table);
		
		$title = 'Inpatient Report';

		$personnel_query = $this->personnel_model->get_all_personnel();
		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Patient No';
			$report[$row_count][2] = 'Patient Name';
			$report[$row_count][3] = 'Gender';
			$report[$row_count][4] = 'Age';
			$report[$row_count][5] = 'Date of Admission';
			$report[$row_count][6] = 'Status';
			$report[$row_count][7] = 'D X';
			$report[$row_count][8] = 'RIP';
			$report[$row_count][9] = 'HC Time In';
			$report[$row_count][10] = 'HC Time Out';
			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;
				$visit_date = date('jS M Y',strtotime($row->visit_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}
				
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$patient_number = $row->patient_number;

				$strath_no = $row->strath_no;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type_id;
				$visit_type = $row->visit_type;
				$gender_id = $row->gender_id;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				// $first_visit_department = $this->reception_model->first_department($visit_id);
				$visit_type_name = $row->visit_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;
				$last_visit = $row->last_visit;
				// $department_name = $row->department_name;
				$branch_code = $row->branch_code;
				$department = $row->department;
				$inpatient = $row->inpatient;
				$rip_status = $row->rip_status;
				// $relative_code = $row->relative_code;
				$referral_reason = $row->referral_reason;
				
				//branch Code
				// if($branch_code =='OSE')
				// {
					$branch_code = 'Main HC';
				// }
				// else
				// {
				// 	$branch_code = 'Oserengoni';
				// }
				
				$close_card = $row->close_card;
				if($close_card == 1)
				{
					$visit_time_out = date('jS M Y H:i a',strtotime($row->visit_time_out));
					$close_card_status = 'Discharged';
				}
				else if($close_card == 0)
				{
					$close_card_status = 'Patient Admitted';
					$visit_time_out = '-';
				}
				else 
				{
					$close_card_status = 'Discharged In';
					$visit_time_out = '-';
				}
				$last_visit_rs = $this->reception_model->get_if_patients_first_visit($patient_id);
				// var_dump($last_visit_rs); die();
				if($last_visit_rs->num_rows() > 1)
				{
					$last_visit_name = 'Re Visit';
				}
				
				else
				{
					$last_visit_name = 'First Visit';
				}

				if($gender_id == 1)
				{
					$gender = 'Male';
				}
				else
				{
					$gender = 'Female';
				}


				if($gender_id == 1)
				{
					$gender = 'Male';
				}
				else
				{
					$gender = 'Female';
				}
				if($rip_status == 1)
				{
					$rip_status = 'RIP';
				}
				else
				{
					$rip_status = '';
				}

				// this is to check for any credit note or debit notes
				$payments_value = $this->accounts_model->total_payments($visit_id);

				$invoice_total = $this->accounts_model->total_invoice($visit_id);

				$balance = $this->accounts_model->balance($payments_value,$invoice_total);
				// end of the debit and credit notes


				//creators and editors
				if($personnel_query->num_rows() > 0)
				{
					$personnel_result = $personnel_query->result();
					
					foreach($personnel_result as $adm)
					{
						$personnel_id2 = $adm->personnel_id;
						
						if($personnel_id == $personnel_id2)
						{
							$doctor = $adm->personnel_onames.' '.$adm->personnel_fname;
							break;
						}
						
						else
						{
							$doctor = '-';
						}
					}
				}
				
				else
				{
					$doctor = '-';
				}


				if($inpatient == 0)
				{
					$patient_type = 'Outpatient';
				}
				else
				{
					$patient_type = 'Inpatient';
				}
				
				
				

				$age = $this->reception_model->calculate_age($patient_date_of_birth);


				$diagnosis_rs = $this->nurse_model->get_visit_diagnosis($visit_id);
				$diagnosis = '';
				if($diagnosis_rs->num_rows() > 0)
				{
					foreach ($diagnosis_rs->result() as $key_other) {
						# code...
						$diseases_name = $key_other->diseases_name;

						$diseases_code = $key_other->diseases_code;

						$diagnosis .= $diseases_name.'  '.$diseases_code.' ';
					}
				}


				$count++;
				
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $patient_number;
				$report[$row_count][2] = $patient_surname.' '.$patient_othernames;
				$report[$row_count][3] = $gender;
				$report[$row_count][4] = $age;
				$report[$row_count][5] = $visit_date;
				$report[$row_count][6] = $close_card_status;
				$report[$row_count][7] = $diagnosis;
				$report[$row_count][8] = $rip_status;
				$report[$row_count][9] = $visit_time;
				$report[$row_count][10] = $visit_time_out;
					
				
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	
	}

	public function get_all_patients_appointments($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*,appointments.appointment_id as appointment_idd');
		$this->db->where($where);
		$this->db->order_by('visit.visit_date','DESC');
		$this->db->join('visit_type','visit_type.visit_type_id = visit.visit_type','left');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}


	/*
	*	Export Time report
	*
	*/
	function export_patients_records($category_id = null,$start_date,$end_date)
	{
		$this->load->library('excel');
		
		// if($category_id > 0)
		// {
		// 	$add_category = ' AND category_id = '.$category_id;
		// 	$patient_search = NULL;
		// }
		// else
		// {
		// 	$patient_search = $this->session->userdata('patient_search');
		// 	$add_category = '';
		// }


		if(!empty($start_date) and !empty($end_date))
		{
			$add_category = ' AND DATE(patient_date) BETWEEN "'.$start_date.'" AND "'.$end_date.'" ';
			$patient_search = NULL;
		}
		else if(!empty($start_date) AND empty($end_date))
		{
			$patient_search = $this->session->userdata('patient_search');
			$add_category = ' AND DATE(patient_date) = "'.$start_date.'" ';
		}
		else if(!empty($start_date) AND empty($end_date))
		{
			$add_category = ' AND DATE(patient_date) = "'.$end_date.'" ';
		}
		else
		{
			$add_category = '';	
		}
		//get all transactions
		$where = 'patient_delete = 0 '.$add_category;
		$table = 'patients';
		
		//$where = '(visit_type_id <> 2 OR visit_type_id <> 1) AND patient_delete = '.$delete;
		
		if(!empty($patient_search))
		{
			$where .= $patient_search;
		}
		
		else
		{
			// $where .= ' AND patients.branch_code = \''.$this->session->userdata('branch_code').'\'';
			$where .='';
		}
		
		$this->db->where($where);
		$this->db->order_by('prefix,suffix', 'ASC');
		$this->db->select('patients.*,branch.branch_name');
		$this->db->join('relationship','relationship.relationship_id = patients.relationship_id','left');
		$this->db->join('branch','branch.branch_id = patients.branch_id','left');
		$visits_query = $this->db->get($table);
		
		$title = 'Patients Export as at '.date('jS M Y',strtotime($start_date)).' to '.date('jS M Y',strtotime($end_date));
		// var_dump($visits_query); die();
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/

			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'New Patient Number';
			$report[$row_count][2] = 'Patient Number';
			$report[$row_count][3] = 'Patient';
			$report[$row_count][4] = 'Phone';
			$report[$row_count][5] = 'Alternate Phone';
			$report[$row_count][6] = 'Email';
			$report[$row_count][7] = 'Next Of Kin';
			$report[$row_count][8] = 'Next of Kin Phone';
			$report[$row_count][9] = 'Registration Date';		
			$report[$row_count][10] = 'Scheme Name';
			$report[$row_count][11] = 'Last Appointment Date';
			$report[$row_count][12] = 'Branch';

			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;
				$registration_date = date('jS M Y',strtotime($row->patient_date));
				$patient_id = $row->patient_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$created_by = $row->created_by;
				$modified_by = $row->modified_by;
				$deleted_by = $row->deleted_by;
				$visit_type_id = $row->visit_type_id;
				$created = $row->patient_date;
				$last_modified = $row->last_modified;
				$patient_year = $row->patient_year;
				$last_visit = $row->last_visit;
				$patient_phone1 = $row->patient_phone1;
				$patient_number = $row->patient_number;
				$patient_email = $row->patient_email;
				$patient_surname = $row->patient_surname;
				$patient_othernames = $row->patient_othernames;
				$patient_first_name = '';
				$patient_postalcode = $row->patient_postalcode;
				$patient_date_of_birth = date('jS M Y',strtotime($row->patient_date_of_birth));
				$place_of_work = '';//$row->place_of_work;
				$occupation = '';//$row->occupation;
				$patient_address = $row->patient_address;
				$patient_town = $row->patient_town;
				$last_visit = $row->last_visit;
				$age_group = '';//$row->age_group;
				$relationship_name = '';
				$patient_kin_sname = $row->patient_kin_sname;
				$patient_kin_othernames = $row->patient_kin_othernames;
				$patient_kin_phonenumber1 = $row->patient_kin_phonenumber1;
				$patient_phone2 = $row->patient_phone2;
				$patient_date = $row->patient_date;
				$branch_name = $row->branch_name;
				$new_patient_number = '';//$row->new_patient_number;
				if($patient_date != NULL)
				{
					$patient_date = date('jS M Y',strtotime($patient_date));
				}
				
				else
				{
					$patient_date = '';
				}
				

				$insurance_company = $this->reception_model->get_patient_insurance_company($patient_id);

				$last_visit_date = $this->reception_model->get_last_visit_date($patient_id);
				// var_dump($last_visit_date);die();
				if($last_visit_date != NULL)
				{
					$last_visit_date = date('jS M Y',strtotime($last_visit_date));
				}
				
				else
				{
					$last_visit_date = '';
				}

				if($last_visit_date == "0000-00-00")
				{
					$last_visit_date = '';
				}

				if($age_group == "A")
				{
					$age_group = "Adult";
				}
				else if($age_group == "D")
				{
					$age_group = "Dependant";
				}
				else
				{
					$age_group ="";
				}
				$count++;
				
				if($last_visit == "0000-00-00")
				{
					$last_visit = '';
				}

				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $new_patient_number;
				$report[$row_count][2] = $patient_number;
				$report[$row_count][3] = $patient_surname;
				$report[$row_count][4] = $patient_phone1;
				$report[$row_count][5] = $patient_phone2;
				$report[$row_count][6] = $patient_email;
				$report[$row_count][7] = $patient_kin_sname;
				$report[$row_count][8] = $patient_kin_phonenumber1;
				$report[$row_count][9] = $patient_date;					
				$report[$row_count][10] = $insurance_company;			
				$report[$row_count][11] = $last_visit_date;
				$report[$row_count][12] = $branch_name;
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	public function export_bookings_records($type_id = null,$start_date,$end_date)
	{
		$this->load->library('excel');
		
		
		if($type_id == 1)
		{
			$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0  AND (appointments.appointment_status = 4 OR appointments.appointment_status = 7 OR appointments.appointment_status = 1) AND visit.visit_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" ';
			$table = 'patients,visit,appointments';
		}
		else if($type_id == 2)
		{
			$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND (appointments.appointment_status = 4 OR appointments.appointment_status = 7) AND appointments.appointment_rescheduled = 0  AND visit.visit_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" ';
			$table = 'patients,visit,appointments';
		}
		else if($type_id == 3)
		{
			$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0  AND appointments.appointment_rescheduled = 1  AND visit.visit_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" ';
			$table = 'patients,visit,appointments';
		}
		else if($type_id == 4)
		{
			$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND ( appointments.appointment_status = 1) AND appointments.appointment_rescheduled = 0  AND visit.visit_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" ';
			$table = 'patients,visit,appointments';
		}

		
		$this->db->where($where);
		$this->db->order_by('patients.prefix,patients.suffix', 'ASC');
		$this->db->select('patients.*,visit.*,appointments.*,personnel.personnel_onames,visit_type.visit_type_name,branch.branch_name');
		$this->db->order_by('visit.visit_date');
		$this->db->join('personnel','personnel.personnel_id = visit.personnel_id','left');
		$this->db->join('visit_type','visit_type.visit_type_id = visit.visit_type','left');
		$this->db->join('branch','branch.branch_id = visit.branch_id','left');
		$visits_query = $this->db->get($table);
		
		$title = 'Patients Booking Export as at '.date('jS M Y',strtotime($start_date)).' to '.date('jS M Y',strtotime($end_date));
		// var_dump($visits_query); die();
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/

			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Patient';
			$report[$row_count][2] = 'Patient Type';
			$report[$row_count][3] = 'Patient Number';
			$report[$row_count][4] = 'Patient Phone';
			$report[$row_count][5] = 'Visit Type';
			$report[$row_count][6] = 'Visit Date';
			$report[$row_count][7] = 'Appointment Time';
			$report[$row_count][8] = 'Appointment Duration';
			$report[$row_count][9] = 'Description';
			$report[$row_count][10] = 'Doctor';		
			$report[$row_count][11] = 'Appointment Status';
			$report[$row_count][12] = 'Branch Name';

			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $res)
			{
				$row_count++;
				$visit_date = date('jS M Y',strtotime($res->appointment_date));
				$appointment_start_time = $res->appointment_start_time; 
				$appointment_end_time = $res->appointment_end_time; 
				$time_start = $res->appointment_date_time_start; 
				$time_end = $res->appointment_date_time_end;
				$patient_id = $res->patient_id;
				$patient_othernames = $res->patient_othernames;
				$patient_surname = $res->patient_surname;	
				$category_id = $res->category_id;	
				$patient_number = $res->patient_number;				
				$visit_id = $res->visit_id;
				$appointment_id = $res->appointment_id;
				$resource_id = $res->resource_id;
				$event_name = $res->event_name;
				$event_description = $res->event_description;
				$appointment_status = $res->appointment_status;
				$procedure_done = $res->procedure_done;
				$patient_phone1 = $res->patient_phone1;
				$new_patient_number = '';//$res->new_patient_number;
				$uncategorised_patient_number = '';//$res->uncategorised_patient_number;
				$resource_id = $res->resource_id;
				$patient_data = $patient_surname.' '.$patient_othernames;
				$patient_phone1 = $res->patient_phone1;
				$patient_email = $res->patient_email;
				$visit_type_name = $res->visit_type_name;
				$time_in_clinic = NULL;//$res->time_in_clinic;
				$time_out_clinic = NULL;//$res->time_out_clinic;
				$personnel_onames = $res->personnel_onames;
				$branch_name = $res->branch_name;

				$appointment_rescheduled = $res->appointment_rescheduled;


				if(!empty($time_out_clinic) )
				{
					$time_out_clinic = date('H:i a',strtotime($res->time_out_clinic));
					$seconds = strtotime($res->time_out_clinic) - strtotime($res->time_in_clinic);//$row->waiting_time;
					$days    = floor($seconds / 86400);
					$hours   = floor(($seconds - ($days * 86400)) / 3600);
					$minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);
					$seconds = floor(($seconds - ($days * 86400) - ($hours * 3600) - ($minutes*60)));
					
					//$total_time = date('H:i',(strtotime($row->visit_time_out) - strtotime($row->visit_time)));//date('H:i',$row->waiting_time);
					$total_time = $days.' '.$hours.':'.$minutes.':'.$seconds;
				}
				else
				{
					$visit_time_out = '-';
					$total_time = '-';
				}
				

				if($appointment_status == 0)
				{
					$color = 'blue';
					$status_name = 'No show';
				}
				else if($appointment_status == 1)
				{
					$color = 'blue';
					$status_name = 'No show';
				}
				else if($appointment_status == 2)
				{
					$color = 'green';
					$status_name = 'Honoured';
				}
				else if($appointment_status == 3)
				{
					$color = 'red';
					$status_name = 'Cancelled';
				}
				else if($appointment_status == 4)
				{
					$color = 'purple';
					$status_name = 'Honoured';
				}
				else if($appointment_status == 5)
				{
					$color = 'black';
					$status_name = 'Not honoured';
				}
				else if($appointment_status == 6)
				{
					$color = 'DarkGoldenRod';
					$status_name = 'Notified';
				}
				else if($appointment_status == 7)
				{
					$color = 'purple';
					$status_name = 'Honoured';
				}
				else
				{
					$color = 'orange';
					$status_name = '';
				}


				if(empty($patient_data))
				{
					$patient_data = '';
				}
				if(empty($procedure_done))
				{
					$procedure_done = '';
				}
				

				if($category_id == 1)
				{
					// new patient
					$patient_number = $new_patient_number;
					$number_color = 'N.P';
					$buttons = '<td><a href="'.site_url().'reception/delete_patient/'.$patient_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this patient details ? \')"><i class="fa fa-trash"></i></a></td>';
				}
				else if($category_id == 2)
				{
					$patient_number = $patient_number;
					$number_color = 'E.P';
					$buttons = '';
				}
				else
				{
					$patient_number = $uncategorised_patient_number;
					$number_color = 'U.P';
					$buttons = '';
				}

				if($appointment_rescheduled == 1)
				{
					$status_name = 'Rescheduled';
				}
				$count++;
				$row_count++;

				

				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $patient_surname;
				$report[$row_count][2] = $number_color;
				$report[$row_count][3] = $patient_number;
				$report[$row_count][4] = $visit_type_name;
				$report[$row_count][5] = $patient_phone1;
				$report[$row_count][6] = $visit_date;
				$report[$row_count][7] = $appointment_start_time.' - '.$appointment_end_time;
				$report[$row_count][8] = $total_time;
				$report[$row_count][9] = $event_description;
				$report[$row_count][10] = $personnel_onames;			
				$report[$row_count][11] = $status_name;
				$report[$row_count][12] = $branch_name;
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	public function get_all_appointments($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit.*,patients.*,visit_type.visit_type_name,appointments.*,visit.appointment_id AS appt_status');
		$this->db->where($where);
		$this->db->order_by('visit.visit_time_in','ASC');
		$this->db->join('visit_type','visit.visit_type = visit_type.visit_type_id','left');
		$this->db->join('personnel','visit.personnel_id = personnel.personnel_id','left');
		$this->db->join('appointments','appointments.visit_id = visit.visit_id AND appointments.appointment_delete = 0','left');
		
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function export_appointment_reports()
	{


		$this->load->library('excel');
		
	

		$where = 'visit.visit_delete = 0 AND visit.patient_id = patients.patient_id AND visit_delete = 0 AND patients.category_id = 2 AND patients.patient_delete = 0 AND visit.close_card >= 3 ';
	
		$table = 'visit,patients';


		$visit_search = $this->session->userdata('appointment_search_query');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
			
		}
		else
		{
			$where .= ' AND visit.visit_date = "'.date('Y-m-d').'" ';
		
		}


		$this->db->select('visit.*,patients.*,visit_type.visit_type_name,appointments.*,visit.appointment_id AS appt_status');
		$this->db->where($where);
		$this->db->order_by('visit.visit_time_in','ASC');
		$this->db->join('visit_type','visit.visit_type = visit_type.visit_type_id','left');
		$this->db->join('personnel','visit.personnel_id = personnel.personnel_id','left');
		$this->db->join('appointments','appointments.visit_id = visit.visit_id AND appointments.appointment_delete = 0','left');
	
		$visits_query = $this->db->get($table);
		
		$title = 'Patients Appointment Export ';
		// var_dump($visits_query); die();
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
				
			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Patient Number';
			$report[$row_count][2] = 'Patient ';
			$report[$row_count][3] = 'Email';
			$report[$row_count][4] = 'Phone';
			$report[$row_count][5] = 'Appointment Date';
			$report[$row_count][6] = 'Doctor';
			$report[$row_count][7] = 'Procedure';
			$report[$row_count][8] = 'Cost';
			$report[$row_count][9] = 'Account';		
			$report[$row_count][10] = 'Time In';
			$report[$row_count][11] = 'Time of Admission';
			$report[$row_count][12] = 'Time Discharged';
			$report[$row_count][13] = 'Time in Clinic';
			$report[$row_count][14] = 'Special Notes';
			$report[$row_count][15] = 'Feedback on appointment';
			//get & display all services
			$personnel_query = $this->personnel_model->get_all_personnel();
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
				$visit_date = date('jS M Y',strtotime($row->visit_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				$visit_time_out = $row->visit_time_out;

				if(!empty($visit_time_out) OR $visit_time_out != NULL)
				{
					$visit_time_out_new = date('h:i a',strtotime($row->visit_time_out));

				}
				else
				{
					$visit_time_out_new = '-';
				}

				$time_taken = $row->visit_time_taken;

				if(!empty($time_taken) OR $time_taken != NULL)
				{
					$visit_time_taken = date('h:i a',strtotime($time_taken));
				}
				else
				{
					$visit_time_taken = '-';
				}
				$visit_time = $row->visit_time_in;

				if(!empty($row->visit_time_in) OR $visit_time_in !=NULL)
				{

					$visit_time = date('H:i a',strtotime($row->visit_time_in));
				}
				else
				{
					$visit_time = '';
				}

				$visit_time_in = date('H:i a',strtotime($row->visit_time_in));
				
				if(!empty($row->visit_time_out))
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
					$seconds = strtotime($row->visit_time_out) - strtotime($row->visit_time_in);//$row->waiting_time;
					// var_dump($row->visit_time_out);die();
					$days    = floor($seconds / 86400);
					$hours   = floor(($seconds - ($days * 86400)) / 3600);
					$minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);
					$seconds = floor(($seconds - ($days * 86400) - ($hours * 3600) - ($minutes*60)));
					
					//$total_time = date('H:i',(strtotime($row->visit_time_out) - strtotime($row->visit_time)));//date('H:i',$row->waiting_time);
					$total_time = $days.' '.$hours.':'.$minutes.':'.$seconds;
				}
				else
				{
					$visit_time_out = '-';
					$total_time = '-';
				}

				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$past_visit_date = $row->visit_date;
				$personnel_id3 = $row->personnel_id;
				$insurance_description = $row->insurance_description;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type_id;
				$patient_first_name = $row->patient_first_name;
				$visit_time_in = $row->visit_time_in;
				$visit_type = $row->visit_type;
				// $accounts = $row->accounts;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				// $coming_from = $this->reception_model->coming_from($visit_id);
				// $sent_to = $this->reception_model->going_to($visit_id);
				$visit_type_name = $row->visit_type_name;
				$patient_othernames = $row->patient_othernames;
				$appointment_id = $row->appointment_id;
				$patient_number = $row->patient_number;
				$current_patient_number = $row->current_patient_number;
				$patient_surname = $row->patient_surname;
				$close_card = $row->close_card;
				$patient_phone1 = $row->patient_phone1;
				$patient_email = $row->patient_email;

				$time_start = $row->time_start;

				$patient_year = $row->patient_year;
				$patient_date_of_birth = $row->patient_date_of_birth;
				$appointment_start_time = $row->appointment_start_time;
				$appointment_end_time = $row->appointment_end_time;
				$appt_status = $row->appt_status;
				$event_description = $row->event_description;

				// var_dump($appt_status);die();

				
				if($appt_status == 1)
				{
					$appointment_start_time = date('H:i a',strtotime($row->appointment_start_time));
					$appointment_date = date('d.m.Y',strtotime($row->appointment_date));
				}
				else
				{
					$appointment_date = date('d.m.Y',strtotime($row->visit_date));
				}


				//creators and editors
				if($personnel_query->num_rows() > 0)
				{
					$personnel_result = $personnel_query->result();
					
					foreach($personnel_result as $adm)
					{
						$personnel_id2 = $adm->personnel_id;
						
						if($personnel_id3 == $personnel_id2)
						{
							$doctor = 'Dr. '.$adm->personnel_onames;
							break;
						}
						
						else
						{
							$doctor = '-';
						}
					}
				}
				
				else
				{
					$doctor = '-';
				}
				
				//cash paying patient sent to department but has to pass through the accounts
				$bill_amount = $this->accounts_model->get_days_invoice_total($patient_id,$row->visit_date);

				$count++;

				// var_dump($visit_id);die();
				$summary_notes ='';
				if($visit_id > 0)
				{
					$visit_order = 'visit_id';		    
					$visit_where = 'visit_id = '.$visit_id;
					$visit_table = 'special_notes_list';

					$notes_rs = $this->reception_model->get_all_visit_type_details($visit_table, $visit_where,$visit_order);

					$counting = $notes_rs->num_rows();

					// var_dump($counting);die();
					
					if($counting > 0)
					{
						foreach ($notes_rs->result() as $key => $value) {
							# code...

							$summary_notes .= $value->summary_notes;

							// var_dump($summary_notes);die();
						}
						
					}


				}
				

			
				if(empty($insurance_description))
				{
					$insurance_description = '-';
				}


								

				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $patient_number;
				$report[$row_count][2] = $patient_surname.' '.$patient_othernames.' '.$patient_first_name;
				$report[$row_count][3] = $patient_email;
				$report[$row_count][4] = $patient_phone1;
				$report[$row_count][5] = $appointment_date;
				$report[$row_count][6] = $doctor;
				$report[$row_count][7] = $event_description;
				$report[$row_count][8] = number_format($bill_amount,2);
				$report[$row_count][9] = $visit_type_name;					
				$report[$row_count][10] = $visit_time;			
				$report[$row_count][11] = $visit_time_taken;
				$report[$row_count][12] = $visit_time_out;
				$report[$row_count][13] = $time_taken;
				$report[$row_count][14] = $summary_notes;
				$report[$row_count][15] = '';
				
			}
		}

		// var_dump($report);die();
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);

	}

}


?>