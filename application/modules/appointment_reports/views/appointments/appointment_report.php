<div class="row">
<!-- search -->
<?php echo $this->load->view('search/search_appointment_report', '', TRUE);?>
<!-- end search -->
 
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?> for <?php echo date('jS M Y',strtotime(date('Y-m-d')));?></h2>
          <div class="widget-icons pull-right" style="margin-top: -25px;">
             <a href="<?php echo site_url().'export-appointments-report'?>" target="_blank" class="btn btn-success btn-sm" >Export Appointment Report </a>
        	
          </div>
    </header>
      <div class="panel-body">
        <div class="padd">
          
		<?php
		$search = $this->session->userdata('appointment_search_query');
		
		if(!empty($search))
		{
			echo '<a href="'.site_url().'reports/close_appointment_search" class="btn btn-warning">Close Search</a>';
		}
		$result = '';
		$queue_one = '';
		$queue_two = '';
		// var_dump($query->num_rows()); die();
		//if users exist display them
		$count_one = 0;
		$count_two = 0;
		if ($query->num_rows() > 0)
		{
			$count = $page;
				
			
			$result .= 
				'
					<table class="table table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Patient No</th>
						  <th>Patient</th>
						  <th>Email</th>
						  <th>Phone</th>
						  <th>Appointment Date</th>
						  <th>Doctor</th>
						  <th>Procedure</th>	
						  <th>Cost</th>
						  <th>Account</th>
						  <th>Time In</th>
						  <th>Time of Admission</th>
						  <th>Time Discharged</th>
						  <th>Time in Clinic</th>
						  <th>Special Notes</th>
						  <th>Feedback on appointment</th>
						</tr>
					  </thead>
					  <tbody>
				';
			
			$personnel_query = $this->personnel_model->get_all_personnel();
			
			foreach ($query->result() as $row)
			{
				$visit_date = date('jS M Y',strtotime($row->visit_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				$visit_time_out = $row->visit_time_out;

				if(!empty($visit_time_out) OR $visit_time_out != NULL)
				{
					$visit_time_out_new = date('h:i a',strtotime($row->visit_time_out));

				}
				else
				{
					$visit_time_out_new = '-';
				}

				$time_taken = $row->visit_time_taken;

				if(!empty($time_taken) OR $time_taken != NULL)
				{
					$visit_time_taken = date('h:i a',strtotime($time_taken));
				}
				else
				{
					$visit_time_taken = '-';
				}
				$visit_time = $row->visit_time_in;

				if(!empty($row->visit_time_in) OR $visit_time_in !=NULL)
				{

					$visit_time = date('H:i a',strtotime($row->visit_time_in));
				}
				else
				{
					$visit_time = '';
				}

				$visit_time_in = date('H:i a',strtotime($row->visit_time_in));
				
				if(!empty($row->visit_time_out))
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
					$seconds = strtotime($row->visit_time_out) - strtotime($row->visit_time_in);//$row->waiting_time;
					// var_dump($row->visit_time_out);die();
					$days    = floor($seconds / 86400);
					$hours   = floor(($seconds - ($days * 86400)) / 3600);
					$minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);
					$seconds = floor(($seconds - ($days * 86400) - ($hours * 3600) - ($minutes*60)));
					
					//$total_time = date('H:i',(strtotime($row->visit_time_out) - strtotime($row->visit_time)));//date('H:i',$row->waiting_time);
					$total_time = $days.' '.$hours.':'.$minutes.':'.$seconds;
				}
				else
				{
					$visit_time_out = '-';
					$total_time = '-';
				}

				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$past_visit_date = $row->visit_date;
				$personnel_id3 = $row->personnel_id;
				$insurance_description = $row->insurance_description;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type_id;
				$patient_first_name = $row->patient_first_name;
				$visit_time_in = $row->visit_time_in;
				$visit_type = $row->visit_type;
				// $accounts = $row->accounts;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				// $coming_from = $this->reception_model->coming_from($visit_id);
				// $sent_to = $this->reception_model->going_to($visit_id);
				$visit_type_name = $row->visit_type_name;
				$patient_othernames = $row->patient_othernames;
				$appointment_id = $row->appointment_id;
				$patient_number = $row->patient_number;
				$current_patient_number = $row->current_patient_number;
				$patient_surname = $row->patient_surname;
				$close_card = $row->close_card;
				$patient_phone1 = $row->patient_phone1;
				$patient_email = $row->patient_email;

				$time_start = $row->time_start;

				$patient_year = $row->patient_year;
				$patient_date_of_birth = $row->patient_date_of_birth;
				$appointment_start_time = $row->appointment_start_time;
				$appointment_end_time = $row->appointment_end_time;
				$appt_status = $row->appt_status;
				$event_description = $row->event_description;

				// var_dump($appt_status);die();

				
				if($appt_status == 1)
				{
					$appointment_start_time = date('H:i a',strtotime($row->appointment_start_time));
					$appointment_date = date('d.m.Y',strtotime($row->appointment_date));
				}
				else
				{
					$appointment_date = date('d.m.Y',strtotime($row->visit_date));
				}


				//creators and editors
				if($personnel_query->num_rows() > 0)
				{
					$personnel_result = $personnel_query->result();
					
					foreach($personnel_result as $adm)
					{
						$personnel_id2 = $adm->personnel_id;
						
						if($personnel_id3 == $personnel_id2)
						{
							$doctor = 'Dr. '.$adm->personnel_onames;
							break;
						}
						
						else
						{
							$doctor = '-';
						}
					}
				}
				
				else
				{
					$doctor = '-';
				}
				
				//cash paying patient sent to department but has to pass through the accounts
				$bill_amount = $this->accounts_model->get_days_invoice_total($patient_id,$row->visit_date);

				$count++;

				// var_dump($visit_id);die();
				$summary_notes ='';
				if($visit_id > 0)
				{
					$visit_order = 'visit_id';		    
					$visit_where = 'visit_id = '.$visit_id;
					$visit_table = 'special_notes_list';

					$notes_rs = $this->reception_model->get_all_visit_type_details($visit_table, $visit_where,$visit_order);

					$counting = $notes_rs->num_rows();

					// var_dump($counting);die();
					
					if($counting > 0)
					{
						foreach ($notes_rs->result() as $key => $value) {
							# code...

							$summary_notes .= $value->summary_notes;

							// var_dump($summary_notes);die();
						}
						
					}


				}
				

			
				if(empty($insurance_description))
				{
					$insurance_description = '-';
				}

				$count_two++;

			
				$queue_two .= '<tr >
									<td>'.$count_two.'</td>
									<td>'.$patient_number.'</td>
									<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_first_name.'</td>
									<td>'.$patient_email.'</td>
									<td>'.$patient_phone1.'</td>
									<td>'.$appointment_date.'</td>
									<td>'.$doctor.'</td>
									<td>'.$event_description.'</td>
									<td>'.number_format($bill_amount,2).'</td>
									<td>'.$visit_type_name.'</td>
									<td>'.$visit_time.'</td>
									<td>'.$visit_time_taken.' </td>
									<td>'.$visit_time_out.'</td>
									<td>'.$total_time.'</td>
									<td>'.$summary_notes.'</td>
									<td></td>
								</tr> ';
				
			
			}
			$result .= $queue_two;
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no patients";
		}
		
?>
		<?php
		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger">'.$error.'</div>';
			$this->session->unset_userdata('error_message');
		}
		
		if(!empty($success))
		{
			echo '<div class="alert alert-success">'.$success.'</div>';
			$this->session->unset_userdata('success_message');
		}
		echo $result;
		?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        </div>
        <!-- Widget ends -->
       

  </section>
</div>