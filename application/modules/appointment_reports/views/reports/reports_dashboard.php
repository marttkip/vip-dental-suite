<div class="padd">
  <h4 class="page-">Appointments Report</h4>
    <div class="row">
        <div class="col-md-4">

                <section class="panel">
                    <header class="panel-heading">
                        <h5 class="pull-left"><i class="icon-reorder"></i> Monthly Attendance Summary</h5>
                        <div class="widget-icons pull-right">

                        </div>
                        <div class="clearfix"></div>
                    </header>
                    <div class="panel-body">
                        <!-- /.box-header -->
                        <a href="<?php echo site_url().'appointments-reports/monthly-statistics'?>">

                            <blockquote>
                                <p>This is a snapshot of monthly period of patient attendance.</p>
                                <!-- <small>Profit<cite title="Source Title"> & Loss</cite></small> -->
                            </blockquote>
                        </a>
                    </div>
                    <!-- /.panel-body -->
                </section>
            <!-- /.box -->

        </div>
        <!-- ./col -->

        <div class="col-md-4">

                <section class="panel">
                    <header class="panel-heading">
                        <h5 class="pull-left"><i class="icon-reorder"></i>Progressive Attendance Summary</h5>
                        <div class="clearfix"></div>
                    </header>
                    <!-- /.box-header -->
                    <div class="panel-body">
                      <a href="<?php echo site_url().'appointments-reports/progressive-statistics'?>">
                        <blockquote>
                            <p>This is a snapshot of six month(s) period of patient attendance.</p>
                            <!-- <small>Pending<cite title="Source Title"> Bills</cite></small> -->
                        </blockquote>
                      </a>
                    </div>
                </section>
                <!-- /.panel-body -->

            <!-- /.box -->
        </div>
        <div class="col-md-4">

                <section class="panel">
                    <header class="panel-heading">
                        <h5 class="pull-left"><i class="icon-reorder"></i>Patient Summaries</h5>
                        <div class="clearfix"></div>
                    </header>
                    <!-- /.box-header -->
                    <div class="panel-body">
                      <a href="<?php echo site_url().'appointment-reports/attendance-summaries'?>">
                        <blockquote>
                            <p>List of all appointments describing whether the patient showed, rescheduled or cancelled the appointments.</p>
                            <!-- <small>Pending<cite title="Source Title"> Bills</cite></small> -->
                        </blockquote>
                      </a>
                    </div>
                </section>
                <!-- /.panel-body -->

            <!-- /.box -->
        </div>
    </div>
    <!-- ./col -->

   

</div>
