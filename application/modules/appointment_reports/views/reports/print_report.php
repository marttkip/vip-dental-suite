
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Appointments Reports</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>


        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" />
      	<!-- Calendar -->
      	<link rel="stylesheet" href="<?php echo base_url()."assets/bluish/"?>style/fullcalendar.css">
		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/";?>fontawesome/css/font-awesome.css">
		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap-datepicker/css/datepicker3.css" />
        
        <link href="<?php echo base_url()."assets/themes/jasny/css/jasny-bootstrap.css"?>" rel="stylesheet"/>

		<!-- Specific Page Vendor CSS -->		
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/morris/morris.css" />
		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/select2/select2.css" />			
		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css" />

		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/morris/morris.css" />		
		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/chartist/chartist.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme.css" />
		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" />

		 <link href="<?php echo base_url()."assets/themes/jasny/css/jasny-bootstrap.css"?>" rel="stylesheet"/>
		<!--  <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" /> -->
		<!-- Theme Custom CSS -->
		

		<!-- Head Libs -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/modernizr/modernizr.js"></script>

		<script src="<?php echo base_url()."assets/themes/bluish/"?>js/jquery.js"></script> <!-- jQuery -->

		<!-- full calendae -->
		<link href='<?php echo base_url()."assets/fullcalendar/";?>fullcalendar.min.css' rel='stylesheet' />
		<link href='<?php echo base_url()."assets/fullcalendar/";?>fullcalendar.print.min.css' rel='stylesheet' media='print' />
		<script src='<?php echo base_url()."assets/fullcalendar/";?>lib/moment.min.js'></script>



        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px; padding: 20px}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-print-12 table {
				/*border:solid #000 !important;*/
				/*border-width:1px 0 0 1px !important;*/
				font-size:12px;
				margin-top:10px;
			}
			
			.row .col-print-12 th, .row .col-print-12 td {
				/*border:solid #000 !important;*/
				/*border-width:0 1px 1px 0 !important;*/
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 /*padding: 2px;*/
				 padding: 15px;
			}
			h3
			{
				font-size: 30px;
			}
			
			.row .col-print-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{ margin:0 auto;}

			.liquid-meter {
				position: relative;
				max-height: 187px;
				max-width: 187px;
				margin: 0 auto;
				width: 100%;
				padding-bottom: 50%;
			}
		</style>
    </head>
    <body class="receipt_spacing">
  

    	<div class="row receipt_bottom_border" >
    		<div class="col-print-12">
	    		<div class="col-print-6  " style="margin-bottom: 10px;text-align: left">
	            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" width=200px; />
	            </div>
	        	<div  class="col-print-6">
	            	<p >
	                	<?php echo $contacts['company_name'];?> <br/>
	                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
	                    Address :<strong> P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></strong> <br/>
	                    Office Line: <strong> <?php echo $contacts['phone'];?></strong> <br/>
	                    Website: <strong> http://www.cityeyehospital.or.ke </strong> E-mail: <strong><?php echo $contacts['email'];?>.</strong><br/>
	                </p>
	            </div>
	        </div>
        </div>
        
      	<div class="row" style="margin-top: 20px;margin-bottom: 20px" >
        	<div class="col-print-12 left-align">
            	<h4><strong>REPORT SUMMARY FOR </strong></h4>
            </div>
        </div>
        <?php

       	$six_month = $start_date;
       	$today_month = $end_date;
		$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND DATE(patients.patient_date) BETWEEN "'.$six_month.'" AND "'.$today_month.'" ';
		$table = 'patients,visit,appointments';
		$new_bookings = $this->reception_model->count_items($table, $where);




		$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND (appointments.appointment_status = 4 OR appointments.appointment_status = 7 OR appointments.appointment_status = 1) AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" ';
		$table = 'patients,visit,appointments';
		$total_appointments = $this->reception_model->count_items($table, $where);



		$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 0 AND (appointments.appointment_status = 4 OR appointments.appointment_status = 7) AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" ';
		$table = 'patients,visit,appointments';
		$honoured_appointments = $this->reception_model->count_items($table, $where);


		$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 1 AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" ';
		$table = 'patients,visit,appointments';
		$rescheduled_appointments = $this->reception_model->count_items($table, $where);




		$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 0 AND (appointments.appointment_status = 4 OR appointments.appointment_status = 7) AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" ';
		$table = 'patients,visit,appointments';
		$month_patients = $this->reception_model->count_items($table, $where);
		$showed_charts = '["Honoured", '.$month_patients.'],';


		 $reschedule_where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 1  AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" ';
		$reschedule_table = 'patients,visit,appointments';
		$reschedule_month_patients = $this->reception_model->count_items($reschedule_table, $reschedule_where);
		$rescheduled_charts = '["Rescheduled", '.$reschedule_month_patients.'],';


		$no_show_where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND (appointments.appointment_status = 1) AND appointments.appointment_rescheduled = 0 AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" ';
		$no_show_table = 'patients,visit,appointments';
		$no_show_month_patients = $this->reception_model->count_items($no_show_table, $no_show_where);
		$no_show_charts = '["No Show", '.$no_show_month_patients.'],';

		$total_appointments = $no_show_month_patients+$reschedule_month_patients+$month_patients;




		if(empty($honoured_appointments))
		{
			$percentage_honored =0;
		}
		else
		{
			$percentage_honored = round(($honoured_appointments *100) /$total_appointments) ;
		}


		// echo $month_patients.' '.$honoured_appointments;

		?>

		<div class="row">
			<div class="col-print-2">
				<section class="card card-featured-left card-featured-primary mb-3">
					<div class="card-body">
						<div class="widget-summary">
							
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">New Patients</h4>
									<div class="info">
										<strong class="amount"><?php echo $new_bookings;?></strong>
										<!-- <span class="text-primary">(14 unread)</span> -->
									</div>
								</div>
								<!-- <div class="summary-footer">
									
								</div> -->
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="col-print-3">
				<section class="card card-featured-left card-featured-secondary">
					<div class="card-body">
						<div class="widget-summary">
							
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">Total Bookings</h4>
									<div class="info">
										<strong class="amount"><?php echo $total_appointments?></strong>
									</div>
								</div>
								<!-- <div class="summary-footer">
									
								</div> -->
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="col-print-2">
				<section class="card card-featured-left card-featured-tertiary mb-3">
					<div class="card-body">
						<div class="widget-summary">
							
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">Honoured</h4>
									<div class="info">
										<strong class="amount"><?php echo $honoured_appointments;?></strong>
									</div>
								</div>
								<!-- <div class="summary-footer">
									
								</div> -->
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="col-print-2">
				<section class="card card-featured-left card-featured-quaternary">
					<div class="card-body">
						<div class="widget-summary">
							
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">Rescheduled</h4>
									<div class="info">
										<strong class="amount"><?php echo $rescheduled_appointments?></strong>
									</div>
								</div>
								<!-- <div class="summary-footer">
									
								</div> -->
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="col-print-2">
				<section class="card card-featured-left card-featured-quaternary">
					<div class="card-body">
						<div class="widget-summary">
							
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">No Show</h4>
									<div class="info">
										<strong class="amount"><?php echo $rescheduled_appointments?></strong>
									</div>
								</div>
								<!-- <div class="summary-footer">
									
								</div> -->
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
		<div class="row">
	
			<div class="col-print-6">
				<section class="panel panel-primary">
					<header class="panel-heading">
						<!-- <div class="panel-actions">
							<a href="ui-elements-charts.html#" class="panel-action panel-action-toggle" data-panel-toggle></a>
							<a href="ui-elements-charts.html#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
						</div> -->

						<h2 class="panel-title">Appointments Report</h2>
						<p class="panel-subtitle">Representation of patients appointments for the past six months in %.</p>
					</header>
					<div class="card-body">
						<div class="row">
							
							<div class="col-print-12">
								<!-- Flot: Pie -->
								<div class="chart chart-sm" id="flotPie" style="margin-left: -100px !important;"></div>
								<script type="text/javascript">

									var flotPieData = [{
										label: "Honoured",
										data: [
											<?php echo $showed_charts?>
										],
										color: '#228B22'
									}, {
										label: "Rescheduled",
										data: [
											<?php echo $rescheduled_charts?>
										],
										color: '#B22222'
									}, {
										label: "No Show",
										data: [
											<?php echo $no_show_charts?>
										],
										color: '#FF4500'
									}];

									// See: assets/javascripts/ui-elements/examples.charts.js for more settings.

								</script>
							</div>
						</div>

					</div>
				</section>
			</div>
			<div class="col-print-6">
				<section class="panel">
					<header class="panel-heading">
						

						<h2 class="panel-title">Honoured Appointments</h2>
						<p class="panel-subtitle">Percentage of honoured bookings out of the total bookings for <?php echo date('M Y')?></p>
					</header>
					<div class="panel-body">
						<div class="row">
							
							<div class="col-print-12">
								<meter min="0" max="100" value="<?php echo $percentage_honored;?>" id="meterDark"></meter>
							</div>
						</div>
					</div>
				</section>
				
			</div>
		</div>

		<div class="row">
			<div class="col-print-9">
				<section class="panel">
					<header class="panel-heading">
						<!-- <div class="panel-actions">
							<a href="ui-elements-charts.html#" class="panel-action panel-action-toggle" data-panel-toggle></a>
							<a href="ui-elements-charts.html#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
						</div> -->

						<h2 class="panel-title">Doctors Appointments Record</h2>
						<p class="panel-subtitle">Display of patients appointment based bookings done for <?php echo date('M Y')?> .</p>
					</header>
					<div class="panel-body">
					<?php
						$doctors = $this->reception_model->get_all_doctors();
						$doctor_view = '';

						$total_honoured = 0;
						$total_old_showed_charts = '';
						if($doctors->num_rows() > 0)
						{
							foreach ($doctors->result() as $key => $value) {
								# code...
								$personnel_fname = $value->personnel_fname;
								$personnel_onames = $value->personnel_onames;
								$personnel_id = $value->personnel_id;


								$six_month = date("Y-m-01");
							    $today_month = date("Y-m-d");

							    $where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 0 AND (appointments.appointment_status = 4 OR appointments.appointment_status = 7) AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" AND visit.personnel_id = '.$personnel_id;
								$table = 'patients,visit,appointments';
								$month_patients = $this->reception_model->count_items($table, $where);

								 $reschedule_where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 1  AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" AND visit.personnel_id = '.$personnel_id;
								$reschedule_table = 'patients,visit,appointments';
								$reschedule_month_patients = $this->reception_model->count_items($reschedule_table, $reschedule_where);


								$no_show_where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_status = 1 AND appointments.appointment_rescheduled = 0  AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" AND visit.personnel_id = '.$personnel_id;
								$no_show_table = 'patients,visit,appointments';
								$no_show_month_patients = $this->reception_model->count_items($no_show_table, $no_show_where);

								$total_honoured += $month_patients;


								$total_old_showed_charts .= '{
															y: "'.$personnel_onames.'",
															a: '.$month_patients.',
															b: '.$reschedule_month_patients.',
															c: '.$no_show_month_patients.'
														},';


							}
						}



						?>
						<!-- Morris: Bar -->
						<div class="chart chart-md" id="morrisStackedPatients"></div>
						<script type="text/javascript">

							var morrisStackedData = [<?php echo $total_old_showed_charts;?>];
							// $(".charrt").css("width","auto");
							// See: assets/javascripts/ui-elements/examples.charts.js for more settings.

						</script>
						<div class="col-print-12 text-center">
							 <span style="padding-left:15px;background-color: #228B22;margin-right: 2px;"></span> Honoured
							 <span style="padding-left:15px;background-color: #B22222;margin-right: 2px;"></span> Rescheduled
							 <span style="padding-left:15px;background-color: #FF4500;margin-right: 2px;"></span> No Show 
						</div>

					</div>
				</section>
			
			

			</div>
			
		</div>

        <p style="page-break-before: always;">&nbsp;</p>
        
    	<div class="row">
        	<div class="col-print-12">
				<table class="table table-hover  col-print-12" style="border: 0;">
                    <thead>
                    <tr>
						<th>#</th>
						<th>Services/Items</th>
						<th>Units</th>
						<th>Unit Cost (Ksh)</th>
						<th>Total</th>
					</tr>
                    </thead>
                    <tbody>
						 
                    </tbody>
                  </table>
            </div>
        </div>
        
   		


   		 <!-- Vendor -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-appear/jquery.appear.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.pie.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.categories.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.resize.js"></script>
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-sparkline/jquery.sparkline.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/raphael/raphael.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/morris/morris.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/gauge/gauge.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/snap-svg/snap.svg.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/liquid-meter/liquid.meter.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/jquery.vmap.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/jquery.vmap.world.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>	
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>			
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/select2/select2.js"></script>


		<!-- chart -->

		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-appear/jquery.appear.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.pie.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.categories.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.resize.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-sparkline/jquery.sparkline.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/raphael/raphael.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/morris/morris.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/gauge/gauge.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/snap-svg/snap.svg.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/liquid-meter/liquid.meter.js"></script>		
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/chartist/chartist.js"></script>


		<!-- end of chart -->
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/assets/javascripts/";?>theme.js"></script>
		<script src="<?php echo base_url()."assets/themes/jasny/js/jasny-bootstrap.js";?>"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/theme.init.js"></script>

		<!-- Example -->
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/dashboard/examples.dashboard.js"></script>
		<script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/charts.js"></script>
        <!-- s -->
        <!-- Full Google Calendar - Calendar -->
		<!-- <script src="<?php echo base_url()."assets/bluish/"?>js/fullcalendar.min.js"></script>  -->
		<script src='<?php echo base_url()."assets/fullcalendar/";?>fullcalendar.min.js'></script>
		<script src='<?php echo base_url()."assets/fullcalendar/";?>scheduler.min.css'></script>
		<script src='<?php echo base_url()."assets/fullcalendar/";?>moment.min.js'></script>
		<script src='<?php echo base_url()."assets/fullcalendar/";?>scheduler.min.js'></script>
        <!-- jQuery Flot -->
        <script src="<?php echo base_url()."assets/bluish/"?>js/excanvas.min.js"></script>
        <script src="<?php echo base_url()."assets/bluish/"?>js/jquery.flot.js"></script>
        <script src="<?php echo base_url()."assets/bluish/"?>js/jquery.flot.resize.js"></script>
        <script src="<?php echo base_url()."assets/bluish/"?>js/jquery.flot.axislabels.js"></script>
        <script src="<?php echo base_url()."assets/bluish/"?>js/jquery.flot.pie.js"></script>
        <script src="<?php echo base_url()."assets/bluish/"?>js/jquery.flot.stack.js"></script>
        <!-- <script src="<?php echo base_url()."assets/"?>js/main.js"></script> -->
	    <link rel="stylesheet" href="<?php echo base_url()."assets/themes/bluish";?>/style/jquery.cleditor.css"> 
		<script src="<?php echo base_url()."assets/themes/bluish";?>/js/jquery.cleditor.min.js"></script> <!-- CLEditor -->
		<script type="text/javascript" src="<?php echo base_url();?>assets/themes/tinymce/tinymce.min.js"></script>
		 <script src='<?php echo base_url()."assets/bluish/"?>src/jquery-customselect.js'></script>
	    <link href='<?php echo base_url()."assets/bluish/"?>src/jquery-customselect.css' rel='stylesheet' />
         <!-- <script src="<?php echo base_url()."assets/bluish/"?>src/owl.carousel.js"></script> -->
	    <script type="text/javascript">
            tinymce.init({
                selector: ".cleditor",
               	height: "150"
            });
        </script>
		<script>
		  $('.owl-carousel').owlCarousel({
		    loop:true,
		    margin:10,
		    nav:true,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:1
		        }
		    }
		})   


		</script>

	        
    </body>
    
</html>