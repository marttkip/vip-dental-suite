<?php
// get visit details


$visit_rs = $this->reception_model->get_visit_details($visit_id);

foreach ($visit_rs as $key) {
	# code...

	$time_end=$key->time_end;
	$time_start=$key->time_start;
	$visit_date=$key->visit_date;
	$doctor_id=$key->personnel_id;
	$room_id=$key->room_id;
	$patient_id=$key->patient_id;
}

$personnel_id = $this->session->userdata('personnel_id');
$is_dentist = $this->reception_model->check_if_admin($personnel_id,4);
$is_admin = $this->reception_model->check_if_admin($personnel_id,3);
$personnel_id = $this->session->userdata('personnel_id');
			
$personnel_id = $this->session->userdata('personnel_id');
			
			$is_dentist = $this->reception_model->check_personnel_department_id($personnel_id,4);
			


			// var_dump($is_physician);die();

			if(($is_dentist) AND $doctor_id > 0 OR $personnel_id == 0)
			{
				
				
			}
			else
			{
				?>
				<div class="col-md-12">
					<div class="center-align">
						<?php echo form_open("dental/attend_to_patient/".$visit_id, array("class" => "form-horizontal"));?>
						<input type="submit" class="btn btn-lg btn-success center-align" value="Attend to patient" onclick="return confirm('Are you sure you want to attend to patient ?');"/>
						<?php echo form_close();?>
					</div>
				</div>
				<?php
				
			}
			


?>
<input type="hidden" name="patient_id" id="patient_id" value="<?php echo $patient_id;?>">
		 <input type="hidden" name="current_date" id="current_date" value="<?php echo $visit_date;?>">
          <input type="hidden" name="visit_id" id="visit_id" value="<?php echo $visit_id;?>">

<?php
if(($is_dentist) AND $doctor_id > 0 OR $personnel_id == 0)
{
?>
<div class="col-print-2" style="padding:5px;">
	<div id="sidebar-detail"></div>
	<?php

    if($doctor_id > 0)
    {
    	?>
    	
           <div class="col-md-12">
              <h4> - NEXT APPOINTMENT <br>  </h4>
              <br>
              <div class="center-align">
                   <a class="btn btn-sm btn-warning "   onclick='appointment_sidebar(<?php echo $patient_id;?>,<?php echo $visit_id;?>,10,<?php echo $doctor_id;?>)' > <i class="fa fa-plus"></i> Book Appointment </a>               
              </div>
              <br>
              <div id="next-appointment-view"></div>
              

           </div>
     
    	<?php
    }
    ?>
      
</div>
<div class="col-print-10">

    
		<div class="center-align">
			<?php
				$error = $this->session->userdata('error_message');
				$validation_error = validation_errors();
				$success = $this->session->userdata('success_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}
				
				if(!empty($validation_error))
				{
					echo '<div class="alert alert-danger">'.$validation_error.'</div>';
				}
				
				if(!empty($success))
				{
					echo '<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}
			?>
		</div>
		
		<?php echo $this->load->view("nurse/allergies_brief", '', TRUE);?>
			
		<div class="clearfix"></div>
		
		<div class="tabbable" style="margin-bottom: 10px;">
			<ul class="nav nav-tabs nav-justified">

				<?php
				if($is_dentist OR $is_admin OR $personnel_id == 0)
				{
					?>
					<li class="active" ><a href="#patient-history" data-toggle="tab">Patient card history</a></li>
					<li><a href="#prescription" data-toggle="tab">Prescription</a></li>
					<li><a href="#history-patients" data-toggle="tab">Sick Leave</a></li>
					<li><a href="#diary" data-toggle="tab">Patient's Appointments</a></li>
					<li><a href="#uploads" data-toggle="tab">Uploads</a></li>
					<li><a href="#billing-form" data-toggle="tab">Visit Billing</a></li>
					<li><a href="#patient_details" data-toggle="tab">Patient Details</a></li>
					<li><a href="#visit_trail" data-toggle="tab">Visit Trail</a></li>
					<?php

				}
				else
				{
					?>
					<li class="active" ><a href="#patient-history" data-toggle="tab">Patient card history</a></li>
					<li><a href="#uploads" data-toggle="tab">Uploads</a></li>
					<?php
				}
				?>
				
			</ul>
			<div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd; ">
				
				<div class="tab-pane active" id="patient-history" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">
					<?php echo $this->load->view("patient_history", '', TRUE);?>
				</div>
				<div class="tab-pane " id="dentine" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">
					<div id="page_item"></div>
				</div>
				<div class="tab-pane " id="prescription" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">

					<?php echo $this->load->view("prescription_view", '', TRUE); ?>
					<div id="visit-prescription"></div>
				</div>
				<div class="tab-pane " id="history-patients" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">
					<?php echo $this->load->view("sick_leave", '', TRUE); //echo $this->load->view("nurse/patients/lifestyle", '', TRUE);?>
				</div>
				<div class="tab-pane " id="uploads" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">
					<?php echo $this->load->view("uploads", '', TRUE);?>
				</div>
				<div class="tab-pane " id="diary" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">
					<?php echo $this->load->view("patient_appointments", '', TRUE);?>
				</div>
				<div class="tab-pane" id="patient_details" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">

					<?php 
					$v_data['patient_details'] = $patient_details;
					echo $this->load->view("patient_details", '', TRUE);?>
				</div>
				<div class="tab-pane" id="billing-form" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">

					<?php echo $this->load->view("billing", '', TRUE);?>
				</div>
				<div class="tab-pane" id="visit_trail" style="height: 75vh !important;overflow-y: scroll;overflow-x: none;">
					<?php echo $this->load->view("visit_trail", '', TRUE);?>
				</div>

			</div>
		</div>
				  


	
			
			<div class="col-md-12">
				
				  <div class="center-align">
					
					 <a href="<?php echo site_url().'dental/send_to_accounts/'.$visit_id.'/'.$mike;?>" onclick="return confirm('Are you sure you want to send to accounts ? ')" class="btn btn-large btn-danger fa fa-lock" > SEND TO ACCOUNTS </a>
					 <a href="<?php echo site_url().'queue'?>"  class="btn btn-large btn-info fa fa-arrow-left" > Back to Queue </a>
				  </div>
		
			
			</div>


	
		
	
</div>
<?php

}
?>
  
  <script type="text/javascript">
  	
	var config_url = $("#config_url").val();
		
	$(document).ready(function(){
		

	  	$.get( config_url+"nurse/get_family_history/<?php echo $visit_id;?>", function( data ) {
			$("#new-nav").html(data);
			$("#checkup_history").html(data);
		});

		get_medication(<?php echo $visit_id;?>);
		prescription_view();

		get_surgeries(<?php echo $visit_id;?>);
		get_page_item(1,<?php echo $patient_id;?>);
		get_sidebar_details(<?php echo $patient_id;?>,<?php echo $visit_id;?>);



	});

	function get_sidebar_details(patient_id,visit_id)
	{

		var config_url = $('#config_url').val();
	 	var url = config_url+"reception/get_sidebar_details/"+patient_id+"/"+visit_id;
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#sidebar-detail").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

	}


	function get_medication(visit_id){
    
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }

	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/load_medication/"+visit_id;

	    if(XMLHttpRequestObject) {
	        
	        var obj = document.getElementById("medication");
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                obj.innerHTML = XMLHttpRequestObject.responseText;
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function get_surgeries(visit_id){
    
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/load_surgeries/"+visit_id;
	    
	    if(XMLHttpRequestObject) {
	        
	        var obj = document.getElementById("surgeries");
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                obj.innerHTML = XMLHttpRequestObject.responseText;
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function save_surgery(visit_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var date = document.getElementById("datepicker").value;
	    var description = document.getElementById("surgery_description").value;
	    var month = document.getElementById("month").value;
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/surgeries/"+date+"/"+description+"/"+month+"/"+visit_id;
	   
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                get_surgeries(visit_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}

	function delete_surgery(id, visit_id){
	    //alert(id);
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	      var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/delete_surgeries/"+id;
	    
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                get_surgeries(visit_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function save_medication(visit_id){
	    var config_url = document.getElementById("config_url").value;
	    var data_url = config_url+"nurse/medication/"+visit_id;
	   
	     var patient_medication = $('#medication_description').val();
	     var patient_medicine_allergies = $('#medicine_allergies').val();
	     var patient_food_allergies = $('#food_allergies').val();
	     var patient_regular_treatment = $('#regular_treatment').val();
	     
	    $.ajax({
	    type:'POST',
	    url: data_url,
	    data:{medication: patient_medication,medicine_allergies: patient_medicine_allergies, food_allergies: patient_food_allergies, regular_treatment: patient_regular_treatment },
	    dataType: 'text',
	    success:function(data){
	     get_medication(visit_id);
	    //obj.innerHTML = XMLHttpRequestObject.responseText;
	    },
	    error: function(xhr, status, error) {
	    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	    alert(error);
	    }

	    });

	       
	}
  </script>

<script type="text/javascript">
		$(document).ready(function() {
	
		// check_date();
		// load_patient_appointments();
		display_next_appointment(<?php echo $patient_id;?>,<?php echo $visit_id;?>);
	});

	function check_date(){
	     var datess=document.getElementById("scheduledate").value;
	     
	     if(datess){
		  $('#show_doctor').fadeToggle(1000); return false;
		 }
		 else{
		  alert('Select Date First')
		 }
	}

	// function load_schedule(){
	// 	var config_url = $('#config_url').val();
	// 	var datess=document.getElementById("scheduledate").value;
	// 	var doctor= <?php echo $this->session->userdata('personnel_id');?>//document.getElementById("doctor").value;

	// 	var url= config_url+"reception/doc_schedule/"+doctor+"/"+datess;
	
	// 	  $('#doctors_schedule').load(url);
	// 	  $('#doctors_schedule').fadeIn(1000); return false;	
	// }
	// function load_patient_appointments(){
	// 	var patient_id = $('#patient_id').val();
	// 	var current_date = $('#current_date').val();

	// 	var url= config_url+"reception/patient_schedule/"+patient_id+"/"+current_date;
		
	// 	$('#patient_schedule').load(url);
	// 	$('#patient_schedule').fadeIn(1000); return false;	

	// 	$('#patient_schedule2').load(url);
	// 	$('#patient_schedule2').fadeIn(1000); return false;	
	// }
	// function load_patient_appointments_two(){
	// 	var patient_id = $('#patient_id').val();
	// 	var current_date = $('#current_date').val();

	// 	var url= config_url+"reception/patient_schedule/"+patient_id+"/"+current_date;
		
	// 	$('#patient_schedule2').load(url);
	// 	$('#patient_schedule2').fadeIn(1000); return false;	
	// }
	function schedule_appointment(appointment_id)
	{
		if(appointment_id == '1')
		{
			$('#appointment_details').css('display', 'block');
		}
		else
		{
			$('#appointment_details').css('display', 'none');
		}
	}

	var config_url = $("#config_url").val();
		
	// $(document).ready(function(){
		
	//   	$.get( config_url+"nurse/get_family_history/<?php echo $visit_id;?>", function( data ) {
	// 		$("#new-nav").html(data);
	// 		$("#checkup_history").html(data);
	// 	});
	// });

	function pass_tooth()
    {

     var tooth_id = document.getElementById("tooth_id").value;
     var visit_id = document.getElementById("visit_id").value;
     var patient_id = document.getElementById("patient_id").value;


    var radios = document.getElementsByName('cavity_status');
    var cavity_status = null;
    for (var i = 0, length = radios.length; i < length; i++)
    {
     if (radios[i].checked)
     {
      // do whatever you want with the checked radio
       cavity_status = radios[i].value;
      // only one radio can be logically checked, don't check the rest
      break;
     }
    }
    
     

     var url = "<?php echo base_url();?>dental/save_dentine/"+visit_id+"/"+patient_id;
     //
     $.ajax({
     type:'POST',
     url: url,
     data:{tooth_id: tooth_id,patient_id: patient_id,cavity_status: cavity_status},
     dataType: 'text',
     success:function(data){
       // var prescription_view = document.getElementById("prescription_view");
       // prescription_view.style.display = 'none';

         var data = jQuery.parseJSON(data);
            
            var status = data.status;

            if(status == 'success')
            {
              alert(data.message);
              get_page_item(7,patient_id);
              // display_patient_history(visit_id,patient_id);

            }
            else
            {
              alert(data.message);
            }
     
     },
     error: function(xhr, status, error) {
     alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
     
     }
     });
     
     return false;
    }

     function get_page_item(page_id,patient_id)
    {
        // alert(page_id);
        // get_page_links(page_id,patient_id);
        // if(page_id > 1)
        // {
            var visit_id = <?php echo $visit_id;?>;//window.localStorage.getItem('visit_id');
             // alert(visit_id);
            if(visit_id > 0)
            {

              var visit_id = visit_id;
              // get_page_header(visit_id); 
              var url = "<?php echo base_url();?>dental/get_page_item/"+page_id+"/"+patient_id+"/"+visit_id;  
              // alert(url);
              $.ajax({
              type:'POST',
              url: url,
              data:{page_id: page_id,patient_id: patient_id},
              dataType: 'text',
              success:function(data){
                  var data = jQuery.parseJSON(data);
                  var page_item = data.page_item;

                  // alert(page_item);
                  $('#page_item').html(data.page_item);
              },
              error: function(xhr, status, error) {
              alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                  // display_patient_bill(visit_id);
              }
              });                
            }
            else
            {
              var visit_id = null;

              if(page_id > 1)
              {
                alert("Please select a date of a visit you wish to see");
              }
              else
              {
                var url = "<?php echo base_url();?>dental/get_page_item/"+page_id+"/"+patient_id+"/"+visit_id;  
            
                $.ajax({
                type:'POST',
                url: url,
                data:{page_id: page_id,patient_id: patient_id},
                dataType: 'text',
                success:function(data){
                    var data = jQuery.parseJSON(data);
                    var page_item = data.page_item;
                    $('#page_item').html(data.page_item);
                },
                error: function(xhr, status, error) {
                alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                    // display_patient_bill(visit_id);
                }
                });   
              }
              
            }

       return false;

    }

     function check_department_type(teeth_number)
  {

    // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = document.getElementById("visit_id").value;
    var patient_id = document.getElementById("patient_id").value;
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_dental_formula/"+teeth_number+"/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("dental-formula").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }




  function save_other_sick_off(visit_id)
	{
		 // start of saving rx
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/save_other_patient_sickoff/"+visit_id;
        //window.alert(data_url);
         var doctor_notes_rx = $('#deductions_and_other').val();//document.getElementById("vital"+vital_id).value;
        $.ajax({
        type:'POST',
        url: data_url,
        data:{notes: doctor_notes_rx},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
           window.alert("You have successfully updated the payment information");
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
      // end of saving rx
	}

	 function prescription_view()
  {

    // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = document.getElementById("visit_id").value;
    var patient_id = document.getElementById("patient_id").value;
    // alert(patient_id);
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_patient_prescription/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("visit-prescription").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }


	function save_prescription(patient_id,visit_id)
	{
		 // start of saving rx
		 // var prescription = tinymce.get('#visit_prescription').getContent();
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/save_prescription/"+patient_id+"/"+visit_id;
        // window.alert(data_url);
         // var prescription = $('#visit_prescription').val();//document.getElementById("vital"+vital_id).value;
         		// console.debug(tinymce.activeEditor.getContent());

        var prescription = tinymce.get('visit_prescription'+visit_id).getContent();

        
			 // alert(visit_id);

         // alert(prescription);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{prescription: prescription},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
           window.alert("You have successfully updated the prescription");
           prescription_view();
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
      // end of saving rx
	}
  

</script>



  <script type="text/javascript">

function appointment_sidebar(patient_id,visit_id,department_id,personnel_id)
  {
      open_sidebar();
     var config_url = $('#config_url').val();
      var data_url = config_url+"dental/appointment_sidebar/"+patient_id+"/"+visit_id+"/"+department_id+"/"+personnel_id;
      // window.alert(data_url);
      $.ajax({
      type:'POST',
      url: data_url,
      data:{visit_id: visit_id},
      dataType: 'text',
      success:function(data){
      //window.alert("You have successfully updated the symptoms");
      //obj.innerHTML = XMLHttpRequestObject.responseText;
       $("#sidebar-div").html(data);

        $('.datepicker').datepicker({
              format: 'yyyy-mm-dd',
              minDate: "dateToday"
          });

        // $('.datepicker').datepicker();
        $('.timepicker').timepicker();
        // alert(data);
      },
      error: function(xhr, status, error) {
      //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
      alert(error);
      }

      });

  }

function save_appointment(visit_id)
{

// $(document).on("submit","form#next_appointment_detail",function(e)
// {
  // alert("sdasdjakgdaskjdag");
  // e.preventDefault();
  
  // var form_data = new FormData(this);

  // alert(form_data);


  var visit_id = $('#visit_id').val();
  var patient_id = $('#patient_id'+visit_id).val();
  var appointment_note = $('#appointment_note'+visit_id).val();
  var next_appointment_date = $('#next_appointment_date'+visit_id).val();
  var doctor_id = $('#doctor_id'+visit_id).val();
  var department_id = $('#department_id'+visit_id).val();
  var event_duration = $('#event_duration'+visit_id).val();
  var timepicker_start = $('#timepicker_start'+visit_id).val();
  
  var config_url = $('#config_url').val();  

   var url = config_url+"reception/save_appointment_accounts/"+patient_id+"/"+visit_id;
       $.ajax({
       type:'POST',
       url: url,
       data:{visit_id: visit_id,patient_id: patient_id,appointment_note: appointment_note,visit_date: next_appointment_date,doctor_id: doctor_id,department_id: department_id,event_duration:event_duration,timepicker_start:timepicker_start},
       dataType: 'text',
       // processData: false,
       // contentType: false,
       success:function(data)
       {
          var data = jQuery.parseJSON(data);
          
          if(data.status == "success")
          {
            
            alert('You have successfully added an appointment');

            display_next_appointment(patient_id,visit_id);
            close_side_bar();
          }
          else
          {

            alert(data.message);
          }
       
       },
       error: function(xhr, status, error) {
       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       
       }
       });
}

function display_next_appointment(patient_id,visit_id)
{
  // alert(patient_id);

  var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     
     var config_url = document.getElementById("config_url").value;
     var url = config_url+"dental/display_appointment_view/"+patient_id+"/"+visit_id;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
   
                 document.getElementById("next-appointment-view").innerHTML=XMLHttpRequestObject.responseText;
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
}


 function view_schedule(visit_id,doctor_id)
 {
     var datess=document.getElementById("next_appointment_date"+visit_id).value;
    var doctor_id= document.getElementById("doctor_id"+visit_id).value;

 
       if(datess && doctor_id){
        load_schedule(visit_id);
        load_patient_appointments_two(visit_id);
      // $('#show_doctor').fadeToggle(1000); return false;
     }
     else{
      alert('Select Date and a Doctor First')
     }
 }

 function load_schedule(visit_id){
    var config_url = $('#config_url').val();
    var datess=document.getElementById("next_appointment_date"+visit_id).value;
    var doctor= document.getElementById("doctor_id"+visit_id).value;

    var url= config_url+"reception/doc_schedule/"+doctor+"/"+datess;


    
      $('#doctors_schedule'+visit_id).load(url);
      $('#doctors_schedule'+visit_id).fadeIn(1000); return false; 
  }
  function load_patient_appointments(visit_id){
    var patient_id = $('#patient_id'+visit_id).val();
    var current_date = $('#next_appointment_date'+visit_id).val();

    var url= config_url+"reception/patient_schedule/"+patient_id+"/"+current_date;
    
    $('#patient_schedule'+visit_id).load(url);
    $('#patient_schedule'+visit_id).fadeIn(1000); return false; 

    $('#patient_schedule2'+visit_id).load(url);
    $('#patient_schedule2'+visit_id).fadeIn(1000); return false;  
  }
  function load_patient_appointments_two(visit_id){
    var patient_id = $('#patient_id'+visit_id).val();
    var current_date = $('#next_appointment_date'+visit_id).val();

    var url= config_url+"reception/patient_schedule/"+patient_id+"/"+current_date;
    
    $('#patient_schedule2'+visit_id).load(url);
    $('#patient_schedule2'+visit_id).fadeIn(1000); return false;  
  }
   function delete_event_details(appointment_id,status,visit_id,patient_id)
  {
    var config_url = $('#config_url').val();  
    var res = confirm('Are you sure you want to delete this schedule ?');

    if(res)
    {

       var url = config_url+"calendar/delete_event_details/"+appointment_id+"/"+status;
           $.ajax({
           type:'POST',
           url: url,
           data:{appointment_id: appointment_id,status: status},
           dataType: 'text',
           processData: false,
           contentType: false,
           success:function(data){
              var data = jQuery.parseJSON(data);
            
              if(data.message == "success")
              {
                display_next_appointment(patient_id,visit_id);
              }
              else
              {
                alert('Please ensure you have added included all the items');
              }
           
           },
           error: function(xhr, status, error) {
           alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
           
           }
           });
     }
     else
     {
     
     }

  }





</script>