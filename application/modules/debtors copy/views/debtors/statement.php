<!-- search -->
<?php echo $this->load->view('search_debtor_account', '', TRUE);?>
<!-- end search -->

<div class="row">
    <div class="col-md-12">

        <section class="panel">
            <header class="panel-heading">
                
                <h2 class="panel-title"><?php echo $title;?></h2>
                <a href="<?php echo site_url();?>accounts/debtors-statements" class="btn btn-sm btn-warning pull-right" style="margin-top: -25px; "><i class="fa fa-arrow-left"></i> Back to debtors</a>
             

                	
            </header>
            
            <div class="panel-body">
                <div class="pull-right">
                	
                	<!--<a href="<?php echo base_url().'administration/sync_app_creditor_account';?>" class="btn btn-sm btn-info"><i class="fa fa-sign-out"></i> Sync</a>-->
                </div>
                <!-- Modal -->
             
                
			<?php
			$error = $this->session->userdata('error_message');
			$success = $this->session->userdata('success_message');
			
			if(!empty($error))
			{
				echo '<div class="alert alert-danger">'.$error.'</div>';
				$this->session->unset_userdata('error_message');
			}
			
			if(!empty($success))
			{
				echo '<div class="alert alert-success">'.$success.'</div>';
				$this->session->unset_userdata('success_message');
			}
					
			$search = $this->session->userdata('creditor_payment_search');
			$search_title = $this->session->userdata('creditor_search_title');
		
			if(!empty($search))
			{
				echo '
				<a href="'.site_url().'accounting/creditors/close_creditor_search/'.$creditor_id.'" class="btn btn-warning btn-sm ">Close Search</a>
				';
				echo $search_title;
			}	
			// var_dump($debtor_id); die();
			$creditor_result = $this->debtors_model->get_debtor_statement($debtor_id);
			?>

				<table class="table table-hover table-bordered ">
				 	<thead>
						<tr>
						  <th>Transaction Date</th>						  
						  <th>Debit (Invoices)</th>
						  <th>Credit (Payments & Credits)</th>
						  <th>Balance</th>	  					
						</tr>
					 </thead>
				  	<tbody>
				  		<?php

				  		$result ='';
				  		$total_balance = 0;
				  		$grand_invoices = 0;
				  		$grand_payments = 0;
				  			// var_dump($creditor_query->num_rows()); die();

				  		$opening_query = $this->debtors_model->get_opening_debtor_balance($debtor_id);
						if($opening_query->num_rows() > 0)
						{
							$row = $opening_query->row();
							$opening_balance = $row->opening_balance;
							$opening_date = $row->created;
							$debit_id = $row->debit_id;
							// var_dump($debit_id); die();
							if($debit_id == 2)
							{
								// this is deniget_all_provider_credit_month
								$result .= 
											'
												<tr>
													<td>'.date('d M Y',strtotime($created)).' Opening Balance </td>
													<td>'.number_format($opening_balance, 2).'</td>
													<td>0.00</td>
													<td>'.number_format($opening_balance, 2).'</td>
												</tr> 
											';
								$grand_invoices += $opening_balance;
								$total_balance += $opening_balance;

							}
							else
							{
								// this is a prepayment
								$result .= 
											'
												<tr>
													<td>'.date('d M Y',strtotime($created)).' Opening Balance </td>
													<td>0.00</td>
													<td>'.number_format($opening_balance, 2).'</td>
													<td>'.number_format($opening_balance, 2).'</td>
												</tr>  
											';
								$grand_payments = $opening_balance;
								$grand_invoices -= $opening_balance;
								$total_balance -= $opening_balance;
							}
						}
						// var_dump($grand_invoices);die();
				  		if($creditor_result->num_rows() > 0)
				  		{
				  			foreach ($creditor_result->result() as $key => $value) {
				  				# code...

				  				$year = $value->year;
				  				$month = $value->month;
				  				$total_debit = $value->total_debit;
				  				$total_credit = $value->total_credit;


				  				$balance = $total_debit - $total_credit;

				  				$date = $year.'-'.$month.'-01';

				  				$start_date = $year.'-'.$month.'-01';

								

				  				$total_date =  date('M Y',strtotime($date));

				  				$end_date =  date("Y-m-t", strtotime($start_date));
				  				$total_balance += $balance;
				  				$grand_invoices += $total_debit;
				  				$grand_payments += $total_credit;
				  				$result .='<tr>	
											  <td>'.$total_date.'</td>						  
											  <td>'.number_format($total_debit,2).'</td>
											  <td>'.number_format($total_credit,2).'</td>
											  <td>'.number_format($balance,2).'</td>
											  <td><a href="'.site_url().'export-debtor-invoices/'.$debtor_id.'/'.$start_date.'/'.$end_date.'"  class="btn btn-xs btn-success" target="_blank">export invoices</a></td>  					
											</tr>';


				  			}

				  			$result .='<tr>	
											  <th>TOTAL</th>						  
											  <th>'.number_format($grand_invoices,2).'</th>
											  <th>'.number_format($grand_payments,2).'</th>
											  <th>'.number_format($total_balance,2).'</th>	  					
											</tr>';
				  		}

				  		echo $result;
				  		?>
					</tbody>
				</table>
          	</div>
		</section>
    </div>
</div>
<script type="text/javascript">
    $(function() {
       $("#billed_account_id").customselect();
    });
</script>