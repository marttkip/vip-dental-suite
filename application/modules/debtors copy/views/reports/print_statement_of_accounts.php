
<!DOCTYPE html>
<html lang="en">

    <head>
          <title><?php echo $contacts['company_name'];?> | Statement of Accounts</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
            body
            {
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
                font-size:15px;                
            }
            .receipt_spacing{letter-spacing:0px; font-size: 13px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            
            .col-md-6 {
                width: 50%;
             }
        
            h3
            {
                font-size: 30px;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{ margin:0 auto;}
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {width:16%; float:left;}
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}

          
            .padd
            {
                padding:10px;
            }
            
        </style>
    </head>
    <body class="receipt_spacing">
        <div class="padd">
             <div class="row">
                <div class="col-md-12">
                    <div class="col-print-6" style="text-align: left;">
                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive"/>
                        <!-- <p style="margin-top: 20px !important;"> Dr. Lorna Sande | Dr. Kinoti M, Dental Surgeons</p> -->
                    </div>
                    <div class="col-print-6 " style="text-align: right;">
                        <strong>
                            <?php echo $contacts['company_name'];?><br/>
		                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
		                    E-Mail:<?php echo $contacts['email'];?>.<br> Tel : <?php echo $contacts['phone'];?><br/>
		                    <?php echo $contacts['location'];?><br/>
                         
                        </strong>
                    </div>
                    
                </div>
		       <div class="col-md-12">
		       		<div style="margin-top: 10px">
		       			<?php
		       			if(!empty($query))
						{
							//if users exist display them
							if ($query->num_rows() > 0)
							{
								$count = 0;
								$visit_type_name = $this->session->userdata('statements_visit_type_name');
								$result = 
									'
										<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
										  <thead>
										  	<tr>
												<th colspan=13> '.$visit_type_name.' As at '.date('jS M Y').'</th>
												
											</tr> 
											<tr>
												<th>Ref No.</th>
												<th>FILE No.</th>
												<th>Patient\'s Name.</th>
												<th>Scheme/Company </th>
												<th>Member No./Policy No.</th>
												<th>Preauthorisation Date</th>
												<th>Transaction Type</th>
												<th>Ageing</th>
												<th>Invoice Date</th>
												<th>Invoice Number</th>
												<th>Invoice Amount</th>
												<th>Payments</th>
												<th>Invoice Balance</th>
											</tr>
										  </thead>
										  <tbody>
								';

								
							
								// $personnel_query = $this->accounting_model->get_all_personnel();
								$total_waiver = 0;
								$total_payments = 0;
								$total_invoice = 0;
								$total_balance = 0;
								$total_rejected_amount = 0;
								$total_cash_balance = 0;
								$total_insurance_payments =0;
								$total_insurance_invoice =0;
								$total_payable_by_patient = 0;
								$total_payable_by_insurance = 0;
								$total_debit_notes = 0;
								$total_credit_notes= 0;
								foreach ($query->result() as $row)
								{
									$total_invoiced = 0;
									$visit_date = date('d.m.y',strtotime($row->transaction_date));
									
									
									$visit_id = $row->visit_id;
									$patient_id = $row->patient_id;
									$personnel_id = $row->personnel_id;
									$dependant_id = $row->dependant_id;
									$patient_number = $row->patient_number;
									$visit_invoice_number = $row->visit_invoice_number;
									$scheme_name = $row->scheme_name;
									$member_number = $row->member_number;
									$preauth_date = $row->preauth_date;
									$authorising_officer = $row->authorising_officer;
									$visit_invoice_id = $row->visit_invoice_id;
									$branch_code = $row->branch_code;

									$visit_type_name = $row->payment_type_name;
									$patient_othernames = $row->patient_othernames;
									$patient_surname = $row->patient_surname;
									$patient_date_of_birth = $row->patient_date_of_birth;
									$patient_first_name = '';//$row->patient_first_name;
									$preauth_date = date('d.m.y',strtotime($row->preauth_date));
									$doctor = $row->personnel_fname;
									$count++;
									$invoice_total = $row->dr_amount;
									$payments_value = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
									$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

									$invoice_total -= $credit_note;

									$balance  = $this->accounts_model->balance($payments_value,$invoice_total);

									$total_payable_by_patient += $invoice_total;
									$total_payments += $payments_value;
									$total_balance += $balance;
									
									$date = $row->transaction_date;
									
									$ageing  = $this->debtors_model->get_aging_time($date);


									$result .= 
										'
												<tr>
													<td>'.$count.'</td>
													<td>'.$patient_number.'</td>
													<td>'.ucwords(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name)).'</td>
													<td>'.$scheme_name.'</td>
													<td>'.$member_number.'</td>
													<td>'.$preauth_date.'</td>
													<td>Invoice</td>
													<td>'.$ageing.'</td>
													<td>'.$visit_date.'</td>
													<td>'.$visit_invoice_number.'</td>
													<td>'.(number_format($invoice_total,2)).'</td>
													<td>'.(number_format($payments_value,2)).'</td>
													<td>'.(number_format($balance,2)).'</td>
												</tr> 
										';
									
								}

								$result .= 
										'
											<tr>
												<td colspan=10> Totals</td>
												<td><strong>'.number_format($total_payable_by_patient,2).'</strong></td>
												<td><strong>'.number_format($total_payments,2).'</strong></td>
												<td><strong>'.number_format($total_balance,2).'</strong></td>
											</tr> 
									';


								$result .= 
										'
											<tr>
												<td colspan=13> Unallocated Payments</td>
												
											</tr> 
										';

								$visit_type = $this->session->userdata('statements_visit_type');

								$this->db->where('batch_unallocations.batch_receipt_id = batch_receipts.batch_receipt_id AND batch_unallocations.unallocated_payment_delete = 0 AND batch_receipts.insurance_id = '.$visit_type);
								// $this->db->get('batch_unallocations.*,batch_receipts.*');
								$query = $this->db->get('batch_unallocations,batch_receipts');
								$visit_type_name = $this->session->userdata('statements_visit_type_name');

								if($query->num_rows() > 0)
								{
									$counting =0;
									foreach ($query->result() as $key => $value) {
										# code...

										$payment_date = $value->payment_date;
										$amount = $value->amount_paid;
										$payment_date = $value->payment_date;
										$receipt_number = $value->receipt_number;

										$payment_date = date('d.m.y',strtotime($value->payment_date));

										$total_balance -= $amount;
										$counting++;
										$result .= 
											'
													<tr>
														<td>'.$counting.'</td>
														<td></td>
														<td>Insurance - '.$visit_type_name.'</td>
														<td></td>
														<td></td>
														<td></td>
														<td>Payment</td>
														<td></td>
														<td></td>
														<td>'.$payment_date.'</td>
														<td>'.$receipt_number.'</td>
														<td>('.(number_format($amount,2)).')</td>
														<td>('.(number_format($amount,2)).')</td>
													</tr> 
											';
									}
									$result .= 
										'
											<tr>
												<td colspan=13></td>
												
											</tr> 
										';
									$result .= 
											'
													<tr>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<th>Total</th>
														<th>'.(number_format($total_balance,2)).'</th>
														<th>'.(number_format($total_balance,2)).'</th>
													</tr> 
											';
								}
								
								$result .= 
								'
											  </tbody>
											</table>
								';
							}
							
							else
							{
								$result .= "There are no visits";
							}

						}
						else
						{
							$result = "There are no records found";
						}
						
						
						echo $result;

		       			?>
		       		</div>
		       </div>
		    </div>
		</div>
	</body>
</html>
   		