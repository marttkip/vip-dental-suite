<?php
        $where = 'batch_receipts.bank_id = account.account_id AND batch_receipts.batch_receipt_id = '.$batch_receipt_id;
        $table = 'batch_receipts,account';
        
    
        $transaction_detail_rs = $this->debtors_model->get_all_unpaid_invoices($table, $where);

        if($transaction_detail_rs->num_rows() > 0)
        {
            foreach ($transaction_detail_rs->result() as $key => $value) {
                # code...
                $account_name = $value->account_name;
                $receipt_number = $value->receipt_number;
                $payment_date = $value->payment_date;
                $total_amount_paid = $value->total_amount_paid;
                $bank_id = $value->bank_id;
                $payment_date = $value->payment_date;
                $receipt_number = $value->receipt_number;

            }
        }
        
        // $where = 'v_statement_of_accounts.dr_amount <> v_statement_of_accounts.cr_amount AND v_statement_of_accounts.payment_type = '.$insurance_id;
        // $table = 'v_statement_of_accounts';

        $where = 'v_transactions_by_date.transactionCategory = "Revenue" AND visit_invoice.visit_invoice_id = v_transactions_by_date.transaction_id AND visit_invoice.visit_invoice_status <> 1 AND patients.patient_id = visit_invoice.patient_id AND visit_invoice.batch_receipt_id = 0 AND visit_invoice.bill_to = '.$insurance_id;
        
        $table = 'v_transactions_by_date,visit_invoice,patients';
        
    
        $query = $this->debtors_model->get_all_unpaid_invoices($table, $where);


        $result = '';
        
        //if users exist display them
        if ($query->num_rows() > 0)
        {
            $count = 0;
            
            $result .= 
            '
            <table class="table table-bordered table-striped table-condensed">
                <thead>
                    <tr>
                        <th></th>
                        <th>#</th>
                        <th>Name</th>
                        <th>Invoice Date</th>
                        <th>Invoice Number</th>
                        <th>Amount Invoiced</th>
                        <th>Balance</th>
                    </tr>
                </thead>
                  <tbody>
                  
            ';
            
            //get all administrators
           
            
            foreach ($query->result() as $row)
            {
               

                $transaction_id = $row->transaction_id;
                $patient_name = $row->patient_surname.' '.$row->patient_othernames;
                $reference_code = $row->reference_code;
                $invoice_date = $row->invoice_date;



                $dr_amount = $row->dr_amount;
                $total_payments = $this->accounts_model->get_visit_invoice_payments($transaction_id);
                $credit_note = $this->accounts_model->get_visit_invoice_credit_notes($transaction_id);

                $dr_amount = $balance = $dr_amount - ($total_payments+$credit_note);
                // $cr_amount = $row->cr_amount;
                $status = $row->status;
                $patient_id = $row->patient_id;
                $invoice_date = date('jS M Y',strtotime($row->invoice_date));

                if($status == 0)
                {
                	$color ='warning';
                	$status = 'Not Reconcilled';
                }
                else
                {
                	$color = 'success';
                	$status = 'Reconcilled';
                }
                 $checkbox_data = array(
                                        'name'        => 'visit_invoices[]',
                                        'id'          => 'checkbox',
                                        'class'          => 'css-checkbox  lrg ',
                                        // 'checked'=>'checked',
                                        'value'       => $transaction_id,
                                        'onclick'=>'get_values('.$transaction_id.','.$batch_receipt_id.')'
                                      );
                $count++;
                $result .= 
                '
                    <tr>
                        <td>'.form_checkbox($checkbox_data).'<label for="checkbox'.$transaction_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
                        <td >'.$count.'</td>
                        <td >'.$patient_name.'</td>
                        <td>'.$invoice_date.'</td>
                        <td >'.$reference_code.'</td>
                        <td>'.number_format($dr_amount,2).'<input type="hidden" class="form-control" colspan="3" name="invoiced_amount'.$transaction_id.'" id="invoiced_amount'.$transaction_id.'" value="'.$dr_amount.'" />
                        <input type="hidden" class="form-control" colspan="3" name="patient_id'.$transaction_id.'" id="patient_id'.$transaction_id.'" value="'.$patient_id.'"/></td>
                        
                        <td>'.number_format($balance,2).'</td>
                       
                    </tr> 
                ';
            }
            
            $result .= 
            '
                          </tbody>
                        </table>
            ';
        }
        
        else
        {
            $result .= "There are no remitance uploaded";
        }
?>

<div class="table-responsive">
            
    <?php echo $result;?>

</div>