<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once "./application/modules/admin/controllers/admin.php";


class providers extends admin
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('providers_model');
		$this->load->model('purchases_model');
    $this->load->model('transfer_model');
    $this->load->model('financials/company_financial_model');
	}



  public function providers_invoices()
  {
    // $v_data['property_list'] = $property_list;


   
    $provider_id = $this->session->userdata('invoice_provider_id_searched');


    $where = 'provider_invoice.provider_invoice_status = 1 AND provider_invoice.provider_id = '.$provider_id;

    $search_purchases = $this->session->userdata('search_purchases');
    if($search_purchases)
    {
      $where .= $search_purchases;
    }
    $table = 'provider_invoice';


    $segment = 3;
    $this->load->library('pagination');
    $config['base_url'] = site_url().'accounting/provider-invoices';
    $config['total_rows'] = $this->purchases_model->count_items($table, $where);
    $config['uri_segment'] = $segment;
    $config['per_page'] = 20;
    $config['num_links'] = 5;

    $config['full_tag_open'] = '<ul class="pagination pull-right">';
    $config['full_tag_close'] = '</ul>';

    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';

    $config['next_tag_open'] = '<li>';
    $config['next_link'] = 'Next';
    $config['next_tag_close'] = '</span>';

    $config['prev_tag_open'] = '<li>';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $this->pagination->initialize($config);

    $page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
    $query = $this->providers_model->get_all_providers_details($table, $where, $config["per_page"], $page, $order='provider_invoice.transaction_date', $order_method='DESC');
    $v_data['provider_invoices'] = $query;
    $v_data['page'] = $page;
    $data['title'] = 'provider Invoices';
    $v_data['title'] = $data['title'];
    $data['content'] = $this->load->view('providers/providers_statement', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
  }


  public function search_providers_invoice()
  {
    // var_dump($_POST);die();
    $provider_id = $provider_id_searched = $this->input->post('provider_id');

    $search_title = '';

    if(!empty($provider_id))
    {

      $provider_id = ' AND provider.provider_id = '.$provider_id.' ';
    }
    $search = $provider_id;

    $this->session->set_userdata('invoice_provider_id_searched', $provider_id_searched);
    $this->session->set_userdata('search_providers_invoice', $search);
    redirect('accounting/provider-invoices');
  }

  public function search_providers_bill($provider_id)
  {

    $provider_id = $provider_id_searched = $provider_id;

    $search_title = '';

    if(!empty($provider_id))
    {

      $provider_id = ' AND provider.provider_id = '.$provider_id.' ';
    }
    $search = $provider_id;

    $this->session->set_userdata('invoice_provider_id_searched', $provider_id_searched);
    $this->session->set_userdata('search_providers_invoice', $search);
    redirect('accounting/provider-invoices');
  }

  public function close_searched_invoices_provider()
  {
    $this->session->unset_userdata('invoice_provider_id_searched');
    $this->session->unset_userdata('search_providers_invoice');
    redirect('accounting/providers');
  }
  public function calculate_value()
  {
    $quantity = $this->input->post('quantity');
    $tax_type_id = $this->input->post('tax_type_id');
    $unit_price = $this->input->post('unit_price');


    if(empty($quantity))
    {
      $quantity = 1;
    }
    if(empty($unit_price))
    {
      $unit_price = 0;
    }
    if(empty($tax_type_id))
    {
      $tax_type_id = 0;
    }

    if($tax_type_id == 0)
    {
      $total_amount = $unit_price *$quantity;
      $vat = 0;
    }
    if($tax_type_id == 1)
    {
      $total_amount = ($unit_price * $quantity)*1.16;
      $vat = ($unit_price * $quantity)*0.16;
    }
    if($tax_type_id == 2)
    {
      $total_amount = ($unit_price * $quantity)*1.05;
      $vat = ($unit_price * $quantity)*0.05;
    }

    if($tax_type_id == 3)
    {
      $total_amount = ($unit_price * $quantity);
      $vat = ($unit_price * $quantity)*0.16;
    }

    $response['message'] = 'success';
    $response['amount'] = $total_amount;
    $response['vat'] = $vat;

    echo json_encode($response);


  }

  public function add_invoice_item($provider_id,$provider_invoice_id = NULL)
  {

    $this->form_validation->set_rules('quantity', 'Invoice Item', 'trim|required|xss_clean');
    $this->form_validation->set_rules('unit_price', 'Unit Price', 'trim|required|xss_clean');
    $this->form_validation->set_rules('account_to_id', 'Expense Account', 'trim|required|xss_clean');
    $this->form_validation->set_rules('item_description', 'Item', 'trim|required|xss_clean');
    $this->form_validation->set_rules('tax_type_id', 'VAT Type', 'trim|xss_clean');
    $this->form_validation->set_rules('vat_amount', 'VAT Amount', 'trim|xss_clean');
    $this->form_validation->set_rules('total_amount', 'Total Amount', 'trim|xss_clean');

    //if form conatins invalid data
    if ($this->form_validation->run())
    {
			// var_dump($_POST);die();
      $this->providers_model->add_invoice_item($provider_id,$provider_invoice_id);
      $this->session->set_userdata("success_message", 'Invoice Item successfully added');
      $response['status'] = 'success';
      $response['message'] = 'Payment successfully added';
    }
    else
    {
      $this->session->set_userdata("error_message", validation_errors());
      $response['status'] = 'fail';
      $response['message'] = validation_errors();

    }
    $redirect_url = $this->input->post('redirect_url');
    redirect($redirect_url);

  }

  public function confirm_invoice_note($provider_id,$provider_invoice_id = NULL)
  {
    $this->form_validation->set_rules('vat_charged', 'tax', 'trim|xss_clean');
		$this->form_validation->set_rules('amount_charged', 'Amount Charged', 'trim|xss_clean');
    $this->form_validation->set_rules('invoice_date', 'Invoice Date ', 'trim|required|xss_clean');
    $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
    $this->form_validation->set_rules('invoice_number', 'Invoice Number', 'trim|required|xss_clean');
		if ($this->form_validation->run())
		{
			// var_dump($_POST);die();
				$this->providers_model->confirm_provider_invoice($provider_id,$provider_invoice_id);

				$this->session->set_userdata("success_message", 'provider invoice successfully added');
				$response['status'] = 'success';
				$response['message'] = 'Payment successfully added';
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();

		}


    if(!empty($provider_invoice_id))
    {
        redirect('accounting/provider-invoices');
    }
    else
    {
      $redirect_url = $this->input->post('redirect_url');
      redirect($redirect_url);
    }

        
  }


  // credit notes

  public function providers_credit_note()
  {
    // $v_data['property_list'] = $property_list;


     $provider_id = $this->session->userdata('credit_note_provider_id_searched');


    $where = 'provider_credit_note.provider_credit_note_status AND provider_credit_note.provider_id = '.$provider_id;

    $search_purchases = $this->session->userdata('search_purchases');
    if($search_purchases)
    {
      $where .= $search_purchases;
    }
    $table = 'provider_credit_note';


    $segment = 3;
    $this->load->library('pagination');
    $config['base_url'] = site_url().'accounting/provider-credit-notes';
    $config['total_rows'] = $this->purchases_model->count_items($table, $where);
    $config['uri_segment'] = $segment;
    $config['per_page'] = 20;
    $config['num_links'] = 5;

    $config['full_tag_open'] = '<ul class="pagination pull-right">';
    $config['full_tag_close'] = '</ul>';

    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';

    $config['next_tag_open'] = '<li>';
    $config['next_link'] = 'Next';
    $config['next_tag_close'] = '</span>';

    $config['prev_tag_open'] = '<li>';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $this->pagination->initialize($config);

    $page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
    $query = $this->providers_model->get_all_providers_details($table, $where, $config["per_page"], $page, $order='provider_credit_note.transaction_date', $order_method='DESC');
    $v_data['provider_credit_notes'] = $query;
    $v_data['page'] = $page;

    $data['title'] = 'provider Credit Notes';
    $v_data['title'] = $data['title'];
    $data['content'] = $this->load->view('providers/providers_credit_notes', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
  }


  public function search_providers_credit_notes($provider_id=null)
  {
    if(!empty($provider_id))
    {
      $provider_id = $provider_id_searched = $provider_id;
    }
    else
    {
      $provider_id = $provider_id_searched = $this->input->post('provider_id');
    }



    $search_title = '';

    if(!empty($provider_id))
    {

      $provider_id = ' AND provider.provider_id = '.$provider_id.' ';
    }
    $search = $provider_id;
      // var_dump($provider_id_searched);die();
    $this->session->set_userdata('credit_note_provider_id_searched', $provider_id_searched);
    $this->session->set_userdata('search_providers_credit_notes', $search);
    redirect('accounting/provider-credit-notes');
  }

  public function close_searched_credit_notes_provider()
  {
    $this->session->unset_userdata('credit_note_provider_id_searched');
    $this->session->unset_userdata('search_providers_credit_notes');
    redirect('accounting/providers');
  }

  public function add_credit_note_item($provider_id,$provider_credit_note_id=NULL)
  {

    $this->form_validation->set_rules('amount', 'Unit Price', 'trim|required|xss_clean');
    $this->form_validation->set_rules('account_to_id', 'Invoice', 'trim|required|xss_clean');
    $this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean');
    $this->form_validation->set_rules('tax_type_id', 'VAT Type', 'trim|xss_clean');

    //if form conatins invalid data
    if ($this->form_validation->run())
    {
      $this->providers_model->add_credit_note_item($provider_id,$provider_credit_note_id);
      $this->session->set_userdata("success_message", 'Invoice Item successfully added');
      $response['status'] = 'success';
      $response['message'] = 'Payment successfully added';
    }
    else
    {
      $this->session->set_userdata("error_message", validation_errors());
      $response['status'] = 'fail';
      $response['message'] = validation_errors();

    }
    $redirect_url = $this->input->post('redirect_url');
    redirect($redirect_url);

  }

  public function confirm_credit_note($provider_id,$provider_credit_note_id=NULL)
  {
    $this->form_validation->set_rules('vat_charged', 'tax', 'trim|xss_clean');
		$this->form_validation->set_rules('amount_charged', 'Amount Charged', 'trim|xss_clean');
    $this->form_validation->set_rules('credit_note_date', 'Invoice Date ', 'trim|required|xss_clean');
    $this->form_validation->set_rules('invoice_id', 'Invoice ', 'trim|required|xss_clean');
    $this->form_validation->set_rules('amount', 'Amount', 'trim|xss_clean');
    $this->form_validation->set_rules('credit_note_number', 'Invoice Number', 'trim|xss_clean');

		if ($this->form_validation->run())
		{
        // var_dump($_POST);die();
				$this->providers_model->confirm_provider_credit_note($provider_id,$provider_credit_note_id);

				$this->session->set_userdata("success_message", 'provider invoice successfully added');
				$response['status'] = 'success';
				$response['message'] = 'Payment successfully added';
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();

		}

    if(!empty($provider_credit_note_id))
    {
     
      redirect('accounting/provider-credit-notes');
    }
    else
    {
      $redirect_url = $this->input->post('redirect_url');
      redirect($redirect_url);
    }
  }

  // providers payments_import


  public function providers_payments()
  {
    // $v_data['property_list'] = $property_list;
      $provider_id = $this->session->userdata('payment_provider_id_searched');


    $where = 'provider_payment.provider_payment_status = 1 AND account.account_id = provider_payment.account_from_id AND provider_payment.provider_id = '.$provider_id;

    $search_purchases = $this->session->userdata('search_purchases');
    if($search_purchases)
    {
      $where .= $search_purchases;
    }
    $table = 'provider_payment,account';

 // $this->db->join('account','account.account_id = provider_payment.account_from_id','left');

    $segment = 3;
    $this->load->library('pagination');
    $config['base_url'] = site_url().'accounting/provider-payments';
    $config['total_rows'] = $this->purchases_model->count_items($table, $where);
    $config['uri_segment'] = $segment;
    $config['per_page'] = 20;
    $config['num_links'] = 5;

    $config['full_tag_open'] = '<ul class="pagination pull-right">';
    $config['full_tag_close'] = '</ul>';

    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';

    $config['next_tag_open'] = '<li>';
    $config['next_link'] = 'Next';
    $config['next_tag_close'] = '</span>';

    $config['prev_tag_open'] = '<li>';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $this->pagination->initialize($config);

    $page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
    $query = $this->providers_model->get_all_providers_details($table, $where, $config["per_page"], $page, $order='provider_payment.transaction_date', $order_method='DESC');
    $v_data['provider_payments'] = $query;
    $v_data['page'] = $page;
    $data['title'] = 'provider Payments';

    $v_data['title'] = $data['title'];
    $data['content'] = $this->load->view('providers/providers_payments', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
  }


  public function search_providers_payments($provider_id=null)
  {
    if(!empty($provider_id))
    {
       $provider_id = $provider_id_searched = $provider_id;
    }
    else
    {
       $provider_id = $provider_id_searched = $this->input->post('provider_id');
    }



    $search_title = '';

    if(!empty($provider_id))
    {

      $provider_id = ' AND provider.provider_id = '.$provider_id.' ';
    }
    $search = $provider_id;
      // var_dump($provider_id_searched);die();
    $this->session->set_userdata('payment_provider_id_searched', $provider_id_searched);
    $this->session->set_userdata('search_providers_payments', $search);
    redirect('accounting/provider-payments');
  }

  public function close_searched_payments_provider()
  {
    $this->session->unset_userdata('payment_provider_id_searched');
    $this->session->unset_userdata('search_providers_payments');
    redirect('accounting/providers');
  }
  public function add_payment_item($provider_id,$provider_payment_id = NULL)
  {

    $this->form_validation->set_rules('amount_paid', 'Unit Price', 'trim|required|xss_clean');
    // $this->form_validation->set_rules('invoice_id', 'Invoice', 'trim|required|xss_clean');

    //if form conatins invalid data
    if ($this->form_validation->run())
    {

      $this->providers_model->add_payment_item($provider_id,$provider_payment_id);
      $this->session->set_userdata("success_message", 'Payment successfully added');
      $response['status'] = 'success';
      $response['message'] = 'Payment successfully added';
    }
    else
    {
      $this->session->set_userdata("error_message", validation_errors());
      $response['status'] = 'fail';
      $response['message'] = validation_errors();

    }
    $redirect_url = $this->input->post('redirect_url');
    redirect($redirect_url);

  }

  public function confirm_payment($provider_id,$provider_payment_id=NULL)
  {
    $this->form_validation->set_rules('reference_number', 'Reference Number', 'trim|required|xss_clean');
    $this->form_validation->set_rules('payment_date', 'Invoice Date ', 'trim|required|xss_clean');
    $this->form_validation->set_rules('amount_paid', 'Amount', 'trim|required|xss_clean');
    $this->form_validation->set_rules('account_from_id', 'Payment Amount', 'trim|required|xss_clean');

    if ($this->form_validation->run())
    {
        // var_dump($_POST);die();


        $status = $this->providers_model->confirm_provider_payment($provider_id,$provider_payment_id);

        if($status == TRUE)
        {

          if(!empty($provider_payment_id))
          {
            $this->session->set_userdata("success_message", 'Payment has been successfully updated');
            $response['status'] = 'success';
            $response['message'] = 'Payment successfully added';
            $redirect_url = $this->input->post('redirect_url');
            redirect('provider-statement/'.$provider_id);

          }
          else
          {
            $this->session->set_userdata("success_message", 'provider invoice successfully added');
            $response['status'] = 'success';
            $response['message'] = 'Payment successfully added';
            $redirect_url = $this->input->post('redirect_url');
            redirect($redirect_url);
          }

        }
        else
        {
          $this->session->set_userdata("success_message", 'Sorry could not make the update. Please try again');
          $response['status'] = 'success';
          $response['message'] = 'Payment successfully added';
          $redirect_url = $this->input->post('redirect_url');
          redirect($redirect_url);
        }
        

    }
    else
    {
      $this->session->set_userdata("error_message", validation_errors());
      $response['status'] = 'fail';
      $response['message'] = validation_errors();

    }


    $redirect_url = $this->input->post('redirect_url');
    redirect($redirect_url);

    


  }


  /*
  *
  * Add a new provider
  *
  */
  public function add_provider()
  {
    //form validation rules
    $this->form_validation->set_rules('provider_name', 'Name', 'required|xss_clean');
    $this->form_validation->set_rules('provider_email', 'Email', 'xss_clean');
    $this->form_validation->set_rules('provider_phone', 'Phone', 'xss_clean');
    $this->form_validation->set_rules('provider_location', 'Location', 'xss_clean');
    $this->form_validation->set_rules('provider_building', 'Building', 'xss_clean');
    $this->form_validation->set_rules('provider_floor', 'Floor', 'xss_clean');
    $this->form_validation->set_rules('provider_address', 'Address', 'xss_clean');
    $this->form_validation->set_rules('provider_post_code', 'Post code', 'xss_clean');
    $this->form_validation->set_rules('provider_city', 'City', 'xss_clean');
    $this->form_validation->set_rules('provider_contact_person_name', 'Contact Name', 'xss_clean');
    $this->form_validation->set_rules('provider_contact_person_onames', 'Contact Other Names', 'xss_clean');
    $this->form_validation->set_rules('provider_contact_person_phone1', 'Contact Phone 1', 'xss_clean');
    $this->form_validation->set_rules('provider_contact_person_phone2', 'Contact Phone 2', 'xss_clean');
    $this->form_validation->set_rules('provider_contact_person_email', 'Contact Email', 'valid_email|xss_clean');
    $this->form_validation->set_rules('provider_description', 'Description', 'xss_clean');
    $this->form_validation->set_rules('balance_brought_forward', 'Balance BroughtF','xss_clean');
    $this->form_validation->set_rules('debit_id', 'Balance BroughtF','xss_clean');

    // var_dump($_POST); die();
    //if form conatins invalid data
    if ($this->form_validation->run())
    {
      $provider_id = $this->providers_model->add_provider();
      if($provider_id > 0)
      {
        $this->session->set_userdata("success_message", "provider added successfully");
        $redirect_url = $this->input->post('redirect_url');
        if(!empty($redirect_url))
        {
          redirect($redirect_url);
        }
        else
        {
          redirect('accounting/providers');
        }
      }

      else
      {
        $this->session->set_userdata("error_message","Could not add provider. Please try again");

        $redirect_url = $this->input->post('redirect_url');
        if(!empty($redirect_url))
        {
          redirect($redirect_url);
        }
        else
        {
          redirect('accounting/providers');
        }
      }
    }
    $data['title'] = 'Add provider';
    $v_data['title'] = $data['title'];
    $data['content'] = $this->load->view('providers/add_provider', $v_data, true);

    $this->load->view('admin/templates/general_page', $data);
  }

  /*
  *
  * Add a new provider
  *
  */
  public function edit_provider($provider_id)
  {
    //form validation rules
    $this->form_validation->set_rules('provider_name', 'Name', 'required|xss_clean');
    $this->form_validation->set_rules('provider_email', 'Email', 'xss_clean');
    $this->form_validation->set_rules('provider_phone', 'Phone', 'xss_clean');
    $this->form_validation->set_rules('provider_location', 'Location', 'xss_clean');
    $this->form_validation->set_rules('provider_building', 'Building', 'xss_clean');
    $this->form_validation->set_rules('provider_floor', 'Floor', 'xss_clean');
    $this->form_validation->set_rules('provider_address', 'Address', 'xss_clean');
    $this->form_validation->set_rules('provider_post_code', 'Post code', 'xss_clean');
    $this->form_validation->set_rules('provider_city', 'City', 'xss_clean');
    $this->form_validation->set_rules('provider_contact_person_name', 'Contact Name', 'xss_clean');
    $this->form_validation->set_rules('provider_contact_person_onames', 'Contact Other Names', 'xss_clean');
    $this->form_validation->set_rules('provider_contact_person_phone1', 'Contact Phone 1', 'xss_clean');
    $this->form_validation->set_rules('provider_contact_person_phone2', 'Contact Phone 2', 'xss_clean');
    $this->form_validation->set_rules('provider_contact_person_email', 'Contact Email', 'valid_email|xss_clean');
    $this->form_validation->set_rules('provider_description', 'Description', 'xss_clean');
    $this->form_validation->set_rules('balance_brought_forward', 'Balance BroughtF','xss_clean');
    $this->form_validation->set_rules('debit_id', 'Balance BroughtF','xss_clean');

    //if form conatins invalid data
    if ($this->form_validation->run())
    {
      $provider_id = $this->providers_model->edit_provider($provider_id);
      if($provider_id > 0)
      {
        $this->session->set_userdata("success_message", "provider updated successfully");
        redirect('accounting/providers');
      }

      else
      {
        $this->session->set_userdata("error_message","Could not add provider. Please try again");
        redirect('finance/edit-provider/'.$provider_id);
      }
    }
    $data['title'] = 'Add provider';
    $v_data['title'] = $data['title'];
    $v_data['provider'] = $this->providers_model->get_provider($provider_id);
    $data['content'] = $this->load->view('providers/edit_provider', $v_data, true);

    $this->load->view('admin/templates/general_page', $data);
  }

  public function transfer_provider_bills()
  {
    $this->db->from('account_invoices');
    $this->db->select('*');
    $this->db->where('account_to_type = 2 AND account_invoice_deleted = 0 AND account_from_id > 0 and sync_status = 0');

    $query = $this->db->get();
    // var_dump($query);die();
    if($query->num_rows() > 0)
    {
      foreach ($query->result() as $key => $value) {
        # code...
        $invoice_amount = $value->invoice_amount;
        $account_to_id = $value->account_to_id;
        $account_invoice_status = $value->account_invoice_status;
        $account_invoice_description = $value->account_invoice_description;
        $account_from_id = $value->account_from_id;
        $created = $value->created;
        $created_by = $value->created_by;
        $invoice_number = $value->invoice_number;
        $department_id = $value->department_id;
        $invoice_date = $value->invoice_date;
        $account_invoice_id = $value->account_invoice_id;

        $exploded = explode('-', $invoice_date);

        $year = $exploded[0];
        $month = $exploded[1];

        $document_number = $this->providers_model->create_invoice_number();

        $invoice['amount'] = $invoice_amount;
        $invoice['provider_invoice_status'] = 1;
        $invoice['vat_charged'] = 0;
        $invoice['total_amount'] = $invoice_amount;
        $invoice['created'] = $created;
        $invoice['created_by'] = $created_by;
        $invoice['transaction_date'] = $invoice_date;
        $invoice['invoice_number'] = $invoice_number;
        $invoice['document_number'] = $document_number;
        $invoice['provider_id'] = $account_from_id;
        $invoice['invoice_year'] = $year;
        $invoice['invoice_month'] = $month;


        $this->db->insert('provider_invoice',$invoice);
        $provider_invoice_id = $this->db->insert_id();


        $invoice_item['provider_invoice_id'] = $provider_invoice_id;
        $invoice_item['item_description'] = $account_invoice_description;
        $invoice_item['unit_price'] = $invoice_amount;
        $invoice_item['total_amount'] = $invoice_amount;
        $invoice_item['created'] = $created;
        $invoice_item['created_by'] = $created_by;
        $invoice_item['quantity'] = 1;
        $invoice_item['year'] = $year;
        $invoice_item['month'] = $month;
        $invoice_item['provider_id'] = $account_from_id;
        $invoice_item['account_to_id'] = $account_from_id;
        $this->db->insert('provider_invoice_item',$invoice_item);

        $update_item['sync_status'] = 1;
        $this->db->where('account_invoice_id',$account_invoice_id);
        $this->db->update('account_invoices',$update_item);


      }
    }
  }



  public function transfer_provider_payments()
  {
    $this->db->from('account_payments');
    $this->db->select('*');
    $this->db->where('account_to_type = 2 AND account_payment_deleted = 0 AND account_to_id > 0 AND sync_status = 0');

    $query = $this->db->get();
    // var_dump($query);die();
    if($query->num_rows() > 0)
    {
      foreach ($query->result() as $key => $value) {
        # code...
        $amount_paid = $value->amount_paid;
        $account_to_id = $value->account_to_id;
        $account_payment_status = $value->account_payment_status;
        $account_payment_description = $value->account_payment_description;
        $account_from_id = $value->account_from_id;
        $created = $value->created;
        $created_by = $value->created_by;
        $receipt_number = $value->receipt_number;
        $payment_to = $value->payment_to;
        $payment_date = $value->payment_date;
        $account_payment_id = $value->account_payment_id;


        $exploded = explode('-', $payment_date);

        $year = $exploded[0];
        $month = $exploded[1];

        $document_number = $this->providers_model->create_credit_payment_number();

        $invoice['total_amount'] = $amount_paid;
        $invoice['provider_payment_status'] = 1;
        $invoice['created'] = $created;
        $invoice['created_by'] = $created_by;
        $invoice['transaction_date'] = $payment_date;
        $invoice['reference_number'] = $receipt_number;
        $invoice['document_number'] = $document_number;
        $invoice['provider_id'] = $account_to_id;
        $invoice['account_from_id'] = $account_from_id;
        $invoice['payment_year'] = $year;
        $invoice['payment_month'] = $month;


        $this->db->insert('provider_payment',$invoice);
        $provider_payment_id = $this->db->insert_id();

        $invoice_item['provider_payment_id'] = $provider_payment_id;
        $invoice_item['description'] = $account_payment_description;
        $invoice_item['amount_paid'] = $amount_paid;
        $invoice_item['created'] = $created;
        $invoice_item['created_by'] = $created_by;
        $invoice_item['year'] = $year;
        $invoice_item['month'] = $month;
        $invoice_item['provider_id'] = $account_to_id;
        $invoice_item['invoice_type'] = 2;
        $this->db->insert('provider_payment_item',$invoice_item);

          $update_item['sync_status'] = 1;
        $this->db->where('account_payment_id',$account_payment_id);
        $this->db->update('account_payments',$update_item);
      }
    }
  }
	public function merge_credit_notes()
	{

		$this->db->select('*');
		$this->db->where('is_store = 3 and supplier_id > 0');
		$query = $this->db->get('orders');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$supplier_invoice_number = $value->supplier_invoice_number;
				$order_id = $value->order_id;

				$this->db->select('*');
				$this->db->where('is_store = 0 and supplier_id > 0 and supplier_invoice_number = "'.$supplier_invoice_number.'"');
				$this->db->limit(1);
				$query2 = $this->db->get('orders');
				if($query2->num_rows() == 1)
				{
					// update the reference_id
					$rows = $query2->row();
					$reference_id = $rows->order_id;

					$update_array['reference_id'] = $reference_id;
					$this->db->where('is_store = 3 and order_id ='.$order_id);
					$this->db->update('orders',$update_array);
				}
			}
		}
	}




  public function providers_list()
  {
    $data['title'] = 'Vendor Expenses';
    $v_data['title'] = $data['title'];

		// var_dump($v_date); die();
    $data['content'] = $this->load->view('providers/providers_accounts', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
  }
	public function delete_provider_payment_item($provider_payment_item_id,$provider_id,$provider_payment_id = NULL)
	{
		$this->db->where('provider_payment_item_id',$provider_payment_item_id);
		$this->db->delete('provider_payment_item');

    if(!empty($provider_payment_id))
    {
        redirect('edit-provider-payment/'.$provider_payment_id);
    }
		else
    {
      redirect('accounting/provider-payments');
    }
	}


	public function delete_provider_invoice_item($provider_invoice_item_id,$provider_id,$provider_invoice_id = NUll)
	{
		$this->db->where('provider_invoice_item_id',$provider_invoice_item_id);
		$this->db->delete('provider_invoice_item');

    if(!empty($provider_invoice_id))
    {
        redirect('provider-invoice/edit-provider-invoice/'.$provider_invoice_id);
    }
    else
    {
          redirect('accounting/provider-invoices');
    }
		
	}


  public function allocate_provider_payment($provider_payment_id,$provider_payment_item_id,$provider_id)
  {


    $data['title'] = 'Allocating provider Payment';
    $v_data['title'] = $data['title'];
    $v_data['provider_id'] = $provider_id;
    $v_data['provider_payment_id'] = $provider_payment_id;
    $v_data['provider_payment_item_id'] = $provider_payment_item_id;


    // var_dump($v_date); die();
    $data['content'] = $this->load->view('providers/allocate_provider_payment', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
  }


  public function delete_payment_item($provider_payment_id,$provider_payment_item_id,$provider_payment_item_id_db,$provider_id)
  {

    // var_dump($provider_payment_item_id_db);die();
    $this->db->where('provider_payment_item_id',$provider_payment_item_id_db);
    $this->db->delete('provider_payment_item');

    redirect('allocate-payment/'.$provider_payment_id.'/'.$provider_payment_item_id.'/'.$provider_id);
  }
  

  public function search_providers()
  {
    $provider_name = $year_from = $this->input->post('provider_name');
    $redirect_url = $this->input->post('redirect_url');
    $provider_search = '';
     if(!empty($provider_name))
    {
      $provider_search .= 'providers LIKE \'%'.$provider_name.'%\'';

    }

    $search = $provider_search;

    

    // var_dump($date_to); die();
    $this->session->set_userdata('provider_search',$search);
    redirect($redirect_url);

  }
  public function close_provider_provider_search()
  {

    $this->session->unset_userdata('provider_search');


    redirect('accounting/providers');

  }

  public function get_invoice_details($provider_invoice_id)
  {
    $data['provider_invoice_id'] = $provider_invoice_id;
    $this->load->view('providers/view_provider_invoice', $data);  
  }


  public function get_suppliers_invoice_details($order_id)
  {
    $data['order_id'] = $order_id;
    $this->load->view('providers/view_supplier_invoice', $data);  
  }
  public function get_payment_details($provider_payment_id)
  {
    $data['provider_payment_id'] = $provider_payment_id;
    $this->load->view('providers/view_provider_payment', $data);  
  }

  public function delete_provider_invoice($provider_invoice_id)
  {
    $update_query['provider_invoice_status'] = 2;
    $update_query['last_modified_by'] = $this->session->userdata('personnel_id');
    $this->db->where('provider_invoice_id',$provider_invoice_id);
    $this->db->update('provider_invoice',$update_query);

    redirect('accounting/provider-invoices');
  }

  public function edit_provider_invoice($provider_invoice_id)
  {

      $data['title'] = 'Edit provider Invoice';
      $v_data['provider_invoice_id'] = $provider_invoice_id;
      $provider_id = $this->session->userdata('invoice_provider_id_searched');
      $v_data['title'] = $data['title'];
      $data['content'] = $this->load->view('providers/edit_provider_invoice', $v_data, true);
      $this->load->view('admin/templates/general_page', $data);

  }


  public function delete_provider_payment($provider_payment_id)
  {
    $update_query['provider_payment_status'] = 2;
    $update_query['last_modified_by'] = $this->session->userdata('personnel_id');
    $this->db->where('provider_payment_id',$provider_payment_id);
    $this->db->update('provider_payment',$update_query);

    redirect('accounting/provider-payments');
  }

  public function edit_provider_payment($provider_payment_id)
  {

      $data['title'] = 'Edit provider Invoice';
      $v_data['provider_payment_id'] = $provider_payment_id;
      $provider_id = $this->session->userdata('payment_provider_id_searched');
      $v_data['title'] = $data['title'];
      $data['content'] = $this->load->view('providers/edit_provider_payment', $v_data, true);
      $this->load->view('admin/templates/general_page', $data);

  }

   public function delete_credit_note_item($provider_credit_note_item_id,$provider_credit_note_id=NULL)
  {
   
    $this->db->where('provider_credit_note_item_id',$provider_credit_note_item_id);
    $this->db->delete('provider_credit_note_item',$update_query);

    if(!empty($provider_credit_note_id))
    {
      redirect('edit-provider-credit-note/'.$provider_credit_note_id);
    }
    else
    {
      redirect('accounting/provider-credit-notes');
    }
  }

  public function delete_provider_credit_note($provider_credit_note_id,$provider_id)
  {


    $update_query['provider_credit_note_status'] = 2;
    $update_query['last_modified_by'] = $this->session->userdata('personnel_id');
    $this->db->where('provider_credit_note_id',$provider_credit_note_id);
    $this->db->update('provider_credit_note',$update_query);

    redirect('accounting/provider-credit-notes');
  }
  public function edit_provider_credit_note($provider_credit_note_id)
  {
     $data['title'] = 'Edit provider Credit Note';
      $v_data['provider_credit_note_id'] = $provider_credit_note_id;
      $provider_id = $this->session->userdata('credit_note_provider_id_searched');
      $v_data['title'] = $data['title'];
      $data['content'] = $this->load->view('providers/edit_provider_credit_note', $v_data, true);
      $this->load->view('admin/templates/general_page', $data);
  }

  public function get_credit_note_details($provider_credit_note_id)
  {
    $data['provider_credit_note_id'] = $provider_credit_note_id;
    $this->load->view('providers/view_provider_credit_notes', $data); 
  }

  public function delete_provider($provider_id)
  {
      $provider_entry['provider_status'] = 1;
      $this->db->where('provider_id',$provider_id);
      $this->db->update('provider',$provider_entry);

      redirect('accounting/providers');
  }

   public function export_providers()
  {
    $this->providers_model->export_providers();
  }

}
?>
