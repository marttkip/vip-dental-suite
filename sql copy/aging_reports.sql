CREATE OR REPLACE VIEW v_aged_payables AS
-- Creditr Invoices
SELECT
	creditor.creditor_id AS recepientId,
	creditor.creditor_name as payables,
	creditor.branch_id as branch_id,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) )  = 0, v_general_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) )  = 0, v_general_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) )  = 0, v_general_ledger_aging.dr_amount, 0 ))
        - Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) )  = 0, v_general_ledger_aging.cr_amount, 0 ))
        END
  ) AS `coming_due`,
	(
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 1 AND 30, v_general_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 1 AND 30, v_general_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 1 AND 30, v_general_ledger_aging.dr_amount, 0 ))
				  - Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) )  BETWEEN 1 AND 30, v_general_ledger_aging.cr_amount, 0 ))
			END
  ) AS `thirty_days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_general_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_general_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_general_ledger_aging.dr_amount, 0 ))
				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_general_ledger_aging.cr_amount, 0 ))
			END
  ) AS `sixty_days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_general_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_general_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_general_ledger_aging.dr_amount, 0 ))
				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) )  BETWEEN 61 AND 90, v_general_ledger_aging.cr_amount, 0 ))
		END
  ) AS `ninety_days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) >90, v_general_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) >90, v_general_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) >90, v_general_ledger_aging.dr_amount, 0 ))
				 - Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) )  >90, v_general_ledger_aging.cr_amount, 0 ))
			END
  ) AS `over_ninety_days`,

  (

    (
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) )  = 0, v_general_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) )  = 0, v_general_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) )  = 0, v_general_ledger_aging.dr_amount, 0 ))
					 - Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) )  = 0, v_general_ledger_aging.cr_amount, 0 ))
  		END
    )-- Getting the Value for 0 Days
    + (
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 1 AND 30, v_general_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 1 AND 30, v_general_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 1 AND 30, v_general_ledger_aging.dr_amount, 0 ))
 - Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) )  BETWEEN 1 AND 30, v_general_ledger_aging.cr_amount, 0 ))
  		END
    ) --  AS `1-30 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_general_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_general_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_general_ledger_aging.dr_amount, 0 ))

				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) )  BETWEEN 31 AND 60, v_general_ledger_aging.cr_amount, 0 ))
				END

    ) -- AS `31-60 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_general_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_general_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_general_ledger_aging.dr_amount, 0 ))
				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_general_ledger_aging.cr_amount, 0 ))
  		END
    ) -- AS `61-90 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) >90, v_general_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) >90, v_general_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) ) >90, v_general_ledger_aging.dr_amount, 0 ))
				  - Sum(IF( DATEDIFF( CURDATE( ), date( v_general_ledger_aging.referenceDate ) )  >90, v_general_ledger_aging.cr_amount, 0 ))
  		END
    ) -- AS `>90 Days`
  ) AS `Total`

	FROM
		creditor
	LEFT JOIN v_general_ledger_aging ON v_general_ledger_aging.recepientId = creditor.creditor_id AND v_general_ledger_aging.recepientId > 0 AND v_general_ledger_aging.referenceDate >= creditor.start_date

	 GROUP BY creditor.creditor_id;


	CREATE OR REPLACE VIEW v_aged_receivables AS
		SELECT
			visit.visit_type AS receivables_id,
			'Organizational' AS receivables_type,
			visit_type.visit_type_name AS receivable_Name,-- DATEDIFF( CURDATE( ), visit_charge.date ) AS dueDate,
			(
				CASE
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) )  = 0, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
		       THEN 0 -- Output
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) )  = 0, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
		       THEN
		-- Sum(IF( DATEDIFF( CURDATE( ), date( orders.supplier_invoice_date ) )  = 0, order_supplier.total_amount, 0 ))
							Sum( IF ( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) = 0, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 ) )
							
							- ( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
							
					END
			)AS `current`,
			(
					CASE
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 1 AND 30, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
		       THEN 0 -- Output
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 1 AND 30, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
		       THEN
					Sum( IF ( DATEDIFF( CURDATE( ), visit_invoice.created ) BETWEEN 1 AND 30, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 ) )
					
					- (( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
					 )
				END
			)AS `thirty_days`,
			(
				CASE
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 31 AND 60, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
		       THEN 0 -- Output
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 31 AND 60, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
		       THEN
						Sum( IF ( DATEDIFF( CURDATE( ), visit_invoice.created ) BETWEEN 31 AND 60, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 ) )
						
						- (( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
						 )
					END
			) `sixty_days`,
			(
				CASE
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 61 AND 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
		       THEN 0 -- Output
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 61 AND 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
		       THEN
							Sum( IF ( DATEDIFF( CURDATE( ), visit_invoice.created ) BETWEEN 61 AND 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 ) )
							
							- (( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
							 )
					END
					)AS `ninetydays`,
					(
							CASE
								WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) > 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
								 THEN 0 -- Output
								WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) > 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
								 THEN
										Sum( IF ( DATEDIFF( CURDATE( ), visit_invoice.created ) > 90, visit_charge.visit_charge_amount, 0 ) )
										
										- (( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
										 )
					END
					)AS `over_ninetydays`,
			(
				(
				CASE
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) )  = 0, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
		       THEN 0 -- Output
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) )  = 0, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
		       THEN
		-- Sum(IF( DATEDIFF( CURDATE( ), date( orders.supplier_invoice_date ) )  = 0, order_supplier.total_amount, 0 ))
							Sum( IF ( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) = 0, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 ) )
							
							- ( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
							
					END
				)

				+
				(
					CASE
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 1 AND 30, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
		       THEN 0 -- Output
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 1 AND 30, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
		       THEN
					Sum( IF ( DATEDIFF( CURDATE( ), visit_invoice.created ) BETWEEN 1 AND 30, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 ) )
					
					- (( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
					 )
				END
				)
				+
				(
				CASE
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 31 AND 60, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
		       THEN 0 -- Output
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 31 AND 60, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
		       THEN
						Sum( IF ( DATEDIFF( CURDATE( ), visit_invoice.created ) BETWEEN 31 AND 60, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 ) )
						
						- (( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
						 )
					END
				)
				+
				(
				CASE
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 61 AND 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
		       THEN 0 -- Output
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 61 AND 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
		       THEN
							Sum( IF ( DATEDIFF( CURDATE( ), visit_invoice.created ) BETWEEN 61 AND 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 ) )
							
							- (( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
							 )
					END
					)
			+
					(
							CASE
								WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) > 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
								 THEN 0 -- Output
								WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) > 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
								 THEN
										Sum( IF ( DATEDIFF( CURDATE( ), visit_invoice.created ) > 90, visit_charge.visit_charge_amount, 0 ) )
										
										- (( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
										 )
					END
					)

				) AS `total_owed`

		FROM
			visit_charge,visit_invoice
			JOIN visit ON visit.visit_id = visit_invoice.visit_id
			LEFT JOIN visit_type ON visit_type.visit_type_id = visit.visit_type
			WHERE visit.visit_delete = 0 
			AND visit_charge.charged = 1 
			AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id 
			AND visit_charge.visit_charge_delete = 0
			GROUP BY visit_type.visit_type_id;

	CREATE OR REPLACE VIEW v_aged_branch_receivables AS
		SELECT
			visit.visit_type AS receivables_id,
			'Organizational' AS receivables_type,
			visit.branch_id AS branch_id,
			visit_type.visit_type_name AS receivable_Name,-- DATEDIFF( CURDATE( ), visit_charge.date ) AS dueDate,
			(
				CASE
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) )  = 0, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
		       THEN 0 -- Output
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) )  = 0, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
		       THEN
		-- Sum(IF( DATEDIFF( CURDATE( ), date( orders.supplier_invoice_date ) )  = 0, order_supplier.total_amount, 0 ))
							Sum( IF ( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) = 0, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 ) )
							
							- ( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
							
					END
			)AS `current`,
			(
					CASE
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 1 AND 30, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
		       THEN 0 -- Output
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 1 AND 30, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
		       THEN
					Sum( IF ( DATEDIFF( CURDATE( ), visit_invoice.created ) BETWEEN 1 AND 30, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 ) )
					
					- (( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
					 )
				END
			)AS `thirty_days`,
			(
				CASE
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 31 AND 60, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
		       THEN 0 -- Output
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 31 AND 60, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
		       THEN
						Sum( IF ( DATEDIFF( CURDATE( ), visit_invoice.created ) BETWEEN 31 AND 60, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 ) )
						
						- (( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
						 )
					END
			) `sixty_days`,
			(
				CASE
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 61 AND 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
		       THEN 0 -- Output
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 61 AND 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
		       THEN
							Sum( IF ( DATEDIFF( CURDATE( ), visit_invoice.created ) BETWEEN 61 AND 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 ) )
							
							- (( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
							 )
					END
					)AS `ninetydays`,
					(
							CASE
								WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) > 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
								 THEN 0 -- Output
								WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) > 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
								 THEN
										Sum( IF ( DATEDIFF( CURDATE( ), visit_invoice.created ) > 90, visit_charge.visit_charge_amount, 0 ) )
										
										- (( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
										 )
					END
					)AS `over_ninetydays`,
			(
				(
				CASE
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) )  = 0, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
		       THEN 0 -- Output
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) )  = 0, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
		       THEN
		-- Sum(IF( DATEDIFF( CURDATE( ), date( orders.supplier_invoice_date ) )  = 0, order_supplier.total_amount, 0 ))
							Sum( IF ( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) = 0, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 ) )
							
							- ( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
							
					END
				)

				+
				(
					CASE
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 1 AND 30, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
		       THEN 0 -- Output
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 1 AND 30, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
		       THEN
					Sum( IF ( DATEDIFF( CURDATE( ), visit_invoice.created ) BETWEEN 1 AND 30, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 ) )
					
					- (( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
					 )
				END
				)
				+
				(
				CASE
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 31 AND 60, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
		       THEN 0 -- Output
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 31 AND 60, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
		       THEN
						Sum( IF ( DATEDIFF( CURDATE( ), visit_invoice.created ) BETWEEN 31 AND 60, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 ) )
						
						- (( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
						 )
					END
				)
				+
				(
				CASE
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 61 AND 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
		       THEN 0 -- Output
		      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) BETWEEN 61 AND 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
		       THEN
							Sum( IF ( DATEDIFF( CURDATE( ), visit_invoice.created ) BETWEEN 61 AND 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 ) )
							
							- (( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
							 )
					END
					)
			+
					(
							CASE
								WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) > 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) = 0 -- condtion to see if the date has no bills
								 THEN 0 -- Output
								WHEN Sum(IF( DATEDIFF( CURDATE( ), date( visit_invoice.created ) ) > 90, (visit_charge.visit_charge_amount*visit_charge.visit_charge_units), 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
								 THEN
										Sum( IF ( DATEDIFF( CURDATE( ), visit_invoice.created ) > 90, visit_charge.visit_charge_amount, 0 ) )
										
										- (( SELECT COALESCE ( sum( payment_item.payment_item_amount ), 0 ) FROM payments,payment_item WHERE ( payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND payment_item.payment_item_deleted = 0 AND payments.cancel = 0 AND payments.payment_type = 1 ) )
										 )
					END
					)

				) AS `total_owed`,
			

		FROM
			visit_charge,visit_invoice
			JOIN visit ON visit.visit_id = visit_invoice.visit_id
			LEFT JOIN visit_type ON visit_type.visit_type_id = visit.visit_type
			WHERE visit.visit_delete = 0 
			AND visit_charge.charged = 1 
			AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id 
			AND visit_charge.visit_charge_delete = 0
			GROUP BY visit_type.visit_type_id,visit.branch_id;
